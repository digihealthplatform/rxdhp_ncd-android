package com.dhp.screening;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.di.component.ApplicationComponent;
import com.dhp.screening.data.di.component.DaggerApplicationComponent;
import com.dhp.screening.data.di.module.ApplicationModule;
import com.dhp.screening.data.event.EventPermissionGranted;
import com.dhp.screening.data.manager.LDatabase;
import com.dhp.screening.data.manager.SQLCipherHelperImpl;
import com.dhp.screening.data.merger.ImportDataEncryptedV1;
import com.dhp.screening.util.LLog;
import com.facebook.stetho.Stetho;
import com.raizlabs.android.dbflow.config.DatabaseConfig;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseHelperListener;
import com.raizlabs.android.dbflow.structure.database.OpenHelper;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

import javax.inject.Inject;

import es.dmoral.toasty.Toasty;

import static com.dhp.screening.util.LConstants.ACTION_OPEN_CAMERA;
import static com.dhp.screening.util.LConstants.ACTION_OPEN_FILE;
import static com.dhp.screening.util.LConstants.ACTION_OPEN_GALLERY;
import static com.dhp.screening.util.LConstants.DATABASE_ENCRYPTION_KEY;

public class LApplication extends Application {

    private static LApplication instance;

    protected ApplicationComponent applicationComponent;

    @Inject
    DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;

//        FlowManager.init(new FlowConfig.Builder(this).build());

        FlowManager.init(new FlowConfig.Builder(this)
                .addDatabaseConfig(
                        new DatabaseConfig.Builder(LDatabase.class)
                                .openHelper(new DatabaseConfig.OpenHelperCreator() {
                                    @Override
                                    public OpenHelper createHelper(DatabaseDefinition databaseDefinition, DatabaseHelperListener helperListener) {
                                        SQLCipherHelperImpl helper = new SQLCipherHelperImpl(databaseDefinition, helperListener);
                                        StaticData.setDatabasePath(helper.getReadableDatabase(DATABASE_ENCRYPTION_KEY).getPath());
                                        StaticData.setSqlCipherHelper(helper);
                                        return helper;
                                    }
                                }).build())
                .build());

        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);

        File oldDb = FlowManager.getContext().getDatabasePath("ncd_screening" + ".db");

        if (oldDb.exists()) {
            try {
                encryptDatabase();
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }

            deleteDatabase("ncd_screening.db");
        }

        Stetho.initializeWithDefaults(this);
    }

    public static LApplication get(Context context) {
        return (LApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public static Context getContext() {
        return instance;
    }

    private void encryptDatabase() {
        try {
            File outputPath = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_backup_path));

            if (!outputPath.exists()) {
                outputPath.mkdirs();
            }

            File outputFolder = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_backup_folder));

            if (!outputFolder.exists()) {
                outputFolder.mkdirs();
            }

            if (outputFolder.canWrite()) {
                String backupDBPath = "temp_database.db";

                File currentDB = FlowManager.getContext().getDatabasePath("ncd_screening" + ".db");
                File tempDb    = new File(outputFolder, backupDBPath);

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(tempDb).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();

                ImportDataEncryptedV1.mergeDb(this, dataManager, tempDb.getPath());

                tempDb.delete();

                Toasty.success(this, "App updated", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toasty.error(this, "Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    public static boolean doActionWithPermissionCheck(Activity activity, int action) {

        ArrayList<String> permissionsList = new ArrayList<>();

        switch (action) {

            case ACTION_OPEN_GALLERY:
            case ACTION_OPEN_FILE: {
                int permissionFile = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (PackageManager.PERMISSION_GRANTED != permissionFile) {
                    permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                break;
            }

            case ACTION_OPEN_CAMERA: {
                int permissionCamera = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
                int permissionFile   = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (PackageManager.PERMISSION_GRANTED != permissionCamera) {
                    permissionsList.add(Manifest.permission.CAMERA);
                }
                if (PackageManager.PERMISSION_GRANTED != permissionFile) {
                    permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                }
                break;
            }
        }

        if (permissionsList.isEmpty()) {
            EventBus.getDefault().post(new EventPermissionGranted(action, true));
            return true;
        } else {
            ActivityCompat.requestPermissions(activity, permissionsList.toArray(new String[0]), action);
            return false;
        }
    }
}