package com.dhp.screening.ui.vital.hemoglobin;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.di.component.ActivityComponent;
import com.dhp.screening.data.di.component.DaggerActivityComponent;
import com.dhp.screening.data.di.module.ActivityModule;
import com.dhp.screening.ui.BaseActivity;

import static com.dhp.screening.util.LConstants.DEFAULT_DURATION;

public class ActivitySettings extends BaseActivity {

    @BindView(R.id.et_duration)
    EditText etDuration;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    private ActivityComponent activityComponent;

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(LApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Settings", true);

        getActivityComponent(this).inject(this);

        etDuration.setText("" + dataManager.getIntPref("duration", DEFAULT_DURATION));
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        try {
            dataManager.setPref("duration", Integer.parseInt(etDuration.getText().toString()));
        } catch (Exception e) {

        }

        Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
        finish();
    }
}