package com.dhp.screening.ui.admin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterLab;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemLab;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.ui.vital.hemoglobin.ActivityCameraView;
import com.dhp.screening.util.LToast;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_A_VALUES;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_B_VALUES;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_HB_WAIT_DURATION;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_L_VALUES;
import static com.dhp.screening.util.LConstants.TYPE_CALIBRATE;


public class ActivityAdminHemoglobin extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_values)
    RecyclerView rvValues;

    @BindView(R.id.et_duration)
    EditText etDuration;

    @Inject
    DataManager dataManager;

    private AdapterLab adapterLab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_hemoglobin);
        initToolbar(toolbar, "Hb - Settings", false);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        ArrayList<ItemLab> labList = getValuesList();

        adapterLab = new AdapterLab(this);
        adapterLab.setItems(labList);

        rvValues.setLayoutManager(new LinearLayoutManager(this));
        rvValues.setAdapter(adapterLab);
        rvValues.setNestedScrollingEnabled(false);

        int duration = dataManager.getIntPref(KEY_HB_WAIT_DURATION, AssetReader.getTestDuration());

        etDuration.setText("" + duration);
    }

    private ArrayList<ItemLab> getValuesList() {
        ArrayList<ItemLab> items = new ArrayList<>(6);

        String[] lValues = dataManager.getStringPref(KEY_L_VALUES, "").split(";");
        String[] aValues = dataManager.getStringPref(KEY_A_VALUES, "").split(";");
        String[] bValues = dataManager.getStringPref(KEY_B_VALUES, "").split(";");

        for (int i = 0; i < 6; i++) {
            ItemLab itemLab = new ItemLab();
            itemLab.setHb("" + (i * 2 + 4));

            try {
                itemLab.setlValue(lValues[i]);
                itemLab.setaValue(aValues[i]);
                itemLab.setbValue(bValues[i]);
            } catch (Exception e) {
            }

            items.add(itemLab);
        }

        return items;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_exit"));
        builder.setPositiveButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityAdminHemoglobin.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin_hemoglobin, menu);

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_logout:
                confirmLogout();
                return true;

            case R.id.action_calibrate:
                Intent intent = new Intent(this, ActivityCameraView.class);
                intent.putExtra("activity_type", TYPE_CALIBRATE);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_save)
    void onSaveClicked() {

        if (!adapterLab.isAllValuesEntered()) {
            LToast.warning("Please enter all LAB values");
            return;
        }

        int duration = 0;

        try {
            duration = Integer.parseInt(etDuration.getText().toString().trim());
        } catch (Exception e) {
        }

        if (0 == duration) {
            LToast.warning("Please enter a valid duration");
            return;
        }

        dataManager.setPref(KEY_HB_WAIT_DURATION, duration);

        ArrayList<ItemLab> items = adapterLab.getItems();

        StringBuilder lValues = new StringBuilder();
        StringBuilder aValues = new StringBuilder();
        StringBuilder bValues = new StringBuilder();

        for (int i = 0; i < items.size(); i++) {
            if (i > 0) {
                lValues.append(";");
                aValues.append(";");
                bValues.append(";");
            }

            lValues.append(items.get(i).getlValue());
            aValues.append(items.get(i).getaValue());
            bValues.append(items.get(i).getbValue());
        }

        dataManager.setPref(KEY_L_VALUES, lValues.toString());
        dataManager.setPref(KEY_A_VALUES, aValues.toString());
        dataManager.setPref(KEY_B_VALUES, bValues.toString());

        LToast.success("Saved");
    }

    private void confirmLogout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_logout"));
        builder.setPositiveButton(AssetReader.getLangKeyword("logout"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.logout();
                Intent intent = new Intent(ActivityAdminHemoglobin.this, ActivityLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    protected void onLanguageSet(String selectedLanguage) {
        String currentSelectedLanguage = dataManager.getStringPref("selected_language", "English");

        if (!currentSelectedLanguage.equals(selectedLanguage)) {

            if (AssetReader.initSelectedLanguage(selectedLanguage)) {
                dataManager.setPref("selected_language", selectedLanguage);

            } else {
                dataManager.setPref("selected_language", "English");
                LToast.error(R.string.unable_to_set_language);
            }
        }
        recreate();
    }

    private void initLanguage() {
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setTitle(AssetReader.getLangKeyword("logout"));
    }
}