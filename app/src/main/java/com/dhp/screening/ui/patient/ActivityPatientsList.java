package com.dhp.screening.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterPatients;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.ui.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ActivityPatientsList extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @Inject
    DataManager dataManager;

    @BindView(R.id.rv_patients)
    RecyclerView rvPatients;

    private AdapterPatients adapterPatients;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_list);
        initToolbar(toolbar, AssetReader.getLangKeyword("existing_profiles"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        adapterPatients = new AdapterPatients(this, dataManager);
        rvPatients.setLayoutManager(new LinearLayoutManager(this));
        rvPatients.setAdapter(adapterPatients);

        List<TableDemographics> patients = dataManager.getAllPatients();

        if (null == patients || 0 == patients.size()) {
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapterPatients.refreshData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        List<TableDemographics> patients = dataManager.getAllPatients();

        if (null != patients && 0 < patients.size()) {
            getMenuInflater().inflate(R.menu.menu_search, menu);
        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_search:
                startActivity(new Intent(this, ActivityPatientSearch.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void initLanguage() {
        tvEmpty.setText(AssetReader.getLangKeyword("empty"));
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);

        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("search"));
        }
    }
}