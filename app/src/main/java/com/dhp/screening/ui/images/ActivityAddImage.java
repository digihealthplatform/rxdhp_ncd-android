package com.dhp.screening.ui.images;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.event.EventFilesRenamed;
import com.dhp.screening.data.event.EventOpenPicker;
import com.dhp.screening.data.item.ItemFileAttachment;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.ui.vital.ActivityVitalBase;
import com.dhp.screening.util.LToast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.net.URLConnection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_RATION;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityAddImage extends ActivityVitalBase {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_attach)
    ImageView ivAttach;

    @BindView(R.id.tv_instruction)
    TextView tvInstruction;

    @BindView(R.id.sp_image_types)
    Spinner spImageTypes;

    @BindView(R.id.et_image_type_other)
    EditText etImageTypeOther;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Inject
    DataManager dataManager;

    private String registrationId;

    // TODO: 20-Oct-19, remove hardcodnig
    private String[] imageTypes = new String[]

            {
                    "Select",
                    "Photo (Face)",
                    "Lab",
                    "ECG",
                    "X-ray",
                    "CT-Scan",
                    "Ultrasound",
                    "MRI",
                    "Prescription",
                    "Reports",
                    "Doc",
                    "Other"
            };

//    private String[] imageTypesEnglish;

    private TableImage tableImage;

    private long lastClicked;

    private ItemFileAttachment itemAttachment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Add Image/Report", true);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, null, null);
        initDataManager(dataManager);

        initLanguage();

        setSpinnerAdapter(spImageTypes, imageTypes);

        if (savedInstanceState == null) {
            registrationId = getIntent().getStringExtra("registration_id");
        } else {
            registrationId = savedInstanceState.getString("registration_id");
        }

        tableImage = new TableImage();

        spImageTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position > 0) {
                    if (imageTypes[position].equals("Others")) {
                        tableImage.setImageType("");
                        etImageTypeOther.setVisibility(VISIBLE);
                    } else {
                        tableImage.setImageType(imageTypes[position]);
                        etImageTypeOther.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("registration_id", registrationId);
        super.onSaveInstanceState(outState);
    }

    @OnClick({R.id.iv_attach, R.id.tv_instruction})
    void onAttachFileClicked() {
        if (lastClicked > System.currentTimeMillis() - 2000) {
            return;
        }

        if (null == itemAttachment) {
            openPicker();

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // TODO: 24-Aug-19, lang translation
            String[] optionsArray = {"View", "Capture new", "Remove"};

            builder.setItems(optionsArray, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            File file = new File(itemAttachment.getFilePath());

                            Intent intent = new Intent(Intent.ACTION_VIEW);

                            intent.setDataAndType(
                                    Uri.fromFile(file),
                                    URLConnection.guessContentTypeFromName(file.getName()));

                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(intent);

                            break;
                        }

                        case 1: {
                            deleteCapture();
                            openPicker();
                            break;
                        }

                        case 2: {
                            deleteCapture();
                            break;
                        }
                    }
                }
            });
            builder.show();
        }

        lastClicked = System.currentTimeMillis();
    }

    private void openPicker() {
        EventOpenPicker event = new EventOpenPicker();
        EventBus.getDefault().post(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(EventFilesRenamed event) {
        try {
            itemAttachment = event.getOutputFilePaths().get(0);
            showCapturedImage();
        } catch (Exception e) {
        }
    }

    private void deleteCapture() {
        if (null != itemAttachment) {
            if (itemAttachment.getFileName().contains("tmpfile_")) {
                new File(itemAttachment.getFilePath()).delete();
            }
        }
        itemAttachment = null;
        ivAttach.setImageResource(R.drawable.camera_plus_black);
        tvInstruction.setVisibility(VISIBLE);
    }

    private void showCapturedImage() {
        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.file_document)
                .error(R.drawable.file_document);

        Glide.with(this)
                .load(itemAttachment.getFilePath())
                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(requestOptions)
                .into(ivAttach);

        tvInstruction.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_save)
    void onSaveClicked() {
        readValues();

        if (isAllEntered()) {
            saveImage();
        }
    }

    private void saveImage() {
        String imgPrefix = dataManager.getPartnerId()
                + "_"
                + dataManager.getDeviceNumber()
                + "_"
                + registrationId
                + "_";

        String newFileName = itemAttachment.getFileName();
        newFileName = newFileName.replace("tmpfile_", imgPrefix);

        File   tmpFile        = new File(itemAttachment.getFilePath());
        String outputFilePath = tmpFile.getParent() + "/" + newFileName;
        tmpFile.renameTo(new File(outputFilePath));

        tableImage.setImageName(newFileName);
        dataManager.createImage(tableImage);
        LToast.success("Saved");
        finish();
    }

    private void readValues() {
        tableImage.setRegistrationId(registrationId);
        tableImage.setGpsLocation(getLocation());

        tableImage.setImageDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
        tableImage.setImageDateTimeMillis(getSurveyDateTimeMillis());

        tableImage.setNotes(etNotes.getText().toString().trim());

        if (VISIBLE == etImageTypeOther.getVisibility()) {
            tableImage.setImageType(etImageTypeOther.getText().toString().trim());
        }

        if (null != itemAttachment) {
            tableImage.setImageCamera(itemAttachment.isCamera());
        }
    }

    private boolean isAllEntered() {

        if (null == itemAttachment) {
            LToast.warning("Please attach a report/image");
            return false;

        } else if (!hasValue(tableImage.getImageType())) {
            LToast.warning("Please select the image type");
            return false;
        }

        return true;
    }

    private void initLanguage() {
//        tvHouseNumber.setText(AssetReader.getLangKeyword("house_number"));
//        tvStreet.setText(AssetReader.getLangKeyword("street"));
//        tvPost.setText(AssetReader.getLangKeyword("post"));
    }
}