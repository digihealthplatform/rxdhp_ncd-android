package com.dhp.screening.ui.statistics;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LUtils;

import java.io.File;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.util.LConstants.PDF_MISSING_DEMOGRAPHICS;


public class ActivityMissingDemographics extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_missing_demographics)
    TextView tvMissingDemographics;

    @BindView(R.id.btn_generate_pdf)
    Button btnGeneratePdf;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_missing_demographics);
        initToolbar(toolbar, AssetReader.getLangKeyword("missing_demographics"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        ArrayList<String> missingDemographics = dataManager.getMissingDemographics();

        StringBuilder text = new StringBuilder();

        for (int i = 0; i < missingDemographics.size(); i++) {
            text.append(i + 1).append(". ").append(missingDemographics.get(i)).append("\n");
        }

        tvMissingDemographics.setText(text.toString());
    }

    @OnClick(R.id.btn_generate_pdf)
    void generatePdf() {
        File outputFile = new File(LUtils.getPdfFilePathMissingDemographics(dataManager));

        GeneratePdfTask generatePdfTask =
                new GeneratePdfTask(this, outputFile, dataManager, PDF_MISSING_DEMOGRAPHICS, null);

        generatePdfTask.execute();
    }

    private void initLanguage() {
        btnGeneratePdf.setText(AssetReader.getLangKeyword("generate_pdf"));
    }
}