package com.dhp.screening.ui.dialog;

import com.dhp.screening.data.table.TableDeathInfo;

public interface DialogDeathInfoCallbacks {
    void onDeathInfoChanged(TableDeathInfo item, int position);
}