package com.dhp.screening.ui.vital;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemMinMax;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.util.LUtils.formatFloat;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.roundOff;


public class ActivityEnterHeight extends ActivityVitalBase {

    private boolean isEdit;

    private String unit;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.et_height)
    EditText etHeight;

    @BindView(R.id.sp_height_unit)
    Spinner spHeightUnit;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.tv_name)
    TextView tvName;

    @Inject
    DataManager dataManager;

    private TableVitals tableVitals;

    private String registrationId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_height);
        initToolbar(toolbar, getString(R.string.enter_height), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.enter_height));

        final String units[] = getResources().getStringArray(R.array.height_units);
        setSpinnerAdapter(spHeightUnit, units);

        tableVitals = new TableVitals();

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit = true;
            tableVitals = StaticData.getCurrentVital();
            etHeight.setText(tableVitals.getVitalValues());

            if (tableVitals.getVitalUnits().equals("cm")) {
                spHeightUnit.setSelection(1);

            } else {
                spHeightUnit.setSelection(2);
            }

            etNotes.setText(tableVitals.getNotes());
            btnSave.setText("Update");
        }

        spHeightUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position > 0) {
                    unit = units[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        registrationId = StaticData.getRegistrationId();

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        boolean popupShown = false;

        if (!isEdit) {
            popupShown = checkAlreadyEntered(registrationId, "Height");
            spHeightUnit.setSelection(1);
        }

        if (!popupShown) {
            showPopupMessage("Height", "height");
        }
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        float height = 0.0f;
        try {
            height = Float.valueOf(etHeight.getText().toString().trim());
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (etHeight.getText().toString().isEmpty() || 0 == height) {
            LToast.warning("Enter height");

        } else if (null == unit) {
            LToast.warning(R.string.select_unit);

        } else {
            if (unit.equals("inch")) {
                height = height * 2.54f;
            }

            ItemMinMax minMax = AssetReader.getVitalRange("Height");

            if (height < minMax.getMin() || height > minMax.getMax()) {
                LToast.warningLong("The height should be between " + formatFloat(minMax.getMin()) + " cm - " + formatFloat(minMax.getMax()) + " cm");
                return;
            }

            if (isEdit) {
                tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

            } else {

                if (VISIBLE == llAadhaar.getVisibility()) {
                    registrationId = getRegistrationId();
                }

                if (null == registrationId || registrationId.equals("")) {
                    LToast.warning("Enter Registration ID");
                    return;
                }

                tableVitals.setRegistrationId(registrationId.toUpperCase());
            }

            tableVitals.setVitalType("Height");

            tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
            tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());
            tableVitals.setVitalSubTypes("Height");
            tableVitals.setVitalValues(roundOff(etHeight.getText().toString()));
            tableVitals.setVitalUnits(unit);
            tableVitals.setNotes(etNotes.getText().toString());
            tableVitals.setGpsLocation(getLocation());
            tableVitals.setQrScanned(qrScanned);

            if (isEdit) {
                dataManager.updateVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("updated"));
                StaticData.setCurrentVital(tableVitals);
            } else {
                dataManager.addVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("saved"));
            }

            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}