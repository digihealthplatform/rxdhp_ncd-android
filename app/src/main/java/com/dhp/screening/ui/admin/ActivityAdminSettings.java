package com.dhp.screening.ui.admin;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.network.JsonSyncTask;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.showSyncOptions;
import static com.dhp.screening.data.manager.SharedPrefsHelper.AUTO_SYNC_ENABLED;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_ALLOW_SYNC;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_AUTO_SYNC;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEVICE_NUMBER;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_LAST_SYNC_DATE_TIME;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityAdminSettings extends BaseActivity
        implements JsonSyncTask.CallBack {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_last_sync)
    TextView tvLastSync;

    @BindView(R.id.cb_allow_sync)
    CheckBox cbAllowSync;

    @BindView(R.id.view_sync_divider)
    View viewSyncDivider;

    @BindView(R.id.cb_auto_sync)
    CheckBox cbAutoSync;

    @BindView(R.id.et_device_number)
    EditText etDeviceNumber;

    @BindView(R.id.et_partner_name)
    EditText etPartnerName;

    @BindView(R.id.ll_sync_options)
    LinearLayout llSyncOptions;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_settings);

        initToolbar(toolbar, getString(R.string.admin_settings), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        if (!showSyncOptions()) {
            cbAllowSync.setVisibility(View.GONE);
            viewSyncDivider.setVisibility(View.GONE);
        }

        cbAllowSync.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dataManager.setPref(KEY_ALLOW_SYNC, b);

                if (b) {
                    llSyncOptions.setVisibility(VISIBLE);
                } else {
                    dataManager.setPref(KEY_AUTO_SYNC, false);
                    cbAutoSync.setChecked(false);
                    llSyncOptions.setVisibility(View.GONE);
                }
            }
        });
        cbAllowSync.setChecked(dataManager.getBooleanPref(KEY_ALLOW_SYNC, false));

        cbAutoSync.setChecked(dataManager.getBooleanPref(KEY_AUTO_SYNC, AUTO_SYNC_ENABLED));
        cbAutoSync.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                dataManager.setPref(KEY_AUTO_SYNC, b);
            }
        });

        etDeviceNumber.setText(dataManager.getStringPref(KEY_DEVICE_NUMBER, ""));
        etPartnerName.setText(dataManager.getPartnerId());

        tvLastSync.setText(getString(R.string.last_sync) + " " + dataManager.getStringPref(KEY_LAST_SYNC_DATE_TIME, ""));
    }

    @Override
    protected void onStart() {
        super.onStart();
        syncDataBackground(dataManager, this);
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        String deviceNumber = etDeviceNumber.getText().toString();

        dataManager.setPref(KEY_DEVICE_NUMBER, deviceNumber);

        final TableCampInfo campInfo = new TableCampInfo();
        campInfo.setPartnerId(etPartnerName.getText().toString().toUpperCase());

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));

        builder.setPositiveButton(AssetReader.getLangKeyword("confirm"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                TableCampInfo tableCampInfo = dataManager.getCampInfo();

                if (null != tableCampInfo) {
                    campInfo.setCampName(tableCampInfo.getCampName());
                    campInfo.setStartDate(tableCampInfo.getStartDate());
                    campInfo.setEndDate(tableCampInfo.getEndDate());
                    campInfo.setDoorToDoor(tableCampInfo.isDoorToDoor());
                }

                dataManager.createOrUpdateCampInfo(campInfo);
                LToast.success(AssetReader.getLangKeyword("saved"));
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @OnClick(R.id.btn_sync_now)
    void syncData() {

//        try {
//            String partnerName = dataManager.getCampInfo().getPartnerId();
//            if (partnerName.isEmpty()) {
//                LToast.warningLong("Please enter the camp details including Partner ID");
//                return;
//            }
//        } catch (Exception e) {
//            LToast.warningLong("Please enter the camp details including Partner ID");
//            return;
//        }

        syncDataForeground(dataManager, this);
    }

    @Override
    public void onSyncStarted() {
        jsonSyncInProgress = true;
    }

    @Override
    public void onSyncComplete() {
        jsonSyncInProgress = false;
        tvLastSync.setText(getString(R.string.last_sync) + " " + dataManager.getStringPref(KEY_LAST_SYNC_DATE_TIME, ""));
    }

    @Override
    public void onSyncFailed(String message) {
        if (hasValue(message)) {
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(message);
            builder.setPositiveButton(AssetReader.getLangKeyword("okay"), null);
            builder.show();
        }
        jsonSyncInProgress = false;
    }
}