package com.dhp.screening.ui.statistics;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.range.ItemRangeCount;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LUtils;

import static com.dhp.screening.util.LUtils.getColorValue;


public class ActivityGraph extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    @BindView(R.id.pie_chart_bmi)
    PieChart pcBmi;

    @BindView(R.id.pie_chart_bp)
    PieChart pcBp;

    @BindView(R.id.pie_chart_rbs)
    PieChart pcRbs;

    @BindView(R.id.pie_chart_fbs)
    PieChart pcFbs;

    @BindView(R.id.pie_chart_ppbs)
    PieChart pcPpbs;

    @BindView(R.id.pie_chart_hb)
    PieChart pcHb;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    private ArrayList<ItemRangeCount> bmiRanges;
    private ArrayList<ItemRangeCount> bpRanges;
    private ArrayList<ItemRangeCount> rbsRanges;
    private ArrayList<ItemRangeCount> fbsRanges;
    private ArrayList<ItemRangeCount> ppbsRanges;
    private ArrayList<ItemRangeCount> hemoglobinRanges;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        initToolbar(toolbar, AssetReader.getLangKeyword("graph"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        ArrayList<String> enabledVitals = AssetReader.getVitalsList();

        bmiRanges = AssetReader.getBmiRanges(true);
        bpRanges = AssetReader.getBpRanges();
        rbsRanges = AssetReader.getRbsRanges();
        fbsRanges = AssetReader.getFbsRanges();
        ppbsRanges = AssetReader.getPpbsRanges();
        hemoglobinRanges = AssetReader.getHemoglobinRanges();

        if (false) {
            ArrayList<Entry> yvalues = new ArrayList<Entry>();
            yvalues.add(new Entry(8f, 0));
            yvalues.add(new Entry(15f, 1));
            yvalues.add(new Entry(12f, 2));
            yvalues.add(new Entry(25f, 3));
            yvalues.add(new Entry(23f, 4));

            PieDataSet dataSet = new PieDataSet(yvalues, "Results");

            ArrayList<String> xVals = new ArrayList<String>();

            xVals.add("Very High");
            xVals.add("High");
            xVals.add("Normal");
            xVals.add("Low");
            xVals.add("Very Low");

            PieData data = new PieData(xVals, dataSet);

            data.setValueTextSize(16f);
            data.setValueTextColor(Color.DKGRAY);

            data.setValueFormatter(new PercentFormatter());

            dataSet.setColors(new int[]{
                    getColorValue(R.color.dark_red),
                    getColorValue(R.color.red),
                    getColorValue(R.color.orange),
                    getColorValue(R.color.yellow),
                    getColorValue(R.color.green)});

            findViewById(R.id.ll_bmi).setVisibility(View.VISIBLE);
            pcBmi.setData(data);

            return;
        }

        List<TableDemographics> tableDemographicsList = dataManager.getAllPatients();

        if (0 == tableDemographicsList.size()) {
            tvEmpty.setVisibility(View.VISIBLE);
            return;
        }

        for (TableDemographics tableDemographics : tableDemographicsList) {
            if (tableDemographics.isInactive()) {
                continue;
            }
            List<TableVitals> vitals = dataManager.getAllVitals(tableDemographics.getRegistrationId(), false);

            for (TableVitals tableVital : vitals) {

                switch (tableVital.getVitalType()) {
                    case "Weight": {
                        if (!enabledVitals.contains("Weight")) {
                            break;
                        }

                        final String latestHeight = dataManager.getLatestHeight(tableVital.getRegistrationId(), tableVital.getVitalDateTimeMillis());

                        if (null != latestHeight) {
                            String[] height = latestHeight.split(";");

                            float weight = 0.0f;

                            try {
                                weight = Float.parseFloat(tableVital.getVitalValues());
                            } catch (Exception e) {
                            }

                            double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                    height[1],
                                    weight);

                            ItemRange itemRange = AssetReader.getBmiRange(bmi, LUtils.calculateAge(tableDemographics.getYob()), true);
                            setRangeCount(bmiRanges, itemRange);
                        }
                        break;
                    }

                    case "Blood Pressure": {

                        if (!enabledVitals.contains("Blood Pressure")) {
                            break;
                        }

                        double systolic  = 0.0;
                        double diastolic = 0.0;

                        try {
                            systolic = Double.valueOf(tableVital.getVitalValues().split(";")[0]);
                            diastolic = Double.valueOf(tableVital.getVitalValues().split(";")[1]);
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic, true);
                        setRangeCount(bpRanges, itemRange);

                        break;
                    }

                    case "Blood Sugar": {
                        if (!enabledVitals.contains("Blood Sugar")) {
                            break;
                        }

                        double value = 0.0;

                        try {
                            value = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getRbsRange(value, tableVital.getVitalSubTypes());

                        if (tableVital.getVitalSubTypes().equals("RBS")) {
                            setRangeCount(rbsRanges, itemRange);

                        } else if (tableVital.getVitalSubTypes().equals("FBS")) {
                            setRangeCount(fbsRanges, itemRange);

                        } else {
                            setRangeCount(ppbsRanges, itemRange);
                        }

                        break;
                    }

                    case "Hemoglobin": {

                        if (!enabledVitals.contains("Hemoglobin")) {
                            break;
                        }

                        double hemoglobin = 0.0;

                        try {
                            hemoglobin = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getHbRange(hemoglobin, tableDemographics.getGender(), LUtils.calculateAge(tableDemographics.getYob()));
                        setRangeCount(hemoglobinRanges, itemRange);

                        break;
                    }

                    case "Albumin": {
                        break;
                    }
                }
            }
        }

        for (String enabledVital : enabledVitals) {

            switch (enabledVital) {
                case "Weight": {
                    if (getTotalCount(bmiRanges) > 0) {
                        findViewById(R.id.ll_bmi).setVisibility(View.VISIBLE);
                        displayGraph(pcBmi, bmiRanges);
                    }
                }
                break;

                case "Blood Pressure": {
                    if (getTotalCount(bpRanges) > 0) {
                        findViewById(R.id.ll_bp).setVisibility(View.VISIBLE);
                        displayGraph(pcBp, bpRanges);
                    }
                }
                break;

                case "Blood Sugar": {
                    if (getTotalCount(rbsRanges) > 0) {
                        findViewById(R.id.ll_rbs).setVisibility(View.VISIBLE);
                        displayGraph(pcRbs, rbsRanges);
                    }

                    if (getTotalCount(fbsRanges) > 0) {
                        findViewById(R.id.ll_fbs).setVisibility(View.VISIBLE);
                        displayGraph(pcFbs, fbsRanges);
                    }

                    if (getTotalCount(ppbsRanges) > 0) {
                        findViewById(R.id.ll_ppbs).setVisibility(View.VISIBLE);
                        displayGraph(pcPpbs, ppbsRanges);
                    }
                }
                break;

                case "Hemoglobin": {
                    if (getTotalCount(hemoglobinRanges) > 0) {
                        findViewById(R.id.ll_hb).setVisibility(View.VISIBLE);
                        displayGraph(pcHb, hemoglobinRanges);
                    }
                }
                break;
            }
        }
    }

    private void displayGraph(PieChart pieChart, ArrayList<ItemRangeCount> ranges) {
        ArrayList<Entry>   yValues = new ArrayList<>();
        ArrayList<String>  xValues = new ArrayList<>();
        ArrayList<Integer> colors  = new ArrayList<>();

        int index = 0;

        for (ItemRangeCount rangeCount : ranges) {
            if (rangeCount.getCount() > 0) {
                yValues.add(new Entry(rangeCount.getCount(), index));
                xValues.add(rangeCount.getRange());
                colors.add(Color.parseColor(rangeCount.getColor()));
            }
            ++index;
        }

        PieDataSet dataSet = new PieDataSet(yValues, "Results");
        PieData    data    = new PieData(xValues, dataSet);
        data.setValueTextSize(16f);
        data.setValueTextColor(Color.DKGRAY);
        data.setValueFormatter(new PercentFormatter());

        dataSet.setColors(colors);

        pieChart.setUsePercentValues(true);
        pieChart.setDrawCenterText(false);
        pieChart.setDrawSliceText(false);
        pieChart.setData(data);
        pieChart.setDescription("");
        pieChart.getLegend().setEnabled(true);
    }

    private void setRangeCount(ArrayList<ItemRangeCount> ranges, ItemRange itemRange) {
        for (ItemRangeCount range : ranges) {
            if (range.getRange().equals(itemRange.getRange())) {
                range.setCount(range.getCount() + 1);
            }
        }
    }

    private int getTotalCount(ArrayList<ItemRangeCount> ranges) {
        int count = 0;

        for (ItemRangeCount range : ranges) {
            count += range.getCount();
        }

        return count;
    }
}