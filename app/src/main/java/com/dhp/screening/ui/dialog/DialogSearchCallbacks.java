package com.dhp.screening.ui.dialog;

public interface DialogSearchCallbacks {
    void onSelected(String value, int position);
}