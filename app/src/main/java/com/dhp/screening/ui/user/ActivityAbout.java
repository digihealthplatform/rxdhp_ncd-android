package com.dhp.screening.ui.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dhp.screening.BuildConfig;
import com.dhp.screening.R;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.ui.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ActivityAbout extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_app_version)
    TextView tvAbout;

    @BindView(R.id.tv_build_type)
    TextView tvBuildType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        initToolbar(toolbar, AssetReader.getLangKeyword("about"), true);
        ButterKnife.bind(this);

        tvAbout.setText(AssetReader.getLangKeyword("app_version") + BuildConfig.VERSION_NAME);

        if (getIntent().getBooleanExtra("super_admin", false)) {
            tvBuildType.setVisibility(View.VISIBLE);
            tvBuildType.setText("Build type: " + AssetReader.getStringKey("build_type"));
        }
    }
}