package com.dhp.screening.ui.admin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterUser;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.merger.ImportData;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.ui.stock.ActivityAddStock;
import com.dhp.screening.ui.user.ActivityAbout;
import com.dhp.screening.util.LConstants;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.dhp.screening.data.manager.LDatabase.DATABASE_NAME;
import static com.dhp.screening.data.manager.SharedPrefsHelper.SUPER_ADMIN_1;
import static com.dhp.screening.data.manager.SharedPrefsHelper.SUPER_ADMIN_2;
import static com.dhp.screening.data.manager.SharedPrefsHelper.SUPER_ADMIN_3;
import static com.dhp.screening.util.LUtils.exportCsv;


public class ActivityAdminHome extends BaseActivity {

    private static final int CODE_FILE_PICKER_REQUEST = 101;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_users)
    TextView tvUsers;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.rv_user_list)
    RecyclerView rvUserList;

    @Inject
    DataManager dataManager;

    private AdapterUser adapterUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        initToolbar(toolbar, AssetReader.getLangKeyword("admin_home"), false);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshUsersList();
    }

    void refreshUsersList() {
        adapterUser = new AdapterUser(this, dataManager, rvUserList, tvEmpty);
        rvUserList.setLayoutManager(new LinearLayoutManager(this));
        rvUserList.setAdapter(adapterUser);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_exit"));
        builder.setPositiveButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityAdminHome.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin_home, menu);

        int adminType = dataManager.getAdminType();

        if (SUPER_ADMIN_1 == adminType) {
            MenuItem item = menu.findItem(R.id.action_admin_settings);
            item.setVisible(true);

        } else if (SUPER_ADMIN_2 == adminType) {
            MenuItem item = menu.findItem(R.id.action_admin_settings);
            item.setVisible(true);

            item = menu.findItem(R.id.action_import);
            item.setVisible(true);

            item = menu.findItem(R.id.action_export);
            item.setVisible(true);

            item = menu.findItem(R.id.action_export_csv);
            item.setVisible(true);

        } else if (SUPER_ADMIN_3 == adminType) {
            MenuItem item = menu.findItem(R.id.action_export_csv);
            item.setVisible(true);
        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_change_language:
                String currentSelectedLanguage = dataManager.getStringPref("selected_language", "English");
                changeLanguage(currentSelectedLanguage);
                return true;

            case R.id.action_export:
                exportDatabase();
                return true;

            case R.id.action_export_csv:

                // TODO: 27-Aug-18, Encrypt with password

                String fileName = exportCsv(dataManager);
                Toasty.success(this, AssetReader.getLangKeyword("exported_successfully")).show();

                Uri path = Uri.fromFile(new File(fileName));
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("vnd.android.cursor.dir/email");

                emailIntent.putExtra(Intent.EXTRA_STREAM, path);
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Survey Data:");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));

                return true;

            case R.id.action_import:
                importDatabase();
                return true;

            case R.id.action_logout:
                confirmLogout();
                return true;

            case R.id.action_about:
                Intent intent = new Intent(this, ActivityAbout.class);
                int adminType = dataManager.getAdminType();
                if (SUPER_ADMIN_2 == adminType) {
                    intent.putExtra("super_admin", true);
                }
                startActivity(intent);
                return true;

            case R.id.action_help:
                AssetReader.openHelp(this);
                return true;

            case R.id.action_camp_settings:
                startActivity(new Intent(ActivityAdminHome.this, ActivityCampSettings.class));
                return true;

            case R.id.action_admin_settings:
                startActivity(new Intent(ActivityAdminHome.this, ActivityAdminSettings.class));
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void importDatabase() {

        File outputPath = new File(Environment.getExternalStorageDirectory(),
                LConstants.DEFAULT_DATABASE_IMPORT_PATH);

        if (!outputPath.exists()) {
            outputPath.mkdirs();
        }

        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(CODE_FILE_PICKER_REQUEST)
                .withHiddenFiles(false)
                .withTitle("Select the database file")
                .withPath(outputPath.getPath())
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (RESULT_OK == resultCode) {
            if (CODE_FILE_PICKER_REQUEST == requestCode) {
                onDatabaseSelected(data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH));
            }
        }
    }

    private void onDatabaseSelected(final String path) {

        String[] optionsArray = {AssetReader.getLangKeyword("merge_existing_data"), AssetReader.getLangKeyword("overwrite_existing_data")};

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setItems(optionsArray, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case 0: {
                        confirmImport(0, path);
                    }
                    break;

                    case 1: {
                        confirmImport(1, path);
                        break;
                    }
                }
            }
        });

        builder.show();
    }

    private void confirmImport(final int mergeReplace, final String path) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);

        if (0 == mergeReplace) {
            builder.setMessage(AssetReader.getLangKeyword("confirm_merge"));
        } else {
            builder.setMessage(AssetReader.getLangKeyword("confirm_overwrite"));
        }

        builder.setPositiveButton(AssetReader.getLangKeyword("yes"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ImportData.mergeDb(ActivityAdminHome.this, dataManager, path, mergeReplace);
                refreshUsersList();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void exportDatabase() {
        try {
            File outputPath = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_backup_path));

            if (!outputPath.exists()) {
                outputPath.mkdirs();
            }

            File outputFolder = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_backup_folder));

            if (!outputFolder.exists()) {
                outputFolder.mkdirs();
            }

            Calendar calendar = Calendar.getInstance();

            if (outputFolder.canWrite()) {
                String backupDBPath = getString(R.string.prefix_export_backup)
                        + "_" + dataManager.getPartnerId()
                        + "_" + dataManager.getDeviceNumber()
                        + "_"
                        + LUtils.getFileName(calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DATE),
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        calendar.get(Calendar.SECOND))
                        + ".db";

                File backupDB  = new File(outputFolder, backupDBPath);

                File currentDB;

                if (Build.VERSION.SDK_INT >= 28) {
                    currentDB = new File(StaticData.getDatabasePath());
                } else {
                    currentDB = FlowManager.getContext().getDatabasePath(DATABASE_NAME + ".db");
                }

                LUtils.copy(currentDB, backupDB);

                Toasty.success(this, "Exported successfully", Toast.LENGTH_LONG).show();

                Uri    path        = Uri.fromFile(backupDB);
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");

                emailIntent.putExtra(Intent.EXTRA_STREAM, path);

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Survey Data:");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        } catch (Exception e) {
            Toasty.error(this, "Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    private void confirmLogout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_logout"));
        builder.setPositiveButton(AssetReader.getLangKeyword("logout"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.logout();
                Intent intent = new Intent(ActivityAdminHome.this, ActivityLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @OnClick(R.id.iv_add_user)
    void addUser() {
        startActivity(new Intent(this, ActivityAddUser.class));
    }

    protected void onLanguageSet(String selectedLanguage) {
        String currentSelectedLanguage = dataManager.getStringPref("selected_language", "English");

        if (!currentSelectedLanguage.equals(selectedLanguage)) {

            if (AssetReader.initSelectedLanguage(selectedLanguage)) {
                dataManager.setPref("selected_language", selectedLanguage);

            } else {
                dataManager.setPref("selected_language", "English");
                LToast.error(R.string.unable_to_set_language);
            }
        }
        recreate();
    }

    private void initLanguage() {
        tvUsers.setText(AssetReader.getLangKeyword("users"));
        tvEmpty.setText(AssetReader.getLangKeyword("empty"));
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_change_language);
        item.setTitle(AssetReader.getLangKeyword("change_language"));

        item = menu.findItem(R.id.action_import);
        item.setTitle(AssetReader.getLangKeyword("import_data"));

        item = menu.findItem(R.id.action_camp_settings);
        item.setTitle(AssetReader.getLangKeyword("camp_settings"));

        item = menu.findItem(R.id.action_help);
        item.setTitle(AssetReader.getLangKeyword("help"));

        item = menu.findItem(R.id.action_about);
        item.setTitle(AssetReader.getLangKeyword("about"));

        item = menu.findItem(R.id.action_logout);
        item.setTitle(AssetReader.getLangKeyword("logout"));

        item = menu.findItem(R.id.action_export);
        item.setTitle(AssetReader.getLangKeyword("export_data"));
    }
}