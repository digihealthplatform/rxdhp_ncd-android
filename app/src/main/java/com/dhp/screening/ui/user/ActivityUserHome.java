package com.dhp.screening.ui.user;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.InputFilter;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.event.EventFilesRenamed;
import com.dhp.screening.data.event.EventOpenPicker;
import com.dhp.screening.data.item.ItemFileAttachment;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.lib.barcodereader.ui.ActivityQr;
import com.dhp.screening.lib.bluetooth.MedcheckWeightConnection;
import com.dhp.screening.lib.printer.PrinterManager;
import com.dhp.screening.network.JsonSyncTask;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.dialog.DialogSearch;
import com.dhp.screening.ui.dialog.DialogSearchCallbacks;
import com.dhp.screening.ui.house.ActivityEditHouse;
import com.dhp.screening.ui.house.ActivityHouseList;
import com.dhp.screening.ui.images.ActivityDisplayImageDark;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.ui.patient.ActivityHouseMembers;
import com.dhp.screening.ui.patient.ActivityPatientDetails;
import com.dhp.screening.ui.patient.ActivityPatientsList;
import com.dhp.screening.ui.statistics.ActivityStatistics;
import com.dhp.screening.ui.stock.ActivityStock;
import com.dhp.screening.ui.vital.ActivitySelectVital;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.asset.AssetReader.KEY_ACCEPT_AADHAAR_NUMBER;
import static com.dhp.screening.data.asset.AssetReader.KEY_ALLOW_DUPLICATE_DEMOGRAPHICS;
import static com.dhp.screening.data.asset.AssetReader.KEY_ALLOW_DUPLICATE_IDENTITY_DETAILS;
import static com.dhp.screening.data.asset.AssetReader.KEY_EXPIRED_DEMOGRAPHICS_COUNT;
import static com.dhp.screening.data.asset.AssetReader.KEY_EXPIRED_POPUP_DEMOGRAPHICS_COUNT;
import static com.dhp.screening.data.asset.AssetReader.KEY_EXPIRED_POPUP_VITALS_COUNT;
import static com.dhp.screening.data.asset.AssetReader.KEY_EXPIRED_VITALS_COUNT;
import static com.dhp.screening.data.asset.AssetReader.KEY_GENDER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_IDENTITY_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_ADD_VITAL_IN_HOME;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_CAPTURE_FACE;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_STOCK_OPTION;
import static com.dhp.screening.data.asset.AssetReader.KEY_USER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.getEnglishIndex;
import static com.dhp.screening.data.asset.AssetReader.getFaceImageType;
import static com.dhp.screening.data.asset.AssetReader.getSelectedLanguageVersion;
import static com.dhp.screening.data.asset.AssetReader.getStringKey;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_ALLOW_SYNC;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_PRINT_ID;
import static com.dhp.screening.util.LConstants.SHOW_SCAN_QR;
import static com.dhp.screening.util.LConstants.ZXING_CAMERA_PERMISSION;
import static com.dhp.screening.util.LUtils.calculateAge;
import static com.dhp.screening.util.LUtils.calculateYob;
import static com.dhp.screening.util.LUtils.getFormattedDate;
import static com.dhp.screening.util.LUtils.getSpinnerIndexOther;
import static com.dhp.screening.util.LUtils.getTextFromXml;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityUserHome extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        JsonSyncTask.CallBack {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_is_demo_user)
    TextView tvIsDemoUser;

    @BindView(R.id.tv_scan_qr)
    TextView tvScanQr;

    @BindView(R.id.iv_capture_face)
    ImageView ivCaptureFace;

    @BindView(R.id.et_reg_id)
    EditText etRegId;

    @BindView(R.id.btn_search_patient)
    Button btnSearch;

    @BindView(R.id.tv_new_profile)
    TextView tvNewProfile;

    @BindView(R.id.tv_clear_fields)
    TextView tvClear;

    @BindView(R.id.et_full_name)
    EditText etFullName;

    @BindView(R.id.tv_date_of_birth)
    TextView tvDobTitle;

    @BindView(R.id.tv_or)
    TextView tvOr;

    @BindView(R.id.tv_years)
    TextView tvYears;

    @BindView(R.id.et_dob)
    EditText etDob;

    @BindView(R.id.et_age)
    EditText etAge;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_email_id)
    EditText etEmailId;

    @BindView(R.id.et_identity_type_other)
    EditText etIdentityTypeOther;

    @BindView(R.id.et_house)
    EditText etHouse;

    @BindView(R.id.tv_identity)
    TextView tvIdentity;

    @BindView(R.id.tv_phone)
    TextView tvPhone;

    @BindView(R.id.tv_email_id)
    TextView tvEmailId;

    @BindView(R.id.tv_house)
    TextView tvHouse;

    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.tv_view_house_members)
    TextView tvHouseMembers;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.iv_scan_qr)
    ImageView ivScanQr;

    @BindView(R.id.et_identity_no)
    EditText etIdentityNumber;

    @BindView(R.id.sp_identity_type)
    Spinner spIdentityType;

    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    @BindView(R.id.rb_male)
    RadioButton rbMale;

    @BindView(R.id.rb_female)
    RadioButton rFemale;

    @BindView(R.id.rb_other)
    RadioButton rbOther;

    @BindView(R.id.iv_add_household)
    ImageView ivAddHouse;

    @BindView(R.id.tv_full_name)
    TextView tvFullName;

    @BindView(R.id.tv_gender)
    TextView tvGender;

    @BindView(R.id.iv_audio_full_name)
    ImageView ivAudioFullName;

    @BindView(R.id.iv_audio_gender)
    ImageView ivAudioGender;

    @BindView(R.id.tv_mr_number)
    TextView tvMrNumber;

    @BindView(R.id.et_mr_number)
    EditText etMrNumber;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Inject
    DataManager dataManager;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private String gender;
    private String identityType = "";

    private String[] identityList;
    private String[] identityListEnglish;

    private String[] houses;
    private String[] houseHeads;
    private String[] houseIds;

    private TableDemographics tableDemographics;

    private TableHouse tableHouse;

    private AlertDialog expireDialog;

    private boolean isSearchRegistrationId;

    private ItemFileAttachment itemFaceImage;

    private long lastClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);
        initToolbar(toolbar, AssetReader.getLangKeyword("ncd_screening"), false);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();
        initAudio();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        ((TextView) (header.findViewById(R.id.tv_logged_in_user))).setText(dataManager.getLoggedInUser().getFullName());

        if (dataManager.getLoggedInUser().isDemoUser()) {
            tvIsDemoUser.setVisibility(View.VISIBLE);
        }

        if (!SHOW_SCAN_QR) {
            tvScanQr.setVisibility(View.GONE);
            ivScanQr.setVisibility(View.GONE);
        }

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgGender.indexOfChild(rgGender.findViewById(indexId));
                gender = getEnglishIndex(KEY_GENDER_TYPES, selectedIndex);
            }
        });

        identityList        = AssetReader.getSpinnerArray(KEY_IDENTITY_TYPES);
        identityListEnglish = AssetReader.getSpinnerArrayEnglish(KEY_IDENTITY_TYPES);

        spIdentityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onIdentitySelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        LView.setSpinnerAdapter(this, spIdentityType, identityList);

        tableDemographics = new TableDemographics();

        if (null != dataManager.getCampInfo()) {
            if (dataManager.getDeviceNumber().equals("000") && dataManager.getCampInfo().getCampName().equals("")) {
                LToast.errorLong(AssetReader.getLangKeyword("warning_enter_device_number_camp_info"));

            } else if (dataManager.getDeviceNumber().equals("000")) {
                LToast.errorLong(AssetReader.getLangKeyword("warning_enter_device_number"));

            } else if (dataManager.getCampInfo().getCampName().equals("")) {
                LToast.errorLong(AssetReader.getLangKeyword("warning_enter_enter_camp_info"));
            }

        } else {
            LToast.errorLong(AssetReader.getLangKeyword("warning_enter_enter_camp_info"));
        }

        if (AssetReader.toShow(KEY_SHOW_STOCK_OPTION)) {
            navigationView.getMenu().findItem(R.id.nav_stock).setVisible(true);
        }

        initNavigationMenu(navigationView.getMenu());

        setDefaultIdentity();

        if (AssetReader.toShow(KEY_SHOW_CAPTURE_FACE)) {
            ivCaptureFace.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestLocationWithPermissionCheck();
        checkIfDataNeedsTobeSynced();
    }

    private void checkIfDataNeedsTobeSynced() {

        if (dataManager.getAllPatients(false).size() >= AssetReader.getIntKey(KEY_EXPIRED_POPUP_DEMOGRAPHICS_COUNT)
                || dataManager.getAllVitals(false).size() >= AssetReader.getIntKey(KEY_EXPIRED_POPUP_VITALS_COUNT)) {

            if (null == expireDialog || !expireDialog.isShowing()) {
                showExpiredDialog();
            }

        } else if (dataManager.getAllPatients(false).size() >= AssetReader.getIntKey(KEY_EXPIRED_DEMOGRAPHICS_COUNT)
                || dataManager.getAllVitals(false).size() >= AssetReader.getIntKey(KEY_EXPIRED_VITALS_COUNT)) {
            LToast.errorLong(AssetReader.getLangKeyword("warning_login_credentials_expiring_soon"));
            syncDataBackground(dataManager, this);

        } else {
            syncDataBackground(dataManager, this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (null != StaticData.getQrResult()) {
            displayAadhaarDetails();
            StaticData.setQrResult(null);
        }

        if (null != StaticData.getQrSearchResult()) {
            displaySearchQrResult();
            StaticData.setQrSearchResult(null);
        }

        if (StaticData.isScannedHouseSaved()) {
            StaticData.setScannedHouseSaved(false);

            tableHouse = null;
            refreshHouseList();

            try {
                etHouse.setText(houses[0]);
                tableDemographics.setHouseId(houseIds[0]);
                showHouseMembers(false);

            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }

        refreshHouseList();
    }

    private void displaySearchQrResult() {
        etRegId.setText(StaticData.getQrSearchResult());
    }

    private void displayAadhaarDetails() {
        String xml = StaticData.getQrResult();

        try {
            InputStream            is  = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder        db  = dbf.newDocumentBuilder();
            Document               doc = db.parse(new InputSource(is));
            doc.getDocumentElement().normalize();
            NodeList nodelist = doc.getElementsByTagName("PrintLetterBarcodeData");

            NamedNodeMap aadhaarAttributes = nodelist.item(0).getAttributes();

            StaticData.setAadhaarAttributes(aadhaarAttributes);

            LView.setTextFromXml(etFullName, "name");

            try {
                tableDemographics.setCareOf(StaticData.getAadhaarAttributes().getNamedItem("co").getTextContent());
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }

            try {
                int yob = Integer.parseInt(aadhaarAttributes.getNamedItem("yob").getTextContent());
                etAge.setText("" + calculateAge(yob));
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }

            LView.setTextFromXml(etDob, "dob");

            if (aadhaarAttributes.getNamedItem("gender").getTextContent().equals("M")) {
                rgGender.check(R.id.rb_male);
            } else {
                rgGender.check(R.id.rb_female);
            }

            LView.setTextFromXml(etIdentityNumber, "uid");

            int spinnerIndex = getSpinnerIndexOther("Aadhaar", identityListEnglish);
            if (-1 != spinnerIndex) {
                spIdentityType.setSelection(spinnerIndex);
            }

            spIdentityType.setEnabled(false);
            etIdentityTypeOther.setEnabled(false);
            etIdentityNumber.setEnabled(false);

            tableDemographics.setQrScanned(true);

            if (null != dataManager.getCampInfo()) {
                if (!dataManager.getCampInfo().isDoorToDoor()) {
                    saveHouseInfo();
                }
            } else if (!AssetReader.isDoorToDoor()) {
                saveHouseInfo();
            }

        } catch (Exception e) {
            LToast.warning(AssetReader.getLangKeyword("warning_invalid_aadhaar_details"));
            LLog.printStackTrace(e);
        }
    }

    @OnClick(R.id.et_house)
    void selectHouse() {
        new DialogSearch(this, new DialogSearchCallbacks() {
            @Override
            public void onSelected(String house, int position) {
                etHouse.setText(house);

                tableDemographics.setHouseId(houseIds[position]);
                showHouseMembers(false);

            }
        }).withItems(houses, houseHeads).showDialog();
    }

    @OnClick(R.id.tv_view_house_members)
    void viewHouseMembers() {
        startActivity(new Intent(this, ActivityHouseMembers.class));
    }

    private void onIdentitySelected(int position) {
        if (position > 0) {
            if (hasValue(identityType) && identityType.equals(identityListEnglish[position])) {
                etIdentityNumber.setText("");
            }

            identityType = identityListEnglish[position];

            etIdentityNumber.setVisibility(View.VISIBLE);
            etIdentityNumber.setEnabled(true);

            if (identityType.equals("Other")) {
                etIdentityTypeOther.setVisibility(View.VISIBLE);

            } else {
                etIdentityTypeOther.setText("");
                etIdentityTypeOther.setVisibility(View.GONE);

                if (identityType.equals("None")) {
                    etIdentityNumber.setVisibility(View.GONE);
                    etIdentityNumber.setText("");

                } else if (identityType.equals("Aadhaar")) {
                    if (!AssetReader.toShow(KEY_ACCEPT_AADHAAR_NUMBER)) {
                        etIdentityNumber.setText("");
                        etIdentityNumber.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    @OnClick(R.id.iv_capture_face)
    void onAttachFileClicked() {

        if (lastClicked > System.currentTimeMillis() - 2000) {
            return;
        }

        if (null == itemFaceImage) {
            openCameraFace();

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // TODO: 24-Aug-19, lang translation
            String[] optionsArray = {"View", "Capture new", "Remove"};

            builder.setItems(optionsArray, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            Intent intent = new Intent(ActivityUserHome.this, ActivityDisplayImageDark.class);
                            intent.putExtra("url", itemFaceImage.getFilePath());
                            startActivity(intent);
                            break;
                        }

                        case 1: {
                            deleteCapture();
                            openCameraFace();
                            break;
                        }

                        case 2: {
                            deleteCapture();
                            break;
                        }
                    }
                }
            });
            builder.show();
        }

        lastClicked = System.currentTimeMillis();
    }

    private void openCameraFace() {
        EventOpenPicker event = new EventOpenPicker();
        event.setCamera(true);
        EventBus.getDefault().post(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(EventFilesRenamed event) {
        try {
            itemFaceImage = event.getOutputFilePaths().get(0);
            showCapturedImage();
        } catch (Exception e) {
        }
    }

    @OnClick(R.id.tv_clear_fields)
    void confirmClear() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_clear"));
        builder.setPositiveButton(AssetReader.getLangKeyword("clear"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                clearFields();
                LToast.normal(AssetReader.getLangKeyword("fields_cleared"));
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            confirmExit();
        }
    }

    private void confirmExit() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_exit"));
        builder.setPositiveButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityUserHome.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void showExpiredDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("error_message_expired"));

        builder.setPositiveButton(AssetReader.getLangKeyword("retry"), null);
        builder.setNegativeButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ActivityUserHome.super.onBackPressed();
            }
        });

        builder.setNeutralButton(AssetReader.getLangKeyword("logout"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                logout();
            }
        });

        builder.setCancelable(false);

        expireDialog = builder.create();

        expireDialog.show();

        expireDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (dataManager.getBooleanPref(KEY_ALLOW_SYNC, false)) {
                        syncDataForeground(dataManager, ActivityUserHome.this);
                    } else {
                        confirmSyncData();
                    }
                } else {
                    LToast.errorLong(AssetReader.getLangKeyword("no_internet"));
                }
            }
        });
    }

    private void confirmSyncData() {
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_user_password, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("enter_password"));

        builder.setView(dialogView);

        final EditText etPassword = (EditText) dialogView.findViewById(R.id.et_password);

        builder.setPositiveButton(AssetReader.getLangKeyword("sync_now"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TableUser tableUser = dataManager.getLoggedInUser();

                if (tableUser.getPassword().equals(etPassword.getText().toString())) {
                    syncDataForeground(dataManager, ActivityUserHome.this);
                    noteDialog.dismiss();
                } else {
                    LToast.warningLong(AssetReader.getLangKeyword("wrong_password"));
                }
            }
        });
    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_patient, menu);

        if (!AssetReader.toShow(KEY_SHOW_ADD_VITAL_IN_HOME)
                && !dataManager.getLoggedInUser().isDemoUser()) {
            menu.getItem(R.id.action_add_vital).setVisible(false);
        }

        initLanguageMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_houses:
                startActivity(new Intent(this, ActivityHouseList.class));
                return true;

            case R.id.action_view_patients:
                startActivity(new Intent(this, ActivityPatientsList.class));
                return true;

            case R.id.action_add_vital:
                StaticData.setRegistrationId(null);
                Intent intent = new Intent(this, ActivitySelectVital.class);
                intent.putExtra("is_new", true);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_sync:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (dataManager.getBooleanPref(KEY_ALLOW_SYNC, false)) {
                            syncDataForeground(dataManager, ActivityUserHome.this);
                        } else {
                            confirmSyncData();
                        }
                    }
                }, 220);
                break;

            case R.id.nav_my_account:
                startActivityDelayed(ActivityMyAccount.class);
                break;

            case R.id.nav_change_language:
                String currentSelectedLanguage = dataManager.getStringPref("selected_language", "English");
                changeLanguage(currentSelectedLanguage);
                break;

            case R.id.nav_statistics:
                startActivityDelayed(ActivityStatistics.class);
                break;

            case R.id.nav_stock:
                startActivityDelayed(ActivityStock.class);
                break;

            case R.id.nav_help:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AssetReader.openHelp(ActivityUserHome.this);
                    }
                }, 150);

                break;

            case R.id.nav_about:
                startActivityDelayed(ActivityAbout.class);
                break;

            case R.id.nav_logout:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        confirmLogout();
                    }
                }, 150);
        }

        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void startActivityDelayed(final Class className) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(ActivityUserHome.this, className));
            }
        }, 220);
    }

    private void confirmLogout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_logout"));
        builder.setPositiveButton(AssetReader.getLangKeyword("logout_lang"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel_lang"), null);
        builder.show();
    }

    private void logout() {
        dataManager.logout();
        Intent intent = new Intent(ActivityUserHome.this, ActivityLogin.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.btn_save)
    void savePatient() {
        readValues();

        if (isAllFieldsFilled()) {

            if (dataManager.isIdentityExists(tableDemographics) && !AssetReader.toShow(KEY_ALLOW_DUPLICATE_IDENTITY_DETAILS)) {
                LToast.errorLong(AssetReader.getLangKeyword("warning_identity_exists"));

            } else if (dataManager.isSimilarDemographicExists(tableDemographics)) {
                if (AssetReader.toShow(KEY_ALLOW_DUPLICATE_DEMOGRAPHICS)) {
                    confirmSave(true);
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(AssetReader.getLangKeyword("similar_demographic_exists"));
                    builder.setPositiveButton(AssetReader.getLangKeyword("okay"), null);
                    builder.show();
                }
            } else {
                confirmSave(false);
            }
        }
    }

    private void confirmSave(boolean similarDemographicExists) {

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);

        if (similarDemographicExists) {
            builder.setMessage(AssetReader.getLangKeyword("confirm_save_similar"));
        } else {
            builder.setMessage(AssetReader.getLangKeyword("confirm_save"));
        }

        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                tableDemographics.setGpsLocation(getLocation());

                if (null != tableHouse) {
                    dataManager.createHouse(tableHouse, null);
                    tableHouse = null;
                    refreshHouseList();
                    tableDemographics.setHouseId(houseIds[0]);
                }

                String registrationId = dataManager.createDemographics(tableDemographics);

                if (null != itemFaceImage && hasValue(itemFaceImage.getFileName())) {
                    String imgPrefix = dataManager.getPartnerId()
                            + "_"
                            + dataManager.getDeviceNumber()
                            + "_"
                            + registrationId
                            + "_";

                    String newFileName = itemFaceImage.getFileName();
                    newFileName = newFileName.replace("tmpfile_", imgPrefix);

                    File   tmpFile        = new File(itemFaceImage.getFilePath());
                    String outputFilePath = tmpFile.getParent() + "/" + newFileName;
                    tmpFile.renameTo(new File(outputFilePath));

                    TableImage tableImage = new TableImage();
                    tableImage.setImageType(getFaceImageType());
                    tableImage.setRegistrationId(registrationId);
                    tableImage.setImageName(newFileName);
                    tableImage.setGpsLocation(getLocation());

                    tableImage.setImageDateTime(LUtils.getCurrentDateTime());
                    tableImage.setImageDateTimeMillis(LUtils.getCurrentDateTimeMillis());
                    tableImage.setImageCamera(true);
                    dataManager.createImage(tableImage);
                }

                LToast.success(AssetReader.getLangKeyword("profile_created"));

                StaticData.setRegistrationId(registrationId);

                if (dataManager.getBooleanPref(KEY_PRINT_ID, false)) {
                    showPrintDialog(registrationId, etFullName.getText().toString());
                } else {
                    startActivity(new Intent(ActivityUserHome.this, ActivityPatientDetails.class));
                }

                clearFields();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void showPrintDialog(final String registrationId, final String name) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("registration_id") + " : " + registrationId);
        builder.setPositiveButton(AssetReader.getLangKeyword("print"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new PrinterManager().printRegId(dataManager, registrationId, name);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        StaticData.setAadhaarAttributes(null);
        MedcheckWeightConnection.clearInstance();
        clearFields();
    }

    @OnClick(R.id.iv_scan_qr)
    void scanQr() {
        isSearchRegistrationId = false;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            openCamera();
        }
    }

    @OnClick(R.id.iv_search_qr)
    void scanQrRegId() {
        isSearchRegistrationId = true;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            openCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openCamera();
                } else {
                    Toast.makeText(this, AssetReader.getLangKeyword("allow_camera_permissions"), Toast.LENGTH_SHORT).show();
                }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void openCamera() {
        Intent intent = new Intent(this, ActivityQr.class);
        intent.putExtra("is_search_registration_id", isSearchRegistrationId);
        startActivity(intent);
    }

    @OnClick(R.id.iv_add_household)
    void addHousehold() {
        Intent intent = new Intent(this, ActivityEditHouse.class);

        if (null != tableHouse) {
            intent.putExtra("edit", true);
            intent.putExtra("is_scanned_edit", true);
            StaticData.setCurrentHouse(tableHouse);

        } else if (!tableDemographics.getHouseId().equals("")) {
            intent.putExtra("edit", true);
            TableHouse tablehouse = dataManager.getHouse(tableDemographics.getHouseId());
            StaticData.setCurrentHouse(tablehouse);
        }

        startActivity(intent);
    }

    @OnClick(R.id.btn_search_patient)
    void searchPatient() {
        if (etRegId.getText().toString().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_reg_id"));

        } else if (null == dataManager.searchPatient(etRegId.getText().toString().toUpperCase())) {
//            AlertDialog.Builder builder;
//            builder = new AlertDialog.Builder(this);
//            builder.setMessage(AssetReader.getLangKeyword("demographics_not_found"));
//            builder.setPositiveButton(AssetReader.getLangKeyword("continue"), new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    StaticData.setRegistrationId(etRegId.getText().toString());
//                    startActivity(new Intent(ActivityUserHome.this, ActivityPatientDetails.class));
//                }
//            });
//            builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
//            builder.show();
            LToast.warning(AssetReader.getLangKeyword("demographics_not_found"));

        } else {
            StaticData.setRegistrationId(etRegId.getText().toString());
            startActivity(new Intent(this, ActivityPatientDetails.class));
        }
    }

    @OnClick(R.id.et_dob)
    void onDobClicked() {
        final Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        onDobSet(dayOfMonth, monthOfYear, year);
                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "DatePickerDialogDob");
    }

    private void onDobSet(int dayOfMonth, int monthOfYear, int year) {
        if (LUtils.isFutureDate(dayOfMonth, monthOfYear, year)) {
            LToast.warning(AssetReader.getLangKeyword("cant_select_future_date"));
            return;
        }

        etDob.setText(getFormattedDate(dayOfMonth, monthOfYear, year));
        etAge.setText("" + calculateAge(year));
        etAge.setEnabled(false);
    }

    private boolean isAllFieldsFilled() {
        if (tableDemographics.getFullName().equals("")) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_name"));
            return false;
        }

        if (null == tableDemographics.getGender()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_gender"));
            return false;
        }

        if (tableDemographics.getDob().equals("") && tableDemographics.getYob() <= 0) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_dob"));
            return false;
        }

        if (!tableDemographics.getPhoneNumber().equals("")) {

            if (AssetReader.getMinPhoneLength() > tableDemographics.getPhoneNumber().length()
                    || AssetReader.getMaxPhoneLength() < tableDemographics.getPhoneNumber().length()
                    || !Patterns.PHONE.matcher(tableDemographics.getPhoneNumber()).matches()) {
                LToast.warning(AssetReader.getLangKeyword("warning_enter_phone"));
                return false;
            }
        }

        if (!tableDemographics.getEmailId().equals("") && !Patterns.EMAIL_ADDRESS.matcher(tableDemographics.getEmailId()).matches()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_email"));
            return false;
        }

        if (identityType.isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_identity_type"));
            return false;
        }

        if (identityType.equals("Other")
                && etIdentityTypeOther.getText().toString().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_identity_type"));
            return false;
        }

        boolean toCheckIdentityNumber = true;

        if (identityType.equals("None")) {
            toCheckIdentityNumber = false;

        } else if (identityType.equals("Aadhaar") && !AssetReader.toShow(KEY_ACCEPT_AADHAAR_NUMBER)) {
            toCheckIdentityNumber = false;
        }

        if (toCheckIdentityNumber) {

            if (etIdentityNumber.getText().toString().isEmpty()) {
                LToast.warning(AssetReader.getLangKeyword("warning_enter_identity_number"));
                return false;
            }

            if (!etIdentityNumber.getText().toString().matches("[A-Za-z0-9]+")) {
                LToast.warningLong(AssetReader.getLangKeyword("warning_enter_valid_identity_number"));
                return false;
            }
        }

        if (etHouse.getText().

                toString().

                isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_house_info"));
            return false;
        }

        return true;
    }

    private void readValues() {
        tableDemographics.setFullName(etFullName.getText().toString().trim());

        try {
            int age = Integer.parseInt(etAge.getText().toString());
            tableDemographics.setYob(calculateYob(age));

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (!etDob.getText().toString().isEmpty()) {
            tableDemographics.setDob(etDob.getText().toString());
        } else {
            // Save jan 1 as dob
//            tableDemographics.setDob("01/01/" + tableDemographics.getYob());
        }

        tableDemographics.setGender(gender);
        tableDemographics.setPhoneNumber(etPhone.getText().toString());
        tableDemographics.setEmailId(etEmailId.getText().toString());

        if (null != identityType && identityType.equals("Other")) {
            tableDemographics.setIdentityType(etIdentityTypeOther.getText().toString());
        } else {
            tableDemographics.setIdentityType(identityType);
        }

        tableDemographics.setIdentityNumber(etIdentityNumber.getText().toString().trim().toUpperCase());
        tableDemographics.setNotes(etNotes.getText().toString().trim());
        tableDemographics.setMrNumber(etMrNumber.getText().toString().trim());
    }

    private void clearFields() {
        etFullName.setText("");
        etDob.setText("");
        etAge.setText("");
        rgGender.clearCheck();
        etPhone.setText("");
        etEmailId.setText("");
        etIdentityNumber.setText("");
        etHouse.setText("");
        etMrNumber.setText("");
        etNotes.setText("");
        spIdentityType.setEnabled(true);
        etIdentityTypeOther.setEnabled(true);
        etIdentityNumber.setEnabled(true);

        LView.setSpinnerAdapter(this, spIdentityType, identityList);

//        ivScanQr.setVisibility(View.GONE);

        etIdentityTypeOther.setVisibility(View.GONE);
        etIdentityNumber.setVisibility(View.GONE);
        identityType = "";
        gender       = null;

        etAge.setEnabled(true);
        etFullName.requestFocus();

        StaticData.setAadhaarAttributes(null);
        tableDemographics = new TableDemographics();
        tvHouseMembers.setVisibility(View.GONE);

        ivAddHouse.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_note_add_black_24dp));

        tableHouse = null;
        deleteCapture();

        setDefaultIdentity();
    }

    private void setDefaultIdentity() {
        String defaultIdentity = getStringKey("default_identity_type");

        int position = 0;

        for (int i = 0; i < identityListEnglish.length; i++) {
            if (identityListEnglish[i].equals(defaultIdentity)) {
                position = i;
                break;
            }
        }

        spIdentityType.setSelection(position);
    }

    private void deleteCapture() {
        if (null != itemFaceImage) {
            if (itemFaceImage.getFileName().contains("tmpfile_")) {
                new File(itemFaceImage.getFilePath()).delete();
            }
        }
        itemFaceImage = null;
        ivCaptureFace.setImageResource(R.drawable.capture_face);
    }

    private void showCapturedImage() {
        Glide.with(this)
                .load(itemFaceImage.getFilePath())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(ivCaptureFace);
    }

    private void saveHouseInfo() {
        // Change the add house icon to edit
        ivAddHouse.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_create_black_24dp));

        tableHouse = new TableHouse();
        tableHouse.setHouseNo(getTextFromXml("house"));
        tableHouse.setStreet(getTextFromXml("street"));
        tableHouse.setPost(getTextFromXml("po") + ", " + getTextFromXml("vtc"));
        tableHouse.setLocality(getTextFromXml("loc"));
        tableHouse.setDistrict(getTextFromXml("dist"));
        tableHouse.setState(getTextFromXml("state"));

        try {
            tableHouse.setPinCode(Integer.parseInt(getTextFromXml("pc")));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        tableHouse.setGpsLocation(getLocation());
        tableHouse.setCountry(AssetReader.getDefaultCountry());

        refreshHouseList();

        try {
            etHouse.setText(houses[0]);
            tableDemographics.setHouseId(houseIds[0]);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

//        showHouseMembers(true);
    }

    private void showHouseMembers(boolean isScanned) {

        tvHouseMembers.setVisibility(View.GONE);

        StaticData.currentSelectedHouse   = null;
        StaticData.currentSelectedHouseId = null;

        if (isScanned) {
            if (null != tableHouse) {
                int count = dataManager.getHouseMembersCount(tableHouse);

                if (0 < count) {
                    tvHouseMembers.setVisibility(View.VISIBLE);
                    tvHouseMembers.setText(Html.fromHtml("<u>" + AssetReader.getLangKeyword("view_members") + " (" + count + ")</u>"));
                    StaticData.currentSelectedHouse = tableHouse;
                }
            }
        } else {
            if (!tableDemographics.getHouseId().isEmpty()) {
                int count = dataManager.getHouseMembersCount(tableDemographics.getHouseId());

                if (0 < count) {
                    tvHouseMembers.setVisibility(View.VISIBLE);
                    tvHouseMembers.setText(Html.fromHtml("<u>" + AssetReader.getLangKeyword("view_members") + "(" + count + ")</u>"));

                    StaticData.currentSelectedHouseId = tableDemographics.getHouseId();
                }
            }
        }
    }

    private void refreshHouseList() {
        List<TableHouse> houses = dataManager.getAllActiveHouses();

        if (null != tableHouse) {
            houses.add(0, tableHouse);
        }

        this.houses     = new String[houses.size()];
        this.houseHeads = new String[houses.size()];

        houseIds = new String[houses.size()];

        for (int i = 0; i < houses.size(); i++) {
            this.houses[i] = houses.get(i).getHouseNo()
                    + " " + houses.get(i).getStreet()
                    + " " + houses.get(i).getPost()
                    + " " + houses.get(i).getLocality()
                    + " " + houses.get(i).getDistrict()
                    + " " + houses.get(i).getState();

            houseIds[i] = houses.get(i).getHouseId();

            this.houseHeads[i] = houses.get(i).getHeadOfHouse();
        }
    }

    @Override
    public void onSyncStarted() {
        jsonSyncInProgress = true;
    }

    @Override
    public void onSyncComplete() {
        if (null != expireDialog && expireDialog.isShowing()) {
            expireDialog.dismiss();
        }
        jsonSyncInProgress = false;
    }

    @Override
    public void onSyncFailed(String message) {
        if (hasValue(message)) {
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(message);
            builder.setPositiveButton(AssetReader.getLangKeyword("okay"), null);
            builder.show();
        }
        jsonSyncInProgress = false;
    }

    protected void onLanguageSet(String selectedLanguage) {
        String currentSelectedLanguage = dataManager.getStringPref("selected_language", "English");

        if (!currentSelectedLanguage.equals(selectedLanguage)) {

            if (AssetReader.initSelectedLanguage(selectedLanguage)) {
                dataManager.setPref("selected_language", selectedLanguage);

            } else {
                dataManager.setPref("selected_language", "English");
                LToast.error(R.string.unable_to_set_language);
            }
        }
        recreate();
    }

    private void initLanguage() {
        String userType = dataManager.getLoggedInUser().getUserType();

        tvIsDemoUser.setText(AssetReader.getLangKeyword("demo")
                + " "
                + getSelectedLanguageVersion(userType, KEY_USER_TYPES));

        etRegId.setHint(AssetReader.getLangKeyword("registration_id"));
        btnSearch.setText(AssetReader.getLangKeyword("search"));
        tvNewProfile.setText(AssetReader.getLangKeyword("new_profile"));

        tvClear.setText(Html.fromHtml("<u>" + AssetReader.getLangKeyword("clear") + "</u>"));

        tvScanQr.setText(AssetReader.getLangKeyword("scan_qr"));

        tvFullName.setText(AssetReader.getLangKeyword("full_name") + " *");
        etFullName.setHint(AssetReader.getLangKeyword("enter_full_name"));
        tvGender.setText(AssetReader.getLangKeyword("gender") + " *");
        rbMale.setText(AssetReader.getLangKeyword("male"));
        rFemale.setText(AssetReader.getLangKeyword("female"));
        rbOther.setText(AssetReader.getLangKeyword("other"));
        tvMrNumber.setText(AssetReader.getLangKeyword("mr_number"));

        tvDobTitle.setText(AssetReader.getLangKeyword("date_of_birth_title") + " *");
        tvOr.setText(AssetReader.getLangKeyword("or"));
        tvYears.setText(AssetReader.getLangKeyword("years"));
        etDob.setHint(AssetReader.getLangKeyword("date_of_birth"));
        etAge.setHint(AssetReader.getLangKeyword("age"));

        tvIdentity.setText(AssetReader.getLangKeyword("identity") + " *");
        etIdentityTypeOther.setHint(AssetReader.getLangKeyword("enter_identity_type"));
        etIdentityNumber.setHint(AssetReader.getLangKeyword("identity_number"));

        tvPhone.setText(AssetReader.getLangKeyword("phone_number"));
        etPhone.setHint(AssetReader.getLangKeyword("enter_phone_number"));
        tvEmailId.setText(AssetReader.getLangKeyword("email_id"));
        etEmailId.setHint(AssetReader.getLangKeyword("enter_email_id"));
        tvHouse.setText(AssetReader.getLangKeyword("house_title") + " *");
        etHouse.setHint(AssetReader.getLangKeyword("select_house"));
        tvNotes.setText(AssetReader.getLangKeyword("notes_title"));
        etNotes.setHint(AssetReader.getLangKeyword("enter"));
        etMrNumber.setHint(AssetReader.getLangKeyword("enter"));

        btnSave.setText(AssetReader.getLangKeyword("save"));

        etPhone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(AssetReader.getMaxPhoneLength())});
    }

    private void initAudio() {
        if (AssetReader.toShowAudio()) {
            if (hasValue(AssetReader.getAudioFileName("full_name"))) {
                ivAudioFullName.setVisibility(View.VISIBLE);
            }
            if (hasValue(AssetReader.getAudioFileName("gender"))) {
                ivAudioGender.setVisibility(View.VISIBLE);
            }
        }
    }

    @OnClick({R.id.iv_audio_full_name, R.id.iv_audio_gender})
    void onAudioClicked(View view) {

        switch (view.getId()) {

            case R.id.iv_audio_full_name: {
                playAudio(AssetReader.getLangAudio("full_name"), ivAudioFullName);
                break;
            }

            case R.id.iv_audio_gender: {
                playAudio(AssetReader.getLangAudio("gender"), ivAudioGender);
                break;
            }
        }
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_view_houses);
        item.setTitle(AssetReader.getLangKeyword("houses"));

        item = menu.findItem(R.id.action_view_patients);
        item.setTitle(AssetReader.getLangKeyword("existing_profiles"));

        item = menu.findItem(R.id.action_add_vital);
        item.setTitle(AssetReader.getLangKeyword("vitals"));
    }

    private void initNavigationMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.nav_statistics);
        item.setTitle(AssetReader.getLangKeyword("statistics"));

        item = menu.findItem(R.id.nav_stock);
        item.setTitle(AssetReader.getLangKeyword("stock"));

        item = menu.findItem(R.id.nav_my_account);
        item.setTitle(AssetReader.getLangKeyword("my_account"));

        item = menu.findItem(R.id.nav_change_language);
        item.setTitle(AssetReader.getLangKeyword("change_language"));

        item = menu.findItem(R.id.nav_sync);
        item.setTitle(AssetReader.getLangKeyword("sync"));

        item = menu.findItem(R.id.nav_help);
        item.setTitle(AssetReader.getLangKeyword("help"));

        item = menu.findItem(R.id.nav_about);
        item.setTitle(AssetReader.getLangKeyword("about"));

        item = menu.findItem(R.id.nav_logout);
        item.setTitle(AssetReader.getLangKeyword("logout_lang"));
    }
}