package com.dhp.screening.ui.admin;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterQuestionAnswer;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemQuestionAnswer;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_GENDER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_INACTIVE_REASON;
import static com.dhp.screening.data.asset.AssetReader.KEY_USER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.getSelectedLanguageVersion;


public class ActivityViewUser extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_user_details)
    RecyclerView rvUserDetails;

    @BindView(R.id.ll_user_details)
    LinearLayout llUserDetails;

    @Inject
    DataManager dataManager;

    private AdapterQuestionAnswer adapter;

    private TableUser tableUser;

    private String inactiveReason;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_user);

        initToolbar(toolbar, AssetReader.getLangKeyword("user"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        String username = getIntent().getStringExtra("username");

        tableUser = dataManager.getUser(username);

        adapter = new AdapterQuestionAnswer(this);

        ArrayList<ItemQuestionAnswer> questionAnswers = new ArrayList<>(16);

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("full_name"), tableUser.getFullName()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("username_title"), tableUser.getUserId()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("password_title"), replaceWithStars(tableUser.getPassword().length())));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("user_type"), getSelectedLanguageVersion(tableUser.getUserType(), KEY_USER_TYPES)));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("gender"), getSelectedLanguageVersion(tableUser.getGender(), KEY_GENDER_TYPES)));

        if (tableUser.getDob().isEmpty()) {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("year_of_birth"), "" + tableUser.getYob()));
        } else {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("date_of_birth") + " :", tableUser.getDob()));
        }

        if (0 < tableUser.getYob()) {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("age") + ":", "" + LUtils.calculateAge(tableUser.getYob())));
        }

//        questionAnswers.add(new ItemQuestionAnswer("Identity type:", tableUser.getIdentityType()));
//        questionAnswers.add(new ItemQuestionAnswer("Identity number:", tableUser.getIdentityNumber()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("phone_number"), tableUser.getPhoneNumber()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("email_id"), tableUser.getEmailId()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("house_number"), tableUser.getHouseNo()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("street"), tableUser.getStreet()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("post"), tableUser.getPost()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("locality"), tableUser.getLocality()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("district"), tableUser.getDistrict()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("state"), tableUser.getState()));

        if (0 < tableUser.getPinCode()) {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("pin_code"), "" + tableUser.getPinCode()));
        }

        String isPrescriptionAllowed = AssetReader.getLangKeyword("no");
        if (tableUser.isAllowPrescription()) {
            isPrescriptionAllowed = AssetReader.getLangKeyword("yes");
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("allow_prescription_entry") + ":", isPrescriptionAllowed));

        String isDemoUser = AssetReader.getLangKeyword("no");
        if (tableUser.isDemoUser()) {
            isDemoUser = AssetReader.getLangKeyword("yes");
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("is_demo_user") + ": ", isDemoUser));

        adapter.setItems(questionAnswers);

        rvUserDetails.setLayoutManager(new LinearLayoutManager(this));
        rvUserDetails.setAdapter(adapter);
    }

    private String replaceWithStars(int length) {
        StringBuilder stars = new StringBuilder();

        for (int i = 0; i < length; ++i) {
            stars.append("*");
        }

        return stars.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_user, menu);

        if (tableUser.isInactive()) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            llUserDetails.setBackground(ContextCompat
                    .getDrawable(this, R.drawable.rounded_border_inactive));
        } else {
            llUserDetails.setBackground(ContextCompat
                    .getDrawable(this, R.drawable.rounded_border));
        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_inactive:

                if (tableUser.isInactive()) {
                    confirmActive();
                } else {
                    confirmInactive();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(AssetReader.getLangKeyword("confirm_activate_user"));
        builder.setPositiveButton(AssetReader.getLangKeyword("activate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tableUser.setInactive(false);
                tableUser.setInactiveReason("");
                dataManager.updateUser(tableUser);

                invalidateOptionsMenu();
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        builder.show();
    }

    private void confirmInactive() {
        inactiveReason = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("inactive_reason"));
        builder.setView(dialogView);

        final String[] reasonList        = AssetReader.getSpinnerArray(KEY_INACTIVE_REASON);
        final String[] reasonListEnglish = AssetReader.getSpinnerArrayEnglish(KEY_INACTIVE_REASON);

        final Spinner spReason = (Spinner) dialogView.findViewById(R.id.sp_inactive_reason);
        LView.setSpinnerAdapter(this, spReason, reasonList);

        final EditText etReasonOther = (EditText) dialogView.findViewById(R.id.et_inactive_reason_other);
        etReasonOther.setHint(AssetReader.getLangKeyword("if_other"));

        spReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (0 < position) {
                    inactiveReason = reasonListEnglish[position];

                    if (inactiveReason.equals("Other")) {
                        etReasonOther.setVisibility(VISIBLE);
                    } else {
                        etReasonOther.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setPositiveButton(AssetReader.getLangKeyword("done"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (VISIBLE == etReasonOther.getVisibility()) {
                    inactiveReason = etReasonOther.getText().toString();
                }

                if (inactiveReason.isEmpty()) {
                    LToast.warning(AssetReader.getLangKeyword("warning_enter_reason"));
                    return;
                }

                tableUser.setInactive(true);
                tableUser.setInactiveReason(inactiveReason);
                dataManager.updateUser(tableUser);

                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_inactive);
        item.setTitle(AssetReader.getLangKeyword("inactive"));
    }
}