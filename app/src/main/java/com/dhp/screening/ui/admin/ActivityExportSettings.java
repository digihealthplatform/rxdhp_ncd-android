package com.dhp.screening.ui.admin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.util.LUtils;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.File;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.dhp.screening.data.manager.LDatabase.DATABASE_NAME;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEVICE_NUMBER;


public class ActivityExportSettings extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_export_settings);

        initToolbar(toolbar, "Settings", true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_exit"));
        builder.setPositiveButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityExportSettings.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin4, menu);

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_logout:
                confirmLogout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_export)
    void onExportClicked() {
        try {
            File outputPath = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_backup_path));

            if (!outputPath.exists()) {
                outputPath.mkdirs();
            }

            File outputFolder = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.export_backup_folder));

            if (!outputFolder.exists()) {
                outputFolder.mkdirs();
            }

            Calendar calendar = Calendar.getInstance();

            if (outputFolder.canWrite()) {
                String backupDBPath = getString(R.string.prefix_export_backup)
                        + "_" + dataManager.getPartnerId()
                        + "_" + dataManager.getDeviceNumber()
                        + "_"
                        + LUtils.getFileName(calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DATE),
                        calendar.get(Calendar.HOUR_OF_DAY),
                        calendar.get(Calendar.MINUTE),
                        calendar.get(Calendar.SECOND))
                        + ".db";

                File backupDB  = new File(outputFolder, backupDBPath);

                File currentDB;

                if (Build.VERSION.SDK_INT >= 28) {
                    currentDB = new File(StaticData.getDatabasePath());
                } else {
                    currentDB = FlowManager.getContext().getDatabasePath(DATABASE_NAME + ".db");
                }

                LUtils.copy(currentDB, backupDB);

                Toasty.success(this, "Exported successfully", Toast.LENGTH_LONG).show();

                Uri    path        = Uri.fromFile(backupDB);
                Intent emailIntent = new Intent(Intent.ACTION_SEND);
                emailIntent.setType("text/plain");

                emailIntent.putExtra(Intent.EXTRA_STREAM, path);

                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Survey Data:");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
            }
        } catch (Exception e) {
            Toasty.error(this, "Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
        }
    }

    private void confirmLogout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_logout"));
        builder.setPositiveButton(AssetReader.getLangKeyword("logout"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.logout();
                Intent intent = new Intent(ActivityExportSettings.this, ActivityLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setTitle(AssetReader.getLangKeyword("logout"));
    }
}