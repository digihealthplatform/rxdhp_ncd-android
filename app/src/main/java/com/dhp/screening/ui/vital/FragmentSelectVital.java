package com.dhp.screening.ui.vital;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dhp.screening.R;
import com.dhp.screening.data.adapter.AdapterSelectVitals;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.range.ItemVital;
import com.dhp.screening.ui.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


@SuppressLint("ValidFragment")
public class FragmentSelectVital extends BaseFragment {

    @BindView(R.id.rv_vitals)
    RecyclerView rvVitals;

    private int     position;
    private boolean isAllowPrescription;

    private ArrayList<ItemVital> vitalsList = new ArrayList<>();

    public static FragmentSelectVital newInstance(int position, boolean isAllowPrescription) {
        FragmentSelectVital fragment = new FragmentSelectVital();

        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putBoolean("allow_prescription", isAllowPrescription);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_vital, container, false);
        ButterKnife.bind(this, view);

        position = getArguments().getInt("position");

        isAllowPrescription = getArguments().getBoolean("allow_prescription", false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ItemVital[] vitalTabs = AssetReader.getVitalTabs();

        if (null == vitalTabs || 0 == vitalTabs.length) {
            return;
        }

        for (ItemVital itemVital : vitalTabs) {
            if (itemVital.getShow().toLowerCase().equals("yes")) {
                vitalsList.add(itemVital);
            }
        }

        ItemVital itemVital = vitalsList.get(position);

        ArrayList<String> items         = new ArrayList<>();
        ArrayList<String> allowedVitals = AssetReader.getVitalsList(isAllowPrescription);

        for (int i = 0; i < itemVital.getItems().length; i++) {
            String vital = itemVital.getItems()[i];
            if (allowedVitals.contains(vital)) {
                items.add(vital);
            }
        }

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        rvVitals.setLayoutManager(gridLayoutManager);

        final AdapterSelectVitals adapter = new AdapterSelectVitals(getActivity(),
                items,
                AssetReader.getVitalIcons());

        rvVitals.setAdapter(adapter);
    }
}