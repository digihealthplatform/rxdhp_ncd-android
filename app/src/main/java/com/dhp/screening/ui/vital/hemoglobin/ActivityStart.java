package com.dhp.screening.ui.vital.hemoglobin;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LConstants;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_L_VALUES;
import static com.dhp.screening.util.LConstants.TYPE_CALIBRATE;

public class ActivityStart extends BaseActivity {

    public static final int REQUEST_CODE = 101;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_start_test)
    Button btnStart;

    @BindView(R.id.tv_warning)
    TextView tvWarning;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Hemoglobin", true);
        getActivityComponent(this).inject(this);

        createFoldersWithPermission();

        String[] values = dataManager.getStringPref(KEY_L_VALUES, "").split(";");

        if (values.length <= 5) {
            btnStart.setVisibility(View.GONE);
            tvWarning.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
        }
        return true;
    }

    @OnClick(R.id.btn_calibrate)
    void calibrate() {
        Intent intent = new Intent(this, ActivityCameraView.class);
        intent.putExtra("activity_type", TYPE_CALIBRATE);
        startActivity(intent);
    }

    @OnClick(R.id.btn_start_test)
    void startTest() {
        Intent intent = new Intent(this, ActivityTestStep1.class);
        startActivity(intent);
        finish();
    }

    private void createFoldersWithPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE);

        } else {
            createFolders();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String result = StaticData.getHbResult();

        if (null != result) {
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createFolders();
                } else {
                    Toast.makeText(this, "Please allow the permissions", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
            break;
        }
    }

    private void createFolders() {
        File folder = new File(Environment.getExternalStorageDirectory(), LConstants.HB_FOLDER_PATH);

        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}