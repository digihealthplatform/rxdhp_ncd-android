package com.dhp.screening.ui.vital;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemPrescriptionDetails;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.network.ViewLocationTask;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.vital.hemoglobin.ActivityEnterHemoglobin;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_INACTIVE_REASON;
import static com.dhp.screening.data.asset.AssetReader.SHOW_HB_LAB_VALUES;


public class ActivityVitalDetails extends BaseActivity {

    private boolean inactive;

    @BindView(R.id.ll_height)
    LinearLayout llHeight;

    @BindView(R.id.ll_weight)
    LinearLayout llWeight;

    @BindView(R.id.ll_bp)
    LinearLayout llBp;

    @BindView(R.id.ll_rbs)
    LinearLayout llRbs;

    @BindView(R.id.ll_hemoglobin)
    LinearLayout llHemoglobin;

    @BindView(R.id.ll_albumin)
    LinearLayout llAlbumin;

    @BindView(R.id.ll_blood_group)
    LinearLayout llBloodGroup;

    @BindView(R.id.ll_pulse)
    LinearLayout llPulse;

    @BindView(R.id.ll_prescription)
    LinearLayout llPrescription;

    @BindView(R.id.ll_test_result)
    LinearLayout llTestResult;

    @BindView(R.id.iv_test)
    ImageView ivTest;

    @BindView(R.id.tv_test_result)
    TextView tvTestResult;

    @BindView(R.id.tv_date_time)
    TextView tvDateTime;

    @BindView(R.id.tv_height)
    TextView tvHeight;

    @BindView(R.id.tv_weight)
    TextView tvWeight;

    @BindView(R.id.tv_bmi_weight)
    TextView tvBmiWeight;

    @BindView(R.id.tv_systolic)
    TextView tvSystolic;

    @BindView(R.id.tv_diastolic)
    TextView tvDiastolic;

    @BindView(R.id.tv_pulse)
    TextView tvPulse;

    @BindView(R.id.tv_hemoglobin)
    TextView tvHemoglobin;

    @BindView(R.id.tv_rbs)
    TextView tvRbs;

    @BindView(R.id.tv_albumin)
    TextView tvAlbumin;

    @BindView(R.id.tv_blood_group)
    TextView tvBloodGroup;

    @BindView(R.id.tv_spo2)
    TextView tvSpo2;

    @BindView(R.id.tv_prescription)
    TextView tvPrescription;

    @BindView(R.id.tv_prescription_timings)
    TextView tvPrescriptionTimings;

    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.ll_vital_details)
    LinearLayout llVitalDetails;

    @Inject
    DataManager dataManager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_edit)
    FloatingActionButton fabEdit;

    private TableVitals tableVitals;

    private String inactiveReason;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_details);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);
        tableVitals = StaticData.getCurrentVital();

        initToolbar(toolbar, tableVitals.getVitalType(), true);

        TableDemographics patient = dataManager.getPatient(StaticData.getCurrentVital().getRegistrationId());

        if (null != patient) {
            getSupportActionBar().setTitle(patient.getFullName());
        } else {
            getSupportActionBar().setTitle("Reg. ID: " + StaticData.getCurrentVital().getRegistrationId());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vital_details, menu);

        if (inactive) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            fabEdit.setVisibility(View.GONE);
            llVitalDetails.setBackground(ContextCompat
                    .getDrawable(ActivityVitalDetails.this, R.drawable.rounded_border_inactive));

        } else {
            if (tableVitals.isSynced()) {
                fabEdit.setVisibility(View.GONE);

            } else if (!tableVitals.getVitalType().equals("Prescription")) {
                fabEdit.setVisibility(View.VISIBLE);
            }

            llVitalDetails.setBackground(ContextCompat
                    .getDrawable(ActivityVitalDetails.this, R.drawable.rounded_border));
        }

        fabEdit.setVisibility(View.GONE);

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_inactive:
                if (inactive) {
                    confirmActive();
                } else {
                    confirmInactive();
                }
                break;

            case R.id.action_view_location:
                new ViewLocationTask(this, tableVitals.getGpsLocation()).execute();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(AssetReader.getLangKeyword("confirm_activate"));
        builder.setPositiveButton(AssetReader.getLangKeyword("activate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tableVitals.setInactive(false);
                tableVitals.setInactiveReason("");
                dataManager.updateVital(tableVitals, getDeviceId());

                inactive = false;
                invalidateOptionsMenu();
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        builder.show();
    }

    private void confirmInactive() {
        inactiveReason = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("inactive_reason"));
        builder.setView(dialogView);

        final String[] reasonList = AssetReader.getSpinnerArray(KEY_INACTIVE_REASON);

        final Spinner spReason = (Spinner) dialogView.findViewById(R.id.sp_inactive_reason);
        LView.setSpinnerAdapter(this, spReason, reasonList);

        final EditText etReasonOther = (EditText) dialogView.findViewById(R.id.et_inactive_reason_other);
        etReasonOther.setHint(AssetReader.getLangKeyword("if_other"));

        spReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (0 < position) {
                    inactiveReason = reasonList[position];

                    if (inactiveReason.equals("Other")) {
                        etReasonOther.setVisibility(VISIBLE);
                    } else {
                        etReasonOther.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setPositiveButton(AssetReader.getLangKeyword("done"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (VISIBLE == etReasonOther.getVisibility()) {
                    inactiveReason = etReasonOther.getText().toString();
                }

                if (inactiveReason.isEmpty()) {
                    LToast.warning("Please enter the reason");
                    return;
                }

                tableVitals.setInactive(true);
                tableVitals.setInactiveReason(inactiveReason);
                dataManager.updateVital(tableVitals, getDeviceId());

                inactive = true;
                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        tableVitals = StaticData.getCurrentVital();

        inactive = tableVitals.isInactive();

        tvDateTime.setText(LUtils.getFormattedDateTimeAm(tableVitals.getVitalDateTimeMillis()));

        switch (tableVitals.getVitalType()) {

            case "Height": {
                tvHeight.setText("Height: " + tableVitals.getVitalValues() + " " + tableVitals.getVitalUnits());
                llHeight.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_height), tableVitals.getVitalType());
            }
            break;

            case "Weight": {
                tvWeight.setText("Weight: " + tableVitals.getVitalValues() + " " + tableVitals.getVitalUnits());

                String latestHeight = dataManager.getLatestHeight(StaticData.getRegistrationId(), tableVitals.getVitalDateTimeMillis());

                if (null != latestHeight) {
                    String[] height = latestHeight.split(";");

                    tvBmiWeight.setText("BMI: " +
                            LUtils.findBmi(Float.parseFloat(height[0]),
                                    height[1],
                                    Float.parseFloat(tableVitals.getVitalValues()))
                            + " kg/m2 ");

                    tvBmiWeight.setVisibility(View.VISIBLE);
                }

                llWeight.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_weight), tableVitals.getVitalType());
            }
            break;

            case "Blood Pressure": {
                String values[] = tableVitals.getVitalValues().split(";");

                tvSystolic.setText("Systolic: " + values[0] + " mmHg");
                tvDiastolic.setText("Diastolic: " + values[1] + " mmHg");

                if (!values[2].equals("0")) {
                    tvPulse.setText("Pulse: " + values[2] + " bpm");
                }

                llBp.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_bp), tableVitals.getVitalType());
            }
            break;

            case "Blood Sugar": {
                tvRbs.setText(tableVitals.getVitalSubTypes() + ": " + tableVitals.getVitalValues() + " mg/dL");

                llRbs.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_rbs), tableVitals.getVitalType());
            }
            break;

            case "Hemoglobin": {
                tvHemoglobin.setText("Hemoglobin: " + tableVitals.getVitalValues() + " mg/dL");
                llHemoglobin.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_hemoglobin), tableVitals.getVitalType());
            }
            break;

            case "Albumin": {
                tvAlbumin.setText("Albumin: " + tableVitals.getVitalValues() + " g/dL");
                llAlbumin.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_albumin), tableVitals.getVitalType());
            }
            break;

            case "Blood Group": {
                tvBloodGroup.setText("Blood Group: " + tableVitals.getVitalValues());
                llBloodGroup.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_blood_group), tableVitals.getVitalType());
            }
            break;

            case "Malaria":
            case "Dengue":
            case "Syphilis":
            case "HIV-AIDS":
            case "Pregnancy":
            case "Heart Attack": {
                String text = tableVitals.getVitalType() + ": " + tableVitals.getVitalSubTypes()
                        + "\n" + tableVitals.getVitalValues();

                tvTestResult.setText(text);
                llTestResult.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_test), tableVitals.getVitalType());
            }
            break;

            case "Pulse": {

                String pi = tableVitals.getVitalValues().split(";")[2];
                String content = "SpO2: " + tableVitals.getVitalValues().split(";")[0] + " %"
                        + "\nPR: " + tableVitals.getVitalValues().split(";")[1] + " bpm";

                if (!pi.equals("0")) {
                    content += "\nPI: " + tableVitals.getVitalValues().split(";")[2] + " %";
                }

                tvSpo2.setText(content);

                llPulse.setVisibility(View.VISIBLE);
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_pulse), "Pulse Oximeter");
            }
            break;

            case "Prescription": {

                ItemPrescriptionDetails prescriptionDetails = LUtils.getPrescriptionDetails(tableVitals.getVitalValues());

                String timing = prescriptionDetails.getTimings();

                timing = timing.replaceAll(",", ", ");

                if (prescriptionDetails.getNew_or_existing().isEmpty()) {
                    tvPrescription.setText("Prescription: " + prescriptionDetails.getPrescription());

                } else {
                    tvPrescription.setText("Prescription (" + prescriptionDetails.getNew_or_existing() + "): " + prescriptionDetails.getPrescription());
                }

                llPrescription.setVisibility(View.VISIBLE);

                tvPrescriptionTimings.setText(timing + "\n" + prescriptionDetails.getFood() + "\n" + LUtils.getDaysText(prescriptionDetails.getDays()));
                LView.setIcon((ImageView) findViewById(R.id.iv_icon_prescription), tableVitals.getVitalType());

                if (!dataManager.isAllowPrescription()) {
                    fabEdit.setVisibility(View.GONE);
                }
            }
            break;
        }

        String notes = tableVitals.getNotes();

        if (tableVitals.getVitalType().equals("Hemoglobin")) {
            if (!AssetReader.toShow(SHOW_HB_LAB_VALUES)) {
                notes = notes.split("\\{")[0];
            }
        }

        if (!notes.isEmpty()) {
            tvNotes.setText("Notes: " + notes);
            tvNotes.setVisibility(VISIBLE);
        } else {
            tvNotes.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_edit)
    void editVital() {

        Intent intent = null;

        switch (tableVitals.getVitalType()) {

            case "Height": {
                intent = new Intent(this, ActivityEnterHeight.class);
            }
            break;

            case "Weight": {
                intent = new Intent(this, ActivityEnterWeight.class);
            }
            break;

            case "Blood Pressure": {
                intent = new Intent(this, ActivityEnterBp.class);
            }
            break;

            case "Blood Sugar": {
                intent = new Intent(this, ActivityEnterRbs.class);
            }
            break;

            case "Hemoglobin": {
                intent = new Intent(this, ActivityEnterHemoglobin.class);
            }
            break;

            case "Albumin": {
                intent = new Intent(this, ActivityEnterAlbumin.class);
            }
            break;

            case "Blood Group": {
                intent = new Intent(this, ActivityEnterBloodGroup.class);
            }
            break;

            case "Pulse": {
                intent = new Intent(this, ActivityEnterPulse.class);
            }
            break;

            case "Prescription": {
                intent = new Intent(this, ActivityEnterPrescription.class);
            }
            break;
        }

        if (null != intent) {
            intent.putExtra("edit", true);

            startActivity(intent);
        }
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_inactive);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("inactive"));
        }

        item = menu.findItem(R.id.action_view_location);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("view_location"));
        }
    }
}