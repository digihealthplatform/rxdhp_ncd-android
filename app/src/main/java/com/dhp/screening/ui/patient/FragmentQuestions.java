package com.dhp.screening.ui.patient;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterQuestionAnswerGroupView;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemQuestionChildren;
import com.dhp.screening.data.item.ItemScreeningQuestion;
import com.dhp.screening.data.table.TableProblems;
import com.dhp.screening.ui.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.item.ItemScreeningQuestion.TYPE_CHILD;


@SuppressLint("ValidFragment")
public class FragmentQuestions extends BaseFragment
        implements BaseFragment.InactiveCallbacks,
        BaseFragment.IBaseFragment {

    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;

    @BindView(R.id.rv_question_answer)
    RecyclerView rvQuestionAnswer;

    @BindView(R.id.btn_edit)
    FloatingActionButton fabEdit;

    private DataManager dataManager;

    private AdapterQuestionAnswerGroupView adapter;

    public FragmentQuestions(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_answers, container, false);
        ButterKnife.bind(this, view);

        rvQuestionAnswer.setLayoutManager(new LinearLayoutManager(getActivity()));
        setHasOptionsMenu(true);

        adapter = new AdapterQuestionAnswerGroupView(getActivity());
        rvQuestionAnswer.setNestedScrollingEnabled(false);
        rvQuestionAnswer.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvQuestionAnswer.setAdapter(adapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<ItemScreeningQuestion> headersList        = AssetReader.getScreeningQuestions(true);
        ArrayList<ItemScreeningQuestion> headersListEnglish = AssetReader.getScreeningQuestions(false);

        ArrayList<ItemScreeningQuestion> childrenList = new ArrayList<>();

        boolean firstTime = true;

        for (ItemScreeningQuestion itemScreeningQuestion : headersList) {
            childrenList.add(itemScreeningQuestion);

            if (firstTime) {
                for (ItemQuestionChildren children : itemScreeningQuestion.getQuestionHeader().getQuestionChildren()) {
                    childrenList.add(new ItemScreeningQuestion(TYPE_CHILD, null, children));
                }
            }

            firstTime = false;
        }

        List<TableProblems> itemQuestions = dataManager.getAllQuestions(StaticData.getRegistrationId());

        if (!itemQuestions.isEmpty()) {
            for (TableProblems itemQuestion : itemQuestions) {

                for (int i = 0; i < headersListEnglish.size(); i++) {
                    ItemScreeningQuestion itemScreeningQuestion = headersListEnglish.get(i);

                    ArrayList<ItemQuestionChildren> questionChildren = itemScreeningQuestion.getQuestionHeader().getQuestionChildren();

                    for (int j = 0; j < questionChildren.size(); j++) {
                        ItemQuestionChildren children = questionChildren.get(j);

                        if (children.getQuestion().equals(itemQuestion.getQuestion())
                                && children.getGroupName().equals(itemQuestion.getQuestionGroup())) {
                            headersList.get(i).getQuestionHeader().getQuestionChildren().get(j).setAnswer(itemQuestion.getAnswer());
                        }
                    }
                }
            }
        }

        adapter.setItems(headersList, childrenList);
    }

    @OnClick(R.id.btn_edit)
    void onEdit() {
        startActivity(new Intent(getActivity(), ActivityEditQuestions.class));
    }

    @Override
    public void onActive(boolean active, String inactiveReason) {

    }

    @Override
    public void setFabVisibility(boolean visible) {
        if (visible) {
            fabEdit.setVisibility(View.VISIBLE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg));

        } else {
            fabEdit.setVisibility(View.GONE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg_inactive));
        }
    }
}