package com.dhp.screening.ui.vital.test;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterTestResult;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemVitalTest;
import com.dhp.screening.data.item.ItemVitalTestType;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.ui.vital.ActivityVitalBase;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;


public class ActivityTestResult extends ActivityVitalBase {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.rv_result)
    RecyclerView rvResult;

    @Inject
    DataManager dataManager;

    private String testType;
    private String testSubType;

    private ItemVitalTestType vitalTestType;

    private AdapterTestResult adapterTestResult;

    private TableVitals tableVitals;

    private String deviceId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_test_result);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        testType = getIntent().getStringExtra("test_type");
        testSubType = getIntent().getStringExtra("test_sub_type");

        initToolbar(toolbar, testType, true);
        LView.setIcon(ivIcon, testType);

        tableVitals = new TableVitals();
        deviceId = getDeviceId();

        registrationId = StaticData.getRegistrationId();

        if (null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
        }

        checkAlreadyEntered(registrationId, "Weight");

        ItemVitalTest vitalTest = AssetReader.getVitalTest(testType);

        if (null == vitalTest) {
            finish();
            return;
        }

        for (ItemVitalTestType testType : vitalTest.getTestTypes()) {
            if (testType.getTestType().equals(testSubType)) {
                vitalTestType = testType;
            }
        }

        if (null == vitalTestType) {
            finish();
            return;
        }

        adapterTestResult = new AdapterTestResult(this, vitalTestType.getTestResults(), vitalTestType.getColumnSize());
    }

    @Override
    protected void onResume() {
        super.onResume();
        rvResult.setLayoutManager(new GridLayoutManager(this, vitalTestType.getColumnSize()));
        rvResult.setNestedScrollingEnabled(false);

        rvResult.post(new Runnable() {
            @Override
            public void run() {
                int height = rvResult.getHeight();
                adapterTestResult.setViewHeight(height);
                rvResult.setAdapter(adapterTestResult);
            }
        });
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        if (-1 == adapterTestResult.getSelectedIndex()) {
            LToast.warning("Please select the result");
            return;

        } else {
            if (VISIBLE == llAadhaar.getVisibility()) {
                registrationId = getRegistrationId();
            }
            if (null == registrationId || registrationId.equals("")) {
                LToast.warning("Enter Registration ID");
                return;
            }
            tableVitals.setRegistrationId(registrationId.toUpperCase());
        }

        tableVitals.setVitalType(testType);
        tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
        tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

        tableVitals.setVitalSubTypes(testSubType);
        tableVitals.setVitalValues(adapterTestResult.getResult());
        tableVitals.setVitalUnits("");
        tableVitals.setNotes(etNotes.getText().toString());
        tableVitals.setDeviceName("");
        tableVitals.setGpsLocation(getLocation());

        dataManager.addVital(tableVitals, deviceId);
        LToast.success(AssetReader.getLangKeyword("saved"));

        finish();
    }
}
