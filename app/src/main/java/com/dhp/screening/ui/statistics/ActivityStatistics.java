package com.dhp.screening.ui.statistics;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.lib.printer.PrinterManager;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.asset.AssetReader.KEY_DATE_RANGES;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_CSV;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_GRAPH_OPTION;
import static com.dhp.screening.util.LConstants.ANALYSIS_REPORT;
import static com.dhp.screening.util.LConstants.PDF_RISK_REPORT;
import static com.dhp.screening.util.LConstants.PDF_VITAL;
import static com.dhp.screening.util.LConstants.REPORT_SUMMARY;


public class ActivityStatistics extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_summary)
    Button btnSummary;

    @BindView(R.id.btn_survey_report)
    Button btnSurveyReport;

    @BindView(R.id.btn_summary_report)
    Button btnSummaryReport;

    @BindView(R.id.btn_risk_report)
    Button btnRiskReport;

    @BindView(R.id.btn_analysis_report)
    Button btnAnalysisReport;

    @BindView(R.id.btn_graph)
    Button btnGraph;

    @BindView(R.id.btn_print_vitals)
    Button btnPrintVitals;

    @Inject
    DataManager dataManager;

    private String reportType      = "";
    private String reportFormat    = "";
    private String reportDetail    = "";
    private String dateRangeString = "";
    private String ordering        = "";
    private String range           = "";

    private String[] ranges;

    private int dateRangeIndex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        initToolbar(toolbar, AssetReader.getLangKeyword("statistics"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        if (!AssetReader.toShow(KEY_SHOW_GRAPH_OPTION)) {
            btnGraph.setVisibility(View.GONE);
        }

        initLanguage();
    }

    @OnClick(R.id.btn_summary)
    void showSummary() {
        startActivity(new Intent(this, ActivitySummary.class));
    }

    @OnClick(R.id.btn_survey_report)
    void generateSurveyReport() {
        showSurveyReportDialog();
    }

    @OnClick(R.id.btn_summary_report)
    void generateSummaryReport() {
        showSummaryReportDialog();
    }

    @OnClick(R.id.btn_risk_report)
    void generateRiskReport() {
        showRiskReportDialog();
    }

    @OnClick(R.id.btn_graph)
    void showGraph() {
        startActivity(new Intent(this, ActivityGraph.class));
    }

    @OnClick(R.id.btn_print_vitals)
    void printVitals() {
        showPrintDialog();
    }

    @OnClick(R.id.btn_analysis_report)
    void generateAnalysisReport() {
        showAnalysisReportDialog();
    }

    private void showAnalysisReportDialog() {
        dateRangeIndex  = 0;
        dateRangeString = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_summary_report, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("generate_analysis_report"));

        builder.setView(dialogView);

        final String[] dateRanges = AssetReader.getSpinnerArray(KEY_DATE_RANGES);

        final Spinner spDate = (Spinner) dialogView.findViewById(R.id.sp_date);

        final LinearLayout llCustomDate = (LinearLayout) dialogView.findViewById(R.id.ll_custom_date);

        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        final EditText etEndDate   = (EditText) dialogView.findViewById(R.id.et_end_date);

        final TextView   tvFormat = (TextView) dialogView.findViewById(R.id.tv_format);
        final RadioGroup rgFormat = (RadioGroup) dialogView.findViewById(R.id.rg_report_format);

        tvFormat.setVisibility(View.GONE);
        rgFormat.setVisibility(View.GONE);

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etStartDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogStart");
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etEndDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogEnd");
            }
        });

        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dateRangeIndex = position;

                if (dateRanges.length - 1 == dateRangeIndex) {
                    dateRangeString = "";
                    llCustomDate.setVisibility(View.VISIBLE);
                } else {
                    llCustomDate.setVisibility(View.GONE);
                    etStartDate.setText("");
                    etEndDate.setText("");
                    dateRangeString = dateRanges[dateRangeIndex];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        LView.setSpinnerAdapter(this, spDate, dateRanges);

        spDate.setSelection(1);

        builder.setPositiveButton(AssetReader.getLangKeyword("generate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (0 == dateRangeIndex) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (dateRanges.length - 1 == dateRangeIndex
                        && (etStartDate.getText().toString().isEmpty() || etEndDate.getText().toString().isEmpty())) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (LUtils.getLongTime(etStartDate.getText().toString()) > LUtils.getLongTime(etEndDate.getText().toString())) {
                    LToast.warningLong(AssetReader.getLangKeyword("start_date_cant_be_greater"));
                    return;

                }

                File outputFile = new File(LUtils.getOutputFilePath("ncd_analysis_report", dataManager, "pdf"));

                GeneratePdfTask generatePdfTask =
                        new GeneratePdfTask(ActivityStatistics.this, outputFile, dataManager, ANALYSIS_REPORT, null);

                long startDateMillis = 0;
                long endDateMillis   = 0;

                Calendar calendar = Calendar.getInstance();

                String date = LUtils.getFormattedDate(calendar.get(java.util.Calendar.DATE),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.YEAR));

                long currentDateMillis = LUtils.getLongTime(date);

                if (1 == dateRangeIndex) { // Today
                    startDateMillis = endDateMillis = currentDateMillis;

                } else if (2 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (6 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else if (3 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (29 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else {
                    startDateMillis = LUtils.getLongTime(etStartDate.getText().toString());
                    endDateMillis   = LUtils.getLongTime(etEndDate.getText().toString());

                    dateRangeString = "From " + etStartDate.getText().toString() + " to " + etEndDate.getText().toString();
                }

                generatePdfTask.setDateRangeMillis(startDateMillis, endDateMillis);
                generatePdfTask.setDateRangeString(dateRangeString);
                generatePdfTask.execute();

                noteDialog.dismiss();
            }
        });
    }

    private void showPrintDialog() {

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_print_all, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("date"));

        builder.setView(dialogView);

        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        final EditText etEndDate   = (EditText) dialogView.findViewById(R.id.et_end_date);

        etStartDate.setText(LUtils.getCurrentDate());
        etEndDate.setText(LUtils.getCurrentDate());

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etStartDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogStart");
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etEndDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogEnd");
            }
        });

        builder.setPositiveButton(AssetReader.getLangKeyword("print"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etStartDate.getText().toString().isEmpty() || etEndDate.getText().toString().isEmpty()) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;
                }

                if (LUtils.getLongTime(etStartDate.getText().toString())
                        > LUtils.getLongTime(etEndDate.getText().toString())) {
                    LToast.warningLong(AssetReader.getLangKeyword("start_date_cant_be_greater"));
                    return;
                }

                new PrinterManager().printAllVitals(dataManager,
                        etStartDate.getText().toString(),
                        etEndDate.getText().toString());

                noteDialog.dismiss();
            }
        });
    }

    private void showSurveyReportDialog() {
        reportType     = "";
        dateRangeIndex = 0;
        ordering       = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_survey_report, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("generate_survey_report"));

        builder.setView(dialogView);

        TextView tvDate = (TextView) dialogView.findViewById(R.id.tv_date);
        tvDate.setText(AssetReader.getLangKeyword("date"));

        final RadioGroup rgReportType = (RadioGroup) dialogView.findViewById(R.id.rg_report);

        final RadioButton rbAll       = (RadioButton) dialogView.findViewById(R.id.rb_all);
        final RadioButton rbNormal    = (RadioButton) dialogView.findViewById(R.id.rb_normal);
        final RadioButton rbNotNormal = (RadioButton) dialogView.findViewById(R.id.rb_not_normal);
        final RadioButton rbOrderDate = (RadioButton) dialogView.findViewById(R.id.rb_order_date);
        final RadioButton rbOrderHoh  = (RadioButton) dialogView.findViewById(R.id.rb_order_hoh);

        rbAll.setText(AssetReader.getLangKeyword("all"));
        rbNormal.setText(AssetReader.getLangKeyword("normal"));
        rbNotNormal.setText(AssetReader.getLangKeyword("not_normal"));
        rbOrderDate.setText(AssetReader.getLangKeyword("order_by_date"));
        rbOrderHoh.setText(AssetReader.getLangKeyword("order_by_hoh"));

        rgReportType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgReportType.indexOfChild(rgReportType.findViewById(indexId));

                if (0 == selectedIndex) {
                    reportType = "All";

                } else if (1 == selectedIndex) {
                    reportType = "Normal";

                } else {
                    reportType = "Abnormal";
                }
            }
        });

        ((RadioButton) rgReportType.getChildAt(0)).setChecked(true);

        final TextView tvOrdering = (TextView) dialogView.findViewById(R.id.tv_ordering);
        tvOrdering.setText(AssetReader.getLangKeyword("ordering"));

        final RadioGroup rgOrdering = (RadioGroup) dialogView.findViewById(R.id.rg_order);

        rgOrdering.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgOrdering.indexOfChild(rgOrdering.findViewById(indexId));

                if (0 == selectedIndex) {
                    ordering = "Order by date";
                } else {
                    ordering = "Order by Head of House";
                }
            }
        });

        ((RadioButton) rgOrdering.getChildAt(0)).setChecked(true);

        Spinner spDate = (Spinner) dialogView.findViewById(R.id.sp_date);

        final LinearLayout llCustomDate = (LinearLayout) dialogView.findViewById(R.id.ll_custom_date);

        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        final EditText etEndDate   = (EditText) dialogView.findViewById(R.id.et_end_date);

        etStartDate.setHint(AssetReader.getLangKeyword("from"));
        etEndDate.setHint(AssetReader.getLangKeyword("to"));

        final String[] dateRanges = AssetReader.getSpinnerArray(KEY_DATE_RANGES);

        TextView tvVitalsRange = (TextView) dialogView.findViewById(R.id.tv_vitals_range);
        tvVitalsRange.setText(AssetReader.getLangKeyword("vitals_range"));

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etStartDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogStart");
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etEndDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogEnd");
            }
        });

        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dateRangeIndex = position;

                if (dateRanges.length - 1 == dateRangeIndex) {
                    llCustomDate.setVisibility(View.VISIBLE);
                } else {
                    llCustomDate.setVisibility(View.GONE);
                    etStartDate.setText("");
                    etEndDate.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        LView.setSpinnerAdapter(this, spDate, dateRanges);

        spDate.setSelection(1);

        builder.setPositiveButton(AssetReader.getLangKeyword("generate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (reportType.isEmpty()) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_vitals_range"));
                    return;

                } else if (0 == dateRangeIndex) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (dateRanges.length - 1 == dateRangeIndex
                        && (etStartDate.getText().toString().isEmpty() || etEndDate.getText().toString().isEmpty())) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (LUtils.getLongTime(etStartDate.getText().toString()) > LUtils.getLongTime(etEndDate.getText().toString())) {
                    LToast.warningLong(AssetReader.getLangKeyword("start_date_cant_be_greater"));
                    return;

                } else if (ordering.isEmpty()) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_ordering"));
                    return;
                }

                File outputFile = new File(LUtils.getOutputFilePath("ncd_survey_report", dataManager, "pdf"));

                GeneratePdfTask generatePdfTask =
                        new GeneratePdfTask(ActivityStatistics.this, outputFile, dataManager, PDF_VITAL, reportType);

                long startDateMillis = 0;
                long endDateMillis   = 0;

                Calendar calendar = Calendar.getInstance();

                String date = LUtils.getFormattedDate(calendar.get(java.util.Calendar.DATE),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.YEAR));

                long currentDateMillis = LUtils.getLongTime(date);

                if (1 == dateRangeIndex) { // Today

                    startDateMillis = endDateMillis = currentDateMillis;

                } else if (2 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (6 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else if (3 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (29 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else {
                    startDateMillis = LUtils.getLongTime(etStartDate.getText().toString());
                    endDateMillis   = LUtils.getLongTime(etEndDate.getText().toString());
                }

                generatePdfTask.setDateRangeMillis(startDateMillis, endDateMillis);
                generatePdfTask.setOrdering(ordering);
                generatePdfTask.execute();

                noteDialog.dismiss();
            }
        });
    }

    private void showSummaryReportDialog() {
        reportFormat   = "";
        reportDetail   = "";
        dateRangeIndex = 0;

        dateRangeString = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_summary_report, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("generate_summary_report"));

        builder.setView(dialogView);

        final Spinner spDate = (Spinner) dialogView.findViewById(R.id.sp_date);

        final LinearLayout llCustomDate = (LinearLayout) dialogView.findViewById(R.id.ll_custom_date);

        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        final EditText etEndDate   = (EditText) dialogView.findViewById(R.id.et_end_date);

        final TextView   tvDate       = (TextView) dialogView.findViewById(R.id.tv_date);
        final TextView   tvFormat     = (TextView) dialogView.findViewById(R.id.tv_format);
        final RadioGroup rgFormat     = (RadioGroup) dialogView.findViewById(R.id.rg_report_format);
        final TextView   tvReportType = (TextView) dialogView.findViewById(R.id.tv_report_type);
        final RadioGroup rgReportType = (RadioGroup) dialogView.findViewById(R.id.rg_report_type);

        final RadioButton rbPdf          = (RadioButton) dialogView.findViewById(R.id.rb_pdf);
        final RadioButton rbCsv          = (RadioButton) dialogView.findViewById(R.id.rb_csv);
        final RadioButton rbDetailed     = (RadioButton) dialogView.findViewById(R.id.rb_detailed);
        final RadioButton rbConsolidated = (RadioButton) dialogView.findViewById(R.id.rb_brief);

        tvReportType.setText(AssetReader.getLangKeyword("report_type"));
        tvDate.setText(AssetReader.getLangKeyword("date"));
        tvFormat.setText(AssetReader.getLangKeyword("format"));

        etStartDate.setHint(AssetReader.getLangKeyword("from"));
        etEndDate.setHint(AssetReader.getLangKeyword("to"));

        rbPdf.setText(AssetReader.getLangKeyword("pdf"));
        rbCsv.setText(AssetReader.getLangKeyword("csv"));
        rbDetailed.setText(AssetReader.getLangKeyword("report_detailed"));
        rbConsolidated.setText(AssetReader.getLangKeyword("report_consolidated"));

        if (!AssetReader.toShow(KEY_SHOW_CSV)) {
            reportFormat = "PDF";
            tvFormat.setVisibility(View.GONE);
            rgFormat.setVisibility(View.GONE);
            tvReportType.setVisibility(View.VISIBLE);
            rgReportType.setVisibility(View.VISIBLE);
        }

        rgFormat.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgFormat.indexOfChild(rgFormat.findViewById(indexId));

                if (0 == selectedIndex) {
                    reportFormat = "PDF";
                } else {
                    reportFormat = "CSV";
                }

                if (reportFormat.equals("PDF")) {
                    tvReportType.setVisibility(View.VISIBLE);
                    rgReportType.setVisibility(View.VISIBLE);

                } else {
                    tvReportType.setVisibility(View.GONE);
                    rgReportType.setVisibility(View.GONE);
                    rgReportType.clearCheck();
                    reportDetail = "";
                }
            }
        });

        rgReportType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgReportType.indexOfChild(rgReportType.findViewById(indexId));

                if (0 == selectedIndex) {
                    reportDetail = "Detailed (Date-wise)";
                } else {
                    reportDetail = "Consolidated";
                }
            }
        });

        ((RadioButton) rgReportType.getChildAt(0)).setChecked(true);

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etStartDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogStart");
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etEndDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogEnd");
            }
        });

        final String[] dateRanges = AssetReader.getSpinnerArray(KEY_DATE_RANGES);

        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dateRangeIndex = position;

                if (dateRanges.length - 1 == dateRangeIndex) {
                    dateRangeString = "";
                    llCustomDate.setVisibility(View.VISIBLE);

                } else {
                    llCustomDate.setVisibility(View.GONE);
                    etStartDate.setText("");
                    etEndDate.setText("");
                    dateRangeString = dateRanges[dateRangeIndex];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        LView.setSpinnerAdapter(this, spDate, dateRanges);

        spDate.setSelection(1);

        builder.setPositiveButton(AssetReader.getLangKeyword("generate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (0 == dateRangeIndex) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (dateRanges.length - 1 == dateRangeIndex
                        && (etStartDate.getText().toString().isEmpty() || etEndDate.getText().toString().isEmpty())) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (LUtils.getLongTime(etStartDate.getText().toString()) > LUtils.getLongTime(etEndDate.getText().toString())) {
                    LToast.warningLong(AssetReader.getLangKeyword("start_date_cant_be_greater"));
                    return;

                } else if (reportFormat.isEmpty()) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_report_format"));
                    return;

                } else if (reportFormat.equals("PDF") && reportDetail.isEmpty()) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_report_type"));
                    return;
                }

                File outputFile = new File(LUtils.getOutputFilePath("ncd_summary_report", dataManager, reportFormat.toLowerCase()));

                GeneratePdfTask generatePdfTask =
                        new GeneratePdfTask(ActivityStatistics.this, outputFile, dataManager, REPORT_SUMMARY, null);

                long startDateMillis = 0;
                long endDateMillis   = 0;

                Calendar calendar = Calendar.getInstance();

                String date = LUtils.getFormattedDate(calendar.get(java.util.Calendar.DATE),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.YEAR));

                long currentDateMillis = LUtils.getLongTime(date);

                if (1 == dateRangeIndex) { // Today
                    startDateMillis = endDateMillis = currentDateMillis;

                } else if (2 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (6 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else if (3 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (29 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else {
                    startDateMillis = LUtils.getLongTime(etStartDate.getText().toString());
                    endDateMillis   = LUtils.getLongTime(etEndDate.getText().toString());

                    dateRangeString = "From " + etStartDate.getText().toString() + " to " + etEndDate.getText().toString();
                }

                generatePdfTask.setDateRangeMillis(startDateMillis, endDateMillis);
                generatePdfTask.setReportFormat(reportFormat);
                generatePdfTask.setReportDetail(reportDetail);
                generatePdfTask.setDateRangeString(dateRangeString);
                generatePdfTask.execute();

                noteDialog.dismiss();
            }
        });
    }

    private void showRiskReportDialog() {
        reportType     = "";
        dateRangeIndex = 0;
        ordering       = "";
        ranges         = null;
        range          = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_risk_report, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("generate_survey_report"));

        builder.setView(dialogView);

        TextView tvDate = (TextView) dialogView.findViewById(R.id.tv_date);
        tvDate.setText(AssetReader.getLangKeyword("date"));

        final RadioGroup rgReportType = (RadioGroup) dialogView.findViewById(R.id.rg_report);

        final RadioButton rbHb = (RadioButton) dialogView.findViewById(R.id.rb_hb);

        final RadioButton rbOrderDate = (RadioButton) dialogView.findViewById(R.id.rb_order_date);
        final RadioButton rbOrderHoh  = (RadioButton) dialogView.findViewById(R.id.rb_order_hoh);

        final LinearLayout llRanges = (LinearLayout) dialogView.findViewById(R.id.ll_ranges);
        final Spinner      spRanges = (Spinner) dialogView.findViewById(R.id.sp_range);

        if (AssetReader.isEnabled("Hemoglobin")) {
            rbHb.setVisibility(View.VISIBLE);
        }

        spRanges.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (null == range || 0 == position) {
                    return;
                }
                range = ranges[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        rbOrderDate.setText(AssetReader.getLangKeyword("order_by_date"));
        rbOrderHoh.setText(AssetReader.getLangKeyword("order_by_hoh"));

        rgReportType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgReportType.indexOfChild(rgReportType.findViewById(indexId));

                if (0 == selectedIndex) {
                    reportType = "BMI";
                    ranges     = AssetReader.getAbnormalBmiRanges();

                } else if (1 == selectedIndex) {
                    reportType = "Diabetes";
                    ranges     = AssetReader.getAbnormalRbsRanges();

                } else if (2 == selectedIndex) {
                    reportType = "Hypertension";
                    ranges     = AssetReader.getAbnormalBpRanges();

                } else {
                    reportType = "Hemoglobin";
                    ranges     = AssetReader.getAbnormalHbRanges();
                }

                llRanges.setVisibility(View.VISIBLE);

                LView.setSpinnerAdapter(ActivityStatistics.this, spRanges, ranges);
            }
        });

        final TextView tvOrdering = (TextView) dialogView.findViewById(R.id.tv_ordering);
        tvOrdering.setText(AssetReader.getLangKeyword("ordering"));

        final RadioGroup rgOrdering = (RadioGroup) dialogView.findViewById(R.id.rg_order);

        rgOrdering.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgOrdering.indexOfChild(rgOrdering.findViewById(indexId));

                if (0 == selectedIndex) {
                    ordering = "Order by date";
                } else {
                    ordering = "Order by Head of House";
                }
            }
        });

        ((RadioButton) rgOrdering.getChildAt(0)).setChecked(true);

        Spinner spDate = (Spinner) dialogView.findViewById(R.id.sp_date);

        final LinearLayout llCustomDate = (LinearLayout) dialogView.findViewById(R.id.ll_custom_date);

        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        final EditText etEndDate   = (EditText) dialogView.findViewById(R.id.et_end_date);

        etStartDate.setHint(AssetReader.getLangKeyword("from"));
        etEndDate.setHint(AssetReader.getLangKeyword("to"));

        final String[] dateRanges = AssetReader.getSpinnerArray(KEY_DATE_RANGES);

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etStartDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogStart");
            }
        });

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etEndDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialogEnd");
            }
        });

        spDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dateRangeIndex = position;

                if (dateRanges.length - 1 == dateRangeIndex) {
                    llCustomDate.setVisibility(View.VISIBLE);
                } else {
                    llCustomDate.setVisibility(View.GONE);
                    etStartDate.setText("");
                    etEndDate.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        LView.setSpinnerAdapter(this, spDate, dateRanges);

        spDate.setSelection(1);

        builder.setPositiveButton(AssetReader.getLangKeyword("generate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (reportType.isEmpty()) {
                    LToast.warningLong("Please select risk based on");
                    return;

                } else if (0 == dateRangeIndex) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (null == range) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_vitals_range"));
                    return;

                } else if (dateRanges.length - 1 == dateRangeIndex
                        && (etStartDate.getText().toString().isEmpty() || etEndDate.getText().toString().isEmpty())) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_date_range"));
                    return;

                } else if (LUtils.getLongTime(etStartDate.getText().toString()) > LUtils.getLongTime(etEndDate.getText().toString())) {
                    LToast.warningLong(AssetReader.getLangKeyword("start_date_cant_be_greater"));
                    return;

                } else if (ordering.isEmpty()) {
                    LToast.warningLong(AssetReader.getLangKeyword("warning_select_ordering"));
                    return;
                }

                File outputFile = new File(LUtils.getOutputFilePath("ncd_risk_report", dataManager, "pdf"));

                GeneratePdfTask generatePdfTask =
                        new GeneratePdfTask(ActivityStatistics.this, outputFile, dataManager, PDF_RISK_REPORT, reportType);

                long startDateMillis = 0;
                long endDateMillis   = 0;

                Calendar calendar = Calendar.getInstance();

                String date = LUtils.getFormattedDate(calendar.get(java.util.Calendar.DATE),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.YEAR));

                long currentDateMillis = LUtils.getLongTime(date);

                if (1 == dateRangeIndex) { // Today

                    startDateMillis = endDateMillis = currentDateMillis;

                } else if (2 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (6 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else if (3 == dateRangeIndex) {
                    startDateMillis = currentDateMillis - (29 * 24 * 60 * 60 * 1000L);
                    endDateMillis   = currentDateMillis;

                } else {
                    startDateMillis = LUtils.getLongTime(etStartDate.getText().toString());
                    endDateMillis   = LUtils.getLongTime(etEndDate.getText().toString());
                }

                generatePdfTask.setVitalRange(range);
                generatePdfTask.setDateRangeMillis(startDateMillis, endDateMillis);
                generatePdfTask.setOrdering(ordering);
                generatePdfTask.execute();

                noteDialog.dismiss();
            }
        });
    }

    private void initLanguage() {
        btnSummary.setText(AssetReader.getLangKeyword("summary"));
        btnSurveyReport.setText(AssetReader.getLangKeyword("survey_report"));
        btnSummaryReport.setText(AssetReader.getLangKeyword("summary_report"));
        btnRiskReport.setText(AssetReader.getLangKeyword("risk_report"));
        btnAnalysisReport.setText(AssetReader.getLangKeyword("analysis_report"));
        btnPrintVitals.setText(AssetReader.getLangKeyword("print_vitals"));
        btnGraph.setText(AssetReader.getLangKeyword("graph"));
    }
}