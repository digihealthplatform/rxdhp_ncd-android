package com.dhp.screening.ui.vital;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.lib.barcodereader.ui.ActivityQr;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LConstants;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityVitalBase extends BaseActivity {

    public String registrationId;
    public String vitalSubtype;

    public DataManager dataManager;

    private EditText etDate;
    private EditText etTime;
    private EditText etAadhaar;
    private TextView tvName;

    protected String surveyDate;
    protected String surveyTime;

    private MediaPlayer audioPlayer;

    boolean dateTimeChanged;

    protected boolean qrScanned;

    protected void showPopupMessage(String vitalType, String vitalTypeNoSpace) {

        String toShow = AssetReader.getNestedValueFromAppConfig("vital_show_popup_message", vitalType);

        if (!hasValue(toShow) || !toShow.equals("Yes")) {
            return;
        }

        String message = AssetReader.getNestedText("vital_popup_message", vitalTypeNoSpace);

        if (!hasValue(message)) {
            return;
        }

        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(message);

        builder.setPositiveButton(AssetReader.getLangKeyword("okay"), null);
        builder.setCancelable(false);
        builder.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestLocationWithPermissionCheck();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationRequest();
    }

    protected String getDeviceId() {
        return Settings.Secure.getString(LApplication.getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID).toUpperCase();
    }

    protected String getRegistrationId() {
        String registrationId = etAadhaar.getText().toString().trim().toUpperCase();

        if (!hasValue(registrationId)) {
            return "";
        }

        if (dataManager.getLoggedInUser().isDemoUser()) {
            if (!registrationId.startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                registrationId = LConstants.DEFAULT_PARTNER_ID + registrationId;
            }
        }

        return registrationId;
    }

    protected void initViews(EditText etDate, EditText etTime, EditText etAadhaar, TextView tvName) {
        this.etDate = etDate;
        this.etTime = etTime;
        this.etAadhaar = etAadhaar;
        this.tvName = tvName;

        surveyDate = LUtils.getCurrentDate();
        surveyTime = LUtils.getCurrentTime();

        this.etDate.setText(surveyDate);
        this.etTime.setText(surveyTime);

        this.etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectDate();
            }
        });
        this.etTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectTime();
            }
        });

        findViewById(R.id.iv_scan_qr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityVitalBase.this, ActivityQr.class);
                intent.putExtra("is_search_registration_id", true);
                startActivity(intent);
            }
        });
    }

    protected void initDataManager(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (null != StaticData.getQrResult()) {
            displayAdhaarNumber();
            StaticData.setQrResult(null);
        }

        if (null != StaticData.getQrSearchResult()) {
            displaySearchQrResult();
            StaticData.setQrSearchResult(null);
        }
    }

    private void displaySearchQrResult() {
        etAadhaar.setText(StaticData.getQrSearchResult());
    }

    protected boolean checkAlreadyEntered(String registrationId, String vitalSubtype) {

        this.registrationId = registrationId;
        this.vitalSubtype = vitalSubtype;

        if (dataManager.checkVitalEnteredAlready(registrationId, vitalSubtype)) {
            showAlreadyEnteredDialog();
            return true;
        }
        return false;
    }

    private void showAlreadyEnteredDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage("The vital is already entered today for this person. Do you want to continue?");
        builder.setPositiveButton(AssetReader.getLangKeyword("continue"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();
    }

    private void displayAdhaarNumber() {
        String xml = StaticData.getQrResult();

        try {
            InputStream            is  = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder        db  = dbf.newDocumentBuilder();
            Document               doc = db.parse(new InputSource(is));
            doc.getDocumentElement().normalize();
            NodeList nodelist = doc.getElementsByTagName("PrintLetterBarcodeData");

            NamedNodeMap adhaarAttributes = nodelist.item(0).getAttributes();

            StaticData.setAadhaarAttributes(adhaarAttributes);

            LView.setTextFromXml(etAadhaar, "uid");
            LView.setTextFromXml(tvName, "name");

            etAadhaar.setEnabled(false);

            qrScanned = true;

            etAadhaar.setSelection(etAadhaar.getText().toString().length());

            if (dataManager.checkVitalEnteredAlready(etAadhaar.getText().toString(), vitalSubtype)) {
                showAlreadyEnteredDialog();
            }

        } catch (UnsupportedEncodingException e) {
            LLog.printStackTrace(e);

        } catch (ParserConfigurationException e) {
            LLog.printStackTrace(e);

        } catch (IOException e) {
            LLog.printStackTrace(e);

        } catch (SAXException e) {
            LToast.warning(AssetReader.getLangKeyword("warning_invalid_aadhaar_details"));
            LLog.printStackTrace(e);
        }
    }

    private void selectTime() {

        final Calendar calendar = java.util.Calendar.getInstance();

        int h = calendar.get(Calendar.HOUR_OF_DAY);

        TimePickerDialog timePickerDialog = TimePickerDialog.newInstance(new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                dateTimeChanged = true;
                surveyTime = LUtils.getFormattedTime(hourOfDay, minute);
                etTime.setText(surveyTime);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);

        timePickerDialog.show(getFragmentManager(), "tm");
    }

    private void selectDate() {
        final Calendar calendar = java.util.Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        dateTimeChanged = true;
                        surveyDate = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                        etDate.setText(surveyDate);
                    }
                },
                calendar.get(java.util.Calendar.YEAR),
                calendar.get(java.util.Calendar.MONTH),
                calendar.get(java.util.Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "DatePickerDialogDob");
    }

    private String getSurveyDateTime() {
        return (etTime.getText() + " " + etDate.getText());
    }

    public long getSurveyDateTimeMillis() {
        if (dateTimeChanged) {
            return LUtils.dateTimeToMilli(getSurveyDateTime());
        } else {
            return Calendar.getInstance().getTimeInMillis();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_go_back"));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityVitalBase.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    protected void playAudio(String key) {
        stopAudio();

        String audioFileName = AssetReader.getAudioFileName(key);

        if (null == audioFileName || audioFileName.isEmpty()) {
            return;
        }

        final File audioFile = new File(Environment.getExternalStorageDirectory(), getString(R.string.audio_folder) + audioFileName);

        if (!audioFile.exists()) {
            return;
        }

        audioPlayer = MediaPlayer.create(this, Uri.fromFile(audioFile));

        try {
            audioPlayer.start();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    protected void stopAudio() {
        try {
            audioPlayer.stop();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }
}
