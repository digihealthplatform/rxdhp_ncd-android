package com.dhp.screening.ui.patient;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterQuestionAnswer;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemQuestionAnswer;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableSociodemographics;
import com.dhp.screening.ui.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.asset.AssetReader.KEY_CHANGES_TO_CONTROL_DISEASE;
import static com.dhp.screening.data.asset.AssetReader.KEY_DISABILITIES;
import static com.dhp.screening.data.asset.AssetReader.KEY_DISEASES;
import static com.dhp.screening.data.asset.AssetReader.KEY_EDUCATION;
import static com.dhp.screening.data.asset.AssetReader.KEY_HABITS;
import static com.dhp.screening.data.asset.AssetReader.KEY_HEALTH_CARD_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_HEALTH_RECORDS_STORAGE;
import static com.dhp.screening.data.asset.AssetReader.KEY_HOUSE_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_LANGUAGES;
import static com.dhp.screening.data.asset.AssetReader.KEY_LITERACY;
import static com.dhp.screening.data.asset.AssetReader.KEY_MARITAL_STATUS;
import static com.dhp.screening.data.asset.AssetReader.KEY_MEDICINES_DAILY;
import static com.dhp.screening.data.asset.AssetReader.KEY_OCCUPATIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_RELATIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_SOCIO_EXTRA_QUESTIONS;
import static com.dhp.screening.data.asset.AssetReader.getSelectedLanguageVersion;
import static com.dhp.screening.util.LUtils.hasItem;
import static com.dhp.screening.util.LUtils.hasValue;


@SuppressLint("ValidFragment")
public class FragmentSociodemographics extends BaseFragment
        implements BaseFragment.InactiveCallbacks,
        BaseFragment.IBaseFragment {

    @BindView(R.id.rv_question_answer)
    RecyclerView rvQuestionAnswer;

    @BindView(R.id.btn_edit)
    FloatingActionButton fabEdit;

    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;

    private DataManager dataManager;

    private AdapterQuestionAnswer adapterQuestionAnswer;

    private TableSociodemographics tableSociodemographics;

    public FragmentSociodemographics() {
    }

    public FragmentSociodemographics(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapterQuestionAnswer = new AdapterQuestionAnswer(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_answers, container, false);
        ButterKnife.bind(this, view);

        rvQuestionAnswer.setLayoutManager(new LinearLayoutManager(getActivity()));
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        tableSociodemographics = dataManager.getSociodemographics(StaticData.getRegistrationId());

        ArrayList<ItemQuestionAnswer> questionAnswers = new ArrayList<>(16);

        if (null == tableSociodemographics) {
            tableSociodemographics = new TableSociodemographics();
        }

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("relation_to_head_of_house"), getSelectedLanguageVersion(tableSociodemographics.getRelationToHoh(), KEY_RELATIONS)));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("marital_status"), getSelectedLanguageVersion(tableSociodemographics.getMaritalStatus(), KEY_MARITAL_STATUS)));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("education"), getSelectedLanguageVersion(tableSociodemographics.getEducation(), KEY_EDUCATION)));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("literacy"), getSelectedLanguageVersion(tableSociodemographics.getLiteracy(), KEY_LITERACY)));

        String healthCard = tableSociodemographics.getHealthCard();
        if (null != healthCard && healthCard.equals("No")) {
            healthCard = AssetReader.getLangKeyword("no");
        } else {
            healthCard = getSelectedLanguageVersion(tableSociodemographics.getHealthCard(), KEY_HEALTH_CARD_TYPES);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("individual_health_card"), healthCard));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("occupation"), getSelectedLanguageVersion(tableSociodemographics.getCurrentOccupation(), KEY_OCCUPATIONS)));

        if (0 != tableSociodemographics.getMonthlyIncome()) {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("monthly_income"), "" + tableSociodemographics.getMonthlyIncome()));
        } else {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("monthly_income"), ""));
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("house_type"), getSelectedLanguageVersion(tableSociodemographics.getHouseType(), KEY_HOUSE_TYPES)));

        String[] list      = tableSociodemographics.getLanguagesSpoken().split(";");
        String   listValue = "";
        for (int i = 0; i < list.length; i++) {
            if (hasValue(listValue)) {
                listValue += ", ";
            }
            listValue += getSelectedLanguageVersion(list[i].trim(), KEY_LANGUAGES);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("languages_spoken"), listValue));

        if (AssetReader.toShow(KEY_SHOW_SOCIO_EXTRA_QUESTIONS)) {
            TableDemographics tableDemographics = dataManager.getPatient(StaticData.getRegistrationId());

            if (tableDemographics.getGender().equals("Female")) {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("does_women_have_to_take_precautions"), tableSociodemographics.getWomenSpecialPrecautionsOnPeriod()));
            }

            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("mobile_phone_used"), tableSociodemographics.getMobileUsed()));
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("mobile_comfort"), tableSociodemographics.getMobileComfort()));
        }

        list = tableSociodemographics.getHabits().split(";");
        listValue = "";
        for (int i = 0; i < list.length; i++) {
            if (hasValue(listValue)) {
                listValue += ", ";
            }
            listValue += getSelectedLanguageVersion(list[i].trim(), KEY_HABITS);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("habits"), listValue));

        if (AssetReader.toShow(KEY_SHOW_SOCIO_EXTRA_QUESTIONS)) {

            list = tableSociodemographics.getMedicinesTakenDaily().split(";");
            listValue = "";
            for (int i = 0; i < list.length; i++) {
                if (hasValue(listValue)) {
                    listValue += ", ";
                }
                listValue += getSelectedLanguageVersion(list[i].trim(), KEY_MEDICINES_DAILY);
            }
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("do_you_take_medicines_daily"), listValue));

            list = tableSociodemographics.getHealthRecordsStorage().split(";");
            listValue = "";
            for (int i = 0; i < list.length; i++) {
                if (hasValue(listValue)) {
                    listValue += ", ";
                }
                listValue += getSelectedLanguageVersion(list[i].trim(), KEY_HEALTH_RECORDS_STORAGE);
            }
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("health_records_storage"), listValue));
        }

        listValue = "";
        if (tableSociodemographics.getExercise().equals("Yes")) {
            listValue = AssetReader.getLangKeyword("yes");
        } else if (tableSociodemographics.getExercise().equals("No")) {
            listValue = AssetReader.getLangKeyword("no");
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("exercise"), listValue));

        listValue = "";
        if (tableSociodemographics.getDiet().equals("Vegetarian")) {
            listValue = AssetReader.getLangKeyword("vegetarian");
        } else if (tableSociodemographics.getDiet().equals("Non-vegetarian")) {
            listValue = AssetReader.getLangKeyword("non_vegetarian");
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("diet"), listValue));

        listValue = "";
        if (tableSociodemographics.getSick().equals("Yes")) {
            listValue = AssetReader.getLangKeyword("yes");
        } else if (tableSociodemographics.getSick().equals("No")) {
            listValue = AssetReader.getLangKeyword("no");
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("sick_during_survey"), listValue));

        list = tableSociodemographics.getDisease().split(";");
        listValue = "";
        for (int i = 0; i < list.length; i++) {
            if (hasValue(listValue)) {
                listValue += ", ";
            }
            listValue += getSelectedLanguageVersion(list[i].trim(), KEY_DISEASES);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("diseases"), listValue));

        if (AssetReader.toShow(KEY_SHOW_SOCIO_EXTRA_QUESTIONS)) {
            String[] diseases        = AssetReader.getCheckBoxArray(KEY_DISEASES);
            String[] diseasesEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_DISEASES);

            String[] diseaseTestLastDone     = tableSociodemographics.getDiseaseTestLastDone().split(",");
            String[] informedAboutDisease    = tableSociodemographics.getInformedAboutDisease().split(",");
            String[] medicationForDisease    = tableSociodemographics.getMedicationForDisease().split(",");
            String[] changesToControlDisease = tableSociodemographics.getChangesToControlDisease().split(",");

            boolean toContinue = false;

            if (diseaseTestLastDone.length > 0
                    || informedAboutDisease.length > 0
                    || medicationForDisease.length > 0
                    || changesToControlDisease.length > 0) {
                toContinue = true;
            }

            if (toContinue) {
                for (int i = 0; i < list.length; i++) {
                    String disease = list[i];

                    if (!hasItem(diseasesEnglish, disease)) {
                        disease = "Other";
                    }

                    String diseaseLang = diseases[getIndex(disease, diseasesEnglish)];

                    for (int j = 0; j < diseaseTestLastDone.length; j++) {

                        String[] splitArray = diseaseTestLastDone[j].split(":");

                        if (splitArray[0].trim().equals(disease)) {
                            try {
                                String time = splitArray[1].trim();

                                if (hasValue(time)) {
                                    time += " " + AssetReader.getLangKeyword("ago");

                                    if (disease.equals("Other")) {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("last_time_test_done_other") + "?", time));
                                    } else {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("last_time_test_done") + " " + diseaseLang + "?", time));
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }

                        splitArray = informedAboutDisease[j].split(":");
                        if (splitArray[0].trim().equals(disease)) {
                            try {
                                if (hasValue(splitArray[1].trim())) {
                                    if (disease.equals("Other")) {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("doctor_told_about_disease_other") + "?",
                                                splitArray[1].trim()));
                                    } else {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("doctor_told_about_disease") + " " + diseaseLang + "?",
                                                splitArray[1].trim()));
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }

                        splitArray = medicationForDisease[j].split(":");
                        if (splitArray[0].trim().equals(disease)) {
                            try {
                                if (hasValue(splitArray[1].trim())) {

                                    if (disease.equals("Other")) {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("do_you_take_medications_for_other"), splitArray[1].trim()));
                                    } else {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("do_you_take_medications_for_it") + " " + diseaseLang + "?", splitArray[1].trim()));
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }

                        splitArray = changesToControlDisease[j].split(":");
                        if (splitArray[0].trim().equals(disease)) {
                            try {
                                if (hasValue(splitArray[1].trim())) {
                                    String[] list1 = splitArray[1].trim().split(";");
                                    listValue = "";
                                    for (int k = 0; k < list1.length; k++) {
                                        if (hasValue(listValue)) {
                                            listValue += ", ";
                                        }
                                        listValue += getSelectedLanguageVersion(list1[k].trim(), KEY_CHANGES_TO_CONTROL_DISEASE);
                                    }

                                    if (disease.equals("Other")) {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("what_changes_have_you_made_to_control_other"), listValue));

                                    } else {
                                        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("what_changes_have_you_made_to_control") + " " + diseaseLang + "?", listValue));
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }
                    }
                }
            }
        }

        list = tableSociodemographics.getDisability().split(";");
        listValue = "";
        for (int i = 0; i < list.length; i++) {
            if (hasValue(listValue)) {
                listValue += ", ";
            }
            listValue += getSelectedLanguageVersion(list[i].trim(), KEY_DISABILITIES);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("disabilities"), listValue));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("notes_title"), tableSociodemographics.getNotes()));

        adapterQuestionAnswer.setItems(questionAnswers);

        rvQuestionAnswer.setAdapter(adapterQuestionAnswer);
    }

    @OnClick(R.id.btn_edit)
    void onEdit() {
        StaticData.setCurrentVital(null);
        startActivity(new Intent(getActivity(), ActivityEditSociodemographics.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_inactive: {

                if (tableSociodemographics.isInactive()) {
                    confirmActive(this);

                } else {
                    confirmInactive(this);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActive(boolean active, String inactiveReason) {

        if (active) {
            tableSociodemographics.setInactive(false);
            tableSociodemographics.setInactiveReason("");
            dataManager.updateSociodemographics(tableSociodemographics);
            getActivity().invalidateOptionsMenu();

        } else {
            tableSociodemographics.setInactive(true);
            tableSociodemographics.setInactiveReason(inactiveReason);
            dataManager.updateSociodemographics(tableSociodemographics);
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void setFabVisibility(boolean visible) {
        if (visible) {
            fabEdit.setVisibility(View.VISIBLE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg));

        } else {
            fabEdit.setVisibility(View.GONE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg_inactive));
        }
    }

    private int getIndex(String item, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(item)) {
                return i;
            }
        }
        return 0;
    }
}
