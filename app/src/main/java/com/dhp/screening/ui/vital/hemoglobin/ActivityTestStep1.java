package com.dhp.screening.ui.vital.hemoglobin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.dhp.screening.R;

public class ActivityTestStep1 extends BaseActivityTest {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_step_1);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Step 1", true);
    }

    @OnClick(R.id.btn_next)
    void onNext() {
        Intent intent = new Intent(this, ActivityTestStep2.class);
        startActivity(intent);
        finish();
    }
}