package com.dhp.screening.ui.patient;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.network.JsonSyncTask;
import com.dhp.screening.network.ViewLocationTask;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.BaseFragment;
import com.dhp.screening.ui.images.ActivityImagesList;
import com.dhp.screening.ui.user.ActivityUserHome;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.screening.data.DataManager.PATIENT_INFO;
import static com.dhp.screening.data.DataManager.SCREENING_QUESTIONS;
import static com.dhp.screening.data.DataManager.SOCIODEMOGRAPHICS;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_PROBLEMS;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_SOCIODEMOGRAPHICS;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityPatientDetails extends BaseActivity
        implements JsonSyncTask.CallBack {

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    private boolean newPatient;

    private TableDemographics tableDemographics;

    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_details);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        String registrationId = StaticData.getRegistrationId();
        tableDemographics = dataManager.getPatient(registrationId);

        if (null == tableDemographics) {
            newPatient = true;
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                invalidateOptionsMenu();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        if (newPatient) {
            initToolbar(toolbar, AssetReader.getLangKeyword("registration_id") + ": " + registrationId, true);
        } else {
            initToolbar(toolbar, tableDemographics.getFullName(), true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        setMenu(menu);
        initLanguageMenu(menu);
        return true;
    }

    private void setMenu(Menu menu) {
        menu.clear();

        Fragment fragment = adapter.getItem(viewPager.getCurrentItem());

        if (fragment instanceof FragmentVitals) {

            if (dataManager.getAllVitals(StaticData.getRegistrationId(), false).size() > 0) {
                getMenuInflater().inflate(R.menu.menu_patient_details_print, menu);

            } else {
                getMenuInflater().inflate(R.menu.menu_patient_details_default, menu);
            }

        } else if (fragment instanceof FragmentDemographics) {
            getMenuInflater().inflate(R.menu.menu_patient_demographics, menu);
            setMenuChecked(menu);

        } else if (fragment instanceof FragmentSociodemographics) {
            getMenuInflater().inflate(R.menu.menu_patient_details, menu);
            setMenuChecked(menu);

        } else {
            getMenuInflater().inflate(R.menu.menu_patient_details_default, menu);
        }

        BaseFragment.IBaseFragment baseFragment = (BaseFragment.IBaseFragment) fragment;

        if (dataManager.isInactive(StaticData.getRegistrationId(), PATIENT_INFO)) {
            baseFragment.setFabVisibility(false);
        } else {
            baseFragment.setFabVisibility(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();
        syncDataBackground(dataManager, this);
    }

    private void setMenuChecked(Menu menu) {
        Fragment                   fragment     = adapter.getItem(viewPager.getCurrentItem());
        BaseFragment.IBaseFragment baseFragment = (BaseFragment.IBaseFragment) fragment;

        int type = -1;

        if (fragment instanceof FragmentDemographics) {
            type = PATIENT_INFO;

        } else if (fragment instanceof FragmentSociodemographics) {
            type = SOCIODEMOGRAPHICS;

            if (null == dataManager.getSociodemographics(StaticData.getRegistrationId())) {
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_patient_details_default, menu);
                return;
            }

        } else if (fragment instanceof FragmentQuestions) {
            type = SCREENING_QUESTIONS;
        }

        if (-1 != type && dataManager.isInactive(StaticData.getRegistrationId(), type)) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            baseFragment.setFabVisibility(false);
        } else {
            baseFragment.setFabVisibility(true);
        }

        if (-1 != type && dataManager.isInactive(StaticData.getRegistrationId(), type)) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            baseFragment.setFabVisibility(false);
        } else {
            baseFragment.setFabVisibility(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_add_patient: {
                Intent intent = new Intent(this, ActivityUserHome.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            }

            case R.id.action_images: {
                Intent intent = new Intent(this, ActivityImagesList.class);
                intent.putExtra("registration_id", tableDemographics.getRegistrationId());
                startActivity(intent);
                break;
            }

            case R.id.action_view_location: {
                new ViewLocationTask(this, tableDemographics.getGpsLocation()).execute();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (!newPatient) {
            adapter.addFrag(new FragmentDemographics(dataManager), AssetReader.getLangKeyword("info"));
        }

        if (AssetReader.toShow(KEY_SHOW_SOCIODEMOGRAPHICS)) {
            adapter.addFrag(new FragmentSociodemographics(dataManager), AssetReader.getLangKeyword("sociodemographics"));
        }

        if (AssetReader.toShow(KEY_SHOW_PROBLEMS)) {
            adapter.addFrag(new FragmentQuestions(dataManager), AssetReader.getLangKeyword("problems"));
        }

        adapter.addFrag(new FragmentVitals(dataManager), AssetReader.getLangKeyword("vitals"));

        viewPager.setAdapter(adapter);
    }

    @Override
    public void onSyncStarted() {
        jsonSyncInProgress = true;
    }

    @Override
    public void onSyncComplete() {
        jsonSyncInProgress = false;
    }

    @Override
    public void onSyncFailed(String message) {
        if (hasValue(message)) {
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(message);
            builder.setPositiveButton(AssetReader.getLangKeyword("okay"), null);
            builder.show();
        }
        jsonSyncInProgress = false;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> fragmentList      = new ArrayList<>();
        private final List<String>   fragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_inactive);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("inactive"));
        }

        item = menu.findItem(R.id.action_add_patient);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("add_new_user"));
        }

        item = menu.findItem(R.id.action_print);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("print"));
        }

        item = menu.findItem(R.id.action_generate_report);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("generate_report"));
        }

        item = menu.findItem(R.id.action_summary);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("view_summary"));
        }

        item = menu.findItem(R.id.action_view_location);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("view_location"));
        }

        item = menu.findItem(R.id.action_print_id);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("print_id"));
        }
    }
}