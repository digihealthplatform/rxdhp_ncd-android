package com.dhp.screening.ui.vital.hemoglobin;

import android.graphics.Bitmap;
import android.util.Log;

import com.dhp.screening.data.StaticData;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import static org.opencv.imgproc.Imgproc.boundingRect;


public class HbImageProcessing {

    private static final String TAG = HbImageProcessing.class.getSimpleName();

    private static Double[] lValues;
    private static Double[] aValues;
    private static Double[] bValues;

    public static void initValues(String lValue, String aValue, String bValue) {
        lValues = new Double[11];
        aValues = new Double[6];
        bValues = new Double[6];

        String[] lvalues = lValue.split(";");
        String[] avalues = aValue.split(";");
        String[] bvalues = bValue.split(";");

        for (int i = 0; i < 6; i++) {
            try {
                double firstVal = Double.valueOf(lvalues[i]);
                lValues[i * 2] = firstVal;

                if (i < 5) {
                    double secondVal = Double.valueOf(lvalues[i + 1]);
                    lValues[i * 2 + 1] = firstVal - ((firstVal - secondVal) / 2.0);
                }

                aValues[i] = Double.valueOf(avalues[i]);
                bValues[i] = Double.valueOf(bvalues[i]);

            } catch (Exception e) {
            }
        }

        Log.d(TAG, "initValues: ");
    }

    public static float findHb(String capture1FileName, String capture2FileName, String capture3FileName, String calibrationFileName) throws Exception {
        Double x, y;
        //Captured images and calibration image read and stored
        Mat captured1 = Imgcodecs.imread(capture1FileName);
        Mat captured2 = Imgcodecs.imread(capture2FileName);
        Mat captured3 = Imgcodecs.imread(capture3FileName);
//        Mat calibration = Imgcodecs.imread(calibrationFileName);

        float hb = 0;
        //Creating an Array of L values obtained from the calibration image and arranging them in descending order
//        ValueArray valueArray = getLValues(calibration);

        //Double[] Lvalues = {75.57865556,68.01370556, 61.28158889, 60.45123333, 49.05091111, 40.58948889}; //valueArray.Lshades; Use for redmi 5 with small ROI
//        Double[] Lvalues = {82.0, 75.0, 70.0, 65.0, 52.0, 44.0};   //Use for redmi 5 with large ROI
        Double[] Lvalues = lValues;
        //Double[] Lvalues = {131.0,  124.0, 118.0, 108.0, 94.0,  78.0};      //Use for Samsung On7Pro for large ROI

        //This would flip the calibration lab values from 14 to 4  ---> 4 to 14
        //Commenting it would mean that the calibration should be placed from 4 to 10

        // Arrays.sort(Lvalues, Collections.<Double>reverseOrder());
 /*       List<Double> calLValues            = Arrays.asList(Lvalues);
        Collections.reverse(calLValues);
        Lvalues = calLValues.toArray(Lvalues);*/

        //Double[] Avalues = { 164.5495296, 164.4933687, 166.6570287, 167.3551651, 163.8084301, 160.1193367}; //valueArray.Avalues; Use for redmi 5
//        Double[] Avalues = {162.17, 163.65, 166.58, 166.72, 164.19, 160.86};  //Use for Redmi 5 with large ROI
        Double[] Avalues = aValues;
        //Double[] Avalues = {161.4416459, 164.7506219, 169.2510365, 170.925995, 171.1849088,166.5049751};  //Use for Samsung On7 Pro with large ROI

        //Arrays.sort(Avalues, Collections.<Double>reverseOrder());
/*        List<Double> calAValues            = Arrays.asList(Avalues);
        Collections.reverse(calAValues);
        Avalues = calAValues.toArray(Avalues);*/

        //Double[] Bvalues ={ 124.2631656, 128.8992042, 129.1427781, 128.6845006, 128.4918124, 127.0777403}; //valueArray.Bvalues; Use for redmi 5
//        Double[] Bvalues = {123.41, 128.22, 130.67, 131.51, 130.45, 127.3};    //For Redmi 5 with large ROI
        Double[] Bvalues = bValues;
        //Double[] Bvalues = {127.8461857, 132.7075041, 133.5906924,136.1796227, 134.9020522, 129.7051202};   //For samsung on7 pro with large ROI

        //Arrays.sort(Bvalues, Collections.<Double>reverseOrder());
/*        List<Double> calBValues            = Arrays.asList(Bvalues);
        Collections.reverse(calBValues);
        Bvalues = calBValues.toArray(Bvalues);*/

        ValueArray valueArray1 = new ValueArray();
        valueArray1.Lshades = Lvalues;
        valueArray1.Avalues = Avalues;
        valueArray1.Bvalues = Bvalues;

        //Obtaining the L values for the 3 captured images and finding the average L value
        Value value1 = hemoglobin(captured1);
        Value value2 = hemoglobin(captured2);
        Value value3 = hemoglobin(captured3);

        Double Lblood = (value1.Lshades + value2.Lshades + value3.Lshades) / 3.0;
        Double Ablood = (value1.Avalues + value2.Avalues + value3.Avalues) / 3.0;
        Double Bblood = (value1.Bvalues + value2.Bvalues + value3.Bvalues) / 3.0;

        // Assigning weights to Hb values
        /*double affinity[] = {1, 0.8, 0.6, 0.4, 0.2, 0.1};*/
        double affinity[] = {0.1, 0.2, 0.4, 0.6, 0.8, 1};
        //double affinity[] = {1, 0.7, 0.4, 0.3, 0.25, 0.05};
        int values[] = {4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};

        //If L value of image > L value of Hb 4
        if (Lblood > Lvalues[0]) {
            hb = 4;
        }
        //If L value of image is < L value of Hb 14
        else if
        (Lblood < Lvalues[10]) {
            hb = 14;
        }
        //Else algorithm run to find x and y
        //if x>y --> L upper --> Lower Hb value
        //if x<y --> L lower --> Higher Hb value
        else {
            for (int i = 0; i < 11; ++i) {
                if ((Lvalues[i] > Lblood) && (Lblood > Lvalues[i + 1])) {

                    //x=(sqrt(pow((Lblood-Lvalues[i]),2)+ pow(Ablood-Avalues[i],2)+ pow(Bblood-Bvalues[i],2)))*affinity[i];
                    //y=(sqrt(pow((Lblood-Lvalues[i+1]),2)+ pow(Ablood-Avalues[i+1],2)+ pow(Bblood-Bvalues[i+1],2)))*affinity[i+1];

//                    x = (sqrt((2 * pow((Lblood - Lvalues[i]), 2) + pow(Ablood - Avalues[i], 2) + pow(Bblood - Bvalues[i], 2)) / 4)) * affinity[i];
//                    y = (sqrt((2 * pow((Lblood - Lvalues[i + 1]), 2) + pow(Ablood - Avalues[i + 1], 2) + pow(Bblood - Bvalues[i + 1], 2)) / 4)) * affinity[i + 1];

                    x = sqrt((2 * pow((Lblood - Lvalues[i]), 2)));
                    y = sqrt((2 * pow((Lblood - Lvalues[i + 1]), 2)));

                    if (x < y) {
                        hb = values[i];
                    } else {
                        hb = values[i + 1];
                    }
                }
            }
        }

        // Save LAB values
        ArrayList<Value> valuesList = new ArrayList<>();
        valuesList.add(value1);
        valuesList.add(value2);
        valuesList.add(value3);
        StaticData.setLabValues(valuesList);

        return hb;
    }

    // Done for the sample image
    private static Value hemoglobin(Mat bgr_image) throws Exception {
        Size size = bgr_image.size();
        double M = size.height;
        double N = size.width;

        //Create captureCount new grey scale image file type
        Mat rgb_image = new Mat(size, CvType.CV_8UC1);
        //Convert the original image from BGR to RGB and store in create image file type
        Imgproc.cvtColor(bgr_image, rgb_image, Imgproc.COLOR_BGR2RGB);

        //Mat smooth = new Mat(size, CvType.CV_8UC1);
        Mat lab_image = new Mat(size, CvType.CV_8UC1);

        //Imgproc.blur(bgr_image, smooth, new Size(5, 5));

        //Convert the original image from BGR to LAB color space
        Imgproc.cvtColor(bgr_image, lab_image, Imgproc.COLOR_BGR2Lab);

        return getLblood(bgr_image, lab_image);

    }

    private static Value getLblood(Mat crop_img, Mat lab_image) throws Exception {
        Size size = crop_img.size();
        //Mat median_blur = new Mat(size, CvType.CV_8UC1);
        //Mat gray = new Mat(size, CvType.CV_8UC1);
        //Mat circles = new Mat(size, CvType.CV_8UC1);
        Mat mask = new Mat(size, CvType.CV_8UC1, new Scalar(0));

        Mat hsv_image = new Mat(size, CvType.CV_8UC1);
        Mat lower_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat upper_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat red_hue_image = new Mat(size, CvType.CV_8UC1);
        Point center = new Point();
        float[] radii = new float[1];
        int radius;

        Mat closing = new Mat(size, CvType.CV_8UC1);
        Mat kernel_9x9 = new Mat(new Size(9, 9), CvType.CV_8UC1, new Scalar(1));

        //Convert the crop image file fromm BGR to HSV color space
        Imgproc.cvtColor(crop_img, hsv_image, Imgproc.COLOR_BGR2HSV);
        //Segmenting the image for shades of red
        Core.inRange(hsv_image, new Scalar(0, 70, 50), new Scalar(10, 255, 255), lower_red_hue_range);
        Core.inRange(hsv_image, new Scalar(150, 70, 50), new Scalar(179, 255, 255), upper_red_hue_range);

        Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);

        Mat hierarchy = new Mat();

        // TODO: 18-May-18 remove
        // Storing the mat file of the red segmented image into captureCount bitmap ---> No use
        Bitmap tempdisp6 = Bitmap.createBitmap(red_hue_image.cols(), red_hue_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(red_hue_image, tempdisp6);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();

        //Making the red colours consistent in morphology (hence the morph_ERODE, morph_RECT could also be used)
        Imgproc.morphologyEx(red_hue_image, closing, Imgproc.MORPH_OPEN, kernel_9x9);
        //Setting up contours for the red shades in the image
        Imgproc.findContours(closing, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        //Finding the max area of the contours present in the segmented sample image
        int maxAreaIdx = 0;
        double maxArea = Imgproc.contourArea(contours.get(0));
        for (int i = 1; i < contours.size(); i++) {
            if (Imgproc.contourArea(contours.get(i)) > maxArea) {
                maxArea = Imgproc.contourArea(contours.get(i));
                maxAreaIdx = i;
            }
        }
        //Finding the minimum enclosing circle required for the sample image
        MatOfPoint2f contour0 = new MatOfPoint2f(contours.get(maxAreaIdx).toArray());
        Imgproc.minEnclosingCircle(contour0, center, radii);
        Imgproc.drawContours(crop_img, contours, maxAreaIdx, new Scalar(0, 0, 255), 1);
        Bitmap tempdisp7 = Bitmap.createBitmap(crop_img.cols(), crop_img.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(crop_img, tempdisp7);
        radius = (int) (radii[0]) - 7;
        Imgproc.circle(mask, center, radius, new Scalar(255, 255, 255), -1);
        Mat fg = new Mat();
        //Imgproc.circle(crop_img,center,radius, new Scalar(0,255,0),2);

        //bitwise OR operation done on crop_img --> Just keeps the contour segmented image and shows it in the circle
        Core.bitwise_or(crop_img, crop_img, fg, mask);
        Imgproc.cvtColor(fg, fg, Imgproc.COLOR_BGR2RGB);
        Bitmap tempdisp5 = Bitmap.createBitmap(fg.cols(), fg.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(fg, tempdisp5);

        Imgproc.medianBlur(lab_image, lab_image, 3);
        Scalar mean_val = Core.mean(lab_image, mask);
        Double Lblood = mean_val.val[0];
        Double Ablood = mean_val.val[1];
        Double Bblood = mean_val.val[2];
        Lblood = Math.round(Lblood * 1e4) / 1e4;

        Value value = new Value();
        value.Lshades = Lblood;
        value.Avalues = Ablood;
        value.Bvalues = Bblood;

        return value;
    }

    //Done for the calibration image
    private static ValueArray getLValues(Mat image) {
        Size size = image.size();
        double M = size.height;
        double N = size.width;

        //Mat crop_img = image.submat(75, (int) (M), 50, 50 + (int) (N - 500));

        Mat gray_image = new Mat(size, CvType.CV_8UC1);     // Not being used anywhere
        //Mat smooth = new Mat(size, CvType.CV_8UC1);
        Mat lab_image = new Mat(size, CvType.CV_8UC1);
        Mat hsv_image = new Mat(size, CvType.CV_8UC1);
        //Mat thresh = new Mat(size, CvType.CV_8UC1);
        Mat closing = new Mat(size, CvType.CV_8UC1);

        Mat kernel_9x9 = new Mat(new Size(9, 9), CvType.CV_8UC1, new Scalar(1));

        Mat lower_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat upper_red_hue_range = new Mat(size, CvType.CV_8UC1);
        Mat red_hue_image = new Mat(size, CvType.CV_8UC1);

        Imgproc.cvtColor(image, gray_image, Imgproc.COLOR_BGR2GRAY); //Grayscale image, not being used anywhere
        //Imgproc.blur(image, smooth, new Size(5, 5));
        Imgproc.cvtColor(image, lab_image, Imgproc.COLOR_BGR2Lab); // LAB image
        Imgproc.cvtColor(image, hsv_image, Imgproc.COLOR_BGR2HSV); // HSV image

        Core.inRange(hsv_image, new Scalar(0, 70, 50), new Scalar(10, 255, 255), lower_red_hue_range);
        Core.inRange(hsv_image, new Scalar(150, 70, 50), new Scalar(179, 255, 255), upper_red_hue_range);

        Core.addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, red_hue_image);

        Bitmap tempdisp1 = Bitmap.createBitmap(red_hue_image.cols(), red_hue_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(red_hue_image, tempdisp1);

//        Imgproc.threshold(gray_image, thresh, 127, 255, Imgproc.THRESH_BINARY_INV);
//
//        Bitmap tempdisp1 = Bitmap.createBitmap(thresh.cols(), thresh.rows(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(thresh, tempdisp1);

        // Morphological Closing  operation on red segmented image --> Why are they using MORPH_CLOSE instead of MORPH_ERODE
        // --> Closing is Dilation followed by Erosion --> Removes any predominant black regions
        //Changed to Open --> To remove any salt noise
        Imgproc.morphologyEx(red_hue_image, closing, Imgproc.MORPH_CLOSE, kernel_9x9);

        tempdisp1 = Bitmap.createBitmap(closing.cols(), closing.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(closing, tempdisp1);

        Mat hierarchy = new Mat();

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(closing, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        //Drawing all the contours found
        for (int contourIdx = 0; contourIdx < contours.size(); contourIdx++) {
            Imgproc.drawContours(closing, contours, contourIdx, new Scalar(0, 0, 255), 1);
        }
        tempdisp1 = Bitmap.createBitmap(closing.cols(), closing.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(closing, tempdisp1);

        int numberOfContours = contours.size();

        int maxAreaIdx = 0;
        double maxArea = Imgproc.contourArea(contours.get(0));
        for (int i = 1; i < contours.size(); i++) {
            if (Imgproc.contourArea(contours.get(i)) > maxArea) {
                maxArea = Imgproc.contourArea(contours.get(i));
                maxAreaIdx = i;
            }
        }

        //double maxArea    = Imgproc.contourArea(contours.get(maxAreaIdx));
        //MatOfPoint maxi = contours.get(maxAreaIdx);
        //MatOfPoint temp_contour;

        Rect xywh = boundingRect(contours.get(maxAreaIdx));
        int diff = (int) (xywh.width / 6);
        int x1 = xywh.x + 20;
        int y1 = xywh.y + 50;
        int x2 = x1 + (diff - 70);
        int y2 = xywh.y + (xywh.height - 50);

        int w = abs(x2 - x1);


        int h = abs(y2 - y1);

        Double[] Lshades = new Double[6];
        Double[] Ashades = new Double[6];
        Double[] Bshades = new Double[6];

        for (int i = 0; i < 6; i++) {

            Rect roi = new Rect(x1, y1, w, h);
            Mat subRects = lab_image.submat(roi);
            tempdisp1 = Bitmap.createBitmap(subRects.cols(), subRects.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(subRects, tempdisp1);

            Imgproc.medianBlur(subRects, subRects, 3);

            Scalar avgLab = Core.mean(subRects);
            Lshades[i] = avgLab.val[0]; //Gets the average L value
            Ashades[i] = avgLab.val[1];// Gets the average captureCount value
            Bshades[i] = avgLab.val[2];//Gets the average b value
            Imgproc.rectangle(lab_image, new Point(x1, y1), new Point(x2, y2), new Scalar(0, 255, 0), 2);
            x1 = x2 + 75;
            x2 = x1 + (diff - 70);
        }

        Bitmap tempdisp2 = Bitmap.createBitmap(lab_image.cols(), lab_image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(lab_image, tempdisp2);

        Mat bgr_image = new Mat(size, CvType.CV_8UC1);
        Imgproc.cvtColor(lab_image, bgr_image, Imgproc.COLOR_Lab2RGB);
        Utils.matToBitmap(bgr_image, tempdisp2);

        ValueArray valueArray = new ValueArray();
        valueArray.Avalues = Ashades;
        valueArray.Bvalues = Bshades;
        valueArray.Lshades = Lshades;

        return valueArray;
    }
}