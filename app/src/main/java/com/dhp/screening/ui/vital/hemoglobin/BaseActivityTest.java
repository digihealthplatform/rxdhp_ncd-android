package com.dhp.screening.ui.vital.hemoglobin;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;

import com.dhp.screening.R;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.ui.BaseActivity;

public class BaseActivityTest extends BaseActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        confirmBackPressed();
    }

    private void confirmBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setMessage(getString(R.string.message_confirm_on_back));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BaseActivityTest.super.onBackPressed();
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }
}