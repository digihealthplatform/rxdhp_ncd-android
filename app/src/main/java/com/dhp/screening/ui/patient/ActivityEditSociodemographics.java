package com.dhp.screening.ui.patient;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemDiseaseControl;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableSociodemographics;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.dialog.DialogSearch;
import com.dhp.screening.ui.dialog.DialogSearchCallbacks;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_AGE_UNITS;
import static com.dhp.screening.data.asset.AssetReader.KEY_CHANGES_TO_CONTROL_DISEASE;
import static com.dhp.screening.data.asset.AssetReader.KEY_DISABILITIES;
import static com.dhp.screening.data.asset.AssetReader.KEY_DISEASES;
import static com.dhp.screening.data.asset.AssetReader.KEY_EDUCATION;
import static com.dhp.screening.data.asset.AssetReader.KEY_HABITS;
import static com.dhp.screening.data.asset.AssetReader.KEY_HEALTH_CARD_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_HEALTH_RECORDS_STORAGE;
import static com.dhp.screening.data.asset.AssetReader.KEY_HOUSE_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_LANGUAGES;
import static com.dhp.screening.data.asset.AssetReader.KEY_LITERACY;
import static com.dhp.screening.data.asset.AssetReader.KEY_MARITAL_STATUS;
import static com.dhp.screening.data.asset.AssetReader.KEY_MEDICINES_DAILY;
import static com.dhp.screening.data.asset.AssetReader.KEY_MOBILE_COMFORT;
import static com.dhp.screening.data.asset.AssetReader.KEY_MOBILE_PHONES;
import static com.dhp.screening.data.asset.AssetReader.KEY_OCCUPATIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_RELATIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_SOCIO_EXTRA_QUESTIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_VACCINATION_RECORDS_STORAGE;
import static com.dhp.screening.data.asset.AssetReader.getSelectedLanguageVersion;
import static com.dhp.screening.util.LUtils.getDietIndex;
import static com.dhp.screening.util.LUtils.getYesNoIndex;
import static com.dhp.screening.util.LUtils.getYesNoUnsureIndex;
import static com.dhp.screening.util.LUtils.hasItem;
import static com.dhp.screening.util.LUtils.hasValue;
import static com.dhp.screening.util.LUtils.isOther;


public class ActivityEditSociodemographics extends BaseActivity
        implements CompoundButton.OnCheckedChangeListener,
        AdapterView.OnItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_relation_with_hoh)
    TextView tvRelationToHoh;

    @BindView(R.id.tv_marital_status)
    TextView tvMaritalStatus;

    @BindView(R.id.tv_education)
    TextView tvEducation;

    @BindView(R.id.tv_literacy)
    TextView tvLiteracy;

    @BindView(R.id.tv_occupation)
    TextView tvOccupation;

    @BindView(R.id.tv_monthly_income)
    TextView tvMonthlyIncome;

    @BindView(R.id.tv_house_type)
    TextView tvHouseType;

    @BindView(R.id.tv_language)
    TextView tvLanguages;

    @BindView(R.id.tv_habits)
    TextView tvHabits;

    @BindView(R.id.tv_diseases)
    TextView tvDiseases;

    @BindView(R.id.tv_disabilities)
    TextView tvDisabilities;

    @BindView(R.id.tv_exercise)
    TextView tvExercise;

    @BindView(R.id.tv_diet)
    TextView tvDiet;

    @BindView(R.id.tv_sick)
    TextView tvSick;

    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.tv_ailments)
    TextView tvAilments;

    @BindView(R.id.sp_marital_status)
    Spinner spMarital;

    @BindView(R.id.sp_education)
    Spinner spEducation;

    @BindView(R.id.sp_literacy)
    Spinner spLiteracy;

    @BindView(R.id.sp_occupation)
    Spinner spOccupation;

    @BindView(R.id.sp_house_type)
    Spinner spHouseType;

    @BindView(R.id.ll_languages)
    LinearLayout llLanguages;

    @BindView(R.id.ll_habits)
    LinearLayout llHabits;

    @BindView(R.id.ll_diseases)
    LinearLayout llDiseases;

    @BindView(R.id.ll_disabilities)
    LinearLayout llDisabilities;

    @BindView(R.id.et_relation_with_hoh)
    EditText etRelation;

    @BindView(R.id.et_relation_other)
    EditText etRelationOther;

    @BindView(R.id.et_marital_status_other)
    EditText etMaritalOther;

    @BindView(R.id.et_education_other)
    EditText etEducationOther;

    @BindView(R.id.et_literacy_other)
    EditText etLiteracyOther;

    @BindView(R.id.et_occupation_other)
    EditText etOccupationOther;

    @BindView(R.id.et_house_type_other)
    EditText etHouseTypeOther;

    @BindView(R.id.et_monthly_income)
    EditText etMonthlyIncome;

    @BindView(R.id.et_languages_other)
    EditText etLanguagesOther;

    @BindView(R.id.et_habits_other)
    EditText etHabitsOther;

    @BindView(R.id.et_diseases_other)
    EditText etDiseasesOther;

    @BindView(R.id.et_disabilities_other)
    EditText etDisabilitiesOther;

    @BindView(R.id.rg_exercise)
    RadioGroup rgExercise;

    @BindView(R.id.rg_diet)
    RadioGroup rgDiet;

    @BindView(R.id.rg_sick)
    RadioGroup rgSick;

    @BindView(R.id.et_ailments)
    EditText etAilments;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.et_dummy)
    EditText etDummy;

    @BindView(R.id.tv_health_card)
    TextView tvHealthCard;

    @BindView(R.id.rg_health_card)
    RadioGroup rgHealthCard;

    @BindView(R.id.rb_health_card_yes)
    RadioButton rbHealthCardYes;

    @BindView(R.id.rb_health_card_no)
    RadioButton rbHealthCardNo;

    @BindView(R.id.et_health_card_other)
    EditText etHealthCardOther;

    @BindView(R.id.sp_health_card)
    Spinner spHealthCard;

    @BindView(R.id.tv_medicines_taken_daily)
    TextView tvMedicinesTakenDaily;

    @BindView(R.id.ll_medicines_taken_daily_wrapper)
    LinearLayout llMedicinesTakenDailyWrapper;

    @BindView(R.id.ll_medicines_taken_daily)
    LinearLayout llMedicinesTakenDaily;

    @BindView(R.id.et_medicines_taken_daily_other)
    EditText etMedicinesTakenDailyOther;

    @BindView(R.id.tv_vaccination_records_storage)
    TextView tvVaccinationRecordsStorage;

    @BindView(R.id.ll_vaccination_records_storage_wrapper)
    LinearLayout llVaccinationRecordsStorageWrapper;

    @BindView(R.id.ll_vaccination_records_storage)
    LinearLayout llVaccinationRecordsStorage;

    @BindView(R.id.et_vaccination_records_storage_other)
    EditText etVaccinationRecordsStorageOther;

    @BindView(R.id.tv_precaution_during_period)
    TextView tvPrecautionDuringPeriod;

    @BindView(R.id.rg_precaution_during_period)
    RadioGroup rgPrecautionDuringPeriod;

    @BindView(R.id.tv_mobile_used)
    TextView tvMobileUsed;

    @BindView(R.id.ll_mobile_used_wrapper)
    LinearLayout llMobileUsedWrapper;

    @BindView(R.id.rg_mobile_used)
    RadioGroup rgMobileUsed;

    @BindView(R.id.et_mobile_used_other)
    EditText etMobileUsedOther;

    @BindView(R.id.tv_mobile_comfort)
    TextView tvMobileComfort;

    @BindView(R.id.ll_mobile_comfort_wrapper)
    LinearLayout llMobileComfortWrapper;

    @BindView(R.id.rg_mobile_comfort)
    RadioGroup rgMobileComfort;

    @BindView(R.id.et_mobile_comfort_other)
    EditText etMobileComfortOther;

    @BindView(R.id.ns_scroll)
    NestedScrollView scrollView;

    @BindView(R.id.et_dummy_2)
    EditText etDummy2;

    private String[] relations;
    private String[] maritalStatus;
    private String[] education;
    private String[] literacy;
    private String[] occupations;
    private String[] houseTypes;
    private String[] healthCardTypes;
    private String[] languages;
    private String[] habits;
    private String[] diseases;
    private String[] disabilities;
    private String[] medicinesDaily;
    private String[] mobilePhones;
    private String[] mobilePhoneComfort;
    private String[] healthRecordsStorage;

    private String[] relationsEnglish;
    private String[] maritalStatusEnglish;
    private String[] educationEnglish;
    private String[] literacyEnglish;
    private String[] occupationsEnglish;
    private String[] houseTypesEnglish;
    private String[] healthCardTypesEnglish;
    private String[] languagesEnglish;
    private String[] habitsEnglish;
    private String[] diseasesEnglish;
    private String[] disabilitiesEnglish;
    private String[] medicinesDailyEnglish;
    private String[] mobilePhonesEnglish;
    private String[] mobilePhoneComfortEnglish;
    private String[] healthRecordsStorageEnglish;

    private ArrayList<CheckBox> cbLanguages            = new ArrayList<>(16);
    private ArrayList<CheckBox> cbHabits               = new ArrayList<>(16);
    private ArrayList<CheckBox> cbDiseases             = new ArrayList<>(16);
    private ArrayList<CheckBox> cbDisabilities         = new ArrayList<>(16);
    private ArrayList<CheckBox> cbMedicinesDaily       = new ArrayList<>(16);
    private ArrayList<CheckBox> cbHealthRecordsStorage = new ArrayList<>(16);

    @Inject
    DataManager dataManager;

    private TableSociodemographics tableSociodemographics;

    private String  relationEntered;
    private boolean autoCheck;

    private LayoutInflater layoutInflater;
    private boolean        automaticCheck;

    private ArrayList<ItemDiseaseControl> diseaseItems = new ArrayList<>();

    private AlertDialog alertDialogDisease;

    private InputMethodManager inputMethodManager;

    private int diseaseIndex;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_sociodemographics);
        initToolbar(toolbar, "Sociodemographics", true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        tableSociodemographics = dataManager.getSociodemographics(StaticData.getRegistrationId());

        layoutInflater = LayoutInflater.from(this);
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        initLanguage();

        relations = AssetReader.getCheckBoxArray(KEY_RELATIONS);
        maritalStatus = AssetReader.getSpinnerArray(KEY_MARITAL_STATUS);
        education = AssetReader.getSpinnerArray(KEY_EDUCATION);
        literacy = AssetReader.getSpinnerArray(KEY_LITERACY);
        occupations = AssetReader.getSpinnerArray(KEY_OCCUPATIONS);
        houseTypes = AssetReader.getSpinnerArray(KEY_HOUSE_TYPES);
        healthCardTypes = AssetReader.getSpinnerArray(KEY_HEALTH_CARD_TYPES);

        languages = AssetReader.getCheckBoxArray(KEY_LANGUAGES);
        habits = AssetReader.getCheckBoxArray(KEY_HABITS);
        diseases = AssetReader.getCheckBoxArray(KEY_DISEASES);
        disabilities = AssetReader.getCheckBoxArray(KEY_DISABILITIES);
        medicinesDaily = AssetReader.getCheckBoxArray(KEY_MEDICINES_DAILY);
        mobilePhones = AssetReader.getCheckBoxArray(KEY_MOBILE_PHONES);
        mobilePhoneComfort = AssetReader.getCheckBoxArray(KEY_MOBILE_COMFORT);
        healthRecordsStorage = AssetReader.getCheckBoxArray(KEY_HEALTH_RECORDS_STORAGE);

        relationsEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_RELATIONS);
        maritalStatusEnglish = AssetReader.getSpinnerArrayEnglish(KEY_MARITAL_STATUS);
        educationEnglish = AssetReader.getSpinnerArrayEnglish(KEY_EDUCATION);
        literacyEnglish = AssetReader.getSpinnerArrayEnglish(KEY_LITERACY);
        occupationsEnglish = AssetReader.getSpinnerArrayEnglish(KEY_OCCUPATIONS);
        houseTypesEnglish = AssetReader.getSpinnerArrayEnglish(KEY_HOUSE_TYPES);
        healthCardTypesEnglish = AssetReader.getSpinnerArrayEnglish(KEY_HEALTH_CARD_TYPES);

        languagesEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_LANGUAGES);
        habitsEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_HABITS);
        diseasesEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_DISEASES);
        disabilitiesEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_DISABILITIES);
        medicinesDailyEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_MEDICINES_DAILY);
        mobilePhonesEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_MOBILE_PHONES);
        mobilePhoneComfortEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_MOBILE_COMFORT);
        healthRecordsStorageEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_HEALTH_RECORDS_STORAGE);

        LView.setSpinnerAdapter(this, spMarital, maritalStatus);
        LView.setSpinnerAdapter(this, spEducation, education);
        LView.setSpinnerAdapter(this, spLiteracy, literacy);
        LView.setSpinnerAdapter(this, spOccupation, occupations);
        LView.setSpinnerAdapter(this, spHouseType, houseTypes);
        LView.setSpinnerAdapter(this, spHealthCard, healthCardTypes);

        spMarital.setOnItemSelectedListener(this);
        spEducation.setOnItemSelectedListener(this);
        spLiteracy.setOnItemSelectedListener(this);
        spOccupation.setOnItemSelectedListener(this);
        spHouseType.setOnItemSelectedListener(this);
        spHealthCard.setOnItemSelectedListener(this);

        initCheckbox(languages, 8888, llLanguages, cbLanguages);
        initCheckbox(habits, 1111, llHabits, cbHabits);
        initCheckboxWithEdit(diseases, 3333, llDiseases, cbDiseases);
        initCheckbox(disabilities, 4444, llDisabilities, cbDisabilities);
        initCheckbox(medicinesDaily, 5555, llMedicinesTakenDaily, cbMedicinesDaily);

        initRadioButtons(rgMobileUsed, mobilePhones);
        rgMobileUsed.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                RadioButton radioButton = rgMobileUsed.findViewById(indexId);
                int         index       = rgMobileUsed.indexOfChild(radioButton);

                if (-1 != index) {
                    tableSociodemographics.setMobileUsed(mobilePhonesEnglish[index]);

                    if (tableSociodemographics.getMobileUsed().equals("Other")) {
                        etMobileUsedOther.setVisibility(VISIBLE);
                    } else {
                        etMobileUsedOther.setVisibility(View.GONE);
                    }
                }
            }
        });

        initRadioButtons(rgMobileComfort, mobilePhoneComfort);
        rgMobileComfort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                RadioButton radioButton = rgMobileComfort.findViewById(indexId);
                int         index       = rgMobileComfort.indexOfChild(radioButton);

                if (-1 != index) {
                    tableSociodemographics.setMobileComfort(mobilePhoneComfortEnglish[index]);

                    if (tableSociodemographics.getMobileComfort().equals("Other")) {
                        etMobileComfortOther.setVisibility(VISIBLE);
                    } else {
                        etMobileComfortOther.setVisibility(View.GONE);
                    }
                }
            }
        });

        initCheckbox(healthRecordsStorage, 7777, llVaccinationRecordsStorage, cbHealthRecordsStorage);

        rgExercise.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgExercise.indexOfChild(rgExercise.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableSociodemographics.setExercise("Yes");
                } else {
                    tableSociodemographics.setExercise("No");
                }
            }
        });

        rgDiet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgDiet.indexOfChild(rgDiet.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableSociodemographics.setDiet("Vegetarian");
                } else {
                    tableSociodemographics.setDiet("Non-vegetarian");
                }
            }
        });

        rgSick.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgSick.indexOfChild(rgSick.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableSociodemographics.setSick("Yes");
                } else {
                    tableSociodemographics.setSick("No");
                }

                if (tableSociodemographics.getSick().equals("Yes")) {
                    tvAilments.setVisibility(VISIBLE);
                    etAilments.setVisibility(VISIBLE);
                } else {
                    etAilments.setVisibility(View.GONE);
                    tvAilments.setVisibility(View.GONE);
                    etAilments.setText("");
                }
            }
        });

        rgPrecautionDuringPeriod.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgPrecautionDuringPeriod.indexOfChild(rgPrecautionDuringPeriod.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableSociodemographics.setWomenSpecialPrecautionsOnPeriod("Yes");

                } else if (1 == selectedIndex) {
                    tableSociodemographics.setWomenSpecialPrecautionsOnPeriod("No");

                } else {
                    tableSociodemographics.setWomenSpecialPrecautionsOnPeriod("Unsure");
                }
            }
        });

        rgHealthCard.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                int selectedIndex = rgHealthCard.indexOfChild(rgHealthCard.findViewById(indexId));

                if (0 == selectedIndex) {
                    spHealthCard.setVisibility(VISIBLE);

                    if (!autoCheck) {
                        tableSociodemographics.setHealthCard("");
                    }
                    autoCheck = false;

                } else {
                    tableSociodemographics.setHealthCard("No");
                    spHealthCard.setVisibility(View.GONE);
                    LView.setSpinnerAdapter(ActivityEditSociodemographics.this, spHealthCard, healthCardTypes);
                    etHealthCardOther.setText("");
                    etHealthCardOther.setVisibility(View.GONE);
                }
            }
        });

        if (null != tableSociodemographics) {
            populateUi();
            btnSave.setText(AssetReader.getLangKeyword("update"));
        } else {
            tableSociodemographics = new TableSociodemographics();
        }

        if (!AssetReader.toShow(KEY_SHOW_SOCIO_EXTRA_QUESTIONS)) {
            hideExtraQuestions();
        } else {
            TableDemographics tableDemographics = dataManager.getPatient(StaticData.getRegistrationId());

            if (tableDemographics.getGender().equals("Female")) {
                tvPrecautionDuringPeriod.setVisibility(VISIBLE);
                rgPrecautionDuringPeriod.setVisibility(VISIBLE);
            }
        }
    }

    private void hideExtraQuestions() {
        tvMedicinesTakenDaily.setVisibility(View.GONE);
        llMedicinesTakenDailyWrapper.setVisibility(View.GONE);
        tvVaccinationRecordsStorage.setVisibility(View.GONE);
        llVaccinationRecordsStorageWrapper.setVisibility(View.GONE);
        tvPrecautionDuringPeriod.setVisibility(View.GONE);
        rgPrecautionDuringPeriod.setVisibility(View.GONE);
        tvMobileUsed.setVisibility(View.GONE);
        llMobileUsedWrapper.setVisibility(View.GONE);
        tvMobileComfort.setVisibility(View.GONE);
        llMobileComfortWrapper.setVisibility(View.GONE);
    }

    private void initCheckbox(String[] array, int idExtra, LinearLayout linearLayout, ArrayList<CheckBox> checkBoxes) {

        for (int i = 0; i < array.length; i++) {
            CheckBox cb = new CheckBox(getApplicationContext());
            cb.setText(array[i]);
            cb.setGravity(Gravity.TOP);
            cb.setId(i + idExtra);
            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[]   = {R.color.teal_dark, R.color.white};
            CompoundButtonCompat.setButtonTintList(cb, new ColorStateList(states, colors));
            cb.setTextColor(ContextCompat.getColor(this, R.color.text_color_dark));
            cb.setTextSize(18.0f);
            linearLayout.addView(cb);

            cb.setOnCheckedChangeListener(this);

            checkBoxes.add(cb);
        }
    }

    private void initCheckboxWithEdit(String[] array, int idExtra, LinearLayout linearLayout, ArrayList<CheckBox> checkBoxes) {

        for (int i = 0; i < array.length; i++) {
            LinearLayout ll = (LinearLayout) layoutInflater.inflate(R.layout.layout_checkbox, linearLayout, false);

            ImageView ivEdit = ll.findViewById(R.id.iv_edit);
            ivEdit.setId(i + 999999);
            ivEdit.setOnClickListener(editClickListener);

            CheckBox cb = ll.findViewById(R.id.cb_disease);
            cb.setId(i + idExtra);
            cb.setText(array[i]);
            cb.setOnCheckedChangeListener(this);

            linearLayout.addView(ll);

            checkBoxes.add(cb);
        }
    }

    private void initRadioButtons(RadioGroup radioGroup, String[] array) {
        for (int i = 0; i < array.length; i++) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(array[i]);
            radioButton.setId(i + 8888);
            radioButton.setTextSize(16);

            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 15, 0, 0);
            radioButton.setLayoutParams(params);

            radioGroup.addView(radioButton);
        }

        radioGroup.requestLayout();
    }

    private void populateUi() {
        this.relationEntered = tableSociodemographics.getRelationToHoh();
        etRelation.setText(getSelectedLanguageVersion(relationEntered, KEY_RELATIONS));

        int spinnerIndex = LUtils.getSpinnerIndexOther(tableSociodemographics.getMaritalStatus(), maritalStatusEnglish);
        if (-1 != spinnerIndex) {
            spMarital.setSelection(spinnerIndex);

            if (maritalStatus[spinnerIndex].equals("Other")) {
                etMaritalOther.setText(tableSociodemographics.getMaritalStatus());
            }
        }

        spinnerIndex = LUtils.getSpinnerIndexOther(tableSociodemographics.getEducation(), educationEnglish);
        if (-1 != spinnerIndex) {
            spEducation.setSelection(spinnerIndex);

            if (education[spinnerIndex].equals("Other")) {
                etEducationOther.setText(tableSociodemographics.getEducation());
            }
        }

        spinnerIndex = LUtils.getSpinnerIndexOther(tableSociodemographics.getLiteracy(), literacyEnglish);
        if (-1 != spinnerIndex) {
            spLiteracy.setSelection(spinnerIndex);

            if (literacy[spinnerIndex].equals("Other")) {
                etLiteracyOther.setText(tableSociodemographics.getLiteracy());
            }
        }

        spinnerIndex = LUtils.getSpinnerIndexOther(tableSociodemographics.getCurrentOccupation(), occupationsEnglish);
        if (-1 != spinnerIndex) {
            spOccupation.setSelection(spinnerIndex);

            if (occupations[spinnerIndex].equals("Other")) {
                etOccupationOther.setText(tableSociodemographics.getCurrentOccupation());
            }
        }

        spinnerIndex = LUtils.getSpinnerIndexOther(tableSociodemographics.getHouseType(), houseTypesEnglish);
        if (-1 != spinnerIndex) {
            spHouseType.setSelection(spinnerIndex);

            if (houseTypes[spinnerIndex].equals("Other")) {
                etHouseTypeOther.setText(tableSociodemographics.getHouseType());
            }
        }

        String[] languages = tableSociodemographics.getLanguagesSpoken().split(";");
        populateCheckBox(languages, this.languagesEnglish, cbLanguages, etLanguagesOther);

        String[] habits = tableSociodemographics.getHabits().split(";");
        populateCheckBox(habits, this.habitsEnglish, cbHabits, etHabitsOther);

        String[] diseases = tableSociodemographics.getDisease().split(";");
        populateCheckBoxDisease(diseases, this.diseasesEnglish, cbDiseases, etDiseasesOther);

        String[] disabilities = tableSociodemographics.getDisability().split(";");
        populateCheckBox(disabilities, this.disabilitiesEnglish, cbDisabilities, etDisabilitiesOther);

        String[] medicines = tableSociodemographics.getMedicinesTakenDaily().split(";");
        populateCheckBox(medicines, this.medicinesDailyEnglish, cbMedicinesDaily, etMedicinesTakenDailyOther);

        String[] storage = tableSociodemographics.getHealthRecordsStorage().split(";");
        populateCheckBox(storage, this.healthRecordsStorageEnglish, cbHealthRecordsStorage, etVaccinationRecordsStorageOther);

        etNotes.setText(tableSociodemographics.getNotes());

        try {
            ((RadioButton) rgExercise.getChildAt(getYesNoIndex(tableSociodemographics.getExercise()))).setChecked(true);
        } catch (Exception e) {
        }

        try {
            ((RadioButton) rgDiet.getChildAt(getDietIndex(tableSociodemographics.getDiet()))).setChecked(true);
        } catch (Exception e) {
        }

        try {
            ((RadioButton) rgSick.getChildAt(getYesNoIndex(tableSociodemographics.getSick()))).setChecked(true);
        } catch (Exception e) {
        }

        try {
            ((RadioButton) rgPrecautionDuringPeriod.getChildAt(getYesNoUnsureIndex(tableSociodemographics.getWomenSpecialPrecautionsOnPeriod())))
                    .setChecked(true);
        } catch (Exception e) {
        }

        if (0 != tableSociodemographics.getMonthlyIncome()) {
            etMonthlyIncome.setText("" + tableSociodemographics.getMonthlyIncome());
        }

        int index = LUtils.getRadioWithOthersIndex(tableSociodemographics.getMobileUsed(), mobilePhonesEnglish);
        if (index > 0) {
            ((RadioButton) rgMobileUsed.getChildAt(index)).setChecked(true);
        }

        index = LUtils.getRadioWithOthersIndex(tableSociodemographics.getMobileComfort(), mobilePhoneComfortEnglish);
        if (index > 0) {
            if (index == mobilePhoneComfortEnglish.length - 1) {
                etMobileComfortOther.setText(tableSociodemographics.getMobileComfort());
                etMobileComfortOther.setVisibility(VISIBLE);
            }

            ((RadioButton) rgMobileComfort.getChildAt(index)).setChecked(true);
        }

        if (hasValue(tableSociodemographics.getHealthCard())) {
            if (tableSociodemographics.getHealthCard().equals("No")) {
                ((RadioButton) rgHealthCard.getChildAt(1)).setChecked(true);

            } else {
                autoCheck = true;
                ((RadioButton) rgHealthCard.getChildAt(0)).setChecked(true);

                spinnerIndex = LUtils.getSpinnerIndexOther(tableSociodemographics.getHealthCard(), healthCardTypesEnglish);

                if (-1 != spinnerIndex) {
                    spHealthCard.setSelection(spinnerIndex);

                    if (healthCardTypes[spinnerIndex].equals("Other")) {
                        etHealthCardOther.setText(tableSociodemographics.getHealthCard());
                        etHealthCardOther.setVisibility(VISIBLE);
                    }
                }
            }
        }

        etAilments.setText(tableSociodemographics.getAilment());

        String[] changes = AssetReader.getCheckBoxArrayEnglish(KEY_CHANGES_TO_CONTROL_DISEASE);

        ArrayList<ItemDiseaseControl> items = new ArrayList<>();

        String[] diseaseTestLastDone     = tableSociodemographics.getDiseaseTestLastDone().split(",");
        String[] informedAboutDisease    = tableSociodemographics.getInformedAboutDisease().split(",");
        String[] medicationForDisease    = tableSociodemographics.getMedicationForDisease().split(",");
        String[] changesToControlDisease = tableSociodemographics.getChangesToControlDisease().split(",");

        for (String disease : diseases) {
            if (!hasItem(this.diseasesEnglish, disease)) {
                disease = "Other";
            }

            ItemDiseaseControl item = new ItemDiseaseControl();
            item.setDiseaseName(disease);
            item.setDiseaseLang(this.diseases[getIndex(disease, this.diseasesEnglish)]);

            boolean[] changesArray = new boolean[changes.length];

            for (int i = 0; i < diseaseTestLastDone.length; i++) {

                String[] splitArray = diseaseTestLastDone[i].split(":");

                if (splitArray[0].trim().equals(disease)) {
                    try {
                        item.setTestDoneTime(splitArray[1].trim().split(" ")[0]);
                        item.setTestDoneTimeUnit(splitArray[1].trim().split(" ")[1]);
                    } catch (Exception e) {
                    }
                }

                splitArray = informedAboutDisease[i].split(":");
                if (splitArray[0].trim().equals(disease)) {
                    try {
                        item.setInformedAboutDisease(splitArray[1].trim());
                    } catch (Exception e) {
                    }
                }

                splitArray = medicationForDisease[i].split(":");
                if (splitArray[0].trim().equals(disease)) {
                    try {
                        item.setMedications(splitArray[1].trim());
                    } catch (Exception e) {
                    }
                }

                splitArray = changesToControlDisease[i].split(":");
                if (splitArray[0].trim().equals(disease)) {
                    try {
                        String[] changesMade = splitArray[1].trim().split(";");

                        for (int j = 0; j < changesMade.length; j++) {

                            boolean present = false;

                            for (int k = 0; k < changes.length; k++) {

                                if (changesMade[j].equals(changes[k])) {
                                    changesArray[k] = true;
                                    present = true;
                                    break;
                                }
                            }

                            if (!present && hasValue(changesMade[j])) {
                                item.setChangesOther(changesMade[j]);
                                changesArray[changes.length - 1] = true;
                            }
                        }
                    } catch (Exception e) {
                    }
                }
                item.setChangesChecked(changesArray);
            }
            items.add(item);
        }

        setDiseaseItems(items);
    }

    private int getIndex(String item, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(item)) {
                return i;
            }
        }
        return 0;
    }

    private void populateCheckBox(String[] savedArray, String[] fullArray, ArrayList<CheckBox> checkBoxes, EditText editText) {
        for (String item : savedArray) {

            for (int i = 0; i < fullArray.length; i++) {
                if (item.equals(fullArray[i])) {
                    checkBoxes.get(i).setChecked(true);
                }
            }

            if (isOther(item, fullArray) && null != editText) {
                if (!editText.getText().toString().isEmpty()) {
                    editText.setText(editText.getText() + ";");
                }

                checkBoxes.get(checkBoxes.size() - 1).setChecked(true);
                editText.setText(editText.getText() + item);
            }
        }
    }
    private void populateCheckBoxDisease(String[] savedArray, String[] fullArray, ArrayList<CheckBox> checkBoxes, EditText editText) {
        for (String item : savedArray) {

            for (int i = 0; i < fullArray.length; i++) {
                if (item.equals(fullArray[i])) {
                    automaticCheck = true;
                    checkBoxes.get(i).setChecked(true);
                }
            }

            if (isOther(item, fullArray) && null != editText) {
                if (!editText.getText().toString().isEmpty()) {
                    editText.setText(editText.getText() + ";");
                }

                automaticCheck = true;
                checkBoxes.get(checkBoxes.size() - 1).setChecked(true);
                editText.setText(editText.getText() + item);
            }
        }
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        readValues();

        if (isAllValuesEntered()) {
            confirmSave();
        }
    }

    private void readValues() {
        tableSociodemographics.setRelationToHoh(relationEntered);

        tableSociodemographics.setLanguagesSpoken(getCheckBoxValues(
                KEY_LANGUAGES, cbLanguages, etLanguagesOther
        ));
        tableSociodemographics.setHabits(getCheckBoxValues(
                KEY_HABITS, cbHabits, etHabitsOther
        ));
        tableSociodemographics.setDisease(getCheckBoxValues(
                KEY_DISEASES, cbDiseases, etDiseasesOther
        ));
        tableSociodemographics.setDisability(getCheckBoxValues(
                KEY_DISABILITIES, cbDisabilities, etDisabilitiesOther
        ));

        if (VISIBLE == etRelationOther.getVisibility()) {
            tableSociodemographics.setRelationToHoh(etRelationOther.getText().toString());
        }

        if (VISIBLE == etMaritalOther.getVisibility()) {
            tableSociodemographics.setMaritalStatus(etMaritalOther.getText().toString());
        }

        if (VISIBLE == etEducationOther.getVisibility()) {
            tableSociodemographics.setEducation(etEducationOther.getText().toString());
        }

        if (VISIBLE == etLiteracyOther.getVisibility()) {
            tableSociodemographics.setLiteracy(etLiteracyOther.getText().toString());
        }

        if (VISIBLE == etOccupationOther.getVisibility()) {
            tableSociodemographics.setCurrentOccupation(etOccupationOther.getText().toString());
        }

        if (VISIBLE == etHouseTypeOther.getVisibility()) {
            tableSociodemographics.setHouseType(etHouseTypeOther.getText().toString());
        }

        try {
            tableSociodemographics.setMonthlyIncome(Integer.parseInt(etMonthlyIncome.getText().toString()));
        } catch (Exception e) {
            tableSociodemographics.setMonthlyIncome(0);
        }

        if (VISIBLE == etAilments.getVisibility()) {
            tableSociodemographics.setAilment(etAilments.getText().toString());
        }

        if (VISIBLE == etHealthCardOther.getVisibility()) {
            tableSociodemographics.setHealthCard(etHealthCardOther.getText().toString());
        }

        if (VISIBLE == etMobileUsedOther.getVisibility()) {
            tableSociodemographics.setMobileUsed(etMobileUsedOther.getText().toString());
        }

        if (VISIBLE == etMobileComfortOther.getVisibility()) {
            tableSociodemographics.setMobileComfort(etMobileComfortOther.getText().toString());
        }

        tableSociodemographics.setMedicinesTakenDaily(getCheckBoxValues(
                KEY_MEDICINES_DAILY, cbMedicinesDaily, etMedicinesTakenDailyOther
        ));

        tableSociodemographics.setHealthRecordsStorage(getCheckBoxValues(
                KEY_VACCINATION_RECORDS_STORAGE, cbHealthRecordsStorage, etVaccinationRecordsStorageOther
        ));

        // Read disease info from adapter
        ArrayList<ItemDiseaseControl> items = getDiseaseItems();

        String diseaseTestLastDone     = "";
        String informedAboutDisease    = "";
        String medicationForDisease    = "";
        String changesToControlDisease = "";

        String[] changes = AssetReader.getCheckBoxArrayEnglish(KEY_CHANGES_TO_CONTROL_DISEASE);

        for (int i = 0; i < items.size(); i++) {
            if (hasValue(diseaseTestLastDone)) {
                diseaseTestLastDone += ",";
                informedAboutDisease += ",";
                medicationForDisease += ",";
                changesToControlDisease += ",";
            }

            ItemDiseaseControl item = items.get(i);

            if (hasValue(item.getTestDoneTime())) {
                diseaseTestLastDone += item.getDiseaseName() + " : " + item.getTestDoneTime() + " " + item.getTestDoneTimeUnit();
            } else {
                diseaseTestLastDone += item.getDiseaseName() + " : ";
            }

            informedAboutDisease += item.getDiseaseName() + " : " + item.getInformedAboutDisease();
            medicationForDisease += item.getDiseaseName() + " : " + item.getMedications();

            String changeMade = "";

            for (int j = 0; j < item.getChangesChecked().length; j++) {
                if (item.getChangesChecked()[j] && !changes[j].equals("Other")) {
                    if (hasValue(changeMade)) {
                        changeMade += ";";
                    }
                    changeMade += changes[j];
                }
            }
            if (hasValue(item.getChangesOther())) {
                changesToControlDisease += item.getDiseaseName() + " : " + changeMade + ";" + item.getChangesOther();
            } else {
                changesToControlDisease += item.getDiseaseName() + " : " + changeMade;
            }
        }

        tableSociodemographics.setDiseaseTestLastDone(diseaseTestLastDone);
        tableSociodemographics.setInformedAboutDisease(informedAboutDisease);
        tableSociodemographics.setMedicationForDisease(medicationForDisease);
        tableSociodemographics.setChangesToControlDisease(changesToControlDisease);

        tableSociodemographics.setNotes(etNotes.getText().toString());
    }

    private String getCheckBoxValues(String keyword, ArrayList<CheckBox> checkBoxes, EditText editText) {
        String values = "";

        for (int i = 0; i < checkBoxes.size(); i++) {
            if (checkBoxes.get(i).isChecked()) {
                if (checkBoxes.get(i).getText().equals(AssetReader.getLangKeyword("other"))) {
                    continue;
                }

                if (!values.isEmpty()) {
                    values += ";";
                }

                values += AssetReader.getEnglishIndex(keyword, i);
            }
        }
        if (null != editText && VISIBLE == editText.getVisibility() && !editText.getText().toString().isEmpty()) {
            if (!values.isEmpty()) {
                values += ";";
            }
            values += editText.getText().toString();
        }

        return values;
    }

    private boolean isAllValuesEntered() {
        if (VISIBLE == etRelationOther.getVisibility()
                && tableSociodemographics.getRelationToHoh().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_relation_to_hoh"));
            return false;
        }

        if (VISIBLE == etMaritalOther.getVisibility() && tableSociodemographics.getMaritalStatus().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_marital_status"));
            return false;
        }

        if (VISIBLE == etEducationOther.getVisibility() && tableSociodemographics.getEducation().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_education"));
            return false;
        }

        if (VISIBLE == etLiteracyOther.getVisibility() && tableSociodemographics.getLiteracy().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_literacy"));
            return false;
        }

        if (VISIBLE == etOccupationOther.getVisibility() && tableSociodemographics.getCurrentOccupation().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_occupation"));
            return false;
        }

        if (VISIBLE == etHouseTypeOther.getVisibility() && tableSociodemographics.getHouseType().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_house_type"));
            return false;
        }

        if (VISIBLE == etLanguagesOther.getVisibility() && etLanguagesOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_language"));
            return false;
        }

        if (VISIBLE == etHabitsOther.getVisibility() && etHabitsOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_habits"));
            return false;
        }

        if (VISIBLE == etDiseasesOther.getVisibility() && etDiseasesOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_disease"));
            return false;
        }

        if (VISIBLE == etDisabilitiesOther.getVisibility() && etDisabilitiesOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_disabilities"));
            return false;
        }

        if (VISIBLE == etHealthCardOther.getVisibility() && etHealthCardOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_health_card"));
            return false;
        }

        if (VISIBLE == etMedicinesTakenDailyOther.getVisibility() && etMedicinesTakenDailyOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_medicine_taken_daily"));
            return false;
        }

        if (VISIBLE == etVaccinationRecordsStorageOther.getVisibility() && etVaccinationRecordsStorageOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_health_records_storage"));
            return false;
        }

        if (VISIBLE == etMobileUsedOther.getVisibility() && etMobileUsedOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_mobile_user"));
            return false;
        }

        if (VISIBLE == etMobileComfortOther.getVisibility() && etMobileComfortOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_mobile_comfort"));
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_go_back"));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityEditSociodemographics.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void confirmSave() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));
        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (null != dataManager.getSociodemographics(StaticData.getRegistrationId())) {
                    dataManager.updateSociodemographics(tableSociodemographics);
                    Toasty.success(ActivityEditSociodemographics.this, AssetReader.getLangKeyword("updated")).show();

                } else {
                    dataManager.createSociodemographics(tableSociodemographics, StaticData.getRegistrationId());
                    Toasty.success(ActivityEditSociodemographics.this, AssetReader.getLangKeyword("saved")).show();
                }

                finish();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();

    }

    @OnClick(R.id.et_relation_with_hoh)
    void selectRelation() {
        new DialogSearch(this, new DialogSearchCallbacks() {
            @Override
            public void onSelected(String relation, int position) {

                relationEntered = relationsEnglish[position];

                if (relation.equals("Other")) {
                    etRelationOther.setVisibility(VISIBLE);
                }

                etRelation.setText(relation);
                etDummy.requestFocus();
            }
        }).withItems(relations, null).showDialog();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

        if (compoundButton.getId() > 8888 && compoundButton.getId() < 8910) {
            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                if (checked) {
                    etLanguagesOther.setVisibility(View.VISIBLE);

                } else {
                    etLanguagesOther.setVisibility(View.GONE);
                    etLanguagesOther.setText("");
                }
            }

        } else if (compoundButton.getId() > 1111 && compoundButton.getId() < 1150) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                if (checked) {
                    etHabitsOther.setVisibility(View.VISIBLE);

                } else {
                    etHabitsOther.setVisibility(View.GONE);
                    etHabitsOther.setText("");
                }
            }
        } else if (compoundButton.getId() >= 3333 && compoundButton.getId() < 3350) {

            int index = 0;

            for (int i = 0; i < cbDiseases.size(); i++) {
                if (cbDiseases.get(i).getId() == compoundButton.getId()) {
                    index = i;
                    break;
                }
            }

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                if (checked) {
                    etDiseasesOther.setVisibility(View.VISIBLE);

                } else {
                    etDiseasesOther.setVisibility(View.GONE);
                    etDiseasesOther.setText("");
                }
            }

            if (checked) {
                addDiseaseItem(diseasesEnglish[index], compoundButton.getText().toString());
                showEdit(index);

                if (!automaticCheck) {
                    showDiseasePopup(index);
                }

            } else {
                removeDiseaseItem(diseasesEnglish[index]);
                hideEdit(index);
            }

            automaticCheck = false;

        } else if (compoundButton.getId() > 4444 && compoundButton.getId() < 4460) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                if (checked) {
                    etDisabilitiesOther.setVisibility(View.VISIBLE);

                } else {
                    etDisabilitiesOther.setVisibility(View.GONE);
                    etDisabilitiesOther.setText("");
                }
            }
        } else if (compoundButton.getId() > 5555 && compoundButton.getId() < 5599) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                if (checked) {
                    etMedicinesTakenDailyOther.setVisibility(View.VISIBLE);

                } else {
                    etMedicinesTakenDailyOther.setVisibility(View.GONE);
                    etMedicinesTakenDailyOther.setText("");
                }
            }
        } else if (compoundButton.getId() > 7777 && compoundButton.getId() < 7800) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                if (checked) {
                    etVaccinationRecordsStorageOther.setVisibility(View.VISIBLE);

                } else {
                    etVaccinationRecordsStorageOther.setVisibility(View.GONE);
                    etVaccinationRecordsStorageOther.setText("");
                }
            }
        }
    }

    private void showEdit(int index) {
        findViewById(index + 999999).setVisibility(VISIBLE);
    }

    private void hideEdit(int index) {
        findViewById(index + 999999).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if (position <= 0) {
            return;
        }

        switch (adapterView.getId()) {

            case R.id.sp_marital_status: {

                if (maritalStatusEnglish[position].equals("Other")) {
                    etMaritalOther.setVisibility(VISIBLE);

                } else {
                    etMaritalOther.setText("");
                    etMaritalOther.setVisibility(View.GONE);
                    tableSociodemographics.setMaritalStatus(maritalStatusEnglish[position]);
                }
                break;
            }

            case R.id.sp_education: {
                if (educationEnglish[position].equals("Other")) {
                    etEducationOther.setVisibility(VISIBLE);

                } else {
                    etEducationOther.setText("");
                    etEducationOther.setVisibility(View.GONE);
                    tableSociodemographics.setEducation(educationEnglish[position]);
                }
                break;
            }

            case R.id.sp_literacy: {
                if (literacyEnglish[position].equals("Other")) {
                    etLiteracyOther.setVisibility(VISIBLE);

                } else {
                    etLiteracyOther.setText("");
                    etLiteracyOther.setVisibility(View.GONE);
                    tableSociodemographics.setLiteracy(literacyEnglish[position]);
                }
                break;
            }

            case R.id.sp_occupation: {
                if (occupationsEnglish[position].equals("Other")) {
                    etOccupationOther.setVisibility(VISIBLE);

                } else {
                    etOccupationOther.setText("");
                    etOccupationOther.setVisibility(View.GONE);
                    tableSociodemographics.setCurrentOccupation(occupationsEnglish[position]);
                }
                break;
            }

            case R.id.sp_house_type: {
                if (houseTypesEnglish[position].equals("Other")) {
                    etHouseTypeOther.setVisibility(VISIBLE);

                } else {
                    etHouseTypeOther.setText("");
                    etHouseTypeOther.setVisibility(View.GONE);
                    tableSociodemographics.setHouseType(houseTypesEnglish[position]);
                }
                break;
            }

            case R.id.sp_health_card: {
                if (healthCardTypesEnglish[position].equals("Other")) {
                    etHealthCardOther.setVisibility(VISIBLE);
                } else {
                    etHealthCardOther.setText("");
                    etHealthCardOther.setVisibility(View.GONE);
                    tableSociodemographics.setHealthCard(healthCardTypesEnglish[position]);
                }
                break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private void initLanguage() {
        tvRelationToHoh.setText(AssetReader.getLangKeyword("relation_to_head_of_house"));
        tvMaritalStatus.setText(AssetReader.getLangKeyword("marital_status"));
        tvEducation.setText(AssetReader.getLangKeyword("education"));
        tvLiteracy.setText(AssetReader.getLangKeyword("literacy"));
        tvOccupation.setText(AssetReader.getLangKeyword("occupation"));
        tvMonthlyIncome.setText(AssetReader.getLangKeyword("monthly_income"));
        tvHouseType.setText(AssetReader.getLangKeyword("house_type"));
        tvLanguages.setText(AssetReader.getLangKeyword("languages_spoken"));
        tvHabits.setText(AssetReader.getLangKeyword("habits"));
        tvDiseases.setText(AssetReader.getLangKeyword("diseases"));
        tvDisabilities.setText(AssetReader.getLangKeyword("disabilities"));
        tvExercise.setText(AssetReader.getLangKeyword("doing_exercise"));
        tvDiet.setText(AssetReader.getLangKeyword("diet"));
        tvSick.setText(AssetReader.getLangKeyword("sick_during_survey"));
        tvNotes.setText(AssetReader.getLangKeyword("notes_title"));
        tvHealthCard.setText(AssetReader.getLangKeyword("individual_health_card"));

        rbHealthCardYes.setText(AssetReader.getLangKeyword("yes"));
        rbHealthCardNo.setText(AssetReader.getLangKeyword("no"));

        etRelation.setHint(AssetReader.getLangKeyword("enter"));
        etRelationOther.setHint(AssetReader.getLangKeyword("if_other"));
        etMaritalOther.setHint(AssetReader.getLangKeyword("if_other"));
        etEducationOther.setHint(AssetReader.getLangKeyword("if_other"));
        etLiteracyOther.setHint(AssetReader.getLangKeyword("if_other"));
        etOccupationOther.setHint(AssetReader.getLangKeyword("if_other"));
        etMonthlyIncome.setHint(AssetReader.getLangKeyword("enter"));
        etLanguagesOther.setHint(AssetReader.getLangKeyword("if_other"));
        etHabitsOther.setHint(AssetReader.getLangKeyword("if_other"));
        etDiseasesOther.setHint(AssetReader.getLangKeyword("if_other"));
        etDisabilitiesOther.setHint(AssetReader.getLangKeyword("if_other"));
        etNotes.setHint(AssetReader.getLangKeyword("enter"));
        etAilments.setHint(AssetReader.getLangKeyword("enter"));
        etHealthCardOther.setHint(AssetReader.getLangKeyword("enter"));

        ((RadioButton) rgExercise.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgExercise.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));

        ((RadioButton) rgDiet.getChildAt(0)).setText(AssetReader.getLangKeyword("vegetarian"));
        ((RadioButton) rgDiet.getChildAt(1)).setText(AssetReader.getLangKeyword("non_vegetarian"));

        ((RadioButton) rgSick.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgSick.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));

        tvMedicinesTakenDaily.setText(AssetReader.getLangKeyword("do_you_take_medicines_daily"));
        tvMobileUsed.setText(AssetReader.getLangKeyword("mobile_phone_used"));
        tvMobileComfort.setText(AssetReader.getLangKeyword("mobile_comfort"));
        tvVaccinationRecordsStorage.setText(AssetReader.getLangKeyword("health_records_storage"));
        tvPrecautionDuringPeriod.setText(AssetReader.getLangKeyword("does_women_have_to_take_precautions"));

        ((RadioButton) rgPrecautionDuringPeriod.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgPrecautionDuringPeriod.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgPrecautionDuringPeriod.getChildAt(2)).setText(AssetReader.getLangKeyword("unsure"));

        etMedicinesTakenDailyOther.setHint(AssetReader.getLangKeyword("enter"));
        etMobileUsedOther.setHint(AssetReader.getLangKeyword("enter"));
        etMobileComfortOther.setHint(AssetReader.getLangKeyword("enter"));
    }

    View.OnClickListener editClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int index = view.getId() - 999999;
            showDiseasePopup(index);
        }
    };

    private void showDiseasePopup(int index) {
        if (null != alertDialogDisease && alertDialogDisease.isShowing()) {
            return;
        }
        diseaseIndex = -1;

        String diseaseEnglish = diseasesEnglish[index];

        for (int i = 0; i < diseaseItems.size(); i++) {
            if (diseaseItems.get(i).getDiseaseName().equals(diseaseEnglish)) {
                diseaseIndex = i;
            }
        }

//        if (-1 == diseaseIndex) {
//            for (int i = 0; i < diseaseItems.size(); i++) {
//                boolean present = false;
//
//                for (String disease : diseasesEnglish) {
//                    if (diseaseItems.get(i).equals(disease)) {
//                        present = true;
//                        break;
//                    }
//                }
//
//                if (!present) {
//                    diseaseIndex = i;
//                }
//            }
//        }

        final ItemDiseaseControl oldItem = diseaseItems.get(diseaseIndex);

        final ItemDiseaseControl item = new ItemDiseaseControl();
        item.setDiseaseName(oldItem.getDiseaseName());
        item.setDiseaseLang(oldItem.getDiseaseLang());
        item.setTestDoneTime(oldItem.getTestDoneTime());
        item.setTestDoneTimeUnit(oldItem.getTestDoneTimeUnit());
        item.setInformedAboutDisease(oldItem.getInformedAboutDisease());
        item.setMedications(oldItem.getMedications());
        item.setChangesOther(oldItem.getChangesOther());
        item.setChangesChecked(new boolean[oldItem.getChangesChecked().length]);

        for (int i = 0; i < oldItem.getChangesChecked().length; i++) {
            item.getChangesChecked()[i] = oldItem.getChangesChecked()[i];
        }

        final View dialogView = getLayoutInflater().inflate(R.layout.item_disease_control, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setView(dialogView);

        final String[] timeUnitsEnglish = AssetReader.getSpinnerArrayEnglish(KEY_AGE_UNITS);

        final TextView     tvTestLastDone         = (TextView) dialogView.findViewById(R.id.tv_disease_test_last_done);
        final EditText     etTestLastDone         = (EditText) dialogView.findViewById(R.id.et_disease_test_last_done);
        final Spinner      spTestLastDone         = (Spinner) dialogView.findViewById(R.id.sp_disease_test_last_done);
        final TextView     tvInformedAboutDisease = (TextView) dialogView.findViewById(R.id.tv_informed_about_disease);
        final RadioGroup   rgInformedAboutDisease = (RadioGroup) dialogView.findViewById(R.id.rg_informed_about_disease);
        final TextView     tvMedication           = (TextView) dialogView.findViewById(R.id.tv_medication_for_disease);
        final RadioGroup   rgMedication           = (RadioGroup) dialogView.findViewById(R.id.rg_medication_for_disease);
        final TextView     tvChanges              = (TextView) dialogView.findViewById(R.id.tv_changes_to_control_disease);
        final LinearLayout llChangesWrapper       = (LinearLayout) dialogView.findViewById(R.id.ll_changes_to_control_disease_wrapper);
        final LinearLayout llChanges              = (LinearLayout) dialogView.findViewById(R.id.ll_changes_to_control_disease);
        final EditText     etChanges              = (EditText) dialogView.findViewById(R.id.et_changes_to_control_disease_other);

        ((RadioButton) rgInformedAboutDisease.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgInformedAboutDisease.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgInformedAboutDisease.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgMedication.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgMedication.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));

        etTestLastDone.setHint(AssetReader.getLangKeyword("enter"));
        etChanges.setHint(AssetReader.getLangKeyword("enter"));

        String[] timeUnits = AssetReader.getSpinnerArray(KEY_AGE_UNITS);
        String[] changes   = AssetReader.getCheckBoxArray(KEY_CHANGES_TO_CONTROL_DISEASE);

        LView.setSpinnerAdapter(this, spTestLastDone, timeUnits);

        final ArrayList<CheckBox> cbChanges = new ArrayList<>(16);

        for (int i = 0; i < changes.length; i++) {
            CheckBox cb = new CheckBox(this);
            cb.setText(changes[i]);
            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[]   = {R.color.teal_dark, R.color.white};
            CompoundButtonCompat.setButtonTintList(cb, new ColorStateList(states, colors));
            cb.setTextColor(ContextCompat.getColor(this, R.color.text_color_dark));
            cb.setTextSize(16.0f);
            llChanges.addView(cb);
            cbChanges.add(cb);
        }

        if (item.getDiseaseName().equals("Other")) {
            tvTestLastDone.setText(AssetReader.getLangKeyword("last_time_test_done_other") + "?");
            tvInformedAboutDisease.setText(AssetReader.getLangKeyword("doctor_told_about_disease_other") + "?");
            tvMedication.setText(AssetReader.getLangKeyword("do_you_take_medications_for_other") + "?");
            tvChanges.setText(AssetReader.getLangKeyword("what_changes_have_you_made_to_control_other") + "?");

        } else {
            tvTestLastDone.setText(AssetReader.getLangKeyword("last_time_test_done") + " " + item.getDiseaseLang() + "?");
            tvInformedAboutDisease.setText(AssetReader.getLangKeyword("doctor_told_about_disease") + " " + item.getDiseaseLang() + "?");
            tvMedication.setText(AssetReader.getLangKeyword("do_you_take_medications_for_it") + " " + item.getDiseaseLang() + "?");
            tvChanges.setText(AssetReader.getLangKeyword("what_changes_have_you_made_to_control") + " " + item.getDiseaseLang() + "?");
        }

        etTestLastDone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                item.setTestDoneTime(editable.toString());
            }
        });

        etTestLastDone.setText(item.getTestDoneTime());

        spTestLastDone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }
                item.setTestDoneTimeUnit(timeUnitsEnglish[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        int indexTime = LUtils.getItemWithOthersIndex(item.getTestDoneTimeUnit(), timeUnitsEnglish);
        if (0 <= indexTime) {
            spTestLastDone.setSelection(indexTime);
        } else {
            spTestLastDone.setSelection(2);
        }

        rgInformedAboutDisease.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgInformedAboutDisease.indexOfChild(rgInformedAboutDisease.findViewById(indexId));

                if (0 == selectedIndex) {
                    tvMedication.setVisibility(VISIBLE);
                    rgMedication.setVisibility(VISIBLE);
                    tvChanges.setVisibility(VISIBLE);
                    llChangesWrapper.setVisibility(VISIBLE);
                    item.setInformedAboutDisease("Yes");

                } else if (1 == selectedIndex) {
                    clearItems(item, tvMedication, rgMedication, tvChanges, llChangesWrapper, cbChanges, etChanges);
                    item.setInformedAboutDisease("No");

                } else {
                    clearItems(item, tvMedication, rgMedication, tvChanges, llChangesWrapper, cbChanges, etChanges);
                    item.setInformedAboutDisease("Don't know");
                }
            }
        });

        if (hasValue(item.getInformedAboutDisease())) {

            if (item.getInformedAboutDisease().equals("Don't know")) {
                clearItems(item, tvMedication, rgMedication, tvChanges, llChangesWrapper, cbChanges, etChanges);
                ((RadioButton) rgInformedAboutDisease.getChildAt(2)).setChecked(true);

            } else if (item.getInformedAboutDisease().equals("No")) {
                clearItems(item, tvMedication, rgMedication, tvChanges, llChangesWrapper, cbChanges, etChanges);
                ((RadioButton) rgInformedAboutDisease.getChildAt(1)).setChecked(true);

            } else {
                tvMedication.setVisibility(VISIBLE);
                rgMedication.setVisibility(VISIBLE);
                tvChanges.setVisibility(VISIBLE);
                llChangesWrapper.setVisibility(VISIBLE);
                ((RadioButton) rgInformedAboutDisease.getChildAt(0)).setChecked(true);
            }
        }

        rgMedication.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgMedication.indexOfChild(rgMedication.findViewById(indexId));

                if (0 == selectedIndex) {
                    item.setMedications("Yes");
                } else {
                    item.setMedications("No");
                }
            }
        });

        if (hasValue(item.getMedications())) {
            if (item.getMedications().equals("Yes")) {
                ((RadioButton) rgMedication.getChildAt(0)).setChecked(true);
            } else {
                ((RadioButton) rgMedication.getChildAt(1)).setChecked(true);
            }
        }

        for (int i = 0; i < cbChanges.size(); i++) {
            final int index1   = i;
            CheckBox  checkBox = cbChanges.get(i);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    item.getChangesChecked()[index1] = b;

                    if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                        if (compoundButton.isChecked()) {
                            etChanges.setVisibility(VISIBLE);
                            etChanges.requestFocus();
                        } else {
                            etChanges.setVisibility(View.GONE);
                            etChanges.setText("");
                        }
                    }
                }
            });
        }

        for (int i = 0; i < item.getChangesChecked().length; i++) {
            if (item.getChangesChecked()[i]) {
                cbChanges.get(i).setChecked(true);
            }
        }

        etChanges.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                item.setChangesOther(editable.toString());
            }
        });

        etChanges.setText(item.getChangesOther());

        if (hasValue(item.getChangesOther())) {
            etChanges.setVisibility(VISIBLE);
        } else {
            etChanges.setVisibility(View.GONE);
        }

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                inputMethodManager.hideSoftInputFromWindow(etTestLastDone.getWindowToken(), 0);
                alertDialogDisease.dismiss();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollToView(scrollView, tvDiseases);

                    }
                }, 300);
            }
        });

        builder.setPositiveButton(AssetReader.getLangKeyword("okay"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                diseaseItems.set(diseaseIndex, item);

                inputMethodManager.hideSoftInputFromWindow(etTestLastDone.getWindowToken(), 0);
                alertDialogDisease.dismiss();
                scrollToView(scrollView, tvDiseases);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollToView(scrollView, tvDiseases);
                    }
                }, 300);
            }
        });

        alertDialogDisease = builder.create();
        alertDialogDisease.setCanceledOnTouchOutside(false);

        alertDialogDisease.show();

        alertDialogDisease.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        alertDialogDisease.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    private void clearItems(ItemDiseaseControl item, TextView tvMedication, RadioGroup rgMedication, TextView tvChanges,
                            LinearLayout llChangesWrapper, ArrayList<CheckBox> cbChanges, EditText etChanges) {

        tvMedication.setVisibility(View.GONE);
        rgMedication.setVisibility(View.GONE);
        tvChanges.setVisibility(View.GONE);
        llChangesWrapper.setVisibility(View.GONE);
        etChanges.setText("");

        item.setMedications("");
        item.setChangesOther("");

        rgMedication.clearCheck();

        for (CheckBox checkBox : cbChanges) {
            checkBox.setChecked(false);
        }
    }

    public void addDiseaseItem(String diseaseName, String diseaseLang) {
        ItemDiseaseControl itemDiseaseControl = new ItemDiseaseControl();
        itemDiseaseControl.setDiseaseName(diseaseName);
        itemDiseaseControl.setDiseaseLang(diseaseLang);
        itemDiseaseControl.setChangesChecked(new boolean[AssetReader.getSpinnerArray(KEY_CHANGES_TO_CONTROL_DISEASE).length]);
        diseaseItems.add(itemDiseaseControl);
    }

    public void removeDiseaseItem(String diseaseName) {
        int index = -1;

        for (int i = 0; i < diseaseItems.size(); i++) {
            if (diseaseItems.get(i).getDiseaseName().equals(diseaseName)) {
                index = i;
                break;
            }
        }

        if (0 <= index) {
            diseaseItems.remove(index);
        }
    }

    public ArrayList<ItemDiseaseControl> getDiseaseItems() {
        return diseaseItems;
    }

    public void setDiseaseItems(ArrayList<ItemDiseaseControl> items) {
        this.diseaseItems = items;
    }

    public static void scrollToView(final NestedScrollView scrollViewParent, final View view) {
        Point childOffset = new Point();
        getDeepChildOffset(scrollViewParent, view.getParent(), view, childOffset);
        scrollViewParent.scrollTo(0, childOffset.y);
    }

    private static void getDeepChildOffset(final ViewGroup mainParent, final ViewParent parent, final View child, final Point accumulatedOffset) {
        ViewGroup parentGroup = (ViewGroup) parent;
        accumulatedOffset.x += child.getLeft();
        accumulatedOffset.y += child.getTop();
        if (parentGroup.equals(mainParent)) {
            return;
        }
        getDeepChildOffset(mainParent, parentGroup.getParent(), parentGroup, accumulatedOffset);
    }

    private void hideKeyBoard() {
        inputMethodManager.hideSoftInputFromWindow(etDiseasesOther.getWindowToken(), 0);
    }
}