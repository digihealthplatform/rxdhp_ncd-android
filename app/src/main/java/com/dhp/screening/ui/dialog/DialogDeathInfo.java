package com.dhp.screening.ui.dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_AGE_UNITS;
import static com.dhp.screening.data.asset.AssetReader.KEY_DEATH_CAUSES;
import static com.dhp.screening.data.asset.AssetReader.KEY_GENDER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.getEnglishIndex;
import static com.dhp.screening.util.LUtils.hasValue;

public class DialogDeathInfo {

    private int age;

    private String ageUnit;
    private String personName;
    private String gender;
    private String relationToHoh;
    private String deathCause;

    private String[] relationsToHoh;
    private String[] relationsToHohEnglish;
    private String[] deathCauses;
    private String[] deathCausesEnglish;
    private String[] ageUnits;
    private String[] ageUnitsEnglish;

    private final String notes;

    private AlertDialog dialog;

    private Activity                 activity;
    private DialogDeathInfoCallbacks callbacks;

    public DialogDeathInfo(Activity activity, DialogDeathInfoCallbacks callbacks, TableDeathInfo item) {
        this.activity = activity;
        this.callbacks = callbacks;

        age = item.getAge();
        ageUnit = item.getAgeUnit();
        personName = item.getPersonName();
        gender = item.getGender();
        relationToHoh = item.getRelationToHoh();
        deathCause = item.getDeathCause();
        notes = item.getNotes();

        ageUnits = AssetReader.getSpinnerArray(KEY_AGE_UNITS);
        ageUnitsEnglish = AssetReader.getSpinnerArrayEnglish(KEY_AGE_UNITS);
        deathCauses = AssetReader.getSpinnerArray(KEY_DEATH_CAUSES);
        deathCausesEnglish = AssetReader.getSpinnerArrayEnglish(KEY_DEATH_CAUSES);

        relationsToHoh = AssetReader.getRelationsDeathInfo();
        relationsToHohEnglish = AssetReader.getRelationsDeathInfoEnglish();
    }

    public void showDialog(final int position) {
        final View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_death_info, null);

        final EditText etName            = (EditText) dialogView.findViewById(R.id.et_person_name);
        final EditText etAge             = (EditText) dialogView.findViewById(R.id.et_age);
        final EditText etRelationToHoh   = (EditText) dialogView.findViewById(R.id.et_relation_to_hh);
        final EditText etRelationOther   = (EditText) dialogView.findViewById(R.id.et_relation_other);
        final EditText etDeathCauseOther = (EditText) dialogView.findViewById(R.id.et_death_cause_other);
        final EditText etNotes           = (EditText) dialogView.findViewById(R.id.et_notes);

        final TextView tvPersonName = (TextView) dialogView.findViewById(R.id.tv_person_name);
        final TextView tvGender     = (TextView) dialogView.findViewById(R.id.tv_gender);
        final TextView tvAge        = (TextView) dialogView.findViewById(R.id.tv_age);
        final TextView tvRelation   = (TextView) dialogView.findViewById(R.id.tv_reation);
        final TextView tvDeathCause = (TextView) dialogView.findViewById(R.id.tv_death_cause);
        final TextView tvNotes      = (TextView) dialogView.findViewById(R.id.tv_notes);

        final RadioButton rbMale   = (RadioButton) dialogView.findViewById(R.id.rb_male);
        final RadioButton rbFemale = (RadioButton) dialogView.findViewById(R.id.rb_female);
        final RadioButton rbOther  = (RadioButton) dialogView.findViewById(R.id.rb_other);

        etName.setHint(AssetReader.getLangKeyword("enter"));
        etAge.setHint(AssetReader.getLangKeyword("enter"));
        etRelationToHoh.setHint(AssetReader.getLangKeyword("enter"));
        etRelationOther.setHint(AssetReader.getLangKeyword("if_other"));
        etDeathCauseOther.setHint(AssetReader.getLangKeyword("if_other"));
        etNotes.setHint(AssetReader.getLangKeyword("enter"));

        tvPersonName.setText(AssetReader.getLangKeyword("person_name") + " *");
        tvGender.setText(AssetReader.getLangKeyword("gender") + " *");
        tvAge.setText(AssetReader.getLangKeyword("age") + " *");
        tvRelation.setText(AssetReader.getLangKeyword("relation_to_head_of_house") + " *");
        tvDeathCause.setText(AssetReader.getLangKeyword("cause_of_death") + " *");
        tvNotes.setText(AssetReader.getLangKeyword("notes_title"));

        rbMale.setText(AssetReader.getLangKeyword("male"));
        rbFemale.setText(AssetReader.getLangKeyword("female"));
        rbOther.setText(AssetReader.getLangKeyword("other"));

        Spinner          spAgeUnit    = (Spinner) dialogView.findViewById(R.id.sp_age_unit);
        Spinner          spDeathCause = (Spinner) dialogView.findViewById(R.id.sp_death_cause);
        final RadioGroup rgGender     = (RadioGroup) dialogView.findViewById(R.id.rg_gender);

        LView.setSpinnerAdapter(activity, spAgeUnit, ageUnits);
        LView.setSpinnerAdapter(activity, spDeathCause, deathCauses);

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgGender.indexOfChild(rgGender.findViewById(indexId));
                gender = getEnglishIndex(KEY_GENDER_TYPES, selectedIndex);
            }
        });

        etRelationToHoh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DialogSearch(activity, new DialogSearchCallbacks() {
                    @Override
                    public void onSelected(String relation, int position) {

                        DialogDeathInfo.this.relationToHoh = relationsToHohEnglish[position];

                        etRelationToHoh.setText(relation);

                        if (DialogDeathInfo.this.relationToHoh.equals("Other")) {
                            etRelationOther.setVisibility(VISIBLE);
                        } else {
                            etRelationOther.setVisibility(View.GONE);
                        }
                    }
                }).withItems(relationsToHoh, null).showDialog();
            }
        });

        if (hasValue(personName)) {
            etName.setText(personName);
            etName.setSelection(etName.getText().length());
        }

        if (0 < age) {
            etAge.setText("" + age);
        }

        if (hasValue(gender)) {
            int index = LUtils.getGenderIndex(gender);

            if (0 <= index) {
                ((RadioButton) rgGender.getChildAt(index)).setChecked(true);
            }
        }

        if (hasValue(ageUnit)) {

            int index = LUtils.getItemWithOthersIndex(ageUnit, ageUnitsEnglish);
            spAgeUnit.setSelection(index);

        } else {
            spAgeUnit.setSelection(1);
        }

        spAgeUnit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }
                ageUnit = ageUnitsEnglish[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (hasValue(relationToHoh)) {
            etRelationToHoh.setText(relationToHoh);
        }

        if (hasValue(notes)) {
            etNotes.setText(notes);
        }

        spDeathCause.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }

                deathCause = deathCausesEnglish[position];

                if (deathCause.equals("Other")) {
                    etDeathCauseOther.setVisibility(VISIBLE);
                } else {
                    etDeathCauseOther.setText("");
                    etDeathCauseOther.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (hasValue(deathCause)) {

            int deathCauseIndex = LUtils.getItemWithOthersIndex(deathCause, deathCausesEnglish);

            if (deathCausesEnglish[deathCauseIndex].equals("Other")) {
                etDeathCauseOther.setText(deathCause);
            }

            spDeathCause.setSelection(deathCauseIndex);
        }

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(activity, R.style.AlertDialogLight);
        builder.setView(dialogView);
        builder.setTitle(AssetReader.getLangKeyword("details"));
        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    age = Integer.parseInt(etAge.getText().toString());
                } catch (Exception e) {
                }

                personName = etName.getText().toString();

                if (VISIBLE == etDeathCauseOther.getVisibility()) {
                    deathCause = etDeathCauseOther.getText().toString();
                }

                if (VISIBLE == etRelationOther.getVisibility()) {
                    relationToHoh = etRelationOther.getText().toString();
                }

                if (personName.equals("")) {
                    LToast.warning("Enter person name");

                } else if (!hasValue(gender)) {
                    LToast.warning("Select gender");

                } else if (1 > age) {
                    LToast.warning("Enter age");

                } else if (!hasValue(relationToHoh)) {
                    LToast.warning("Enter relation to head of house");

                } else if (!hasValue(deathCause)) {
                    LToast.warning("Enter death cause");

                } else {
                    TableDeathInfo tableDeathInfo = new TableDeathInfo();
                    tableDeathInfo.setPersonName(personName);
                    tableDeathInfo.setGender(gender);
                    tableDeathInfo.setAge(age);
                    tableDeathInfo.setAgeUnit(ageUnit);
                    tableDeathInfo.setRelationToHoh(relationToHoh);
                    tableDeathInfo.setDeathCause(deathCause);
                    tableDeathInfo.setNotes(etNotes.getText().toString());

                    callbacks.onDeathInfoChanged(tableDeathInfo, position);
                    dialog.dismiss();
                }
            }
        });
    }
}
