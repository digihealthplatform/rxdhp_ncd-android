package com.dhp.screening.ui.vital.hemoglobin;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemMinMax;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.ui.vital.ActivityVitalBase;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.HB_ALLOW_FREE_TEXT;
import static com.dhp.screening.data.asset.AssetReader.KEY_HB_VALUES;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_HB_CAPTURE;
import static com.dhp.screening.util.LConstants.HB_FOLDER_PATH;
import static com.dhp.screening.util.LUtils.formatFloat;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.hasValue;
import static com.dhp.screening.util.LView.resetRangeColor;


public class ActivityEnterHemoglobin extends ActivityVitalBase {

    private boolean isEdit;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private int age;

    private String gender;

    @Inject
    DataManager dataManager;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.sp_hemoglobin)
    Spinner spHemoglobin;

    @BindView(R.id.et_unit)
    EditText etUnit;

    @BindView(R.id.tv_range_text)
    TextView tvRangeText;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.ll_hb)
    LinearLayout llHb;

    @BindView(R.id.et_hb)
    EditText etHb;

    @BindView(R.id.btn_capture)
    Button btnCapture;

    private TableVitals tableVitals;

    private String registrationId;

    private String[] hbValues;

    private String hemoglobin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_hemoglobin);
        initToolbar(toolbar, getString(R.string.enter_hemoglobin), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.enter_hemoglobin));

        tableVitals = new TableVitals();

        registrationId = StaticData.getRegistrationId();

        if (AssetReader.toShow(HB_ALLOW_FREE_TEXT)) {
            etHb.setVisibility(VISIBLE);
            llHb.setVisibility(View.GONE);
        } else {
            etHb.setVisibility(View.GONE);
            llHb.setVisibility(VISIBLE);
        }

        if (AssetReader.isAllowSave("Hemoglobin")) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
            btnSave.setBackgroundResource(R.drawable.btn_background_disabled);
        }

        hbValues = AssetReader.getJsonSpinnerArray(KEY_HB_VALUES);
        LView.setSpinnerAdapter(this, spHemoglobin, hbValues);

        try {
            TableDemographics tableDemographics = dataManager.getPatient(registrationId);
            age    = LUtils.calculateAge(tableDemographics.getYob());
            gender = tableDemographics.getGender();

        } catch (Exception e) {
        }

        spHemoglobin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (0 == position) {
                    return;
                }
                hemoglobin = hbValues[position];
                showRange();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        etAadhaar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                showRange();
            }
        });

        etHb.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                hemoglobin = etHb.getText().toString().trim();
                showRange();
            }
        });

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit      = true;
            tableVitals = StaticData.getCurrentVital();

            int index = LUtils.getSpinnerIndexOther(tableVitals.getVitalValues(), hbValues);
            if (0 < index) {
                spHemoglobin.setSelection(index);
            }

            etNotes.setText(tableVitals.getNotes());
            btnSave.setText("Update");
        }

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
            tvName.setVisibility(VISIBLE);
        }

        if (!isEdit) {
            checkAlreadyEntered(registrationId, "Hemoglobin");
        }

        if (AssetReader.toShow(KEY_SHOW_HB_CAPTURE)) {
            btnCapture.setVisibility(VISIBLE);
        } else {
            btnCapture.setVisibility(View.GONE);
        }
    }

    private void showRange() {
        if (VISIBLE == llAadhaar.getVisibility()) {
            TableDemographics demographics = dataManager.getPatient(etAadhaar.getText().toString());

            if (null == demographics) {
                demographics = dataManager.getPatientByIdentity(etAadhaar.getText().toString());
            }

            if (null == demographics) {
                return;
            } else {
                age    = LUtils.calculateAge(demographics.getYob());
                gender = demographics.getGender();
            }
        }

        double value = 0.0;

        try {
            value = Double.valueOf(hemoglobin);
        } catch (Exception e) {
            resetRangeColor(etUnit);
            return;
        }

        ItemRange itemRange = AssetReader.getHbRange(value, gender, age);

        if (!itemRange.getColor().isEmpty()) {
            tvRangeText.setText(itemRange.getRange());
            etUnit.setBackgroundColor(Color.parseColor(itemRange.getColor()));
        } else {
            tvRangeText.setText("");
            resetRangeColor(etUnit);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showCapturedResult();
    }

    private void showCapturedResult() {
        String result = StaticData.getHbResult();

        if (null != result) {
            try {
                double hbValue = Double.parseDouble(result);

                int index = 0;

                for (int i = 1; i < hbValues.length; i++) {

                    double val = Double.parseDouble(hbValues[i]);

                    if (val == hbValue) {
                        index = i;
                        break;
                    }
                }

                if (index > 0) {
                    spHemoglobin.setSelection(index);
                }

            } catch (Exception e) {
            }

            etHb.setText(result);
            btnSave.setEnabled(true);
            btnSave.setBackgroundResource(R.drawable.btn_background);
            StaticData.saveResult(null);
        }
    }

    @OnClick(R.id.btn_capture)
    void capture() {
        StaticData.setCampDeviceInfo(getImagePrefix());
        startActivity(new Intent(this, ActivityStart.class));
    }

    @OnClick(R.id.btn_save)
    void onSave() {

        if (AssetReader.toShow(HB_ALLOW_FREE_TEXT)) {
            hemoglobin = etHb.getText().toString().trim();
        }

        if (!hasValue(hemoglobin)) {
            LToast.warning("Enter hemoglobin value");
        } else {
            if (isEdit) {
                tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

            } else {
                if (VISIBLE == llAadhaar.getVisibility()) {
                    registrationId = getRegistrationId();
                }
                if (null == registrationId || registrationId.equals("")) {
                    LToast.warning("Enter Registration ID");
                    return;
                }

                tableVitals.setRegistrationId(registrationId.toUpperCase());
            }
            String capturedImageName = null;

            try {
                capturedImageName = renameCapturedImage();
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }

            if (null != capturedImageName) {
                tableVitals.setVitalImage(capturedImageName);
            }

            float hbValue = 0.0f;

            try {
                hbValue = Float.parseFloat(hemoglobin);
            } catch (Exception e) {
            }

            ItemMinMax minMax = AssetReader.getVitalRange("Hemoglobin");
            if (hbValue < minMax.getMin() || hbValue > minMax.getMax()) {
                LToast.warningLong("Hemoglobin value should be between " + formatFloat(minMax.getMin()) + " g/dL - " + formatFloat(minMax.getMax()) + " g/dL");
                return;
            }

            tableVitals.setVitalType("Hemoglobin");

            tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
            tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

            tableVitals.setVitalSubTypes("Hemoglobin");

            tableVitals.setVitalValues(hemoglobin);
            tableVitals.setVitalUnits("g/dL");

            String notes = etNotes.getText().toString();

            if (null != StaticData.getLabValues()) {
                ArrayList<Value> labValues = StaticData.getLabValues();

                notes += "{" + String.format("%.2f", new BigDecimal(labValues.get(0).Lshades)) + "," +
                        String.format("%.2f", new BigDecimal(labValues.get(0).Avalues)) + "," +
                        String.format("%.2f", new BigDecimal(labValues.get(0).Bvalues)) + ";" +
                        String.format("%.2f", new BigDecimal(labValues.get(1).Lshades)) + "," +
                        String.format("%.2f", new BigDecimal(labValues.get(1).Avalues)) + "," +
                        String.format("%.2f", new BigDecimal(labValues.get(1).Bvalues)) + ";" +
                        String.format("%.2f", new BigDecimal(labValues.get(2).Lshades)) + "," +
                        String.format("%.2f", new BigDecimal(labValues.get(2).Avalues)) + "," +
                        String.format("%.2f", new BigDecimal(labValues.get(2).Bvalues)) + "}";

                StaticData.setLabValues(null);
            }

            tableVitals.setNotes(notes);
            tableVitals.setGpsLocation(getLocation());
            tableVitals.setQrScanned(qrScanned);

            if (isEdit) {
                dataManager.updateVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("updated"));
                StaticData.setCurrentVital(tableVitals);
            } else {
                dataManager.addVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("saved"));
            }

            finish();
        }
    }

    private String getImagePrefix() {
        return dataManager.getPartnerId()
                + "_"
                + dataManager.getDeviceNumber();
    }

    private String renameCapturedImage() {

        if (null == StaticData.capturedImageNames) {
            return null;
        }

        String capturedFileName = "";

        for (String fileName : StaticData.capturedImageNames) {

            if (hasValue(capturedFileName)) {
                capturedFileName += ";";
            }

            String toFileName = "hb_" + getImagePrefix()
                    + "_" + tableVitals.getRegistrationId()
                    + fileName;

            File fromFile = new File(Environment.getExternalStorageDirectory() + "/" + HB_FOLDER_PATH,
                    fileName);

            File toFile = new File(Environment.getExternalStorageDirectory() + "/" + HB_FOLDER_PATH,
                    toFileName);

            fromFile.renameTo(toFile);

            capturedFileName += toFileName;
        }

        StaticData.capturedImageNames = null;

        return capturedFileName;
    }
}