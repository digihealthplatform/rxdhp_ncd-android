package com.dhp.screening.ui.vital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.ALBUMIN_ALLOW_FREE_TEXT;
import static com.dhp.screening.data.asset.AssetReader.KEY_ALBUMIN_VALUES;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityEnterAlbumin extends ActivityVitalBase {

    private boolean isEdit;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.sp_albumin)
    Spinner spAlbumin;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.et_albumin)
    EditText etAlbumin;

    @BindView(R.id.ll_albumin)
    LinearLayout llAlbumin;

    private TableVitals tableVitals;

    private String registrationId;

    private String[] albuminValues;

    private String albumin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_albumin);
        initToolbar(toolbar, getString(R.string.albumin), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.albumin));

        tableVitals = new TableVitals();

        if (AssetReader.toShow(ALBUMIN_ALLOW_FREE_TEXT)) {
            etAlbumin.setVisibility(VISIBLE);
            llAlbumin.setVisibility(View.GONE);
        } else {
            etAlbumin.setVisibility(View.GONE);
            llAlbumin.setVisibility(VISIBLE);
        }

        if (AssetReader.isAllowSave("Albumin")) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
            btnSave.setBackgroundResource(R.drawable.btn_background_disabled);
        }

        albuminValues = AssetReader.getJsonSpinnerArray(KEY_ALBUMIN_VALUES);
        LView.setSpinnerAdapter(this, spAlbumin, albuminValues);

        spAlbumin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (0 == position) {
                    return;
                }
                albumin = albuminValues[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit = true;
            tableVitals = StaticData.getCurrentVital();

            int index = LUtils.getSpinnerIndexOther(tableVitals.getVitalValues(), albuminValues);
            if (0 < index) {
                spAlbumin.setSelection(index);
            }

            etNotes.setText(tableVitals.getNotes());
            btnSave.setText("Update");
        }

        registrationId = StaticData.getRegistrationId();

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        boolean popupShown = false;

        if (!isEdit) {
            popupShown = checkAlreadyEntered(registrationId, "Albumin");
        }

        if (!popupShown) {
            showPopupMessage("Albumin", "albumin");
        }
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        if (AssetReader.toShow(ALBUMIN_ALLOW_FREE_TEXT)) {
            albumin = etAlbumin.getText().toString().trim();
        }

        if (!hasValue(albumin)) {
            LToast.warning("Enter albumin value");

        } else {
            if (isEdit) {
                tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

            } else {
                if (VISIBLE == llAadhaar.getVisibility()) {
                    registrationId = getRegistrationId();
                }
                if (null == registrationId || registrationId.equals("")) {
                    LToast.warning("Enter Registration ID");
                    return;
                }
                tableVitals.setRegistrationId(registrationId.toUpperCase());
            }

            tableVitals.setVitalType("Albumin");

            tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
            tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

            tableVitals.setVitalSubTypes("Albumin");
            tableVitals.setVitalValues(albumin);
            tableVitals.setVitalUnits("g/dL");
            tableVitals.setNotes(etNotes.getText().toString());
            tableVitals.setGpsLocation(getLocation());
            tableVitals.setQrScanned(qrScanned);

            if (isEdit) {
                dataManager.updateVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("updated"));
                StaticData.setCurrentVital(tableVitals);
            } else {
                dataManager.addVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("saved"));
            }
            finish();
        }
    }
}