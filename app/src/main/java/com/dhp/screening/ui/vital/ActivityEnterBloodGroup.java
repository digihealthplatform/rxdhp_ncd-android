package com.dhp.screening.ui.vital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import static android.view.View.VISIBLE;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;

public class ActivityEnterBloodGroup extends ActivityVitalBase {

    private boolean isEdit;

    private String bloodGroup;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.sp_blood_group)
    Spinner spBloodGroup;

    @Inject
    DataManager dataManager;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    private TableVitals tableVitals;
    private String      registrationId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_blood_group);
        initToolbar(toolbar, getString(R.string.enter_blood_group), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.enter_blood_group));

        final String bloodGroups[] = getResources().getStringArray(R.array.blood_groups);
        setSpinnerAdapter(spBloodGroup, bloodGroups);

        tableVitals = new TableVitals();

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit = true;
            tableVitals = StaticData.getCurrentVital();

            int index = LUtils.getSpinnerIndexOther(tableVitals.getVitalValues(), bloodGroups);
            if (-1 != index) {
                spBloodGroup.setSelection(index);
            }

            etNotes.setText(tableVitals.getNotes());
            btnSave.setText("Update");
        }

        spBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position > 0) {
                    bloodGroup = bloodGroups[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        registrationId = StaticData.getRegistrationId();

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        boolean popupShown = false;

        if (!isEdit) {
            popupShown = checkAlreadyEntered(registrationId, "Blood Group");
        }

        if (!popupShown) {
            showPopupMessage("Blood Group", "blood_group");
        }
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        if (null == bloodGroup) {
            LToast.warning("Select blood group");
        } else {
            if (isEdit) {
                tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

            } else {
                if (VISIBLE == llAadhaar.getVisibility()) {
                    registrationId = getRegistrationId();
                }
                if (null == registrationId || registrationId.equals("")) {
                    LToast.warning("Enter Registration ID");
                    return;
                }
                tableVitals.setRegistrationId(registrationId.toUpperCase());
            }

            tableVitals.setVitalType("Blood Group");
            tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
            tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

            tableVitals.setVitalSubTypes("Blood Group");
            tableVitals.setVitalValues(bloodGroup);

            tableVitals.setNotes(etNotes.getText().toString());
            tableVitals.setGpsLocation(getLocation());
            tableVitals.setQrScanned(qrScanned);

            if (isEdit) {
                dataManager.updateVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("updated"));
                StaticData.setCurrentVital(tableVitals);
            } else {
                dataManager.addVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("saved"));
            }

            finish();
        }
    }
}