package com.dhp.screening.ui.vital.hemoglobin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.di.component.ActivityComponent;
import com.dhp.screening.data.di.component.DaggerActivityComponent;
import com.dhp.screening.data.di.module.ActivityModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_HB_WAIT_DURATION;
import static com.dhp.screening.util.LConstants.DEFAULT_DURATION;
import static com.dhp.screening.util.LConstants.TYPE_TEST;

public class ActivityTestStep2 extends BaseActivityTest {

    private int pendingTime = DEFAULT_DURATION;

    @BindView(R.id.btn_next)
    Button btnNext;

    @BindView(R.id.tv_duration)
    TextView tvDuration;

    @BindView(R.id.tv_description)
    TextView tvDescription;

    @BindView(R.id.tv_wait)
    TextView tvWait;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    private ActivityComponent activityComponent;

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(LApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_step_2);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Step 2", true);
        getActivityComponent(this).inject(this);

        pendingTime = dataManager.getIntPref(KEY_HB_WAIT_DURATION, AssetReader.getTestDuration());
    }

    @OnClick(R.id.btn_next)
    void onNext() {
        btnNext.setVisibility(View.GONE);
        tvDescription.setVisibility(View.GONE);
        tvDuration.setText("" + pendingTime);

        count();
    }

    private void count() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                --pendingTime;
                tvDuration.setText("" + pendingTime);

                if (0 == pendingTime) {
                    showNext();
                } else {
                    count();
                }
            }
        }, 1000);
    }

    private void showNext() {
        Intent intent = new Intent(this, ActivityCameraView.class);
        intent.putExtra("activity_type", TYPE_TEST);
        startActivity(intent);
        finish();
    }
}