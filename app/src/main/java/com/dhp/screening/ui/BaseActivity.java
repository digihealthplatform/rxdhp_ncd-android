package com.dhp.screening.ui;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterSpinner;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.di.component.ActivityComponent;
import com.dhp.screening.data.di.component.DaggerActivityComponent;
import com.dhp.screening.data.di.module.ActivityModule;
import com.dhp.screening.data.event.EventFilesRenamed;
import com.dhp.screening.data.event.EventOpenPicker;
import com.dhp.screening.data.event.EventPermissionGranted;
import com.dhp.screening.data.item.ItemFile;
import com.dhp.screening.data.item.ItemFileAttachment;
import com.dhp.screening.data.item.ItemLanguage;
import com.dhp.screening.network.ImageUploadTask;
import com.dhp.screening.network.JsonSyncTask;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.TaskCompressImage;
import com.dhp.screening.util.TaskMoveFiles;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import es.dmoral.toasty.Toasty;

import static com.dhp.screening.LApplication.doActionWithPermissionCheck;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_AUTO_SYNC;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_LAST_SYNC_MILLIS;
import static com.dhp.screening.util.LConstants.ACTION_OPEN_CAMERA;
import static com.dhp.screening.util.LConstants.ACTION_OPEN_FILE;
import static com.dhp.screening.util.LConstants.ACTION_OPEN_GALLERY;
import static com.dhp.screening.util.LConstants.CODE_LOCATION_PERMISSION;
import static com.dhp.screening.util.LConstants.REQUEST_CAPTURE;
import static com.dhp.screening.util.LConstants.REQUEST_FILE;
import static com.dhp.screening.util.LConstants.REQUEST_GALLERY;


public class BaseActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        TaskCompressImage.Callback,
        TaskMoveFiles.Callback,
        ResultCallback<LocationSettingsResult>, View.OnClickListener {

    private boolean locationRequestDialogShown;

    protected ActivityComponent activityComponent;

    protected static final String TAG = BaseActivity.class.getSimpleName();

    protected GoogleApiClient googleApiClient;

    protected Location        lastLocation;
    protected LocationRequest locationRequest;

    private int REQUEST_CHECK_SETTINGS = 100;

    protected boolean jsonSyncInProgress;

    private MediaPlayer audioPlayer;
    private ImageView   ivAudio;

    private AlertDialog alertDialogLanguage;

    private String selectedLanguage;

    private File capturedFile;

    private BottomSheetDialog dialogPicker;

    private ProgressDialog progressDialog;
    private boolean        isCamera;

    protected String getDeviceId() {
        return Settings.Secure.getString(LApplication.getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID).toUpperCase();
    }

    protected void initToolbar(Toolbar toolbar, String title, boolean setHomeUpEnabled) {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);

        setSupportActionBar(toolbar);

        if (setHomeUpEnabled) {
            try {
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            } catch (NullPointerException e) {
                LLog.printStackTrace(e);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(EventOpenPicker event) {
        this.isCamera = event.isCamera();
        onAttachFileClicked();
    }

    void onAttachFileClicked() {
        if (null != dialogPicker && dialogPicker.isShowing()) {
            return;
        }

        if (isCamera) {
            doActionWithPermissionCheck(this, ACTION_OPEN_CAMERA);
            return;
        }

        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.dialog_picker, null);

        dialogPicker = new BottomSheetDialog(this);
        dialogPicker.setContentView(dialogView);
        dialogPicker.show();

        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(((View) dialogView.getParent()));
        bottomSheetBehavior.setHideable(false);

        dialogView.findViewById(R.id.ll_file).setOnClickListener(this);

        dialogView.findViewById(R.id.ll_camera).setOnClickListener(this);
        dialogView.findViewById(R.id.ll_gallery).setOnClickListener(this);
    }

    protected void syncDataBackground(DataManager dataManager, JsonSyncTask.CallBack callBack) {
        if (!StaticData.imageUploadInProgress) {
            new ImageUploadTask(this, dataManager).execute();
        }

        if (jsonSyncInProgress) {
            return;
        }

        if (!dataManager.getBooleanPref(KEY_AUTO_SYNC, false)) {
            return;
        }

        long currentTimeMills = LUtils.getCurrentDateTimeMillis();
        long lastSyncMillis   = dataManager.getLongPref(KEY_LAST_SYNC_MILLIS, 0);

        long autoSyncInterval = AssetReader.getAutoSyncInterval();

        if (currentTimeMills - lastSyncMillis > autoSyncInterval) {
            JsonSyncTask jsonSyncTask = new JsonSyncTask(this, dataManager, true);
            jsonSyncTask.setCallback(callBack);
            jsonSyncTask.execute();
        }
    }

    protected void syncDataForeground(DataManager dataManager, JsonSyncTask.CallBack callBack) {
        if (!StaticData.imageUploadInProgress) {
            new ImageUploadTask(this, dataManager).execute();
        }

        if (jsonSyncInProgress) {
            LToast.normal(AssetReader.getLangKeyword("sync_in_progress"));
        } else {
            JsonSyncTask jsonSyncTask = new JsonSyncTask(this, dataManager, false);
            jsonSyncTask.setCallback(callBack);
            jsonSyncTask.execute();
        }
    }

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(LApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    protected void setSpinnerAdapter(Spinner spinner, String[] list) {
        final AdapterSpinner<String> adapterSpinner = new AdapterSpinner<>(
                this,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(list))
        );
        adapterSpinner.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(adapterSpinner);
    }

    protected void requestLocationWithPermissionCheck() {

        if (null != lastLocation) {
            return;
        }

        if (!checkPlayServices()) {
            LToast.warning("Play services not available grant location permission");
            return;
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    CODE_LOCATION_PERMISSION);

        } else {
            fetchLocation();
        }
    }

    protected void stopLocationRequest() {
        if (googleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case CODE_LOCATION_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLocation();
                } else {
                    locationRequestDialogShown = false;
                    LToast.errorLong(AssetReader.getLangKeyword("R.string.grant_location_permission"));
                }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    protected synchronized void fetchLocation() {
        if (null == locationRequest) {
            locationRequest = LocationRequest.create();
            locationRequest.setInterval(1000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        }

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (null != lastLocation) {
            return;
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(
                googleApiClient,
                builder.build());

        result.setResultCallback(this);
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
        final Status status = locationSettingsResult.getStatus();

        switch (status.getStatusCode()) {

            case LocationSettingsStatusCodes.SUCCESS:
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                try {
                    if (!locationRequestDialogShown) {
                        status.startResolutionForResult(BaseActivity.this, REQUEST_CHECK_SETTINGS);
                        locationRequestDialogShown = true;
                    }

                } catch (IntentSender.SendIntentException e) {
                    LLog.printStackTrace(e);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS) {

            if (resultCode == RESULT_OK) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);

            } else {
                locationRequestDialogShown = false;
                LToast.errorLong(AssetReader.getLangKeyword("grant_location_permission"));
            }
        }

        if (RESULT_OK != resultCode) {
            return;
        }

        switch (requestCode) {

            case REQUEST_FILE: {
                ArrayList<ItemFileAttachment> fileAttachments = new ArrayList<>();

                for (String filePath : data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS)) {
                    File tempFile = new File(filePath);

                    ItemFileAttachment itemFileAttachment = new ItemFileAttachment();

                    itemFileAttachment.setFileExtension(LUtils.getFileExtension(tempFile.getName()));
                    itemFileAttachment.setFileName(tempFile.getName());
                    itemFileAttachment.setFilePath(tempFile.getPath());

                    fileAttachments.add(itemFileAttachment);
                }

                if (!fileAttachments.isEmpty()) {
                    new TaskMoveFiles(fileAttachments, this).execute();
                    showLoaderBase("Please wait...");
                }

                break;
            }

            case REQUEST_GALLERY: {
                new TaskCompressImage(data.getData(), requestCode, this).execute();
                break;
            }

            case REQUEST_CAPTURE: {
                if (null != capturedFile && capturedFile.exists()) {
                    new TaskCompressImage(capturedFile.getPath(), requestCode, this, true).execute();
                    break;
                }
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {

    }

    @Override
    public void onConnectionSuspended(int cause) {
    }

    @Override
    public void onLocationChanged(Location location) {
        this.lastLocation = location;
        stopLocationRequest();
    }

    private boolean checkPlayServices() {
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        return resultCode == ConnectionResult.SUCCESS;
    }

    public String getLocation() {
        if (null != lastLocation) {
            return lastLocation.getLatitude() + "," + lastLocation.getLongitude();
        }
        return "";
    }

    protected void playAudio(String audioFileName, ImageView ivAudio) {
        stopAudio();

        final File audioFile = new File(Environment.getExternalStorageDirectory(), getString(R.string.audio_folder) + audioFileName);

        if (!audioFile.exists()) {
            Toasty.warning(this, AssetReader.getLangKeyword("audio_file_not_found")).show();
            return;
        }

        this.ivAudio = ivAudio;

        ivAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.playing_audio));
        audioPlayer = MediaPlayer.create(this, Uri.fromFile(audioFile));

        try {
            audioPlayer.start();
            audioPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    stopAudio();
                }
            });
        } catch (Exception e) {
        }
    }

    private void stopAudio() {
        if (null != audioPlayer && audioPlayer.isPlaying()) {
            audioPlayer.stop();
        }

        if (null != ivAudio) {
            ivAudio.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.play_audio));
        }
    }

    protected void changeLanguage(String currentLanguage) {
        if (null != alertDialogLanguage && alertDialogLanguage.isShowing()) {
            return;
        }

        selectedLanguage = currentLanguage;

        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_select_language, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setView(dialogView);

        builder.setTitle(AssetReader.getLangKeyword("select_language"));

        final ArrayList<ItemLanguage> languages = AssetReader.getLanguages();

        final RadioGroup rgLanguages = (RadioGroup) dialogView.findViewById(R.id.rg_languages);

        for (int i = 0; i < languages.size(); i++) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(languages.get(i).getLanguageTitle());
            radioButton.setId(i + 8888);
            radioButton.setTextSize(20);

            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 20, 0, 0);
            radioButton.setLayoutParams(params);

            rgLanguages.addView(radioButton);
        }

        rgLanguages.requestLayout();

        rgLanguages.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int index = rgLanguages.indexOfChild(dialogView.findViewById(indexId));

                if (-1 != index) {
                    selectedLanguage = languages.get(index).getLanguage();
                }
            }
        });

        int index = 0;

        for (int i = 0; i < languages.size(); i++) {
            if (languages.get(i).getLanguage().equals(currentLanguage)) {
                index = i;
                break;
            }
        }
        ((RadioButton) rgLanguages.getChildAt(index)).setChecked(true);

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel_lang"), null);

        builder.setPositiveButton(AssetReader.getLangKeyword("apply_lang"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                onLanguageSet(selectedLanguage);
            }
        });

        alertDialogLanguage = builder.create();
        alertDialogLanguage.setCanceledOnTouchOutside(false);

        alertDialogLanguage.show();
    }

    protected void onLanguageSet(String selectedLanguage) {
    }

    @Override
    public void onImageCompressed(ItemFile itemCompressedImage) {
        EventFilesRenamed event = new EventFilesRenamed();

        ArrayList<ItemFileAttachment> tmp = new ArrayList<>();

        ItemFileAttachment itemFileAttachment = new ItemFileAttachment();
        itemFileAttachment.setFileName(itemCompressedImage.getName());
        itemFileAttachment.setFilePath(itemCompressedImage.getLocalPath());
        itemFileAttachment.setCamera(itemCompressedImage.isCamera());

        tmp.add(itemFileAttachment);

        event.setOutputFilePaths(tmp);
        EventBus.getDefault().post(event);
    }

    @Override
    public void onCompressError() {
        hideLoaderBase();
        LToast.warning(AssetReader.getLangKeyword("something_went_wrong"));
    }

    @Override
    public void onClick(View view) {
        dialogPicker.dismiss();

        int action = 0;

        switch (view.getId()) {

            case R.id.ll_file: {
                action = ACTION_OPEN_FILE;
                break;
            }

            case R.id.ll_camera: {
                action = ACTION_OPEN_CAMERA;
                break;
            }

            case R.id.ll_gallery: {
                action = ACTION_OPEN_GALLERY;
                break;
            }
        }

        doActionWithPermissionCheck(this, action);
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(EventPermissionGranted event) {
        if (event.isPermissionGranted()) {
            doAction(event.getAction());
        }
    }

    private void doAction(int action) {

        switch (action) {

            case ACTION_OPEN_FILE: {
                openFilePicker();
                break;
            }

            case ACTION_OPEN_CAMERA: {
                openCamera();
                break;
            }

            case ACTION_OPEN_GALLERY: {
                openGallery();
                break;
            }
        }
    }

    private void openFilePicker() {
        FilePickerBuilder.getInstance().setMaxCount(1)
                .enableImagePicker(true)
                .setActivityTheme(R.style.AppThemePicker)
                .pickFile(this, REQUEST_FILE);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File filesPath = LUtils.getLocalFilesPath();

        capturedFile = new File(filesPath, "tmpfile_" + Calendar.getInstance().getTimeInMillis() + ".jpeg");

        Uri uriCapturedImage = Uri.fromFile(capturedFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uriCapturedImage);

        startActivityForResult(intent, REQUEST_CAPTURE);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    protected void showLoaderBase(String message) {
        progressDialog = new ProgressDialog(this, R.style.ProgressDialog);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        progressDialog.setMessage(message);
        progressDialog.show();
    }

    protected void hideLoaderBase() {
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onFilesMoved(ArrayList<ItemFileAttachment> outputFilePaths) {
        hideLoaderBase();

        EventFilesRenamed event = new EventFilesRenamed();
        event.setOutputFilePaths(outputFilePaths);
        EventBus.getDefault().post(event);
    }

    @Override
    public void onFileMoveError() {
        hideLoaderBase();
        LToast.warning(AssetReader.getLangKeyword("something_went_wrong"));
    }
}