package com.dhp.screening.ui.images;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.io.File;
import java.net.URLConnection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_INACTIVE_REASON;
import static com.dhp.screening.util.LUtils.getFormattedDateTimeAm;


public class ActivityImageDetails extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.ll_root)
    LinearLayout llRoot;

    @BindView(R.id.iv_attach)
    ImageView ivAttach;

    @BindView(R.id.tv_image_type)
    TextView tvImageType;

    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.tv_date_time)
    TextView tvDateTime;

    @Inject
    DataManager dataManager;

    private TableImage tableImage;

    private int tableKey;

    private String localPath;

    private String inactiveReason;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_details);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Image/Report", true);
        getActivityComponent(this).inject(this);

        if (savedInstanceState == null) {
            tableKey = getIntent().getIntExtra("table_key", 0);
        } else {
            tableKey = savedInstanceState.getInt("table_key");
        }

        if (0 == tableKey) {
            finish();
            return;
        }

        tableImage = dataManager.getImageTable(tableKey);
        localPath = LUtils.getLocalFilesPath() + "/" + tableImage.getImageName();

        RequestOptions requestOptions = new RequestOptions()
                .placeholder(R.drawable.file_document)
                .error(R.drawable.file_document);

        Glide.with(this)
                .load(localPath)
                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(requestOptions)
                .into(ivAttach);

        tvImageType.setText(tableImage.getImageType());
        tvNotes.setText(tableImage.getNotes());
        tvDateTime.setText(getFormattedDateTimeAm(tableImage.getImageDateTimeMillis()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_image, menu);

        if (tableImage.isInactive()) {
            menu.findItem(R.id.action_inactive).setChecked(true);
        }

        initLanguageMenu(menu);

        showBackground();

        return true;
    }

    private void showBackground() {
        if (tableImage.isInactive()) {
            llRoot.setBackgroundColor(ContextCompat.getColor(this, R.color.window_bg_inactive));
        } else {
            llRoot.setBackgroundColor(ContextCompat.getColor(this, R.color.window_bg));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_inactive:
                if (tableImage.isInactive()) {
                    confirmActive();
                } else {
                    confirmInactive();
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.iv_attach, R.id.tv_instruction})
    void onAttachFileClicked() {
        File file = new File(localPath);

        Intent intent = new Intent(Intent.ACTION_VIEW);

        intent.setDataAndType(
                Uri.fromFile(file),
                URLConnection.guessContentTypeFromName(file.getName()));

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("table_key", tableKey);
        super.onSaveInstanceState(outState);
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_inactive);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("inactive"));
        }
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(AssetReader.getLangKeyword("confirm_activate"));
        builder.setPositiveButton(AssetReader.getLangKeyword("activate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tableImage.setInactive(false);
                tableImage.setInactiveReason("");
                dataManager.updateImage(tableImage, getDeviceId());

                invalidateOptionsMenu();
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        builder.show();
    }

    private void confirmInactive() {
        inactiveReason = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("inactive_reason"));
        builder.setView(dialogView);

        final String[] reasonList = AssetReader.getSpinnerArray(KEY_INACTIVE_REASON);

        final Spinner spReason = (Spinner) dialogView.findViewById(R.id.sp_inactive_reason);
        LView.setSpinnerAdapter(this, spReason, reasonList);

        final EditText etReasonOther = (EditText) dialogView.findViewById(R.id.et_inactive_reason_other);
        etReasonOther.setHint(AssetReader.getLangKeyword("if_other"));

        spReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (0 < position) {
                    inactiveReason = reasonList[position];

                    if (inactiveReason.equals("Other")) {
                        etReasonOther.setVisibility(VISIBLE);
                    } else {
                        etReasonOther.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setPositiveButton(AssetReader.getLangKeyword("done"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (VISIBLE == etReasonOther.getVisibility()) {
                    inactiveReason = etReasonOther.getText().toString();
                }

                if (inactiveReason.isEmpty()) {
                    LToast.warning("Please enter the reason");
                    return;
                }

                tableImage.setInactive(true);
                tableImage.setInactiveReason(inactiveReason);
                dataManager.updateImage(tableImage, getDeviceId());

                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });
    }
}