package com.dhp.screening.ui.vital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemPrescriptionDetails;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import static android.view.View.VISIBLE;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;

public class ActivityEnterPrescription extends ActivityVitalBase {

    private boolean isEdit;

    private String food = "";

    private String newOrExisting = "";

    private String timings;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.et_prescription)
    EditText etPrescription;

    @BindView(R.id.et_days)
    EditText etDays;

    @BindView(R.id.rg_food)
    RadioGroup rgFood;

    @BindView(R.id.rg_new_or_existing)
    RadioGroup rgNewOrExisting;

    @BindView(R.id.cb_morning)
    CheckBox cbMorning;

    @BindView(R.id.cb_afternoon)
    CheckBox cbAfternoon;

    @BindView(R.id.cb_night)
    CheckBox cbNight;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @Inject
    DataManager dataManager;

    private TableVitals tableVitals;

    private String registrationId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_prescription);
        initToolbar(toolbar, getString(R.string.enter_prescription), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.enter_prescription));

        rgFood.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                food = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });

        rgNewOrExisting.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                newOrExisting = ((RadioButton) findViewById(indexId)).getText().toString();
            }
        });

        tableVitals = new TableVitals();

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit = true;
            tableVitals = StaticData.getCurrentVital();
            etNotes.setText(tableVitals.getNotes());

            ItemPrescriptionDetails prescriptionDetails = LUtils.getPrescriptionDetails(tableVitals.getVitalValues());

            etPrescription.setText(prescriptionDetails.getPrescription());
            etDays.setText("" + prescriptionDetails.getDays());

            food = prescriptionDetails.getFood();
            newOrExisting = prescriptionDetails.getNew_or_existing();

            if (prescriptionDetails.getTimings().contains("Morning")) {
                cbMorning.setChecked(true);
            }

            if (prescriptionDetails.getTimings().contains("Afternoon")) {
                cbAfternoon.setChecked(true);
            }

            if (prescriptionDetails.getTimings().contains("Night")) {
                cbNight.setChecked(true);
            }

            if (food.equals("Before food")) {
                ((RadioButton) rgFood.getChildAt(0)).setChecked(true);
            } else {
                ((RadioButton) rgFood.getChildAt(1)).setChecked(true);
            }

            if (newOrExisting.equals("New")) {
                ((RadioButton) rgNewOrExisting.getChildAt(0)).setChecked(true);
            } else if (newOrExisting.equals("Existing")) {
                ((RadioButton) rgNewOrExisting.getChildAt(1)).setChecked(true);
            }

            btnSave.setText("Update");
        }

        registrationId = StaticData.getRegistrationId();

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        boolean popupShown = false;

        if (!isEdit) {
            popupShown = checkAlreadyEntered(registrationId, "Prescription");

            String defaultPrescriptionType = AssetReader.getDefaultPrescriptionType();

            if (defaultPrescriptionType.equals("new")) {
                ((RadioButton) rgNewOrExisting.getChildAt(0)).setChecked(true);
            } else if (newOrExisting.equals("existing")) {
                ((RadioButton) rgNewOrExisting.getChildAt(1)).setChecked(true);
            }
        }

        if (!popupShown) {
            showPopupMessage("Prescription", "prescription");
        }
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        // Read values
        timings = "";

        if (cbMorning.isChecked()) {
            timings = "Morning";
        }

        if (cbAfternoon.isChecked()) {
            if (!timings.isEmpty()) {
                timings += ",";
            }
            timings += "Afternoon";
        }

        if (cbNight.isChecked()) {
            if (!timings.isEmpty()) {
                timings += ",";
            }
            timings += "Night";
        }

        if (etPrescription.getText().toString().isEmpty()) {
            LToast.warning("Enter prescription");

        } else if (etDays.getText().toString().isEmpty() || etDays.getText().toString().equals("0")) {
            LToast.warning("Enter days");

        } else if (timings.isEmpty()) {
            LToast.warning("Select timings");

        } else if (food.isEmpty()) {
            LToast.warning("Select before or after food");

        } else if (newOrExisting.isEmpty()) {
            LToast.warning("Select new or existing");

        } else {
            if (isEdit) {
                tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

            } else {
                if (VISIBLE == llAadhaar.getVisibility()) {
                    registrationId = getRegistrationId();
                }
                if (null == registrationId || registrationId.equals("")) {
                    LToast.warning("Enter Registration ID");
                    return;
                }
                tableVitals.setRegistrationId(registrationId.toUpperCase());
            }

            tableVitals.setVitalType("Prescription");
            tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
            tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

            tableVitals.setVitalSubTypes("Prescription,NewOrExisting,Timing,Food");

            int days = Integer.parseInt(etDays.getText().toString());

            String prescriptionJson = "{"
                    + "\"prescription\":" + "\"" + etPrescription.getText().toString() + "\","
                    + "\"new_or_existing\":" + newOrExisting + ","
                    + "\"days\":" + days + ","
                    + "\"timings\":" + "\"" + timings + "\","
                    + "\"food\":" + "\"" + food + "\""
                    + "}";

            tableVitals.setVitalValues(prescriptionJson);

            tableVitals.setNotes(etNotes.getText().toString());
            tableVitals.setGpsLocation(getLocation());
            tableVitals.setQrScanned(qrScanned);

            if (isEdit) {
                dataManager.updateVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("updated"));
                StaticData.setCurrentVital(tableVitals);
            } else {
                dataManager.addVital(tableVitals, getDeviceId());
                LToast.success(AssetReader.getLangKeyword("saved"));
            }

            finish();
        }
    }
}