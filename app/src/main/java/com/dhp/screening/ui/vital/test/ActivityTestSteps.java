package com.dhp.screening.ui.vital.test;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.adapter.AdapterPagination;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemVitalTest;
import com.dhp.screening.data.item.ItemVitalTestStep;
import com.dhp.screening.data.item.ItemVitalTestType;
import com.dhp.screening.ui.vital.ActivityVitalBase;
import com.dhp.screening.util.LView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ActivityTestSteps extends ActivityVitalBase {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_pic)
    ImageView ivPic;

    @BindView(R.id.tv_duration)
    TextView tvDuration;

    @BindView(R.id.iv_prev)
    ImageView ivPrev;

    @BindView(R.id.iv_next)
    ImageView ivNext;

    @BindView(R.id.rv_pagination)
    RecyclerView rvPagination;

    private String testType;
    private String testSubType;

    private int currentIndex;

    private AdapterPagination adapterPagination;

    private ItemVitalTestType vitalTestType;

    private boolean isTimerStarted;
    private boolean isTimerComplete;

    private long timerDuration;

    private CountDownTimer countDownTimer;

    private Handler handler = new Handler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_test_steps);
        ButterKnife.bind(this);

        testType = getIntent().getStringExtra("test_type");
        testSubType = getIntent().getStringExtra("test_sub_type");

        initToolbar(toolbar, testType, true);

        ItemVitalTest vitalTest = AssetReader.getVitalTest(testType);

        if (null == vitalTest) {
            finish();
            return;
        }

        for (ItemVitalTestType testType : vitalTest.getTestTypes()) {
            if (testType.getTestType().equals(testSubType)) {
                vitalTestType = testType;
            }
        }

        if (null == vitalTestType) {
            finish();
            return;
        }

        adapterPagination = new AdapterPagination(this, vitalTestType.getTestSteps().length);
        rvPagination.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvPagination.setAdapter(adapterPagination);

        showCurrentStep();
    }

    private void showCurrentStep() {
        adapterPagination.setSelection(currentIndex);

        if (0 == currentIndex) {
            ivPrev.setVisibility(View.GONE);
        } else {
            ivPrev.setVisibility(View.VISIBLE);
        }

        ItemVitalTestStep currentStep = vitalTestType.getTestSteps()[currentIndex];

        if (0 < currentStep.getDuration()) {
            ivNext.setVisibility(View.GONE);

            if (!isTimerStarted) {
                timerDuration = currentStep.getDuration();
                startTimer();
                isTimerStarted = true;
            }

            if (isTimerComplete) {
                ivNext.setVisibility(View.VISIBLE);
            }

            tvDuration.setVisibility(View.VISIBLE);
            ivPic.setVisibility(View.GONE);

        } else {
            tvDuration.setVisibility(View.GONE);
            ivPic.setVisibility(View.VISIBLE);
            LView.setImageFromFile(ivPic, currentStep.getImage());
            ivNext.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.iv_prev)
    void onPrevClicked() {
        if (0 == currentIndex) {
            return;
        }

        --currentIndex;
        showCurrentStep();
    }

    @OnClick(R.id.iv_next)
    void onNexClicked() {
        if ((vitalTestType.getTestSteps().length - 1) == currentIndex) {
            Intent intent = new Intent(this, ActivityTestResult.class);
            intent.putExtra("test_type", testType);
            intent.putExtra("test_sub_type", testSubType);
            startActivity(intent);
            finish();
            return;
        }

        ++currentIndex;
        showCurrentStep();
    }

    public void startTimer() {
        updateCountDownText(tvDuration);
        handler.postDelayed(new Runnable() {
            public void run() {
                timerDuration -= 1000;
                if (timerDuration <= 0) {
                    timerDuration = 0;
                    isTimerComplete = true;
                    ivNext.setVisibility(View.VISIBLE);
                    tvDuration.setText("Continue");
                } else {
                    updateCountDownText(tvDuration);
                    handler.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }

    public void updateCountDownText(TextView tvTimer) {
        tvTimer.setText("Wait..." + "\n" + getTimeFormatted());
    }

    private String getTimeFormatted() {
        int minutes = (int) timerDuration / 60000;
        int seconds = (int) (timerDuration % 60000) / 1000;

        return String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
    }

    public void resetTimer() {
        countDownTimer.cancel();
    }
}