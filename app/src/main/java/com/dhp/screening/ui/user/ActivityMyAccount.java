package com.dhp.screening.ui.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.ui.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.screening.data.asset.AssetReader.KEY_USER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.getSelectedLanguageVersion;


public class ActivityMyAccount extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_user_name)
    TextView tvUserName;

    @BindView(R.id.tv_user_type)
    TextView tvUserType;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        initToolbar(toolbar, AssetReader.getLangKeyword("my_account"), true);
        ButterKnife.bind(this);

        getActivityComponent(this).inject(this);

        TableUser tableUser = dataManager.getLoggedInUser();

        tvUserName.setText(AssetReader.getLangKeyword("name") + " " + tableUser.getFullName());
        tvUserType.setText(AssetReader.getLangKeyword("user_type") + " " + getSelectedLanguageVersion(tableUser.getUserType(), KEY_USER_TYPES));
    }
}