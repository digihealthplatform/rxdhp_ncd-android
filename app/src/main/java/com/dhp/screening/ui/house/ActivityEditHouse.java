package com.dhp.screening.ui.house;

import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterDeathInfo;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_AGE_UNITS;
import static com.dhp.screening.data.asset.AssetReader.KEY_BATHING_TOILET_FACILITY;
import static com.dhp.screening.data.asset.AssetReader.KEY_COOKING_FUELS;
import static com.dhp.screening.data.asset.AssetReader.KEY_DRINKING_WATER_CLEANING_METHODS;
import static com.dhp.screening.data.asset.AssetReader.KEY_ELECTRICITY_AVAILABILITY;
import static com.dhp.screening.data.asset.AssetReader.KEY_HOUSE_OWNERSHIP_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_HOUSE_STATUS;
import static com.dhp.screening.data.asset.AssetReader.KEY_INSURANCE;
import static com.dhp.screening.data.asset.AssetReader.KEY_MEAL_TIMINGS;
import static com.dhp.screening.data.asset.AssetReader.KEY_PETS_BITE_ACTION;
import static com.dhp.screening.data.asset.AssetReader.KEY_RATION;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_HOUSE_EXTRA_QUESTIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_STATE;
import static com.dhp.screening.data.asset.AssetReader.KEY_TOILET_AVAILABILITY;
import static com.dhp.screening.data.asset.AssetReader.KEY_TREATMENT_PLACE;
import static com.dhp.screening.data.asset.AssetReader.KEY_VACCINATION_RECORDS_STORAGE;
import static com.dhp.screening.data.asset.AssetReader.KEY_VEHICLES;
import static com.dhp.screening.data.asset.AssetReader.KEY_WATER_SOURCE;
import static com.dhp.screening.data.asset.AssetReader.getEnglishIndex;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_DISTRICT;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_GRAM_PANCHAYAT;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_STATE;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_VILLAGE;
import static com.dhp.screening.util.LUtils.hasValue;
import static com.dhp.screening.util.LUtils.isOther;


public class ActivityEditHouse extends BaseActivity
        implements RadioGroup.OnCheckedChangeListener,
        AdapterView.OnItemSelectedListener,
        CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_house_number)
    TextView tvHouseNumber;

    @BindView(R.id.tv_street)
    TextView tvStreet;

    @BindView(R.id.tv_post)
    TextView tvPost;

    @BindView(R.id.tv_locality)
    TextView tvLocality;

    @BindView(R.id.tv_village)
    TextView tvVillage;

    @BindView(R.id.tv_gram_panchayat)
    TextView tvGramPanchayat;

    @BindView(R.id.tv_district)
    TextView tvDistrict;

    @BindView(R.id.tv_state)
    TextView tvState;

    @BindView(R.id.tv_country)
    TextView tvCountry;

    @BindView(R.id.tv_pin_code)
    TextView tvPincode;

    @BindView(R.id.tv_head_of_house)
    TextView tvHeadOfHouse;

    @BindView(R.id.tv_head_of_house_spouse)
    TextView tvSpouseName;

    @BindView(R.id.tv_respondent)
    TextView tvRespondent;

    @BindView(R.id.tv_total_members)
    TextView tvTotalMembers;

    @BindView(R.id.tv_total_rooms)
    TextView tvTotalRooms;

    @BindView(R.id.tv_house_ownership)
    TextView tvOwnership;

    @BindView(R.id.tv_house_status)
    TextView tvHouseStatus;

    @BindView(R.id.tv_ration_card)
    TextView tvRationCard;

    @BindView(R.id.tv_health_insurance)
    TextView tvHealthInsurance;

    @BindView(R.id.tv_meal_timings)
    TextView tvMealTimings;

    @BindView(R.id.tv_cooking_fuel)
    TextView tvCookingFuel;

    @BindView(R.id.tv_toilet_availability)
    TextView tvToiletAvailability;

    @BindView(R.id.tv_electricity_availability)
    TextView tvElectricityAvailability;

    @BindView(R.id.tv_water_source)
    TextView tvWaterSource;

    @BindView(R.id.tv_vehicle)
    TextView tvVehicle;

    @BindView(R.id.tv_treatment_place)
    TextView tvTreatmentPlace;

    @BindView(R.id.tv_any_death)
    TextView tvAnyDeath;

    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.et_house_number)
    EditText etHouseNumber;

    @BindView(R.id.et_street)
    EditText etStreet;

    @BindView(R.id.et_post)
    EditText etPost;

    @BindView(R.id.et_locality)
    EditText etLocality;

    @BindView(R.id.et_village)
    EditText etVillage;

    @BindView(R.id.et_gram_panchayat)
    EditText etGramPanchayat;

    @BindView(R.id.et_district)
    EditText etDistrict;

    @BindView(R.id.et_state)
    AutoCompleteTextView etState;

    @BindView(R.id.et_country)
    EditText etCountry;

    @BindView(R.id.et_pin)
    EditText etPin;

    @BindView(R.id.et_head_of_house)
    EditText etHeadOfHouse;

    @BindView(R.id.et_spouse_name)
    EditText etSpouseName;

    @BindView(R.id.et_respondent)
    EditText etRespondent;

    @BindView(R.id.et_total_members)
    EditText etTotalMembers;

    @BindView(R.id.et_total_rooms)
    EditText etTotalRooms;

    @BindView(R.id.et_house_status_other)
    EditText etHouseStatusOther;

    @BindView(R.id.et_ration_other)
    EditText etRationOther;

    @BindView(R.id.et_insurance_other)
    EditText etInsuranceOther;

    @BindView(R.id.et_treatment_other)
    EditText etTreatmentOther;

    @BindView(R.id.rg_ration)
    RadioGroup rgRation;

    @BindView(R.id.rb_ration_yes)
    RadioButton rbRationYes;

    @BindView(R.id.rb_ration_no)
    RadioButton rbRationNo;

    @BindView(R.id.sp_ration)
    Spinner spRation;

    @BindView(R.id.rg_insurance)
    RadioGroup rgInsurance;

    @BindView(R.id.rb_insurance_yes)
    RadioButton rbInsuranceYes;

    @BindView(R.id.rb_insurance_no)
    RadioButton rbInsuranceNo;

    @BindView(R.id.sp_insurance)
    Spinner spInsurance;

    @BindView(R.id.sp_house_status)
    Spinner spHouseStatus;

    @BindView(R.id.ll_meal_timings)
    LinearLayout llMealTimings;

    @BindView(R.id.ll_cooking_fuels)
    LinearLayout llCookingFuels;

    @BindView(R.id.ll_toilet_availability)
    LinearLayout llToiletAvailability;

    @BindView(R.id.ll_electricity_availability)
    LinearLayout llElectricityAvailability;

    @BindView(R.id.ll_water_source)
    LinearLayout llWaterSource;

    @BindView(R.id.ll_vehicle)
    LinearLayout llVehicle;

    @BindView(R.id.et_cooking_fuel_other)
    EditText etCookingFuelOther;

    @BindView(R.id.et_toilet_availability_other)
    EditText etToiletAvailabilityOther;

    @BindView(R.id.et_electricity_availability_other)
    EditText etElectricityAvailabilityOther;

    @BindView(R.id.et_water_source_other)
    EditText etWaterSourceOther;

    @BindView(R.id.et_vehicle_other)
    EditText etVehicleOther;

    @BindView(R.id.rg_ownership)
    RadioGroup rgOwnership;

    @BindView(R.id.rb_own)
    RadioButton rbOwn;

    @BindView(R.id.rb_rented)
    RadioButton rbRented;

    @BindView(R.id.sp_treatment)
    Spinner spTreatment;

    @BindView(R.id.rg_recent_death)
    RadioGroup rgRecentDeath;

    @BindView(R.id.rb_recent_death_yes)
    RadioButton rbRecentDeathYes;

    @BindView(R.id.rb_recent_death_no)
    RadioButton rbRecentDeathNo;

    @BindView(R.id.tv_death_info)
    TextView tvDeathInfo;

    @BindView(R.id.rv_death_info)
    RecyclerView rvDeathInfo;

    @BindView(R.id.btn_add_more_death_info)
    Button btnAddMoreDeathInfo;

    @BindView(R.id.tv_total_earning_members)
    TextView tvTotalEarningMembers;

    @BindView(R.id.et_total_earning_members)
    TextView etTotalEarningMembers;

    @BindView(R.id.tv_house_income)
    TextView tvHouseIncome;

    @BindView(R.id.et_house_income)
    TextView etHouseIncome;

    @BindView(R.id.tv_treatment_cost)
    TextView tvTreatmentCost;

    @BindView(R.id.et_treatment_cost)
    TextView etTreatmentCost;

    @BindView(R.id.tv_drink_weekly)
    TextView tvDrinkWeekly;

    @BindView(R.id.rg_drink_weekly)
    RadioGroup rgDrinkWeekly;

    @BindView(R.id.tv_drug_use)
    TextView tvDrugUse;

    @BindView(R.id.rg_drug_use)
    RadioGroup rgDrugUse;

    @BindView(R.id.tv_handicapped_present)
    TextView tvHandicappedPresent;

    @BindView(R.id.rg_handicapped_present)
    RadioGroup rgHandicappedPresent;

    @BindView(R.id.tv_handicapped_suffering)
    TextView tvHandicappedSuffering;

    @BindView(R.id.rg_handicapped_suffering)
    RadioGroup rgHandicappedSuffering;

    @BindView(R.id.et_handicapped_suffering)
    EditText etHandicappedSuffering;

    @BindView(R.id.tv_pets)
    TextView tvPets;

    @BindView(R.id.rg_pets)
    RadioGroup rgPets;

    @BindView(R.id.tv_pets_bitten)
    TextView tvPetsBitten;

    @BindView(R.id.rg_pets_bitten)
    RadioGroup rgPetsBitten;

    @BindView(R.id.tv_pets_bite_action)
    TextView tvPetsBiteAction;

    @BindView(R.id.ll_pets_bite_action)
    LinearLayout llPetsBiteAction;

    @BindView(R.id.ll_pets_bite_action_wrapper)
    LinearLayout llPetsBiteActionWrapper;

    @BindView(R.id.et_pets_bite_action_other)
    EditText etPetsBiteActionOther;

    @BindView(R.id.tv_drinking_water_cleaning_method)
    TextView tvDrinkingWaterCleaningMethod;

    @BindView(R.id.ll_drinking_water_cleaning_method_wrapper)
    LinearLayout llDrinkingWaterCleaningMethodWrapper;

    @BindView(R.id.ll_drinking_water_cleaning_method)
    LinearLayout llDrinkingWaterCleaningMethod;

    @BindView(R.id.et_drinking_water_cleaning_method)
    EditText etDrinkingWaterCleaningMethod;

    @BindView(R.id.tv_bathing_toilet_facility)
    TextView tvBathingToiletFacility;

    @BindView(R.id.ll_bathing_toilet_facility_wrapper)
    LinearLayout llBathingToiletFacilityWrapper;

    @BindView(R.id.ll_bathing_toilet_facility)
    LinearLayout llBathingToiletFacility;

    @BindView(R.id.et_bathing_toilet_facility)
    EditText etBathingToiletFacility;

    @BindView(R.id.tv_children_present)
    TextView tvChildrenPresent;

    @BindView(R.id.rg_children_present)
    RadioGroup rgChildrenPresent;

    @BindView(R.id.tv_child_health_checkup_last_done)
    TextView tvChildHealthCheckupLastDone;

    @BindView(R.id.ll_children_checkup)
    LinearLayout llChildrenCheckup;

    @BindView(R.id.et_child_health_checkup_last_done)
    EditText etChildHealthCheckupLastDone;

    @BindView(R.id.sp_child_health_checkup_last_done)
    Spinner spChildHealthCheckupLastDone;

    @BindView(R.id.tv_children_vaccinated)
    TextView tvChildrenVaccinated;

    @BindView(R.id.rg_children_vaccinated)
    RadioGroup rgChildrenVaccinated;

    @BindView(R.id.tv_vaccination_records_storage)
    TextView tvVaccinationRecordsStorage;

    @BindView(R.id.ll_vaccination_records_storage_wrapper)
    LinearLayout llVaccinationRecordsStorageWrapper;

    @BindView(R.id.ll_vaccination_records_storage)
    LinearLayout llVaccinationRecordsStorage;

    @BindView(R.id.et_vaccination_records_storage_other)
    EditText etVaccinationRecordsStorageOther;

    @BindView(R.id.tv_child_gender_responsibility)
    TextView tvChildGenderResponsibility;

    @BindView(R.id.rg_child_gender_responsibility)
    RadioGroup rgChildGenderResponsibility;

    @BindView(R.id.tv_preferred_child_gender)
    TextView tvPreferredChildGender;

    @BindView(R.id.rg_preferred_child_gender)
    RadioGroup rgPreferredChildGender;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Inject
    DataManager dataManager;

    private ArrayList<CheckBox> cbMealTimings               = new ArrayList<>(4);
    private ArrayList<CheckBox> cbCookingFuels              = new ArrayList<>(4);
    private ArrayList<CheckBox> cbToiletAvailability        = new ArrayList<>(4);
    private ArrayList<CheckBox> cbElectricityAvailability   = new ArrayList<>(4);
    private ArrayList<CheckBox> cbWaterSource               = new ArrayList<>(4);
    private ArrayList<CheckBox> cbVehicle                   = new ArrayList<>(4);
    private ArrayList<CheckBox> cbPetsBiteActions           = new ArrayList<>(4);
    private ArrayList<CheckBox> cbWaterCleaningMethods      = new ArrayList<>(4);
    private ArrayList<CheckBox> cbBathingToiletFacility     = new ArrayList<>(4);
    private ArrayList<CheckBox> cbVaccinationRecordsStorage = new ArrayList<>(4);

    private String[] states;
    private String[] rationTypes;
    private String[] insuranceTypes;
    private String[] treatmentPlaces;
    private String[] mealTimings;
    private String[] houseStatus;
    private String[] cookingFuels;
    private String[] toiletAvailability;
    private String[] electricityAvailability;
    private String[] waterSource;
    private String[] vehicles;
    private String[] petsBiteActions;
    private String[] waterCleaningMethods;
    private String[] bathingToiletFacilities;
    private String[] vaccinationRecordsStorage;

    private String[] rationTypesEnglish;
    private String[] insuranceTypesEnglish;
    private String[] treatmentPlacesEnglish;
    private String[] mealTimingsEnglish;
    private String[] houseStatusEnglish;
    private String[] cookingFuelsEnglish;
    private String[] toiletAvailabilityEnglish;
    private String[] electricityAvailabilityEnglish;
    private String[] waterSourceEnglish;
    private String[] vehiclesEnglish;
    private String[] petsBiteActionsEnglish;
    private String[] waterCleaningMethodsEnglish;
    private String[] bathingToiletFacilitiesEnglish;
    private String[] vaccinationRecordsStorageEnglish;

    private String[] timeUnits;
    private String[] timeUnitsEnglish;

    private AdapterDeathInfo adapterDeathInfo;

    private TableHouse tableHouse;

    private List<TableDeathInfo> tableDeathInfoList;

    private boolean isEdit;
    private boolean isScannedEdit;
    private boolean autoCheck;

    private String timeUnit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_house);
        initToolbar(toolbar, AssetReader.getLangKeyword("new_house"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        tableHouse = new TableHouse();
        tableDeathInfoList = new ArrayList<TableDeathInfo>();

        states = AssetReader.getSpinnerArray(KEY_STATE);

        rationTypes = AssetReader.getSpinnerArray(KEY_RATION);
        insuranceTypes = AssetReader.getSpinnerArray(KEY_INSURANCE);
        treatmentPlaces = AssetReader.getSpinnerArray(KEY_TREATMENT_PLACE);
        houseStatus = AssetReader.getSpinnerArray(KEY_HOUSE_STATUS);
        mealTimings = AssetReader.getCheckBoxArray(KEY_MEAL_TIMINGS);
        cookingFuels = AssetReader.getCheckBoxArray(KEY_COOKING_FUELS);
        toiletAvailability = AssetReader.getCheckBoxArray(KEY_TOILET_AVAILABILITY);
        electricityAvailability = AssetReader.getCheckBoxArray(KEY_ELECTRICITY_AVAILABILITY);
        waterSource = AssetReader.getCheckBoxArray(KEY_WATER_SOURCE);
        vehicles = AssetReader.getCheckBoxArray(KEY_VEHICLES);
        petsBiteActions = AssetReader.getCheckBoxArray(KEY_PETS_BITE_ACTION);
        waterCleaningMethods = AssetReader.getCheckBoxArray(KEY_DRINKING_WATER_CLEANING_METHODS);
        bathingToiletFacilities = AssetReader.getCheckBoxArray(KEY_BATHING_TOILET_FACILITY);
        vaccinationRecordsStorage = AssetReader.getCheckBoxArray(KEY_VACCINATION_RECORDS_STORAGE);

        rationTypesEnglish = AssetReader.getSpinnerArrayEnglish(KEY_RATION);
        insuranceTypesEnglish = AssetReader.getSpinnerArrayEnglish(KEY_INSURANCE);
        treatmentPlacesEnglish = AssetReader.getSpinnerArrayEnglish(KEY_TREATMENT_PLACE);
        houseStatusEnglish = AssetReader.getSpinnerArrayEnglish(KEY_HOUSE_STATUS);
        mealTimingsEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_MEAL_TIMINGS);
        cookingFuelsEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_COOKING_FUELS);
        toiletAvailabilityEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_TOILET_AVAILABILITY);
        electricityAvailabilityEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_ELECTRICITY_AVAILABILITY);
        waterSourceEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_WATER_SOURCE);
        vehiclesEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_VEHICLES);
        petsBiteActionsEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_PETS_BITE_ACTION);
        waterCleaningMethodsEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_DRINKING_WATER_CLEANING_METHODS);
        bathingToiletFacilitiesEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_BATHING_TOILET_FACILITY);
        vaccinationRecordsStorageEnglish = AssetReader.getCheckBoxArrayEnglish(KEY_VACCINATION_RECORDS_STORAGE);

        adapterDeathInfo = new AdapterDeathInfo(this);
        rvDeathInfo.setLayoutManager(new LinearLayoutManager(this));
        rvDeathInfo.setAdapter(adapterDeathInfo);

        ArrayAdapter<String> adapterStates = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, states);
        etState.setAdapter(adapterStates);

        initUi();
        populateUi();
    }

    private void initUi() {
        rgOwnership.setOnCheckedChangeListener(this);
        rgRation.setOnCheckedChangeListener(this);
        rgInsurance.setOnCheckedChangeListener(this);
        rgRecentDeath.setOnCheckedChangeListener(this);
        rgDrinkWeekly.setOnCheckedChangeListener(this);
        rgDrugUse.setOnCheckedChangeListener(this);
        rgHandicappedPresent.setOnCheckedChangeListener(this);
        rgHandicappedSuffering.setOnCheckedChangeListener(this);
        rgPets.setOnCheckedChangeListener(this);
        rgPetsBitten.setOnCheckedChangeListener(this);
        rgChildrenPresent.setOnCheckedChangeListener(this);
        rgChildrenVaccinated.setOnCheckedChangeListener(this);
        rgChildGenderResponsibility.setOnCheckedChangeListener(this);
        rgPreferredChildGender.setOnCheckedChangeListener(this);

        initCheckbox(mealTimings, 2222, llMealTimings, cbMealTimings);
        initCheckbox(cookingFuels, 8888, llCookingFuels, cbCookingFuels);
        initCheckbox(toiletAvailability, 4444, llToiletAvailability, cbToiletAvailability);
        initCheckbox(electricityAvailability, 3333, llElectricityAvailability, cbElectricityAvailability);
        initCheckbox(waterSource, 5555, llWaterSource, cbWaterSource);
        initCheckbox(vehicles, 6666, llVehicle, cbVehicle);
        initCheckbox(petsBiteActions, 12222, llPetsBiteAction, cbPetsBiteActions);
        initCheckbox(waterCleaningMethods, 13222, llDrinkingWaterCleaningMethod, cbWaterCleaningMethods);
        initCheckbox(bathingToiletFacilities, 14222, llBathingToiletFacility, cbBathingToiletFacility);
        initCheckbox(vaccinationRecordsStorage, 15222, llVaccinationRecordsStorage, cbVaccinationRecordsStorage);

        LView.setSpinnerAdapter(this, spRation, rationTypes);
        LView.setSpinnerAdapter(this, spInsurance, insuranceTypes);
        LView.setSpinnerAdapter(this, spTreatment, treatmentPlaces);
        LView.setSpinnerAdapter(this, spHouseStatus, houseStatus);

        spRation.setOnItemSelectedListener(this);
        spInsurance.setOnItemSelectedListener(this);
        spTreatment.setOnItemSelectedListener(this);
        spHouseStatus.setOnItemSelectedListener(this);

        if (!AssetReader.toShow(KEY_SHOW_HOUSE_EXTRA_QUESTIONS)) {
            hideExtraQuestions();
        }

        timeUnits = AssetReader.getSpinnerArray(KEY_AGE_UNITS);
        timeUnitsEnglish = AssetReader.getSpinnerArrayEnglish(KEY_AGE_UNITS);

        LView.setSpinnerAdapter(this, spChildHealthCheckupLastDone, timeUnits);

        spChildHealthCheckupLastDone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }
                timeUnit = timeUnitsEnglish[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spChildHealthCheckupLastDone.setSelection(2);
    }

    private void hideExtraQuestions() {
        tvTotalEarningMembers.setVisibility(View.GONE);
        etTotalEarningMembers.setVisibility(View.GONE);
        tvHouseIncome.setVisibility(View.GONE);
        etHouseIncome.setVisibility(View.GONE);
        tvTreatmentCost.setVisibility(View.GONE);
        etTreatmentCost.setVisibility(View.GONE);
        tvDrinkWeekly.setVisibility(View.GONE);
        rgDrinkWeekly.setVisibility(View.GONE);
        tvDrugUse.setVisibility(View.GONE);
        rgDrugUse.setVisibility(View.GONE);
        tvHandicappedPresent.setVisibility(View.GONE);
        rgHandicappedPresent.setVisibility(View.GONE);
        tvHandicappedSuffering.setVisibility(View.GONE);
        rgHandicappedSuffering.setVisibility(View.GONE);
        etHandicappedSuffering.setVisibility(View.GONE);
        tvPets.setVisibility(View.GONE);
        rgPets.setVisibility(View.GONE);
        tvPetsBitten.setVisibility(View.GONE);
        rgPetsBitten.setVisibility(View.GONE);
        tvPetsBiteAction.setVisibility(View.GONE);
        llPetsBiteActionWrapper.setVisibility(View.GONE);
        tvDrinkingWaterCleaningMethod.setVisibility(View.GONE);
        llDrinkingWaterCleaningMethodWrapper.setVisibility(View.GONE);
        tvBathingToiletFacility.setVisibility(View.GONE);
        llBathingToiletFacilityWrapper.setVisibility(View.GONE);
        tvChildrenPresent.setVisibility(View.GONE);
        rgChildrenPresent.setVisibility(View.GONE);
        tvChildHealthCheckupLastDone.setVisibility(View.GONE);
        llChildrenCheckup.setVisibility(View.GONE);
        tvChildrenVaccinated.setVisibility(View.GONE);
        rgChildrenVaccinated.setVisibility(View.GONE);
        tvVaccinationRecordsStorage.setVisibility(View.GONE);
        llVaccinationRecordsStorageWrapper.setVisibility(View.GONE);
        tvChildGenderResponsibility.setVisibility(View.GONE);
        rgChildGenderResponsibility.setVisibility(View.GONE);
        tvPreferredChildGender.setVisibility(View.GONE);
        rgPreferredChildGender.setVisibility(View.GONE);
    }

    private void populateUi() {
        if (null != StaticData.getAadhaarAttributes()) {
            LView.setTextFromXml(etHouseNumber, "house");
            LView.setTextFromXml(etStreet, "street");
            LView.setTextFromXml(etPost, "po", "vtc");
            LView.setTextFromXml(etLocality, "loc");
            LView.setTextFromXml(etDistrict, "dist");
            LView.setTextFromXml(etState, "state");
            LView.setTextFromXml(etPin, "pc");

            etCountry.setText(AssetReader.getDefaultCountry());
        }

        if (null != StaticData.getCurrentHouse()
                && getIntent().getBooleanExtra("edit", false)) {

            isEdit = true;

            isScannedEdit = getIntent().getBooleanExtra("is_scanned_edit", false);

            toolbar.setTitle(AssetReader.getLangKeyword("edit_house_info"));

            tableHouse = StaticData.getCurrentHouse();

            tableDeathInfoList = dataManager.getDeathInfoList(tableHouse.getHouseId());

            etHouseNumber.setText(tableHouse.getHouseNo());
            etStreet.setText(tableHouse.getStreet());

            etPost.setText(tableHouse.getPost());
            etLocality.setText(tableHouse.getLocality());
            etVillage.setText(tableHouse.getVillage());
            etGramPanchayat.setText(tableHouse.getGramPanchayat());
            etDistrict.setText(tableHouse.getDistrict());
            etCountry.setText(tableHouse.getCountry());

            etState.setText(tableHouse.getState());

            etPin.setText("" + tableHouse.getPinCode());
            etNotes.setText(tableHouse.getNotes());

            etHeadOfHouse.setText(tableHouse.getHeadOfHouse());
            etSpouseName.setText(tableHouse.getSpouseOfHeadOfHouse());
            etRespondent.setText(tableHouse.getRespondent());

            if (0 != tableHouse.getTotalMembers()) {
                etTotalMembers.setText("" + tableHouse.getTotalMembers());
            }

            if (0 != tableHouse.getTotalRooms()) {
                etTotalRooms.setText("" + tableHouse.getTotalRooms());
            }

            if (0 != tableHouse.getEarningMembers()) {
                etTotalEarningMembers.setText("" + tableHouse.getEarningMembers());
            }

            if (0 != tableHouse.getTotalIncome()) {
                etHouseIncome.setText("" + tableHouse.getTotalIncome());
            }

            if (0 != tableHouse.getTreatmentCost()) {
                etTreatmentCost.setText("" + tableHouse.getTreatmentCost());
            }

            if (tableHouse.getHouseOwnership().equals("Own")) {
                ((RadioButton) rgOwnership.getChildAt(0)).setChecked(true);

            } else if (tableHouse.getHouseOwnership().equals("Rented")) {
                ((RadioButton) rgOwnership.getChildAt(1)).setChecked(true);
            }

            if (hasValue(tableHouse.getRationCard())) {
                if (tableHouse.getRationCard().equals("No")) {
                    ((RadioButton) rgRation.getChildAt(1)).setChecked(true);

                } else {
                    autoCheck = true;
                    ((RadioButton) rgRation.getChildAt(0)).setChecked(true);

                    int spinnerIndex = LUtils.getSpinnerIndexOther(tableHouse.getRationCard(), rationTypesEnglish);
                    if (-1 != spinnerIndex) {
                        spRation.setSelection(spinnerIndex);

                        if (rationTypes[spinnerIndex].equals("Other")) {
                            etRationOther.setText(tableHouse.getRationCard());
                            etRationOther.setVisibility(VISIBLE);
                        }
                    }
                }
            }

            if (hasValue(tableHouse.getHealthInsurance())) {
                if (tableHouse.getHealthInsurance().equals("No")) {
                    ((RadioButton) rgInsurance.getChildAt(1)).setChecked(true);

                } else {
                    autoCheck = true;
                    ((RadioButton) rgInsurance.getChildAt(0)).setChecked(true);

                    int spinnerIndex = LUtils.getSpinnerIndexOther(tableHouse.getHealthInsurance(), insuranceTypesEnglish);
                    if (-1 != spinnerIndex) {
                        spInsurance.setSelection(spinnerIndex);

                        if (insuranceTypes[spinnerIndex].equals("Other")) {
                            etInsuranceOther.setText(tableHouse.getHealthInsurance());
                            etInsuranceOther.setVisibility(VISIBLE);
                        }
                    }
                }
            }

            if (hasValue(tableHouse.getTreatmentPlace())) {

                int spinnerIndex = LUtils.getSpinnerIndexOther(tableHouse.getTreatmentPlace(), treatmentPlacesEnglish);

                if (-1 != spinnerIndex) {
                    spTreatment.setSelection(spinnerIndex);

                    if (treatmentPlacesEnglish[spinnerIndex].equals("Other")) {
                        etTreatmentOther.setText(tableHouse.getTreatmentPlace());
                        etTreatmentOther.setVisibility(VISIBLE);
                    }
                }
            }

            if (hasValue(tableHouse.getHouseStatus())) {

                int spinnerIndex = LUtils.getSpinnerIndexOther(tableHouse.getHouseStatus(), houseStatusEnglish);

                if (-1 != spinnerIndex) {
                    spHouseStatus.setSelection(spinnerIndex);

                    if (houseStatusEnglish[spinnerIndex].equals("Other")) {
                        etHouseStatusOther.setText(tableHouse.getHouseStatus());
                        etHouseStatusOther.setVisibility(VISIBLE);
                    }
                }
            }

            String[] mealTimings = tableHouse.getMealTimes().split(";");
            populateCheckBox(mealTimings, this.mealTimingsEnglish, cbMealTimings, null);

            String[] cookingFuels = tableHouse.getCookingFuel().split(";");
            populateCheckBox(cookingFuels, this.cookingFuelsEnglish, cbCookingFuels, etCookingFuelOther);

            if (hasValue(tableHouse.getToiletAvailability())) {
                String[] toiletAvailability = tableHouse.getToiletAvailability().split(";");
                populateCheckBox(toiletAvailability, this.toiletAvailabilityEnglish, cbToiletAvailability, etToiletAvailabilityOther);
            }

            if (hasValue(tableHouse.getElectricityAvailability())) {
                String[] electricityAvailability = tableHouse.getElectricityAvailability().split(";");
                populateCheckBox(electricityAvailability, this.electricityAvailabilityEnglish, cbElectricityAvailability, etElectricityAvailabilityOther);
            }

            if (hasValue(tableHouse.getDrinkingWaterSource())) {
                String[] waterSource = tableHouse.getDrinkingWaterSource().split(";");
                populateCheckBox(waterSource, this.waterSourceEnglish, cbWaterSource, etWaterSourceOther);
            }

            if (hasValue(tableHouse.getMotorisedVehicle())) {
                String[] vehicle = tableHouse.getMotorisedVehicle().split(";");
                populateCheckBox(vehicle, this.vehiclesEnglish, cbVehicle, etVehicleOther);
            }

            if (hasValue(tableHouse.getRecentDeath())) {
                if (tableHouse.getRecentDeath().equals("Yes")) {
                    ((RadioButton) rgRecentDeath.getChildAt(0)).setChecked(true);

                } else if (tableHouse.getRecentDeath().equals("No")) {
                    ((RadioButton) rgRecentDeath.getChildAt(1)).setChecked(true);
                }
            }
            btnSave.setText(AssetReader.getLangKeyword("update"));

            if (hasValue(tableHouse.getDrinkWeekly())) {
                if (tableHouse.getDrinkWeekly().equals("Yes")) {
                    ((RadioButton) rgDrinkWeekly.getChildAt(0)).setChecked(true);

                } else if (tableHouse.getDrinkWeekly().equals("No")) {
                    ((RadioButton) rgDrinkWeekly.getChildAt(1)).setChecked(true);

                } else {
                    ((RadioButton) rgDrinkWeekly.getChildAt(2)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getDrugsUse())) {
                if (tableHouse.getDrugsUse().equals("Yes")) {
                    ((RadioButton) rgDrugUse.getChildAt(0)).setChecked(true);

                } else if (tableHouse.getDrugsUse().equals("No")) {
                    ((RadioButton) rgDrugUse.getChildAt(1)).setChecked(true);

                } else {
                    ((RadioButton) rgDrugUse.getChildAt(2)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getHandicappedPresent())) {
                if (tableHouse.getHandicappedPresent().equals("Yes")) {
                    ((RadioButton) rgHandicappedPresent.getChildAt(0)).setChecked(true);
                    showHandicappedSuffering();

                } else {
                    ((RadioButton) rgHandicappedPresent.getChildAt(1)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getHandicappedSuffering())) {
                if (tableHouse.getHandicappedSuffering().equals("Don't know")) {
                    ((RadioButton) rgHandicappedSuffering.getChildAt(2)).setChecked(true);

                } else if (tableHouse.getHandicappedSuffering().equals("No")) {
                    ((RadioButton) rgHandicappedSuffering.getChildAt(1)).setChecked(true);

                } else {
                    ((RadioButton) rgHandicappedSuffering.getChildAt(0)).setChecked(true);
                    etHandicappedSuffering.setVisibility(VISIBLE);
                    etHandicappedSuffering.setText(tableHouse.getHandicappedSuffering());
                }
            }

            if (hasValue(tableHouse.getPets())) {
                if (tableHouse.getPets().equals("Yes")) {
                    ((RadioButton) rgPets.getChildAt(0)).setChecked(true);
                    showPetBite();

                } else if (tableHouse.getPets().equals("No")) {
                    ((RadioButton) rgPets.getChildAt(1)).setChecked(true);

                } else {
                    ((RadioButton) rgPets.getChildAt(2)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getPetsBitten())) {
                if (tableHouse.getPetsBitten().equals("Yes")) {
                    ((RadioButton) rgPets.getChildAt(0)).setChecked(true);
                    showPetBiteAction();

                } else if (tableHouse.getPets().equals("No")) {
                    ((RadioButton) rgPets.getChildAt(1)).setChecked(true);

                } else {
                    ((RadioButton) rgPets.getChildAt(2)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getPetsBiteAction())) {
                String[] action = tableHouse.getPetsBiteAction().split(";");
                populateCheckBox(action, this.petsBiteActionsEnglish, cbPetsBiteActions, etPetsBiteActionOther);
            }

            if (hasValue(tableHouse.getDrinkingWaterCleaningMethod())) {
                String[] action = tableHouse.getDrinkingWaterCleaningMethod().split(";");
                populateCheckBox(action, this.waterCleaningMethodsEnglish, cbWaterCleaningMethods, etDrinkingWaterCleaningMethod);
            }

            if (hasValue(tableHouse.getBathingToiletFacility())) {
                String[] action = tableHouse.getBathingToiletFacility().split(";");
                populateCheckBox(action, this.bathingToiletFacilitiesEnglish, cbBathingToiletFacility, etBathingToiletFacility);
            }

            if (hasValue(tableHouse.getChildHealthCheckupLastDone())
                    || hasValue(tableHouse.getChildrenVaccinated())
                    || hasValue(tableHouse.getVaccinationRecordsStorage())) {
                ((RadioButton) rgChildrenPresent.getChildAt(0)).setChecked(true);
            }

            if (hasValue(tableHouse.getChildrenVaccinated())) {
                if (tableHouse.getChildrenVaccinated().equals("Yes")) {
                    ((RadioButton) rgChildrenVaccinated.getChildAt(0)).setChecked(true);
                    showPetBiteAction();

                } else if (tableHouse.getPets().equals("No")) {
                    ((RadioButton) rgChildrenVaccinated.getChildAt(1)).setChecked(true);

                } else {
                    ((RadioButton) rgChildrenVaccinated.getChildAt(2)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getVaccinationRecordsStorage())) {
                String[] action = tableHouse.getVaccinationRecordsStorage().split(";");
                populateCheckBox(action, this.vaccinationRecordsStorageEnglish, cbVaccinationRecordsStorage, etVaccinationRecordsStorageOther);
            }

            if (hasValue(tableHouse.getChildGenderResponsibility())) {

                if (tableHouse.getChildGenderResponsibility().equals("Father")) {
                    ((RadioButton) rgChildGenderResponsibility.getChildAt(0)).setChecked(true);
                    showPetBiteAction();

                } else if (tableHouse.getChildGenderResponsibility().equals("Mother")) {
                    ((RadioButton) rgChildGenderResponsibility.getChildAt(1)).setChecked(true);

                } else if (tableHouse.getChildGenderResponsibility().equals("Neither")) {
                    ((RadioButton) rgChildGenderResponsibility.getChildAt(2)).setChecked(true);

                } else {
                    ((RadioButton) rgChildGenderResponsibility.getChildAt(3)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getPreferredChildGender())) {
                if (tableHouse.getPreferredChildGender().equals("Boy")) {
                    ((RadioButton) rgPreferredChildGender.getChildAt(0)).setChecked(true);
                } else {
                    ((RadioButton) rgPreferredChildGender.getChildAt(1)).setChecked(true);
                }
            }

            if (hasValue(tableHouse.getChildHealthCheckupLastDone())) {
                try {
                    String[] parts = tableHouse.getChildHealthCheckupLastDone().split(";");

                    etChildHealthCheckupLastDone.setText(parts[0]);
                    timeUnit = parts[1];

                    int index = LUtils.getItemWithOthersIndex(timeUnit, timeUnitsEnglish);
                    spChildHealthCheckupLastDone.setSelection(index);

                } catch (Exception e) {
                    spChildHealthCheckupLastDone.setSelection(2);
                }
            } else {
                spChildHealthCheckupLastDone.setSelection(2);
            }

        } else {
            // Set default state and district
            etVillage.setText(dataManager.getStringPref(KEY_DEFAULT_VILLAGE, ""));
            etGramPanchayat.setText(dataManager.getStringPref(KEY_DEFAULT_GRAM_PANCHAYAT, ""));

            etDistrict.setText(dataManager.getStringPref(KEY_DEFAULT_DISTRICT, ""));
            etState.setText(dataManager.getStringPref(KEY_DEFAULT_STATE, ""));

            etCountry.setText(AssetReader.getDefaultCountry());
        }
    }

    @OnClick(R.id.btn_add_more_death_info)
    void addMoreDeathInfo() {
        adapterDeathInfo.addNewItem(false);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_go_back"));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityEditHouse.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @OnClick(R.id.btn_save)
    void onSave() {

        readValues();

        if (isAllValuesEntered()) {
            if (!((RadioButton) rgChildrenPresent.getChildAt(0)).isChecked()) {
                tableHouse.setChildHealthCheckupLastDone("");
                tableHouse.setChildrenVaccinated("");
                tableHouse.setVaccinationRecordsStorage("");
            }

            confirmSave();
        }
    }

    private void confirmSave() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));
        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isEdit && !isScannedEdit) {
                    dataManager.updateHouse(tableHouse, tableDeathInfoList);
                    Toasty.normal(ActivityEditHouse.this, AssetReader.getLangKeyword("house_info_updated")).show();
                } else {
                    dataManager.createHouse(tableHouse, tableDeathInfoList);
                    Toasty.normal(ActivityEditHouse.this, AssetReader.getLangKeyword("house_info_added")).show();

                    if (isScannedEdit) {
                        StaticData.setScannedHouseSaved(true);
                    }
                }

                finish();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void readValues() {
        tableHouse.setHouseNo(etHouseNumber.getText().toString());
        tableHouse.setStreet(etStreet.getText().toString());
        tableHouse.setPost(etPost.getText().toString());
        tableHouse.setLocality(etLocality.getText().toString());
        tableHouse.setVillage(etVillage.getText().toString());
        tableHouse.setGramPanchayat(etGramPanchayat.getText().toString());
        tableHouse.setDistrict(etDistrict.getText().toString());
        tableHouse.setCountry(etCountry.getText().toString());
        tableHouse.setState(etState.getText().toString());

        try {
            tableHouse.setPinCode(Integer.parseInt(etPin.getText().toString()));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        tableHouse.setHeadOfHouse(etHeadOfHouse.getText().toString());
        tableHouse.setSpouseOfHeadOfHouse(etSpouseName.getText().toString());
        tableHouse.setRespondent(etRespondent.getText().toString());

        try {
            tableHouse.setTotalMembers(Integer.parseInt(etTotalMembers.getText().toString()));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        try {
            tableHouse.setTotalRooms(Integer.parseInt(etTotalRooms.getText().toString()));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        try {
            tableHouse.setEarningMembers(Integer.parseInt(etTotalEarningMembers.getText().toString()));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        try {
            tableHouse.setTotalIncome(Integer.parseInt(etHouseIncome.getText().toString()));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        try {
            tableHouse.setTreatmentCost(Integer.parseInt(etTreatmentCost.getText().toString()));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (VISIBLE == etRationOther.getVisibility()) {
            tableHouse.setRationCard(etRationOther.getText().toString());
        }

        if (VISIBLE == etInsuranceOther.getVisibility()) {
            tableHouse.setHealthInsurance(etInsuranceOther.getText().toString());
        }

        tableHouse.setMealTimes(getCheckBoxValues(
                "meal_timings", cbMealTimings, null));

        tableHouse.setCookingFuel(getCheckBoxValues(
                "cooking_fuels", cbCookingFuels, etCookingFuelOther));

        tableHouse.setToiletAvailability(getCheckBoxValues(
                "toilet_availability", cbToiletAvailability, etToiletAvailabilityOther));

        tableHouse.setElectricityAvailability(getCheckBoxValues(
                "electricity_availability", cbElectricityAvailability, etElectricityAvailabilityOther));

        tableHouse.setDrinkingWaterSource(getCheckBoxValues(
                "drinking_water_source", cbWaterSource, etWaterSourceOther));

        tableHouse.setMotorisedVehicle(getCheckBoxValues(
                "motorised_vehicle", cbVehicle, etVehicleOther));

        tableHouse.setPetsBiteAction(getCheckBoxValues(
                "pets_bite_action", cbPetsBiteActions, etPetsBiteActionOther));

        tableHouse.setDrinkingWaterCleaningMethod(getCheckBoxValues(
                "drinking_water_cleaning_methods", cbWaterCleaningMethods, etDrinkingWaterCleaningMethod));

        tableHouse.setBathingToiletFacility(getCheckBoxValues(
                "bathing_toilet_facility", cbBathingToiletFacility, etBathingToiletFacility));

        tableHouse.setVaccinationRecordsStorage(getCheckBoxValues(
                "vaccination_records_storage", cbVaccinationRecordsStorage, etVaccinationRecordsStorageOther));

        if (VISIBLE == etTreatmentOther.getVisibility()) {
            tableHouse.setTreatmentPlace(etTreatmentOther.getText().toString());
        }

        if (VISIBLE == etHouseStatusOther.getVisibility()) {
            tableHouse.setHouseStatus(etHouseStatusOther.getText().toString());
        }

        if (VISIBLE == etHandicappedSuffering.getVisibility()) {
            tableHouse.setHandicappedSuffering(etHandicappedSuffering.getText().toString());
        }

        if (hasValue(etChildHealthCheckupLastDone.getText().toString().trim())) {
            tableHouse.setChildHealthCheckupLastDone(etChildHealthCheckupLastDone.getText().toString().trim() + ";" + timeUnit);
        }

        tableHouse.setNotes(etNotes.getText().toString());

        tableHouse.setGpsLocation(getLocation());

        tableDeathInfoList = adapterDeathInfo.getItems();
    }

    @Override
    protected void onStart() {
        super.onStart();
        requestLocationWithPermissionCheck();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationRequest();
    }

    private boolean isAllValuesEntered() {

        if (!hasValue(etPin.getText().toString())) {
            LToast.warning(AssetReader.getLangKeyword("enter_pin_code"));
            return false;

        } else if (VISIBLE == spRation.getVisibility() && !hasValue(tableHouse.getRationCard())) {
            LToast.warning(AssetReader.getLangKeyword("enter_ration_card"));
            return false;

        } else if (VISIBLE == spInsurance.getVisibility() && !hasValue(tableHouse.getHealthInsurance())) {
            LToast.warning(AssetReader.getLangKeyword("enter_insurance"));
            return false;

        } else if (VISIBLE == etTreatmentOther.getVisibility() && !hasValue(tableHouse.getTreatmentPlace())) {
            LToast.warning(AssetReader.getLangKeyword("enter_treatment_place"));
            return false;

        } else if (VISIBLE == etCookingFuelOther.getVisibility() && etCookingFuelOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_cooking_fuel"));
            return false;

        } else if (VISIBLE == etHouseStatusOther.getVisibility() && etHouseStatusOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_house_status"));
            return false;

        } else if (VISIBLE == etToiletAvailabilityOther.getVisibility() && etToiletAvailabilityOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_toilet_availability"));
            return false;

        } else if (VISIBLE == etElectricityAvailabilityOther.getVisibility() && etElectricityAvailabilityOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_electricity_availability"));
            return false;

        } else if (VISIBLE == etWaterSourceOther.getVisibility() && etWaterSourceOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_drinking_water_source"));
            return false;

        } else if (VISIBLE == etVehicleOther.getVisibility() && etVehicleOther.getText().toString().trim().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_motorised_vehicle"));
            return false;

        } else if (VISIBLE == etHandicappedSuffering.getVisibility() && !hasValue(tableHouse.getHandicappedSuffering())) {
            LToast.warning(AssetReader.getLangKeyword("enter_handicapped_suffering_from"));
            return false;

        } else if (VISIBLE == etPetsBiteActionOther.getVisibility() && !hasValue(tableHouse.getPetsBiteAction())) {
            LToast.warning(AssetReader.getLangKeyword("enter_pets_bite_action"));
            return false;

        } else if (VISIBLE == etDrinkingWaterCleaningMethod.getVisibility() && !hasValue(tableHouse.getDrinkingWaterCleaningMethod())) {
            LToast.warning(AssetReader.getLangKeyword("enter_drinking_water_cleaning_methods"));
            return false;

        } else if (VISIBLE == etBathingToiletFacility.getVisibility() && !hasValue(tableHouse.getBathingToiletFacility())) {
            LToast.warning(AssetReader.getLangKeyword("enter_bathing_toilet_facility"));
            return false;

        } else if (VISIBLE == etVaccinationRecordsStorageOther.getVisibility() && !hasValue(tableHouse.getVaccinationRecordsStorage())) {
            LToast.warning(AssetReader.getLangKeyword("enter_vaccination_records_storage"));
            return false;
        }

        if (null != tableDeathInfoList) {
            for (TableDeathInfo tableDeathInfo : tableDeathInfoList) {
                if (tableDeathInfo.getPersonName().isEmpty()
                        || tableDeathInfo.getGender().isEmpty()
                        || tableDeathInfo.getAge() <= 0
                        || tableDeathInfo.getDeathCause().isEmpty()
                        || tableDeathInfo.getRelationToHoh().isEmpty()) {
                    LToast.warningLong(AssetReader.getLangKeyword("enter_recent_death_details"));
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int indexId) {

        switch (radioGroup.getId()) {

            case R.id.rg_ownership: {
                int selectedIndex = rgOwnership.indexOfChild(rgOwnership.findViewById(indexId));
                tableHouse.setHouseOwnership(getEnglishIndex(KEY_HOUSE_OWNERSHIP_TYPES, selectedIndex));
                break;
            }

            case R.id.rg_ration: {
                int selectedIndex = rgRation.indexOfChild(rgRation.findViewById(indexId));

                if (0 == selectedIndex) {
                    spRation.setVisibility(VISIBLE);

                    if (!autoCheck) {
                        tableHouse.setRationCard("");
                    }
                    autoCheck = false;

                } else {
                    tableHouse.setRationCard("No");
                    spRation.setVisibility(View.GONE);
                    LView.setSpinnerAdapter(this, spRation, rationTypes);
                    etRationOther.setText("");
                    etRationOther.setVisibility(View.GONE);
                }
                break;
            }

            case R.id.rg_insurance: {
                int selectedIndex = rgInsurance.indexOfChild(rgInsurance.findViewById(indexId));

                if (0 == selectedIndex) {
                    spInsurance.setVisibility(VISIBLE);

                    if (!autoCheck) {
                        tableHouse.setHealthInsurance("");
                    }
                    autoCheck = false;

                } else {
                    tableHouse.setHealthInsurance("No");
                    spInsurance.setVisibility(View.GONE);
                    etInsuranceOther.setText("");
                    LView.setSpinnerAdapter(this, spInsurance, insuranceTypes);
                    etInsuranceOther.setVisibility(View.GONE);
                }
                break;
            }

            case R.id.rg_recent_death: {
                int selectedIndex = rgRecentDeath.indexOfChild(rgRecentDeath.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setRecentDeath("Yes");
                } else {
                    tableHouse.setRecentDeath("No");
                }

                if (tableHouse.getRecentDeath().equals("Yes")) {
                    showRecentDeathInfo();
                } else {
                    hideDeathInfo();
                }

                break;
            }

            case R.id.rg_drink_weekly: {
                int selectedIndex = rgDrinkWeekly.indexOfChild(rgDrinkWeekly.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setDrinkWeekly("Yes");
                } else if (1 == selectedIndex) {
                    tableHouse.setDrinkWeekly("No");
                } else {
                    tableHouse.setDrinkWeekly("Don't know");
                }
                break;
            }

            case R.id.rg_drug_use: {
                int selectedIndex = rgDrugUse.indexOfChild(rgDrugUse.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setDrugsUse("Yes");
                } else if (1 == selectedIndex) {
                    tableHouse.setDrugsUse("No");
                } else {
                    tableHouse.setDrugsUse("Don't know");
                }
                break;
            }

            case R.id.rg_handicapped_present: {
                int selectedIndex = rgHandicappedPresent.indexOfChild(rgHandicappedPresent.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setHandicappedPresent("Yes");
                    showHandicappedSuffering();

                } else if (1 == selectedIndex) {
                    tableHouse.setHandicappedPresent("No");
                    hideHandicappedSuffering();

                } else {
                    tableHouse.setHandicappedPresent("Don't know");
                    hideHandicappedSuffering();
                }
                break;
            }

            case R.id.rg_handicapped_suffering: {
                int selectedIndex = rgHandicappedSuffering.indexOfChild(rgHandicappedSuffering.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setHandicappedSuffering("Yes");
                    etHandicappedSuffering.setVisibility(VISIBLE);

                } else if (1 == selectedIndex) {
                    tableHouse.setHandicappedSuffering("No");
                    etHandicappedSuffering.setVisibility(View.GONE);

                } else {
                    tableHouse.setHandicappedSuffering("Don't know");
                    etHandicappedSuffering.setVisibility(View.GONE);
                }
                break;
            }

            case R.id.rg_pets: {
                int selectedIndex = rgPets.indexOfChild(rgPets.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setPets("Yes");
                    showPetBite();

                } else if (1 == selectedIndex) {
                    tableHouse.setPets("No");
                    hidePetBite();

                } else {
                    tableHouse.setPets("Don't know");
                    hidePetBite();
                }
                break;
            }

            case R.id.rg_pets_bitten: {
                int selectedIndex = rgPetsBitten.indexOfChild(rgPetsBitten.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setPetsBitten("Yes");
                    showPetBiteAction();

                } else if (1 == selectedIndex) {
                    tableHouse.setPetsBitten("No");
                    hidePetBiteAction();

                } else {
                    tableHouse.setPetsBitten("Don't know");
                    hidePetBiteAction();
                }
                break;
            }

            case R.id.rg_children_present: {
                int selectedIndex = rgChildrenPresent.indexOfChild(rgChildrenPresent.findViewById(indexId));

                if (0 == selectedIndex) {
                    showChildrenPresent();
                } else {
                    hideChildrenPresent();
                }
                break;
            }

            case R.id.rg_children_vaccinated: {
                int selectedIndex = rgChildrenVaccinated.indexOfChild(rgChildrenVaccinated.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setChildrenVaccinated("Yes");
                    showVaccinationStorage();

                } else if (1 == selectedIndex) {
                    tableHouse.setChildrenVaccinated("No");
                    hideVaccinationStorage();

                } else {
                    tableHouse.setChildrenVaccinated("Don't know");
                    hideVaccinationStorage();
                }
                break;
            }

            case R.id.rg_child_gender_responsibility: {
                int selectedIndex = rgChildGenderResponsibility.indexOfChild(rgChildGenderResponsibility.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setChildGenderResponsibility("Father");
                } else if (1 == selectedIndex) {
                    tableHouse.setChildGenderResponsibility("Mother");
                } else if (2 == selectedIndex) {
                    tableHouse.setChildGenderResponsibility("Neither");
                } else {
                    tableHouse.setChildGenderResponsibility("Don't know");
                }
                break;
            }

            case R.id.rg_preferred_child_gender: {
                int selectedIndex = rgPreferredChildGender.indexOfChild(rgPreferredChildGender.findViewById(indexId));

                if (0 == selectedIndex) {
                    tableHouse.setPreferredChildGender("Boy");
                } else {
                    tableHouse.setPreferredChildGender("Girl");
                }
                break;
            }
        }
    }

    private void hideChildrenPresent() {
        tvChildHealthCheckupLastDone.setVisibility(View.GONE);
        llChildrenCheckup.setVisibility(View.GONE);
        tvChildrenVaccinated.setVisibility(View.GONE);
        rgChildrenVaccinated.setVisibility(View.GONE);

        hideVaccinationStorage();
    }

    private void showChildrenPresent() {
        tvChildHealthCheckupLastDone.setVisibility(VISIBLE);
        llChildrenCheckup.setVisibility(VISIBLE);
        tvChildrenVaccinated.setVisibility(VISIBLE);
        rgChildrenVaccinated.setVisibility(VISIBLE);

        if (hasValue(tableHouse.getChildrenVaccinated()) && tableHouse.getChildrenVaccinated().equals("Yes")) {
            showVaccinationStorage();
        }
    }

    private void hideVaccinationStorage() {
        tvVaccinationRecordsStorage.setVisibility(View.GONE);
        llVaccinationRecordsStorageWrapper.setVisibility(View.GONE);
    }

    private void showVaccinationStorage() {
        tvVaccinationRecordsStorage.setVisibility(VISIBLE);
        llVaccinationRecordsStorageWrapper.setVisibility(VISIBLE);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        if (position <= 0) {
            return;
        }

        switch (adapterView.getId()) {

            case R.id.sp_ration: {
                if (rationTypesEnglish[position].equals("Other")) {
                    etRationOther.setVisibility(VISIBLE);
                } else {
                    etRationOther.setText("");
                    etRationOther.setVisibility(View.GONE);
                    tableHouse.setRationCard(rationTypesEnglish[position]);
                }
                break;
            }

            case R.id.sp_insurance: {
                if (insuranceTypesEnglish[position].equals("Other")) {
                    etInsuranceOther.setVisibility(VISIBLE);
                } else {
                    etInsuranceOther.setText("");
                    etInsuranceOther.setVisibility(View.GONE);
                    tableHouse.setHealthInsurance(insuranceTypesEnglish[position]);
                }
                break;
            }

            case R.id.sp_treatment: {
                if (treatmentPlacesEnglish[position].equals("Other")) {
                    etTreatmentOther.setVisibility(VISIBLE);
                } else {
                    etTreatmentOther.setText("");
                    etTreatmentOther.setVisibility(View.GONE);
                    tableHouse.setTreatmentPlace(treatmentPlacesEnglish[position]);
                }
                break;
            }

            case R.id.sp_house_status: {
                if (houseStatusEnglish[position].equals("Other")) {
                    etHouseStatusOther.setVisibility(VISIBLE);
                } else {
                    etHouseStatusOther.setText("");
                    etHouseStatusOther.setVisibility(View.GONE);
                    tableHouse.setHouseStatus(houseStatusEnglish[position]);
                }
                break;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void showRecentDeathInfo() {
        tvDeathInfo.setVisibility(VISIBLE);
        rvDeathInfo.setVisibility(VISIBLE);
        btnAddMoreDeathInfo.setVisibility(VISIBLE);
        adapterDeathInfo.setItems(tableDeathInfoList);

        if (tableDeathInfoList.isEmpty()) {
            adapterDeathInfo.addNewItem(true);
        }
    }

    private void hideDeathInfo() {
        tableDeathInfoList = new ArrayList<>();
        tvDeathInfo.setVisibility(View.GONE);
        rvDeathInfo.setVisibility(View.GONE);
        btnAddMoreDeathInfo.setVisibility(View.GONE);
        adapterDeathInfo.setItems(tableDeathInfoList);
    }

    private void initCheckbox(String[] array, int idExtra, LinearLayout linearLayout, ArrayList<CheckBox> checkBoxes) {

        for (int i = 0; i < array.length; i++) {
            CheckBox cb = new CheckBox(getApplicationContext());
            cb.setText(array[i]);
            cb.setId(i + idExtra);
            int states[][] = {{android.R.attr.state_checked}, {}};
            int colors[]   = {R.color.teal_dark, R.color.white};
            CompoundButtonCompat.setButtonTintList(cb, new ColorStateList(states, colors));
            cb.setTextColor(ContextCompat.getColor(this, R.color.text_color_dark));
            cb.setTextSize(18.0f);
            linearLayout.addView(cb);

            cb.setOnCheckedChangeListener(this);

            checkBoxes.add(cb);
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

        if (compoundButton.getId() > 8888 && compoundButton.getId() < 8910) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etCookingFuelOther.setVisibility(View.VISIBLE);

                } else {
                    etCookingFuelOther.setVisibility(View.GONE);
                    etCookingFuelOther.setText("");
                }
            }
        }

        if (compoundButton.getId() > 4444 && compoundButton.getId() < 4544) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etToiletAvailabilityOther.setVisibility(View.VISIBLE);

                } else {
                    etToiletAvailabilityOther.setVisibility(View.GONE);
                    etToiletAvailabilityOther.setText("");
                }
            }
        }

        if (compoundButton.getId() > 3333 && compoundButton.getId() < 3433) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etElectricityAvailabilityOther.setVisibility(View.VISIBLE);

                } else {
                    etElectricityAvailabilityOther.setVisibility(View.GONE);
                    etElectricityAvailabilityOther.setText("");
                }
            }
        }

        if (compoundButton.getId() > 5555 && compoundButton.getId() < 5655) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etWaterSourceOther.setVisibility(View.VISIBLE);

                } else {
                    etWaterSourceOther.setVisibility(View.GONE);
                    etWaterSourceOther.setText("");
                }
            }
        }

        if (compoundButton.getId() > 6666 && compoundButton.getId() < 6766) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etVehicleOther.setVisibility(View.VISIBLE);

                } else {
                    etVehicleOther.setVisibility(View.GONE);
                    etVehicleOther.setText("");
                }
            }
        }

        if (compoundButton.getId() >= 12222 && compoundButton.getId() < 12322) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etPetsBiteActionOther.setVisibility(View.VISIBLE);

                } else {
                    etPetsBiteActionOther.setVisibility(View.GONE);
                    etPetsBiteActionOther.setText("");
                }
            }

            int     noneIndex     = 0;
            boolean othersChecked = false;

            for (int i = 0; i < cbPetsBiteActions.size(); i++) {

                if (cbPetsBiteActions.get(i).getText().toString().equals(AssetReader.getLangKeyword("none"))) {
                    noneIndex = i;

                } else if (cbPetsBiteActions.get(i).isChecked()) {
                    othersChecked = true;
                }
            }

            cbPetsBiteActions.get(noneIndex).setEnabled(!othersChecked);

            if (othersChecked) {
                cbPetsBiteActions.get(noneIndex).setChecked(false);
            }
        }

        if (compoundButton.getId() > 13222 && compoundButton.getId() < 13322) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etDrinkingWaterCleaningMethod.setVisibility(View.VISIBLE);

                } else {
                    etDrinkingWaterCleaningMethod.setVisibility(View.GONE);
                    etDrinkingWaterCleaningMethod.setText("");
                }
            }
        }

        if (compoundButton.getId() > 14222 && compoundButton.getId() < 14322) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etBathingToiletFacility.setVisibility(View.VISIBLE);

                } else {
                    etBathingToiletFacility.setVisibility(View.GONE);
                    etBathingToiletFacility.setText("");
                }
            }
        }

        if (compoundButton.getId() > 15222 && compoundButton.getId() < 15322) {

            if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {

                if (checked) {
                    etVaccinationRecordsStorageOther.setVisibility(View.VISIBLE);

                } else {
                    etVaccinationRecordsStorageOther.setVisibility(View.GONE);
                    etVaccinationRecordsStorageOther.setText("");
                }
            }
        }
    }

    private String getCheckBoxValues(String keyword, ArrayList<CheckBox> checkBoxes, EditText editText) {
        String values = "";

        for (int i = 0; i < checkBoxes.size(); i++) {
            if (checkBoxes.get(i).isChecked()) {
                if (checkBoxes.get(i).getText().equals(AssetReader.getLangKeyword("other"))) {
                    continue;
                }

                if (!values.isEmpty()) {
                    values += ";";
                }

                values += AssetReader.getEnglishIndex(keyword, i);
            }
        }
        if (null != editText && VISIBLE == editText.getVisibility() && !editText.getText().toString().isEmpty()) {
            if (!values.isEmpty()) {
                values += ";";
            }
            values += editText.getText().toString();
        }

        return values;
    }

    private void populateCheckBox(String[] savedArray, String[] fullArray, ArrayList<CheckBox> checkBoxes, EditText editText) {

        for (String item : savedArray) {

            for (int i = 0; i < fullArray.length; i++) {
                if (item.equals(fullArray[i])) {
                    checkBoxes.get(i).setChecked(true);
                }
            }

            if (isOther(item, fullArray) && null != editText) {
                if (!editText.getText().toString().isEmpty()) {
                    editText.setText(editText.getText() + ";");
                }

                checkBoxes.get(checkBoxes.size() - 1).setChecked(true);
                editText.setText(editText.getText() + item);
            }
        }
    }

    private void showHandicappedSuffering() {
        tvHandicappedSuffering.setVisibility(VISIBLE);
        rgHandicappedSuffering.setVisibility(VISIBLE);
    }

    private void hideHandicappedSuffering() {
        tvHandicappedSuffering.setVisibility(View.GONE);
        rgHandicappedSuffering.setVisibility(View.GONE);
    }

    private void showPetBite() {
        tvPetsBitten.setVisibility(VISIBLE);
        rgPetsBitten.setVisibility(VISIBLE);
    }

    private void hidePetBite() {
        tvPetsBitten.setVisibility(View.GONE);
        rgPetsBitten.setVisibility(View.GONE);
    }

    private void showPetBiteAction() {
        tvPetsBiteAction.setVisibility(VISIBLE);
        llPetsBiteActionWrapper.setVisibility(VISIBLE);
    }

    private void hidePetBiteAction() {
        tvPetsBiteAction.setVisibility(View.GONE);
        llPetsBiteActionWrapper.setVisibility(View.GONE);
    }

    private void initLanguage() {
        tvHouseNumber.setText(AssetReader.getLangKeyword("house_number"));
        tvStreet.setText(AssetReader.getLangKeyword("street"));
        tvPost.setText(AssetReader.getLangKeyword("post"));
        tvLocality.setText(AssetReader.getLangKeyword("locality"));
        tvDistrict.setText(AssetReader.getLangKeyword("district"));
        tvState.setText(AssetReader.getLangKeyword("state"));
        tvCountry.setText(AssetReader.getLangKeyword("country"));
        tvPincode.setText(AssetReader.getLangKeyword("pin_code"));
        tvHeadOfHouse.setText(AssetReader.getLangKeyword("name_of_head_of_house"));
        tvSpouseName.setText(AssetReader.getLangKeyword("name_of_spouse_of_head_of_house"));
        tvRespondent.setText(AssetReader.getLangKeyword("name_of_respondent"));
        tvTotalMembers.setText(AssetReader.getLangKeyword("total_members_in_house"));
        tvTotalRooms.setText(AssetReader.getLangKeyword("total_rooms_in_house"));
        tvOwnership.setText(AssetReader.getLangKeyword("house_ownership"));
        tvHouseStatus.setText(AssetReader.getLangKeyword("house_status"));
        tvRationCard.setText(AssetReader.getLangKeyword("ration_card"));
        tvHealthInsurance.setText(AssetReader.getLangKeyword("health_insurance"));
        tvMealTimings.setText(AssetReader.getLangKeyword("meal_timing_title"));
        tvCookingFuel.setText(AssetReader.getLangKeyword("cooking_fuel_title"));
        tvToiletAvailability.setText(AssetReader.getLangKeyword("toilet_availability"));
        tvElectricityAvailability.setText(AssetReader.getLangKeyword("electricity_availability"));
        tvWaterSource.setText(AssetReader.getLangKeyword("drinking_water_source"));
        tvVehicle.setText(AssetReader.getLangKeyword("motorised_vehicle"));
        tvTreatmentPlace.setText(AssetReader.getLangKeyword("treatment_place_title"));
        tvAnyDeath.setText(AssetReader.getLangKeyword("any_death"));
        tvDeathInfo.setText(AssetReader.getLangKeyword("if_yes"));
        btnAddMoreDeathInfo.setText(AssetReader.getLangKeyword("add_more"));
        tvNotes.setText(AssetReader.getLangKeyword("notes_title"));
        btnSave.setText(AssetReader.getLangKeyword("save"));

        tvVillage.setText(AssetReader.getLangKeyword("village"));
        tvGramPanchayat.setText(AssetReader.getLangKeyword("gram_panchayat"));

        rbInsuranceYes.setText(AssetReader.getLangKeyword("yes"));
        rbInsuranceNo.setText(AssetReader.getLangKeyword("no"));
        rbRationYes.setText(AssetReader.getLangKeyword("yes"));
        rbRationNo.setText(AssetReader.getLangKeyword("no"));
        rbRecentDeathYes.setText(AssetReader.getLangKeyword("yes"));
        rbRecentDeathNo.setText(AssetReader.getLangKeyword("no"));
        rbOwn.setText(AssetReader.getLangKeyword("own"));
        rbRented.setText(AssetReader.getLangKeyword("rented"));

        etInsuranceOther.setHint(AssetReader.getLangKeyword("if_other"));
        etRationOther.setHint(AssetReader.getLangKeyword("if_other"));
        etTreatmentOther.setHint(AssetReader.getLangKeyword("if_other"));
        etCookingFuelOther.setHint(AssetReader.getLangKeyword("if_other"));
        etHouseStatusOther.setHint(AssetReader.getLangKeyword("if_other"));
        etToiletAvailabilityOther.setHint(AssetReader.getLangKeyword("if_other"));
        etElectricityAvailabilityOther.setHint(AssetReader.getLangKeyword("if_other"));
        etWaterSourceOther.setHint(AssetReader.getLangKeyword("if_other"));

        etHandicappedSuffering.setHint(AssetReader.getLangKeyword("enter"));

        tvTotalEarningMembers.setText(AssetReader.getLangKeyword("total_earning_members_in_house"));
        tvHouseIncome.setText(AssetReader.getLangKeyword("household_monthly_income"));
        tvTreatmentCost.setText(AssetReader.getLangKeyword("treatment_cost"));
        tvDrinkWeekly.setText(AssetReader.getLangKeyword("drink_weekly"));
        tvDrugUse.setText(AssetReader.getLangKeyword("drugs_use"));
        tvHandicappedPresent.setText(AssetReader.getLangKeyword("handicapped_present"));
        tvHandicappedSuffering.setText(AssetReader.getLangKeyword("handicapped_suffering"));
        tvPets.setText(AssetReader.getLangKeyword("pets"));
        tvPetsBitten.setText(AssetReader.getLangKeyword("pets_bitten"));
        tvPetsBiteAction.setText(AssetReader.getLangKeyword("pets_bite_action"));
        tvDrinkingWaterCleaningMethod.setText(AssetReader.getLangKeyword("drinking_water_cleaning_method"));
        tvBathingToiletFacility.setText(AssetReader.getLangKeyword("bathing_toilet_facility"));
        tvChildrenPresent.setText(AssetReader.getLangKeyword("children_present_in_house"));
        tvChildHealthCheckupLastDone.setText(AssetReader.getLangKeyword("child_health_checkup_last_done"));
        tvChildrenVaccinated.setText(AssetReader.getLangKeyword("children_vaccinated"));
        tvVaccinationRecordsStorage.setText(AssetReader.getLangKeyword("vaccination_records_storage"));
        tvChildGenderResponsibility.setText(AssetReader.getLangKeyword("child_gender_responsibility"));
        tvPreferredChildGender.setText(AssetReader.getLangKeyword("preferred_child_gender"));

//        etTotalEarningMembers.setHint(AssetReader.getLangKeyword("enter"));
//        etHouseIncome.setHint(AssetReader.getLangKeyword("enter"));
//        etTreatmentCost.setHint(AssetReader.getLangKeyword("enter"));
//        etHandicappedSuffering.setHint(AssetReader.getLangKeyword("enter"));
        etChildHealthCheckupLastDone.setHint(AssetReader.getLangKeyword("enter"));

        ((RadioButton) rgDrinkWeekly.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgDrinkWeekly.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgDrinkWeekly.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgDrugUse.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgDrugUse.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgDrugUse.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgHandicappedPresent.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgHandicappedPresent.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));

        ((RadioButton) rgHandicappedSuffering.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgHandicappedSuffering.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgHandicappedSuffering.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgPets.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgPets.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgPets.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgPetsBitten.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgPetsBitten.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgPetsBitten.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgChildrenPresent.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgChildrenPresent.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));

        ((RadioButton) rgChildrenVaccinated.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
        ((RadioButton) rgChildrenVaccinated.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
        ((RadioButton) rgChildrenVaccinated.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgChildGenderResponsibility.getChildAt(0)).setText(AssetReader.getLangKeyword("father"));
        ((RadioButton) rgChildGenderResponsibility.getChildAt(1)).setText(AssetReader.getLangKeyword("mother"));
        ((RadioButton) rgChildGenderResponsibility.getChildAt(2)).setText(AssetReader.getLangKeyword("neither"));
        ((RadioButton) rgChildGenderResponsibility.getChildAt(3)).setText(AssetReader.getLangKeyword("dont_know"));

        ((RadioButton) rgPreferredChildGender.getChildAt(0)).setText(AssetReader.getLangKeyword("boy"));
        ((RadioButton) rgPreferredChildGender.getChildAt(1)).setText(AssetReader.getLangKeyword("girl"));
    }
}