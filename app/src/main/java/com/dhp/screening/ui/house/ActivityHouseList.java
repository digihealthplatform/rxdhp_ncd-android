package com.dhp.screening.ui.house;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterHouse;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.ui.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ActivityHouseList extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @Inject
    DataManager dataManager;

    @BindView(R.id.rv_houses)
    RecyclerView rvHouses;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_list);
        initToolbar(toolbar, AssetReader.getLangKeyword("houses"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();
    }

    @Override
    protected void onResume() {
        super.onResume();
        List<TableHouse> houses = dataManager.getAllHouses();

        String[]  householdList = new String[houses.size()];
        String[]  houseIds      = new String[houses.size()];
        boolean[] houseInactive = new boolean[houses.size()];

        for (int i = 0; i < houses.size(); i++) {

            TableHouse house = houses.get(i);

            householdList[i] = house.getHouseNo()
                    + " " + house.getStreet()
                    + " " + house.getPost()
                    + " " + house.getLocality()
                    + " " + house.getDistrict()
                    + " " + house.getState();

            houseIds[i] = house.getHouseId();

            if (house.isInactive()) {
                houseInactive[i] = true;
            }
        }

        AdapterHouse adapterHouse = new AdapterHouse(this, householdList, houseIds, houseInactive);
        rvHouses.setLayoutManager(new LinearLayoutManager(this));
        rvHouses.setAdapter(adapterHouse);

        if (0 == householdList.length) {
            tvEmpty.setVisibility(View.VISIBLE);
        }
    }

    private void initLanguage() {
        tvEmpty.setText(AssetReader.getLangKeyword("empty"));
    }
}