package com.dhp.screening.ui.patient;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.dialog.DialogSearch;
import com.dhp.screening.ui.dialog.DialogSearchCallbacks;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.asset.AssetReader.KEY_ACCEPT_AADHAAR_NUMBER;
import static com.dhp.screening.data.asset.AssetReader.KEY_GENDER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_IDENTITY_TYPES;
import static com.dhp.screening.data.asset.AssetReader.getEnglishIndex;
import static com.dhp.screening.util.LUtils.calculateAge;
import static com.dhp.screening.util.LUtils.calculateYob;
import static com.dhp.screening.util.LUtils.getFormattedDate;
import static com.dhp.screening.util.LUtils.getGenderIndex;
import static com.dhp.screening.util.LUtils.getSpinnerIndexOther;

public class ActivityEditDemographics extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_full_name)
    EditText etFullName;

    @BindView(R.id.tv_full_name)
    TextView tvFullName;

    @BindView(R.id.tv_gender)
    TextView tvGender;

    @BindView(R.id.tv_date_of_birth)
    TextView tvDobTitle;

    @BindView(R.id.tv_or)
    TextView tvOr;

    @BindView(R.id.tv_years)
    TextView tvYears;

    @BindView(R.id.et_dob)
    EditText etDob;

    @BindView(R.id.rb_male)
    RadioButton rbMale;

    @BindView(R.id.rb_female)
    RadioButton rFemale;

    @BindView(R.id.rb_other)
    RadioButton rbOther;

    @BindView(R.id.et_age)
    EditText etAge;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_email_id)
    EditText etEmailId;

    @BindView(R.id.et_identity_type_other)
    EditText etIdentityTypeOther;

    @BindView(R.id.et_house)
    EditText etHouse;

    @BindView(R.id.et_identity_no)
    EditText etIdentityNumber;

    @BindView(R.id.sp_identity_type)
    Spinner spIdentityType;

    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.tv_identity)
    TextView tvIdentity;

    @BindView(R.id.tv_phone)
    TextView tvPhone;

    @BindView(R.id.tv_email_id)
    TextView tvEmailId;

    @BindView(R.id.tv_house)
    TextView tvHouse;

    @BindView(R.id.tv_notes)
    TextView tvNotes;

    @BindView(R.id.tv_mr_number)
    TextView tvMrNumber;

    @BindView(R.id.et_mr_number)
    EditText etMrNumber;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Inject
    DataManager dataManager;

    private TableDemographics tableDemographics;

    private String gender;
    private String identityType = "";

    private String[] identityList;
    private String[] identityListEnglish;

    private String[] householdList;
    private String[] houseHeads;
    private String[] houseIds;

    private boolean autoFill;

    String oldIdentityType;
    String oldIdentityNumber;

    private boolean isFirstSelect = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_demographics);
        initToolbar(toolbar, "Edit info", true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgGender.indexOfChild(rgGender.findViewById(indexId));
                gender = getEnglishIndex(KEY_GENDER_TYPES, selectedIndex);
            }
        });

        identityList = AssetReader.getSpinnerArray(KEY_IDENTITY_TYPES);
        identityListEnglish = AssetReader.getSpinnerArrayEnglish(KEY_IDENTITY_TYPES);

        spIdentityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onIdentitySelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        tableDemographics = dataManager.getPatient(StaticData.getRegistrationId());

        if (tableDemographics.getIdentityType().equals("Aadhaar")
                && !AssetReader.toShow(KEY_ACCEPT_AADHAAR_NUMBER)) {
            etIdentityNumber.setVisibility(View.GONE);
        }

        oldIdentityType = tableDemographics.getIdentityType();
        oldIdentityNumber = tableDemographics.getIdentityNumber();

        LView.setSpinnerAdapter(this, spIdentityType, identityList);

        int spinnerIndex = getSpinnerIndexOther(tableDemographics.getIdentityType(), identityListEnglish);
        if (-1 != spinnerIndex) {
            autoFill = true;
            spIdentityType.setSelection(spinnerIndex);
            etIdentityNumber.setText(tableDemographics.getIdentityNumber());

            if (identityListEnglish[spinnerIndex].equals("Other")) {
                etIdentityTypeOther.setText(tableDemographics.getIdentityType());
            }
        }

        if (tableDemographics.isQrScanned()) {
            spIdentityType.setEnabled(false);
        }

        etFullName.setText(tableDemographics.getFullName());

        ((RadioButton) rgGender.getChildAt(getGenderIndex(tableDemographics.getGender()))).setChecked(true);

        if (!tableDemographics.getDob().isEmpty()) {
            etDob.setText(tableDemographics.getDob());
        }

        etAge.setText("" + calculateAge(tableDemographics.getYob()));

        etPhone.setText(tableDemographics.getPhoneNumber());
        etEmailId.setText(tableDemographics.getEmailId());

        List<TableHouse> houses = dataManager.getAllActiveHouses();

        householdList = new String[houses.size()];
        this.houseHeads = new String[houses.size()];

        houseIds = new String[houses.size()];

        for (int i = 0; i < houses.size(); i++) {
            householdList[i] = houses.get(i).getHouseNo()
                    + " " + houses.get(i).getStreet()
                    + " " + houses.get(i).getPost()
                    + " " + houses.get(i).getLocality();

            houseIds[i] = houses.get(i).getHouseId();

            this.houseHeads[i] = houses.get(i).getHeadOfHouse();
        }

        for (int i = 0; i < houseIds.length; i++) {
            if (houseIds[i].equals(tableDemographics.getHouseId())) {
                etHouse.setText(householdList[i]);
                break;
            }
        }

        etNotes.setText(tableDemographics.getNotes());
        etMrNumber.setText(tableDemographics.getMrNumber());
    }

    @OnClick(R.id.et_dob)
    void onDobClicked() {
        final Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        onDobSet(dayOfMonth, monthOfYear, year);
                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "DatePickerDialogDob");
    }

    private void onDobSet(int dayOfMonth, int monthOfYear, int year) {
        if (LUtils.isFutureDate(dayOfMonth, monthOfYear, year)) {
            LToast.warning(AssetReader.getLangKeyword("cant_select_future_date"));
            return;
        }

        etDob.setText(getFormattedDate(dayOfMonth, monthOfYear, year));
        etAge.setText("" + calculateAge(year));
        etAge.setEnabled(false);
    }

    private void onIdentitySelected(int position) {
        if (position > 0) {
            identityType = identityListEnglish[position];

            etIdentityNumber.setVisibility(View.VISIBLE);

            if (identityType.equals("Aadhaar") && !AssetReader.toShow(KEY_ACCEPT_AADHAAR_NUMBER)) {
                etIdentityNumber.setVisibility(View.GONE);
            }

            if (!autoFill) {
                etIdentityNumber.setText("");
            }

            autoFill = false;

            if (identityType.equals("Other")) {
                etIdentityTypeOther.setVisibility(View.VISIBLE);

            } else {
                etIdentityTypeOther.setText("");
                etIdentityTypeOther.setVisibility(View.GONE);

                if (identityType.equals("None")) {
                    etIdentityNumber.setVisibility(View.GONE);
                    etIdentityNumber.setText("");
                }
            }
        }
    }

    @OnClick(R.id.et_house)
    void selectHouse() {
        new DialogSearch(this, new DialogSearchCallbacks() {
            @Override
            public void onSelected(String house, int position) {
                etHouse.setText(house);

                tableDemographics.setHouseId(houseIds[position]);
            }
        }).withItems(householdList, houseHeads).showDialog();
    }

    private boolean isAllFieldsFilled() {
        if (tableDemographics.getFullName().equals("")) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_name"));
            return false;
        }

        if (null == tableDemographics.getGender()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_gender"));
            return false;
        }

        if (tableDemographics.getDob().equals("") && tableDemographics.getYob() <= 0) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_dob"));
            return false;
        }

        if (!tableDemographics.getPhoneNumber().equals("")) {

            if (AssetReader.getMinPhoneLength() > tableDemographics.getPhoneNumber().length()
                    || AssetReader.getMaxPhoneLength() < tableDemographics.getPhoneNumber().length()
                    || !Patterns.PHONE.matcher(tableDemographics.getPhoneNumber()).matches()) {
                LToast.warning(AssetReader.getLangKeyword("warning_enter_phone"));
                return false;
            }
        }

        if (!tableDemographics.getEmailId().equals("") && !Patterns.EMAIL_ADDRESS.matcher(tableDemographics.getEmailId()).matches()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_email"));
            return false;
        }

        if (!identityType.isEmpty()
                && identityType.equals("Other")
                && etIdentityTypeOther.getText().toString().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_identity_type"));
            return false;
        }

        boolean toCheckIdentityNumber = true;

        if (identityType.equals("None")) {
            toCheckIdentityNumber = false;

        } else if (identityType.equals("Aadhaar") && !AssetReader.toShow(KEY_ACCEPT_AADHAAR_NUMBER)) {
            toCheckIdentityNumber = false;
        }

        if (toCheckIdentityNumber) {

            if (etIdentityNumber.getText().toString().isEmpty()) {
                LToast.warning(AssetReader.getLangKeyword("warning_enter_identity_number"));
                return false;
            }

            if (!etIdentityNumber.getText().toString().matches("[A-Za-z0-9]+")) {
                LToast.warningLong(AssetReader.getLangKeyword("warning_enter_valid_identity_number"));
                return false;
            }
        }

        if (tableDemographics.getHouseId().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("enter_house_info"));
            return false;
        }

        return true;
    }

    private void readValues() {
        tableDemographics.setFullName(etFullName.getText().toString().trim());

        try {
            int age = Integer.parseInt(etAge.getText().toString());
            tableDemographics.setYob(calculateYob(age));

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (!etDob.getText().toString().isEmpty()) {
            tableDemographics.setDob(etDob.getText().toString());
        } else {
            // Save jan 1 as dob
//            tableDemographics.setDob("01/01/" + tableDemographics.getYob());
        }

        tableDemographics.setGender(gender);
        tableDemographics.setPhoneNumber(etPhone.getText().toString());
        tableDemographics.setEmailId(etEmailId.getText().toString());

        if (null != identityType && identityType.equals("Other")) {
            tableDemographics.setIdentityType(etIdentityTypeOther.getText().toString().trim());
        } else {
            tableDemographics.setIdentityType(identityType);
        }

        tableDemographics.setIdentityNumber(etIdentityNumber.getText().toString().toUpperCase());
        tableDemographics.setNotes(etNotes.getText().toString().trim());
        tableDemographics.setMrNumber(etMrNumber.getText().toString().trim());
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_go_back"));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityEditDemographics.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        readValues();

        if (isAllFieldsFilled()) {

//            if (!(tableDemographics.getIdentityType().equals(identityType)
//                    && tableDemographics.getIdentityNumber().equals(oldIdentityNumber))
//                    && !tableDemographics.getIdentityNumber().isEmpty()
//                    && dataManager.isIdentityExists(tableDemographics)) {
//                LToast.errorLong("Identity details already exit");
//
//            } else {
            confirmSave();
//            }
        }
    }

    private void confirmSave() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));
        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.updatePatient(tableDemographics);
                LToast.success(AssetReader.getLangKeyword("updated"));
                finish();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void initLanguage() {
        tvFullName.setText(AssetReader.getLangKeyword("full_name") + " *");
        etFullName.setHint(AssetReader.getLangKeyword("enter_full_name"));
        tvGender.setText(AssetReader.getLangKeyword("gender") + " *");
        rbMale.setText(AssetReader.getLangKeyword("male"));
        rFemale.setText(AssetReader.getLangKeyword("female"));
        rbOther.setText(AssetReader.getLangKeyword("other"));

        tvDobTitle.setText(AssetReader.getLangKeyword("date_of_birth_title") + " *");
        tvOr.setText(AssetReader.getLangKeyword("or"));
        tvYears.setText(AssetReader.getLangKeyword("years"));
        etDob.setHint(AssetReader.getLangKeyword("date_of_birth"));
        etAge.setHint(AssetReader.getLangKeyword("age"));

        tvMrNumber.setText(AssetReader.getLangKeyword("mr_number"));
        etMrNumber.setHint(AssetReader.getLangKeyword("enter"));

        tvIdentity.setText(AssetReader.getLangKeyword("identity") + " *");
        etIdentityTypeOther.setHint(AssetReader.getLangKeyword("enter_identity_type"));
        etIdentityNumber.setHint(AssetReader.getLangKeyword("identity_number"));

        tvPhone.setText(AssetReader.getLangKeyword("phone_number"));
        etPhone.setHint(AssetReader.getLangKeyword("enter_phone_number"));
        tvEmailId.setText(AssetReader.getLangKeyword("email_id"));
        etEmailId.setHint(AssetReader.getLangKeyword("enter_email_id"));
        tvHouse.setText(AssetReader.getLangKeyword("house_title") + " *");
        etHouse.setHint(AssetReader.getLangKeyword("select_house"));
        tvNotes.setText(AssetReader.getLangKeyword("notes_title"));
        etNotes.setHint(AssetReader.getLangKeyword("enter"));

        etPhone.setFilters(new InputFilter[]{new InputFilter.LengthFilter(AssetReader.getMaxPhoneLength())});

        btnSave.setText(AssetReader.getLangKeyword("update"));
    }
}