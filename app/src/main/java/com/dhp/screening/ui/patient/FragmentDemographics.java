package com.dhp.screening.ui.patient;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterQuestionAnswer;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemQuestionAnswer;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.lib.printer.PrinterManager;
import com.dhp.screening.ui.BaseFragment;
import com.dhp.screening.ui.house.ActivityEditHouse;
import com.dhp.screening.ui.images.ActivityAddImage;
import com.dhp.screening.ui.images.ActivityDisplayImageDark;
import com.dhp.screening.util.LUtils;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.asset.AssetReader.KEY_DISPLAY_AADHAAR_NUMBER;
import static com.dhp.screening.data.asset.AssetReader.KEY_GENDER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_IDENTITY_TYPES;
import static com.dhp.screening.data.asset.AssetReader.getSelectedLanguageVersion;
import static com.dhp.screening.util.LUtils.hasValue;


@SuppressLint("ValidFragment")
public class FragmentDemographics extends BaseFragment
        implements BaseFragment.InactiveCallbacks,
        BaseFragment.IBaseFragment {

    private boolean startEditing;

    @BindView(R.id.rv_question_answer)
    RecyclerView rvQuestionAnswer;

    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;

    @BindView(R.id.fab_edit)
    FloatingActionMenu fabMenu;

    @BindView(R.id.fab_edit_person)
    FloatingActionButton fabEditDemographics;

    @BindView(R.id.fab_edit_house)
    FloatingActionButton fabEditHouseInfo;

    @BindView(R.id.iv_profile_pic)
    ImageView ivProfilePic;

    private DataManager dataManager;

    private AdapterQuestionAnswer adapterQuestionAnswer;

    private TableDemographics tableDemographics;
    private TableHouse        tableHouse;

    private String profilePicPath;

    public FragmentDemographics() {
    }

    public FragmentDemographics(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapterQuestionAnswer = new AdapterQuestionAnswer(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_demographics, container, false);
        ButterKnife.bind(this, view);

        rvQuestionAnswer.setLayoutManager(new LinearLayoutManager(getActivity()));
        fabMenu.setClosedOnTouchOutside(true);

        setHasOptionsMenu(true);

        initLanguage();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        startEditing = false;

        tableDemographics = dataManager.getPatient(StaticData.getRegistrationId());
        tableHouse = dataManager.getHouse(tableDemographics.getHouseId());

        ArrayList<ItemQuestionAnswer> questionAnswers = new ArrayList<>(16);

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("registration_id"), tableDemographics.getRegistrationId()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("full_name"), tableDemographics.getFullName()));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("gender"), getSelectedLanguageVersion(tableDemographics.getGender(), KEY_GENDER_TYPES)));

        if (tableDemographics.getDob().isEmpty()) {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("year_of_birth"), "" + tableDemographics.getYob()));
        } else {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("date_of_birth") + " :", tableDemographics.getDob()));
        }

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("age") + ":", "" + LUtils.calculateAge(tableDemographics.getYob())));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("phone_number"), tableDemographics.getPhoneNumber()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("email_id"), tableDemographics.getEmailId()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("identity_type"), getSelectedLanguageVersion(tableDemographics.getIdentityType(), KEY_IDENTITY_TYPES)));

        if (tableDemographics.getIdentityType().equals("Aadhaar")) {
            if (AssetReader.toShow(KEY_DISPLAY_AADHAAR_NUMBER)) {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("identity_number") + ":", tableDemographics.getIdentityNumber()));
            }
        } else {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("identity_number") + ":", tableDemographics.getIdentityNumber()));
        }

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("mr_number"), tableDemographics.getMrNumber()));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("notes_title"), tableDemographics.getNotes()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("house_number"), tableHouse.getHouseNo()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("post"), tableHouse.getPost()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("street"), tableHouse.getStreet()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("locality"), tableHouse.getLocality()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("district"), tableHouse.getDistrict()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("state"), tableHouse.getState()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("pin_code"), " " + tableHouse.getPinCode()));

        adapterQuestionAnswer.setItems(questionAnswers);

        rvQuestionAnswer.setAdapter(adapterQuestionAnswer);
        rvQuestionAnswer.setNestedScrollingEnabled(false)
        ;
        String fileName = dataManager.getProfilePic(tableDemographics.getRegistrationId());

        if (hasValue(fileName)) {
            ivProfilePic.setVisibility(View.VISIBLE);
            profilePicPath = LUtils.getLocalFilesPath() + "/" + fileName;

            RequestOptions requestOptions = new RequestOptions()
                    .placeholder(R.drawable.profile_placeholder)
                    .error(R.drawable.profile_placeholder);

            Glide.with(this)
                    .load(profilePicPath)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .apply(requestOptions)
                    .apply(RequestOptions.bitmapTransform(new CircleCrop()))
                    .into(ivProfilePic);
        } else {
            ivProfilePic.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.iv_profile_pic)
    void onProfileClicked() {
        Intent intent = new Intent(getActivity(), ActivityDisplayImageDark.class);
        intent.putExtra("url", profilePicPath);
        startActivity(intent);
    }

    @OnClick(R.id.fab_add)
    void onAddClicked() {
        Intent intent = new Intent(getActivity(), ActivityAddImage.class);
        intent.putExtra("registration_id", tableDemographics.getRegistrationId());
        startActivity(intent);
    }

    @OnClick(R.id.fab_edit_house)
    void onEditHouse() {
        fabMenu.close(true);

        StaticData.setCurrentHouse(tableHouse);

        if (startEditing) {
            return;
        }
        startEditing = true;

        Intent intent = new Intent(getActivity(), ActivityEditHouse.class);
        intent.putExtra("edit", true);
        startActivity(intent);
    }

    @OnClick(R.id.fab_edit_person)
    void onEditPerson() {
        fabMenu.close(true);

        if (startEditing) {
            return;
        }

        startEditing = true;

        startActivity(new Intent(getActivity(), ActivityEditDemographics.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_inactive: {

                if (tableDemographics.isInactive()) {
                    confirmActive(this);

                } else {
                    confirmInactive(this);
                }

                break;
            }

            case R.id.action_print_id: {
                new PrinterManager().printRegId(dataManager, tableDemographics.getRegistrationId(), tableDemographics.getFullName());
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActive(boolean active, String inactiveReason) {

        if (active) {
            tableDemographics.setInactive(false);
            tableDemographics.setInactiveReason("");
            dataManager.updatePatient(tableDemographics);
            getActivity().invalidateOptionsMenu();

        } else {
            tableDemographics.setInactive(true);
            tableDemographics.setInactiveReason(inactiveReason);
            dataManager.updatePatient(tableDemographics);
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void setFabVisibility(boolean visible) {

        if (visible) {
            fabMenu.setVisibility(View.VISIBLE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg));

        } else {
            fabMenu.setVisibility(View.GONE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg_inactive));
        }
    }

    private void initLanguage() {
        fabEditDemographics.setLabelText(AssetReader.getLangKeyword("edit_person_info"));
        fabEditHouseInfo.setLabelText(AssetReader.getLangKeyword("edit_house_info"));
    }
}