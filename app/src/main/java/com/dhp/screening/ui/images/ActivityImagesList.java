package com.dhp.screening.ui.images;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterImages;
import com.dhp.screening.ui.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ActivityImagesList extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String registrationId;

    @BindView(R.id.rv_images)
    RecyclerView rvImages;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @Inject
    DataManager dataManager;

    private AdapterImages adapterImages;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images_list);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Images/Reports", true);

        getActivityComponent(this).inject(this);

        if (savedInstanceState == null) {
            registrationId = getIntent().getStringExtra("registration_id");
        } else {
            registrationId = savedInstanceState.getString("registration_id");
        }

        adapterImages = new AdapterImages(this);
        rvImages.setLayoutManager(new LinearLayoutManager(this));
        rvImages.setAdapter(adapterImages);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("registration_id", registrationId);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapterImages.setItems(dataManager.getImages(registrationId));

        if (0 == adapterImages.getItemCount()) {
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            tvEmpty.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_add)
    void onAddClicked() {
        Intent intent = new Intent(this, ActivityAddImage.class);
        intent.putExtra("registration_id", registrationId);
        startActivity(intent);
    }
}