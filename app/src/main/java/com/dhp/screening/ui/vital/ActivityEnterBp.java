package com.dhp.screening.ui.vital;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemMinMax;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.lib.bluetooth.BleCallback;
import com.dhp.screening.lib.bluetooth.BleConnection;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;
import com.getmedcheck.lib.MedCheck;
import com.getmedcheck.lib.model.BleDevice;
import com.getmedcheck.lib.model.BloodPressureData;
import com.getmedcheck.lib.model.IDeviceData;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_PLUS;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_START_TEST_BP;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.BATTERY_LEVEL;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_FINAL_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_START_TEST_NOW;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_TEST_ERROR;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_TEST_IN_PROGRESS;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_TEST_STARTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_WAIT;
import static com.dhp.screening.lib.bluetooth.BleConstants.SFBP;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTING;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_DISCONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.TECHNAXX_BP_DEVICE;
import static com.dhp.screening.util.LUtils.formatFloat;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.hasValue;
import static com.dhp.screening.util.LUtils.roundOff;
import static com.dhp.screening.util.LView.resetRangeColor;


public class ActivityEnterBp extends MedCheckActivity
        implements BleCallback {

    private int connectionState = STATE_DISCONNECTED;

    private boolean isEdit;
    private boolean testStarted;
    private boolean isOnceConnected;

    private String values[];

    private static final String TAG = ActivityEnterBp.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_bp)
    Button btnBp;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.et_systolic)
    EditText etSystolic;

    @BindView(R.id.et_diastolic)
    EditText etDiastolic;

    @BindView(R.id.et_unit)
    EditText etUnit;

    @BindView(R.id.tv_range_text)
    TextView tvRangeText;

    @BindView(R.id.ll_pulse)
    LinearLayout llPulse;

    @BindView(R.id.et_pulse)
    EditText etPulse;

    @BindView(R.id.tv_battery_level)
    TextView tvBattery;

    @BindView(R.id.tv_text)
    TextView tvValue;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @Inject
    DataManager dataManager;

    private ProgressDialog progressDialog;

    private BleConnection bleConnection;

    private TableVitals tableVitals;

    private String registrationId;

    private String deviceName = "";
    private String deviceId   = "";
    private String macAddress = "";

    private ArrayList<ItemBluetoothDevice> itemBluetoothDevices;

    private String connectedDeviceName;

    private boolean isTestComplete;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_bp);
        initToolbar(toolbar, getString(R.string.enter_bp), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.enter_bp));

        progressDialog = new ProgressDialog(this, R.style.AlertDialogLight);
        bleConnection = new BleConnection();

        itemBluetoothDevices = dataManager.getBluetoothDevices("bp");

        if (AssetReader.isAllowSave("Blood Pressure")) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
            btnSave.setBackgroundResource(R.drawable.btn_background_disabled);
        }

        deviceId = getDeviceId();

        tableVitals = new TableVitals();

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit = true;
            tableVitals = StaticData.getCurrentVital();
            String[] values = tableVitals.getVitalValues().split(";");

            etSystolic.setText(values[0]);
            etDiastolic.setText(values[1]);
            etPulse.setText(values[2]);

            etNotes.setText(tableVitals.getNotes());
            deviceName = tableVitals.getDeviceName();
            deviceId = tableVitals.getDeviceId();

            btnSave.setText("Update");

            btnSave.setEnabled(true);
            btnSave.setBackgroundResource(R.drawable.btn_background);
        }

        registrationId = StaticData.getRegistrationId();

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        boolean popupShown = false;

        if (!isEdit) {
            popupShown = checkAlreadyEntered(registrationId, "Systolic,Diastolic,Pulse");
        }

        if (!popupShown) {
            showPopupMessage("Blood Pressure", "blood_pressure");
        }

        etSystolic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                onValueChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        etDiastolic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
                onValueChanged();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        registerCallback();
    }

    private void onValueChanged() {
        double systolic  = 0.0;
        double diastolic = 0.0;

        try {
            systolic = Double.valueOf(etSystolic.getText().toString());
            diastolic = Double.valueOf(etDiastolic.getText().toString());
        } catch (Exception e) {
            resetRangeColor(etUnit);
            tvRangeText.setText("");
            return;
        }

        ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic, true);

        if (!itemRange.getColor().isEmpty()) {
            etUnit.setBackgroundColor(Color.parseColor(itemRange.getColor()));
            tvRangeText.setText(itemRange.getRange());
        } else {
            resetRangeColor(etUnit);
            tvRangeText.setText("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        bleConnection.init(this, this);
    }

    @Override
    protected void onPause() {
        bleConnection.deInit();
        super.onPause();
    }

    @OnClick(R.id.btn_bp)
    void onButtonClicked() {

        switch (btnBp.getText().toString()) {
            case "Connect": {
                bleConnection.startScan();
                showLoader("Connecting...");
                startConnectionTimer();
            }
            break;

            case "Start Test": {
                etSystolic.setText("");
                etDiastolic.setText("");
                etPulse.setText("");

                if (connectedDeviceName.equals(ASHA_PLUS)) {
                    bleConnection.startAshaTest(ASHA_START_TEST_BP);
                } else {
                    bleConnection.startTest();
                    btnBp.setEnabled(false);
                }

                btnBp.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.stop_test), null, null, null);
                btnBp.setText("Stop Test");
                setText(tvValue, -1, "Test in progress...");
            }
            break;

            case "Stop Test": {
                if (connectedDeviceName.equals(ASHA_PLUS)) {
                    bleConnection.stopAshaTest();
                } else {
                    bleConnection.stopTest();
                }

                btnBp.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.start_test), null, null, null);
                btnBp.setText("Start Test");
                setText(tvValue, -1, "");
            }
            break;
        }
    }

    private void startConnectionTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (STATE_CONNECTED != connectionState && !isOnceConnected) {
                    bleConnection.disconnect();
                    dismissLoader();
                    LToast.errorLong(R.string.unable_to_connect);
                    connectionState = STATE_DISCONNECTED;
                }
            }
        }, 20000);
    }

    private void showLoader(String message) {
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void dismissLoader() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        bleConnection.disconnect();

        if (!hasValue(connectedDeviceName) || connectedDeviceName.equals(ASHA_PLUS)) {
            bleConnection.turnOffBle();
        }

        super.onDestroy();
    }

    @Override
    public void onBleScan(BluetoothDevice device) {

        if (null != itemBluetoothDevices) {
            if (LUtils.toConnectDevice(device.getName(), device.getAddress(), itemBluetoothDevices)) {
                tryConnectingToDevice(device);
            }
        } else if (null != device.getName()) {
            tryConnectingToDevice(device);
        }
    }

    private void tryConnectingToDevice(BluetoothDevice device) {

        switch (device.getName()) {

            case TECHNAXX_BP_DEVICE: {
                tryConnecting(device);
                break;
            }

            case SFBP: {
                tryConnectingMedcheck(device);
                isTestComplete = false;
                break;
            }

            case ASHA_PLUS: {
                tryConnectingToAsha(device);
                break;
            }
        }
    }

    private void tryConnectingMedcheck(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState && !isTestComplete) {

            connectedDeviceName = device.getName();

            MedCheck.getInstance().connect(this, device.getAddress());
            connectionState = STATE_CONNECTING;
            macAddress = device.getAddress();

            if (!testStarted) {
                tvValue.setText("Connecting...");
            }
        }
    }

    private void tryConnectingToAsha(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            connectedDeviceName = device.getName();
            connectionState = STATE_CONNECTING;
            bleConnection.connectToAsha(device);
        }
    }

    private void tryConnecting(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            bleConnection.connectDevice(device);
            connectionState = STATE_CONNECTING;
            macAddress = device.getAddress();
            connectedDeviceName = device.getName();
        }
    }

    @Override
    protected void onDeviceConnectionStateChange(BleDevice bleDevice, int status) {
        super.onDeviceConnectionStateChange(bleDevice, status);
        if (bleDevice.getMacAddress().equals(macAddress) && status == 1) {
            onConnected();
        } else {
            onDisconnected();
        }
    }

    @Override
    protected void onDeviceDataReadingStateChange(int state, String message) {
        super.onDeviceDataReadingStateChange(state, message);

        switch (state) {
            case 1: {
                setText(tvValue, BP_TEST_IN_PROGRESS, message);
                break;
            }
            case 2: {
                setText(tvValue, BP_TEST_IN_PROGRESS, "Connected. Please wait.....");
                break;
            }
            case 3: {
                if (testStarted) {
//                    setText(tvValue, BP_TEST_IN_PROGRESS, message);
                }
                break;
            }
            case 4: {
                if (!testStarted) {
                    setText(tvValue, BP_START_TEST_NOW, "Start the test now (Click the centre button)");
                }
                break;
            }

            case 7: {
                testStarted = true;
                setText(tvValue, BP_TEST_IN_PROGRESS, message);

                bleConnection.stopScan();
                bleConnection.disconnect();
                bleConnection.turnOffBle();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        connectionState = STATE_DISCONNECTED;
                        bleConnection.startScan();
                    }
                }, 5000);

                break;
            }

            case 8: {
                if (!testStarted) {
                    setText(tvValue, BP_START_TEST_NOW, "Start the test now (Click the centre button)");
                }
            }
        }
    }

    @Override
    protected void onDeviceDataReceive(BluetoothDevice device, ArrayList<IDeviceData> deviceData, String json, String deviceType) {
        super.onDeviceDataReceive(device, deviceData, json, deviceType);
        if (deviceData == null) {
            return;
        }

        if (deviceData.size() == 0) {
            if (testStarted) {
                setText(tvValue, BP_TEST_ERROR, "Error. \n" +
                        "Retry (Click the centre button)");
            }
            return;
        } else if (testStarted) {
            try {
                BloodPressureData bloodPressureData = (BloodPressureData) deviceData.get(deviceData.size() - 1).getObject();
                onValueRead(BP_FINAL_RESULT, bloodPressureData.getSystolic() + ";" + bloodPressureData.getDiastolic() + ";" + bloodPressureData.getHeartRate());
                MedCheck.getInstance().clearDevice(this, macAddress);

                bleConnection.stopScan();
                bleConnection.disconnect();
                bleConnection.turnOffBle();

                connectionState = STATE_DISCONNECTED;

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onConnected() {
        LLog.d(TAG, "onConnected");
        connectionState = STATE_CONNECTED;
        isOnceConnected = true;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnSave.setEnabled(true);
                btnSave.setBackgroundResource(R.drawable.btn_background);
                dismissLoader();
                btnBp.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.start_test), null, null, null);
                btnBp.setText("Start Test");

                if (connectedDeviceName.equals(SFBP)) {
                    btnBp.setVisibility(View.INVISIBLE);
                    tvBattery.setVisibility(View.GONE);
                }

                if (connectedDeviceName.equals(ASHA_PLUS)) {
                    tvBattery.setVisibility(View.GONE);
                    llPulse.setVisibility(View.GONE);
                    tvValue.setText("Connected");
                }
            }
        });
    }

    @Override
    public void onServicesDiscovered() {
        bleConnection.getBatteryLevel();

        if (!connectedDeviceName.equals(TECHNAXX_BP_DEVICE)) {
            bleConnection.startTest();
        }
    }

    @Override
    public void onDisconnected() {
        if (STATE_DISCONNECTED == connectionState) {
            return;
        }

        connectionState = STATE_DISCONNECTED;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissLoader();
                btnBp.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.connect), null, null, null);
                btnBp.setText("Connect");

                if (!isOnceConnected) {
                    tvValue.setText("");
                    btnBp.setVisibility(VISIBLE);
                    btnBp.setEnabled(true);
                }
            }
        });
    }

    @Override
    public void onValueRead(int type, String value) {

        switch (type) {

            case BATTERY_LEVEL: {
                setText(tvBattery, type, "Device Battery Level: " + value + "%");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Don't auto start
//                        btnBp.callOnClick();
                    }
                });
                break;
            }

            case BP_TEST_STARTED: {
                setText(tvValue, type, "Test in progress... ");
                testStarted = true;
                break;
            }

            case BP_WAIT: {
                if (!testStarted) {
                    setText(tvValue, type, "Please wait...");
                }
                break;
            }

            case BP_START_TEST_NOW: {
                if (!testStarted && !isTestComplete) {
                    setText(tvValue, type, "Start the test now (Click the centre button)");
                }
                break;
            }

            case BP_TEST_IN_PROGRESS: {
                setText(tvValue, type, "Test in progress... " + value);
                break;
            }

            case BP_TEST_ERROR: {
                testStarted = false;
                setText(tvValue, type, "Error. Try again");
                break;
            }

            case BP_FINAL_RESULT: {

                if (testStarted) {
                    this.values = value.split(";");
                    setText(tvValue, type, "Test complete");

                    isTestComplete = true;
                }

//                testStarted = false;
                break;
            }

            case ASHA_TEST_RESULT: {
                value = value.split("#")[0];
                String[] items = value.split("_");

                switch (items[0]) {
                    case "C": {
                        setText(tvValue, BP_TEST_IN_PROGRESS, items[1]);
                        break;
                    }

                    case "B": {
                        values = new String[3];
                        values[0] = items[1];
                        values[1] = items[2];
                        values[2] = "";
                        setText(tvValue, BP_FINAL_RESULT, "Test complete");
                        break;
                    }

                    case "E": {
                        setText(tvValue, BP_TEST_ERROR, items[1]);
                    }
                }
                // C_meansuring
                // B_133_74
                // E_ Air Leakage
                break;
            }
        }
    }

    @Override
    public void onFailure() {

    }

    private void setText(final TextView view, final int type, final String text) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setText(text);

                // set color
                tvValue.setTextColor(ContextCompat.getColor(ActivityEnterBp.this, R.color.colorPrimaryDark));

                if (BP_TEST_ERROR == type) {
                    btnBp.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.start_test), null, null, null);
                    btnBp.setText("Start Test");
                    btnBp.setEnabled(true);

                    tvValue.setTextColor(ContextCompat.getColor(ActivityEnterBp.this, R.color.red));
                    // Set color in red

//                    if ((connectedDeviceName.equals(SFBP))) {
//                        bleConnection.disconnect();
//                        btnBp.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.connect), null, null, null);
//                        btnBp.setText("Connect");
//                        btnBp.setEnabled(true);
//                        btnBp.setVisibility(View.VISIBLE);
//                    }

                } else if (BP_FINAL_RESULT == type) {
                    btnBp.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.start_test), null, null, null);
                    btnBp.setText("Start Test");
                    btnBp.setEnabled(true);

                    etSystolic.setText(values[0]);
                    etDiastolic.setText(values[1]);
                    etPulse.setText(values[2]);

                    deviceName = connectedDeviceName;
                    deviceId = macAddress;

                } else if (BP_START_TEST_NOW == type) {
                    tvValue.setTextColor(ContextCompat.getColor(ActivityEnterBp.this, R.color.red));
                }
            }
        });
    }

    @OnClick(R.id.btn_save)
    void onSave() {

        float systolic  = 0.0f;
        float diastolic = 0.0f;
        float pulse     = 0.0f;

        try {
            systolic = Float.valueOf(etSystolic.getText().toString().trim());
            diastolic = Float.valueOf(etDiastolic.getText().toString().trim());
            pulse = Float.valueOf(etPulse.getText().toString().trim());
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (etSystolic.getText().toString().isEmpty() || 0 == systolic) {
            LToast.warning("Enter systolic");
            return;

        } else if (etDiastolic.getText().toString().isEmpty() || 0 == diastolic) {
            LToast.warning("Enter diastolic");
            return;

        } else if ((etPulse.getText().toString().isEmpty() || 0 == pulse) && !(null != connectedDeviceName && connectedDeviceName.equals(ASHA_PLUS))) {
            LToast.warning("Enter pulse");
            return;
        }

        if (isEdit) {
            tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

        } else {
            if (VISIBLE == llAadhaar.getVisibility()) {
                registrationId = getRegistrationId();
            }

            if (null == registrationId || registrationId.equals("")) {
                LToast.warning("Enter Registration ID");
                return;
            }

            tableVitals.setRegistrationId(registrationId.toUpperCase());
        }

        ItemMinMax minMax = AssetReader.getVitalRange("BP Systolic");
        if (systolic < minMax.getMin() || systolic > minMax.getMax()) {
            LToast.warningLong("Systolic value should be between " + formatFloat(minMax.getMin()) + " mmHg - " + formatFloat(minMax.getMax()) + " mmHg");
            return;
        }

        minMax = AssetReader.getVitalRange("BP Diastolic");
        if (diastolic < minMax.getMin() || diastolic > minMax.getMax()) {
            LToast.warningLong("Diastolic value should be between " + formatFloat(minMax.getMin()) + " mmHg - " + formatFloat(minMax.getMax()) + " mmHg");
            return;
        }

        minMax = AssetReader.getVitalRange("Pulse");
        if (diastolic < minMax.getMin() || diastolic > minMax.getMax()) {
            LToast.warningLong("Pulse value should be between " + formatFloat(minMax.getMin()) + " bpm - " + formatFloat(minMax.getMax()) + " bpm");
            return;
        }

        tableVitals.setVitalType("Blood Pressure");

        tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
        tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

        tableVitals.setVitalSubTypes("Systolic,Diastolic,Pulse");
        tableVitals.setVitalValues(roundOff(etSystolic.getText().toString())
                + ";" + roundOff(etDiastolic.getText().toString())
                + ";" + roundOff(etPulse.getText().toString()));
        tableVitals.setVitalUnits("mmHg;mmHg;bpm");
        tableVitals.setNotes(etNotes.getText().toString());
        tableVitals.setDeviceName(deviceName);
        tableVitals.setGpsLocation(getLocation());
        tableVitals.setQrScanned(qrScanned);

        if (isEdit) {
            dataManager.updateVital(tableVitals, deviceId);
            LToast.success(AssetReader.getLangKeyword("updated"));
            StaticData.setCurrentVital(tableVitals);
        } else {
            dataManager.addVital(tableVitals, deviceId);
            LToast.success(AssetReader.getLangKeyword("saved"));
        }

        finish();
    }


}