package com.dhp.screening.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.admin.ActivityAdmin4Settings;
import com.dhp.screening.ui.admin.ActivityAdminHemoglobin;
import com.dhp.screening.ui.admin.ActivityAdminHome;
import com.dhp.screening.ui.admin.ActivityDeviceNumberSettings;
import com.dhp.screening.ui.admin.ActivityExportSettings;
import com.dhp.screening.ui.admin.ActivityImportSettings;
import com.dhp.screening.ui.admin.ActivityPartnerIdSettings;
import com.dhp.screening.ui.user.ActivityAbout;
import com.dhp.screening.ui.user.ActivityUserHome;
import com.dhp.screening.util.LToast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static com.dhp.screening.data.manager.SharedPrefsHelper.SUPER_ADMIN_1;
import static com.dhp.screening.data.manager.SharedPrefsHelper.SUPER_ADMIN_2;
import static com.dhp.screening.data.manager.SharedPrefsHelper.SUPER_ADMIN_3;
import static com.dhp.screening.util.LConstants.ADMIN_PASSWORD;
import static com.dhp.screening.util.LConstants.DEFAULT_PARTNER_ID;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_1_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_2_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_3_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_4_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_DEVICE_NUMBER_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_EXPORT_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_HB_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_IMPORT_PASSWORD;
import static com.dhp.screening.util.LConstants.SUPER_ADMIN_PARTNER_ID_PASSWORD;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityLogin extends BaseActivity {

    @BindView(R.id.et_username)
    EditText etUserName;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.tv_login_user)
    TextView tvLoginUser;

    @BindView(R.id.tv_login_admin)
    TextView tvLoginAdmin;

    @BindView(R.id.ll_user)
    LinearLayout llUser;

    @BindView(R.id.ll_admin)
    LinearLayout llAdmin;

    @BindView(R.id.cb_remember_me)
    CheckBox cbRememberMe;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_login)
    Button btnLogin;

    @Inject
    DataManager dataManager;

    private boolean userLogin = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initToolbar(toolbar, AssetReader.getLangKeyword("ncd_screening"), false);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        initLanguageMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_change_language:
                String currentSelectedLanguage = dataManager.getStringPref("selected_language", "English");
                changeLanguage(currentSelectedLanguage);
                return true;

            case R.id.action_about:
                startActivity(new Intent(this, ActivityAbout.class));
                return true;

            case R.id.action_help:

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        AssetReader.openHelp(ActivityLogin.this);
                    }
                }, 150);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.ll_admin)
    void adminLogin() {
        if (!userLogin) {
            return;
        }

        userLogin = false;

        tvLoginUser.setTextColor(ContextCompat.getColor(this, R.color.color_grey_dark));
        tvLoginAdmin.setTextColor(ContextCompat.getColor(this, R.color.white));

        llUser.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        llAdmin.setBackgroundColor(ContextCompat.getColor(this, R.color.green));

        btnLogin.setText(AssetReader.getLangKeyword("login_as_admin"));

        etUserName.setVisibility(View.GONE);

        clearFields();
    }

    @OnClick(R.id.ll_user)
    void userLogin() {

        if (userLogin) {
            return;
        }
        userLogin = true;

        tvLoginUser.setTextColor(ContextCompat.getColor(this, R.color.white));
        tvLoginAdmin.setTextColor(ContextCompat.getColor(this, R.color.color_grey_dark));

        llUser.setBackgroundColor(ContextCompat.getColor(this, R.color.green));
        llAdmin.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        btnLogin.setText(AssetReader.getLangKeyword("login_as_user"));

        etUserName.setVisibility(View.VISIBLE);

        clearFields();
        cbRememberMe.setChecked(true);
    }

    private void clearFields() {
        etUserName.setText("");
        etPassword.setText("");
        cbRememberMe.setChecked(false);
        etUserName.requestFocus();
    }

    @OnClick(R.id.btn_login)
    void login() {

        if (!userLogin) {
            if (etPassword.getText().toString().equals(ADMIN_PASSWORD)) {
                onAdminLoggedIn();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_1_PASSWORD)) {
                dataManager.setAdminType(SUPER_ADMIN_1);
                onAdminLoggedIn();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_2_PASSWORD)) {
                dataManager.setAdminType(SUPER_ADMIN_2);
                onAdminLoggedIn();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_3_PASSWORD)) {
                dataManager.setAdminType(SUPER_ADMIN_3);
                onAdminLoggedIn();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_HB_PASSWORD)) {
                onAdminHbLoggedIn();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_4_PASSWORD)) {
                onAdmin4LoggedIn();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_IMPORT_PASSWORD)) {
                dataManager.adminImportLoggedIn(cbRememberMe.isChecked());
                startActivity(new Intent(this, ActivityImportSettings.class));
                finish();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_EXPORT_PASSWORD)) {
                dataManager.adminExportLoggedIn(cbRememberMe.isChecked());
                startActivity(new Intent(this, ActivityExportSettings.class));
                finish();

            } else if (etPassword.getText().toString().equals(SUPER_ADMIN_PARTNER_ID_PASSWORD)) {
                dataManager.adminPartnerIdLoggedIn(cbRememberMe.isChecked());
                startActivity(new Intent(this, ActivityPartnerIdSettings.class));
                finish();

            } else if (hasValue(dataManager.getPartnerId())
                    && !dataManager.getPartnerId().equals(DEFAULT_PARTNER_ID)
                    && etPassword.getText().toString().toLowerCase().equals(dataManager.getPartnerId().toLowerCase() + SUPER_ADMIN_DEVICE_NUMBER_PASSWORD)) {
                dataManager.adminDeviceNumberLoggedIn(cbRememberMe.isChecked());
                startActivity(new Intent(this, ActivityDeviceNumberSettings.class));
                finish();

            } else {
                Toasty.warning(LApplication.getContext(), AssetReader.getLangKeyword("invalid_credentials"),
                        Toast.LENGTH_SHORT).show();
            }
            return;
        }

        TableUser tableUser = dataManager.validateUserLogin(etUserName.getText().toString(), etPassword.getText().toString());

        if (null != tableUser) {
            if (tableUser.isInactive()) {
                LToast.errorLong(AssetReader.getLangKeyword("user_inactive"));
            } else {
                dataManager.userLoggedIn(cbRememberMe.isChecked(), tableUser);
                startActivity(new Intent(this, ActivityUserHome.class));
                finish();
            }
        } else {
            Toasty.warning(LApplication.getContext(), AssetReader.getLangKeyword("invalid_credentials"),
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void onLanguageSet(String selectedLanguage) {
        String currentSelectedLanguage = dataManager.getStringPref("selected_language", "English");

        if (!currentSelectedLanguage.equals(selectedLanguage)) {

            if (AssetReader.initSelectedLanguage(selectedLanguage)) {
                dataManager.setPref("selected_language", selectedLanguage);

            } else {
                dataManager.setPref("selected_language", "English");
                LToast.error(R.string.unable_to_set_language);
            }
        }
        recreate();
    }

    private void initLanguage() {
        tvLoginUser.setText(AssetReader.getLangKeyword("user"));
        tvLoginAdmin.setText(AssetReader.getLangKeyword("admin"));
        etUserName.setHint(AssetReader.getLangKeyword("username"));
        etPassword.setHint(AssetReader.getLangKeyword("password"));
        cbRememberMe.setText(AssetReader.getLangKeyword("remember_me"));
        btnLogin.setText(AssetReader.getLangKeyword("login_as_user"));
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_change_language);
        item.setTitle(AssetReader.getLangKeyword("change_language"));

        item = menu.findItem(R.id.action_help);
        item.setTitle(AssetReader.getLangKeyword("help"));

        item = menu.findItem(R.id.action_about);
        item.setTitle(AssetReader.getLangKeyword("about"));
    }

    private void onAdminLoggedIn() {
        dataManager.adminLoggedIn(cbRememberMe.isChecked());
        startActivity(new Intent(this, ActivityAdminHome.class));
        finish();
    }

    private void onAdminHbLoggedIn() {
        dataManager.adminHbLoggedIn(cbRememberMe.isChecked());
        startActivity(new Intent(this, ActivityAdminHemoglobin.class));
        finish();
    }

    private void onAdmin4LoggedIn() {
        dataManager.admin4LoggedIn(cbRememberMe.isChecked());
        startActivity(new Intent(this, ActivityAdmin4Settings.class));
        finish();
    }
}