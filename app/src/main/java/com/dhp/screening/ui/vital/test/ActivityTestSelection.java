package com.dhp.screening.ui.vital.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemVitalTest;
import com.dhp.screening.ui.vital.ActivityVitalBase;
import com.dhp.screening.util.LView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ActivityTestSelection extends ActivityVitalBase {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.btn_test_1)
    Button btnTest1;

    @BindView(R.id.btn_test_2)
    Button btnTest2;

    private String testType;

    private ItemVitalTest vitalTest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_test_selection);
        ButterKnife.bind(this);

        testType = getIntent().getStringExtra("test_type");

        initToolbar(toolbar, testType, true);
        LView.setIcon(ivIcon, testType);

        tvTitle.setText(testType);

        vitalTest = AssetReader.getVitalTest(testType);

        if (null == vitalTest) {
            finish();
        }

        if (vitalTest.getTestTypes().length == 2) {
            btnTest2.setText(vitalTest.getTestTypes()[1].getTestType());
            btnTest2.setVisibility(View.VISIBLE);
        }

        btnTest1.setText(vitalTest.getTestTypes()[0].getTestType());
    }

    @OnClick(R.id.btn_test_1)
    void onBtn1Clicked() {
        Intent intent = new Intent(this, ActivityTestSteps.class);
        intent.putExtra("test_type", testType);
        intent.putExtra("test_sub_type", vitalTest.getTestTypes()[0].getTestType());
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_test_2)
    void onBtn2Clicked() {
        Intent intent = new Intent(this, ActivityTestSteps.class);
        intent.putExtra("test_type", testType);
        intent.putExtra("test_sub_type", vitalTest.getTestTypes()[1].getTestType());
        startActivity(intent);
        finish();
    }
}