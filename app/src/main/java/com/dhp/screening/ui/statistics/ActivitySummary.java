package com.dhp.screening.ui.statistics;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterSummary;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemSummary;
import com.dhp.screening.data.item.ItemSummaryPrint;
import com.dhp.screening.data.item.ItemSummaryView;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.range.ItemRangeCount;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.lib.printer.PrinterManager;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dhp.screening.data.item.ItemSummaryView.TYPE_DIVIDER;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_HEADER;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_BMI;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_BP;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_HB;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_SUGAR;


public class ActivitySummary extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_total_demographics)
    TextView tvTotalDemographics;

    @BindView(R.id.tv_male)
    TextView tvMale;

    @BindView(R.id.tv_female)
    TextView tvFemale;

    @BindView(R.id.tv_other)
    TextView tvOther;

    @BindView(R.id.tv_height)
    TextView tvHeight;

    @BindView(R.id.tv_weight)
    TextView tvWeight;

    @BindView(R.id.tv_bp)
    TextView tvBp;

    @BindView(R.id.tv_sugar)
    TextView tvSugar;

    @BindView(R.id.tv_hemoglobin)
    TextView tvHemoglobin;

    @BindView(R.id.tv_prescription)
    TextView tvPrescription;

    @BindView(R.id.tv_total_vitals)
    TextView tvTotalVitals;

    @BindView(R.id.rv_summary)
    RecyclerView rvSummary;

    @Inject
    DataManager dataManager;

    ArrayList<ItemSummaryPrint> summaryPrint = new ArrayList<>(16);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);
        initToolbar(toolbar, AssetReader.getLangKeyword("summary"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        ItemSummary itemSummary = dataManager.getSurveySummary();

        ArrayList<ItemSummaryView> summaryViews = new ArrayList<>(16);

        ArrayList<ItemRangeCount> ranges = new ArrayList<>(4);
        ranges.add(new ItemRangeCount("Total", itemSummary.getTotalDemographics()));
        ranges.add(new ItemRangeCount("Male", itemSummary.getMale()));
        ranges.add(new ItemRangeCount("Female", itemSummary.getFemale()));
        ranges.add(new ItemRangeCount("Other", itemSummary.getOthers()));

        tvTotalDemographics.setText("Total Demographics: " + itemSummary.getTotalDemographics());

        tvMale.setText("Male: " + itemSummary.getMale());
        tvFemale.setText("Female: " + itemSummary.getFemale());
        tvOther.setText("Other: " + itemSummary.getOthers());

        tvTotalVitals.setText("Total Vitals: " + itemSummary.getTotalVitals());

        for (ItemRangeCount range : ranges) {
//            summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, range.getRange(), range.getCount()));
        }

        summaryPrint.add(new ItemSummaryPrint("", ranges));

//        summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));

        ranges = new ArrayList<>(4);

        for (String vital : AssetReader.getVitalsList()) {

            switch (vital) {

                case "Height": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Height", itemSummary.getHeight()));
                    tvHeight.setText("Height: " + itemSummary.getHeight());
                    tvHeight.setVisibility(View.VISIBLE);
                    ranges.add(new ItemRangeCount("Height", itemSummary.getHeight()));
                    break;
                }
                case "Weight": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Weight", itemSummary.getWeight()));
                    tvWeight.setText("Weight: " + itemSummary.getWeight());
                    tvWeight.setVisibility(View.VISIBLE);
                    ranges.add(new ItemRangeCount("Weight", itemSummary.getWeight()));
                    break;
                }
                case "Blood Pressure": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Blood Pressure", itemSummary.getBp()));
                    tvBp.setText("BP: " + itemSummary.getBp());
                    tvBp.setVisibility(View.VISIBLE);
                    ranges.add(new ItemRangeCount("Blood Pressure", itemSummary.getBp()));
                    break;
                }
                case "Blood Sugar": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Blood Sugar", itemSummary.getRbs()));
                    tvSugar.setText("Sugar: " + itemSummary.getRbs());
                    tvSugar.setVisibility(View.VISIBLE);
                    ranges.add(new ItemRangeCount("Blood Sugar", itemSummary.getRbs()));
                    break;
                }
                case "Hemoglobin": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Hemoglobin", itemSummary.getHemoglobin()));
                    tvHemoglobin.setText("Hemoglobin: " + itemSummary.getHemoglobin());
                    tvHemoglobin.setVisibility(View.VISIBLE);
                    ranges.add(new ItemRangeCount("Hemoglobin", itemSummary.getHemoglobin()));
                    break;
                }
                case "Albumin": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Albumin", itemSummary.getAlbumin()));
                    ranges.add(new ItemRangeCount("Albumin", itemSummary.getAlbumin()));
                    break;
                }
                case "Blood Group": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Blood Group", itemSummary.getBloodGroup()));
                    ranges.add(new ItemRangeCount("Blood Group", itemSummary.getBloodGroup()));
                    break;
                }
                case "Prescription": {
//                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Prescription", itemSummary.getPrescription()));

                    if (dataManager.toShowPrescription()) {
                        tvPrescription.setText("Prescription: " + itemSummary.getPrescription());
                        tvPrescription.setVisibility(View.VISIBLE);
                    }
                    ranges.add(new ItemRangeCount("Prescription", itemSummary.getPrescription()));
                    break;
                }
            }
        }

        summaryPrint.add(new ItemSummaryPrint("", ranges));

        summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));

        ArrayList<String> enabledVitals = AssetReader.getVitalsList();

        ArrayList<ItemRangeCount> bmiRanges        = AssetReader.getBmiRanges(false);
        ArrayList<ItemRangeCount> bpRanges         = AssetReader.getBpRanges();
        ArrayList<ItemRangeCount> rbsRanges        = AssetReader.getRbsRanges();
        ArrayList<ItemRangeCount> fbsRanges        = AssetReader.getFbsRanges();
        ArrayList<ItemRangeCount> ppbsRanges       = AssetReader.getPpbsRanges();
        ArrayList<ItemRangeCount> hemoglobinRanges = AssetReader.getHemoglobinRanges();

        List<TableDemographics> tableDemographicsList = dataManager.getAllPatients();

        for (TableDemographics tableDemographics : tableDemographicsList) {
            if (tableDemographics.isInactive()) {
                continue;
            }

            List<TableVitals> vitals = dataManager.getAllVitals(tableDemographics.getRegistrationId(), false);

            for (TableVitals tableVital : vitals) {

                switch (tableVital.getVitalType()) {

                    case "Weight": {
                        if (!enabledVitals.contains("Weight")) {
                            break;
                        }

                        final String latestHeight = dataManager.getLatestHeight(tableVital.getRegistrationId(), tableVital.getVitalDateTimeMillis());

                        if (null != latestHeight) {
                            String[] height = latestHeight.split(";");

                            float weight = 0.0f;

                            try {
                                weight = Float.parseFloat(tableVital.getVitalValues());
                            } catch (Exception e) {
                            }

                            double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                    height[1],
                                    weight);

                            ItemRange itemRange = AssetReader.getBmiRange(bmi, LUtils.calculateAge(tableDemographics.getYob()), true);
                            setRangeCount(bmiRanges, itemRange);
                        }
                        break;
                    }

                    case "Blood Pressure": {

                        if (!enabledVitals.contains("Blood Pressure")) {
                            break;
                        }

                        double systolic  = 0.0;
                        double diastolic = 0.0;

                        try {
                            systolic = Double.valueOf(tableVital.getVitalValues().split(";")[0]);
                            diastolic = Double.valueOf(tableVital.getVitalValues().split(";")[1]);
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic, true);
                        setRangeCount(bpRanges, itemRange);

                        break;
                    }

                    case "Blood Sugar": {
                        if (!enabledVitals.contains("Blood Sugar")) {
                            break;
                        }

                        double value = 0.0;

                        try {
                            value = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getRbsRange(value, tableVital.getVitalSubTypes());

                        if (tableVital.getVitalSubTypes().equals("RBS")) {
                            setRangeCount(rbsRanges, itemRange);

                        } else if (tableVital.getVitalSubTypes().equals("FBS")) {
                            setRangeCount(fbsRanges, itemRange);

                        } else {
                            setRangeCount(ppbsRanges, itemRange);
                        }

                        break;
                    }

                    case "Hemoglobin": {

                        if (!enabledVitals.contains("Hemoglobin")) {
                            break;
                        }

                        double hemoglobin = 0.0;

                        try {
                            hemoglobin = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getHbRange(hemoglobin, tableDemographics.getGender(), LUtils.calculateAge(tableDemographics.getYob()));
                        setRangeCount(hemoglobinRanges, itemRange);

                        break;
                    }

                    case "Albumin": {
                        break;
                    }
                }
            }
        }

        for (String vital : AssetReader.getVitalsList()) {
            switch (vital) {
                case "Weight": {
                    summaryViews.add(new ItemSummaryView(TYPE_HEADER, "BMI", 0));

//                    for (ItemRangeCount rangeCount : bmiRanges) {
//                        summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL, rangeCount.getRange(), rangeCount.getCount()));
//                    }

                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL_BMI, bmiRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));

                    summaryPrint.add(new ItemSummaryPrint("BMI", bmiRanges));
                    break;
                }
                case "Blood Pressure": {
                    summaryViews.add(new ItemSummaryView(TYPE_HEADER, "Blood Pressure", 0));

//                    for (ItemRangeCount rangeCount : bpRanges) {
//                        summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL, rangeCount.getRange(), rangeCount.getCount()));
//                    }

                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL_BP, bpRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));
                    summaryPrint.add(new ItemSummaryPrint("Blood Pressure", bpRanges));

                    break;
                }
                case "Blood Sugar": {
                    summaryViews.add(new ItemSummaryView(TYPE_HEADER, "RBS", 0));

//                    for (ItemRangeCount rangeCount : rbsRanges) {
//                        summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL, rangeCount.getRange(), rangeCount.getCount()));
//                    }
                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL_SUGAR, rbsRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));
                    summaryPrint.add(new ItemSummaryPrint("RBS", rbsRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_HEADER, "FBS", 0));

//                    for (ItemRangeCount rangeCount : fbsRanges) {
//                        summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL_SUGAR, rangeCount.getRange(), rangeCount.getCount()));
//                    }

                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL_SUGAR, fbsRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));
                    summaryPrint.add(new ItemSummaryPrint("FBS", fbsRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_HEADER, "PPBS", 0));
//                    for (ItemRangeCount rangeCount : ppbsRanges) {
//                        summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL, rangeCount.getRange(), rangeCount.getCount()));
//                    }

                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL_SUGAR, ppbsRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));
                    summaryPrint.add(new ItemSummaryPrint("PPBS", ppbsRanges));

                    break;
                }
                case "Hemoglobin": {
                    summaryViews.add(new ItemSummaryView(TYPE_HEADER, "Hemoglobin", 0));
//                    for (ItemRangeCount rangeCount : hemoglobinRanges) {
//                        summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL, rangeCount.getRange(), rangeCount.getCount()));
//                    }

                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY_VITAL_HB, hemoglobinRanges));

                    summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));
                    summaryPrint.add(new ItemSummaryPrint("Hemoglobin", hemoglobinRanges));

                    break;
                }
                case "Albumin": {
                    summaryViews.add(new ItemSummaryView(TYPE_SUMMARY, "Albumin", itemSummary.getAlbumin()));
                    summaryViews.add(new ItemSummaryView(TYPE_DIVIDER, "", 0));
                    break;
                }
            }
        }

        AdapterSummary adapterSummary = new AdapterSummary(this);
        adapterSummary.setItems(summaryViews);

        rvSummary.setLayoutManager(new
                LinearLayoutManager(this));
        rvSummary.setAdapter(adapterSummary);
    }

    private void setRangeCount(ArrayList<ItemRangeCount> ranges, ItemRange itemRange) {
        for (ItemRangeCount range : ranges) {
            if (range.getRange().equals(itemRange.getRange())) {
                range.setCount(range.getCount() + 1);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_print: {
                new PrinterManager().printSummary(dataManager, summaryPrint);
            }
            break;

            case R.id.action_missing_demographics: {

                if (0 < dataManager.getMissingDemographics().size()) {
                    startActivity(new Intent(this, ActivityMissingDemographics.class));

                } else {
                    LToast.success("All demographics information present");
                }
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_summary, menu);
        initLanguageMenu(menu);

        return true;
    }

    private void setText(TextView textView, int count) {
        textView.setText("" + count);
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_print);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("print_summary"));
        }

        item = menu.findItem(R.id.action_missing_demographics);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("missing_demographics"));
        }
    }
}