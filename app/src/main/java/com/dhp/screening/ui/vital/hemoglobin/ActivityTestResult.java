package com.dhp.screening.ui.vital.hemoglobin;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.ui.BaseActivity;

import java.math.BigDecimal;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityTestResult extends BaseActivity {

    @BindView(R.id.tv_hemoglobin)
    TextView tvHemoglobin;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_result);
        ButterKnife.bind(this);
        initToolbar(toolbar, "Result", false);
        String result = StaticData.getHbResult();

        try {
            double f = Double.parseDouble(result);
            tvHemoglobin.setText("Hemoglobin: " + String.format("%.2f", new BigDecimal(f)) + " g/dl");
        } catch (Exception e) {
            tvHemoglobin.setText("Hemoglobin: " + "___" + " g/dl");
        }
    }

    @Override
    public void onBackPressed() {
    }

    @OnClick(R.id.btn_done)
    void onDoneClicked() {
        finish();
    }
}