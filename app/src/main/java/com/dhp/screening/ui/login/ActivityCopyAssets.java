package com.dhp.screening.ui.login;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.CopyAssetsTask;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;

import java.io.File;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

import static com.dhp.screening.util.LConstants.CODE_WRITE_PERMISSION;


public class ActivityCopyAssets extends BaseActivity
        implements CopyAssetsTask.CallBack {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copy_assets);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initToolbar(toolbar, getString(R.string.app_name), false);

        createFolderWithPermissionCheck();
    }

    @Override
    public void onBackPressed() {
    }

    private void createFolderWithPermissionCheck() {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

        if (PackageManager.PERMISSION_GRANTED != permissionCheck) {

            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    CODE_WRITE_PERMISSION);
        } else {
            createFolders();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CODE_WRITE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    createFolders();
                } else {
                    Toasty.warning(this, getString(R.string.message_permission_needed), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            break;
        }
    }

    private void createFolders() {
        createFolder(getString(R.string.ncd_reports_folder));
        createFolder(getString(R.string.export_backup_path));
        createFolder(getString(R.string.export_backup_folder));
        createFolder(getString(R.string.input_path));
        createFolder(getString(R.string.audio_folder));
        createFolder(getString(R.string.icons_folder));
        createFolder(getString(R.string.input_json_folder));

        new CopyAssetsTask(this, this).execute();
    }

    private void createFolder(String folderPath) {

        File filePath = new File(Environment.getExternalStorageDirectory(), folderPath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
    }

    @Override
    public void onAssetCopied(boolean copySuccess) {

        if (copySuccess) {
            dataManager.onAssetsCopied();
            startActivity(new Intent(this, ActivityLoginCheck.class));

        } else {
            LToast.warning(getString(R.string.invalid_apk));
        }

        finish();
    }
}