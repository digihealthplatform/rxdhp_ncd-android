package com.dhp.screening.ui.admin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.util.LToast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEVICE_NUMBER;


public class ActivityAdmin4Settings extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_device_number)
    EditText etDeviceNumber;

    @BindView(R.id.et_partner_name)
    EditText etPartnerName;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin4_settings);

        initToolbar(toolbar, getString(R.string.admin_settings), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        etDeviceNumber.setText(dataManager.getStringPref(KEY_DEVICE_NUMBER, ""));
        etPartnerName.setText(dataManager.getPartnerId());
    }

    @Override
    public void onBackPressed() {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_exit"));
        builder.setPositiveButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityAdmin4Settings.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin4, menu);

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_logout:
                confirmLogout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        String deviceNumber = etDeviceNumber.getText().toString();

        dataManager.setPref(KEY_DEVICE_NUMBER, deviceNumber);

        final TableCampInfo campInfo = new TableCampInfo();
        campInfo.setPartnerId(etPartnerName.getText().toString().toUpperCase());

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));

        builder.setPositiveButton(AssetReader.getLangKeyword("confirm"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                TableCampInfo tableCampInfo = dataManager.getCampInfo();

                if (null != tableCampInfo) {
                    campInfo.setCampName(tableCampInfo.getCampName());
                    campInfo.setStartDate(tableCampInfo.getStartDate());
                    campInfo.setEndDate(tableCampInfo.getEndDate());
                    campInfo.setDoorToDoor(tableCampInfo.isDoorToDoor());
                }

                dataManager.createOrUpdateCampInfo(campInfo);
                LToast.success(AssetReader.getLangKeyword("saved"));
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void confirmLogout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_logout"));
        builder.setPositiveButton(AssetReader.getLangKeyword("logout"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.logout();
                Intent intent = new Intent(ActivityAdmin4Settings.this, ActivityLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setTitle(AssetReader.getLangKeyword("logout"));
    }
}