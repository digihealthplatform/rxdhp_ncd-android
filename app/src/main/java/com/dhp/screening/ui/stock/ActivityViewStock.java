package com.dhp.screening.ui.stock;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterQuestionAnswer;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemQuestionAnswer;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_INACTIVE_REASON;


public class ActivityViewStock extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_stock_details)
    RecyclerView rvStockDetails;

    @BindView(R.id.ll_stock_details)
    LinearLayout llStockDetails;

    @Inject
    DataManager dataManager;

    private AdapterQuestionAnswer adapterQuestionAnswer;

    TableStock tableStock;

    private boolean inactive;

    private String inactiveReason;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_stock);
        initToolbar(toolbar, AssetReader.getLangKeyword("stock"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        rvStockDetails.setLayoutManager(new LinearLayoutManager(this));
        adapterQuestionAnswer = new AdapterQuestionAnswer(this);

        int tableKey = getIntent().getIntExtra("table_key", 0);
        tableStock = dataManager.getStock(tableKey);

        inactive = tableStock.isInactive();

        invalidateOptionsMenu();
    }

    @Override
    public void onResume() {
        super.onResume();

        ArrayList<ItemQuestionAnswer> questionAnswers = new ArrayList<>(16);

        questionAnswers.add(new ItemQuestionAnswer("Purchase date:", tableStock.getPurchasedDate()));
        questionAnswers.add(new ItemQuestionAnswer("Item:", tableStock.getItemName()));
        questionAnswers.add(new ItemQuestionAnswer("Quantity:", "" + tableStock.getQuantity()));
        questionAnswers.add(new ItemQuestionAnswer("Price:", "" + tableStock.getPrice()));
        questionAnswers.add(new ItemQuestionAnswer("Notes:", tableStock.getNotes()));

        adapterQuestionAnswer.setItems(questionAnswers);

        rvStockDetails.setAdapter(adapterQuestionAnswer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_stock_details, menu);

        if (inactive) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            llStockDetails.setBackground(ContextCompat
                    .getDrawable(ActivityViewStock.this, R.drawable.rounded_border_inactive));
        } else {
            llStockDetails.setBackground(ContextCompat
                    .getDrawable(ActivityViewStock.this, R.drawable.rounded_border));
        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_inactive: {

                if (inactive) {
                    confirmActive();
                } else {
                    confirmInactive();
                }
            }
            break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(AssetReader.getLangKeyword("confirm_activate"));
        builder.setPositiveButton(AssetReader.getLangKeyword("activate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tableStock.setInactive(false);
                tableStock.setInactiveReason("");
                dataManager.updateStock(tableStock);

                inactive = false;
                invalidateOptionsMenu();
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        builder.show();
    }

    private void confirmInactive() {

        inactiveReason = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("inactive_reason"));
        builder.setView(dialogView);

        final String[] reasonList = AssetReader.getSpinnerArray(KEY_INACTIVE_REASON);

        final Spinner spReason = (Spinner) dialogView.findViewById(R.id.sp_inactive_reason);
        LView.setSpinnerAdapter(this, spReason, reasonList);

        final EditText etReasonOther = (EditText) dialogView.findViewById(R.id.et_inactive_reason_other);
        etReasonOther.setHint(AssetReader.getLangKeyword("if_other"));

        builder.setPositiveButton(AssetReader.getLangKeyword("done"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        spReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (0 < position) {
                    inactiveReason = reasonList[position];

                    if (inactiveReason.equals("Other")) {
                        etReasonOther.setVisibility(VISIBLE);
                    } else {
                        etReasonOther.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (VISIBLE == etReasonOther.getVisibility()) {
                    inactiveReason = etReasonOther.getText().toString();
                }

                if (inactiveReason.isEmpty()) {
                    LToast.warning("Please enter the reason");
                    return;
                }

                tableStock.setInactive(true);
                tableStock.setInactiveReason(inactiveReason);
                dataManager.updateStock(tableStock);

                inactive = true;
                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_inactive);

        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("inactive"));
        }
    }
}