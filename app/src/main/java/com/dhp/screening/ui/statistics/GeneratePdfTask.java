package com.dhp.screening.ui.statistics;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemUserSurveySummary;
import com.dhp.screening.data.range.ItemAnalysisCount;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.range.ItemRangeCount;
import com.dhp.screening.data.range.ItemRangeCountAnalysis;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.html.simpleparser.HTMLWorker;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.dhp.screening.util.LConstants.ANALYSIS_REPORT;
import static com.dhp.screening.util.LConstants.PDF_MISSING_DEMOGRAPHICS;
import static com.dhp.screening.util.LConstants.PDF_RISK_REPORT;
import static com.dhp.screening.util.LConstants.PDF_VITAL;
import static com.dhp.screening.util.LConstants.PDF_VITAL_INDIVIDUAL;
import static com.dhp.screening.util.LConstants.REPORT_SUMMARY;
import static com.dhp.screening.util.LConstants.STOCK_REPORT;
import static com.dhp.screening.util.LUtils.hasValue;


public class GeneratePdfTask extends AsyncTask<String, Integer, String> {

    private final int pdfType;

    private boolean mError;

    private final DataManager dataManager;

    private Document doc = new Document();

    private ProgressDialog progressDialog;

    private File     outputFile;
    private BaseFont bfBold;

    private BaseFont bf;

    private Context context;

    private String range;

    private int rowCount;
    private int rowCountDemographics;

    private long startDateMillis;
    private long endDateMillis;

    private String reportFormat;
    private String reportDetail;
    private String dateRangeString;
    private String ordering;

    private String registrationId;

    private HashMap<String, Integer> priority = new HashMap<>();
    private String                   vitalRange;

    public GeneratePdfTask(Context context, File outputFile, DataManager dataManager,
                           int pdfType, String range) {
        this.context     = context;
        this.outputFile  = outputFile;
        this.dataManager = dataManager;
        this.pdfType     = pdfType;
        this.range       = range;

        priority.put("Height", 1);
        priority.put("Weight", 2);
        priority.put("Blood Pressure", 3);
        priority.put("Pulse", 4);
        priority.put("Blood Sugar", 5);
        priority.put("Hemoglobin", 6);
        priority.put("Albumin", 7);
        priority.put("Blood Group", 8);
        priority.put("Prescription", 9);
        priority.put("Malaria", 10);
        priority.put("Dengue", 11);
        priority.put("Syphilis", 12);
        priority.put("HIV-AIDS", 13);
        priority.put("Pregnancy", 14);
        priority.put("Heart Attack", 15);
    }

    public void setDateRangeMillis(long startDateMillis, long endDateMillis) {
        this.startDateMillis = startDateMillis;
        this.endDateMillis   = endDateMillis;
    }

    public void setOrdering(String ordering) {
        this.ordering = ordering;
    }

    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    public void setReportDetail(String reportDetail) {
        this.reportDetail = reportDetail;
    }

    public void setDateRangeString(String dateRangeString) {
        this.dateRangeString = dateRangeString;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);

        if (PDF_VITAL == pdfType || PDF_VITAL_INDIVIDUAL == pdfType || PDF_RISK_REPORT == pdfType) {
            progressDialog.setMessage(AssetReader.getLangKeyword("generating"));

        } else if (PDF_MISSING_DEMOGRAPHICS == pdfType) {
            progressDialog.setMessage(AssetReader.getLangKeyword("generating"));

        } else if (REPORT_SUMMARY == pdfType) {
            progressDialog.setMessage(AssetReader.getLangKeyword("generating"));

        } else if (STOCK_REPORT == pdfType) {
            progressDialog.setMessage(AssetReader.getLangKeyword("generating"));

        } else if (ANALYSIS_REPORT == pdfType) {
            progressDialog.setMessage(AssetReader.getLangKeyword("generating"));
        }

        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            if (PDF_VITAL == pdfType) {
                generateVitalPdf();

            } else if (PDF_RISK_REPORT == pdfType) {
                generateRiskReport();

            } else if (PDF_VITAL_INDIVIDUAL == pdfType) {
                generateVitalPdfIndividual();

            } else if (PDF_MISSING_DEMOGRAPHICS == pdfType) {
                generateMissingDemographicsPdf();

            } else if (REPORT_SUMMARY == pdfType) {

                if (reportFormat.equals("PDF")) {
                    if (reportDetail.equals("Detailed (Date-wise)")) {
                        generateSummaryPdfDetailed();
                    } else {
                        generateSummaryPdfBrief();
                    }
                } else {
                    generateSummaryCsv();
                }

            } else if (STOCK_REPORT == pdfType) {
                generateStocksReport();

            } else if (ANALYSIS_REPORT == pdfType) {
                generateAnalysisReport();
            }

        } catch (FileNotFoundException | DocumentException e) {
            mError = true;
        }

        return null;
    }

    private void generateVitalPdfIndividual() throws FileNotFoundException, DocumentException {
        TableDemographics tableDemographics = dataManager.getPatient(registrationId);

        int vitalsCount = 0;

        List<TableVitals> vitals = dataManager.getAllVitals(tableDemographics.getRegistrationId(), false);

        for (TableVitals tableVital : vitals) {
            if (LUtils.getLongTime(tableVital.getVitalDateTime()) >= startDateMillis
                    && LUtils.getLongTime(tableVital.getVitalDateTime()) <= endDateMillis) {
                ++vitalsCount;
            }
        }

        if (0 == vitalsCount) {
            return;
        }

        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(registrationId.getBytes(),
                registrationId.getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.vitals_report_title), 18);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n\n"));

        String text = "Reg. ID: " + tableDemographics.getRegistrationId() + "\n";
        text += "Name: " + tableDemographics.getFullName() + "\n";

        String hoh = dataManager.getHeadOfHouse(tableDemographics.getHouseId());

        text += "Age: " + LUtils.calculateAge(tableDemographics.getYob()) + "\n";

        if (!hoh.isEmpty()) {
            text += "Head of House: " + hoh + "\n";
        }

        text += "\n";

        ArrayList<String> enabledVitals = AssetReader.getVitalsList();

        StringBuilder table = new StringBuilder();

        table.append("<table border=\"2\">");
        table.append("<tr align=\"center\">" +
                "<th>Vital</th>" +
                "<th>Subtype</th>" +
                "<th>Value</th>" +
                "<th>Unit</th>" +
                "<th>Range</th>" +
                "<th>Date time</th>" +
                "</tr>");

        for (TableVitals tableVital : vitals) {
            if (!(LUtils.getLongTime(tableVital.getVitalDateTime()) >= startDateMillis
                    && LUtils.getLongTime(tableVital.getVitalDateTime()) <= endDateMillis)) {
                continue;
            }

            switch (tableVital.getVitalType()) {

                case "Height": {
                    if (!enabledVitals.contains("Height")) {
                        break;
                    }

                    addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                            tableVital.getVitalValues(),
                            tableVital.getVitalUnits(), "");

                    break;
                }

                case "Weight": {
                    if (!enabledVitals.contains("Weight")) {
                        break;
                    }

                    final String latestHeight = dataManager.getLatestHeight(tableVital.getRegistrationId(), tableVital.getVitalDateTimeMillis());

                    if (null != latestHeight) {
                        String[] height = latestHeight.split(";");

                        float weight = 0.0f;

                        try {
                            weight = Float.parseFloat(tableVital.getVitalValues());
                        } catch (Exception e) {
                        }

                        double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                height[1],
                                weight);

                        ItemRange itemRange = AssetReader.getBmiRange(bmi, LUtils.calculateAge(tableDemographics.getYob()));

                        if (range.equals("All")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());

                        } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());

                        } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());
                        }
                    } else {
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), "");
                    }
                    break;
                }

                case "Blood Pressure": {

                    if (!enabledVitals.contains("Blood Pressure")) {
                        break;
                    }

                    double systolic  = 0.0;
                    double diastolic = 0.0;

                    try {
                        systolic  = Double.valueOf(tableVital.getVitalValues().split(";")[0]);
                        diastolic = Double.valueOf(tableVital.getVitalValues().split(";")[1]);
                    } catch (Exception e) {
                        return;
                    }

                    ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic);

                    String[] subtypes = tableVital.getVitalSubTypes().split(",");
                    String[] values   = tableVital.getVitalValues().split(";");
                    String[] units    = tableVital.getVitalUnits().split(";");

                    if (range.equals("All")) {
                        addVitalRow(table, tableVital, subtypes[0] + "/" + subtypes[1],
                                values[0] + "/" + values[1],
                                units[0], itemRange.getRange());

                        if (!values[2].equals("0")) {
                            addVitalRow(table, tableVital, subtypes[2],
                                    values[2],
                                    units[2], "");
                        }

                    } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                        addVitalRow(table, tableVital, subtypes[0] + "/" + subtypes[1],
                                values[0] + "/" + values[1],
                                units[0], itemRange.getRange());

                        if (!values[2].equals("0")) {
                            addVitalRow(table, tableVital, subtypes[2],
                                    values[2],
                                    units[2], "");
                        }

                    } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                        addVitalRow(table, tableVital, subtypes[0] + "/" + subtypes[1],
                                values[0] + "/" + values[1],
                                units[0], itemRange.getRange());

                        if (!values[2].equals("0")) {
                            addVitalRow(table, tableVital, subtypes[2],
                                    values[2],
                                    units[2], "");
                        }
                    }

                    break;
                }

                case "Blood Sugar": {
                    if (!enabledVitals.contains("Blood Sugar")) {
                        break;
                    }

                    double value = 0.0;

                    try {
                        value = Double.valueOf(tableVital.getVitalValues());
                    } catch (Exception e) {
                        return;
                    }

                    ItemRange itemRange = AssetReader.getRbsRange(value, tableVital.getVitalSubTypes());

                    if (range.equals("All")) {
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());

                    } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());

                    } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());
                    }

                    break;
                }

                case "Hemoglobin": {

                    if (!enabledVitals.contains("Hemoglobin")) {
                        break;
                    }

                    double hemoglobin = 0.0;

                    try {
                        hemoglobin = Double.valueOf(tableVital.getVitalValues());
                    } catch (Exception e) {
                        return;
                    }

                    ItemRange itemRange = AssetReader.getHbRange(hemoglobin, tableDemographics.getGender(), LUtils.calculateAge(tableDemographics.getYob()));
                    if (range.equals("All")) {
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());

                    } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());

                    } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());
                    }
                    break;
                }

                case "Albumin": {
                    break;
                }
            }
        }

        table.append("</table>");

        if (0 != rowCountDemographics) {
            toParagraph.add(new Chunk(text));
            doc.add(toParagraph);
        }

        printTable(table.toString());

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    private void generateAnalysisReport() throws DocumentException, FileNotFoundException {
        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(dataManager.getPdfPassword().getBytes(),
                dataManager.getPdfPassword().getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        docWriter.createXmpMetadata();

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.app_name) + ": " + "Analysis Report", 18);

        String campName = "";

        if (null != dataManager.getCampInfo()) {
            campName = dataManager.getCampInfo().getCampName();
        }

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\nPartner ID: " + dataManager.getPartnerId() + "\n"));

        if (hasValue(campName)) {
            toParagraph.add(new Chunk("Camp Name: " + campName + "\n"));
        }

        toParagraph.add(new Chunk("Date: " + dateRangeString + "\n\n"));

        doc.add(toParagraph);

        ArrayList<String> enabledVitals = AssetReader.getVitalsList();

        ArrayList<ItemRangeCountAnalysis> bmiRanges        = getAnalysisCount(AssetReader.getBmiRanges());
        ArrayList<ItemRangeCountAnalysis> bpRanges         = getAnalysisCount(AssetReader.getBpRanges());
        ArrayList<ItemRangeCountAnalysis> rbsRanges        = getAnalysisCount(AssetReader.getRbsRanges());
        ArrayList<ItemRangeCountAnalysis> fbsRanges        = getAnalysisCount(AssetReader.getFbsRanges());
        ArrayList<ItemRangeCountAnalysis> ppbsRanges       = getAnalysisCount(AssetReader.getPpbsRanges());
        ArrayList<ItemRangeCountAnalysis> hemoglobinRanges = getAnalysisCount(AssetReader.getHemoglobinRanges());

        List<TableDemographics> tableDemographicsList = dataManager.getAllPatients();

        for (TableDemographics tableDemographics : tableDemographicsList) {
            if (tableDemographics.isInactive()) {
                continue;
            }

            List<TableVitals> vitals = dataManager.getAllVitals(tableDemographics.getRegistrationId(), false);

            for (TableVitals tableVital : vitals) {

                switch (tableVital.getVitalType()) {

                    case "Weight": {
                        if (!enabledVitals.contains("Weight")) {
                            break;
                        }

                        final String latestHeight = dataManager.getLatestHeight(tableVital.getRegistrationId(), tableVital.getVitalDateTimeMillis());

                        if (null != latestHeight) {
                            String[] height = latestHeight.split(";");

                            float weight = 0.0f;

                            try {
                                weight = Float.parseFloat(tableVital.getVitalValues());
                            } catch (Exception e) {
                            }

                            double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                    height[1],
                                    weight);

                            ItemRange itemRange = AssetReader.getBmiRange(bmi, LUtils.calculateAge(tableDemographics.getYob()));
                            setRangeCount(bmiRanges, itemRange, tableDemographics);
                        }
                        break;
                    }

                    case "Blood Pressure": {

                        if (!enabledVitals.contains("Blood Pressure")) {
                            break;
                        }

                        double systolic  = 0.0;
                        double diastolic = 0.0;

                        try {
                            systolic  = Double.valueOf(tableVital.getVitalValues().split(";")[0]);
                            diastolic = Double.valueOf(tableVital.getVitalValues().split(";")[1]);
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic);
                        setRangeCount(bpRanges, itemRange, tableDemographics);

                        break;
                    }

                    case "Blood Sugar": {
                        if (!enabledVitals.contains("Blood Sugar")) {
                            break;
                        }

                        double value = 0.0;

                        try {
                            value = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getRbsRange(value, tableVital.getVitalSubTypes());

                        if (tableVital.getVitalSubTypes().equals("RBS")) {
                            setRangeCount(rbsRanges, itemRange, tableDemographics);

                        } else if (tableVital.getVitalSubTypes().equals("FBS")) {
                            setRangeCount(fbsRanges, itemRange, tableDemographics);

                        } else {
                            setRangeCount(ppbsRanges, itemRange, tableDemographics);
                        }

                        break;
                    }

                    case "Hemoglobin": {

                        if (!enabledVitals.contains("Hemoglobin")) {
                            break;
                        }

                        double hemoglobin = 0.0;

                        try {
                            hemoglobin = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getHbRange(hemoglobin, tableDemographics.getGender(), LUtils.calculateAge(tableDemographics.getYob()));
                        setRangeCount(hemoglobinRanges, itemRange, tableDemographics);

                        break;
                    }

                    case "Albumin": {
                        break;
                    }
                }
            }
        }

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\nBMI: \n\n"));
        doc.add(toParagraph);

        printTable(bmiRanges);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n\nBlood Pressure: \n\n"));
        doc.add(toParagraph);

        printTable(bpRanges);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n\nRBS: \n\n"));
        doc.add(toParagraph);

        printTable(rbsRanges);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n\nFBS: \n\n"));
        doc.add(toParagraph);

        printTable(fbsRanges);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n\nPPBS: \n\n"));
        doc.add(toParagraph);

        printTable(ppbsRanges);

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    private void printTable(ArrayList<ItemRangeCountAnalysis> vitalRanges) {

        StringBuilder table = new StringBuilder();

        table.append("<table border=\"2\">");
        table.append("<tr align=\"center\">" +
                "<th colspan = \"4\">Gender</th>" +
                "<th colspan = \"3\">Male</th>" +
                "<th colspan = \"3\">Female</th>" +
                "</tr>");

        table.append("<tr align=\"center\">" +
                "<th colspan = \"4\">Age (years)</th>" +
                "<th>0 - 40</th>" +
                "<th>41 - 60</th>" +
                "<th>> 60</th>" +
                "<th>0 - 40</th>" +
                "<th>41 - 60</th>" +
                "<th>> 60</th>" +
                "</tr>");

        for (ItemRangeCountAnalysis ranges : vitalRanges) {
            table.append("<tr align=\"center\">" + "<td colspan = \"4\"> ")
                    .append(ranges.getRange()).append("</td>")
                    .append("<td> ").append(ranges.getMaleAnalysis().getFirstRange()).append("</td>")
                    .append("<td> ").append(ranges.getMaleAnalysis().getSecondRange()).append("</td>")
                    .append("<td> ").append(ranges.getMaleAnalysis().getThirdRange()).append("</td>")
                    .append("<td> ").append(ranges.getFemaleAnalysis().getFirstRange()).append("</td>")
                    .append("<td> ").append(ranges.getFemaleAnalysis().getSecondRange()).append("</td>")
                    .append("<td> ").append(ranges.getFemaleAnalysis().getThirdRange()).append("</td>")
                    .append("</tr>");
        }

        table.append("</table>");
        printTable(table.toString());

    }

    private ArrayList<ItemRangeCountAnalysis> getAnalysisCount(ArrayList<ItemRangeCount> ranges) {
        ArrayList<ItemRangeCountAnalysis> list = new ArrayList<>();

        for (ItemRangeCount item : ranges) {
            ItemRangeCountAnalysis analysis = new ItemRangeCountAnalysis(item.getRange());
            analysis.setMaleAnalysis(new ItemAnalysisCount());
            analysis.setFemaleAnalysis(new ItemAnalysisCount());
            list.add(analysis);
        }

        return list;
    }

    private void setRangeCount(ArrayList<ItemRangeCountAnalysis> ranges, ItemRange itemRange, TableDemographics demographics) {
        for (ItemRangeCountAnalysis range : ranges) {
            if (range.getRange().equals(itemRange.getRange())) {
                ItemAnalysisCount itemAnalysisCount;

                if (demographics.getGender().equals("Male")) {
                    itemAnalysisCount = range.getMaleAnalysis();
                } else {
                    itemAnalysisCount = range.getFemaleAnalysis();
                }

                int age = LUtils.calculateAge(demographics.getYob());

                if (age >= 0 && age <= 40) {
                    itemAnalysisCount.setFirstRange(itemAnalysisCount.getFirstRange() + 1);
                } else if (age > 40 && age <= 60) {
                    itemAnalysisCount.setSecondRange(itemAnalysisCount.getSecondRange() + 1);
                } else if (age > 60) {
                    itemAnalysisCount.setThirdRange(itemAnalysisCount.getThirdRange() + 1);
                }

                if (demographics.getGender().equals("Male")) {
                    range.setMaleAnalysis(itemAnalysisCount);
                } else {
                    range.setFemaleAnalysis(itemAnalysisCount);
                }
            }
        }
    }

    private void generateSummaryPdfDetailed() throws DocumentException, FileNotFoundException {
        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(dataManager.getPdfPassword().getBytes(),
                dataManager.getPdfPassword().getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.app_name) + ": " + "Summary Report", 18);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\nPartner ID: " + dataManager.getPartnerId() + "\n"));

        if (null != dataManager.getCampInfo() && hasValue(dataManager.getCampInfo().getCampName())) {
            toParagraph.add(new Chunk("Camp Name: " + dataManager.getCampInfo().getCampName() + "\n"));
        }

        toParagraph.add(new Chunk("Date: " + dateRangeString + "\n\n\n"));

        doc.add(toParagraph);

        List<TableUser> users = dataManager.getAllActiveUsers();

        Collections.sort(users, new Comparator<TableUser>() {
            @Override
            public int compare(TableUser left, TableUser right) {
                int val = 0;

                try {
                    val = left.getFullName().compareTo(right.getFullName());
                } catch (Exception e) {
                    LLog.printStackTrace(e);
                }
                return val;
            }
        });

        long startDateMillisTemp;

        for (TableUser user : users) {
            toParagraph = new Paragraph();
            toParagraph.add(new Chunk("User Name: " + user.getFullName() + "\n\n"));
            doc.add(toParagraph);

            StringBuilder table = new StringBuilder();

            table.append("<table border=\"2\" style=\"width:100%; font-size:9px;\">");

            table.append("<tr align=\"center\">" +
                    "<th>Date</th>" +
                    "<th>Demographics</th>" +
                    "<th>Height</th>" +
                    "<th>Weight</th>" +
                    "<th>BP</th>" +
                    "<th>Sugar</th>");

            if (AssetReader.isEnabled("Hemoglobin")) {
                table.append("<th>Hb</th>");
            }

            if (dataManager.toShowPrescription()) {
                table.append("<th>Prescription</th>");
            }

            table.append("</tr>");

            startDateMillisTemp = startDateMillis;

            while (startDateMillisTemp <= endDateMillis) {
                ItemUserSurveySummary summary = dataManager.getUserSummary(startDateMillisTemp, startDateMillisTemp, user.getUserId());
                addSummaryRowDetailed(table, summary, startDateMillisTemp);
                startDateMillisTemp += 24 * 60 * 60 * 1000;
            }

            ItemUserSurveySummary summary = dataManager.getUserSummary(startDateMillis, endDateMillis, user.getUserId());
            summary.setUserName("Total");
            addSummaryRow(table, summary);

            table.append("</table>");

            printTable(table.toString());

            toParagraph = new Paragraph();
            toParagraph.add(new Chunk("\n\n"));
            doc.add(toParagraph);
        }

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    private void generateStocksReport() throws FileNotFoundException, DocumentException {
        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(dataManager.getPdfPassword().getBytes(),
                dataManager.getPdfPassword().getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.app_name) + ": " + "Stock Report", 18);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\nPartner ID: " + dataManager.getPartnerId() + "\n"));
        if (null != dataManager.getCampInfo() && hasValue(dataManager.getCampInfo().getCampName())) {
            toParagraph.add(new Chunk("Camp Name: " + dataManager.getCampInfo().getCampName() + "\n"));
        }
        toParagraph.add(new Chunk("Device Number: " + dataManager.getDeviceNumber() + "\n\n"));

        doc.add(toParagraph);

        List<TableStock> summaries = dataManager.getStockItemsReport();

        Collections.sort(summaries, new Comparator<TableStock>() {
            @Override
            public int compare(TableStock left, TableStock right) {
                int val = 0;

                try {
                    val = left.getSurveyorUserId().compareTo(right.getSurveyorUserId());
                } catch (Exception e) {
                    LLog.printStackTrace(e);
                }
                return val;
            }
        });

        StringBuilder table = new StringBuilder();

        table.append("<table border=\"2\">");

        table.append("<tr align=\"center\">" +
                "<th>User Name</th>" +
                "<th>Item</th>" +
                "<th>Quantity</th>" +
                "<th>Price</th>" +
                "<th>Purchase date</th>" +
                "</tr>");

        for (TableStock summary : summaries) {
            addStockRow(table, summary);
        }

        table.append("</table>");

        printTable(table.toString());

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    private void generateSummaryCsv() {
        try {
            CSVWriter writer = new CSVWriter(new FileWriter(outputFile));
            writeHeaders(writer);
            writeData(writer, dataManager);
            writer.close();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    private void writeHeaders(CSVWriter writer) {
        ArrayList<String> headers = new ArrayList<>(16);
        headers.add("User Name");
        headers.add("Demographics");
        headers.add("Height");
        headers.add("Weight");
        headers.add("BP");
        headers.add("Sugar");
        headers.add("Hb");

        if (dataManager.toShowPrescription()) {
            headers.add("Prescription");
        }

        String[] data = new String[headers.size()];
        headers.toArray(data);
        writer.writeNext(data);
    }

    private void writeData(CSVWriter writer, DataManager dataManager) {
        ArrayList<ItemUserSurveySummary> summaries = dataManager.getUsersSummary(startDateMillis, endDateMillis);

        for (ItemUserSurveySummary summary : summaries) {
            ArrayList<String> headers = new ArrayList<>(16);

            headers.add(summary.getUserName());
            headers.add("" + summary.getDemographics());
            headers.add("" + summary.getHeight());
            headers.add("" + summary.getWeight());
            headers.add("" + summary.getBp());
            headers.add("" + summary.getSugar());
            headers.add("" + summary.getHb());

            if (dataManager.toShowPrescription()) {
                headers.add("" + summary.getPrescription());
            }

            String[] data = new String[headers.size()];
            headers.toArray(data);
            writer.writeNext(data);
        }
    }

    private void generateSummaryPdfBrief() throws FileNotFoundException, DocumentException {
        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(dataManager.getPdfPassword().getBytes(),
                dataManager.getPdfPassword().getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.app_name) + ": " + "Summary Report", 18);

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\nPartner ID: " + dataManager.getPartnerId() + "\n"));
        if (null != dataManager.getCampInfo() && hasValue(dataManager.getCampInfo().getCampName())) {
            toParagraph.add(new Chunk("Camp Name: " + dataManager.getCampInfo().getCampName() + "\n"));
        }
        toParagraph.add(new Chunk("Date: " + dateRangeString + "\n\n"));

        doc.add(toParagraph);

        ArrayList<ItemUserSurveySummary> summaries = dataManager.getUsersSummary(startDateMillis, endDateMillis);

        StringBuilder table = new StringBuilder();

        table.append("<table border=\"2\" style=\"width:100%; font-size:9px;\">");

        table.append("<tr align=\"center\">" +
                "<th>User Name</th>" +
                "<th>Demographics</th>" +
                "<th>Height</th>" +
                "<th>Weight</th>" +
                "<th>BP</th>" +
                "<th>Sugar</th>");

        if (AssetReader.isEnabled("Hemoglobin")) {
            table.append("<th>Hb</th>");
        }

        if (dataManager.toShowPrescription()) {
            table.append("<th>Prescription</th>");
        }

        table.append("</tr>");

        for (ItemUserSurveySummary summary : summaries) {
            addSummaryRow(table, summary);
        }

        table.append("</table>");

        printTable(table.toString());

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    private void generateMissingDemographicsPdf() throws FileNotFoundException, DocumentException {
        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(dataManager.getPdfPassword().getBytes(),
                dataManager.getPdfPassword().getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.app_name) + ": " + "List of missing Demographics", 18);

        ArrayList<String> missingDemographics = dataManager.getMissingDemographics();

        StringBuilder text = new StringBuilder();

        for (int i = 0; i < missingDemographics.size(); i++) {
            text.append(i + 1).append(". ").append(missingDemographics.get(i)).append("\n");
        }

        toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\nPartner ID: " + dataManager.getPartnerId() + "\n"));
        if (null != dataManager.getCampInfo() && hasValue(dataManager.getCampInfo().getCampName())) {
            toParagraph.add(new Chunk("Camp Name: " + dataManager.getCampInfo().getCampName() + "\n"));
        }
        toParagraph.add(new Chunk("Device Number: " + dataManager.getDeviceNumber() + "\n\n"));
        toParagraph.add(new Chunk(text.toString()));

        doc.add(toParagraph);

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();

        if (mError) {
            Toasty.warning(context, AssetReader.getLangKeyword("something_went_wrong"),
                    Toast.LENGTH_SHORT).show();
        } else {

            String message = "";

            if (PDF_VITAL == pdfType || PDF_VITAL_INDIVIDUAL == pdfType || PDF_RISK_REPORT == pdfType) {
                if (0 == rowCount) {
                    LToast.warningLong(AssetReader.getLangKeyword("no_data_present"));
                    return;
                }
//                Toasty.success(context, context.getString(R.string.message_pdf_save_success),
//                        Toast.LENGTH_SHORT).show();
                message = AssetReader.getLangKeyword("generated");

            } else if (PDF_MISSING_DEMOGRAPHICS == pdfType) {
//                Toasty.success(context, "PDF generated successfully",
//                        Toast.LENGTH_SHORT).show();
                message = AssetReader.getLangKeyword("generated");

            } else if (REPORT_SUMMARY == pdfType) {
//                Toasty.success(context, "Summary report generated",
//                        Toast.LENGTH_SHORT).show();
                message = AssetReader.getLangKeyword("generated");

            } else if (STOCK_REPORT == pdfType) {
//                Toasty.success(context, "Stocks report generated",
//                        Toast.LENGTH_SHORT).show();
                message = AssetReader.getLangKeyword("generated");

            } else if (ANALYSIS_REPORT == pdfType) {
                message = AssetReader.getLangKeyword("generated");
            }

            if (null != reportFormat && reportFormat.equals("CSV")) {
                shareFile();
            } else {
                showOpenOrShareDialog(message);
            }
        }
    }

    private void showOpenOrShareDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

        builder.setMessage(message);

        builder.setPositiveButton(AssetReader.getLangKeyword("open"), null);
        builder.setNegativeButton(AssetReader.getLangKeyword("share"), null);
        builder.setNeutralButton(AssetReader.getLangKeyword("close"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File   file   = new File(outputFile.getPath());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.setDataAndType(Uri.fromFile(file), "application/pdf");

                try {
                    context.startActivity(intent);
                } catch (Exception e) {
                }
            }
        });

        noteDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareFile();
                noteDialog.dismiss();
            }
        });
    }

    private void shareFile() {
        Uri    path        = Uri.fromFile(outputFile);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("vnd.android.cursor.dir/email");

        emailIntent.putExtra(Intent.EXTRA_STREAM, path);

        if (PDF_VITAL == pdfType) {
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Survey Report:");

        } else if (PDF_RISK_REPORT == pdfType) {
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Risk Report:");

        } else if (PDF_VITAL_INDIVIDUAL == pdfType) {
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Vitals Report:");

        } else if (PDF_MISSING_DEMOGRAPHICS == pdfType) {
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Missing Demographics:");

        } else if (REPORT_SUMMARY == pdfType) {
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "NCD Screening - Summary Report:");
        }

        context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public static final String CSS = "tr { text-align: center; } th { background-color: lightgreen; padding: 3px; } td {background-color: lightblue;  padding: 3px; }";

    private void generateRiskReport() throws FileNotFoundException, DocumentException {
        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(dataManager.getPdfPassword().getBytes(),
                dataManager.getPdfPassword().getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.survey_report_title), 18);

        List<TableDemographics> tableDemographicsList = dataManager.getAllPatients();

        if (ordering.equals("Order by Head of House")) {
            Collections.sort(tableDemographicsList, new Comparator<TableDemographics>() {
                @Override
                public int compare(TableDemographics left, TableDemographics right) {
                    int val = 0;

                    try {
                        val = dataManager.getHeadOfHouse(left.getHouseId()).compareTo(dataManager.getHeadOfHouse(right.getHouseId()));
                    } catch (Exception e) {
                        LLog.printStackTrace(e);
                    }
                    return val;
                }
            });
        }

        for (TableDemographics tableDemographics : tableDemographicsList) {
            rowCountDemographics = 0;

            if (tableDemographics.isInactive()) {
                continue;
            }

            int vitalsCount = 0;

            List<TableVitals> vitals = dataManager.getAllVitals(tableDemographics.getRegistrationId(), false);

            Collections.sort(vitals, new Comparator<TableVitals>() {
                @Override
                public int compare(TableVitals left, TableVitals right) {
                    int val = 0;

                    try {
                        val = priority.get(left.getVitalType()) - priority.get(right.getVitalType());

                        if (0 == val) {
                            val = (int) (left.getVitalDateTimeMillis() - right.getVitalDateTimeMillis());
                        }
                    } catch (Exception e) {
                        LLog.printStackTrace(e);
                    }
                    return val;
                }
            });

            for (TableVitals tableVital : vitals) {
                if (LUtils.getLongTime(tableVital.getVitalDateTime()) >= startDateMillis
                        && LUtils.getLongTime(tableVital.getVitalDateTime()) <= endDateMillis) {
                    ++vitalsCount;
                }
            }

            if (0 == vitalsCount) {
                continue;
            }

            boolean isAbnormalPresent = false;
            // Check if the vitals come under abnormal category

            for (TableVitals tableVital : vitals) {
                if (!(LUtils.getLongTime(tableVital.getVitalDateTime()) >= startDateMillis
                        && LUtils.getLongTime(tableVital.getVitalDateTime()) <= endDateMillis)) {
                    continue;
                }

                if (range.equals("BMI") && tableVital.getVitalType().equals("Weight")) {
                    final String latestHeight = dataManager.getLatestHeight(tableVital.getRegistrationId(), tableVital.getVitalDateTimeMillis());

                    if (null != latestHeight) {
                        String[] height = latestHeight.split(";");

                        float weight = 0.0f;

                        try {
                            weight = Float.parseFloat(tableVital.getVitalValues());
                        } catch (Exception e) {
                        }

                        double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                height[1],
                                weight);

                        ItemRange itemRange = AssetReader.getBmiRange(bmi, LUtils.calculateAge(tableDemographics.getYob()));

                        if (itemRange.getRange().equals(vitalRange)) {
                            isAbnormalPresent = true;
                            break;
                        }
                    }

                } else if (range.equals("Diabetes") && tableVital.getVitalType().equals("Blood Sugar")) {
                    double value = 0.0;

                    try {
                        value = Double.valueOf(tableVital.getVitalValues());
                    } catch (Exception e) {
                        return;
                    }

                    ItemRange itemRange = AssetReader.getRbsRange(value, tableVital.getVitalSubTypes());
                    if (itemRange.getRange().equals(vitalRange)) {
                        isAbnormalPresent = true;
                        break;
                    }

                } else if (range.equals("Hypertension") && tableVital.getVitalType().equals("Blood Pressure")) {
                    double systolic  = 0.0;
                    double diastolic = 0.0;

                    try {
                        systolic  = Double.valueOf(tableVital.getVitalValues().split(";")[0]);
                        diastolic = Double.valueOf(tableVital.getVitalValues().split(";")[1]);
                    } catch (Exception e) {
                        return;
                    }

                    ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic);
                    if (itemRange.getRange().equals(vitalRange)) {
                        isAbnormalPresent = true;
                        break;
                    }

                } else if (range.equals("Hemoglobin") && tableVital.getVitalType().equals("Hemoglobin")) {

                    double hemoglobin = 0.0;

                    try {
                        hemoglobin = Double.valueOf(tableVital.getVitalValues());
                    } catch (Exception e) {
                        return;
                    }

                    ItemRange itemRange = AssetReader.getHbRange(hemoglobin, tableDemographics.getGender(), LUtils.calculateAge(tableDemographics.getYob()));
                    if (itemRange.getRange().equals(vitalRange)) {
                        isAbnormalPresent = true;
                        break;
                    }
                }
            }

            if (!isAbnormalPresent) {
                continue;
            }

            toParagraph = new Paragraph();
            toParagraph.add(new Chunk("\n\n"));

            String text = "Reg. ID: " + tableDemographics.getRegistrationId() + "\n";
            text += "Name: " + tableDemographics.getFullName() + "\n";

            String hoh = dataManager.getHeadOfHouse(tableDemographics.getHouseId());

            text += "Age: " + LUtils.calculateAge(tableDemographics.getYob()) + "\n";

            if (hasValue(tableDemographics.getPhoneNumber())) {
                text += "Phone: " + tableDemographics.getPhoneNumber() + "\n";
            }

            if (!hoh.isEmpty()) {
                text += "Head of House: " + hoh + "\n";
            }

            text += "Address: " + dataManager.getAddress(tableDemographics.getHouseId()) + "\n";
            text += "\n";

            ArrayList<String> enabledVitals = AssetReader.getVitalsList();

            StringBuilder table = new StringBuilder();

            table.append("<table border=\"2\">");
            table.append("<tr align=\"center\">" +
                    "<th>Vital</th>" +
                    "<th>Subtype</th>" +
                    "<th>Value</th>" +
                    "<th>Unit</th>" +
                    "<th>Range</th>" +
                    "<th>Date time</th>" +
                    "</tr>");

            for (TableVitals tableVital : vitals) {
                if (!(LUtils.getLongTime(tableVital.getVitalDateTime()) >= startDateMillis
                        && LUtils.getLongTime(tableVital.getVitalDateTime()) <= endDateMillis)) {
                    continue;
                }

                switch (tableVital.getVitalType()) {
                    case "Height": {
                        if (!enabledVitals.contains("Height")) {
                            break;
                        }

                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), "");

                        break;
                    }

                    case "Weight": {
                        if (!enabledVitals.contains("Weight")) {
                            break;
                        }

                        final String latestHeight = dataManager.getLatestHeight(tableVital.getRegistrationId(), tableVital.getVitalDateTimeMillis());

                        if (null != latestHeight) {
                            String[] height = latestHeight.split(";");

                            float weight = 0.0f;

                            try {
                                weight = Float.parseFloat(tableVital.getVitalValues());
                            } catch (Exception e) {
                            }

                            double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                    height[1],
                                    weight);

                            ItemRange itemRange = AssetReader.getBmiRange(bmi, LUtils.calculateAge(tableDemographics.getYob()));

                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());
                        } else {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), "");
                        }
                        break;
                    }

                    case "Blood Pressure": {

                        if (!enabledVitals.contains("Blood Pressure")) {
                            break;
                        }

                        double systolic  = 0.0;
                        double diastolic = 0.0;

                        try {
                            systolic  = Double.valueOf(tableVital.getVitalValues().split(";")[0]);
                            diastolic = Double.valueOf(tableVital.getVitalValues().split(";")[1]);
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic);

                        String[] subtypes = tableVital.getVitalSubTypes().split(",");
                        String[] values   = tableVital.getVitalValues().split(";");
                        String[] units    = tableVital.getVitalUnits().split(";");

                        addVitalRow(table, tableVital, subtypes[0] + "/" + subtypes[1],
                                values[0] + "/" + values[1],
                                units[0], itemRange.getRange());

                        if (!values[2].equals("0")) {
                            addVitalRow(table, tableVital, subtypes[2],
                                    values[2],
                                    units[2], "");
                        }

                        break;
                    }

                    case "Blood Sugar": {
                        if (!enabledVitals.contains("Blood Sugar")) {
                            break;
                        }

                        double value = 0.0;

                        try {
                            value = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getRbsRange(value, tableVital.getVitalSubTypes());

                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());


                        break;
                    }

                    case "Hemoglobin": {

                        if (!enabledVitals.contains("Hemoglobin")) {
                            break;
                        }

                        double hemoglobin = 0.0;

                        try {
                            hemoglobin = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getHbRange(hemoglobin, tableDemographics.getGender(), LUtils.calculateAge(tableDemographics.getYob()));
                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), itemRange.getRange());

                        break;
                    }

                    case "Albumin": {
                        break;
                    }
                }
            }

            table.append("</table>");

            if (0 != rowCountDemographics) {
                toParagraph.add(new Chunk(text));
                doc.add(toParagraph);
            }

            printTable(table.toString());
        }

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    private void generateVitalPdf() throws FileNotFoundException, DocumentException {
        initializeFonts();

        PdfWriter docWriter = PdfWriter.getInstance(doc, new FileOutputStream(outputFile));

        docWriter.setEncryption(dataManager.getPdfPassword().getBytes(),
                dataManager.getPdfPassword().getBytes(),
                PdfWriter.ALLOW_COPY, PdfWriter.STANDARD_ENCRYPTION_40);

        doc.addAuthor(context.getString(R.string.developer_name));
        doc.setPageSize(PageSize.A4);
        doc.open();

        Paragraph toParagraph = new Paragraph();
        toParagraph.add(new Chunk("\n"));
        toParagraph.add(new Chunk("Generated on: " + LUtils.getCurrentDateTime()));
        doc.add(toParagraph);

        PdfContentByte cb = docWriter.getDirectContent();

        createTextBold(cb, 30, 800, context.getString(R.string.survey_report_title), 18);

        List<TableDemographics> tableDemographicsList = dataManager.getAllPatients();

        if (ordering.equals("Order by Head of House")) {
            Collections.sort(tableDemographicsList, new Comparator<TableDemographics>() {
                @Override
                public int compare(TableDemographics left, TableDemographics right) {
                    int val = 0;

                    try {
                        val = dataManager.getHeadOfHouse(left.getHouseId()).compareTo(dataManager.getHeadOfHouse(right.getHouseId()));
                    } catch (Exception e) {
                        LLog.printStackTrace(e);
                    }
                    return val;
                }
            });
        }

        for (TableDemographics tableDemographics : tableDemographicsList) {
            rowCountDemographics = 0;

            if (tableDemographics.isInactive()) {
                continue;
            }

            int vitalsCount = 0;

            List<TableVitals> vitals = dataManager.getAllVitals(tableDemographics.getRegistrationId(), false);

            Collections.sort(vitals, new Comparator<TableVitals>() {
                @Override
                public int compare(TableVitals left, TableVitals right) {
                    int val = 0;

                    try {
                        val = priority.get(left.getVitalType()) - priority.get(right.getVitalType());

                        if (0 == val) {
                            val = (int) (left.getVitalDateTimeMillis() - right.getVitalDateTimeMillis());
                        }
                    } catch (Exception e) {
                        LLog.printStackTrace(e);
                    }
                    return val;
                }
            });

            for (TableVitals tableVital : vitals) {
                if (LUtils.getLongTime(tableVital.getVitalDateTime()) >= startDateMillis
                        && LUtils.getLongTime(tableVital.getVitalDateTime()) <= endDateMillis) {
                    ++vitalsCount;
                }
            }

            if (0 == vitalsCount) {
                continue;
            }

            toParagraph = new Paragraph();
            toParagraph.add(new Chunk("\n\n"));

            String text = "Reg. ID: " + tableDemographics.getRegistrationId() + "\n";
            text += "Name: " + tableDemographics.getFullName() + "\n";

            String hoh = dataManager.getHeadOfHouse(tableDemographics.getHouseId());

            text += "Age: " + LUtils.calculateAge(tableDemographics.getYob()) + "\n";

            if (hasValue(tableDemographics.getPhoneNumber())) {
                text += "Phone: " + tableDemographics.getPhoneNumber() + "\n";
            }

            if (!hoh.isEmpty()) {
                text += "Head of House: " + hoh + "\n";
            }

            text += "\n";

            ArrayList<String> enabledVitals = AssetReader.getVitalsList();

            StringBuilder table = new StringBuilder();

            table.append("<table border=\"2\">");
            table.append("<tr align=\"center\">" +
                    "<th>Vital</th>" +
                    "<th>Subtype</th>" +
                    "<th>Value</th>" +
                    "<th>Unit</th>" +
                    "<th>Range</th>" +
                    "<th>Date time</th>" +
                    "</tr>");

            for (TableVitals tableVital : vitals) {
                if (!(LUtils.getLongTime(tableVital.getVitalDateTime()) >= startDateMillis
                        && LUtils.getLongTime(tableVital.getVitalDateTime()) <= endDateMillis)) {
                    continue;
                }

                switch (tableVital.getVitalType()) {

                    case "Height": {
                        if (!enabledVitals.contains("Height")) {
                            break;
                        }

                        addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                tableVital.getVitalValues(),
                                tableVital.getVitalUnits(), "");

                        break;
                    }

                    case "Weight": {
                        if (!enabledVitals.contains("Weight")) {
                            break;
                        }

                        final String latestHeight = dataManager.getLatestHeight(tableVital.getRegistrationId(), tableVital.getVitalDateTimeMillis());

                        if (null != latestHeight) {
                            String[] height = latestHeight.split(";");

                            float weight = 0.0f;

                            try {
                                weight = Float.parseFloat(tableVital.getVitalValues());
                            } catch (Exception e) {
                            }

                            double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                    height[1],
                                    weight);

                            ItemRange itemRange = AssetReader.getBmiRange(bmi, LUtils.calculateAge(tableDemographics.getYob()));

                            if (range.equals("All")) {
                                addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                        tableVital.getVitalValues(),
                                        tableVital.getVitalUnits(), itemRange.getRange());

                            } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                                addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                        tableVital.getVitalValues(),
                                        tableVital.getVitalUnits(), itemRange.getRange());

                            } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                                addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                        tableVital.getVitalValues(),
                                        tableVital.getVitalUnits(), itemRange.getRange());
                            }
                        } else {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), "");
                        }
                        break;
                    }

                    case "Blood Pressure": {

                        if (!enabledVitals.contains("Blood Pressure")) {
                            break;
                        }

                        double systolic  = 0.0;
                        double diastolic = 0.0;

                        try {
                            systolic  = Double.valueOf(tableVital.getVitalValues().split(";")[0]);
                            diastolic = Double.valueOf(tableVital.getVitalValues().split(";")[1]);
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getBpRange(systolic, diastolic);

                        String[] subtypes = tableVital.getVitalSubTypes().split(",");
                        String[] values   = tableVital.getVitalValues().split(";");
                        String[] units    = tableVital.getVitalUnits().split(";");

                        if (range.equals("All")) {
                            addVitalRow(table, tableVital, subtypes[0] + "/" + subtypes[1],
                                    values[0] + "/" + values[1],
                                    units[0], itemRange.getRange());

                            if (!values[2].equals("0")) {
                                addVitalRow(table, tableVital, subtypes[2],
                                        values[2],
                                        units[2], "");
                            }

                        } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, subtypes[0] + "/" + subtypes[1],
                                    values[0] + "/" + values[1],
                                    units[0], itemRange.getRange());

                            if (!values[2].equals("0")) {
                                addVitalRow(table, tableVital, subtypes[2],
                                        values[2],
                                        units[2], "");
                            }

                        } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, subtypes[0] + "/" + subtypes[1],
                                    values[0] + "/" + values[1],
                                    units[0], itemRange.getRange());

                            if (!values[2].equals("0")) {
                                addVitalRow(table, tableVital, subtypes[2],
                                        values[2],
                                        units[2], "");
                            }
                        }

                        break;
                    }

                    case "Blood Sugar": {
                        if (!enabledVitals.contains("Blood Sugar")) {
                            break;
                        }

                        double value = 0.0;

                        try {
                            value = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getRbsRange(value, tableVital.getVitalSubTypes());

                        if (range.equals("All")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());

                        } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());

                        } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());
                        }

                        break;
                    }

                    case "Hemoglobin": {

                        if (!enabledVitals.contains("Hemoglobin")) {
                            break;
                        }

                        double hemoglobin = 0.0;

                        try {
                            hemoglobin = Double.valueOf(tableVital.getVitalValues());
                        } catch (Exception e) {
                            return;
                        }

                        ItemRange itemRange = AssetReader.getHbRange(hemoglobin, tableDemographics.getGender(), LUtils.calculateAge(tableDemographics.getYob()));
                        if (range.equals("All")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());

                        } else if (range.equals("Normal") && itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());

                        } else if (range.equals("Abnormal") && !itemRange.getRange().equals("Normal")) {
                            addVitalRow(table, tableVital, tableVital.getVitalSubTypes(),
                                    tableVital.getVitalValues(),
                                    tableVital.getVitalUnits(), itemRange.getRange());
                        }
                        break;
                    }

                    case "Albumin": {
                        break;
                    }
                }
            }

            table.append("</table>");

            if (0 != rowCountDemographics) {
                toParagraph.add(new Chunk(text));
                doc.add(toParagraph);
            }

            printTable(table.toString());
        }

        if (doc != null) {
            doc.close();
        }

        docWriter.close();
    }

    private void addVitalRow(StringBuilder table, TableVitals tableVital, String subtype, String value, String unit, String range) {
        table.append("<tr align=\"center\">");
        table.append("<td>").append(tableVital.getVitalType()).append("</td>");
        table.append("<td>").append(subtype).append("</td>");
        table.append("<td>").append(value).append("</td>");
        table.append("<td>").append(unit).append("</td>");
        table.append("<td>").append(range).append("</td>");
        table.append("<td>").append(tableVital.getVitalDateTime()).append("</td>");
        table.append("</tr>");

        ++rowCount;
        ++rowCountDemographics;
    }

    private void addSummaryRow(StringBuilder table, ItemUserSurveySummary summary) {
        table.append("<tr align=\"center\">");
        table.append("<td>").append(summary.getUserName()).append("</td>");
        table.append("<td>").append(summary.getDemographics()).append("</td>");
        table.append("<td>").append(summary.getHeight()).append("</td>");
        table.append("<td>").append(summary.getWeight()).append("</td>");
        table.append("<td>").append(summary.getBp()).append("</td>");
        table.append("<td>").append(summary.getSugar()).append("</td>");

        if (AssetReader.isEnabled("Hemoglobin")) {
            table.append("<td>").append(summary.getHb()).append("</td>");
        }

        if (dataManager.toShowPrescription()) {
            table.append("<td>").append(summary.getPrescription()).append("</td>");
        }

        table.append("</tr>");
    }

    private void addSummaryRowDetailed(StringBuilder table, ItemUserSurveySummary summary, long startDateMillis) {
        table.append("<tr align=\"center\">");
        table.append("<td>").append(LUtils.getFormattedDate(startDateMillis)).append("</td>");
        table.append("<td>").append(summary.getDemographics()).append("</td>");
        table.append("<td>").append(summary.getHeight()).append("</td>");
        table.append("<td>").append(summary.getWeight()).append("</td>");
        table.append("<td>").append(summary.getBp()).append("</td>");
        table.append("<td>").append(summary.getSugar()).append("</td>");

        if (AssetReader.isEnabled("Hemoglobin")) {
            table.append("<td>").append(summary.getHb()).append("</td>");
        }

        if (dataManager.toShowPrescription()) {
            table.append("<td>").append(summary.getPrescription()).append("</td>");
        }

        table.append("</tr>");
    }

    private void addStockRow(StringBuilder table, TableStock summary) {
        table.append("<tr align=\"center\">");

        TableUser tableUser = dataManager.getUser(summary.getSurveyorUserId());

        table.append("<td>").append(tableUser.getFullName()).append("</td>");
        table.append("<td>").append(summary.getItemName()).append("</td>");
        table.append("<td>").append(summary.getQuantity()).append("</td>");
        table.append("<td>").append(summary.getPrice()).append("</td>");
        table.append("<td>").append(summary.getPurchasedDate()).append("</td>");
        table.append("</tr>");
    }

    private void printTable(String content) {

        try {
            HTMLWorker htmlWorker = new HTMLWorker(doc);
            htmlWorker.parse(new StringReader(content));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    private void initializeFonts() {

        try {
            bfBold = BaseFont.createFont(BaseFont.HELVETICA_BOLD, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            bf     = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);

        } catch (Exception e) {
            //LLog.printStackTrace(e);
        }
    }

    private void createTextBold(PdfContentByte cb, float x, float y, String text, int fontSize) {
        cb.beginText();
        cb.setFontAndSize(bfBold, fontSize);
        cb.setTextMatrix(x, y);
        cb.showText(text.trim());
        cb.endText();
    }

    private void createText(PdfContentByte cb, float x, float y, String text, int fontSize) {
        cb.beginText();
        cb.setFontAndSize(bf, fontSize);
        cb.setTextMatrix(x, y);
        cb.showText(text.trim());
        cb.endText();
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public void setVitalRange(String range) {
        this.vitalRange = range;
    }
}