package com.dhp.screening.ui.vital;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.lib.bluetooth.BleCallback;
import com.dhp.screening.lib.bluetooth.BleConnection;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_PLUS;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_START_TEST_SPO2;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.BERRY_MED_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.BERRY_MED_DEVICE_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTING;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_DISCONNECTED;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityEnterPulse extends ActivityVitalBase
        implements BleCallback {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.tv_connection_status)
    TextView tvConnectionStatus;

    @BindView(R.id.et_spo2)
    EditText etSpo2;

    @BindView(R.id.et_pulse)
    EditText etPulse;

    @BindView(R.id.et_pi)
    EditText etPi;

    @BindView(R.id.ll_pi)
    LinearLayout llPi;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    private String deviceName = "";
    private String deviceId   = "";
    private String macAddress = "";

    private BleConnection bleConnection;

    private int connectionState = STATE_DISCONNECTED;

    private TableVitals tableVitals;

    private boolean isEdit;

    private ArrayList<ItemBluetoothDevice> itemBluetoothDevices;

    private String connectedDeviceName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_pulse);
        initToolbar(toolbar, getString(R.string.pulse_oximeter), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, null);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.pulse_oximeter));

        itemBluetoothDevices = dataManager.getBluetoothDevices("pulse");

        if (AssetReader.isAllowSave("Pulse Oximeter")) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
            btnSave.setBackgroundResource(R.drawable.btn_background_disabled);
        }

        tableVitals = new TableVitals();

        deviceId = getDeviceId();

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit = true;
            tableVitals = StaticData.getCurrentVital();
            etSpo2.setText(tableVitals.getVitalValues().split(";")[0]);
            etPulse.setText(tableVitals.getVitalValues().split(";")[1]);
            etPi.setText(tableVitals.getVitalValues().split(";")[2]);
            etNotes.setText(tableVitals.getNotes());
            deviceName = tableVitals.getDeviceName();
            deviceId = tableVitals.getDeviceId();
            btnSave.setText("Update");

            btnSave.setEnabled(true);
            btnSave.setBackgroundResource(R.drawable.btn_background);
        }

        registrationId = StaticData.getRegistrationId();

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        boolean popupShown = false;

        if (!isEdit) {
            popupShown = checkAlreadyEntered(registrationId, "Pulse");
        }

        if (!popupShown) {
            showPopupMessage("Pulse", "pulse");
        }

        bleConnection = new BleConnection();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bleConnection.init(this, this);
        bleConnection.startScan();

        if (STATE_CONNECTED != connectionState) {
            tvConnectionStatus.setText("Connecting...");
        }
    }

    @Override
    protected void onPause() {
        bleConnection.deInit();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        bleConnection.disconnect();
        bleConnection.turnOffBle();
        super.onDestroy();
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        if (etSpo2.getText().toString().trim().isEmpty()) {
            LToast.warning("Enter SpO2");

        } else if (etPulse.getText().toString().trim().isEmpty()) {
            LToast.warning("Enter PR");

        } else if (etPi.getText().toString().trim().trim().isEmpty() && !(null != connectedDeviceName && connectedDeviceName.equals(ASHA_PLUS))) {
            LToast.warning("Enter PI");

        } else {
            if (isEdit) {
                tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

            } else {
                if (VISIBLE == llAadhaar.getVisibility()) {
                    registrationId = getRegistrationId();
                }
                if (null == registrationId || registrationId.equals("")) {
                    LToast.warning("Enter Registration ID");
                    return;
                }
                tableVitals.setRegistrationId(registrationId.toUpperCase());
            }

            tableVitals.setVitalType("Pulse");
            tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
            tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

            tableVitals.setVitalSubTypes("SpO2;PR;PI");

            String pi = etPi.getText().toString().trim().trim();

            if (!hasValue(pi)) {
                pi = "0";
            }

            tableVitals.setVitalValues(etSpo2.getText().toString().trim()
                    + ";" + etPulse.getText().toString().trim()
                    + ";" + pi);
            tableVitals.setVitalUnits("%;bpm;%");
            tableVitals.setNotes(etNotes.getText().toString().trim());
            tableVitals.setDeviceName(deviceName);
            tableVitals.setGpsLocation(getLocation());
            tableVitals.setQrScanned(qrScanned);

            if (isEdit) {
                dataManager.updateVital(tableVitals, deviceId);
                LToast.success(AssetReader.getLangKeyword("updated"));
                StaticData.setCurrentVital(tableVitals);
            } else {
                dataManager.addVital(tableVitals, deviceId);
                LToast.success(AssetReader.getLangKeyword("saved"));
            }

            finish();
        }
    }

    @Override
    public void onBleScan(BluetoothDevice device) {
        if (null != itemBluetoothDevices) {
            if (LUtils.toConnectDevice(device.getName(), device.getAddress(), itemBluetoothDevices)) {
                tryConnectingToDevice(device);
            }
        } else if (null != device.getName()) {
            tryConnectingToDevice(device);
        }
    }

    private void tryConnectingToDevice(BluetoothDevice device) {

        switch (device.getName()) {

            case BERRY_MED_DEVICE: {
                tryConnecting(device);
                break;
            }

            case ASHA_PLUS: {
                tryConnectingToAsha(device);
                break;
            }
        }
    }

    private void tryConnectingToAsha(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            connectedDeviceName = device.getName();
            connectionState = STATE_CONNECTING;
            bleConnection.connectToAsha(device);
        }
    }

    private void tryConnecting(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            bleConnection.connectDevice(device);
            connectionState = STATE_CONNECTING;
            macAddress = device.getAddress();
        }
    }

    @Override
    public void onConnected() {
        connectionState = STATE_CONNECTED;
        setConnectionStatus("Connected");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnSave.setEnabled(true);
                btnSave.setBackgroundResource(R.drawable.btn_background);

                if (null != connectedDeviceName && connectedDeviceName.equals(ASHA_PLUS)) {
                    bleConnection.startAshaTest(ASHA_START_TEST_SPO2);
                    llPi.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onServicesDiscovered() {
        bleConnection.startTest();
        setConnectionStatus("Reading...");
    }

    @Override
    public void onDisconnected() {
        connectionState = STATE_DISCONNECTED;
        setConnectionStatus("Not Connected");
    }

    @Override
    public void onValueRead(int type, String values) {

        switch (type) {

            case BERRY_MED_DEVICE_TEST_RESULT: {
                final String[] valueList = values.split(",");
                deviceName = BERRY_MED_DEVICE;
                deviceId = macAddress;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (!valueList[0].isEmpty()) {
                            etSpo2.setText(valueList[0]);
                        }

                        if (!valueList[1].isEmpty() && !valueList[1].equals("255")) {
                            etPulse.setText(valueList[1]);
                        }

                        if (!valueList[2].isEmpty()) {
                            if (valueList[2].equals("0.0")) {
                                if (etPi.getText().toString().trim().isEmpty()) {
                                    etPi.setText(valueList[2]);
                                }
                            } else {
                                etPi.setText(valueList[2]);
                            }
                        }
                    }
                });

                break;
            }

            case ASHA_TEST_RESULT: {
                values = values.split("#")[0];
                final String[] items = values.split("_");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (items[0]) {
                            case "C": {
                                tvConnectionStatus.setText(items[1]);
                                break;
                            }

                            case "O": {
                                etSpo2.setText(items[1]);
                                etPulse.setText(items[2]);
                                tvConnectionStatus.setText("Test complete");
                                break;
                            }
                        }
                    }
                });
                break;
            }
        }
    }

    @Override
    public void onFailure() {
    }

    private void setConnectionStatus(final String connectionStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectionStatus.setText(connectionStatus);
            }
        });
    }
}