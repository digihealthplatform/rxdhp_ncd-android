package com.dhp.screening.ui.patient;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterVitals;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemVital;
import com.dhp.screening.data.item.ItemVitalHeader;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.lib.printer.PrinterManager;
import com.dhp.screening.ui.BaseFragment;
import com.dhp.screening.ui.statistics.GeneratePdfTask;
import com.dhp.screening.ui.vital.ActivitySelectVital;
import com.dhp.screening.ui.vital.ActivityVitalSummary;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.item.ItemVital.VITAL_CHILD;
import static com.dhp.screening.data.item.ItemVital.VITAL_HEADER;
import static com.dhp.screening.util.LConstants.PDF_VITAL_INDIVIDUAL;


@SuppressLint("ValidFragment")
public class FragmentVitals extends Fragment
        implements BaseFragment.IBaseFragment {

    private String registrationId;

    @BindView(R.id.rl_root)
    RelativeLayout rlRoot;

    @BindView(R.id.rv_vitals)
    RecyclerView rvVitals;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.btn_enter_vitals)
    FloatingActionButton btnEnterVitals;

    private DataManager dataManager;

    private List<TableVitals> vitalsList;

    public FragmentVitals(DataManager dataManager) {
        this.dataManager = dataManager;
        registrationId = StaticData.getRegistrationId();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vitals, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_print: {
                showPrintDialog();
                break;
            }

            case R.id.action_generate_report: {
                showGenerateDialog();
                break;
            }

            case R.id.action_summary: {
                startActivity(new Intent(getActivity(), ActivityVitalSummary.class));
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void showGenerateDialog() {
        final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_print, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogLight);

        builder.setTitle("Select date:");

        builder.setView(dialogView);

        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        etStartDate.setText(LUtils.getCurrentDate());

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etStartDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getActivity().getFragmentManager(), "DatePickerDialogStart");
            }
        });

        final EditText etEndDate = (EditText) dialogView.findViewById(R.id.et_end_date);
        etEndDate.setText(LUtils.getCurrentDate());

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etEndDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getActivity().getFragmentManager(), "DatePickerDialogEnd");
            }
        });

        builder.setPositiveButton("Generate", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dateRangeString = "";

                if (etStartDate.getText().toString().equals(etEndDate.getText().toString())) {
                    dateRangeString = etStartDate.getText().toString();
                } else {
                    dateRangeString = "From " + etStartDate.getText().toString() + " to " + etEndDate.getText().toString();
                }

                File outputFile = new File(LUtils.getOutputFilePath("Vitals_Report_" + registrationId, dataManager, "pdf"));

                GeneratePdfTask generatePdfTask =
                        new GeneratePdfTask(getActivity(), outputFile, dataManager, PDF_VITAL_INDIVIDUAL, "All");

                long startDateMillis = LUtils.getLongTime(etStartDate.getText().toString());
                long endDateMillis   = LUtils.getLongTime(etEndDate.getText().toString());

                generatePdfTask.setDateRangeMillis(startDateMillis, endDateMillis);
                generatePdfTask.setDateRangeString(dateRangeString);
                generatePdfTask.setRegistrationId(registrationId);

                generatePdfTask.execute();

                noteDialog.dismiss();
            }
        });
    }

    private void showPrintDialog() {
        final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_print, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogLight);

        builder.setTitle("Select date:");

        builder.setView(dialogView);

        final EditText etStartDate = (EditText) dialogView.findViewById(R.id.et_start_date);
        etStartDate.setText(LUtils.getCurrentDate());

        etStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etStartDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getActivity().getFragmentManager(), "DatePickerDialogStart");
            }
        });

        final EditText etEndDate = (EditText) dialogView.findViewById(R.id.et_end_date);
        etEndDate.setText(LUtils.getCurrentDate());

        etEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etEndDate.setText(date);
                            }
                        },
                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getActivity().getFragmentManager(), "DatePickerDialogEnd");
            }
        });

        builder.setPositiveButton("Print", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AdapterVitals adapterVitals = (AdapterVitals) rvVitals.getAdapter();

                List<TableVitals> vitalsList = adapterVitals.getVitalsList(etStartDate.getText().toString(),
                        etEndDate.getText().toString());

                if (0 == vitalsList.size()) {
                    LToast.warningLong("No vitals found for the selected date");
                } else {
                    new PrinterManager().printVitalDetails(dataManager, registrationId, vitalsList);
                }

                noteDialog.dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        vitalsList = dataManager.getAllVitals(registrationId, true);

        List<ItemVitalHeader> vitalHeaders = dataManager.getAllVitalHeaders(registrationId);

        List<ItemVital> itemVitals = new ArrayList<>();

        boolean firstTime = true;

        for (ItemVitalHeader vitalHeader : vitalHeaders) {
            itemVitals.add(new ItemVital(VITAL_HEADER, vitalHeader, null));

            if (firstTime) {
                for (TableVitals vital : vitalHeader.getVitals()) {
                    itemVitals.add(new ItemVital(VITAL_CHILD, null, vital));
                }

                firstTime = false;
            }
        }

        if (0 == vitalsList.size()) {
            tvEmpty.setVisibility(View.VISIBLE);

        } else {
//            btnPrint.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
            AdapterVitals adapterVitals = new AdapterVitals(getActivity(), vitalHeaders, itemVitals);
            rvVitals.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvVitals.setAdapter(adapterVitals);
        }
    }

    @OnClick(R.id.btn_enter_vitals)
    void selectVitals() {
        startActivity(new Intent(getActivity(), ActivitySelectVital.class));
    }

    @Override
    public void setFabVisibility(boolean visible) {
        if (visible) {
            btnEnterVitals.setVisibility(View.VISIBLE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg));

        } else {
            btnEnterVitals.setVisibility(View.GONE);
            rlRoot.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.window_bg_inactive));
        }
    }
}