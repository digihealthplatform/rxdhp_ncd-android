package com.dhp.screening.ui.login;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemDefaultUser;
import com.dhp.screening.data.item.ItemPartnerUser;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.admin.ActivityAdmin4Settings;
import com.dhp.screening.ui.admin.ActivityAdminHemoglobin;
import com.dhp.screening.ui.admin.ActivityAdminHome;
import com.dhp.screening.ui.admin.ActivityDeviceNumberSettings;
import com.dhp.screening.ui.admin.ActivityExportSettings;
import com.dhp.screening.ui.admin.ActivityImportSettings;
import com.dhp.screening.ui.admin.ActivityPartnerIdSettings;
import com.dhp.screening.ui.user.ActivityUserHome;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.File;
import java.util.Calendar;

import javax.inject.Inject;

import static com.dhp.screening.data.manager.LDatabase.DATABASE_NAME;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN4_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_DEVICE_NUMBER_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_EXPORT_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_HB_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_IMPORT_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_PARTNER_ID_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_ALLOW_SYNC;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_AUTO_SYNC;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_USER_CREATED;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEVICE_NUMBER;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_REMEMBER_LOG_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.NOT_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.USER_LOGGED_IN;
import static com.dhp.screening.util.LConstants.DEFAULT_PARTNER_ID;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityLoginCheck extends BaseActivity {

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent(this).inject(this);

        if (dataManager.isCopyAssetsNeeded()) {
            startActivity(new Intent(this, ActivityCopyAssets.class));
            finish();

        } else {
            if (!AssetReader.init()) {
                LToast.error("APK is installed from invalid sources");
                finish();
                return;
            }

            String selectedLanguage = dataManager.getStringPref("selected_language", "English");

            if (!AssetReader.initSelectedLanguage(selectedLanguage)) {
                dataManager.setPref("selected_language", "English");
                LToast.error(R.string.unable_to_set_language);
            }

            loadCampInfo();
            updateBluetoothDevices();

            createDemoUser();
            createPartnerUser();

            backupDb();

            decideLogin();
        }

        finish();
    }

    private void backupDb() {
        try {
            int date = Calendar.getInstance().get(Calendar.DATE);

            if (date == dataManager.getIntPref("backup_date", 0)) {
                return;
            }

            File outputFolder = new File(Environment.getExternalStorageDirectory(), getResources().getString(R.string.ncd_reports_folder));

            if (!outputFolder.exists()) {
                outputFolder.mkdirs();
            }

            if (outputFolder.canWrite()) {

                File outputDb = new File(outputFolder, "ixywvxfp.yw");

                if (outputDb.exists()) {
                    outputDb.delete();
                }

                File currentDB;

                if (Build.VERSION.SDK_INT >= 28) {
                    currentDB = new File(StaticData.getDatabasePath());
                } else {
                    currentDB = FlowManager.getContext().getDatabasePath(DATABASE_NAME + ".db");
                }

                LUtils.copy(currentDB, outputDb);
            }

            dataManager.setPref("backup_date", date);

        } catch (Exception e) {
            Log.d(TAG, "backupDb: ");
        }
    }

    private void createPartnerUser() {
        String partnerId = dataManager.getPartnerId();

        if (partnerId.equals(DEFAULT_PARTNER_ID)) {
            return;
        }

        ItemPartnerUser partnerUser = AssetReader.getPartnerUser();

        if (null != partnerUser
                && hasValue(partnerUser.getFullName())
                && hasValue(partnerUser.getUserId())) {

            // Check if already created
            TableUser tableUser = dataManager.getUser(partnerUser.getUserId());

            if (null == tableUser || !hasValue(tableUser.getUserId())) {
                tableUser = new TableUser();
                tableUser.setFullName(partnerUser.getFullName());
                tableUser.setUserId(partnerUser.getUserId());
                tableUser.setPassword(partnerUser.getPassword());
                tableUser.setUserType(partnerUser.getUserType());
                dataManager.createUser(tableUser);
            }
        }
    }

    private void createDemoUser() {

        // Create a demo user on first time opening.
        boolean isDefaultUserCreated = dataManager.getBooleanPref(KEY_DEFAULT_USER_CREATED, false);

        if (!isDefaultUserCreated) {

            ItemDefaultUser defaultUser = AssetReader.getDefaultUser();

            if (null != defaultUser
                    && hasValue(defaultUser.getFullName())
                    && hasValue(defaultUser.getUserId())) {

                TableUser tableUser = new TableUser();
                tableUser.setFullName(defaultUser.getFullName());
                tableUser.setUserId(defaultUser.getUserId());
                tableUser.setPassword(defaultUser.getPassword());

                if (dataManager.getPartnerId().equals(DEFAULT_PARTNER_ID)) {
                    if (defaultUser.getIsDemoUser().toLowerCase().equals("yes")) {
                        tableUser.setDemoUser(true);
                        dataManager.createUser(tableUser);
                        dataManager.setPref(KEY_DEFAULT_USER_CREATED, true);
                    }
                } else {
                    if (defaultUser.getIsDemoUser().toLowerCase().equals("yes")) {
                        tableUser.setDemoUser(true);
                    } else {
                        tableUser.setDemoUser(false);
                    }

                    dataManager.createUser(tableUser);
                    dataManager.setPref(KEY_DEFAULT_USER_CREATED, true);
                }
            }
        }
    }

    private void updateBluetoothDevices() {
        ItemBluetoothDevice[] itemBluetoothDevices = AssetReader.getBluetoothDevices();

        if (null != itemBluetoothDevices && itemBluetoothDevices.length > 0) {
            dataManager.updateBluetoothDevices(itemBluetoothDevices);
        }
    }

    private void loadCampInfo() {
        TableCampInfo tableCampInfo = dataManager.getCampInfo();

        String currentPartnerId = AssetReader.getCurrentPartnerId();
        String deviceNumber     = AssetReader.getCurrentDeviceNumber();

        if (null == tableCampInfo
                && !currentPartnerId.isEmpty()) {
            TableCampInfo campInfo = new TableCampInfo();
            campInfo.setPartnerId(currentPartnerId);
            dataManager.createOrUpdateCampInfo(campInfo);

        } else if (null != tableCampInfo
                && (tableCampInfo.getPartnerId().isEmpty()
                || (tableCampInfo.getPartnerId().toLowerCase().equals("demo") && !currentPartnerId.toLowerCase().equals("demo"))
                || (tableCampInfo.getPartnerId().toLowerCase().equals("dem1") && !currentPartnerId.toLowerCase().equals("dem1")))
                && !currentPartnerId.isEmpty()) {
            tableCampInfo.setPartnerId(currentPartnerId);
            dataManager.createOrUpdateCampInfo(tableCampInfo);
        }

        if (dataManager.getStringPref(KEY_DEVICE_NUMBER, "").isEmpty()
                && !deviceNumber.isEmpty()) {
            dataManager.setPref(KEY_DEVICE_NUMBER, deviceNumber);
        }

        if (!dataManager.getBooleanPref(KEY_ALLOW_SYNC, false)
                && AssetReader.allowDeviceToSync()) {
            dataManager.setPref(KEY_ALLOW_SYNC, true);
        }

        if (!dataManager.getBooleanPref(KEY_AUTO_SYNC, false)
                && AssetReader.isAutoSyncEnabled()) {
            dataManager.setPref(KEY_AUTO_SYNC, true);
        }
    }

    private void decideLogin() {
        Intent intent = new Intent(this, ActivityLogin.class);

        int loginState = dataManager.getIntPref(KEY_LOGGED_IN, NOT_LOGGED_IN);

        switch (loginState) {
            case NOT_LOGGED_IN: {
                intent = new Intent(this, ActivityLogin.class);
            }
            break;

            case USER_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityUserHome.class);
                }
            }
            break;

            case ADMIN_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityAdminHome.class);
                }
            }
            break;

            case ADMIN_HB_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityAdminHemoglobin.class);
                }
            }
            break;

            case ADMIN4_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityAdmin4Settings.class);
                }
            }
            break;

            case ADMIN_PARTNER_ID_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityPartnerIdSettings.class);
                }
                break;
            }

            case ADMIN_DEVICE_NUMBER_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityDeviceNumberSettings.class);
                }
                break;
            }

            case ADMIN_EXPORT_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityExportSettings.class);
                }
                break;
            }

            case ADMIN_IMPORT_LOGGED_IN: {
                if (dataManager.getBooleanPref(KEY_REMEMBER_LOG_IN, false)) {
                    intent = new Intent(this, ActivityImportSettings.class);
                }
                break;
            }
        }

        startActivity(intent);

        finish();
    }
}