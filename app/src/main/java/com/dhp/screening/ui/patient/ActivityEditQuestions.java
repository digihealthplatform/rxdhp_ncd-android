package com.dhp.screening.ui.patient;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterQuestionAnswerGroup;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemQuestionChildren;
import com.dhp.screening.data.item.ItemScreeningQuestion;
import com.dhp.screening.data.table.TableProblems;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.asset.AssetReader.KEY_SCREENING_QUESTIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_SCREENING_QUESTION_GROUPS;
import static com.dhp.screening.data.asset.AssetReader.getEnglishVersion;
import static com.dhp.screening.data.item.ItemScreeningQuestion.TYPE_CHILD;


public class ActivityEditQuestions extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_screening_questions)
    RecyclerView rvScreeningQuestions;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Inject
    DataManager dataManager;

    private AdapterQuestionAnswerGroup adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_screening_questions);
        initToolbar(toolbar, AssetReader.getLangKeyword("screening_questions"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        ArrayList<ItemScreeningQuestion> headersList        = AssetReader.getScreeningQuestions(true);
        ArrayList<ItemScreeningQuestion> headersListEnglish = AssetReader.getScreeningQuestions(false);
        ArrayList<ItemScreeningQuestion> childrenList       = new ArrayList<>();

        boolean firstTime = true;

        for (ItemScreeningQuestion itemScreeningQuestion : headersList) {
            childrenList.add(itemScreeningQuestion);

            if (firstTime) {
                for (ItemQuestionChildren children : itemScreeningQuestion.getQuestionHeader().getQuestionChildren()) {
                    childrenList.add(new ItemScreeningQuestion(TYPE_CHILD, null, children));
                }
            }

            firstTime = false;
        }

        // Set previous answers
        List<TableProblems> itemQuestions = dataManager.getAllQuestions(StaticData.getRegistrationId());

        if (!itemQuestions.isEmpty()) {
            for (TableProblems itemQuestion : itemQuestions) {

                for (int i = 0; i < headersListEnglish.size(); i++) {
                    ItemScreeningQuestion itemScreeningQuestion = headersListEnglish.get(i);

                    ArrayList<ItemQuestionChildren> questionChildren = itemScreeningQuestion.getQuestionHeader().getQuestionChildren();

                    for (int j = 0; j < questionChildren.size(); j++) {
                        ItemQuestionChildren children = questionChildren.get(j);

                        if (children.getQuestion().equals(itemQuestion.getQuestion())
                                && children.getGroupName().equals(itemQuestion.getQuestionGroup())) {
                            headersList.get(i).getQuestionHeader().getQuestionChildren().get(j).setAnswer(itemQuestion.getAnswer());
                        }
                    }
                }
            }
        }

        adapter = new AdapterQuestionAnswerGroup(this);
        adapter.setItems(headersList, childrenList);

        rvScreeningQuestions.setNestedScrollingEnabled(false);
        rvScreeningQuestions.setLayoutManager(new LinearLayoutManager(this));
        rvScreeningQuestions.setAdapter(adapter);

        btnSave.setText(AssetReader.getLangKeyword("update"));
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_go_back"));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityEditQuestions.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        confirmSave();
    }

    private void confirmSave() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));
        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ArrayList<ItemScreeningQuestion> headersList = adapter.getHeaderList();

                for (ItemScreeningQuestion itemScreeningQuestion : headersList) {
                    for (ItemQuestionChildren children : itemScreeningQuestion.getQuestionHeader().getQuestionChildren()) {
                        TableProblems tableProblems = new TableProblems();

                        tableProblems.setQuestionGroup(getEnglishVersion(children.getGroupName(), KEY_SCREENING_QUESTION_GROUPS));
                        tableProblems.setQuestion(getEnglishVersion(children.getQuestion(), KEY_SCREENING_QUESTIONS));
                        tableProblems.setQuestionOrder(children.getQuestionOrder());
                        tableProblems.setAnswer(children.getAnswer());

                        dataManager.addOrUpdateQuestion(StaticData.getRegistrationId(), tableProblems);
                    }
                }

                LToast.success(AssetReader.getLangKeyword("saved"));
                finish();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }
}