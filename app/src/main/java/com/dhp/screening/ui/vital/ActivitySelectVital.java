package com.dhp.screening.ui.vital;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.range.ItemVital;
import com.dhp.screening.ui.BaseActivity;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivitySelectVital extends BaseActivity {

    private boolean isNew;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @Inject
    DataManager dataManager;

    private ArrayList<ItemVital> vitalsList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_vital);
        initToolbar(toolbar, getString(R.string.select_vital), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        isNew = getIntent().getBooleanExtra("is_new", false);

        ItemVital[] vitalTabs = AssetReader.getVitalTabs();

        if (null == vitalTabs || 0 == vitalTabs.length) {
            finish();
            return;
        }

        for (ItemVital itemVital : vitalTabs) {
            if (itemVital.getShow().toLowerCase().equals("yes")) {
                vitalsList.add(itemVital);
            }
        }

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);

        if (1 == vitalsList.size()) {
            tabLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isNew) {
            getMenuInflater().inflate(R.menu.menu_select_vital, menu);
        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_vitals: {
                startActivity(new Intent(this, ActivityVitalsList.class));
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return FragmentSelectVital.newInstance(position, dataManager.isAllowPrescription());
        }

        @Override
        public int getCount() {
            return vitalsList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return vitalsList.get(position).getTitle();
        }
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_view_vitals);
        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("vitals"));
        }
    }
}