package com.dhp.screening.ui.patient;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterPatients;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.ui.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ActivityHouseMembers extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @Inject
    DataManager dataManager;

    @BindView(R.id.rv_patients)
    RecyclerView rvPatients;

    private AdapterPatients adapterPatients;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_list);
        initToolbar(toolbar, AssetReader.getLangKeyword("house_members"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        adapterPatients = new AdapterPatients(this, dataManager);
        rvPatients.setLayoutManager(new LinearLayoutManager(this));
        rvPatients.setAdapter(adapterPatients);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_house_members, menu);

        if (dataManager.getBooleanPref("show_inactive_house_members", false)) {
            menu.findItem(R.id.action_show_inactive).setChecked(true);
        } else {
            menu.findItem(R.id.action_show_inactive).setChecked(false);
        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_show_inactive: {

                if (dataManager.getBooleanPref("show_inactive_house_members", false)) {
                    dataManager.setPref("show_inactive_house_members", false);

                } else {
                    dataManager.setPref("show_inactive_house_members", true);
                }

                break;
            }
        }

        invalidateOptionsMenu();
        showHouseMembers();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showHouseMembers();
    }

    private void showHouseMembers() {
        List<TableDemographics> demographicsList = null;

        if (null != StaticData.currentSelectedHouseId) {
            demographicsList = dataManager.getHouseMembers(StaticData.currentSelectedHouseId);

        } else if (null != StaticData.currentSelectedHouse) {
            demographicsList = dataManager.getHouseMembers(StaticData.currentSelectedHouse);
        }

        if (null == demographicsList || 0 == demographicsList.size()) {
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            adapterPatients.setData(demographicsList);
        }
    }

    private void initLanguage() {
        tvEmpty.setText(AssetReader.getLangKeyword("empty"));
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_show_inactive);
        item.setTitle(AssetReader.getLangKeyword("show_inactive"));
    }
}