package com.dhp.screening.ui.stock;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_STOCK_ITEMS;
import static com.dhp.screening.util.LUtils.getFormattedDate;


public class ActivityAddStock extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_purchase_date)
    EditText etPurchaseDate;

    @BindView(R.id.et_quantity)
    EditText etQuantity;

    @BindView(R.id.et_price)
    EditText etPrice;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.sp_item)
    Spinner spItem;

    @BindView(R.id.et_item_other)
    EditText etItemOther;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Inject
    DataManager dataManager;

    private boolean isEdit;

    private String[] itemList;

    private TableStock tableStock;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_stock);
        initToolbar(toolbar, AssetReader.getLangKeyword("stock"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        isEdit = getIntent().getBooleanExtra("is_edit", false);

        tableStock = new TableStock();

        itemList = AssetReader.getSpinnerArray(KEY_STOCK_ITEMS);

        LView.setSpinnerAdapter(this, spItem, itemList);

        spItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (0 == position) {
                    return;
                }

                tableStock.setItemName(itemList[position]);

                if (tableStock.getItemName().equals("Other")) {
                    etItemOther.setVisibility(VISIBLE);
                } else {
                    etItemOther.setText("");
                    etItemOther.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        if (isEdit) {
            int tableKey = getIntent().getIntExtra("table_key", 0);
            tableStock = dataManager.getStock(tableKey);

            etPurchaseDate.setText(tableStock.getPurchasedDate());

            int index = LUtils.getSpinnerIndexOther(tableStock.getItemName(), itemList);

            if (0 < index) {
                spItem.setSelection(index);

                if (index == itemList.length - 2) {
                    etItemOther.setVisibility(VISIBLE);
                    etItemOther.setText(tableStock.getItemName());
                }
            }

            etQuantity.setText("" + tableStock.getQuantity());
            etPrice.setText("" + tableStock.getPrice());
            etNotes.setText(tableStock.getNotes());

            btnSave.setText("Update");
        }
    }

    @OnClick(R.id.et_purchase_date)
    void onDateClicked() {
        final Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        etPurchaseDate.setText(getFormattedDate(dayOfMonth, monthOfYear, year));
                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "DatePickerDialogDob");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                confirmGoBack();
            }
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        confirmGoBack();
    }

    private void confirmGoBack() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_go_back"));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityAddStock.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        readValues();

        if (isAllFieldsEntered()) {
            saveEntries();
        }
    }

    private void readValues() {
        tableStock.setPurchasedDate(etPurchaseDate.getText().toString());

        if (VISIBLE == etItemOther.getVisibility()) {
            tableStock.setItemName(etItemOther.getText().toString());
        }

        try {
            tableStock.setQuantity(Integer.parseInt(etQuantity.getText().toString()));
            tableStock.setPrice(Integer.parseInt(etPrice.getText().toString()));
        } catch (Exception e) {
        }
        tableStock.setNotes(etNotes.getText().toString());
    }

    private boolean isAllFieldsEntered() {
        if (tableStock.getPurchasedDate().isEmpty()) {
            LToast.warning("Please enter the purchase date");
            return false;
        }

        if (tableStock.getItemName().isEmpty()) {
            if (VISIBLE == etItemOther.getVisibility()) {
                LToast.warning("Please enter item name");
            } else {
                LToast.warning("Please select item");
            }
            return false;
        }

        if (0 == tableStock.getQuantity()) {
            LToast.warning("Please enter the quantity");
            return false;
        }

        if (0 == tableStock.getPrice()) {
            LToast.warning("Please enter the price");
            return false;
        }
        return true;
    }

    private void saveEntries() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));
        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (!isEdit) {
                    dataManager.addStock(tableStock);
                    LToast.success("Stock added");

                } else {
                    dataManager.updateStock(tableStock);
                    LToast.success("Stock updated");
                }

                finish();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }
}