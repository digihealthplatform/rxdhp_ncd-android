package com.dhp.screening.ui.dialog;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import com.dhp.screening.R;
import com.dhp.screening.data.adapter.AdapterDialogSearch;


public class DialogSearch implements DialogSearchCallbacks {

    private DialogSearchCallbacks callbacks;
    private Activity              activity;
    private AlertDialog           dialog;
    private InputMethodManager    inputMethodManager;

    private EditText etSearchText;

    private ArrayList<String> items;
    private ArrayList<String> houseHeads;

    public DialogSearch(Activity activity, DialogSearchCallbacks callback) {
        callbacks = callback;
        this.activity = activity;
    }

    public DialogSearch withItems(String[] items, String[] houseHeads) {
        this.items = new ArrayList(Arrays.asList(items));

        if (null != houseHeads) {
            this.houseHeads = new ArrayList(Arrays.asList(houseHeads));
        }

        return this;
    }

    public void showDialog() {
        inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        final View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_search, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(activity, R.style.AlertDialogLight);
        builder.setView(dialogView);

        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        TextView tvEmpty = (TextView) dialogView.findViewById(R.id.tv_empty);

        final AdapterDialogSearch adapter = new AdapterDialogSearch(this, activity, items, houseHeads, tvEmpty);

        etSearchText = (EditText) dialogView.findViewById(R.id.et_text);
        etSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> items      = new ArrayList<String>(0);
                ArrayList<String> houseHeads = null;

                int i = 0;
                for (String item : DialogSearch.this.items) {
                    if (item.toLowerCase().contains(s.toString().toLowerCase())) {
                        items.add(item);

                        if (null != DialogSearch.this.houseHeads) {
                            if (null == houseHeads) {
                                houseHeads = new ArrayList<>();
                            }
                            houseHeads.add(DialogSearch.this.houseHeads.get(i));
                        }
                    }
                    ++i;
                }

                adapter.setItems(items, houseHeads);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etSearchText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                etSearchText.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(etSearchText, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });
        etSearchText.requestFocus();

        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.rv_relation_type);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(adapter);

        dialog.show();
    }

    public void onSelected(String value, int position) {
        inputMethodManager.hideSoftInputFromWindow(etSearchText.getWindowToken(), 0);
        dialog.dismiss();
        callbacks.onSelected(value, position);
    }
}