package com.dhp.screening.ui.admin;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.merger.ImportData;
import com.dhp.screening.network.download.InputStreamVolleyRequest;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.util.LConstants;
import com.dhp.screening.util.LToast;
import com.nbsp.materialfilepicker.MaterialFilePicker;
import com.nbsp.materialfilepicker.ui.FilePickerActivity;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ActivityImportSettings extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    private static final int CODE_FILE_PICKER_REQUEST = 101;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_import_settings);

        initToolbar(toolbar, "Settings", true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_exit"));
        builder.setPositiveButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityImportSettings.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin4, menu);

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_logout:
                confirmLogout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_import)
    void onImportClicked() {
        downloadDbFile();
    }

    private void downloadDbFile() {
        progressDialog = new ProgressDialog(this);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Downloading...");
        progressDialog.show();

        final File tempFolder = new File(Environment.getExternalStorageDirectory(),
                LApplication.getContext().getResources().getString(R.string.temp_folder));

        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }

        File file = new File(tempFolder, "restore.db");

        final String dbPath = file.getPath();

        if (file.exists()) {
            file.delete();
        }

        String downloadUrl = AssetReader.getStringKey("db_restore_url")
                + "/?partner_id=" + dataManager.getPartnerId()
                + "&device_number=" + dataManager.getDeviceNumber();

        InputStreamVolleyRequest request = new InputStreamVolleyRequest(Request.Method.GET, downloadUrl,
                new Response.Listener<byte[]>() {
                    @Override
                    public void onResponse(byte[] response) {
                        try {
                            if (response != null) {

                                try {
                                    InputStream          input  = new ByteArrayInputStream(response);
                                    File                 file   = new File(tempFolder, "restore.db");
                                    BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file));
                                    byte                 data[] = new byte[1024];

                                    long total = 0;

                                    int count;

                                    while ((count = input.read(data)) != -1) {
                                        total += count;
                                        output.write(data, 0, count);
                                    }

                                    output.flush();
                                    output.close();
                                    input.close();

                                    onDownloadComplete(dbPath);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            onDownloadFailed();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                onDownloadFailed();
            }
        }, null);

        RequestQueue requestQueue = Volley.newRequestQueue(LApplication.getContext(), new HurlStack());
        requestQueue.add(request);
    }

    private void confirmLogout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_logout"));
        builder.setPositiveButton(AssetReader.getLangKeyword("logout"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.logout();
                Intent intent = new Intent(ActivityImportSettings.this, ActivityLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setTitle(AssetReader.getLangKeyword("logout"));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (RESULT_OK == resultCode) {
            if (CODE_FILE_PICKER_REQUEST == requestCode) {
                onDatabaseSelected(data.getStringExtra(FilePickerActivity.RESULT_FILE_PATH));
            }
        }
    }

    private void onDatabaseSelected(final String path) {

        String[] optionsArray = {AssetReader.getLangKeyword("merge_existing_data"), AssetReader.getLangKeyword("overwrite_existing_data")};

        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setItems(optionsArray, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {
                    case 0: {
                        confirmImport(0, path);
                    }
                    break;

                    case 1: {
                        confirmImport(1, path);
                        break;
                    }
                }
            }
        });

        builder.show();
    }

    private void confirmImport(final int mergeReplace, final String path) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);

        if (0 == mergeReplace) {
            builder.setMessage(AssetReader.getLangKeyword("confirm_merge"));
        } else {
            builder.setMessage(AssetReader.getLangKeyword("confirm_overwrite"));
        }

        builder.setPositiveButton(AssetReader.getLangKeyword("yes"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ImportData.mergeDb(ActivityImportSettings.this, dataManager, path, mergeReplace);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    public void onDownloadComplete(String dbPath) {
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        onDatabaseSelected(dbPath);
    }

    public void onDownloadFailed() {
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        LToast.warning("No internet connection");

        File outputPath = new File(Environment.getExternalStorageDirectory(),
                LConstants.DEFAULT_DATABASE_IMPORT_PATH);

        if (!outputPath.exists()) {
            outputPath.mkdirs();
        }

        new MaterialFilePicker()
                .withActivity(this)
                .withRequestCode(CODE_FILE_PICKER_REQUEST)
                .withHiddenFiles(false)
                .withTitle("Select the database file")
                .withPath(outputPath.getPath())
                .start();
    }
}