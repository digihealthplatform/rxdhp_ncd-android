package com.dhp.screening.ui.admin;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_CUSTOM_TEXT_TO_PRINT;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_DISTRICT;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_GRAM_PANCHAYAT;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_STATE;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEFAULT_VILLAGE;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_PRINT_ID;
import static com.dhp.screening.data.manager.SharedPrefsHelper.PRINT_ID;
import static com.dhp.screening.util.LUtils.getFormattedDate;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityCampSettings extends BaseActivity {

    private boolean doorToDoor;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_camp_name)
    TextView tvCampName;

    @BindView(R.id.tv_village)
    TextView tvVillage;

    @BindView(R.id.tv_gram_panchayat)
    TextView tvGramPanchayat;

    @BindView(R.id.tv_district)
    TextView tvDistrict;

    @BindView(R.id.tv_state)
    TextView tvState;

    @BindView(R.id.cb_print_id)
    CheckBox cbPrintId;

    @BindView(R.id.et_camp_name)
    EditText etCampName;

    @BindView(R.id.et_village)
    EditText etVillage;

    @BindView(R.id.et_gram_panchayat)
    EditText etGramPanchayat;

    @BindView(R.id.et_district)
    EditText etDistrict;

    @BindView(R.id.et_state)
    AutoCompleteTextView etState;

    @BindView(R.id.et_start_date)
    EditText etStartDate;

    @BindView(R.id.et_end_date)
    EditText etEndDate;

    @BindView(R.id.rg_camp_type)
    RadioGroup rgCampType;

    @BindView(R.id.rb_door_to_door)
    RadioButton rbDoorToDoor;

    @BindView(R.id.rb_camp_screening)
    RadioButton rbCampScreening;

    @BindView(R.id.et_text_to_print)
    EditText etTextToPrint;

    @BindView(R.id.btn_save)
    Button btnSave;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camp_settings);

        initToolbar(toolbar, AssetReader.getLangKeyword("camp_settings"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        rgCampType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                doorToDoor = R.id.rb_door_to_door == indexId;
            }
        });

        cbPrintId.setChecked(dataManager.getBooleanPref(KEY_PRINT_ID, PRINT_ID));

        String[] states = AssetReader.getSpinnerArray("states");

        ArrayAdapter<String> adapterStates = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, states);
        etState.setAdapter(adapterStates);

        etDistrict.setText(dataManager.getStringPref(KEY_DEFAULT_DISTRICT, ""));
        etState.setText(dataManager.getStringPref(KEY_DEFAULT_STATE, ""));

        ((RadioButton) rgCampType.getChildAt(0)).setChecked(true);

        TableCampInfo campInfo = dataManager.getCampInfo();

        if (null != campInfo) {
            etCampName.setText(campInfo.getCampName());
            etStartDate.setText(campInfo.getStartDate());
            etEndDate.setText(campInfo.getEndDate());

            if (!campInfo.isDoorToDoor()) {
                ((RadioButton) rgCampType.getChildAt(1)).setChecked(true);
            }
        } else {
            if (AssetReader.isDoorToDoor()) {
                ((RadioButton) rgCampType.getChildAt(0)).setChecked(true);

            } else {
                ((RadioButton) rgCampType.getChildAt(1)).setChecked(true);

            }
        }

        if (hasValue(dataManager.getStringPref(KEY_CUSTOM_TEXT_TO_PRINT, ""))) {
            etTextToPrint.setText(dataManager.getStringPref(KEY_CUSTOM_TEXT_TO_PRINT, ""));
        }
    }

    @OnClick(R.id.et_start_date)
    void selectStartDate() {
        Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        etStartDate.setText(getFormattedDate(dayOfMonth, monthOfYear, year));
                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "StartDate");
    }

    @OnClick(R.id.et_end_date)
    void selectEndDate() {
        Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        etEndDate.setText(getFormattedDate(dayOfMonth, monthOfYear, year));
                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "EndDate");
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        final TableCampInfo campInfo = new TableCampInfo();
        campInfo.setCampName(etCampName.getText().toString());
        campInfo.setStartDate(etStartDate.getText().toString());
        campInfo.setEndDate(etEndDate.getText().toString());
        campInfo.setDoorToDoor(doorToDoor);

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));

        builder.setPositiveButton(AssetReader.getLangKeyword("confirm"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                TableCampInfo tableCampInfo = dataManager.getCampInfo();
                if (null != tableCampInfo) {
                    campInfo.setPartnerId(tableCampInfo.getPartnerId());
                }

                dataManager.createOrUpdateCampInfo(campInfo);

                if (cbPrintId.isChecked()) {
                    dataManager.setPref(KEY_PRINT_ID, true);
                } else {
                    dataManager.setPref(KEY_PRINT_ID, false);
                }

                dataManager.setPref(KEY_DEFAULT_VILLAGE, etVillage.getText().toString().trim());
                dataManager.setPref(KEY_DEFAULT_GRAM_PANCHAYAT, etGramPanchayat.getText().toString().trim());

                dataManager.setPref(KEY_DEFAULT_DISTRICT, etDistrict.getText().toString().trim());
                dataManager.setPref(KEY_DEFAULT_STATE, etState.getText().toString().trim());

                dataManager.setPref(KEY_CUSTOM_TEXT_TO_PRINT, etTextToPrint.getText().toString().trim());

                LToast.success(AssetReader.getLangKeyword("saved"));
                finish();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void initLanguage() {
        tvCampName.setText(AssetReader.getLangKeyword("camp_name"));
        tvVillage.setText(AssetReader.getLangKeyword("village"));
        tvGramPanchayat.setText(AssetReader.getLangKeyword("gram_panchayat"));
        tvDistrict.setText(AssetReader.getLangKeyword("district"));
        tvState.setText(AssetReader.getLangKeyword("state"));

        etCampName.setHint(AssetReader.getLangKeyword("enter"));
        etVillage.setHint(AssetReader.getLangKeyword("enter"));
        etGramPanchayat.setHint(AssetReader.getLangKeyword("enter"));
        etDistrict.setHint(AssetReader.getLangKeyword("enter"));
        etState.setHint(AssetReader.getLangKeyword("enter"));
        etStartDate.setHint(AssetReader.getLangKeyword("start_date"));
        etEndDate.setHint(AssetReader.getLangKeyword("end_date"));

        rbDoorToDoor.setText(AssetReader.getLangKeyword("door_to_door"));
        rbCampScreening.setText(AssetReader.getLangKeyword("camp_screening"));

        cbPrintId.setText(AssetReader.getLangKeyword("print_id_after_registration"));

        btnSave.setText(AssetReader.getLangKeyword("save"));
    }
}