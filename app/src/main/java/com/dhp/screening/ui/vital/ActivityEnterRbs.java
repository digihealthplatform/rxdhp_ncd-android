package com.dhp.screening.ui.vital;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemMinMax;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.lib.bluetooth.BleCallback;
import com.dhp.screening.lib.bluetooth.BleConnection;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.AUDIO_ERROR;
import static com.dhp.screening.data.asset.AssetReader.AUDIO_INSERT_BLOOD;
import static com.dhp.screening.data.asset.AssetReader.AUDIO_INSERT_STRIP;
import static com.dhp.screening.data.asset.AssetReader.AUDIO_TEST_COMPLETE;
import static com.dhp.screening.data.asset.AssetReader.AUDIO_USE_NEW_STRIP;
import static com.dhp.screening.data.asset.AssetReader.AUDIO_WAIT;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_PLUS;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_START_TEST_GLUCOSE;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.BATTERY_LEVEL;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTING;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_DISCONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNCPLUS_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_TEST_RESULT;
import static com.dhp.screening.util.LUtils.formatFloat;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.hasValue;
import static com.dhp.screening.util.LUtils.roundOff;
import static com.dhp.screening.util.LView.resetRangeColor;


public class ActivityEnterRbs extends ActivityVitalBase implements BleCallback {

    private int connectionState = STATE_DISCONNECTED;

    private boolean isEdit;

    private static final String TAG = ActivityEnterRbs.class.getSimpleName();

    private String type;
    private String value = "";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_rbs)
    Button btnRbs;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.et_blood_sugar)
    EditText etBs;

    @BindView(R.id.et_unit)
    EditText etUnit;

    @BindView(R.id.tv_battery_level)
    TextView tvBattery;

    @BindView(R.id.tv_text)
    TextView tvValue;

    @BindView(R.id.rg_type)
    RadioGroup rgType;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_range_text)
    TextView tvRangeText;

    @Inject
    DataManager dataManager;

    private ProgressDialog progressDialog;

    private BleConnection bleConnection;

    private TableVitals tableVitals;

    private boolean connectionStarted;
    private String  registrationId;

    private String deviceName = "";
    private String deviceId   = "";
    private String macAddress = "";

    private ArrayList<ItemBluetoothDevice> itemBluetoothDevices;

    private String connectedDeviceName;
    private String connectingDeviceName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_rbs);
        initToolbar(toolbar, getString(R.string.enter_rbs), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        progressDialog = new ProgressDialog(this, R.style.AlertDialogLight);
        bleConnection = new BleConnection();

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.enter_rbs));

        registrationId = StaticData.getRegistrationId();

        showPopupMessage("Blood Sugar", "blood_sugar");

        rgType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }
                type = ((RadioButton) findViewById(indexId)).getText().toString();

                valueChanged();

                boolean popupShown = false;

                if (!isEdit) {
                    popupShown = checkAlreadyEntered(registrationId, type);
                }
            }
        });

        ((RadioButton) rgType.getChildAt(0)).setChecked(true);

        itemBluetoothDevices = dataManager.getBluetoothDevices("sugar");

        if (AssetReader.isAllowSave("Blood Sugar")) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
            btnSave.setBackgroundResource(R.drawable.btn_background_disabled);
        }

        tableVitals = new TableVitals();

        deviceId = getDeviceId();

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit = true;
            tableVitals = StaticData.getCurrentVital();

            switch (tableVitals.getVitalSubTypes()) {
                case "RBS":
                    ((RadioButton) rgType.getChildAt(0)).setChecked(true);

                    break;
                case "FBS":
                    ((RadioButton) rgType.getChildAt(1)).setChecked(true);

                    break;
                default:
                    ((RadioButton) rgType.getChildAt(2)).setChecked(true);
                    break;
            }

            etBs.setText(tableVitals.getVitalValues());

            etNotes.setText(tableVitals.getNotes());
            deviceName = tableVitals.getDeviceName();
            deviceId = tableVitals.getDeviceId();

            btnSave.setText("Update");

            btnSave.setEnabled(true);
            btnSave.setBackgroundResource(R.drawable.btn_background);
        }

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        etBs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                valueChanged();
            }
        });
    }

    private void valueChanged() {
        double value = 0.0;

        try {
            value = Double.valueOf(etBs.getText().toString());
        } catch (Exception e) {
            resetRangeColor(etUnit);
            tvRangeText.setText("");
            return;
        }

        ItemRange itemRange = AssetReader.getRbsRange(value, type);

        if (!itemRange.getColor().isEmpty()) {
            etUnit.setBackgroundColor(Color.parseColor(itemRange.getColor()));
            tvRangeText.setText(itemRange.getRange());
        } else {
            resetRangeColor(etUnit);
            tvRangeText.setText("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        bleConnection.init(this, this);
    }

    @Override
    protected void onPause() {
        bleConnection.deInit();
        super.onPause();
    }

    @OnClick(R.id.btn_rbs)
    void onButtonClicked() {

        switch (btnRbs.getText().toString()) {
            case "Connect": {
                bleConnection.startScan();
                connectionStarted = true;
                showLoader("Connecting...");
                startConnectionTimer();
            }
            break;

            case "Start Test": {
                etBs.setText("");

                if (null != connectedDeviceName && connectedDeviceName.equals(ASHA_PLUS)) {
                    bleConnection.startAshaTest(ASHA_START_TEST_GLUCOSE);
                    setText(tvValue, "Insert strip and blood sample");

                } else {
                    setText(tvValue, "Insert strip");
                    bleConnection.startTest();
                }

                btnRbs.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.stop_test), null, null, null);
                btnRbs.setText("Stop Test");
                playAudio(AUDIO_INSERT_STRIP);
            }
            break;

            case "Stop Test": {

                if (null != connectedDeviceName && connectedDeviceName.equals(ASHA_PLUS)) {
                    bleConnection.stopAshaTest();
                } else {
                    bleConnection.stopTest();
                }

                btnRbs.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.start_test), null, null, null);
                btnRbs.setText("Start Test");
                setText(tvValue, "");
            }
            break;
        }
    }

    private void startConnectionTimer() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (STATE_CONNECTED != connectionState && connectionStarted) {
                    bleConnection.disconnect();
                    dismissLoader();
                    LToast.errorLong(R.string.unable_to_connect);
                    connectionState = STATE_DISCONNECTED;
                }
            }
        }, 20000);
    }

    private void showLoader(String message) {
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void dismissLoader() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        bleConnection.disconnect();
        bleConnection.turnOffBle();

        connectionStarted = false;

        super.onDestroy();
    }

    @Override
    public void onBleScan(BluetoothDevice device) {

        if (null != itemBluetoothDevices) {
            if (LUtils.toConnectDevice(device.getName(), device.getAddress(), itemBluetoothDevices)) {
                tryConnectingToDevice(device);
            }
        } else if (null != device.getName()) {
            tryConnectingToDevice(device);
        }
    }

    private void tryConnectingToDevice(BluetoothDevice device) {

        switch (device.getName()) {

            case SYNCPLUS_DEVICE:
            case SYNC_DEVICE: {
                tryConnecting(device);
                break;
            }

            case ASHA_PLUS: {
                tryConnectingToAsha(device);
                break;
            }
        }
    }

    private void tryConnecting(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            connectingDeviceName = device.getName();

            bleConnection.connectDevice(device);
            connectionState = STATE_CONNECTING;
            macAddress = device.getAddress();
        }
    }

    private void tryConnectingToAsha(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            connectedDeviceName = device.getName();
            connectionState = STATE_CONNECTING;
            bleConnection.connectToAsha(device);
        }
    }

    @Override
    public void onConnected() {
        LLog.d(TAG, "onConnected");
        connectionState = STATE_CONNECTED;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnSave.setEnabled(true);
                btnSave.setBackgroundResource(R.drawable.btn_background);
                dismissLoader();
                btnRbs.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.start_test), null, null, null);
                btnRbs.setText("Start Test");

                if (null != connectedDeviceName && connectedDeviceName.equals(ASHA_PLUS)) {
                    tvBattery.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public void onServicesDiscovered() {
        bleConnection.getBatteryLevel();
    }

    @Override
    public void onDisconnected() {
        if (STATE_DISCONNECTED == connectionState) {
            return;
        }

        connectionState = STATE_DISCONNECTED;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissLoader();
                btnRbs.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.connect), null, null, null);
                btnRbs.setText("Connect");
            }
        });
    }

    @Override
    public void onValueRead(int type, String value) {
        switch (type) {
            case BATTERY_LEVEL: {
                setText(tvBattery, "Device Battery Level: " + value + "%");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnRbs.callOnClick();
                    }
                });

                break;
            }
            case SYNC_TEST_RESULT: {
                switch (value) {
                    case "2000":
                        setText(tvValue, "Insert blood sample");
                        playAudio(AUDIO_INSERT_BLOOD);
                        break;

                    case "2001":
                        setText(tvValue, "Please use a new strip");
                        playAudio(AUDIO_USE_NEW_STRIP);
                        break;

                    case "2002":
                        setText(tvValue, "Error. Try again");
                        playAudio(AUDIO_ERROR);
                        break;

                    case "3000":
                        setText(tvValue, "Wait for 5 seconds...");
                        playAudio(AUDIO_WAIT);
                        break;

                    default:
                        this.value = value;

                        if (hasValue(connectingDeviceName) && connectingDeviceName.equals(SYNCPLUS_DEVICE)) {
                            this.deviceName = SYNCPLUS_DEVICE;
                        } else {
                            this.deviceName = SYNC_DEVICE;
                        }
                        deviceId = macAddress;
                        setText(tvValue, "Test complete");
                        playAudio(AUDIO_TEST_COMPLETE);
                        break;
                }
                break;
            }

            case ASHA_TEST_RESULT: {
                value = value.split("#")[0];
                final String[] items = value.split("_");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (items[0]) {

                            case "G": {
                                if (items[1].equals("0")) {
                                    setText(tvValue, "Error please try again");
                                } else {
                                    setText(tvValue, "Test complete");
                                    etBs.setText(items[1]);
                                }

                                break;
                            }

                            case "E": {
                                setText(tvValue, items[1]);
                                break;
                            }
                        }
                    }
                });

                break;
            }
        }
    }

    @Override
    public void onFailure() {

    }

    private void setText(final TextView view, final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setText(text);

                if (text.startsWith("Test complete")) {
                    btnRbs.setCompoundDrawablesWithIntrinsicBounds(LUtils.getDrawable(R.drawable.start_test), null, null, null);
                    btnRbs.setText("Start Test");
                    etBs.setText(value);

                } else if (text.startsWith("Error")) {
                    btnRbs.setText("Start Test");
                }
            }
        });
    }

    @OnClick(R.id.btn_save)
    void onSave() {

        float bs = 0.0f;
        try {
            bs = Float.valueOf(etBs.getText().toString().trim());
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (etBs.getText().toString().isEmpty() || 0 == bs) {
            LToast.warning("Please enter blood sugar");
            return;
        }

        if (isEdit) {
            tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

        } else {
            if (VISIBLE == llAadhaar.getVisibility()) {
                registrationId = getRegistrationId();
            }

            if (null == registrationId || registrationId.equals("")) {
                LToast.warning("Enter Registration ID");
                return;
            }

            tableVitals.setRegistrationId(registrationId.toUpperCase());
        }

        ItemMinMax minMax = AssetReader.getVitalRange("Blood Sugar");
        if (bs < minMax.getMin() || bs > minMax.getMax()) {
            LToast.warningLong("Glucose value should be between " + formatFloat(minMax.getMin()) + " mg/dL - " + formatFloat(minMax.getMax()) + " mg/dL");
            return;
        }

        tableVitals.setVitalType("Blood Sugar");

        tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
        tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

        tableVitals.setVitalSubTypes(type);
        tableVitals.setVitalValues(roundOff(etBs.getText().toString()));
        tableVitals.setVitalUnits("mg/dL");
        tableVitals.setNotes(etNotes.getText().toString());
        tableVitals.setDeviceName(deviceName);
        tableVitals.setGpsLocation(getLocation());
        tableVitals.setQrScanned(qrScanned);

        if (isEdit) {
            dataManager.updateVital(tableVitals, deviceId);
            LToast.success(AssetReader.getLangKeyword("updated"));
            StaticData.setCurrentVital(tableVitals);
        } else {
            dataManager.addVital(tableVitals, deviceId);
            LToast.success(AssetReader.getLangKeyword("saved"));
        }

        finish();
    }
}