package com.dhp.screening.ui.house;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterQuestionAnswer;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemQuestionAnswer;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.network.ViewLocationTask;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_BATHING_TOILET_FACILITY;
import static com.dhp.screening.data.asset.AssetReader.KEY_COOKING_FUELS;
import static com.dhp.screening.data.asset.AssetReader.KEY_DRINKING_WATER_CLEANING_METHODS;
import static com.dhp.screening.data.asset.AssetReader.KEY_ELECTRICITY_AVAILABILITY;
import static com.dhp.screening.data.asset.AssetReader.KEY_HOUSE_OWNERSHIP_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_HOUSE_STATUS;
import static com.dhp.screening.data.asset.AssetReader.KEY_INACTIVE_REASON;
import static com.dhp.screening.data.asset.AssetReader.KEY_INSURANCE;
import static com.dhp.screening.data.asset.AssetReader.KEY_MEAL_TIMINGS;
import static com.dhp.screening.data.asset.AssetReader.KEY_PETS_BITE_ACTION;
import static com.dhp.screening.data.asset.AssetReader.KEY_RATION;
import static com.dhp.screening.data.asset.AssetReader.KEY_SHOW_HOUSE_EXTRA_QUESTIONS;
import static com.dhp.screening.data.asset.AssetReader.KEY_TOILET_AVAILABILITY;
import static com.dhp.screening.data.asset.AssetReader.KEY_TREATMENT_PLACE;
import static com.dhp.screening.data.asset.AssetReader.KEY_VACCINATION_RECORDS_STORAGE;
import static com.dhp.screening.data.asset.AssetReader.KEY_VEHICLES;
import static com.dhp.screening.data.asset.AssetReader.KEY_WATER_SOURCE;
import static com.dhp.screening.data.asset.AssetReader.getSelectedLanguageVersion;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityHouseDetails extends BaseActivity {

    private String houseId;

    private boolean inactive;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_house_details)
    RecyclerView rvHouseDetails;

    @Inject
    DataManager dataManager;

    private AdapterQuestionAnswer adapterQuestionAnswer;

    private TableHouse           tablehouse;
    private List<TableDeathInfo> tableDeathInfoList;

    @BindView(R.id.ll_house_details)
    LinearLayout llHouseDetails;

    private String inactiveReason;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_house_details);
        initToolbar(toolbar, AssetReader.getLangKeyword("house_info"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        houseId = getIntent().getStringExtra("house_id");

        rvHouseDetails.setLayoutManager(new LinearLayoutManager(this));

        adapterQuestionAnswer = new AdapterQuestionAnswer(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        tablehouse = dataManager.getHouse(houseId);
        tableDeathInfoList = dataManager.getDeathInfoList(houseId);

        inactive = tablehouse.isInactive();

        ArrayList<ItemQuestionAnswer> questionAnswers = new ArrayList<>(16);

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("house_number"), tablehouse.getHouseNo()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("street"), tablehouse.getStreet()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("post"), tablehouse.getPost()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("locality"), tablehouse.getLocality()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("village"), tablehouse.getVillage()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("gram_panchayat"), tablehouse.getGramPanchayat()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("district"), tablehouse.getDistrict()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("state"), tablehouse.getState()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("country"), tablehouse.getCountry()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("pin_code"), "" + tablehouse.getPinCode()));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("name_of_head_of_house"), tablehouse.getHeadOfHouse()));
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("name_of_respondent"), tablehouse.getRespondent()));

        if (0 != tablehouse.getTotalMembers()) {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("total_members_in_house"), "" + tablehouse.getTotalMembers()));
        } else {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("total_members_in_house"), ""));
        }

        if (0 != tablehouse.getTotalRooms()) {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("total_rooms_in_house"), "" + tablehouse.getTotalRooms()));
        } else {
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("total_rooms_in_house"), ""));
        }

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("house_ownership"), getSelectedLanguageVersion(tablehouse.getHouseOwnership(), KEY_HOUSE_OWNERSHIP_TYPES)));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("house_status"), getSelectedLanguageVersion(tablehouse.getHouseStatus(), KEY_HOUSE_STATUS)));

        String ration = tablehouse.getRationCard();
        if (ration.equals("No")) {
            ration = AssetReader.getLangKeyword("no");
        } else {
            ration = getSelectedLanguageVersion(tablehouse.getRationCard(), KEY_RATION);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("ration_card"), ration));

        String insurance = tablehouse.getHealthInsurance();
        if (insurance.equals("No")) {
            insurance = AssetReader.getLangKeyword("no");
        } else {
            insurance = getSelectedLanguageVersion(tablehouse.getHealthInsurance(), KEY_INSURANCE);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("health_insurance"), insurance));

        String[] mealTimings     = tablehouse.getMealTimes().split(";");
        String   mealTimingsText = "";
        for (int i = 0; i < mealTimings.length; i++) {
            if (hasValue(mealTimingsText)) {
                mealTimingsText += ", ";
            }
            mealTimingsText += getSelectedLanguageVersion(mealTimings[i].trim(), KEY_MEAL_TIMINGS);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("meal_timing_title"), mealTimingsText));

        String[] cookingFuels     = tablehouse.getCookingFuel().split(";");
        String   cookingFuelsText = "";
        for (int i = 0; i < cookingFuels.length; i++) {
            if (hasValue(cookingFuelsText)) {
                cookingFuelsText += ", ";
            }
            cookingFuelsText += getSelectedLanguageVersion(cookingFuels[i].trim(), KEY_COOKING_FUELS);
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("cooking_fuel_title"), cookingFuelsText));

        String value = "";
        if (hasValue(tablehouse.getToiletAvailability())) {
            String[] values = tablehouse.getToiletAvailability().split(";");
            for (int i = 0; i < values.length; i++) {
                if (hasValue(value)) {
                    value += ", ";
                }
                value += getSelectedLanguageVersion(values[i].trim(), KEY_TOILET_AVAILABILITY);
            }
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("toilet_availability"), value));

        value = "";
        if (hasValue(tablehouse.getElectricityAvailability())) {
            String[] values = tablehouse.getElectricityAvailability().split(";");
            value = "";
            for (int i = 0; i < values.length; i++) {
                if (hasValue(value)) {
                    value += ", ";
                }
                value += getSelectedLanguageVersion(values[i].trim(), KEY_ELECTRICITY_AVAILABILITY);
            }
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("electricity_availability"), value));

        value = "";
        if (hasValue(tablehouse.getMotorisedVehicle())) {
            String[] values = tablehouse.getMotorisedVehicle().split(";");
            value = "";
            for (int i = 0; i < values.length; i++) {
                if (hasValue(value)) {
                    value += ", ";
                }
                value += getSelectedLanguageVersion(values[i].trim(), KEY_VEHICLES);
            }
        }
        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("motorised_vehicle"), value));

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("treatment_place_title"), getSelectedLanguageVersion(tablehouse.getTreatmentPlace(), KEY_TREATMENT_PLACE)));

        String recentDeath = "";
        if (tablehouse.getRecentDeath().equals("Yes")) {
            recentDeath = AssetReader.getLangKeyword("yes");
        } else if (tablehouse.getRecentDeath().equals("No")) {
            recentDeath = AssetReader.getLangKeyword("no");
        }

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("any_death"), recentDeath));

        if (AssetReader.toShow(KEY_SHOW_HOUSE_EXTRA_QUESTIONS)) {

            if (0 == tablehouse.getEarningMembers()) {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("total_earning_members_in_house"), ""));
            } else {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("total_earning_members_in_house"), "" + tablehouse.getEarningMembers()));
            }

            if (0 == tablehouse.getTotalIncome()) {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("household_monthly_income"), ""));
            } else {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("household_monthly_income"), "" + tablehouse.getTotalIncome()));
            }

            if (0 == tablehouse.getTreatmentCost()) {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("treatment_cost"), ""));
            } else {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("treatment_cost"), "" + tablehouse.getTreatmentCost()));
            }

            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("drink_weekly"), "" + tablehouse.getDrinkWeekly()));

            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("drugs_use"), "" + tablehouse.getDrugsUse()));

            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("handicapped_present"), "" + tablehouse.getHandicappedPresent()));

            if (hasValue(tablehouse.getHandicappedPresent())
                    && !tablehouse.getHandicappedPresent().equals("No")
                    && !tablehouse.getHandicappedPresent().equals("Don't know")) {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("handicapped_suffering"), "" + tablehouse.getHandicappedSuffering()));
            }

            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("pets"), tablehouse.getPets()));

            if (hasValue(tablehouse.getPets()) && tablehouse.getPets().equals("Yes")) {
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("pets_bitten"), "" + tablehouse.getPetsBitten()));
            }

            if (hasValue(tablehouse.getPetsBitten()) && tablehouse.getPetsBitten().equals("Yes")) {

                value = "";
                if (hasValue(tablehouse.getPetsBiteAction())) {
                    String[] values = tablehouse.getPetsBiteAction().split(";");
                    value = "";
                    for (int i = 0; i < values.length; i++) {
                        if (hasValue(value)) {
                            value += ", ";
                        }
                        value += getSelectedLanguageVersion(values[i].trim(), KEY_PETS_BITE_ACTION);
                    }
                }
                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("pets_bite_action"), value));
            }

            value = "";
            if (hasValue(tablehouse.getDrinkingWaterSource())) {
                String[] values = tablehouse.getDrinkingWaterSource().split(";");
                value = "";
                for (int i = 0; i < values.length; i++) {
                    if (hasValue(value)) {
                        value += ", ";
                    }
                    value += getSelectedLanguageVersion(values[i].trim(), KEY_WATER_SOURCE);
                }
            }
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("drinking_water_source"), value));

            value = "";
            if (hasValue(tablehouse.getDrinkingWaterCleaningMethod())) {
                String[] values = tablehouse.getDrinkingWaterCleaningMethod().split(";");
                value = "";
                for (int i = 0; i < values.length; i++) {
                    if (hasValue(value)) {
                        value += ", ";
                    }
                    value += getSelectedLanguageVersion(values[i].trim(), KEY_DRINKING_WATER_CLEANING_METHODS);
                }
            }
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("drinking_water_cleaning_method"), value));

            value = "";
            if (hasValue(tablehouse.getBathingToiletFacility())) {
                String[] values = tablehouse.getBathingToiletFacility().split(";");
                value = "";
                for (int i = 0; i < values.length; i++) {
                    if (hasValue(value)) {
                        value += ", ";
                    }
                    value += getSelectedLanguageVersion(values[i].trim(), KEY_BATHING_TOILET_FACILITY);
                }
            }
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("bathing_toilet_facility"), value));

            if (hasValue(tablehouse.getChildHealthCheckupLastDone())
                    || hasValue(tablehouse.getChildrenVaccinated())
                    || hasValue(tablehouse.getVaccinationRecordsStorage())) {

                if (hasValue(tablehouse.getChildHealthCheckupLastDone().replace(";", " "))) {
                    questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("child_health_checkup_last_done"),
                            tablehouse.getChildHealthCheckupLastDone().replace(";", " ") + " " + AssetReader.getLangKeyword("ago")));
                } else {
                    questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("child_health_checkup_last_done"), ""));
                }

                questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("children_vaccinated"), tablehouse.getChildrenVaccinated()));

                if (hasValue(tablehouse.getChildrenVaccinated()) && tablehouse.getChildrenVaccinated().equals("Yes")) {
                    value = "";
                    if (hasValue(tablehouse.getVaccinationRecordsStorage())) {
                        String[] values = tablehouse.getVaccinationRecordsStorage().split(";");
                        value = "";
                        for (int i = 0; i < values.length; i++) {
                            if (hasValue(value)) {
                                value += ", ";
                            }
                            value += getSelectedLanguageVersion(values[i].trim(), KEY_VACCINATION_RECORDS_STORAGE);
                        }
                    }
                    questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("vaccination_records_storage"), value));
                }
            }
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("child_gender_responsibility"), tablehouse.getChildGenderResponsibility()));
            questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("preferred_child_gender"), tablehouse.getPreferredChildGender()));
        }

        questionAnswers.add(new ItemQuestionAnswer(AssetReader.getLangKeyword("notes_title"), tablehouse.getNotes()));

        adapterQuestionAnswer.setItems(questionAnswers);

        rvHouseDetails.setAdapter(adapterQuestionAnswer);
    }

    @OnClick(R.id.btn_edit_house)
    void editHouse() {
        TableHouse tablehouse = dataManager.getHouse(houseId);

        StaticData.setCurrentHouse(tablehouse);

        Intent intent = new Intent(this, ActivityEditHouse.class);
        intent.putExtra("edit", true);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_house_details, menu);

        if (inactive) {
            menu.findItem(R.id.action_inactive).setChecked(true);
            llHouseDetails.setBackground(ContextCompat
                    .getDrawable(ActivityHouseDetails.this, R.drawable.rounded_border_inactive));

        } else {
            llHouseDetails.setBackground(ContextCompat
                    .getDrawable(ActivityHouseDetails.this, R.drawable.rounded_border));

        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_inactive: {

                if (inactive) {
                    confirmActive();
                } else {
                    confirmInactive();
                }
            }
            break;

            case R.id.action_view_location: {
                new ViewLocationTask(this, tablehouse.getGpsLocation()).execute();
                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void confirmActive() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage(AssetReader.getLangKeyword("confirm_activate"));
        builder.setPositiveButton(AssetReader.getLangKeyword("activate"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tablehouse.setInactive(false);
                tablehouse.setInactiveReason("");
                dataManager.updateHouse(tablehouse, tableDeathInfoList);

                inactive = false;
                invalidateOptionsMenu();
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        builder.show();
    }

    private void confirmInactive() {
        inactiveReason = "";

        final View dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_inactive, null);

        AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);

        builder.setTitle(AssetReader.getLangKeyword("inactive_reason"));
        builder.setView(dialogView);

        final String[] reasonList        = AssetReader.getSpinnerArray(KEY_INACTIVE_REASON);
        final String[] reasonListEnglish = AssetReader.getSpinnerArrayEnglish(KEY_INACTIVE_REASON);

        final Spinner spReason = (Spinner) dialogView.findViewById(R.id.sp_inactive_reason);
        LView.setSpinnerAdapter(this, spReason, reasonList);

        final EditText etReasonOther = (EditText) dialogView.findViewById(R.id.et_inactive_reason_other);
        etReasonOther.setHint(AssetReader.getLangKeyword("if_other"));

        spReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (0 < position) {
                    inactiveReason = reasonListEnglish[position];

                    if (inactiveReason.equals("Other")) {
                        etReasonOther.setVisibility(VISIBLE);
                    } else {
                        etReasonOther.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        builder.setPositiveButton(AssetReader.getLangKeyword("done"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);

        final AlertDialog noteDialog = builder.create();
        noteDialog.show();
        noteDialog.setCanceledOnTouchOutside(false);

        noteDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (VISIBLE == etReasonOther.getVisibility()) {
                    inactiveReason = etReasonOther.getText().toString();
                }

                if (inactiveReason.isEmpty()) {
                    LToast.warning(AssetReader.getLangKeyword("warning_enter_reason"));
                    return;
                }

                tablehouse.setInactive(true);
                tablehouse.setInactiveReason(inactiveReason);
                dataManager.updateHouse(tablehouse, tableDeathInfoList);

                inactive = true;
                invalidateOptionsMenu();
                noteDialog.dismiss();
            }
        });
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_view_location);
        item.setTitle(AssetReader.getLangKeyword("view_survey_location"));

        item = menu.findItem(R.id.action_inactive);
        item.setTitle(AssetReader.getLangKeyword("inactive"));
    }
}