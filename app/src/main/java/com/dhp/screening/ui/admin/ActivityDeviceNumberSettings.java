package com.dhp.screening.ui.admin;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.util.LToast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEVICE_NUMBER;


public class ActivityDeviceNumberSettings extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_device_number)
    EditText etDeviceNumber;

    @Inject
    DataManager dataManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_number_settings);

        initToolbar(toolbar, "Settings", true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        etDeviceNumber.setText(dataManager.getStringPref(KEY_DEVICE_NUMBER, ""));
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_exit"));
        builder.setPositiveButton(AssetReader.getLangKeyword("exit"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityDeviceNumberSettings.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin4, menu);

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_logout:
                confirmLogout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        String deviceNumber = etDeviceNumber.getText().toString();

        dataManager.setPref(KEY_DEVICE_NUMBER, deviceNumber);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));

        builder.setPositiveButton(AssetReader.getLangKeyword("confirm"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LToast.success(AssetReader.getLangKeyword("saved"));
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void confirmLogout() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_logout"));
        builder.setPositiveButton(AssetReader.getLangKeyword("logout"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dataManager.logout();
                Intent intent = new Intent(ActivityDeviceNumberSettings.this, ActivityLogin.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setTitle(AssetReader.getLangKeyword("logout"));
    }
}