package com.dhp.screening.ui.patient;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterPatients;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.ui.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ActivityPatientSearch extends BaseActivity {

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_no_results)
    TextView tvEmpty;

    @BindView(R.id.rv_search_results)
    RecyclerView rvPatients;

    @Inject
    DataManager dataManager;

    private InputMethodManager inputMethodManager;
    private AdapterPatients    adapterPatients;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patients_search);
        initToolbar(toolbar, "", true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchProfiles();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        adapterPatients = new AdapterPatients(this, dataManager);

        rvPatients.setLayoutManager(new LinearLayoutManager(this));
        rvPatients.setAdapter(adapterPatients);
    }

    @Override
    protected void onResume() {
        super.onResume();
        searchProfiles();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        initLanguageMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        inputMethodManager.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

        switch (item.getItemId()) {

            case R.id.action_search:
                searchProfiles();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void searchProfiles() {
        tvEmpty.setVisibility(View.GONE);
        adapterPatients.searchPatient(etSearch.getText().toString());
    }

    public void onEmpty() {
        tvEmpty.setVisibility(View.VISIBLE);
    }

    private void initLanguage() {
        tvEmpty.setText(AssetReader.getLangKeyword("empty"));
        etSearch.setHint(AssetReader.getLangKeyword("hint_search"));
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);

        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("search"));
        }
    }
}