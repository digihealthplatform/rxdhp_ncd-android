package com.dhp.screening.ui.vital.hemoglobin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.di.component.ActivityComponent;
import com.dhp.screening.data.di.component.DaggerActivityComponent;
import com.dhp.screening.data.di.module.ActivityModule;
import com.dhp.screening.util.LUtils;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_A_VALUES;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_B_VALUES;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_L_VALUES;
import static com.dhp.screening.util.LConstants.HB_FOLDER_PATH;
import static com.dhp.screening.util.LConstants.TYPE_TEST;

public class ActivityCameraView extends Activity implements CameraBridgeViewBase.CvCameraViewListener2 {

    private int currentContrast = 0;
    private int currentIso      = 0;

    private int activityType;
    File imgFile1, imgFile2, imgFile3, imgFile;
    private final int CONTRAST_MIN = 0;
    private final int CONTRAST_MAX = 6;
    private final int ISO_MIN      = 0;
    private       int ISO_MAX      = 6;

    private String[] isoValues;

    private static final String TAG = "ActivityCameraView";

    private Mat rgba;
    private Mat rgba_1;

    private Bitmap[] bitmap = new Bitmap[8];

    private ProgressDialog progressDialog;

    private AlertDialog waitDialog;
    private View        dialogView;

    @BindView(R.id.jc_view)
    JavaCameraView jcView;

    @BindView(R.id.btn_iso_plus)
    Button btnIsoPlus;

    @BindView(R.id.btn_iso_minus)
    Button btnIsoMinus;

    @BindView(R.id.tv_iso_value)
    TextView tvIsoValue;

    @BindView(R.id.btn_contrast_plus)
    Button btnContrastPlus;

    @BindView(R.id.btn_contrast_minus)
    Button btnContrastMinus;

    @BindView(R.id.tv_contrast_value)
    TextView tvContrastValue;

    @BindView(R.id.btn_calibrate)
    Button btnCalibrate;

    @BindView(R.id.btn_capture)
    Button btnCapture;

    @BindView(R.id.ll_contrast)
    LinearLayout llContrast;

    @BindView(R.id.ll_saturation)
    LinearLayout llSaturation;

    private HttpURLConnection connection;
    private Context           context;

    String serverResponseMessage;
    int    serverResponseCode;

    long requestLength;

    @Inject
    DataManager dataManager;

    private ActivityComponent activityComponent;

    protected ActivityComponent getActivityComponent(Activity activity) {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(activity))
                    .applicationComponent(LApplication.get(activity).getComponent())
                    .build();
        }
        return activityComponent;
    }

    private BaseLoaderCallback loaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    jcView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera_view);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        activityType = getIntent().getIntExtra("activity_type", 0);

        if (TYPE_TEST == activityType) {
            btnCalibrate.setVisibility(View.GONE);
            llContrast.setVisibility(View.GONE);
            llSaturation.setVisibility(View.GONE);

        } else {
            btnCalibrate.setVisibility(View.GONE);
            btnCapture.setVisibility(View.GONE);
        }

        jcView.setVisibility(SurfaceView.VISIBLE);
        jcView.setCvCameraViewListener(this);

        jcView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
            }
        });

        btnContrastPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContrastMinus.setEnabled(true);

                ++currentContrast;

                if (CONTRAST_MAX <= currentContrast) {
                    currentContrast = CONTRAST_MAX;
                    btnContrastPlus.setEnabled(false);
                } else {
                    btnContrastPlus.setEnabled(true);
                }

                jcView.setContrast(currentContrast);

                dataManager.setPref("contrast", currentContrast);
                tvContrastValue.setText("" + currentContrast);
            }
        });

        btnContrastMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnContrastPlus.setEnabled(true);

                --currentContrast;

                if (CONTRAST_MIN >= currentContrast) {
                    btnContrastMinus.setEnabled(false);
                    currentContrast = CONTRAST_MIN;
                } else {
                    btnContrastMinus.setEnabled(true);
                }

                jcView.setContrast(currentContrast);

                dataManager.setPref("contrast", currentContrast);
                tvContrastValue.setText("" + currentContrast);
            }
        });

        btnIsoPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnIsoMinus.setEnabled(true);

                ++currentIso;

                if (ISO_MAX <= currentIso) {
                    currentIso = ISO_MAX;
                    btnIsoPlus.setEnabled(false);
                } else {
                    btnIsoPlus.setEnabled(true);
                }

                jcView.setIso(isoValues[currentIso]);
                dataManager.setPref("iso", currentIso);

                tvIsoValue.setText(isoValues[currentIso]);
            }
        });

        btnIsoMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnIsoPlus.setEnabled(true);

                --currentIso;

                if (ISO_MIN >= currentIso) {
                    currentIso = ISO_MIN;
                    btnIsoMinus.setEnabled(false);
                } else {
                    btnIsoMinus.setEnabled(true);
                }

                jcView.setIso(isoValues[currentIso]);

                dataManager.setPref("iso", currentIso);
                tvIsoValue.setText(isoValues[currentIso]);
            }
        });

        HbImageProcessing.initValues(dataManager.getStringPref(KEY_L_VALUES, ""),
                dataManager.getStringPref(KEY_A_VALUES, ""),
                dataManager.getStringPref(KEY_B_VALUES, ""));
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, loaderCallback);
        } else {
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        rgba = new Mat(height, width, CvType.CV_8UC4);
        rgba_1 = new Mat(height, width, CvType.CV_8UC4);

        currentContrast = dataManager.getIntPref("contrast", -1);

        if (-1 == currentContrast) {
            currentContrast = jcView.getContrast();
        }

        jcView.setContrast(currentContrast);

        isoValues = jcView.getIsoValues().split(",");

        ISO_MAX = isoValues.length;

        btnIsoMinus.setEnabled(false);

        tvContrastValue.setText("" + currentContrast);

        currentIso = dataManager.getIntPref("iso", 0);

        jcView.setIso(isoValues[currentIso]);
        tvIsoValue.setText("" + isoValues[currentIso]);

        if (CONTRAST_MIN >= currentContrast) {
            btnContrastMinus.setEnabled(false);
            currentContrast = CONTRAST_MIN;
        } else {
            btnContrastMinus.setEnabled(true);
        }

        if (ISO_MIN >= currentIso) {
            currentIso = ISO_MIN;
            btnIsoMinus.setEnabled(false);
        } else {
            btnIsoMinus.setEnabled(true);
        }
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        rgba = inputFrame.rgba();
        rgba_1 = inputFrame.rgba();
        //Point centre = new Point(rgba_1.width()*0.515, rgba_1.height()*0.5);
        //int radius = (int)(rgba_1.width()*0.035);

        if (TYPE_TEST == activityType) {
            //capture box for Blood sample
            Point tl = new Point(rgba.width() * 0.5, rgba.height() * 0.45);
            Point br = new Point(rgba.width() * 0.53, rgba.height() * 0.75);
            Imgproc.rectangle(rgba, tl, br, new Scalar(0, 255, 0), 2);

//            Imgproc.circle(rgba_1, centre, radius, new Scalar(255, 0, 0), 2);

            return rgba;
        } else {
            Point tl = new Point(rgba.width() * 0.3, rgba.height() * 0.35);
            Point br = new Point(rgba.width() * 0.85, rgba.height() * 0.65);
            Imgproc.rectangle(rgba, tl, br, new Scalar(0, 255, 0), 5);
            return rgba;
        }
    }

    @Override
    public void onCameraViewStopped() {

    }

    int pendingTime  = 2;
    int captureCount = 0;

    private void count() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                --pendingTime;

                if (0 == pendingTime) {
                    if (TYPE_TEST == activityType) {
                        capture();
                        captureCount++;
                        if (captureCount < 3) {
                            pendingTime = 2;
                            count();
                        }
                    } else capture();
                } else {
                    ((TextView) dialogView.findViewById(R.id.tv_wait)).setText("" + pendingTime);
                    count();
                }
            }
        }, 1000);
    }

    @OnClick({R.id.btn_calibrate, R.id.btn_capture})
    void onCapture() {
        captureCount = 0;
        dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_wait, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogLight);
        builder.setTitle("Wait...");
        builder.setView(dialogView);

        waitDialog = builder.create();
        waitDialog.setCanceledOnTouchOutside(false);
        waitDialog.show();
        ((TextView) dialogView.findViewById(R.id.tv_wait)).setText("" + pendingTime);

        count();
    }

    float height = 0;

    void capture() {
        // Dismiss loader
        if (captureCount == 2) {
            waitDialog.dismiss();
        }


        Mat paramRegion;

        if (TYPE_TEST == activityType) {
            //capture image for blood sample
            /*Point tl = new Point(rgba.width() * 0.505, rgba.height() * 0.46);
            Point br = new Point(rgba.width() * 0.525, rgba.height() * 0.54);*/
            Point tl = new Point(rgba.width() * 0.505, rgba.height() * 0.455);
            Point br = new Point(rgba.width() * 0.525, rgba.height() * 0.745);
            Rect  crop = new Rect(tl, br);

            height = crop.height;

            paramRegion = rgba.submat(crop);
        } else {
            Point tl   = new Point(rgba.width() * 0.3, rgba.height() * 0.35);
            Point br   = new Point(rgba.width() * 0.85, rgba.height() * 0.65);
            Rect  crop = new Rect(tl, br);

            paramRegion = rgba.submat(crop);
        }

        bitmap[0] = Bitmap.createBitmap(paramRegion.cols(), paramRegion.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(paramRegion, bitmap[0]);


        if (TYPE_TEST == activityType) {
            switch (captureCount) {
                case 0:
                    imgFile1 = saveTempImage(bitmap[0], "capture1_");
                    break;
                case 1:
                    imgFile2 = saveTempImage(bitmap[0], "capture2_");
                    break;
                case 2:
                    imgFile3 = saveTempImage(bitmap[0], "capture3_");
                    break;
            }

            if (captureCount == 2) {
                //captureCount=0;
                processFile(imgFile1, imgFile2, imgFile3);
            }
        } else {
            imgFile = saveTempImage(bitmap[0], "calibration_");
            processFile(imgFile, null, null);
        }

    }

    private void processFile(final File imgFile1, final File imgFile2, final File imgFile3) {
        showLoader();

        new Thread(new Runnable() {
            @Override
            public void run() {
//                uploadFile(imgFile.getAbsolutePath());

                if (TYPE_TEST == activityType) {
                    float Hb = 0;
                    try {
                        Hb = HbImageProcessing.findHb(imgFile1.getPath(), imgFile2.getPath(), imgFile3.getPath(),
                                Environment.getExternalStorageDirectory() + "/" + HB_FOLDER_PATH + "/" + dataManager.getStringPref("calibration_file", ""));

                        StaticData.saveResult("" + Hb);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                finishTask(imgFile1.getName());
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                pendingTime = 2;
                                progressDialog.dismiss();
                                Toast.makeText(ActivityCameraView.this, "Unable to process. Try again", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            finishTask(imgFile.getName());
                        }
                    });
                }
            }
        }).start();
    }

    private void showLoader() {
        progressDialog = new ProgressDialog(this, R.style.AlertDialogLight);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    private void finishTask(String fileName) {
        progressDialog.dismiss();

        if (TYPE_TEST == activityType) {
//            startActivity(new Intent(ActivityCameraView.this, ActivityTestResult.class));

        } else {
            dataManager.setPref("calibration_file", fileName);
            Toast.makeText(ActivityCameraView.this, "Calibrated", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

//    private File saveTempImage(Bitmap bitmapImage, String name) {
//
//
//        File folder = new File(Environment.getExternalStorageDirectory(), HB_FOLDER_PATH);
//
//        SimpleDateFormat sdf  = new SimpleDateFormat("dd_MM_yyyy-HH_mm_ss");
//        Date             date = new Date();
//
//        String fileName = name + sdf.format(date) + "-iso_" + isoValues[currentIso] + "-C_" + currentContrast + ".png";
//        File   imgFile  = new File(folder, fileName);
//
//        try {
//            FileOutputStream fos = new FileOutputStream(imgFile);
//            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
//            fos.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        float mHei = bitmapImage.getHeight();
//
//        if (height != mHei) {
//
//            Toast.makeText(ActivityCameraView.this, "Try capturing again", Toast.LENGTH_SHORT).show();
//        }
//        return imgFile;
//    }

    private File saveTempImage(Bitmap bitmapImage, String postfix) {
        File folder = new File(Environment.getExternalStorageDirectory(), HB_FOLDER_PATH);

        if (!folder.exists()) {
            folder.mkdirs();
        }

        String fileName;

        if (postfix.equals("calibration")) {
            StaticData.capturedImageNames = null;

            fileName = "calibration_hb_" + StaticData.getCampDeviceInfo()
                    + "_iso-" + isoValues[currentIso]
                    + "_C-" + currentContrast
                    + "_" + LUtils.getCurrentDateTimeMillis()
                    + ".png";

        } else {
            fileName = "_" + captureCount + "_iso-" + isoValues[currentIso]
                    + "_C-" + currentContrast
                    + "_" + LUtils.getCurrentDateTimeMillis()
                    + ".png";

            if (null == StaticData.capturedImageNames) {
                StaticData.capturedImageNames = new String[3];
            }

            StaticData.capturedImageNames[captureCount] = fileName;
        }

        File imgFile = new File(folder, fileName);

        try {
            FileOutputStream fos = new FileOutputStream(imgFile);
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        float mHei = bitmapImage.getHeight();

        if (height > mHei){

            Toast.makeText(ActivityCameraView.this, "Try capturing again", Toast.LENGTH_SHORT).show();
        }

        return imgFile;
    }

    public void onDestroy() {
        super.onDestroy();

        if (jcView != null)
            jcView.disableView();
    }
}