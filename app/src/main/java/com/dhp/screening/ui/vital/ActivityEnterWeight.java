package com.dhp.screening.ui.vital;

import android.bluetooth.BluetoothDevice;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.event.EventConnectionStateChanged;
import com.dhp.screening.data.event.EventWeightRead;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemMinMax;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.lib.bluetooth.BleCallback;
import com.dhp.screening.lib.bluetooth.BleConnection;
import com.dhp.screening.lib.bluetooth.MedcheckWeightConnection;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.VISIBLE;
import static com.dhp.screening.lib.bluetooth.BleConstants.CHIPSEA_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.MEDCHECK_WEIGHT;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTING;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_DISCONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.SWAN_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SWAN_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_TEST_RESULT;
import static com.dhp.screening.util.LUtils.formatFloat;
import static com.dhp.screening.util.LUtils.getFormattedDateTime;
import static com.dhp.screening.util.LUtils.roundOff;
import static com.dhp.screening.util.LView.resetRangeColor;


public class ActivityEnterWeight extends ActivityVitalBase
        implements BleCallback {

    private int connectionState = STATE_DISCONNECTED;

    private boolean isEdit;

    private static final String TAG = "ActivityEnterWeight";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.et_time)
    EditText etTime;

    @BindView(R.id.iv_icon)
    ImageView ivIcon;

    @BindView(R.id.tv_connection_status)
    TextView tvConnectionStatus;

    @BindView(R.id.et_weight)
    EditText etWeight;

    @BindView(R.id.et_unit)
    EditText etUnit;

    @BindView(R.id.tv_range_text)
    TextView tvRangeText;

    @BindView(R.id.tv_bmi)
    TextView tvBmi;

    @BindView(R.id.et_notes)
    EditText etNotes;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.ll_aadhaar)
    LinearLayout llAadhaar;

    @BindView(R.id.et_aadhaar)
    EditText etAadhaar;

    @BindView(R.id.tv_name)
    TextView tvName;

    @Inject
    DataManager dataManager;

    private TableVitals tableVitals;

    private BleConnection bleConnection;

    private String registrationId;

    private String deviceName = "";
    private String deviceId   = "";
    private String macAddress = "";

    private String latestHeight;

    private ArrayList<ItemBluetoothDevice> itemBluetoothDevices;

    private boolean isOnceConnected;

    private String deviceNameTemp;

    private Handler  handler  = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (!isOnceConnected) {
                LToast.error("Please Pair the Device first and try again");
                handler.postDelayed(this, 20000);
            }
        }
    };

    private boolean isTestStarted;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_weight);
        initToolbar(toolbar, getString(R.string.enter_weight), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initViews(etDate, etTime, etAadhaar, tvName);
        initDataManager(dataManager);

        LView.setIcon(ivIcon, getString(R.string.enter_weight));

        tableVitals = new TableVitals();

        deviceId = getDeviceId();

        bleConnection = new BleConnection();

        itemBluetoothDevices = dataManager.getBluetoothDevices("weight");

        if (AssetReader.isAllowSave("Weight")) {
            btnSave.setEnabled(true);
        } else {
            btnSave.setEnabled(false);
            btnSave.setBackgroundResource(R.drawable.btn_background_disabled);
        }

        if (null != StaticData.getCurrentVital() && getIntent().getBooleanExtra("edit", false)) {
            isEdit      = true;
            tableVitals = StaticData.getCurrentVital();
            etWeight.setText(tableVitals.getVitalValues());
            etNotes.setText(tableVitals.getNotes());
            deviceName = tableVitals.getDeviceName();

            deviceId = tableVitals.getDeviceId();

            btnSave.setText("Update");
            btnSave.setEnabled(true);
            btnSave.setBackgroundResource(R.drawable.btn_background);
        }

        registrationId = StaticData.getRegistrationId();

        if (!isEdit && null == registrationId) {
            llAadhaar.setVisibility(VISIBLE);
//            tvName.setVisibility(VISIBLE);
        }

        boolean popupShown = false;

        if (!isEdit) {
            popupShown = checkAlreadyEntered(registrationId, "Weight");
        }

        if (!popupShown) {
            showPopupMessage("Weight", "weight");
        }

        latestHeight = dataManager.getLatestHeight(StaticData.getRegistrationId(), tableVitals.getVitalDateTimeMillis());

        etAadhaar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                showRange();
            }
        });

        etWeight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                showRange();
            }
        });

        handler.postDelayed(runnable, 20000);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(EventWeightRead event) {
        etWeight.setText(event.getWeight());
    }

    @Subscribe(threadMode = ThreadMode.MAIN_ORDERED)
    public void onEvent(EventConnectionStateChanged event) {

        if (STATE_CONNECTING == event.getState()) {
            tvConnectionStatus.setText("Connecting...");

        } else if (STATE_DISCONNECTED == event.getState()) {
            tvConnectionStatus.setText("Disconnected");

        } else if (STATE_CONNECTED == event.getState()) {
            isOnceConnected = true;
            connectionState = STATE_CONNECTED;
            tvConnectionStatus.setText("Connected");
            btnSave.setEnabled(true);
            btnSave.setBackgroundResource(R.drawable.btn_background);
        }
    }

    private void showRange() {
        if (VISIBLE == llAadhaar.getVisibility()) {
            latestHeight = dataManager.getLatestHeight(etAadhaar.getText().toString(), tableVitals.getVitalDateTimeMillis());
            resetRangeColor(etUnit);
            tvBmi.setText("");
            tvRangeText.setText("");
        }

        float  weight = 0.0f;
        double bmi    = 0.0;

        try {
            weight = Float.valueOf(etWeight.getText().toString());

        } catch (Exception e) {
            resetRangeColor(etUnit);
            tvBmi.setText("");
            tvRangeText.setText("");
            return;
        }

        if (null != latestHeight) {
            String[] height = latestHeight.split(";");

            bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                    height[1],
                    weight);

            tvBmi.setText(bmi + " kg/m2");

            ItemRange itemRange = AssetReader.getBmiRange(bmi, 20, true);

            if (!itemRange.getColor().isEmpty()) {
                etUnit.setBackgroundColor(Color.parseColor(itemRange.getColor()));
                tvRangeText.setText(itemRange.getRange());
            } else {
                resetRangeColor(etUnit);
                tvBmi.setText("");
                tvRangeText.setText("");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int connectionStatus = MedcheckWeightConnection.getInstance().getConnectionStatus();

        if (STATE_CONNECTED == connectionStatus) {
            isOnceConnected = true;
//            connectionState = STATE_CONNECTED;
//            tvConnectionStatus.setText("Connected");
//            btnSave.setEnabled(true);
            tvConnectionStatus.setText("Connecting... \n(Please click \"SET\" before taking the measurement)");

        } else {
            bleConnection.init(this, this);
            bleConnection.startScan();

            if (STATE_CONNECTED != connectionState) {
                tvConnectionStatus.setText("Connecting...");
            }
        }
    }

    @Override
    protected void onPause() {
        bleConnection.deInit();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        bleConnection.disconnect();
        if (null != deviceName
                && (deviceName.equals(CHIPSEA_DEVICE) || deviceName.equals(SWAN_DEVICE))) {
            bleConnection.turnOffBle();
        }

        handler.removeCallbacks(runnable);
        super.onDestroy();
    }

    @OnClick(R.id.btn_save)
    void onSave() {
        float weight = 0.0f;
        try {
            weight = Float.valueOf(etWeight.getText().toString().trim());
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (etWeight.getText().toString().isEmpty() || 0 == weight) {
            LToast.warning("Enter weight");

        } else {
            ItemMinMax minMax = AssetReader.getVitalRange("Weight");

            if (weight < minMax.getMin() || weight > minMax.getMax()) {
                LToast.warningLong("The weight should be between " + formatFloat(minMax.getMin()) + " kg - " + formatFloat(minMax.getMax()) + " kg");
                return;
            }

            if (isEdit) {
                tableVitals.setRegistrationId(StaticData.getCurrentVital().getRegistrationId());

            } else {
                if (VISIBLE == llAadhaar.getVisibility()) {
                    registrationId = getRegistrationId();
                }
                if (null == registrationId || registrationId.equals("")) {
                    LToast.warning("Enter Registration ID");
                    return;
                }
                tableVitals.setRegistrationId(registrationId.toUpperCase());
            }

            tableVitals.setVitalType("Weight");
            tableVitals.setVitalDateTime(getFormattedDateTime(getSurveyDateTimeMillis()));
            tableVitals.setVitalDateTimeMillis(getSurveyDateTimeMillis());

            tableVitals.setVitalSubTypes("Weight");
            tableVitals.setVitalValues(roundOff(etWeight.getText().toString()));
            tableVitals.setVitalUnits("kg");
            tableVitals.setNotes(etNotes.getText().toString());
            tableVitals.setDeviceName(deviceName);
            tableVitals.setGpsLocation(getLocation());
            tableVitals.setQrScanned(qrScanned);

            if (isEdit) {
                dataManager.updateVital(tableVitals, deviceId);
                LToast.success(AssetReader.getLangKeyword("updated"));
                StaticData.setCurrentVital(tableVitals);
            } else {
                dataManager.addVital(tableVitals, deviceId);
                LToast.success(AssetReader.getLangKeyword("saved"));
            }

            finish();
        }
    }

    @Override
    public void onBleScan(final BluetoothDevice device) {
        if (null != itemBluetoothDevices) {

            if (LUtils.toConnectDevice(device.getName(), device.getAddress(), itemBluetoothDevices)) {
                if (device.getName().equals(CHIPSEA_DEVICE) || device.getName().equals(SWAN_DEVICE)) {
                    tryConnecting(device);

                } else if (device.getName().equals(MEDCHECK_WEIGHT)) {
                    tryConnectingMedcheck(device);
                }
            }
        } else if (null != device.getName()
                && (device.getName().equals(CHIPSEA_DEVICE) || device.getName().equals(SWAN_DEVICE))) {
            tryConnecting(device);

        } else if (null != device.getName() && device.getName().equals(MEDCHECK_WEIGHT)) {
            tryConnecting(device);
        }
    }

    private void tryConnectingMedcheck(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            MedcheckWeightConnection.getInstance().connect(this);
            connectionState = STATE_CONNECTING;
            macAddress      = device.getAddress();
        }
    }

    private void tryConnecting(BluetoothDevice device) {
        if (STATE_DISCONNECTED == connectionState) {
            bleConnection.connectDevice(device);
            connectionState = STATE_CONNECTING;
            macAddress      = device.getAddress();

            if (device.getName().equals(CHIPSEA_DEVICE)) {
                deviceNameTemp = CHIPSEA_DEVICE;
            } else if (device.getName().equals(SWAN_DEVICE)) {
                deviceNameTemp = SWAN_DEVICE;
            }
        }
    }

    @Override
    public void onConnected() {
        LLog.d(TAG, "onConnected");

        isOnceConnected = true;
        connectionState = STATE_CONNECTED;
        setConnectionStatus("Connected");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnSave.setEnabled(true);
                btnSave.setBackgroundResource(R.drawable.btn_background);
            }
        });
    }

    private void setConnectionStatus(final String connectionStatus) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectionStatus.setText(connectionStatus);
            }
        });
    }

    @Override
    public void onServicesDiscovered() {
        // Get weight
        if (!isTestStarted) {
            bleConnection.startTest();
            setConnectionStatus("Reading Weight...");
            isTestStarted = true;
        }
    }

    @Override
    public void onDisconnected() {
        connectionState = STATE_DISCONNECTED;
        setConnectionStatus("Not Connected");
        isTestStarted = false;
    }

    @Override
    public void onValueRead(final int type, final String values) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (SYNC_TEST_RESULT == type) {
                    int   weight        = Integer.parseInt(values);
                    float weightDecimal = (float) ((float) weight / 10.0);
                    etWeight.setText("" + weightDecimal);
                    etWeight.setSelection(etWeight.getText().toString().length());

                } else if (SWAN_TEST_RESULT == type) {
                    String[] split         = values.split("-");
                    float    weightDecimal = Float.parseFloat(split[0]);
                    etWeight.setText(values);
                    etWeight.setSelection(etWeight.getText().toString().length());
                }

                deviceName = deviceNameTemp;
                deviceId   = macAddress;
            }
        });
    }

    @Override
    public void onFailure() {

    }
}