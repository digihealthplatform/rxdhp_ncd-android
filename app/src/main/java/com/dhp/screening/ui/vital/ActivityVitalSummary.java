package com.dhp.screening.ui.vital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dhp.screening.data.asset.AssetReader;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterVitalSummary;
import com.dhp.screening.data.item.ItemPrescriptionDetails;
import com.dhp.screening.data.item.ItemVitalHeader;
import com.dhp.screening.data.item.ItemVitalValue;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LUtils;

public class ActivityVitalSummary extends BaseActivity {

    private String registrationId;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.et_date)
    EditText etDate;

    @BindView(R.id.tv_no_vitals)
    TextView tvNoVitals;

    @BindView(R.id.rv_vitals_summary)
    RecyclerView rvSummary;

    @Inject
    DataManager dataManager;

    private AdapterVitalSummary adapter;

    HashMap<String, Integer> priority = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_summary);
        initToolbar(toolbar, AssetReader.getLangKeyword("summary"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        priority.put("Height", 1);
        priority.put("Weight", 2);
        priority.put("BMI", 3);
        priority.put("Blood Pressure", 4);
        priority.put("Pulse", 5);
        priority.put("RBS", 6);
        priority.put("FBS", 7);
        priority.put("PPBS", 8);
        priority.put("Hemoglobin", 9);
        priority.put("Albumin", 10);
        priority.put("Blood Group", 11);
        priority.put("Prescription", 12);

        registrationId = StaticData.getRegistrationId();

        adapter = new AdapterVitalSummary(this);

        etDate.setText(LUtils.getCurrentDate());

        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = java.util.Calendar.getInstance();

                DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                                String date = LUtils.getFormattedDate(dayOfMonth, monthOfYear, year);
                                etDate.setText(date);

                                showVitals();
                            }
                        },

                        calendar.get(java.util.Calendar.YEAR),
                        calendar.get(java.util.Calendar.MONTH),
                        calendar.get(java.util.Calendar.DATE));

                datePickerDialog.show(getFragmentManager(), "DatePickerDialog");
            }
        });

        rvSummary.setLayoutManager(new LinearLayoutManager(this));
        rvSummary.setAdapter(adapter);

        showVitals();
    }

    private void showVitals() {
        List<ItemVitalHeader> vitalHeaders = dataManager.getAllVitalHeaders(registrationId);

        String date = etDate.getText().toString();

        ArrayList<ItemVitalValue> vitalValues = new ArrayList<>(10);

        for (ItemVitalHeader vitalHeader : vitalHeaders) {
            if (vitalHeader.getDate().equals(date)) {
                vitalValues = selectVitals(vitalHeader.getVitals());
                break;
            }
        }

        if (0 == vitalValues.size()) {
            tvNoVitals.setVisibility(View.VISIBLE);
            rvSummary.setVisibility(View.GONE);

        } else {
            tvNoVitals.setVisibility(View.GONE);
            rvSummary.setVisibility(View.VISIBLE);

            Collections.sort(vitalValues, new Comparator<ItemVitalValue>() {
                @Override
                public int compare(ItemVitalValue left, ItemVitalValue right) {
                    int val = 0;

                    try {
                        val = priority.get(left.getVital()) - priority.get(right.getVital());
                    } catch (Exception e) {
                        LLog.d("wrong-", "compare: " + left.getVital() + "  " + right.getVital());
                        LLog.printStackTrace(e);
                    }
                    return val;
                }
            });
            adapter.setItems(vitalValues);
        }
    }

    private ArrayList<ItemVitalValue> selectVitals(List<TableVitals> vitals) {
        ArrayList<ItemVitalValue> vitalValues = new ArrayList<>(10);

        String date = etDate.getText().toString();

        for (TableVitals vital : vitals) {
            switch (vital.getVitalType()) {

                case "Height":
                    vitalValues.add(new ItemVitalValue(vital.getVitalType(), vital.getVitalValues(), vital.getVitalUnits()));
                    break;

                case "Weight":
                    String bmiText = "";

                    String latestHeight = dataManager.getHeightOnDate(StaticData.getRegistrationId(), date);

                    if (null != latestHeight) {
                        String[] height = latestHeight.split(";");

                        double bmi = LUtils.findBmi(Float.parseFloat(height[0]),
                                height[1],
                                Float.parseFloat(vital.getVitalValues()));

                        bmiText = " (BMI: " + bmi + "kg/m2" + ")";
                    }

                    vitalValues.add(new ItemVitalValue(vital.getVitalType(), vital.getVitalValues(), vital.getVitalUnits() + bmiText));

                    break;

                case "Blood Pressure":
                    String[] values = vital.getVitalValues().split(";");
                    String[] units = vital.getVitalUnits().split(";");

                    vitalValues.add(new ItemVitalValue(vital.getVitalType(), values[0] + " / " + values[1], units[0]));

                    if (!values[2].equals("0")) {
                        vitalValues.add(new ItemVitalValue("Pulse", values[2], units[2]));
                    }

                    break;

                case "Blood Sugar":
                    vitalValues.add(new ItemVitalValue(vital.getVitalSubTypes(), vital.getVitalValues(), vital.getVitalUnits()));
                    break;

                case "Hemoglobin":
                    vitalValues.add(new ItemVitalValue(vital.getVitalType(), vital.getVitalValues(), vital.getVitalUnits()));
                    break;

                case "Albumin":
                    vitalValues.add(new ItemVitalValue(vital.getVitalType(), vital.getVitalValues(), vital.getVitalUnits()));
                    break;

                case "Blood Group":
                    vitalValues.add(new ItemVitalValue(vital.getVitalType(), vital.getVitalValues(), ""));
                    break;

                case "Prescription": {

                    ItemPrescriptionDetails prescriptionDetails = LUtils.getPrescriptionDetails(vital.getVitalValues());

                    String prescription = prescriptionDetails.getPrescription();
                    String timing       = prescriptionDetails.getTimings();
                    String food         = prescriptionDetails.getFood();

                    timing = timing.replaceAll(",", ", ");
                    String foodShort = food.equals("Before food") ? "BF" : "AF";

                    String timingBinary = "";
                    if (timing.contains("Morning")) {
                        timingBinary += "1-";
                    } else {
                        timingBinary += "0-";
                    }

                    if (timing.contains("Afternoon")) {
                        timingBinary += "1-";
                    } else {
                        timingBinary += "0-";
                    }

                    if (timing.contains("Night")) {
                        timingBinary += "1";
                    } else {
                        timingBinary += "0";
                    }

                    vitalValues.add(new ItemVitalValue(vital.getVitalType(), prescription + "\n" + timingBinary + " " + foodShort + "\n" + LUtils.getDaysText(prescriptionDetails.getDays()), ""));
                }
                break;
            }
        }
        return vitalValues;
    }
}