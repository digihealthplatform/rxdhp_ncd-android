package com.dhp.screening.ui.admin;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.lib.barcodereader.ui.ActivityQr;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_GENDER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_IDENTITY_TYPES;
import static com.dhp.screening.data.asset.AssetReader.KEY_USER_TYPES;
import static com.dhp.screening.data.asset.AssetReader.getEnglishIndex;
import static com.dhp.screening.util.LConstants.DEFAULT_PARTNER_ID;
import static com.dhp.screening.util.LConstants.SHOW_SCAN_QR;
import static com.dhp.screening.util.LConstants.ZXING_CAMERA_PERMISSION;
import static com.dhp.screening.util.LUtils.calculateAge;
import static com.dhp.screening.util.LUtils.calculateYob;
import static com.dhp.screening.util.LUtils.getFormattedDate;
import static com.dhp.screening.util.LUtils.getGenderIndex;
import static com.dhp.screening.util.LUtils.getSpinnerIndexOther;
import static com.dhp.screening.util.LUtils.hasValue;


public class ActivityAddUser extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_save)
    Button btnSave;

    @BindView(R.id.tv_scan_qr)
    TextView tvScanQr;

    @BindView(R.id.tv_full_name)
    TextView tvFullName;

    @BindView(R.id.tv_user_name_title)
    TextView tvUsernameTitle;

    @BindView(R.id.tv_password_title)
    TextView tvPasswordTitle;

    @BindView(R.id.tv_user_type)
    TextView tvUserType;

    @BindView(R.id.tv_identity)
    TextView tvIdentity;

    @BindView(R.id.tv_gender)
    TextView tvGender;

    @BindView(R.id.tv_date_of_birth)
    TextView tvDobTitle;

    @BindView(R.id.tv_or)
    TextView tvOr;

    @BindView(R.id.tv_years)
    TextView tvYears;

    @BindView(R.id.tv_phone)
    TextView tvPhone;

    @BindView(R.id.tv_email_id)
    TextView tvEmailId;

    @BindView(R.id.tv_house_number)
    TextView tvHouseNumber;

    @BindView(R.id.tv_street)
    TextView tvStreet;

    @BindView(R.id.tv_post)
    TextView tvPost;

    @BindView(R.id.tv_locality)
    TextView tvLocality;

    @BindView(R.id.tv_district)
    TextView tvDistrict;

    @BindView(R.id.tv_state)
    TextView tvState;

    @BindView(R.id.tv_country)
    TextView tvCountry;

    @BindView(R.id.tv_pin_code)
    TextView tvPinCode;

    @BindView(R.id.et_full_name)
    EditText etFullName;

    @BindView(R.id.et_username)
    EditText etUsername;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.sp_user_types)
    Spinner spUserTypes;

    @BindView(R.id.et_user_type_other)
    EditText etUserTypeOther;

    @BindView(R.id.rb_male)
    RadioButton rbMale;

    @BindView(R.id.rb_female)
    RadioButton rFemale;

    @BindView(R.id.rb_other)
    RadioButton rbOther;

    @BindView(R.id.et_dob)
    EditText etDob;

    @BindView(R.id.et_age)
    EditText etAge;

    @BindView(R.id.et_phone)
    EditText etPhone;

    @BindView(R.id.et_email_id)
    EditText etEmailId;

    @BindView(R.id.et_identity_type_other)
    EditText etIdentityTypeOther;

    @BindView(R.id.iv_scan_qr)
    ImageView ivScanQr;

    @BindView(R.id.et_identity_no)
    EditText etIdentityNumber;

    @BindView(R.id.sp_identity_type)
    Spinner spIdentityType;

    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    @BindView(R.id.et_house_number)
    EditText etHouseNumber;

    @BindView(R.id.et_street)
    EditText etStreet;

    @BindView(R.id.et_post)
    EditText etPost;

    @BindView(R.id.et_locality)
    EditText etLocality;

    @BindView(R.id.et_district)
    EditText etDistrict;

    @BindView(R.id.et_state)
    AutoCompleteTextView etState;

    @BindView(R.id.et_country)
    EditText etCountry;

    @BindView(R.id.et_pin)
    EditText etPin;

    @BindView(R.id.cb_allow_prescription)
    CheckBox cbAllowPrescription;

    @BindView(R.id.cb_is_demo_user)
    CheckBox cbIsDemoUser;

    @Inject
    DataManager dataManager;

    private boolean autoSet;

    private TableUser tableUser;

    private boolean isEdit;

    private String gender = "";

    private String identityType = "";
    private String userType     = "";

    private String[] identityList;
    private String[] identityListEnglish;
    private String[] userTypes;
    private String[] userTypesEnglish;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        initToolbar(toolbar, AssetReader.getLangKeyword("add_new_user"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        initLanguage();

        if (!SHOW_SCAN_QR) {
            tvScanQr.setVisibility(View.GONE);
            ivScanQr.setVisibility(View.GONE);
        }

        identityList = AssetReader.getSpinnerArray(KEY_IDENTITY_TYPES);
        identityListEnglish = AssetReader.getSpinnerArrayEnglish(KEY_IDENTITY_TYPES);

        userTypes = AssetReader.getSpinnerArray(KEY_USER_TYPES);
        userTypesEnglish = AssetReader.getSpinnerArrayEnglish(KEY_USER_TYPES);

        LView.setSpinnerAdapter(this, spUserTypes, userTypes);

        isEdit = getIntent().getBooleanExtra("is_edit", false);

        rgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = rgGender.indexOfChild(rgGender.findViewById(indexId));
                gender = getEnglishIndex(KEY_GENDER_TYPES, selectedIndex);
            }
        });

        String[] states = AssetReader.getSpinnerArray("states");

        ArrayAdapter<String> adapterStates = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, states);
        etState.setAdapter(adapterStates);


        LView.setSpinnerAdapter(this, spIdentityType, identityList);

        if (isEdit) {
            getSupportActionBar().setTitle(AssetReader.getLangKeyword("update_user"));

            String username = getIntent().getStringExtra("username");

            tableUser = dataManager.getUser(username);

            etFullName.setText(tableUser.getFullName());
            etFullName.setEnabled(false);

            etUsername.setText(tableUser.getUserId());
            etUsername.setEnabled(false);

            etPassword.setText(tableUser.getPassword());

            userType = tableUser.getUserType();
            identityType = tableUser.getIdentityType();

            etDob.setText(tableUser.getDob());

            if (0 != tableUser.getYob()) {
                etAge.setText("" + LUtils.calculateAge(tableUser.getYob()));
            }

            if (hasValue(tableUser.getGender())) {
                ((RadioButton) rgGender.getChildAt(getGenderIndex(tableUser.getGender()))).setChecked(true);
            }

            int spinnerIndex = LUtils.getSpinnerIndexOther(userType, userTypesEnglish);

            if (-1 != spinnerIndex) {

                spUserTypes.setSelection(spinnerIndex);

                if (userTypesEnglish[spinnerIndex].equals("Other")) {
                    etUserTypeOther.setText(userType);
                }
            }

            int index = LUtils.getSpinnerIndexOther(identityType, identityListEnglish);

            if (0 < index) {

                autoSet = true;

                spIdentityType.setSelection(index);

                etIdentityNumber.setVisibility(VISIBLE);
                etIdentityNumber.setText(tableUser.getIdentityNumber());

                if (index == identityListEnglish.length - 2) {
                    etIdentityTypeOther.setVisibility(VISIBLE);
                    etIdentityTypeOther.setText(identityType);
                }
            }

            if (tableUser.isQrScanned()) {
                spIdentityType.setEnabled(false);
            }

            etPhone.setText(tableUser.getPhoneNumber());
            etEmailId.setText(tableUser.getEmailId());
            etHouseNumber.setText(tableUser.getHouseNo());
            etStreet.setText(tableUser.getStreet());
            etPost.setText(tableUser.getPost());
            etLocality.setText(tableUser.getLocality());
            etDistrict.setText(tableUser.getDistrict());
            etState.setText(tableUser.getState());
            etCountry.setText(tableUser.getCountry());

            if (0 != tableUser.getPinCode()) {
                etPin.setText("" + tableUser.getPinCode());
            }

            cbAllowPrescription.setChecked(tableUser.isAllowPrescription());
            cbIsDemoUser.setChecked(tableUser.isDemoUser());

            btnSave.setText(AssetReader.getLangKeyword("update"));

        } else {
            tableUser = new TableUser();
            etCountry.setText(AssetReader.getDefaultCountry());
        }

        spUserTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (0 != position) {

                    userType = userTypesEnglish[position];

                    if (userType.equals("Other")) {
                        etUserTypeOther.setVisibility(VISIBLE);
                    } else {
                        etUserTypeOther.setVisibility(View.GONE);
                    }

                    if (!isEdit) {
                        if (AssetReader.isPrescriptionAllowed(userType)) {
                            cbAllowPrescription.setChecked(true);
                        } else {
                            cbAllowPrescription.setChecked(false);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spIdentityType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                onIdentitySelected(position);
                autoSet = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (null != StaticData.getQrResult()) {
            displayAadhaarDetails();
            StaticData.setQrResult(null);
        }
    }

    @OnClick(R.id.iv_scan_qr)
    void scanQr() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            openCamera();
        }
    }

    private void openCamera() {
        Intent intent = new Intent(this, ActivityQr.class);
        startActivity(intent);
    }

    @OnClick(R.id.et_dob)
    void onDobClicked() {
        final Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        onDobSet(dayOfMonth, monthOfYear, year);
                    }
                },

                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE));

        datePickerDialog.show(getFragmentManager(), "DatePickerDialogDob");
    }

    private void onDobSet(int dayOfMonth, int monthOfYear, int year) {
        if (LUtils.isFutureDate(dayOfMonth, monthOfYear, year)) {
            LToast.warning(AssetReader.getLangKeyword("cant_select_future_date"));
            return;
        }

        etDob.setText(getFormattedDate(dayOfMonth, monthOfYear, year));
        etAge.setText("" + calculateAge(year));
        etAge.setEnabled(false);
    }

    private boolean isAllFieldsFilled() {
        if (tableUser.getFullName().equals("")) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_name"));
            return false;
        }

        if (tableUser.getUserId().equals("")) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_user_id"));
            return false;
        }

        if (tableUser.getPassword().equals("")) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_password"));
            return false;
        }

        if (!hasValue(userType)) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_user_type"));
            return false;
        }

        if (!tableUser.getPhoneNumber().equals("")) {

            if (AssetReader.getMinPhoneLength() > tableUser.getPhoneNumber().length()
                    || AssetReader.getMaxPhoneLength() < tableUser.getPhoneNumber().length()
                    || !Patterns.PHONE.matcher(tableUser.getPhoneNumber()).matches()) {
                LToast.warning(AssetReader.getLangKeyword("warning_enter_phone"));
                return false;
            }
        }

        if (!tableUser.getEmailId().equals("")
                && !Patterns.EMAIL_ADDRESS.matcher(tableUser.getEmailId()).matches()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_email"));
            return false;
        }

        if (!identityType.isEmpty()
                && identityType.equals("Other")
                && etIdentityTypeOther.getText().toString().isEmpty()) {
            LToast.warning(AssetReader.getLangKeyword("warning_enter_identity_type"));
            return false;
        }

        if (!identityType.isEmpty()
                && !identityType.equals("None")
                && !identityType.equals("Aadhaar")) {

            if (etIdentityNumber.getText().toString().isEmpty()) {
                LToast.warning(AssetReader.getLangKeyword("warning_enter_identity_number"));
                return false;
            } else if (!etIdentityNumber.getText().toString().matches("[A-Za-z0-9]+")) {
                LToast.warningLong(AssetReader.getLangKeyword("warning_enter_valid_identity_number"));
                return false;
            }
        }

        return true;
    }

    private void readValues() {

        tableUser.setFullName(etFullName.getText().toString().trim());
        tableUser.setUserId(etUsername.getText().toString().trim());
        tableUser.setPassword(etPassword.getText().toString());

        if (VISIBLE == etUserTypeOther.getVisibility()) {
            userType = etUserTypeOther.getText().toString();
        }

        tableUser.setUserType(userType);
        tableUser.setDob(etDob.getText().toString());

        try {
            int age = Integer.parseInt(etAge.getText().toString());
            tableUser.setYob(calculateYob(age));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        tableUser.setGender(gender);
        tableUser.setPhoneNumber(etPhone.getText().toString());
        tableUser.setEmailId(etEmailId.getText().toString());

        if (VISIBLE == etIdentityTypeOther.getVisibility()) {
            tableUser.setIdentityType(etIdentityTypeOther.getText().toString());
        } else {
            tableUser.setIdentityType(identityType);
        }

        tableUser.setIdentityNumber(etIdentityNumber.getText().toString().trim().toUpperCase());

        tableUser.setHouseNo(etHouseNumber.getText().toString().trim());
        tableUser.setStreet(etStreet.getText().toString().trim());
        tableUser.setPost(etPost.getText().toString().trim());
        tableUser.setLocality(etLocality.getText().toString().trim());
        tableUser.setDistrict(etDistrict.getText().toString().trim());
        tableUser.setState(etState.getText().toString().trim());
        tableUser.setCountry(etCountry.getText().toString().trim());

        try {
            tableUser.setPinCode(Integer.parseInt(etPin.getText().toString().trim()));
        } catch (Exception e) {
        }

        tableUser.setAllowPrescription(cbAllowPrescription.isChecked());
        tableUser.setDemoUser(cbIsDemoUser.isChecked());
    }

    @OnClick(R.id.btn_save)
    void onSave() {

        readValues();

        if (isAllFieldsFilled()) {
            confirmSave();
        }
    }

    private void confirmSave() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_save"));
        builder.setPositiveButton(AssetReader.getLangKeyword("save"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String username = etUsername.getText().toString();

                List<TableUser> users = dataManager.getAllUsers();

                if (isEdit) {
                    for (TableUser tableUser : users) {
                        if (username.equals(tableUser.getUserId()) && !tableUser.getUserId().equals(username)) {
                            Toasty.warning(ActivityAddUser.this, AssetReader.getLangKeyword("warning_user_exists"), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                } else {
                    for (TableUser tableUser : users) {
                        if (username.equals(tableUser.getUserId())) {
                            Toasty.warning(ActivityAddUser.this, AssetReader.getLangKeyword("warning_user_exists"), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                }

                // Check if partner ID is present
                if (!tableUser.isDemoUser() && dataManager.getPartnerId().equals(DEFAULT_PARTNER_ID)) {
                    LToast.errorLong(AssetReader.getLangKeyword("contact_dhp_for_enrollment"));
                    return;
                }

                if (isEdit) {
                    dataManager.updateUser(tableUser);
                    Toasty.success(ActivityAddUser.this, AssetReader.getLangKeyword("updated"), Toast.LENGTH_SHORT).show();

                } else {
                    dataManager.createUser(tableUser);
                    Toasty.success(ActivityAddUser.this, AssetReader.getLangKeyword("new_user_added"), Toast.LENGTH_SHORT).show();
                }

                finish();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void displayAadhaarDetails() {
        String xml = StaticData.getQrResult();

        try {
            InputStream            is  = new ByteArrayInputStream(xml.getBytes("UTF-8"));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder        db  = dbf.newDocumentBuilder();
            Document               doc = db.parse(new InputSource(is));
            doc.getDocumentElement().normalize();
            NodeList nodelist = doc.getElementsByTagName("PrintLetterBarcodeData");

            NamedNodeMap aadhaarAttributes = nodelist.item(0).getAttributes();
            StaticData.setAadhaarAttributes(aadhaarAttributes);

            LView.setTextFromXml(etFullName, "name");
            LView.setTextFromXml(etHouseNumber, "house");
            LView.setTextFromXml(etStreet, "street");
            LView.setTextFromXml(etPost, "po", "vtc");
            LView.setTextFromXml(etLocality, "loc");
            LView.setTextFromXml(etDistrict, "dist");
            LView.setTextFromXml(etState, "state");
            LView.setTextFromXml(etPin, "pc");

            try {
                int yob = Integer.parseInt(aadhaarAttributes.getNamedItem("yob").getTextContent());
                etAge.setText("" + calculateAge(yob));
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }

            LView.setTextFromXml(etDob, "dob");

            if (aadhaarAttributes.getNamedItem("gender").getTextContent().equals("M")) {
                rgGender.check(R.id.rb_male);
            } else {
                rgGender.check(R.id.rb_female);
            }

            LView.setTextFromXml(etIdentityNumber, "uid");

            int spinnerIndex = getSpinnerIndexOther("Aadhaar", identityListEnglish);
            if (-1 != spinnerIndex) {
                spIdentityType.setSelection(spinnerIndex);
            }

            spIdentityType.setEnabled(false);
            etIdentityTypeOther.setEnabled(false);
            etIdentityNumber.setEnabled(false);

            tableUser.setQrScanned(true);

        } catch (Exception e) {
            LToast.warning(AssetReader.getLangKeyword("warning_invalid_aadhaar_details"));
            LLog.printStackTrace(e);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
            }
        }
        return true;
    }

    private void onIdentitySelected(int position) {
        if (position > 0) {
            identityType = identityListEnglish[position];

            etIdentityNumber.setVisibility(VISIBLE);

            if (!autoSet) {
                etIdentityNumber.setText("");
            }
            etIdentityNumber.setEnabled(true);

            if (identityType.equals("Other")) {
                etIdentityTypeOther.setVisibility(VISIBLE);
                tvScanQr.setVisibility(View.GONE);
                ivScanQr.setVisibility(View.GONE);

            } else {

                if (!autoSet) {
                    etIdentityTypeOther.setText("");
                }
                etIdentityTypeOther.setVisibility(View.GONE);
                tvScanQr.setVisibility(View.GONE);
                ivScanQr.setVisibility(View.GONE);

                if (identityType.equals("None")) {
                    etIdentityNumber.setVisibility(View.GONE);
                    etIdentityNumber.setText("");

                } else if (identityType.equals("Aadhaar")) {
//                    ivScanQr.setVisibility(VISIBLE);
                    etIdentityNumber.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(AssetReader.getLangKeyword("confirm_go_back"));
        builder.setPositiveButton(AssetReader.getLangKeyword("go_back"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityAddUser.super.onBackPressed();
            }
        });
        builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
        builder.show();
    }

    private void initLanguage() {
        tvScanQr.setText(AssetReader.getLangKeyword("scan_from_qr"));
        tvFullName.setText(AssetReader.getLangKeyword("full_name") + " *");
        tvUsernameTitle.setText(AssetReader.getLangKeyword("username_title") + " *");
        tvPasswordTitle.setText(AssetReader.getLangKeyword("password_title") + " *");
        tvUserType.setText(AssetReader.getLangKeyword("user_type") + " *");
        tvIdentity.setText(AssetReader.getLangKeyword("identity_type"));
        tvGender.setText(AssetReader.getLangKeyword("gender_user"));
        tvDobTitle.setText(AssetReader.getLangKeyword("date_of_birth_title_user"));
        tvOr.setText(AssetReader.getLangKeyword("or"));
        tvYears.setText(AssetReader.getLangKeyword("years"));
        tvPhone.setText(AssetReader.getLangKeyword("phone_number"));
        tvEmailId.setText(AssetReader.getLangKeyword("email_id"));

        tvHouseNumber.setText(AssetReader.getLangKeyword("house_number"));
        tvStreet.setText(AssetReader.getLangKeyword("street"));
        tvPost.setText(AssetReader.getLangKeyword("post"));
        tvLocality.setText(AssetReader.getLangKeyword("locality"));
        tvDistrict.setText(AssetReader.getLangKeyword("district"));
        tvState.setText(AssetReader.getLangKeyword("state"));
        tvCountry.setText(AssetReader.getLangKeyword("country"));
        tvPinCode.setText(AssetReader.getLangKeyword("pin_code"));

        etDob.setHint(AssetReader.getLangKeyword("date_of_birth"));
        etAge.setHint(AssetReader.getLangKeyword("age"));

        rbMale.setText(AssetReader.getLangKeyword("male"));
        rFemale.setText(AssetReader.getLangKeyword("female"));
        rbOther.setText(AssetReader.getLangKeyword("other"));

        etUserTypeOther.setHint(AssetReader.getLangKeyword("if_other"));
        etIdentityTypeOther.setHint(AssetReader.getLangKeyword("if_other"));
        etIdentityNumber.setHint(AssetReader.getLangKeyword("identity_number"));

        cbAllowPrescription.setText(AssetReader.getLangKeyword("allow_prescription_entry"));
        cbIsDemoUser.setText(AssetReader.getLangKeyword("is_demo_user"));

        btnSave.setText(AssetReader.getLangKeyword("save"));
    }
}