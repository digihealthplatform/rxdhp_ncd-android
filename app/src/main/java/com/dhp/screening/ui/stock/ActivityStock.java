package com.dhp.screening.ui.stock;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterStock;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.ui.statistics.GeneratePdfTask;
import com.dhp.screening.util.LUtils;

import static com.dhp.screening.util.LConstants.STOCK_REPORT;


public class ActivityStock extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_items)
    RecyclerView rvItems;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @Inject
    DataManager dataManager;

    private boolean inactive;

    private AdapterStock adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock);
        initToolbar(toolbar, AssetReader.getLangKeyword("stock"), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);

        adapter = new AdapterStock(this);

        rvItems.setLayoutManager(new LinearLayoutManager(this));
        rvItems.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (0 < dataManager.getStockItemsReport().size()) {
            getMenuInflater().inflate(R.menu.menu_stock, menu);

        } else {
            menu.clear();
        }

        initLanguageMenu(menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_generate_report: {
                File outputFile = new File(LUtils.getOutputFilePath("ncd_stock_report", dataManager, "pdf"));

                GeneratePdfTask generatePdfTask =
                        new GeneratePdfTask(this, outputFile, dataManager, STOCK_REPORT, null);
                generatePdfTask.execute();

                break;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();

        List<TableStock> items = dataManager.getStockItems();
        adapter.setItems(items);

        if (0 < items.size()) {
            tvEmpty.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.VISIBLE);
        }

        invalidateOptionsMenu();
    }

    @OnClick(R.id.btn_add_item)
    void addItem() {
        startActivity(new Intent(this, ActivityAddStock.class));
    }

    private void initLanguageMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_generate_report);

        if (null != item) {
            item.setTitle(AssetReader.getLangKeyword("generate_report"));
        }
    }
}