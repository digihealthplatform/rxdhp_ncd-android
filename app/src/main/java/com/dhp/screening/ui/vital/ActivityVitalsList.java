package com.dhp.screening.ui.vital;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.adapter.AdapterVitals;
import com.dhp.screening.data.item.ItemVital;
import com.dhp.screening.data.item.ItemVitalHeader;
import com.dhp.screening.data.table.TableVitals;

import static com.dhp.screening.data.item.ItemVital.VITAL_CHILD;
import static com.dhp.screening.data.item.ItemVital.VITAL_HEADER;

public class ActivityVitalsList extends ActivityVitalBase {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Inject
    DataManager dataManager;

    @BindView(R.id.tv_empty)
    TextView tvEmpty;

    @BindView(R.id.rv_vitals)
    RecyclerView rvVitals;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vital_list);
        initToolbar(toolbar, getString(R.string.vitals_list), true);
        ButterKnife.bind(this);
        getActivityComponent(this).inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        todo, Show only demo vitals for demo users

        List<TableVitals> vitalsList = dataManager.getAllVitals("", true);

        List<ItemVitalHeader> vitalHeaders = dataManager.getAllVitalHeaders("");

        List<ItemVital> itemVitals = new ArrayList<>();

        boolean firstTime = true;

        for (ItemVitalHeader vitalHeader : vitalHeaders) {
            itemVitals.add(new ItemVital(VITAL_HEADER, vitalHeader, null));

            if (firstTime) {
                for (TableVitals vital : vitalHeader.getVitals()) {
                    itemVitals.add(new ItemVital(VITAL_CHILD, null, vital));
                }

                firstTime = false;
            }
        }

        if (0 == vitalsList.size()) {
            tvEmpty.setVisibility(View.VISIBLE);

        } else {
//            btnPrint.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
            AdapterVitals adapterVitals = new AdapterVitals(this, vitalHeaders, itemVitals);
            rvVitals.setLayoutManager(new LinearLayoutManager(this));
            rvVitals.setAdapter(adapterVitals);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}