package com.dhp.screening.util;

import android.os.AsyncTask;

import com.dhp.screening.data.item.ItemFileAttachment;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import static com.dhp.screening.util.LUtils.getLocalFilesPath;


public class TaskMoveFiles extends AsyncTask<Void, Void, Void> {

    private boolean isError;

    private final Callback callback;

    private ArrayList<ItemFileAttachment> inputFilePaths;
    private ArrayList<ItemFileAttachment> outputFilePaths;

    public interface Callback {
        void onFilesMoved(ArrayList<ItemFileAttachment> outputFilePaths);

        void onFileMoveError();
    }

    public TaskMoveFiles(ArrayList<ItemFileAttachment> inputFilePaths, Callback callback) {
        this.inputFilePaths = inputFilePaths;
        this.outputFilePaths = new ArrayList<>();
        this.callback = callback;
    }

    @Override
    protected void onPostExecute(Void result) {
        if (isError) {
            callback.onFileMoveError();
        } else {
            callback.onFilesMoved(outputFilePaths);
        }
    }

    @Override
    protected Void doInBackground(Void... params) {

        for (ItemFileAttachment input : inputFilePaths) {
            try {
                String destinationFilePath = getLocalFilesPath().getPath() + "/tmpfile_" + Calendar.getInstance().getTimeInMillis() + input.getFileExtension();

                LUtils.copyFile(input.getFilePath(), destinationFilePath);

                File outputFile = new File(destinationFilePath);

                ItemFileAttachment output = new ItemFileAttachment();
                output.setFileName(outputFile.getName());
                output.setFilePath(destinationFilePath);

                outputFilePaths.add(output);

            } catch (Exception e) {
                LLog.printStackTrace(e);
                isError = true;
            }
        }

        return null;
    }
}