package com.dhp.screening.util;


public class LConstants {

    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "dhp#123";

    public static final String SUPER_ADMIN_1_PASSWORD = "dhp@789$";

    public static final String SUPER_ADMIN_2_PASSWORD  = "d#4h$5p@6";
    public static final String SUPER_ADMIN_3_PASSWORD  = "dhp!@#321";
    public static final String SUPER_ADMIN_4_PASSWORD  = "dhp$456!";
    public static final String SUPER_ADMIN_HB_PASSWORD = "dhp#123hb";

    public static final String SUPER_ADMIN_PARTNER_ID_PASSWORD    = "dhp!357";
    public static final String SUPER_ADMIN_DEVICE_NUMBER_PASSWORD = "&9999";
    public static final String SUPER_ADMIN_EXPORT_PASSWORD        = "dhp246@x";
    public static final String SUPER_ADMIN_IMPORT_PASSWORD        = "dhp357@i";

    public static final String INPUT_JSON_DECRYPT_KEY = "dhp123enc@key";

    public static final String DATABASE_ENCRYPTION_KEY = "dhp@db123key";

    public static final String DEFAULT_DATABASE_IMPORT_PATH = "/Bluetooth";

    public static String DEFAULT_STRIP_CODE    = "CKD123456ABC";
    public static String DEFAULT_CAMPAIGN_NAME = "Bangalore Health Drive 2017";

    public static final int CODE_WRITE_PERMISSION    = 101;
    public static final int CODE_LOCATION_PERMISSION = 102;

    public static final int VITAL_HEIGHT     = 1;
    public static final int VITAL_WEIGHT     = 2;
    public static final int VITAL_RBS        = 3;
    public static final int VITAL_BP         = 4;
    public static final int VITAL_HEMOGLOBIN = 5;
    public static final int VITAL_ALBUMIN    = 6;

    public static final int ACTION_OPEN_FILE    = 101;
    public static final int ACTION_OPEN_CAMERA  = 102;
    public static final int ACTION_OPEN_GALLERY = 103;

    public static final int REQUEST_FILE    = 111;
    public static final int REQUEST_GALLERY = 112;
    public static final int REQUEST_CAPTURE = 113;

    public static final int ZXING_CAMERA_PERMISSION = 511;

    public static final String DEFAULT_PARTNER_ID = "DEMO";

    public static final int DEFAULT_DURATION           = 30;
    public static final int DEFAULT_AUTO_SYNC_INTERVAL = 3600;

    public static final int TYPE_CALIBRATE = 110;
    public static final int TYPE_TEST      = 111;

    public static final String HB_FOLDER_PATH          = ".Hemoglobin";
    public static final String HB_UPLOADED_FOLDER_PATH = ".HemoglobinUploaded";

    public static final String JSON_SYNC_URL = "http://13.126.85.194/cardio/ncduploadtoserver.php";

    public static final String SCREEN_TYPE = "ncd-test";

    public static final int PDF_VITAL                = 1;
    public static final int PDF_MISSING_DEMOGRAPHICS = 2;
    public static final int REPORT_SUMMARY           = 3;
    public static final int STOCK_REPORT             = 4;
    public static final int ANALYSIS_REPORT          = 5;
    public static final int PDF_VITAL_INDIVIDUAL     = 6;
    public static final int PDF_RISK_REPORT          = 7;

    public static final boolean SHOW_SCAN_QR = true;
}