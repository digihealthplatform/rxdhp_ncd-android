package com.dhp.screening.util;


import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.support.v4.content.ContextCompat;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemPrescriptionDetails;
import com.dhp.screening.data.table.TableVitals;
import com.google.gson.Gson;
import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class LUtils {

    private static final String TAG = "LUtils";

    public static String getFormattedDate(int date, int month, int year) {

        ++month;

        String formattedDate = "";

        if (date < 10) {
            formattedDate = "0" + date;
        } else {
            formattedDate = "" + date;
        }

        formattedDate = formattedDate + "/";

        if (month < 10) {
            formattedDate = formattedDate + "0" + month;
        } else {
            formattedDate = formattedDate + "" + month;
        }

        formattedDate = formattedDate + "/" + year;

        return formattedDate;
    }

    public static int calculateYob(int age) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - age;
    }

    public static int calculateAge(int year) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear - year;
    }

    private static long getDifferenceInDays(int date, int month, int year) {
        Calendar currentDay = Calendar.getInstance();
        Calendar birthDay   = Calendar.getInstance();
        birthDay.set(Calendar.YEAR, year);
        birthDay.set(Calendar.MONTH, month);
        birthDay.set(Calendar.DATE, date);
        birthDay.set(Calendar.HOUR_OF_DAY, 0);
        birthDay.set(Calendar.MINUTE, 0);
        birthDay.set(Calendar.SECOND, 0);
        birthDay.set(Calendar.MILLISECOND, 0);

        return (long) (currentDay.getTimeInMillis() - birthDay.getTimeInMillis()) / (1000 * 60 * 60 * 24);
    }

    public static boolean isFutureDate(int dayOfMonth, int monthOfYear, int year) {
        if (getDifferenceInDays(dayOfMonth, monthOfYear, year) < 0) {
            return true;
        }
        return false;
    }

    public static Drawable getDrawable(int drawableId) {
        return ContextCompat.getDrawable(LApplication.getContext(), drawableId);
    }

    public static String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return getFormattedDate(calendar.get(Calendar.DATE), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
    }

    public static String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        return getFormattedTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }

    public static String getCurrentDateTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatter.format(new Date(Calendar.getInstance().getTimeInMillis()));
    }

    public static long getCurrentDateTimeMillis() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public static String getFormattedDateTime(long dateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return formatter.format(new Date(dateTime));
    }

    public static String getFormattedDate(long dateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(new Date(dateTime));
    }

    public static String getFormattedDateTimeAm(long dateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("h:mm a, dd/MM/yyyy");
        return formatter.format(new Date(dateTime));
    }

    public static long dateTimeToMilli(String dateTime) {
        SimpleDateFormat sdf  = new SimpleDateFormat("h:mm a dd/MM/yyyy");
        Date             date = null;
        try {
            date = sdf.parse(dateTime);
            return date.getTime();
        } catch (ParseException e) {
            LLog.printStackTrace(e);
        }
        return 0;
    }

    public static long getLongTime(String dateTime) {
        SimpleDateFormat sdf  = new SimpleDateFormat("dd/MM/yyyy");
        Date             date = null;
        try {
            date = sdf.parse(dateTime);
            return date.getTime();
        } catch (ParseException e) {
            LLog.printStackTrace(e);
        }
        return 0;
    }

    public static String getFormattedTime(int hour, int min) {

        String time;
        String am_pm;

        if (hour < 1) {
            hour  = 12;
            am_pm = " AM";
        } else if (hour < 12) {
            am_pm = " AM";
        } else if (hour == 12) {
            hour  = 12;
            am_pm = " PM";
        } else {
            hour  = hour % 12;
            am_pm = " PM";
        }

        String minute    = Integer.toString(min);
        String hourOfDay = Integer.toString(hour);

        if (min < 10) {
            minute = "0" + minute;
        }

//        if (hour < 10) {
//            hourOfDay = "0" + hourOfDay;
//        }

        time = hourOfDay + ":" + minute + am_pm;


        return time;
    }

    public static double findBmi(float height, String heightUnit, float weight) {

        try {
            float bmi = 0.0f;

            if (heightUnit.equals("cm")) {
                height = height / 100.0f;
                bmi    = weight / (height * height);
                return Math.round(bmi * 100.0) / 100.0;

            } else if (heightUnit.equals("inch")) {
                height = height * 0.0254f;
                bmi    = weight / (height * height);
                return Math.round(bmi * 100.0) / 100.0;
            }

        } catch (Exception e) {
        }

        return 0.0;
    }

    public static String getFileName(int year, int month, int date, int hour, int min, int second) {
        String fileName;

        ++month;

        String am_pm;
        String sDate  = "" + date;
        String sMonth = "" + month;

        if (hour < 1) {
            hour  = 12;
            am_pm = "am";
        } else if (hour < 12) {
            am_pm = "am";
        } else if (hour == 12) {
            hour  = 12;
            am_pm = "pm";
        } else {
            hour  = hour % 12;
            am_pm = "pm";
        }

        String minute    = Integer.toString(min);
        String hourOfDay = Integer.toString(hour);

        if (min < 10) {
            minute = "0" + minute;
        }

        if (hour < 10) {
            hourOfDay = "0" + hourOfDay;
        }

        if (date < 10) {
            sDate = "0" + date;
        }

        if (month < 10) {
            sMonth = "0" + month;
        }

        fileName = sDate + "-" + sMonth + "-" + year + "_" + hourOfDay + "-" + minute + "-" + second + am_pm;

        return fileName;
    }

    public static String prependZeros(String value, int totalLength) {

        int neededZeros = totalLength - value.length();

        for (int i = 0; i < neededZeros; i++) {
            value = "0" + value;
        }

        return value;
    }

    public static int getItemWithOthersIndex(String item, String[] itemWithOthers) {
        if (null == item || item.equals("")) {
            return -1;
        }

        for (int i = 0; i < itemWithOthers.length; ++i) {
            if (item.equals(itemWithOthers[i])) {
                return i;
            }
        }

        return itemWithOthers.length - 1;
    }

    public static boolean isOther(String item, String[] items) {
        if (null == items || item.equals("")) {
            return false;
        }

        for (String item1 : items) {
            if (item.equals(item1)) {
                return false;
            }
        }

        return true;
    }

    public static boolean hasValue(String text) {
        return !(null == text || text.isEmpty());
    }

    public static int getSpinnerIndexOther(String item, String[] itemWithOthers) {
        if (null == item || item.equals("")) {
            return -1;
        }

        int otherIndex = 0;

        for (int i = 0; i < itemWithOthers.length; ++i) {
            if (itemWithOthers[i].equals("Other")) {
                otherIndex = i;
            }

            if (item.equals(itemWithOthers[i])) {
                return i;
            }
        }

        return otherIndex;
    }

    public static int getRadioWithOthersIndex(String item, String[] itemWithOthers) {
        if (null == itemWithOthers || null == item || item.equals("")) {
            return -1;
        }

        for (int i = 0; i < itemWithOthers.length; i++) {
            if (itemWithOthers[i].equals(item)) {
                return i;
            }
        }

        return itemWithOthers.length - 1;
    }

    public static int getGenderIndex(String gender) {
        if (gender.equals("Male")) {
            return 0;

        } else if (gender.equals("Female")) {
            return 1;

        } else if (gender.equals("Other")) {
            return 2;
        }

        return 0;
    }

    public static String getTextFromXml(String key) {
        String value = "";

        try {
            value = StaticData.getAadhaarAttributes().getNamedItem(key).getTextContent();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return value;
    }

    public static int getRadioIndex(String item, String[] items) {
        int index = -1;

        if (null == item || item.equals("")) {
            return index;
        }

        for (int i = 0; i < items.length; ++i) {

            if (item.equals(items[i])) {
                index = i;
                break;
            }
        }

        return index;
    }

    public static int getYesNoIndex(String yesNo) {
        switch (yesNo) {
            case "Yes":
                return 0;

            case "No":
                return 1;

            default:
                return -1;
        }
    }

    public static int getYesNoUnsureIndex(String yesNo) {
        switch (yesNo) {
            case "Yes":
                return 0;

            case "No":
                return 1;

            case "Unsure":
                return 2;

            default:
                return -1;
        }
    }

    public static int getDietIndex(String yesNo) {
        switch (yesNo) {
            case "Vegetarian":
                return 0;

            case "Non-vegetarian":
                return 1;

            default:
                return -1;
        }
    }

    public static int getColorValue(int resource) {
        return LApplication.getContext().getResources().getColor(resource);
    }

    public static Integer[] RANGE_COLORS = new Integer[]{
            R.color.white,
            R.color.green_range,
            R.color.yellow,
            R.color.orange,
            R.color.red,
            R.color.dark_red,
    };

    public static String exportCsv(DataManager dataManager) {
        try {
            String filePath = getCsvFilePath(dataManager);

            CSVWriter writer = new CSVWriter(new FileWriter(filePath));
            writeHeaders(writer);
            writeData(writer, dataManager);
            writer.close();

            return filePath;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static String getCsvFilePath(DataManager dataManager) {
        File outputPath = new File(Environment.getExternalStorageDirectory(),
                LApplication.getContext().getString(R.string.ncd_reports_folder));

        String prefix = dataManager.getPartnerId();

        Calendar calendar = Calendar.getInstance();

        String fileName = LApplication.getContext().getString(R.string.prefix_export_backup) + "_"
                + LUtils.getFileName(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND))
                + "_" + prefix;

        int  i          = 1;
        File outputFile = new File(outputPath.getPath(), fileName + ".csv");

        while (outputFile.exists()) {
            outputFile = new File(outputPath.getPath(), fileName + "(" + i++ + ").csv");
        }

        return outputFile.getPath();
    }

    public static String getOutputFilePath(String prefix, DataManager dataManager, String format) {

        File outputPath = new File(Environment.getExternalStorageDirectory(),
                LApplication.getContext().getString(R.string.ncd_reports_folder));

        Calendar calendar = Calendar.getInstance();

        String fileName = prefix + "_"
                + dataManager.getPartnerId()
                + "_"
                + dataManager.getDeviceNumber()
                + "_"
                + LUtils.getFileName(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND));

        int i = 1;

        File outputFile = new File(outputPath.getPath(), fileName + "." + format);

        while (outputFile.exists()) {
            outputFile = new File(outputPath.getPath(), fileName + "(" + i++ + ")." + format);
        }

        return outputFile.getPath();
    }

    public static String getPdfFilePathMissingDemographics(DataManager dataManager) {

        File outputPath = new File(Environment.getExternalStorageDirectory(),
                LApplication.getContext().getString(R.string.ncd_reports_folder));

        String prefix = dataManager.getCampInfo().getPartnerId();

        Calendar calendar = Calendar.getInstance();

        String fileName = LApplication.getContext().getString(R.string.prefix_export_backup) + "_"
                + "Missing_Demographics_"
                + dataManager.getPartnerId() + "_"
                + dataManager.getDeviceNumber() + "_"
                + LUtils.getFileName(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DATE),
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                calendar.get(Calendar.SECOND))
                + "_" + prefix;

        int i = 1;

        File outputFile = new File(outputPath.getPath(), fileName + ".pdf");

        while (outputFile.exists()) {
            outputFile = new File(outputPath.getPath(), fileName + "(" + i++ + ").pdf");
        }

        return outputFile.getPath();
    }

    private static String getFileName() {
        Calendar calendar = Calendar.getInstance();

        String fileName = "";

        if (10 > calendar.get(Calendar.DATE)) {
            fileName += "0" + calendar.get(Calendar.DATE);
        } else {
            fileName += calendar.get(Calendar.DATE);
        }

        fileName += "_";

        if (9 > calendar.get(Calendar.MONTH)) {
            fileName += "0" + (calendar.get(Calendar.MONTH) + 1);
        } else {
            fileName += (calendar.get(Calendar.MONTH) + 1);
        }

        fileName += "_" + calendar.get(Calendar.YEAR) + "_";

        if (10 > calendar.get(Calendar.HOUR_OF_DAY)) {
            fileName += "0" + calendar.get(Calendar.HOUR_OF_DAY);
        } else {
            fileName += calendar.get(Calendar.HOUR_OF_DAY);
        }

        fileName += "-";

        if (10 > calendar.get(Calendar.MINUTE)) {
            fileName += "0" + calendar.get(Calendar.MINUTE);
        } else {
            fileName += calendar.get(Calendar.MINUTE);
        }

        return fileName;
    }

    private static void writeHeaders(CSVWriter writer) {
        ArrayList<String> headers = new ArrayList<>(16);
        headers.add("Reg.ID");
        headers.add("Name");
        headers.add("Vital");
        headers.add("Value");
        headers.add("Unit");
        headers.add("Date Time");

        String[] data = new String[headers.size()];
        headers.toArray(data);
        writer.writeNext(data);
    }

    private static void writeData(CSVWriter writer, DataManager dataManager) {

        List<TableVitals> tableVitals = dataManager.getAllVitals();

        for (TableVitals tableVital : tableVitals) {
            if (tableVital.getVitalType().equals("Prescription")) {
                continue;
            }

            ArrayList<String> headers = new ArrayList<>(8);
            String            name    = "";

            try {
                name = dataManager.getPatient(tableVital.getRegistrationId()).getFullName();
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }

            switch (tableVital.getVitalType()) {
                case "Blood Pressure": {
                    String[] vitalSubtypes = tableVital.getVitalSubTypes().split(",");
                    String[] vitalValues   = tableVital.getVitalValues().split(";");
                    String[] vitalUnits    = tableVital.getVitalUnits().split(";");

                    for (int i = 0; i < 3; ++i) {
                        headers = new ArrayList<>(8);

                        headers.add(tableVital.getRegistrationId());
                        headers.add(name);
                        headers.add(vitalSubtypes[i]);
                        headers.add(vitalValues[i]);
                        headers.add(vitalUnits[i]);
                        headers.add(tableVital.getVitalDateTime());

                        printHeaders(writer, headers);
                    }
                    break;
                }

                default: {
                    headers.add(tableVital.getRegistrationId());
                    headers.add(name);
                    headers.add(tableVital.getVitalSubTypes());
                    headers.add(tableVital.getVitalValues());
                    headers.add(tableVital.getVitalUnits());
                    headers.add(tableVital.getVitalDateTime());

                    printHeaders(writer, headers);
                    break;
                }
            }
        }
    }

    private static void printHeaders(CSVWriter writer, ArrayList<String> headers) {
        String[] data = new String[headers.size()];
        headers.toArray(data);
        writer.writeNext(data);
    }

    public static int getColor(int color) {
        return ContextCompat.getColor(LApplication.getContext(), color);
    }

    public static boolean toBoolean(int value) {
        return value > 0;
    }

    public static String roundOff(String value) {

        try {
            Double valueDouble = Double.valueOf(value);

            DecimalFormat format = new DecimalFormat("0.00");

            double returnValue = Double.parseDouble(format.format(valueDouble));

            if (returnValue == (long) returnValue) {
                return String.format("%d", (long) returnValue);
            } else {
                return String.format("%s", returnValue);
            }
//            return String.format("%.2f", Float.valueOf(value));
        } catch (Exception e) {
            return "0";
        }
    }

    public static String formatFloat(float value) {
        if (value == (long) value)
            return String.format("%d", (long) value);
        else
            return String.format("%s", value);
    }

    public static int toInt(String value) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
            return 0;
        }
    }

    public static double toDouble(String value) {
        try {
            return Double.valueOf(value);
        } catch (Exception e) {
            return 0.0;
        }
    }

    public static String readFromFile(String fileName) {
        File file = new File(fileName);

        BufferedReader reader;

        StringBuilder sb = new StringBuilder();

        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String mLine = reader.readLine();

            while (mLine != null) {
                sb.append(mLine);
                mLine = reader.readLine();
            }

            reader.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }

        return sb.toString();
    }

    public static ItemPrescriptionDetails getPrescriptionDetails(String prescriptionDetails) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(prescriptionDetails, ItemPrescriptionDetails.class);

        } catch (Exception e) {
        }

        return new ItemPrescriptionDetails();
    }

    public static String getDaysText(int days) {
        if (days <= 1) {
            return days + " day";
        } else {
            return days + " days";
        }
    }

    public static boolean toConnectDevice(String deviceName, String macAddress, ArrayList<ItemBluetoothDevice> itemBluetoothDevices) {

        for (ItemBluetoothDevice item : itemBluetoothDevices) {

            if (item.getDeviceName().equals(deviceName)) {
                // Compare mac addresses

                if (item.getMacAddress().isEmpty()) {
                    return true;
                } else {
                    String[] macAddresses = item.getMacAddress().split(",");

                    for (String mac : macAddresses) {
                        if (macAddress.equals(mac.trim())) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean toConnectDevicePrinter(String deviceName, String macAddress, ArrayList<ItemBluetoothDevice> itemBluetoothDevices) {

        for (ItemBluetoothDevice item : itemBluetoothDevices) {

            boolean deviceFound = false;

            if (item.getDeviceName().equals(deviceName)) {
                deviceFound = true;
            } else {
                try {
                    if (item.getDeviceName().startsWith("CIE-DYNO") && deviceName.contains("CIE-DYNO")) {
                        return true;
                    }
                } catch (Exception e) {
                }
            }

            if (deviceFound) {
                // Compare mac addresses

                if (item.getMacAddress().isEmpty()) {
                    return true;
                } else {
                    String[] macAddresses = item.getMacAddress().split(",");

                    for (String mac : macAddresses) {
                        if (macAddress.equals(mac.trim())) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public static boolean isMacAddressEmpty(ArrayList<ItemBluetoothDevice> itemBluetoothDevices) {
        boolean isEmpty = false;

        for (ItemBluetoothDevice item : itemBluetoothDevices) {
            if (item.getMacAddress().isEmpty()) {
                isEmpty = true;
            }
        }

        return isEmpty;
    }

    public static String getFileExtension(String fileName) {
        try {
            return fileName.substring(fileName.lastIndexOf("."));

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static String getFileNameWithoutExtension(String fileName) {
        try {
            String parts[] = fileName.split("\\.");

            StringBuilder name = new StringBuilder();

            for (int i = 0; i < parts.length - 1; i++) {
                name.append(parts[i]);
            }

            return name.toString();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return fileName;
    }

    public static String getFileName(Uri uri) {
        Cursor cursor = LApplication.getContext()
                .getContentResolver()
                .query(uri, null, null, null, null);

        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        cursor.moveToFirst();

        return cursor.getString(nameIndex);
    }

    public static File getLocalFilesPath() {
        File filesPath = new File(Environment.getExternalStorageDirectory() + LApplication.getContext().getString(R.string.docs_folder));

        if (!filesPath.exists()) {
            filesPath.mkdirs();
        }

        return filesPath;
    }

    public static void copyFile(String fromPath, String toPath) {
        try {
            File dir = getLocalFilesPath();

            InputStream  in  = new FileInputStream(fromPath);
            OutputStream out = new FileOutputStream(toPath);

            byte[] buffer = new byte[1024];

            int read;

            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }

            in.close();
            out.flush();
            out.close();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static boolean hasItem(String[] array, String value) {
        for (String item : array) {
            if (item.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public static void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                byte[] buf = new byte[1024];
                int    len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
}