package com.dhp.screening.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.media.ExifInterface;

import com.dhp.screening.LApplication;
import com.dhp.screening.data.item.ItemFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Calendar;

import static com.dhp.screening.util.LConstants.REQUEST_CAPTURE;
import static com.dhp.screening.util.LConstants.REQUEST_GALLERY;


public class TaskCompressImage extends AsyncTask<Void, Void, Void> {

    private final int requestCode;

    private boolean isError;

    private final Callback callback;

    private Uri    imageUri;
    private String imagePath;

    private ItemFile itemCompressedImage;

    private static float MAX_HEIGHT = 1440.0f;
    private static float MAX_WIDTH  = 1440.0f;

    public interface Callback {

        void onImageCompressed(ItemFile itemCompressedImage);

        void onCompressError();
    }

    public TaskCompressImage(Uri imageUri, int requestCode, Callback callback) {
        this.requestCode = requestCode;
        this.imageUri = imageUri;
        this.callback = callback;
    }

    public TaskCompressImage(String imagePath, int requestCode, Callback callback, boolean toCompress) {
        this.requestCode = requestCode;
        this.imagePath = imagePath;
        this.callback = callback;

        if (toCompress) {
            MAX_HEIGHT = 5440.0f;
            MAX_WIDTH = 5440.0f;
        }
    }

    @Override
    protected void onPostExecute(Void result) {
        if (isError) {
            callback.onCompressError();
        } else {
            callback.onImageCompressed(itemCompressedImage);
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        itemCompressedImage = new ItemFile();

        byte[] imageData = new byte[1024];

        File filesPath = LUtils.getLocalFilesPath();

        if (REQUEST_GALLERY == requestCode) {

            try {
                InputStream in = LApplication.getContext().getContentResolver().openInputStream(imageUri);

                File tempFile = new File(filesPath.getPath() + "/" + "tmpfile_" + Calendar.getInstance().getTimeInMillis() + ".jpeg");

                OutputStream out = new FileOutputStream(tempFile);

                int bytesRead;

                while ((bytesRead = in.read(imageData)) > 0) {
                    out.write(Arrays.copyOfRange(imageData, 0, Math.max(0, bytesRead)));
                }

//                String outputFilePath = filesPath.getPath() + "/" + fileName;
//                tempFile.renameTo(new File(outputFilePath));

                itemCompressedImage.setName(tempFile.getName());
                itemCompressedImage.setLocalPath(tempFile.getPath());

            } catch (Exception e) {
                LLog.printStackTrace(e);
                isError = true;
            }

        } else if (REQUEST_CAPTURE == requestCode) {

            String fileName = "tmpfile_" + Calendar.getInstance().getTimeInMillis() + ".jpeg";

            String outputFilePath = filesPath.getPath() + "/" + fileName;

            compressImage(imagePath, outputFilePath);
            new File(imagePath).delete();

            itemCompressedImage.setName(fileName);
            itemCompressedImage.setLocalPath(outputFilePath);
            itemCompressedImage.setCamera(true);
        }

        return null;
    }


    private void compressImage(String imagePath, String outPath) {
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int   actualHeight = options.outHeight;
        int   actualWidth  = options.outWidth;
        float imgRatio     = (float) actualWidth / (float) actualHeight;
        float maxRatio     = MAX_WIDTH / MAX_HEIGHT;

        if (actualHeight > MAX_HEIGHT || actualWidth > MAX_WIDTH) {
            if (imgRatio < maxRatio) {
                imgRatio = MAX_HEIGHT / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) MAX_HEIGHT;

            } else if (imgRatio > maxRatio) {
                imgRatio = MAX_WIDTH / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) MAX_WIDTH;

            } else {
                actualHeight = (int) MAX_HEIGHT;
                actualWidth = (int) MAX_WIDTH;
            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX  = actualWidth / (float) options.outWidth;
        float ratioY  = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;

        try {
            exif = new ExifInterface(imagePath);
            int    orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix      = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        } catch (IOException e) {
            isError = true;
        }

        try {
            File             comp = new File(outPath);
            FileOutputStream fOut = new FileOutputStream(comp.getPath());
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();

        } catch (IOException e) {
            isError = true;
        }
    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width  = options.outWidth;

        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio  = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        final float totalPixels       = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;

        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}