package com.dhp.screening.util;

import android.widget.Toast;

import com.dhp.screening.LApplication;

import es.dmoral.toasty.Toasty;


public class LToast {

    public static void toast(int stringId) {
        Toast.makeText(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_SHORT).show();
    }

    public static void toast(String message) {
        Toast.makeText(LApplication.getContext(), message, Toast.LENGTH_LONG).show();
    }

    public static void normal(String message) {
        Toasty.normal(LApplication.getContext(), message,
                Toast.LENGTH_LONG).show();
    }

    public static void info(int stringId) {
        Toasty.info(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_SHORT).show();
    }

    public static void success(int stringId) {
        Toasty.success(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_SHORT).show();
    }

    public static void success(String message) {
        Toasty.success(LApplication.getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public static void warning(int stringId) {
        Toasty.warning(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_SHORT).show();
    }

    public static void warning(String message) {
        Toasty.warning(LApplication.getContext(), message,
                Toast.LENGTH_SHORT).show();
    }

    public static void warningLong(String message) {
        Toasty.warning(LApplication.getContext(), message,
                Toast.LENGTH_LONG).show();
    }

    public static void warningLong(int stringId) {
        Toasty.warning(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_LONG).show();
    }

    public static void error(int stringId) {
        Toasty.error(LApplication.getContext(), LApplication.getContext().getString(stringId),
                Toast.LENGTH_LONG).show();
    }

    public static void error(String message) {
        Toasty.error(LApplication.getContext(), message,
                Toast.LENGTH_LONG).show();
    }

    public static void errorLong(String message) {
        Toasty.error(LApplication.getContext(), message,
                Toast.LENGTH_LONG).show();
    }

    public static void errorLong(int stringId) {
        errorLong(LApplication.getContext().getString(stringId));
    }
}