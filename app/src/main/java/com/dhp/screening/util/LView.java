package com.dhp.screening.util;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.adapter.AdapterSpinner;
import com.dhp.screening.data.asset.AssetReader;

public class LView {

    public static void setSpinnerAdapter(Context context, Spinner spinner, String[] list) {
        final AdapterSpinner<String> adapterSpinner = new AdapterSpinner<>(
                context,
                R.layout.spinner_layout,
                new ArrayList<>(Arrays.asList(list))
        );
        adapterSpinner.setDropDownViewResource(R.layout.spinner_layout);
        spinner.setAdapter(adapterSpinner);
    }

    public static String setTextFromXml(EditText editText, String key) {
        String value = "";

        try {
            value = StaticData.getAadhaarAttributes().getNamedItem(key).getTextContent();
            editText.setText(value);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return value;
    }

    public static String setTextFromXml(EditText editText, String key1, String key2) {
        String value = "";

        try {
            value = StaticData.getAadhaarAttributes().getNamedItem(key1).getTextContent()
                    + ", "
                    + StaticData.getAadhaarAttributes().getNamedItem(key2).getTextContent();
            editText.setText(value);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return value;
    }

    public static String setTextFromXml(TextView textView, String key) {
        String value = "";

        try {
            value = StaticData.getAadhaarAttributes().getNamedItem(key).getTextContent();
            textView.setText(value);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return value;
    }

    public static void setIcon(ImageView imageView, String key) {

        String fileName = AssetReader.getIconFileName(key);

        File imageFile = new File(Environment.getExternalStorageDirectory(),
                LApplication.getContext().getString(R.string.icons_folder) + fileName);

        imageView.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
    }

    public static void setImageFromFile(ImageView imageView, String imgName) {
        File imageFile = new File(Environment.getExternalStorageDirectory(),
                LApplication.getContext().getString(R.string.icons_folder) + imgName);

        imageView.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
    }

    public static void resetRangeColor(EditText editText) {
        editText.setBackground(LUtils.getDrawable(R.drawable.rounded_border));
        editText.setTextColor(LUtils.getColor(R.color.default_text_color));
    }
}