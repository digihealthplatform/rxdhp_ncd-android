package com.dhp.screening.lib.bluetooth;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.dhp.screening.data.event.EventConnectionStateChanged;
import com.dhp.screening.data.event.EventWeightRead;
import com.dhp.screening.util.LLog;
import com.lifesense.ble.LsBleManager;
import com.lifesense.ble.PairCallback;
import com.lifesense.ble.ReceiveDataCallback;
import com.lifesense.ble.SearchCallback;
import com.lifesense.ble.bean.BloodPressureData;
import com.lifesense.ble.bean.HeightData;
import com.lifesense.ble.bean.KitchenScaleData;
import com.lifesense.ble.bean.LsDeviceInfo;
import com.lifesense.ble.bean.PedometerData;
import com.lifesense.ble.bean.WeightData_A2;
import com.lifesense.ble.bean.WeightData_A3;
import com.lifesense.ble.bean.WeightUserInfo;
import com.lifesense.ble.commom.BroadcastType;
import com.lifesense.ble.commom.DeviceType;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_CONNECTING;
import static com.dhp.screening.lib.bluetooth.BleConstants.STATE_DISCONNECTED;


public class MedcheckWeightConnection {

    private static final String TAG = MedcheckWeightConnection.class.getSimpleName();

    private static MedcheckWeightConnection instance;

    public static MedcheckWeightConnection getInstance() {
        if (null == instance) {
            instance = new MedcheckWeightConnection();
        }
        return instance;
    }

    public static void clearInstance() {
        instance = null;
    }

    private int connectionState = STATE_DISCONNECTED;
    private boolean isInitiated;

    private LsBleManager lsBleManager;
    public  LsDeviceInfo lsConnectedDevice;

    private ReceiveDataCallback dataReceivedCallback = new ReceiveDataCallback() {

        @Override
        public void onReceiveWeightDta_A2(WeightData_A2 wData_A2) {
            if (wData_A2 != null) {
                LLog.d(TAG, "onReceiveWeightDta_A2: ");
            }
        }

        @Override
        public void onReceiveWeightData_A3(WeightData_A3 wData) {
            if (wData != null) {
                EventBus.getDefault().post(new EventConnectionStateChanged(STATE_CONNECTED));

                EventWeightRead event = new EventWeightRead();
                event.setWeight("" + wData.getWeight());

                // TODO: 08-Mar-19 , Save other info
//                event.setWeight("" + wData.getBasalMetabolism());
//                event.setWeight("" + wData.getBodyFatRatio());
//                event.setWeight("" + wData.getBodyWaterRatio());
//                event.setWeight("" + wData.getMuscleMassRatio());
//                event.setWeight("" + wData.getBoneDensity());
//                event.setWeight("" + wData.getVisceralFatLevel());

                EventBus.getDefault().post(event);
            }
        }

        @Override
        public void onReceiveBloodPressureData(BloodPressureData bpData) {
            LLog.d(TAG, "onReceiveBloodPressureData: ");
        }

        @Override
        public void onReceivePedometerData(final PedometerData pData) {
            LLog.d(TAG, "onReceivePedometerData: ");
        }

        @Override
        public void onReceiveHeightData(HeightData hData) {
            LLog.d(TAG, "onReceiveHeightData: ");
        }

        @Override
        public void onReceiveUserInfo(WeightUserInfo proUserInfo) {
            LLog.d(TAG, "onReceiveUserInfo: ");
        }

        @Override
        public void onReceiveKitchenScaleData(KitchenScaleData kiScaleData) {
            super.onReceiveKitchenScaleData(kiScaleData);
        }

        @Override
        public void onReceiveDeviceInfo(LsDeviceInfo device) {
            LLog.d(TAG, "onReceiveDeviceInfo: ");
        }
    };

    private PairCallback pairCallback = new PairCallback() {

        @Override
        public void onDiscoverUserInfo(List userList) {
            if (userList != null) {
//                List<DeviceUserInfo> mDeviceUserList = (List<DeviceUserInfo>) userList;

                lsBleManager.bindDeviceUser(1, "NCD User");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        lsBleManager.addMeasureDevice(lsConnectedDevice);
                        lsBleManager.startDataReceiveService(dataReceivedCallback);
                    }
                }, 2000);
                LLog.d(TAG, "onDiscoverUserInfo: ");
            }
        }

        @Override
        public void onPairResults(final LsDeviceInfo lsDevice, final int status) {
            if (0 == status) {
                connectionState = STATE_CONNECTED;
                EventBus.getDefault().post(new EventConnectionStateChanged(STATE_CONNECTED));
            } else {
                connectionState = STATE_DISCONNECTED;
                EventBus.getDefault().post(new EventConnectionStateChanged(STATE_DISCONNECTED));
            }
        }
    };

    private SearchCallback scanCallback = new SearchCallback() {

        @Override
        public void onSearchResults(final LsDeviceInfo lsDevice) {
            if (lsDevice != null) {
                if (STATE_DISCONNECTED == connectionState) {
                    connectionState = STATE_CONNECTING;
                    lsBleManager.stopSearch();
                    MedcheckWeightConnection.this.lsConnectedDevice = lsDevice;
                    lsBleManager.startPairing(lsDevice, pairCallback);
                }
            }
        }
    };

    public void connect(Context context) {
        if (!isInitiated) {
            isInitiated = true;
            lsBleManager = LsBleManager.newInstance();
            lsBleManager.initialize(context);
            lsBleManager.searchLsDevice(scanCallback, getDeviceTypes(), BroadcastType.ALL);
        }
    }

    public int getConnectionStatus() {
        return connectionState;
    }

    private List<DeviceType> getDeviceTypes() {
        ArrayList<DeviceType> scanDeviceTypes = new ArrayList<>();

        scanDeviceTypes = new ArrayList<DeviceType>();
        scanDeviceTypes.add(DeviceType.SPHYGMOMANOMETER);
        scanDeviceTypes.add(DeviceType.FAT_SCALE);
        scanDeviceTypes.add(DeviceType.WEIGHT_SCALE);
        scanDeviceTypes.add(DeviceType.HEIGHT_RULER);
        scanDeviceTypes.add(DeviceType.PEDOMETER);
        scanDeviceTypes.add(DeviceType.KITCHEN_SCALE);

        return scanDeviceTypes;
    }
}