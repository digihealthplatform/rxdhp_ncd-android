package com.dhp.screening.lib.bluetooth;

import java.util.UUID;

public class BleConstants {

    public static final int BATTERY_LEVEL    = 10;
    public static final int SYNC_TEST_RESULT = 11;
    public static final int SWAN_TEST_RESULT = 12;

    public static final int BP_TEST_STARTED     = 12;
    public static final int BP_TEST_IN_PROGRESS = 13;
    public static final int BP_WAIT             = 14;
    public static final int BP_START_TEST_NOW   = 15;
    public static final int BP_TEST_ERROR       = 16;
    public static final int BP_FINAL_RESULT     = 17;

    public static final int ASHA_TEST_RESULT        = 20;
    public static final int ASHA_START_TEST_BP      = 30;
    public static final int ASHA_START_TEST_SPO2    = 31;
    public static final int ASHA_START_TEST_GLUCOSE = 32;

    public static final int BERRY_MED_DEVICE_TEST_RESULT = 16;

    public static final String SYNC_DEVICE                    = "Sync";
    public static final String SYNCPLUS_DEVICE                = "SyncPlus";
    public static final String SYNC_BATTERY_LEVEL_SERVICE     = "0000180f-0000-1000-8000-00805f9b34fb";
    public static final String SYNC_LEVEL_CHARACTERISTIC      = "00002a19-0000-1000-8000-00805f9b34fb";
    public static final String SYNC_START_TEST_SERVICE        = "0003abcd-0000-1000-8000-00805f9b0131";
    public static final String SYNC_START_TEST_CHARACTERISTIC = "00031234-0000-1000-8000-00805f9b0131";

    public static final String TECHNAXX_BP_DEVICE                       = "Technaxx BP";
    public static final String TECHNAXX_BP_BATTERY_LEVEL_SERVICE        = "000018f0-0000-1000-8000-00805f9b34fb";
    public static final String TECHNAXX_BP_BATTERY_LEVEL_CHARACTERISTIC = "00002af0-0000-1000-8000-00805f9b34fb";
    public static final UUID   TECHNAXX_BP_BATTERY_LEVEL_UUID           = UUID.fromString(TECHNAXX_BP_BATTERY_LEVEL_CHARACTERISTIC);
    public static final String TECHNAXX_BP_START_TEST_SERVICE           = "000018f0-0000-1000-8000-00805f9b34fb";
    public static final String TECHNAXX_BP_START_TEST_CHARACTERISTIC    = "00002af1-0000-1000-8000-00805f9b34fb";

    public static final String SFBP            = "SFBPBLE";
    public static final String MEDCHECK_WEIGHT = "1SFBS1";

    public static final String CHIPSEA_DEVICE                    = "Chipsea-BLE";
    public static final String CHIPSEA_START_TEST_SERVICE        = "0000fff0-0000-1000-8000-00805f9b34fb";
    public static final String CHIPSEA_START_TEST_CHARACTERISTIC = "0000fff1-0000-1000-8000-00805f9b34fb";

    public static final String SWAN_DEVICE                    = "SWAN";
    public static final String SWAN_START_TEST_SERVICE        = "0000ffb0-0000-1000-8000-00805f9b34fb";
    public static final String SWAN_START_TEST_CHARACTERISTIC = "0000ffb1-0000-1000-8000-00805f9b34fb";

    public static final String ASHA_PLUS = "Dual-SPP";
    public static final UUID   ASHA_UUID = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");

    public static final String BERRY_MED_DEVICE       = "BerryMed";
    public static final String UUID_SERVICE_DATA      = "49535343-fe7d-4ae5-8fa9-9fafd205e455";
    public static final String UUID_CHARACTER_RECEIVE = "49535343-1e4d-4bd9-ba61-23c647249616";

    public static final String UUID_SERVICE_DATA_SFBP       = "00002902-0000-1000-8000-00805f9b34fb";
    public static final String UUID_CHARACTER_RECEIVE_SF_BP = "0000fff4-0000-1000-8000-00805f9b34fb";

    public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    public static final String ADD_BLOOD          = "2000";
    public static final String ERROR_USED_STRIP   = "2001";
    public static final String COUNT_DOWN_STARTED = "3000";

    public static final int STATE_CONNECTING   = 1;
    public static final int STATE_CONNECTED    = 2;
    public static final int STATE_DISCONNECTED = 3;
}