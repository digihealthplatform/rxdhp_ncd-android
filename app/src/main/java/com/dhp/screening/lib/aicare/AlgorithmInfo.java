package com.dhp.screening.lib.aicare;

import android.os.Parcel;
import android.os.Parcelable;

public class AlgorithmInfo implements Parcelable {
    public static final Creator<AlgorithmInfo> CREATOR = new Creator<AlgorithmInfo>() {
        public AlgorithmInfo createFromParcel(Parcel parcel) {
            return new AlgorithmInfo(parcel);
        }

        public AlgorithmInfo[] newArray(int i) {
            return new AlgorithmInfo[i];
        }
    };
    private             int                    adc;
    private             int                    algorithmId;
    private             DecInfo                decInfo;
    private             float                  weight;

    public int describeContents() {
        return 0;
    }

    public AlgorithmInfo(float f, int i, int i2, DecInfo decInfo2) {
        this.weight = f;
        this.algorithmId = i;
        this.adc = i2;
        this.decInfo = decInfo2;
    }

    public AlgorithmInfo() {
    }

    protected AlgorithmInfo(Parcel parcel) {
        this.weight = parcel.readFloat();
        this.algorithmId = parcel.readInt();
        this.adc = parcel.readInt();
        this.decInfo = (DecInfo) parcel.readParcelable(DecInfo.class.getClassLoader());
    }

    public int getAlgorithmId() {
        return this.algorithmId;
    }

    public void setAlgorithmId(int i) {
        this.algorithmId = i;
    }

    public int getAdc() {
        return this.adc;
    }

    public void setAdc(int i) {
        this.adc = i;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(float f) {
        this.weight = f;
    }

    public DecInfo getDecInfo() {
        return this.decInfo;
    }

    public void setDecInfo(DecInfo decInfo2) {
        this.decInfo = decInfo2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("AlgorithmInfo{weight=");
        sb.append(this.weight);
        sb.append(", algorithmId(算法序列)=");
        sb.append(this.algorithmId);
        sb.append(", adc(阻抗值)=");
        sb.append(this.adc);
        sb.append(", decInfo=");
        sb.append(this.decInfo);
        sb.append('}');
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.weight);
        parcel.writeInt(this.algorithmId);
        parcel.writeInt(this.adc);
        parcel.writeParcelable(this.decInfo, i);
    }
}
