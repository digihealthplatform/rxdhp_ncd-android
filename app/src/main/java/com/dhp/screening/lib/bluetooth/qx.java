// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.dhp.screening.lib.bluetooth;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class qx {

    private static final char a[] = "0123456789ABCDEF".toCharArray();

    public static String a(byte byte0) {
        return String.format("%8s", new Object[]{
                Integer.toBinaryString(byte0 & 0xff)
        }).replace(' ', '0');
    }


    public static String a(byte abyte0[]) {
        char ac[] = new char[abyte0.length * 2];
        for (int i = 0; i < abyte0.length; i++) {
            int j = abyte0[i] & 0xff;
            ac[i * 2] = a[j >>> 4];
            ac[i * 2 + 1] = a[j & 0xf];
        }

        return new String(ac);
    }

    public static ArrayList a(String s, int i) {
        if (i > s.length()) {
            return null;
        }
        ArrayList arraylist = new ArrayList();
        for (int j = 0; j < s.length() - 1; j += i) {
            arraylist.add(s.substring(j, Math.min(i, Math.abs(s.length() - j)) + j));
        }

        return arraylist;
    }

    public static String b(byte abyte0[]) {
        StringBuilder stringbuilder = new StringBuilder();
        int           j             = abyte0.length;
        for (int i = 0; i < j; i++) {
            stringbuilder.append(a(abyte0[i]));
        }

        return stringbuilder.toString();
    }

    public static boolean b(String s) {
        if (!TextUtils.isEmpty(s)) {
            return Pattern.compile("^(([0-9]*)|(([0-9]*)))$").matcher(s).matches();
        } else {
            return true;
        }
    }

}
