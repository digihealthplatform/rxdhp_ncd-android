package com.dhp.screening.lib.bluetooth;// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 


// Referenced classes of package com.getmedcheck.api.response.a:
//            d

import java.text.SimpleDateFormat;
import java.util.Locale;

public class gx {

    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    public gx() {
        a = "0";
        b = "0";
        c = "0";
        d = "0";
        e = "0";
        f = "0";
    }

    public long a() {
        return a("dd-MMM-yyyy hh:mm a", f);
    }

    public static long a(String s, String s1) {
        SimpleDateFormat sd = new SimpleDateFormat(s, Locale.ENGLISH);
        long             l;
        try {
            l = sd.parse(s1).getTime();
        }
        // Misplaced declaration of an exception variable
        catch (Exception e) {
            e.printStackTrace();
            return -1L;
        }
        return l;
    }

    public void a(String s) {
        b = s;
    }

    public String b() {
        return b;
    }

    public void b(String s) {
        c = s;
    }

    public String c() {
        return c;
    }

    public void c(String s) {
        d = s;
    }

    public String d() {
        return d;
    }

    public void d(String s) {
        e = s;
    }

    public String e() {
        return e;
    }

    public String f() {
        return f;
    }

    public String g() {
        return a;
    }

    public String toString() {
        return (new StringBuilder()).append("DeviceDataBpmResponse{sys='").append(b).append('\'').append(", dia='").append(c).append('\'').append(", pulse='").append(d).append('\'').append(", ihbIndicator='").append(e).append('\'').append(", readingTime='").append(f).append('\'').append('}').toString();
    }
}
