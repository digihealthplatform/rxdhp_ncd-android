package com.dhp.screening.lib.aicare;

import android.support.v4.view.InputDeviceCompat;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class ParseData {
    public static float fgToKG(float f) {
        return f / 2.0f;
    }

    public static float gToKG(float f) {
        return f / 1000.0f;
    }

    public static float getPercent(float f) {
        if (f > 100.0f) {
            return 0.0f;
        }
        return f;
    }

    public static float lbToOz(float f) {
        return f * 16.0f;
    }

    public static float ozToG(float f) {
        return f * 28.3495f;
    }

    public static float stToLb(float f) {
        return f * 14.0f;
    }

    public static String binaryToHex(byte b) {
        String        hexString = Integer.toHexString(Integer.parseInt(byteToBit(b), 2));
        StringBuilder sb        = new StringBuilder();
        if (hexString.length() == 1) {
            sb.append("0");
        }
        sb.append(hexString);
        return sb.toString().toUpperCase();
    }

    public static int binaryToDecimal(byte b) {
        return Integer.parseInt(byteToBit(b), 2);
    }

    public static String byteToBit(byte b) {
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append((byte) ((b >> 7) & 1));
        sb.append((byte) ((b >> 6) & 1));
        sb.append((byte) ((b >> 5) & 1));
        sb.append((byte) ((b >> 4) & 1));
        sb.append((byte) ((b >> 3) & 1));
        sb.append((byte) ((b >> 2) & 1));
        sb.append((byte) ((b >> 1) & 1));
        sb.append((byte) (b & 1));
        return sb.toString();
    }

    public static byte[] int2byte(int i) {
        byte[] bArr = new byte[2];
        bArr[1] = (byte) (i & 255);
        bArr[0] = (byte) ((i >> 8) & 255);
        return bArr;
    }

    public static byte bitToByte(String str) {
        int i;
        if (str == null) {
            return 0;
        }
        int length = str.length();
        if (length != 4 && length != 8) {
            return 0;
        }
        if (length != 8) {
            i = Integer.parseInt(str, 2);
        } else if (str.charAt(0) == '0') {
            i = Integer.parseInt(str, 2);
        } else {
            i = Integer.parseInt(str, 2) + InputDeviceCompat.SOURCE_ANY;
        }
        return (byte) i;
    }

    public static int getData(int i, int i2, int i3, byte[] bArr) {
        byte          b  = (byte) (((bArr[i] & AicareBleConfig.SYNC_HISTORY) << 16) | ((bArr[i2] & AicareBleConfig.SYNC_HISTORY) << 8) | (bArr[i3] & AicareBleConfig.SYNC_HISTORY));
        StringBuilder sb = new StringBuilder();
        sb.append("data = ");
        sb.append(b);
        return b;
    }

    public static int getDataInt(int i, int i2, byte[] bArr) {
        int           i3 = ((bArr[i] & AicareBleConfig.SYNC_HISTORY) << 8) + (bArr[i2] & AicareBleConfig.SYNC_HISTORY);
        StringBuilder sb = new StringBuilder();
        sb.append("getDataInt = ");
        sb.append(i3);
        return i3;
    }

    public static int getDataInt(byte b, byte b2) {
        int           i  = ((b & AicareBleConfig.SYNC_HISTORY) << 8) + (b2 & AicareBleConfig.SYNC_HISTORY);
        StringBuilder sb = new StringBuilder();
        sb.append("getDataInt = ");
        sb.append(i);
        return i;
    }

    public static String getTime() {
        return new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date(System.currentTimeMillis()));
    }

    public static String getDate() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date(System.currentTimeMillis()));
    }

    public static String getCurrentTime() {
        return new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date(System.currentTimeMillis()));
    }

    public static String getTime(long j) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US).format(new Date(j));
    }

    public static byte[] contact(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null) {
            return null;
        }
        byte[] copyOf = Arrays.copyOf(bArr, bArr.length + bArr2.length);
        System.arraycopy(bArr2, 0, copyOf, bArr.length, bArr2.length);
        return copyOf;
    }

    public static String addZero(String str) {
        StringBuilder sb = new StringBuilder();
        if (str.length() != 1) {
            return str;
        }
        sb.append("0");
        sb.append(str);
        return sb.toString();
    }

    public static String byteArr2Str(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < bArr.length; i++) {
            sb.append("0x");
            sb.append(addZero(binaryToHex(bArr[i])));
            if (i < bArr.length - 1) {
                sb.append(", ");
            }
        }
        sb.append("]");
        return sb.toString();
    }

    public static String keepDecimal(double d, int i) {
        Locale        locale = Locale.US;
        StringBuilder sb     = new StringBuilder();
        sb.append("%.");
        sb.append(i);
        sb.append("f");
        return String.format(locale, sb.toString(), new Object[]{Double.valueOf(d)});
    }

    public static String getLbWeight(double d, DecInfo decInfo) {
        long   pow  = (long) Math.pow(10.0d, (double) decInfo.getLbDecimal());
        double d2   = (d * 22046.0d) / 10000.0d;
        double pow2 = (double) ((long) Math.pow(10.0d, (double) decInfo.getSourceDecimal()));
        Double.isNaN(pow2);
        double d3 = d2 / pow2;
        double d4 = (double) pow;
        Double.isNaN(d4);
        long j            = (long) ((d3 * d4) + 0.5d);
        int  lbGraduation = decInfo.getLbGraduation();
        if (lbGraduation != 0) {
            long j2 = (long) lbGraduation;
            j = (j / j2) * j2;
        }
        return String.valueOf((((float) j) * 1.0f) / ((float) pow));
    }

    static String getStWeight(double d, DecInfo decInfo) {
        long   pow  = (long) Math.pow(10.0d, (double) decInfo.getStDecimal());
        double d2   = (d * 22046.0d) / 10000.0d;
        double pow2 = (double) ((long) Math.pow(10.0d, (double) decInfo.getSourceDecimal()));
        Double.isNaN(pow2);
        double d3 = d2 / pow2;
        double d4 = (double) pow;
        Double.isNaN(d4);
        long j            = (long) ((d3 * d4) + 0.5d);
        int  lbGraduation = decInfo.getLbGraduation();
        if (lbGraduation != 0) {
            long j2 = (long) lbGraduation;
            j = (j / j2) * j2;
        }
        float         f     = (float) pow;
        float         f2    = (((float) j) * 1.0f) / f;
        long          j3    = ((long) f2) / 14;
        float         round = (((float) Math.round((f2 - ((float) (14 * j3))) * f)) * 1.0f) / f;
        StringBuilder sb    = new StringBuilder();
        sb.append(String.valueOf(j3));
        sb.append(":");
        sb.append(round);
        return sb.toString();
    }

    public static String getKgWeight(double d, DecInfo decInfo) {
        long   pow  = (long) Math.pow(10.0d, (double) decInfo.getKgDecimal());
        double pow2 = (double) ((long) Math.pow(10.0d, (double) decInfo.getSourceDecimal()));
        Double.isNaN(pow2);
        double d2 = d / pow2;
        double d3 = (double) pow;
        Double.isNaN(d3);
        long j = (long) ((d2 * d3) + 0.5d);
        if (decInfo.getKgGraduation() != 0) {
            j = (j / ((long) decInfo.getKgGraduation())) * ((long) decInfo.getKgGraduation());
        }
        return String.valueOf((((float) j) * 1.0f) / ((float) pow));
    }

    static String getJinWeight(double d, DecInfo decInfo) {
        return keepDecimal((d / Math.pow(10.0d, (double) decInfo.getSourceDecimal())) * 2.0d, decInfo.getKgDecimal());
    }

    public static String int2HexStr(int i) {
        return binaryToHex(Integer.valueOf(i).byteValue());
    }

    public static byte[] address2byte(String str) {
        String[] split = str.split(":");
        byte[]   bArr  = new byte[split.length];
        for (int i = 0; i < split.length; i++) {
            bArr[i] = Integer.valueOf(split[i], 16).byteValue();
        }
        return reverse(bArr);
    }

    public static byte[] reverse(byte[] bArr) {
        int length = bArr.length;
        for (int i = 0; i < length / 2; i++) {
            byte b  = bArr[i];
            int  i2 = (length - 1) - i;
            bArr[i]  = bArr[i2];
            bArr[i2] = b;
        }
        return bArr;
    }

    public static boolean arrStartWith(byte[] bArr, byte[] bArr2) {
        boolean z = false;
        if (bArr.length >= bArr2.length) {
            return false;
        }
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (bArr[i2] == bArr2[i2]) {
                i++;
            }
        }
        if (i == bArr.length) {
            z = true;
        }
        return z;
    }
}
