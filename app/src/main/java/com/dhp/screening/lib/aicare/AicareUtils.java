package com.dhp.screening.lib.aicare;

import java.util.Random;

public class AicareUtils {
    private static final byte[] AICARE_FLAG = {-83, 1};

    public static native boolean compareAddress(String str);

    public static native boolean compareBytes(byte[] bArr, byte[] bArr2);

    public static native boolean compareVersion(String str, String str2);

    public static native byte[] decrypt(byte[] bArr, boolean z);

    public static native byte[] encrypt(byte[] bArr, boolean z);

    public static native boolean isVersionOk(String str);

    public static byte[] initRandomByteArr() {
        byte[] bArr = new byte[18];
        byte[] bArr2 = AICARE_FLAG;
        bArr[0] = bArr2[0];
        bArr[1] = bArr2[1];
        Random random = new Random();
        for (int i = 2; i < bArr.length; i++) {
            bArr[i] = Integer.valueOf(random.nextInt(256)).byteValue();
        }
        return bArr;
    }
}
