package com.dhp.screening.lib.aicare;

import android.bluetooth.BluetoothDevice;
import android.os.ParcelUuid;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseArray;


import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class AicareBleConfig {
    private static final byte          ADC                        = -3;
    public static final  int           ADC_STR                    = 6;
    private static final byte          AICARE_FLAG                = -84;
    private static final int           AICARE_TYPE_UNKNOWN        = 65452;
    private static final int           AICARE_TYPE_WEI            = 684;
    private static final int           AICARE_TYPE_WEI_BROAD      = 172;
    private static final int           AICARE_TYPE_WEI_BROAD_OLD  = 2086;
    private static final int           AICARE_TYPE_WEI_TEMP       = 940;
    private static final int           AICARE_TYPE_WEI_TEMP_BROAD = 428;
    public static final  int           ALGORITHM_INFO             = 11;
    public static final  byte          SET_DID                    = 29;
    private static final byte[]        ALI_AICARE_FAT             = {1, 1, SET_DID};
    private static final byte[]        ALI_FAT                    = {1, 1, 26};
    private static final int           ALI_FLAG                   = 424;
    private static final String        ALI_UUID                   = "0000feb3-0000-1000-8000-00805f9b34fb";
    private static final byte[]        ALI_WEI                    = {1, 1, 27};
    public static final  int           BLE_VERSION                = 2;
    public static final  byte          BM02V6_WEI_DATA_CHANGE     = 6;
    private static final byte          BM02V6_WEI_DATA_FAT        = 8;
    public static final  byte          BM02V6_WEI_DATA_STABLE     = 7;
    private static final String        BM09_UUID                  = "0000ffa0-0000-1000-8000-00805f9b34fb";
    public static final  int           BM15KG1                    = 1;
    public static final  int           BM15KG2                    = 4;
    public static final  int           BM15LB1                    = 2;
    public static final  int           BM15LB2                    = 5;
    public static final  int           BM15ST1                    = 3;
    public static final  int           BM15ST2                    = 6;
    public static final  byte          BM_09                      = 9;
    public static final  byte          BM_15                      = 15;
    private static final byte          BM_15_FLAG                 = -68;
    public static final  int           BODY_FAT_DATA              = 8;
    public static final  int           CMD_INDEX                  = 3;
    private static final byte          DATA                       = -53;
    private static final byte          DATA_SEND_OVER             = -4;
    public static final  int           DECIMAL_INFO               = 10;
    public static final  int           DID_STR                    = 9;
    private static final byte          FAT_DATA                   = -2;
    public static final  byte          GET_BLE_VERSION            = -9;
    public static final  byte          GET_DECIMAL_INFO           = 4;
    public static final  int           HISTORY_DATA               = 7;
    private static final String        JD_UUID                    = "0000d618-0000-1000-8000-00805f9b34fb";
    private static final byte          MCU_DATE                   = -5;
    public static final  int           MCU_DATE_STR               = 4;
    private static final byte          MCU_TIME                   = -6;
    public static final  int           MCU_TIME_STR               = 5;
    private static final byte          NEW_HISTORY_DATA           = 5;
    private static final byte          OPERATE_OR_STATE           = -2;
    public static final  byte          QUERY_DID                  = 30;
    private static final byte          SETTINGS                   = -52;
    public static final  int           SETTINGS_STATUS            = 1;
    private static final byte          START_FLAG                 = -82;
    private static final int           SUM_END                    = 7;
    private static final int           SUM_START                  = 2;
    public static final  byte          SYNC_DATE                  = -3;
    public static final  byte          SYNC_HISTORY               = -1;
    private static final byte          SYNC_HISTORY_OR_LIST       = -49;
    private static final byte          SYNC_HISTORY_STATUS        = -2;
    public static final  byte          SYNC_LIST_OVER             = 2;
    public static final  byte          SYNC_TIME                  = -4;
    public static final  byte          SYNC_UNIT                  = 6;
    public static final  byte          SYNC_USER_ID               = -6;
    public static final  byte          SYNC_USER_INFO             = -5;
    private static final byte          SYNC_USER_STATUS           = -4;
    private static final String        TAG                        = "AicareBleConfig";
    private static final byte          TYPE_WEI                   = 2;
    public static final  byte          TYPE_WEI_BROAD             = 0;
    private static final byte          TYPE_WEI_TEMP              = 3;
    public static final  byte          TYPE_WEI_TEMP_BROAD        = 1;
    public static final  byte          UNIT_JIN                   = 3;
    public static final  byte          UNIT_KG                    = 0;
    public static final  byte          UNIT_LB                    = 1;
    public static final  byte          UNIT_ST                    = 2;
    private static final byte          UPDATE_USER                = 1;
    public static final  byte          UPDATE_USER_OR_LIST        = -3;
    private static final byte          USER_ID                    = -4;
    public static final  int           USER_ID_STR                = 3;
    public static final  int           WEIGHT_DATA                = 0;
    private static final byte          WEI_CHANGE                 = -50;
    public static final  byte          WEI_DATA_CHANGE            = 1;
    public static final  byte          WEI_DATA_STABLE            = 2;
    private static final byte          WEI_STABLE                 = -54;
    private static       AlgorithmInfo algorithmInfo;
    private static       byte          deviceType                 = 2;
    private static       FatData       fatData                    = null;
    private static       boolean       isHistory                  = false;
    private static       byte[]        mByte                      = null;
    private static       byte[]        preByte                    = null;

    @Retention(RetentionPolicy.SOURCE)
    public @interface SettingStatus {
        public static final int ADC_ERROR                = 21;
        public static final int ADC_MEASURED_ING         = 20;
        public static final int DATA_SEND_END            = 25;
        public static final int ERROR                    = 3;
        public static final int HISTORY_SEND_OVER        = 18;
        public static final int HISTORY_START_SEND       = 17;
        public static final int LOW_POWER                = 1;
        public static final int LOW_VOLTAGE              = 2;
        public static final int NORMAL                   = 0;
        public static final int NO_HISTORY               = 16;
        public static final int NO_MATCH_USER            = 19;
        public static final int REQUEST_DISCONNECT       = 22;
        public static final int SET_DID_FAILED           = 24;
        public static final int SET_DID_SUCCESS          = 23;
        public static final int SET_TIME_FAILED          = 9;
        public static final int SET_TIME_SUCCESS         = 8;
        public static final int SET_UNIT_FAILED          = 7;
        public static final int SET_UNIT_SUCCESS         = 6;
        public static final int SET_USER_FAILED          = 11;
        public static final int SET_USER_SUCCESS         = 10;
        public static final int TIME_OUT                 = 4;
        public static final int UNKNOWN                  = -1;
        public static final int UNSTABLE                 = 5;
        public static final int UPDATE_USER_FAILED       = 15;
        public static final int UPDATE_USER_LIST_FAILED  = 13;
        public static final int UPDATE_USER_LIST_SUCCESS = 12;
        public static final int UPDATE_USER_SUCCESS      = 14;
        public static final int USER_LIST_SEND_OVER      = 26;
    }

    private static class UserComparator implements Comparator<User> {
        private UserComparator() {
        }

        public int compare(User user, User user2) {
            return user.getId() - user2.getId();
        }
    }

    static {
        try {
            System.loadLibrary("aicare-lib");
        } catch (Exception unused) {
        }
    }

    public static byte[] initCmd(byte b, User user, byte b2) {
        byte[] bArr = new byte[8];
        bArr[0] = AICARE_FLAG;
        bArr[1] = deviceType;
        if (user != null) {
            String        str = TAG;
            StringBuilder sb  = new StringBuilder();
            sb.append("syncUser: ");
            sb.append(user.toString());
        }
        if (b == -9) {
            bArr[2] = -9;
            bArr[6] = SETTINGS;
        } else if (b == -1) {
            bArr[2] = -1;
            bArr[6] = SYNC_HISTORY_OR_LIST;
        } else if (b == 2) {
            bArr[2] = -3;
            bArr[3] = 2;
            bArr[6] = SYNC_HISTORY_OR_LIST;
        } else if (b != 6) {
            switch (b) {
                case -6:
                    bArr[2] = -6;
                    bArr[3] = (byte) user.getId();
                    bArr[6] = SETTINGS;
                    break;
                case -5:
                    bArr[2] = -5;
                    bArr[3] = (byte) user.getSex();
                    bArr[4] = Integer.valueOf(user.getAge()).byteValue();
                    bArr[5] = Integer.valueOf(user.getHeight()).byteValue();
                    bArr[6] = SETTINGS;
                    break;
                case -4:
                    String[] split = ParseData.getTime().split(":");
                    bArr[2] = -4;
                    bArr[3] = Integer.valueOf(split[0]).byteValue();
                    bArr[4] = Integer.valueOf(split[1]).byteValue();
                    bArr[5] = Integer.valueOf(split[2]).byteValue();
                    bArr[6] = SETTINGS;
                    break;
                case -3:
                    String[] split2 = ParseData.getDate().split("-");
                    bArr[2] = -3;
                    bArr[3] = Integer.valueOf(split2[0].substring(2, 4)).byteValue();
                    bArr[4] = Integer.valueOf(split2[1]).byteValue();
                    bArr[5] = Integer.valueOf(split2[2]).byteValue();
                    bArr[6] = SETTINGS;
                    break;
            }
        } else {
            bArr[2] = -2;
            bArr[3] = 6;
            bArr[4] = b2;
            bArr[6] = SETTINGS;
        }
        bArr[7] = getByteSum(bArr, 2, 7);
        String        str2 = TAG;
        StringBuilder sb2  = new StringBuilder();
        sb2.append("initCmd: ");
        sb2.append(ParseData.byteArr2Str(bArr));
        return bArr;
    }

    public static byte[] initNewCmd(byte b) {
        byte[] bArr = new byte[6];
        bArr[0] = START_FLAG;
        bArr[1] = 3;
        bArr[2] = deviceType;
        bArr[3] = b;
        bArr[4] = 1;
        bArr[5] = getByteSum(bArr, 2, bArr.length - 1);
        return bArr;
    }

    public static byte[] initDIDCmd(byte b, int i) {
        byte[] bArr = new byte[8];
        bArr[0] = AICARE_FLAG;
        bArr[1] = deviceType;
        bArr[2] = -2;
        bArr[3] = b;
        if (b == 29) {
            byte[] int2byte = ParseData.int2byte(i);
            bArr[4] = int2byte[0];
            bArr[5] = int2byte[1];
        }
        bArr[6] = SETTINGS;
        bArr[7] = getByteSum(bArr, 2, 7);
        String        str = TAG;
        StringBuilder sb  = new StringBuilder();
        sb.append("initDIDCmd: ");
        sb.append(ParseData.byteArr2Str(bArr));
        L.e(str, sb.toString());
        return bArr;
    }

    public static byte[] initUpdateUserCmd(User user) {
        String        str = TAG;
        StringBuilder sb  = new StringBuilder();
        sb.append("updateUser: ");
        sb.append(user.toString());
        L.e(str, sb.toString());
        byte[] bArr = new byte[20];
        bArr[0] = AICARE_FLAG;
        bArr[1] = deviceType;
        bArr[2] = -3;
        bArr[3] = 1;
        initUserListByteArray(bArr, 4, user);
        String        str2 = TAG;
        StringBuilder sb2  = new StringBuilder();
        sb2.append("initUpdateUserCmd: ");
        sb2.append(ParseData.byteArr2Str(bArr));
        return bArr;
    }

    private static byte getByteSum(byte[] bArr, int i, int i2) {
        int i3 = 0;
        while (i < i2) {
            i3 += bArr[i];
            i++;
        }
        return (byte) (i3 & 255);
    }

    private static boolean checkData(byte[] bArr) {
        boolean z = false;
        if (bArr == null || bArr.length == 0) {
            return false;
        }
        if ((bArr.length != 8 && bArr.length != 20) || bArr[0] != -84 || (bArr[1] != 2 && bArr[1] != 3 && bArr[1] != 1 && bArr[1] != 0)) {
            return false;
        }
        byte          byteSum = getByteSum(bArr, 2, 7);
        String        str     = TAG;
        StringBuilder sb      = new StringBuilder();
        sb.append("result = ");
        sb.append(byteSum);
        String        str2 = TAG;
        StringBuilder sb2  = new StringBuilder();
        sb2.append("b[SUM_END] = ");
        sb2.append(bArr[7]);
        if (byteSum == bArr[7]) {
            z = true;
        }
        return z;
    }

    public static boolean isValid(byte[] bArr) {
        boolean z = false;
        if (bArr[0] != -82 || ((bArr[2] != 2 && bArr[2] != 3) || bArr[1] + 2 >= bArr.length)) {
            return false;
        }
        if (getByteSum(bArr, 2, bArr[1] + 2) == bArr[bArr.length - 1]) {
            z = true;
        }
        return z;
    }

    public static List<byte[]> initUserListCmds(List<User> list) {
        Collections.sort(list, new UserComparator());
        ArrayList arrayList = new ArrayList();
        byte[]    bArr      = new byte[0];
        for (int i = 0; i < list.size(); i++) {
            String        str = TAG;
            StringBuilder sb  = new StringBuilder();
            sb.append("userList: ");
            sb.append(((User) list.get(i)).toString());
            if (i % 2 == 0) {
                bArr    = new byte[20];
                bArr[0] = AICARE_FLAG;
                bArr[1] = deviceType;
                bArr[2] = -3;
                initUserListByteArray(bArr, 4, (User) list.get(i));
                if (i == list.size() - 1) {
                    arrayList.add(bArr);
                }
            } else {
                initUserListByteArray(bArr, 12, (User) list.get(i));
                arrayList.add(bArr);
            }
        }
        return arrayList;
    }

    private static void initUserListByteArray(byte[] bArr, int i, User user) {
        bArr[i] = (byte) user.getId();
        int i2 = i + 1;
        bArr[i2] = (byte) user.getSex();
        int i3 = i2 + 1;
        bArr[i3] = (byte) user.getAge();
        int i4 = i3 + 1;
        bArr[i4] = (byte) user.getHeight();
        byte[] int2byte = ParseData.int2byte(Double.valueOf((double) user.getWeight()).intValue());
        int    i5       = i4 + 1;
        bArr[i5] = int2byte[0];
        int i6 = i5 + 1;
        bArr[i6] = int2byte[1];
        byte[] int2byte2 = ParseData.int2byte(user.getAdc());
        int    i7        = i6 + 1;
        bArr[i7]     = int2byte2[0];
        bArr[i7 + 1] = int2byte2[1];
    }

    public static BroadData getBroadData(BluetoothDevice bluetoothDevice, int i, byte[] bArr) {
        ScanRecord parseFromBytes = ScanRecord.parseFromBytes(bArr);
        if (parseFromBytes != null) {
            SparseArray manufacturerSpecificData = parseFromBytes.getManufacturerSpecificData();
            List        serviceUuids             = parseFromBytes.getServiceUuids();
            if (manufacturerSpecificData != null && manufacturerSpecificData.size() > 0) {
                BroadData broadData = new BroadData();
                broadData.setAddress(bluetoothDevice.getAddress());
                broadData.setName(bluetoothDevice.getName());
                broadData.setRssi(i);
                boolean z     = false;
                int     keyAt = manufacturerSpecificData.keyAt(0);
                byte[]  bArr2 = (byte[]) manufacturerSpecificData.get(keyAt);
                if (isListEmpty(serviceUuids)) {
                    byte[] reverse = ParseData.reverse(ParseData.int2byte(keyAt));
                    if (reverse[0] == -68) {
                        broadData.setSpecificData(ParseData.contact(reverse, bArr2));
                        StringBuilder sb = new StringBuilder();
                        sb.append("广播数据：");
                        sb.append(ParseData.byteArr2Str(broadData.getSpecificData()));
                        L.i("2018-07-04", sb.toString());
                        broadData.setBright(true);
                        broadData.setDeviceType(15);
                        return broadData;
                    }
                } else if (isContainJD(serviceUuids)) {
                    String        str = TAG;
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("JD manufacturerId: ");
                    sb2.append(keyAt);
                    L.e(str, sb2.toString());
                    if (keyAt == AICARE_TYPE_WEI || keyAt == AICARE_TYPE_WEI_TEMP || keyAt == AICARE_TYPE_UNKNOWN) {
                        String        str2 = TAG;
                        StringBuilder sb3  = new StringBuilder();
                        sb3.append("specificData: ");
                        sb3.append(ParseData.byteArr2Str(bArr2));
                        L.e(str2, sb3.toString());
                        setDeviceType(keyAt);
                        if (getFlag(bArr2) != 0) {
                            z = true;
                        }
                        broadData.setBright(z);
                        broadData.setDeviceType(deviceType);
                        return broadData;
                    }
                } else if (isContainALI(serviceUuids)) {
                    String        str3 = TAG;
                    StringBuilder sb4  = new StringBuilder();
                    sb4.append("ALI manufacturerId: ");
                    sb4.append(keyAt);
                    if (keyAt == ALI_FLAG) {
                        String        str4 = TAG;
                        StringBuilder sb5  = new StringBuilder();
                        sb5.append("specificData: ");
                        sb5.append(ParseData.byteArr2Str(bArr2));
                        L.e(str4, sb5.toString());
                        if (!isArrEmpty(bArr2) && (isArrStartWith(bArr2, ALI_WEI) || isArrStartWith(bArr2, ALI_FAT) || isArrStartWith(bArr2, ALI_AICARE_FAT))) {
                            setDeviceType(bArr2);
                            if (getFlag(bArr2) != 0) {
                                z = true;
                            }
                            broadData.setBright(z);
                            broadData.setDeviceType(deviceType);
                            return broadData;
                        }
                    }
                } else if (isContainAicare(serviceUuids)) {
                    String        str5 = TAG;
                    StringBuilder sb6  = new StringBuilder();
                    sb6.append("Aicare manufacturerId: ");
                    sb6.append(keyAt);
                    L.e(str5, sb6.toString());
                    if (keyAt == AICARE_TYPE_WEI || keyAt == AICARE_TYPE_WEI_TEMP || keyAt == AICARE_TYPE_UNKNOWN) {
                        String        str6 = TAG;
                        StringBuilder sb7  = new StringBuilder();
                        sb7.append("specificData: ");
                        sb7.append(ParseData.byteArr2Str(bArr2));
                        L.e(str6, sb7.toString());
                        setDeviceType(keyAt);
                        if (getFlag(bArr2) != 0) {
                            z = true;
                        }
                        broadData.setBright(z);
                        broadData.setDeviceType(deviceType);
                        return broadData;
                    } else if (keyAt == AICARE_TYPE_WEI_BROAD || keyAt == AICARE_TYPE_WEI_TEMP_BROAD) {
                        setDeviceType(keyAt);
                        broadData.setBright(true);
                        broadData.setSpecificData(getSpecialData(bluetoothDevice.getAddress(), bArr2));
                        broadData.setDeviceType(deviceType);
                        return broadData;
                    }
                } else if (keyAt == AICARE_TYPE_WEI_BROAD_OLD) {
                    broadData.setSpecificData(bArr2);
                    broadData.setBright(true);
                    broadData.setDeviceType(0);
                    return broadData;
                } else if (isBM09(serviceUuids)) {
                    broadData.setSpecificData(ParseData.contact(ParseData.reverse(ParseData.int2byte(keyAt)), bArr2));
                    broadData.setBright(true);
                    broadData.setDeviceType(9);
                    return broadData;
                }
            }
        }
        return null;
    }

    private static byte[] getSpecialData(String str, byte[] bArr) {
        byte[] copyOf = Arrays.copyOf(ParseData.address2byte(str), 10);
        copyOf[6] = 11;
        copyOf[7] = -1;
        copyOf[8] = 38;
        copyOf[9] = 8;
        if (ParseData.arrStartWith(copyOf, bArr)) {
            return Arrays.copyOfRange(bArr, copyOf.length, bArr.length);
        }
        return null;
    }

    private static void setDeviceType(int i) {
        if (i == AICARE_TYPE_WEI_BROAD) {
            deviceType = 0;
        } else if (i == AICARE_TYPE_WEI_TEMP_BROAD) {
            deviceType = 1;
        } else if (i == AICARE_TYPE_WEI) {
            deviceType = 2;
        } else if (i == AICARE_TYPE_WEI_TEMP) {
            deviceType = 3;
        }
    }

    private static void setDeviceType(byte[] bArr) {
        if (isArrStartWith(bArr, ALI_WEI)) {
            deviceType = 2;
        } else if (isArrStartWith(bArr, ALI_AICARE_FAT) || isArrStartWith(bArr, ALI_FAT)) {
            deviceType = 2;
        } else {
            deviceType = 2;
        }
    }

    private static int getFlag(byte[] bArr) {
        byte b = 0;
        if (bArr.length < 7) {
            return 0;
        }
        if (bArr[6] <= 1 && bArr[6] >= 0) {
            b = bArr[6];
        }
        return b;
    }

    private static boolean isArrStartWith(byte[] bArr, byte[] bArr2) {
        if (bArr.length == 0 || bArr2.length == 0) {
            return false;
        }
        for (int i = 0; i < bArr2.length; i++) {
            if (bArr2[i] != bArr[i]) {
                return false;
            }
        }
        return true;
    }

    private static boolean isContainJD(List<ParcelUuid> list) {
        return !isListEmpty(list) && list.contains(ParcelUuid.fromString(JD_UUID)) && list.contains(ParcelUuid.fromString("0000ffb0-0000-1000-8000-00805f9b34fb"));
    }

    private static boolean isContainALI(List<ParcelUuid> list) {
        return !isListEmpty(list) && list.contains(ParcelUuid.fromString(ALI_UUID)) && list.contains(ParcelUuid.fromString("0000ffb0-0000-1000-8000-00805f9b34fb"));
    }

    private static boolean isContainAicare(List<ParcelUuid> list) {
        return !isListEmpty(list) && list.contains(ParcelUuid.fromString("0000ffb0-0000-1000-8000-00805f9b34fb"));
    }

    private static boolean isBM09(List<ParcelUuid> list) {
        return !isListEmpty(list) && list.contains(ParcelUuid.fromString(BM09_UUID));
    }

    private static boolean isArrEmpty(byte[] bArr) {
        return bArr == null || bArr.length == 0;
    }

    private static <T> boolean isListEmpty(List<T> list) {
        return list == null || list.size() == 0;
    }

    public static SparseArray<Object> getDatas(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        if (checkData(bArr)) {
            switch (bArr[6]) {
                case -54:
                case -50:
                    return getWeiData(bArr);
                case -53:
                    return getData(bArr);
                case -52:
                    return getDeviceStatus(bArr);
                case -49:
                    if (bArr[2] == -2) {
                        return getHistoryStatus(bArr);
                    }
                    if (bArr[2] == -4) {
                        return getSyncUserStatus(bArr);
                    }
                    return sparseArray;
                default:
                    return sparseArray;
            }
        } else if (isBm02V6(bArr)) {
            return getBm02V6Data(bArr);
        } else {
            if (isValid(bArr)) {
                return getBleData(bArr);
            }
            if (isHistoryData(bArr)) {
                return getHistoryData(getConcatBytes(bArr));
            }
            byte[] unpack = unpack(bArr);
            if (unpack == null) {
                return sparseArray;
            }
            if (unpack[0] != -84) {
                return unpack[0] == -82 ? getDecimalInfo(unpack) : sparseArray;
            }
            if (unpack[2] == -9) {
                sparseArray.put(2, getVersion(unpack, 3, 2015));
                return sparseArray;
            } else if (unpack[2] == -2 && unpack[6] == -49) {
                return getHistoryStatus(unpack);
            } else {
                return sparseArray;
            }
        }
    }

    private static boolean isBm02V6(byte[] bArr) {
        boolean z = false;
        if ((bArr[0] & SYNC_HISTORY) != 174 || ((bArr[2] != 2 && bArr[2] != 3) || (bArr[3] != 6 && bArr[3] != 7 && bArr[3] != 8))) {
            return false;
        }
        int i = bArr[1] + 2;
        if (getByteSum(bArr, 2, i) == bArr[i]) {
            z = true;
        }
        return z;
    }

    private static byte[] unpack(byte[] bArr) {
        List index = getIndex(bArr);
        if (!index.isEmpty()) {
            for (int i = 0; i < index.size(); i++) {
                int intValue = ((Integer) index.get(i)).intValue();
                int i2       = intValue + 8;
                if (bArr.length >= i2 && bArr[intValue] == -84) {
                    int i3 = intValue + 2;
                    if (bArr[i3] == -9) {
                        byte[] bArr2 = new byte[8];
                        System.arraycopy(bArr, intValue, bArr2, 0, bArr2.length);
                        return bArr2;
                    } else if (bArr[i3] == -2 && bArr[intValue + 6] == -49) {
                        byte[] bArr3 = new byte[8];
                        System.arraycopy(bArr, intValue, bArr3, 0, bArr3.length);
                        return bArr3;
                    }
                } else if (bArr[intValue] == -82 && bArr[intValue + 3] == 4) {
                    int i4 = intValue + 1;
                    if (bArr[i4] == 5) {
                        if (bArr.length >= i2) {
                            byte[] bArr4 = new byte[8];
                            System.arraycopy(bArr, intValue, bArr4, 0, bArr4.length);
                            return bArr4;
                        }
                    } else if (bArr[i4] == 6 && bArr.length >= intValue + 9) {
                        byte[] bArr5 = new byte[9];
                        System.arraycopy(bArr, intValue, bArr5, 0, bArr5.length);
                        return bArr5;
                    }
                }
            }
        }
        return null;
    }

    private static List<Integer> getIndex(byte[] bArr) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < bArr.length; i++) {
            if (bArr[i] == -84 || bArr[i] == -82) {
                arrayList.add(Integer.valueOf(i));
            }
        }
        return arrayList;
    }

    private static SparseArray<Object> getBodyFatData(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        if (fatData == null) {
            fatData = new FatData();
        }
        byte b = bArr[3];
        if (b != -4) {
            switch (b) {
                case 0:
                    fatData.setWeight((float) ParseData.getDataInt(4, 5, bArr));
                    break;
                case 1:
                    fatData.setBmi(((float) ParseData.getDataInt(4, 5, bArr)) / 10.0f);
                    break;
                case 2:
                    fatData.setBfr(((float) ParseData.getDataInt(4, 5, bArr)) / 10.0f);
                    break;
                case 3:
                    fatData.setSfr(((float) ParseData.getDataInt(4, 5, bArr)) / 10.0f);
                    break;
                case 4:
                    fatData.setUvi(ParseData.getDataInt(4, 5, bArr));
                    break;
                case 5:
                    fatData.setRom(((float) ParseData.getDataInt(4, 5, bArr)) / 10.0f);
                    break;
                case 6:
                    fatData.setBmr((float) ParseData.getDataInt(4, 5, bArr));
                    break;
                case 7:
                    fatData.setBm(((float) ParseData.getDataInt(4, 5, bArr)) / 10.0f);
                    break;
                case 8:
                    fatData.setVwc(((float) ParseData.getDataInt(4, 5, bArr)) / 10.0f);
                    break;
                case 9:
                    fatData.setBodyAge(ParseData.getDataInt(4, 5, bArr));
                    break;
                case 10:
                    fatData.setPp(((float) ParseData.getDataInt(4, 5, bArr)) / 10.0f);
                    break;
            }
        } else {
            L.e(TAG, "DATA_SEND_OVER");
        }
        return sparseArray;
    }

    private static SparseArray<Object> getData(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        switch (bArr[2]) {
            case -6:
                String dateOrTime = getDateOrTime((byte) -6, bArr, 3);
                if (fatData == null) {
                    fatData = new FatData();
                }
                fatData.setTime(dateOrTime);
                sparseArray.put(5, dateOrTime);
                return sparseArray;
            case -5:
                String dateOrTime2 = getDateOrTime((byte) -5, bArr, 3);
                if (fatData == null) {
                    fatData = new FatData();
                }
                fatData.setDate(dateOrTime2);
                sparseArray.put(4, dateOrTime2);
                return sparseArray;
            case -4:
                return getUserId(bArr);
            case -3:
                return getADC(bArr);
            case -2:
                return getBodyFatData(bArr);
            default:
                return sparseArray;
        }
    }

    private static String getDateOrTime(byte b, byte[] bArr, int i) {
        StringBuilder sb       = new StringBuilder();
        String        valueOf  = String.valueOf(ParseData.binaryToDecimal(bArr[i]));
        int           i2       = i + 1;
        String        valueOf2 = String.valueOf(ParseData.binaryToDecimal(bArr[i2]));
        String        valueOf3 = String.valueOf(ParseData.binaryToDecimal(bArr[i2 + 1]));
        switch (b) {
            case -6:
                sb.append(ParseData.addZero(valueOf));
                sb.append(":");
                sb.append(ParseData.addZero(valueOf2));
                sb.append(":");
                sb.append(ParseData.addZero(valueOf3));
                break;
            case -5:
                sb.append("20");
                sb.append(ParseData.addZero(valueOf));
                sb.append("-");
                sb.append(ParseData.addZero(valueOf2));
                sb.append("-");
                sb.append(ParseData.addZero(valueOf3));
                break;
        }
        return sb.toString();
    }

    private static SparseArray<Object> getADC(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        switch (bArr[3]) {
            case -1:
                sparseArray.put(1, Integer.valueOf(21));
                algorithmInfo = null;
                fatData = null;
                break;
            case 0:
                sparseArray.put(1, Integer.valueOf(20));
                int dataInt = ParseData.getDataInt(4, 5, bArr);
                if (dataInt > 0 && algorithmInfo == null) {
                    algorithmInfo = new AlgorithmInfo();
                    algorithmInfo.setAlgorithmId(dataInt);
                    break;
                }
            case 1:
                int dataInt2 = ParseData.getDataInt(4, 5, bArr);
                AlgorithmInfo algorithmInfo2 = algorithmInfo;
                if (algorithmInfo2 == null) {
                    if (fatData == null) {
                        fatData = new FatData();
                    }
                    fatData.setAdc(dataInt2);
                    sparseArray.put(6, Integer.valueOf(dataInt2));
                    break;
                } else {
                    algorithmInfo2.setAdc(dataInt2);
                    FatData fatData2 = fatData;
                    if (fatData2 != null) {
                        algorithmInfo.setWeight(fatData2.getWeight());
                        fatData = null;
                    }
                    sparseArray.put(11, algorithmInfo);
                    algorithmInfo = null;
                    break;
                }
        }
        return sparseArray;
    }

    private static SparseArray<Object> getUserId(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        if (bArr[3] == Byte.MAX_VALUE) {
            sparseArray.put(1, Integer.valueOf(19));
        } else {
            sparseArray.put(3, String.valueOf(ParseData.binaryToDecimal(bArr[3])));
        }
        return sparseArray;
    }

    private static SparseArray<Object> getWeiData(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        sparseArray.put(0, getWeightData(bArr));
        return sparseArray;
    }

    private static SparseArray<Object> getBleData(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        byte                b           = bArr[3];
        if (b == 4) {
            return getDecimalInfo(bArr);
        }
        switch (b) {
            case 1:
                return getWeiData(bArr, 1);
            case 2:
                return getWeiData(bArr, 2);
            default:
                return sparseArray;
        }
    }

    private static SparseArray<Object> getBm02V6Data(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        WeiData             weiData     = new WeiData();
        float               f           = (float) (((bArr[4] & SYNC_HISTORY) << 8) + (bArr[5] & SYNC_HISTORY));
        weiData.setWeight(f);
        if (bArr[2] == 3) {
            int i = (bArr[6] & BM_15) << ((bArr[7] & SYNC_HISTORY) + 8);
            if ((bArr[6] & 240) == 240) {
                i = -i;
            }
            weiData.setTemp(((float) i) / 10.0f);
        } else {
            weiData.setTemp(Float.MAX_VALUE);
        }
        DecInfo decInfo = new DecInfo();
        decInfo.setSourceDecimal(bArr[8] >> 4);
        decInfo.setKgDecimal(bArr[8] & BM_15);
        decInfo.setLbDecimal(bArr[9] >> 4);
        decInfo.setStDecimal(bArr[9] & BM_15);
        decInfo.setKgGraduation(bArr[10] >> 4);
        decInfo.setLbGraduation(bArr[10] & BM_15);
        weiData.setDecInfo(decInfo);
        switch (bArr[3]) {
            case 6:
                weiData.setCmdType(1);
                break;
            case 7:
                weiData.setCmdType(2);
                if (fatData == null) {
                    fatData = new FatData();
                }
                fatData.setWeight(f);
                break;
        }
        sparseArray.put(0, weiData);
        return sparseArray;
    }

    private static SparseArray<Object> getWeiData(byte[] bArr, int i) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        float               data        = (float) ParseData.getData(4, 5, 6, bArr);
        float               temp        = bArr[2] == 3 ? getTemp(7, 8, bArr) : Float.MAX_VALUE;
        if (i == 2) {
            if (fatData == null) {
                fatData = new FatData();
            }
            fatData.setWeight(data);
        }
        sparseArray.put(0, new WeiData(i, data, temp, null));
        return sparseArray;
    }

    private static SparseArray<Object> getDecimalInfo(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        sparseArray.put(10, getDecimalInfo(bArr, 4));
        return sparseArray;
    }

    private static DecInfo getDecimalInfo(byte[] bArr, int i) {
        byte b;
        int  i2;
        int  i3 = i + 1;
        int  i4 = bArr[i3] >> 4;
        byte b2 = (byte) (bArr[i3] & BM_15);
        int  i5 = i3 + 1;
        int  i6 = bArr[i5] >> 4;
        byte b3 = (byte) (bArr[i5] & BM_15);
        if (bArr[1] == 6) {
            int i7 = i5 + 1;
            b  = (byte) (bArr[i7] & BM_15);
            i2 = bArr[i7] >> 4;
        } else {
            i2 = 0;
            b  = 0;
        }

        DecInfo decInfo = new DecInfo(i4, b2, i6, b3, i2, b);

        return decInfo;
    }

    private static SparseArray<Object> getHistoryStatus(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        switch (bArr[3]) {
            case 0:
                sparseArray.put(1, Integer.valueOf(16));
                break;
            case 1:
                isHistory = true;
                sparseArray.put(1, Integer.valueOf(17));
                break;
            case 2:
                isHistory = false;
                sparseArray.put(1, Integer.valueOf(18));
                break;
        }
        return sparseArray;
    }

    private static SparseArray<Object> getSyncUserStatus(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        switch (bArr[3]) {
            case 0:
                sparseArray.put(1, Integer.valueOf(12));
                break;
            case 1:
                sparseArray.put(1, Integer.valueOf(15));
                break;
            case 2:
                sparseArray.put(1, Integer.valueOf(14));
                break;
            case 3:
                sparseArray.put(1, Integer.valueOf(15));
                break;
        }
        return sparseArray;
    }

    private static SparseArray<Object> getDeviceStatus(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        if (bArr[2] == -9) {
            sparseArray.put(2, getVersion(bArr, 3, 2015));
            return sparseArray;
        }
        byte b = bArr[3];
        if (b == 16) {
            Log.i("TAG", "getDeviceStatus: ---------------------- DATA_SEND_END");
            FatData fatData2 = fatData;
            if (fatData2 == null || fatData2.getWeight() <= 0.0f) {
                return sparseArray;
            }
            Log.i("TAG", "getDeviceStatus: ----------------sparseArray.put(BODY_FAT_DATA, fatData)");
            sparseArray.put(8, fatData);
            fatData = null;
            return sparseArray;
        } else if (b != 27) {
            switch (b) {
                case 0:
                    sparseArray.put(1, Integer.valueOf(0));
                    return sparseArray;
                case 1:
                    sparseArray.put(1, Integer.valueOf(1));
                    return sparseArray;
                case 2:
                    sparseArray.put(1, Integer.valueOf(2));
                    return sparseArray;
                case 3:
                    sparseArray.put(1, Integer.valueOf(3));
                    return sparseArray;
                case 4:
                    sparseArray.put(1, Integer.valueOf(4));
                    return sparseArray;
                case 5:
                    sparseArray.put(1, Integer.valueOf(5));
                    return sparseArray;
                case 6:
                    return getChangeUnitStatus(bArr);
                case 7:
                    sparseArray.put(1, Integer.valueOf(8));
                    return sparseArray;
                case 8:
                    sparseArray.put(1, Integer.valueOf(9));
                    return sparseArray;
                case 9:
                    sparseArray.put(1, Integer.valueOf(10));
                    return sparseArray;
                case 10:
                    sparseArray.put(1, Integer.valueOf(11));
                    return sparseArray;
                default:
                    switch (b) {
                        case 29:
                        case 30:
                            return getDid(bArr);
                        default:
                            return sparseArray;
                    }
            }
        } else {
            sparseArray.put(1, Integer.valueOf(22));
            return sparseArray;
        }
    }

    private static SparseArray<Object> getDid(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        switch (bArr[3]) {
            case 29:
                switch (bArr[4]) {
                    case 0:
                        sparseArray.put(1, Integer.valueOf(24));
                        break;
                    case 1:
                        sparseArray.put(1, Integer.valueOf(23));
                        break;
                }
            case 30:
                if (!((bArr[4] & SYNC_HISTORY) == 255 || (bArr[5] & SYNC_HISTORY) == 255)) {
                    sparseArray.put(9, Integer.valueOf(ParseData.getDataInt(4, 5, bArr)));
                    break;
                }
        }
        return sparseArray;
    }

    private static String getVersion(byte[] bArr, int i, int i2) {
        int           binaryToDecimal  = (ParseData.binaryToDecimal(bArr[i]) / 16) + i2;
        int           binaryToDecimal2 = ParseData.binaryToDecimal(bArr[i]) % 16;
        int           i3               = i + 1;
        int           binaryToDecimal3 = ParseData.binaryToDecimal(bArr[i3]);
        float         floatValue       = new BigDecimal((double) (((float) ParseData.binaryToDecimal(bArr[i3 + 1])) / 10.0f)).setScale(1, 4).floatValue();
        StringBuilder sb               = new StringBuilder();
        sb.append(binaryToDecimal);
        sb.append(ParseData.addZero(String.valueOf(binaryToDecimal2)));
        sb.append(ParseData.addZero(String.valueOf(binaryToDecimal3)));
        sb.append("_");
        sb.append(floatValue);
        return sb.toString();
    }

    private static SparseArray<Object> getChangeUnitStatus(byte[] bArr) {
        SparseArray<Object> sparseArray = new SparseArray<>();
        switch (bArr[4]) {
            case -2:
                sparseArray.put(1, Integer.valueOf(6));
                break;
            case -1:
                sparseArray.put(1, Integer.valueOf(7));
                break;
        }
        return sparseArray;
    }

    private static float getTemp(int i, int i2, byte[] bArr) {
        byte  byteValue = Integer.valueOf(bArr[i] >> 4).byteValue();
        float dataInt   = ((float) ParseData.getDataInt(0, 1, new byte[]{Integer.valueOf(bArr[i] & BM_15).byteValue(), bArr[i2]})) / 10.0f;
        return byteValue == 15 ? -dataInt : dataInt;
    }

    private static boolean isHistoryData(byte[] bArr) {
        if (bArr.length != 20 || (!isHistoryStart(bArr) && bArr[0] != 1)) {
            byte[] bArr2 = preByte;
            if (bArr2 != null && isHistoryStart(bArr2)) {
                byte[] bArr3 = preByte;
                if ((bArr3[3] == 0 || bArr3[4] == 0) && bArr[0] == 1) {
                    preByte = null;
                    return true;
                }
            }
            preByte = null;
            return false;
        }
        preByte = bArr;
        return true;
    }

    private static boolean isHistoryStart(byte[] bArr) {
        if (!(bArr[0] == -84 && ((bArr[1] == 2 || bArr[1] == 3) && bArr[2] == -1))) {
            if (bArr[0] != -82) {
                return false;
            }
            if (!((bArr[2] == 2 || bArr[2] == 3) && bArr[3] == 5)) {
                return false;
            }
        }
        return true;
    }

    private static byte[] getConcatBytes(byte[] bArr) {
        byte[] bArr2 = mByte;
        if (bArr2 != null) {
            char c = bArr2[0] == -84 ? (char) 3 : 65535;
            if (mByte[0] == -82) {
                c = 4;
            }
            if (c == 65535) {
                return bArr;
            }
            byte   b     = bArr[0];
            byte[] bArr3 = mByte;
            if (b - bArr3[c] == 1) {
                byte[] contact = ParseData.contact(bArr3, bArr);
                mByte = null;
                return contact;
            }
            mByte = null;
        } else {
            mByte = bArr;
        }
        return bArr;
    }

    private static SparseArray<Object> getHistoryData(byte[] bArr) {
        float         f;
        float         f2;
        byte[]        bArr2 = bArr;
        StringBuilder sb    = new StringBuilder();
        sb.append("b.length = ");
        sb.append(bArr2.length);
        SparseArray<Object> sparseArray = new SparseArray<>();
        if (bArr2.length > 20) {
            int i = bArr2[0] == -84 ? 6 : -1;
            if (bArr2[0] == -82) {
                i = 5;
            }
            if (i == -1) {
                return sparseArray;
            }
            String dateOrTime  = getDateOrTime((byte) -5, bArr2, i);
            int    i2          = i + 3;
            String dateOrTime2 = getDateOrTime((byte) -6, bArr2, i2);
            int    i3          = i2 + 3;
            if (bArr2[0] == -84) {
                int i4 = i3 + 1;
                int i5 = i4;
                f  = (float) ParseData.getDataInt(i3, i4, bArr2);
                i3 = i5;
            } else {
                f = -1.0f;
            }
            if (bArr2[0] == -82) {
                int i6 = i3 + 1;
                int i7 = i6 + 1;
                f2 = (float) ParseData.getData(i3, i6, i7, bArr2);
                i3 = i7;
            } else {
                f2 = f;
            }
            if (f2 == -1.0f) {
                return sparseArray;
            }
            int     i8              = i3 + 1;
            int     i9              = i8 + 1;
            float   dataInt         = ((float) ParseData.getDataInt(i8, i9, bArr2)) / 10.0f;
            int     i10             = i9 + 1;
            int     i11             = i10 + 1;
            float   percent         = ParseData.getPercent(((float) ParseData.getDataInt(i10, i11, bArr2)) / 10.0f);
            int     i12             = i11 + 1;
            int     i13             = i12 + 1;
            float   dataInt2        = ((float) ParseData.getDataInt(i12, i13, bArr2)) / 10.0f;
            int     i14             = i13 + 1 + 1;
            int     i15             = i14 + 1;
            int     dataInt3        = ParseData.getDataInt(i14, i15, bArr2);
            int     i16             = i15 + 1;
            int     i17             = i16 + 1;
            float   percent2        = ParseData.getPercent(((float) ParseData.getDataInt(i16, i17, bArr2)) / 10.0f);
            int     i18             = i17 + 1;
            int     i19             = i18 + 1;
            float   dataInt4        = (float) ParseData.getDataInt(i18, i19, bArr2);
            int     i20             = i19 + 1;
            int     i21             = i20 + 1;
            float   dataInt5        = ((float) ParseData.getDataInt(i20, i21, bArr2)) / 10.0f;
            int     i22             = i21 + 1;
            int     i23             = i22 + 1;
            float   percent3        = ParseData.getPercent(((float) ParseData.getDataInt(i22, i23, bArr2)) / 10.0f);
            int     i24             = i23 + 1;
            int     binaryToDecimal = ParseData.binaryToDecimal(bArr2[i24]);
            int     i25             = i24 + 1;
            int     i26             = i25 + 1;
            int     i27             = i26 + 1;
            int     i28             = i27 + 1;
            int     i29             = i28 + 1;
            int     i30             = i29 + 1;
            int     i31             = i30 + 1;
            FatData fatData2        = new FatData(dateOrTime, dateOrTime2, f2, dataInt, percent, dataInt2, dataInt3, percent2, dataInt4, dataInt5, percent3, binaryToDecimal, ((float) ParseData.getDataInt(i25, i26, bArr2)) / 10.0f, ParseData.binaryToDecimal(bArr2[i27]), ParseData.binaryToDecimal(bArr2[i28]), ParseData.binaryToDecimal(bArr2[i29]), ParseData.binaryToDecimal(bArr2[i30]), ParseData.getDataInt(i31, i31 + 1, bArr2), null);
            if (isHistory) {
                sparseArray.put(7, fatData2);
            } else {
                sparseArray.put(8, fatData2);
            }
        }
        return sparseArray;
    }

    public static byte[] getRandomBytes() {
        return AicareUtils.initRandomByteArr();
    }

    public static byte[] encrypt(byte[] bArr, boolean z) {
        return AicareUtils.encrypt(bArr, z);
    }

    public static byte[] decrypt(byte[] bArr, boolean z) {
        return AicareUtils.decrypt(bArr, z);
    }

    public static boolean compareBytes(byte[] bArr, byte[] bArr2) {
        return AicareUtils.compareBytes(bArr, bArr2);
    }

    public static boolean compareVersion(String str, String str2) {
        return AicareUtils.compareVersion(str, str2);
    }

    public static boolean compareAddress(String str) {
        return AicareUtils.compareAddress(str);
    }

    public static WeiData getWeightData(byte[] bArr) {
        if (!checkData(bArr) || (bArr[6] != -50 && bArr[6] != -54)) {
            return null;
        }
        int   i       = 2;
        float dataInt = (float) ParseData.getDataInt(2, 3, bArr);
        float f       = Float.MAX_VALUE;
        if (bArr[1] == 3 || bArr[1] == 1) {
            f = getTemp(4, 5, bArr);
        }
        byte b = bArr[6];
        if (bArr[6] == -54) {
            if (fatData == null) {
                fatData = new FatData();
            }
            fatData.setWeight(dataInt);
        } else {
            i = 1;
        }
        return new WeiData(i, dataInt, f, null);
    }

    public static String getWeight(double d, byte b, DecInfo decInfo) {
        if (decInfo == null) {
            L.i(TAG, "getWeight:decInfo == null,赋值全部为1");
            decInfo = new DecInfo(1, 1, 1, 1, 1, 1);
        }
        switch (b) {
            case 1:
                return ParseData.getLbWeight(d, decInfo);
            case 2:
                return ParseData.getStWeight(d, decInfo);
            case 3:
                return ParseData.getJinWeight(d, decInfo);
            default:
                return ParseData.getKgWeight(d, decInfo);
        }
    }

    @NonNull
    public static BM09Data getBm09Data(String str, byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("原始数据: ");
        sb.append(ParseData.byteArr2Str(bArr));
        L.e("BM09Data", sb.toString());
        BM09Data bM09Data = new BM09Data();
        if (bArr == null || bArr.length < 18) {
            return bM09Data;
        }
        boolean       z       = false;
        byte[]        decrypt = decrypt(Arrays.copyOfRange(bArr, 0, 16), false);
        StringBuilder sb2     = new StringBuilder();
        sb2.append("解密: ");
        sb2.append(ParseData.byteArr2Str(decrypt));
        L.e("BM09Data", sb2.toString());
        byte[]        contact = ParseData.contact(decrypt, Arrays.copyOfRange(bArr, 16, bArr.length));
        StringBuilder sb3     = new StringBuilder();
        sb3.append("newData: ");
        sb3.append(ParseData.byteArr2Str(contact));
        L.e("BM09Data", sb3.toString());
        if (contact == null) {
            return bM09Data;
        }
        bM09Data.setAddress(str);
        bM09Data.setAgreementType(contact[0] >> 4);
        bM09Data.setUnitType(contact[0] & BM_15);
        bM09Data.setDecInfo(getDecimalInfo(contact, 0));
        bM09Data.setWeight((float) ParseData.getData(3, 4, 5, contact));
        bM09Data.setAdc(ParseData.getDataInt(6, 7, contact));
        bM09Data.setTemp(((float) ParseData.getDataInt(8, 9, contact)) / 10.0f);
        if ((contact[10] >> 6) == 1) {
            z = true;
        }
        bM09Data.setStable(z);
        bM09Data.setAlgorithmType(ParseData.getDataInt((byte) (contact[10] & 63), contact[11]));
        bM09Data.setDid(ParseData.getDataInt(12, 13, contact));
        bM09Data.setBleVersion(getVersion(contact, 15, 2018));
        bM09Data.setBleType(ParseData.binaryToDecimal(contact[14]));
        bM09Data.setTimeMillis(System.currentTimeMillis());
        return bM09Data;
    }

    public static BM15Data getBm15Data(String str, byte[] bArr) {
        BM15Data bM15Data = new BM15Data();
        if (bArr != null && bArr.length > 11 && bArr[0] == -68) {
            bM15Data.setAddress(str);
            byte[] decrypt = decrypt(Arrays.copyOfRange(bArr, 3, 11), true);
            System.arraycopy(decrypt, 0, bArr, 3, decrypt.length);
            double binaryToDecimal = (double) ParseData.binaryToDecimal(bArr[1]);
            Double.isNaN(binaryToDecimal);
            bM15Data.setVersion(String.valueOf(ParseData.keepDecimal(binaryToDecimal / 10.0d, 1)));
            bM15Data.setAgreementType(bArr[2] >> 3);
            bM15Data.setUnitType(bArr[2] & 7);
            bM15Data.setWeight((float) (((bArr[3] & SYNC_HISTORY) << 8) + (bArr[4] & SYNC_HISTORY)));
            bM15Data.setAdc(ParseData.getDataInt(5, 6, bArr));
            float f = Float.MAX_VALUE;
            if (!(bArr[7] == -1 || bArr[8] == -1)) {
                f = ((float) (((bArr[7] & SYNC_HISTORY) << 8) + (bArr[8] & SYNC_HISTORY))) / 10.0f;
            }
            bM15Data.setTemp(f);
            bM15Data.setAlgorithmType(ParseData.binaryToDecimal(bArr[9]));
            bM15Data.setDid(ParseData.binaryToDecimal(bArr[10]));
            bM15Data.setBleType(ParseData.binaryToDecimal(BM_15));
        }
        return bM15Data;
    }
}
