package com.dhp.screening.lib.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.IntentFilter;

import java.math.BigInteger;

public class BleUtils {

    private static final char[] a = "0123456789ABCDEF".toCharArray();

    public static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        intentFilter.addAction(BluetoothDevice.EXTRA_BOND_STATE);
        return intentFilter;
    }

    public static String ByteArraytoHex(byte[] bytes) {
        if (bytes != null) {
            StringBuilder sb = new StringBuilder();
            for (byte b : bytes) {
                sb.append(String.format("%X ", b));
            }
            return sb.toString();
        }
        return "";
    }

    static int fromByteArray(byte[] bytes) {
        return new BigInteger(bytes).intValue();
    }

    static byte[] hexStringToByteArray(String paramString) {
        int    j           = paramString.length();
        byte[] arrayOfByte = new byte[j / 2];
        int    i           = 0;
        while (i < j) {
            arrayOfByte[(i / 2)] = ((byte) ((Character.digit(paramString.charAt(i), 16) << 4) + Character.digit(paramString.charAt(i + 1), 16)));
            i += 2;
        }
        return arrayOfByte;
    }

    public static String x1(byte[] paramArrayOfByte) {
        char[] arrayOfChar = new char[paramArrayOfByte.length * 2];
        int    i           = 0;
        while (i < paramArrayOfByte.length) {
            int j = paramArrayOfByte[i] & 0xFF;
            arrayOfChar[(i * 2)] = a[(j >>> 4)];
            arrayOfChar[(i * 2 + 1)] = a[(j & 0xF)];
            i += 1;
        }
        return new String(arrayOfChar);
    }

    public static int bytesToInt(byte[] paramArrayOfByte) {
        String x = bytesToHexString(paramArrayOfByte);
        try {
            int i = Integer.parseInt(x, 16);
            return i;
        } catch (NumberFormatException e) {
        }
        return 0;
    }

    public static String bytesToHexString(byte[] paramArrayOfByte) {
        StringBuilder localStringBuilder = new StringBuilder("");
        if ((paramArrayOfByte == null) || (paramArrayOfByte.length <= 0)) {
            return null;
        }
        int i = 0;
        while (i < paramArrayOfByte.length) {
            String str = Integer.toHexString(paramArrayOfByte[i] & 0xFF);
            if (str.length() < 2) {
                localStringBuilder.append(0);
            }
            localStringBuilder.append(str);
            i += 1;
        }
        return localStringBuilder.toString();
    }
}