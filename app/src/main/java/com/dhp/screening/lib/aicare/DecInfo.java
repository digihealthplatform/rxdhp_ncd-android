package com.dhp.screening.lib.aicare;

import android.os.Parcel;
import android.os.Parcelable;

public class DecInfo implements Parcelable {
    public static final Creator<DecInfo> CREATOR = new Creator<DecInfo>() {
        public DecInfo createFromParcel(Parcel parcel) {
            return new DecInfo(parcel);
        }

        public DecInfo[] newArray(int i) {
            return new DecInfo[i];
        }
    };
    private             int              kgDecimal;
    private             int              kgGraduation;
    private             int              lbDecimal;
    private             int              lbGraduation;
    private             int              sourceDecimal;
    private             int              stDecimal;

    public int describeContents() {
        return 0;
    }

    public DecInfo() {
    }

    public DecInfo(int i, int i2, int i3, int i4) {
        this.sourceDecimal = i;
        this.kgDecimal = i2;
        this.lbDecimal = i3;
        this.stDecimal = i4;
    }

    public DecInfo(int i, int i2, int i3, int i4, int i5, int i6) {
        this.sourceDecimal = i;
        this.kgDecimal = i2;
        this.lbDecimal = i3;
        this.stDecimal = i4;
        this.kgGraduation = i5;
        this.lbGraduation = i6;
    }

    protected DecInfo(Parcel parcel) {
        this.sourceDecimal = parcel.readInt();
        this.kgDecimal = parcel.readInt();
        this.lbDecimal = parcel.readInt();
        this.stDecimal = parcel.readInt();
        this.kgGraduation = parcel.readInt();
        this.lbGraduation = parcel.readInt();
    }

    public int getSourceDecimal() {
        return this.sourceDecimal;
    }

    public void setSourceDecimal(int i) {
        this.sourceDecimal = i;
    }

    public int getKgDecimal() {
        return this.kgDecimal;
    }

    public void setKgDecimal(int i) {
        this.kgDecimal = i;
    }

    public int getLbDecimal() {
        return this.lbDecimal;
    }

    public void setLbDecimal(int i) {
        this.lbDecimal = i;
    }

    public int getStDecimal() {
        return this.stDecimal;
    }

    public void setStDecimal(int i) {
        this.stDecimal = i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DecimalInfo{sourceDecimal=");
        sb.append(this.sourceDecimal);
        sb.append(", kgDecimal=");
        sb.append(this.kgDecimal);
        sb.append(", lbDecimal=");
        sb.append(this.lbDecimal);
        sb.append(", stDecimal=");
        sb.append(this.stDecimal);
        sb.append(", kgGraduation=");
        sb.append(this.kgGraduation);
        sb.append(", lbGraduation=");
        sb.append(this.lbGraduation);
        sb.append('}');
        return sb.toString();
    }

    public int getKgGraduation() {
        return this.kgGraduation;
    }

    public void setKgGraduation(int i) {
        this.kgGraduation = i;
    }

    public int getLbGraduation() {
        return this.lbGraduation;
    }

    public void setLbGraduation(int i) {
        this.lbGraduation = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.sourceDecimal);
        parcel.writeInt(this.kgDecimal);
        parcel.writeInt(this.lbDecimal);
        parcel.writeInt(this.stDecimal);
        parcel.writeInt(this.kgGraduation);
        parcel.writeInt(this.lbGraduation);
    }
}
