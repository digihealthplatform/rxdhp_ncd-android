package com.dhp.screening.lib.aicare;

public class BleInfo {
    private String address;
    private int    isCheck;
    private String name;
    private String version;

    public void setAddress(String str) {
        this.address = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setVersion(String str) {
        this.version = str;
    }

    public void setIsCheck(int i) {
        this.isCheck = i;
    }

    public String getAddress() {
        return this.address;
    }

    public String getName() {
        return this.name;
    }

    public String getVersion() {
        return this.version;
    }

    public int getIsCheck() {
        return this.isCheck;
    }
}
