package com.dhp.screening.lib.aicare;

import android.os.Parcel;
import android.os.Parcelable;

public class WeiData  {
    public static final Parcelable.Creator<WeiData> CREATOR = new Parcelable.Creator<WeiData>() {
        public WeiData createFromParcel(Parcel parcel) {
            return new WeiData(parcel);
        }

        public WeiData[] newArray(int i) {
            return new WeiData[i];
        }
    };
    private             int                         adc;
    private             int                         algorithmType;
    private             int                         cmdType;
    private             DecInfo                     decInfo;
    private             int                         deviceType;
    private             float                       temp    = Float.MAX_VALUE;
    private             int              unitType;
    private             float            weight;

    public int describeContents() {
        return 0;
    }

    public WeiData(int i, float f, float f2, DecInfo decInfo2) {
        this.cmdType = i;
        this.weight = f;
        this.temp = f2;
        this.decInfo = decInfo2;
    }

    public WeiData(int i, float f, float f2, DecInfo decInfo2, int i2, int i3, int i4, int i5) {
        this.cmdType = i;
        this.weight = f;
        this.temp = f2;
        this.decInfo = decInfo2;
        this.adc = i2;
        this.algorithmType = i3;
        this.unitType = i4;
        this.deviceType = i5;
    }

    public WeiData() {
    }

    protected WeiData(Parcel parcel) {
        this.cmdType = parcel.readInt();
        this.weight = parcel.readFloat();
        this.temp = parcel.readFloat();
        this.decInfo = (DecInfo) parcel.readParcelable(DecInfo.class.getClassLoader());
        this.adc = parcel.readInt();
        this.algorithmType = parcel.readInt();
        this.unitType = parcel.readInt();
        this.deviceType = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.cmdType);
        parcel.writeFloat(this.weight);
        parcel.writeFloat(this.temp);
        parcel.writeParcelable(this.decInfo, i);
        parcel.writeInt(this.adc);
        parcel.writeInt(this.algorithmType);
        parcel.writeInt(this.unitType);
        parcel.writeInt(this.deviceType);
    }

    public int getCmdType() {
        return this.cmdType;
    }

    public void setCmdType(int i) {
        this.cmdType = i;
    }

    public float getWeight() {
        return this.weight;
    }

    public float getTemp() {
        return this.temp;
    }

    public DecInfo getDecInfo() {
        return this.decInfo;
    }

    public void setDecInfo(DecInfo decInfo2) {
        this.decInfo = decInfo2;
    }

    public void setWeight(float f) {
        this.weight = f;
    }

    public void setTemp(float f) {
        this.temp = f;
    }

    public int getAdc() {
        return this.adc;
    }

    public void setAdc(int i) {
        this.adc = i;
    }

    public int getAlgorithmType() {
        return this.algorithmType;
    }

    public void setAlgorithmType(int i) {
        this.algorithmType = i;
    }

    public int getUnitType() {
        return this.unitType;
    }

    public void setUnitType(int i) {
        this.unitType = i;
    }

    public int getDeviceType() {
        return this.deviceType;
    }

    public void setDeviceType(int i) {
        this.deviceType = i;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WeiData)) {
            return false;
        }
        WeiData weiData = (WeiData) obj;
        if (this.cmdType != weiData.cmdType || Float.compare(weiData.weight, this.weight) != 0 || Float.compare(weiData.temp, this.temp) != 0 || this.adc != weiData.adc || this.algorithmType != weiData.algorithmType || this.unitType != weiData.unitType || this.deviceType != weiData.deviceType) {
            return false;
        }
        DecInfo decInfo2 = this.decInfo;
        if (decInfo2 != null) {
            z = decInfo2.equals(weiData.decInfo);
        } else if (weiData.decInfo != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int i = this.cmdType * 31;
        float f = this.weight;
        int i2 = 0;
        int floatToIntBits = (i + (f != 0.0f ? Float.floatToIntBits(f) : 0)) * 31;
        float f2 = this.temp;
        int floatToIntBits2 = (floatToIntBits + (f2 != 0.0f ? Float.floatToIntBits(f2) : 0)) * 31;
        DecInfo decInfo2 = this.decInfo;
        if (decInfo2 != null) {
            i2 = decInfo2.hashCode();
        }
        return ((((((((floatToIntBits2 + i2) * 31) + this.adc) * 31) + this.algorithmType) * 31) + this.unitType) * 31) + this.deviceType;
    }
}
