// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.dhp.screening.lib.bluetooth;

import android.os.Parcel;
import android.text.TextUtils;

import com.dhp.screening.util.LLog;

import java.text.DecimalFormat;
import java.util.regex.Pattern;

// Referenced classes of package com.getmedcheck.model:
//            IModelBloodTest

public class ModelBloodPressureData
        implements IModelBloodTest {

    private long   a;
    private String b;
    private String c;
    private String year;
    private String month;
    private String date;
    private String hour;
    private String minute;
    private String i;
    private String j;
    private String k;
    private String l;
    private String systolic;
    private String diastolic;

    public String getSystolic() {
        return systolic;
    }

    public void setSystolic(String systolic) {
        this.systolic = systolic;
    }

    public String getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(String diastolic) {
        this.diastolic = diastolic;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    private String pulse;
    private byte   p[];
    private String q;
    private String r;
    private long   s;

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    private long   dateTime;
    private String u;

    public ModelBloodPressureData() {
        b = "";
        c = "";
        year = "0";
        month = "0";
        date = "0";
        hour = "0";
        minute = "0";
        i = "";
        j = "";
        k = "";
        l = "";
        systolic = "0";
        diastolic = "0";
        pulse = "0";
        p = new byte[0];
        q = "";
        r = "";
        u = "";
    }

    public ModelBloodPressureData(gx gx1) {
        b = "";
        c = "";
        year = "0";
        month = "0";
        date = "0";
        hour = "0";
        minute = "0";
        i = "";
        j = "";
        k = "";
        l = "";
        systolic = "0";
        diastolic = "0";
        pulse = "0";
        p = new byte[0];
        q = "";
        r = "";
        u = "";
        systolic = gx1.b();
        diastolic = gx1.c();
        pulse = gx1.d();
        l = gx1.e();
        if (!TextUtils.isEmpty(gx1.f())) {
            dateTime = gx1.a("dd-MMM-yyyy hh:mm a", gx1.f());
        }
        if (!TextUtils.isEmpty(gx1.g())) {
            a = Long.parseLong(gx1.g());
        }
    }

    public ModelBloodPressureData(byte abyte0[], String s1) {
        b = "";
        c = "";
        year = "0";
        month = "0";
        date = "0";
        hour = "0";
        minute = "0";
        i = "";
        j = "";
        k = "";
        l = "";
        systolic = "0";
        diastolic = "0";
        pulse = "0";
        p = new byte[0];
        q = "";
        r = "";
        u = "";
        p = abyte0;
        r = qx.a(abyte0);
        q = qx.b(abyte0);
        j(s1);
    }


    private void j(String s) {
        int           i1 = k(s);
        DecimalFormat s1 = new DecimalFormat("00");
        year = (new StringBuilder()).append("").append(i1 + 2000 + Integer.parseInt(q.substring(0, 4), 2)).toString();

        try {
            int yearNum = Integer.parseInt(year) - 5;
            year = "" + yearNum;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        month = s1.format(Integer.parseInt(q.substring(4, 8), 2));

        date = s1.format(Integer.parseInt(q.substring(8, 16), 2));
        i = (new StringBuilder()).append("").append(Integer.parseInt(q.substring(16, 17), 2)).toString();
        l = (new StringBuilder()).append("").append(Integer.parseInt(q.substring(19, 20), 2)).toString();
        hour = s1.format(Integer.parseInt(q.substring(20, 24), 2));
        minute = s1.format(Integer.parseInt(q.substring(24, 32), 2));
        j = Integer.toHexString(Integer.parseInt(q.substring(32, 36), 2));
        k = Integer.toHexString(Integer.parseInt(q.substring(36, 40), 2));
        i1 = l(j);
        int j1 = l(k);
        systolic = Integer.toHexString(Integer.parseInt(q.substring(40, 48), 2));
        if (b1(systolic)) {
            systolic = String.valueOf(i1 * 100 + Integer.parseInt(systolic));
        }
        diastolic = Integer.toHexString(Integer.parseInt(q.substring(48, 56), 2));
        if (b1(diastolic)) {
            diastolic = String.valueOf(Integer.parseInt(diastolic) + j1 * 100);
        }
        pulse = (new StringBuilder()).append("").append(Integer.parseInt(q.substring(56, 64), 2)).toString();
        StringBuilder stringbuilder = (new StringBuilder()).append(date).append("-").append(month).append("-").append(year).append(" ").append(hour).append(":").append(minute).append(" ");

        if (i.equals("0")) {
            s = "AM";
        } else {
            s = "PM";
        }

        dateTime = gx.a("dd-MM-yyyy hh:mm a", stringbuilder.append(s).toString());
    }


    public static boolean b1(String s) {
        if (!TextUtils.isEmpty(s)) {
            return Pattern.compile("^(([0-9]*)|(([0-9]*)))$").matcher(s).matches();
        } else {
            return true;
        }
    }

    private int k(String s1) {
        int i1 = Integer.parseInt(s1.substring(2, 4), 16);
        return i1;
    }

    private int l(String s1) {
        int i1 = Integer.parseInt(s1);
        return i1;
    }

    public String a() {
        return "BPM";
    }

    public void a(long l1) {
        s = l1;
    }

    public void a(String s1) {
        b = s1;
    }

    public Object b() {
        return p();
    }

    public void b(long l1) {
        dateTime = l1;
    }

    public void b(String s1) {
        c = s1;
    }


    public void c(String s1) {
        l = s1;
    }

    public long d() {
        return a;
    }

    public void d(String s1) {
        systolic = s1;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return b;
    }

    public void e(String s1) {
        diastolic = s1;
    }

    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            obj = (ModelBloodPressureData) obj;
            if (l() != null) {
                return l().equals(((ModelBloodPressureData) (obj)).l());
            }
            if (((ModelBloodPressureData) (obj)).l() != null) {
                return false;
            }
        }
        return true;
    }

    public String f() {
        return c;
    }

    public void f(String s1) {
        pulse = s1;
    }

    public String g() {
        return l;
    }

    public void g(String s1) {
        q = s1;
    }

    public String h() {
        return systolic;
    }

    public void h(String s1) {
        r = s1;
    }

    public int hashCode() {
        int i1;
        if (l() != null) {
            i1 = l().hashCode();
        } else {
            i1 = 0;
        }
        return i1 + 31;
    }

    public String i() {
        return diastolic;
    }

    public void i(String s1) {
        u = s1;
    }

    public String j() {
        return pulse;
    }

    public String k() {
        return q;
    }

    public String l() {
        return r;
    }

    public long m() {
        return s;
    }

    public long n() {
        return dateTime;
    }

    public String o() {
        return u;
    }

    public ModelBloodPressureData p() {
        return this;
    }

    public String toString() {
        return (new StringBuilder()).append("[hex=").append(r).append("year=").append(year).append(", month=").append(month).append(", day=").append(date).append(", hours=").append(hour).append(", minute=").append(minute).append(", amPm='").append(i).append('\'').append(", systolicFlag=").append(j).append(", diastolicFlag=").append(k).append(", systolic=").append(systolic).append(", diastolic=").append(diastolic).append(", heartRate=").append(pulse).append(']').toString();
    }

    public void writeToParcel(Parcel parcel, int i1) {
        parcel.writeLong(a);
        parcel.writeString(b);
        parcel.writeString(c);
        parcel.writeString(year);
        parcel.writeString(month);
        parcel.writeString(date);
        parcel.writeString(hour);
        parcel.writeString(minute);
        parcel.writeString(i);
        parcel.writeString(j);
        parcel.writeString(k);
        parcel.writeString(l);
        parcel.writeString(systolic);
        parcel.writeString(diastolic);
        parcel.writeString(pulse);
        parcel.writeByteArray(p);
        parcel.writeString(q);
        parcel.writeString(r);
        parcel.writeLong(s);
        parcel.writeLong(dateTime);
        parcel.writeString(u);
    }

}
