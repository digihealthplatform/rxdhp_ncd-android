package com.dhp.screening.lib.aicare;

public class BM09Data {
    private int     adc;
    private String  address;
    private int     agreementType;
    private int     algorithmType;
    private int     bleType;
    private String  bleVersion;
    private DecInfo decInfo;
    private int     did;
    private boolean isStable;
    private float   temp;
    private long    timeMillis;
    private int     unitType;
    private float   weight;

    public BM09Data(int i, int i2, DecInfo decInfo2, float f, int i3, float f2, int i4, int i5, String str, int i6, String str2, boolean z) {
        this.agreementType = i;
        this.unitType = i2;
        this.decInfo = decInfo2;
        this.weight = f;
        this.adc = i3;
        this.temp = f2;
        this.algorithmType = i4;
        this.did = i5;
        this.bleVersion = str;
        this.bleType = i6;
        this.address = str2;
        this.isStable = z;
    }

    public BM09Data() {
    }

    public int getAgreementType() {
        return this.agreementType;
    }

    public void setAgreementType(int i) {
        this.agreementType = i;
    }

    public int getUnitType() {
        return this.unitType;
    }

    public void setUnitType(int i) {
        this.unitType = i;
    }

    public DecInfo getDecInfo() {
        return this.decInfo;
    }

    public void setDecInfo(DecInfo decInfo2) {
        this.decInfo = decInfo2;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(float f) {
        this.weight = f;
    }

    public int getAdc() {
        return this.adc;
    }

    public void setAdc(int i) {
        this.adc = i;
    }

    public float getTemp() {
        return this.temp;
    }

    public void setTemp(float f) {
        this.temp = f;
    }

    public int getAlgorithmType() {
        return this.algorithmType;
    }

    public void setAlgorithmType(int i) {
        this.algorithmType = i;
    }

    public int getDid() {
        return this.did;
    }

    public void setDid(int i) {
        this.did = i;
    }

    public String getBleVersion() {
        return this.bleVersion;
    }

    public void setBleVersion(String str) {
        this.bleVersion = str;
    }

    public int getBleType() {
        return this.bleType;
    }

    public void setBleType(int i) {
        this.bleType = i;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public long getTimeMillis() {
        return this.timeMillis;
    }

    public void setTimeMillis(long j) {
        this.timeMillis = j;
    }

    public boolean isStable() {
        return this.isStable;
    }

    public void setStable(boolean z) {
        this.isStable = z;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("广播数据{协议类型=");
        sb.append(this.agreementType);
        sb.append(", 当前单位=");
        sb.append(this.unitType);
        sb.append(", 小数点位数=");
        sb.append(this.decInfo);
        sb.append(", 重量=");
        sb.append(AicareBleConfig.getWeight((double) this.weight, (byte) this.unitType, this.decInfo));
        sb.append(", 阻抗=");
        sb.append(this.adc);
        sb.append(", 温度=");
        sb.append(this.temp);
        sb.append(", 算法序列=");
        sb.append(this.algorithmType);
        sb.append(", did=");
        sb.append(this.did);
        sb.append(", bleType=");
        sb.append(this.bleType);
        sb.append(", 蓝牙版本='");
        sb.append(this.bleVersion);
        sb.append('\'');
        sb.append(", isStable=");
        sb.append(this.isStable);
        sb.append('}');
        return sb.toString();
    }
}
