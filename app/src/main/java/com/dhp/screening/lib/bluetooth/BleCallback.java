package com.dhp.screening.lib.bluetooth;

import android.bluetooth.BluetoothDevice;

public interface BleCallback {

    void onBleScan(BluetoothDevice device);

    void onConnected();

    void onServicesDiscovered();

    void onDisconnected();

    void onValueRead(int type, String values);

    void onFailure();
}