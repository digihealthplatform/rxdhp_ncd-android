package com.dhp.screening.lib.aicare;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Arrays;

public class BroadData implements Parcelable {
    public static final Creator<BroadData> CREATOR = new Creator<BroadData>() {
        public BroadData createFromParcel(Parcel parcel) {
            return new BroadData(parcel);
        }

        public BroadData[] newArray(int i) {
            return new BroadData[i];
        }
    };
    private             String             address;
    private             int                deviceType;
    private             boolean            isBright;
    private             String             name;
    private             int                rssi;
    private             byte[]             specificData;

    public int describeContents() {
        return 0;
    }

    public BroadData() {
    }

    protected BroadData(Parcel parcel) {
        this.name = parcel.readString();
        this.address = parcel.readString();
        this.isBright = parcel.readByte() != 0;
        this.rssi = parcel.readInt();
        this.specificData = parcel.createByteArray();
        this.deviceType = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.address);
        parcel.writeByte(this.isBright ? (byte) 1 : 0);
        parcel.writeInt(this.rssi);
        parcel.writeByteArray(this.specificData);
        parcel.writeInt(this.deviceType);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public boolean isBright() {
        return this.isBright;
    }

    public void setBright(boolean z) {
        this.isBright = z;
    }

    public int getRssi() {
        return this.rssi;
    }

    public void setRssi(int i) {
        this.rssi = i;
    }

    public byte[] getSpecificData() {
        return this.specificData;
    }

    public void setSpecificData(byte[] bArr) {
        this.specificData = bArr;
    }

    public int getDeviceType() {
        return this.deviceType;
    }

    public void setDeviceType(int i) {
        this.deviceType = i;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BroadData)) {
            return super.equals(obj);
        }
        return this.address.equals(((BroadData) obj).getAddress());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BroadData{name='");
        sb.append(this.name);
        sb.append('\'');
        sb.append(", address='");
        sb.append(this.address);
        sb.append('\'');
        sb.append(", isBright=");
        sb.append(this.isBright);
        sb.append(", rssi=");
        sb.append(this.rssi);
        sb.append(", specificData=");
        sb.append(Arrays.toString(this.specificData));
        sb.append(", deviceType=");
        sb.append(this.deviceType);
        sb.append('}');
        return sb.toString();
    }
}
