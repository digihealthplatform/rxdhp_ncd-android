package com.dhp.screening.lib.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.content.Context;

public interface BleConnectionInterface {

    void init(Context context, BleCallback callback);

    void startScan();

    void stopScan();

    void connectDevice(BluetoothDevice device);

    void disconnect();

    void getBatteryLevel();

    void startTest();

    void stopTest();

    void deInit();
}