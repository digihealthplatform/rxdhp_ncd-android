package com.dhp.screening.lib.printer;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

import com.dhp.screening.LApplication;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemPrescriptionDetails;
import com.dhp.screening.data.item.ItemSummaryPrint;
import com.dhp.screening.data.item.ItemVitalHeader;
import com.dhp.screening.data.range.ItemRangeCount;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.example.hoinprinterlib.HoinPrinter;
import com.example.hoinprinterlib.module.PrinterCallback;
import com.example.hoinprinterlib.module.PrinterEvent;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_CUSTOM_TEXT_TO_PRINT;
import static com.dhp.screening.util.LUtils.getFormattedDateTimeAm;
import static com.dhp.screening.util.LUtils.hasValue;

public class PrinterManager implements PrinterCallback {

    private static final String TAG = "PrinterManager";

    private int readBufferPosition;

    private byte   FONT_TYPE;
    private byte[] readBuffer;

    private volatile boolean stopWorker;

    private OutputStream outputStream;
    private InputStream  inputStream;
    private Thread       workerThread;

    private BluetoothSocket  socket;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice  device;

    private static final int PRINT_REG_ID        = 11;
    private static final int PRINT_ALL_VITALS    = 12;
    private static final int PRINT_VITAL_DETAILS = 13;
    private static final int PRINT_SUMMARY       = 14;

    private HoinPrinter hoinPrinter;
    private int         printAction;

    private String                 name;
    private String                 regId;
    private DataManager            dataManager;
    private List<ItemSummaryPrint> summaryList;
    private List<TableVitals>      vitalsList;

    private long startDateLong;
    private long endDateLong;

    private Set<BluetoothDevice> pairedDevices;

    public void printVitalDetails(final DataManager dataManager, final String regId, final List<TableVitals> vitalsList) {

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                findBT(dataManager);

                if (null != device && device.getName().equals("BlueTooth Printer")) {
                    printAction                     = PRINT_VITAL_DETAILS;
                    PrinterManager.this.dataManager = dataManager;
                    PrinterManager.this.regId       = regId;
                    PrinterManager.this.vitalsList  = vitalsList;

                    hoinPrinter = HoinPrinter.getInstance(LApplication.getContext(), 1, PrinterManager.this);
                    hoinPrinter.connect(device.getAddress());

                } else {
                    try {
                        openBT();
                        sendData(dataManager, regId, vitalsList);
                        printCustomText(dataManager);
                        closeBT();
                    } catch (Exception e) {
                        LToast.errorLong("Printer not connected. Please connect the printer and try again");
                        LLog.printStackTrace(e);
                        try {
                            closeBT();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    public void printSummary(final DataManager dataManager, final List<ItemSummaryPrint> summaryList) {

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                findBT(dataManager);

                if (null != device && device.getName().equals("BlueTooth Printer")) {
                    printAction                     = PRINT_SUMMARY;
                    PrinterManager.this.dataManager = dataManager;
                    PrinterManager.this.summaryList = summaryList;

                    hoinPrinter = HoinPrinter.getInstance(LApplication.getContext(), 1, PrinterManager.this);
                    hoinPrinter.connect(device.getAddress());

                } else {
                    try {
                        openBT();
                        sendSummary(dataManager, summaryList);
                        closeBT();
                    } catch (Exception e) {
                        LToast.errorLong("Printer not connected. Please connect the printer and try again");
                        LLog.printStackTrace(e);
                        try {
                            closeBT();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void sendSummary(DataManager dataManager, final List<ItemSummaryPrint> summaryList) throws Exception {
        byte[] arrayOfByte1 = {27, 33, 0};
        byte[] format       = {27, 33, 0};

        byte[] left   = new byte[]{0x1b, 0x61, 0x00};
        byte[] center = new byte[]{0x1b, 0x61, 0x01};
        byte[] right  = new byte[]{0x1b, 0x61, 0x02};

        //------------------------------- FORMAT 1 --------------------------------//

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df            = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String           formattedDate = df.format(c.getTime());

        String date_time = "Date & Time " + formattedDate + "\n";

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(date_time.getBytes(), 0, date_time.getBytes().length);

        OutputStream opstream = null;

        try {
            opstream = socket.getOutputStream();
        } catch (IOException e) {
            LLog.printStackTrace(e);
        }

        outputStream = opstream;

        try {
            outputStream = socket.getOutputStream();
            byte[] printformat = {0x1B, 0 * 21, FONT_TYPE};
            outputStream.write(printformat);

        } catch (IOException e) {
            LLog.printStackTrace(e);
        }

        //------------------------------- FORMAT 2 --------------------------------//

        byte[] format2 = {27, 33, 0};
        format2[2] = (byte) (((byte) (0x80 | arrayOfByte1[2])) + ((byte) (0x8 | arrayOfByte1[2])) + ((byte) (0x20 | arrayOfByte1[2])));

        byte[] format3 = {27, 33, 0};
        format3[2] = (byte) (((byte) (0x77 | arrayOfByte1[2])) + ((byte) (0x8 | arrayOfByte1[2])) + ((byte) (0x22 | arrayOfByte1[2])));

        TableCampInfo campInfo = dataManager.getCampInfo();

        String heading = "";

        try {
            String campName = campInfo.getCampName();

            if (!campName.isEmpty()) {
                heading = "Camp name: ";

                outputStream.write(left);
                outputStream.write(format);
                outputStream.write(heading.getBytes(), 0, heading.getBytes().length);
                outputStream.flush();

                heading = trim(campName, 14);

                heading += "\n";
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        outputStream.write(left);
        outputStream.write(format3);
        outputStream.write(heading.getBytes(), 0, heading.getBytes().length);

        heading = "\nSummary\n\n";

        outputStream.write(center);
        outputStream.write(format3);
        outputStream.write(heading.getBytes(), 0, heading.getBytes().length);

        try {
            outputStream = socket.getOutputStream();
            byte[] printformat = {0x1B, 0 * 21, FONT_TYPE};
            outputStream.write(printformat);

        } catch (IOException e) {
            LLog.printStackTrace(e);
        }

        String data = "";

        for (ItemSummaryPrint itemSummaryPrint : summaryList) {

            if (!data.isEmpty()) {
                data += "--------------------------------\n";
            }

            if (!itemSummaryPrint.getVital().isEmpty()) {
                data += itemSummaryPrint.getVital() + "\n";
            }

            for (ItemRangeCount rangeCount : itemSummaryPrint.getRanges()) {
                data += rangeCount.getRange() + " : " + rangeCount.getCount() + "\n";
            }
        }

        data += "\n\n\n";

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(data.getBytes(), 0, data.getBytes().length);
        outputStream.flush();
    }

    public void printAllVitals(final DataManager dataManager, String startDate, String endDate) {
        final long startDateLong = LUtils.getLongTime(startDate);
        final long endDateLong   = LUtils.getLongTime(endDate);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                findBT(dataManager);

                if (null != device && device.getName().equals("BlueTooth Printer")) {
                    printAction                       = PRINT_ALL_VITALS;
                    PrinterManager.this.dataManager   = dataManager;
                    PrinterManager.this.startDateLong = startDateLong;
                    PrinterManager.this.endDateLong   = endDateLong;

                    hoinPrinter = HoinPrinter.getInstance(LApplication.getContext(), 1, PrinterManager.this);
                    hoinPrinter.connect(device.getAddress());

                } else {
                    try {
                        openBT();
                        printAllVitals();
                        closeBT();

                    } catch (Exception e) {
                        LToast.errorLong("Printer not connected. Please connect the printer and try again");
                        LLog.printStackTrace(e);

                        try {
                            closeBT();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void printAllVitals() throws Exception {

        boolean dataAvailable = false;

        List<TableDemographics> tableDemographicsList = dataManager.getAllPatients();

        for (TableDemographics tableDemographics : tableDemographicsList) {
            if (tableDemographics.isInactive()) {
                continue;
            }

            List<ItemVitalHeader> vitalHeaders = dataManager.getAllVitalHeaders(tableDemographics.getRegistrationId());

            List<TableVitals> vitalsList = new ArrayList<>();

            for (ItemVitalHeader vitalHeader : vitalHeaders) {

                if (LUtils.getLongTime(vitalHeader.getDate()) >= startDateLong
                        && LUtils.getLongTime(vitalHeader.getDate()) <= endDateLong) {
                    vitalsList.addAll(vitalHeader.getVitals());
                }
            }

            if (0 < vitalsList.size()) {
                dataAvailable = true;
                sendData(dataManager, tableDemographics.getRegistrationId(), vitalsList);
                printCustomText(dataManager);
            }
        }

        if (!dataAvailable) {
            LToast.warningLong("No data to print");
        }
    }

    public void printRegId(final DataManager dataManager, final String regId, final String name) {

        new Handler().post(new Runnable() {

            @Override
            public void run() {
                findBT(dataManager);

                if (null != device && device.getName().equals("BlueTooth Printer")) {
                    printAction                     = PRINT_REG_ID;
                    PrinterManager.this.dataManager = dataManager;
                    PrinterManager.this.regId       = regId;
                    PrinterManager.this.name        = name;

                    hoinPrinter = HoinPrinter.getInstance(LApplication.getContext(), 1, PrinterManager.this);
                    hoinPrinter.connect(device.getAddress());

                } else {
                    try {
                        openBT();
                        printId(dataManager, regId, name);
                        closeBT();
                    } catch (Exception e) {
                        LToast.errorLong("Printer not connected. Please connect the printer and try again");
                        LLog.printStackTrace(e);
                        try {
                            closeBT();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }
        });
    }

    private void findBT(DataManager dataManager) {
        try {
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (!bluetoothAdapter.isEnabled()) {
                bluetoothAdapter.enable();
                Thread.sleep(1500);
            }

            pairedDevices = new HashSet<>();
            pairedDevices.addAll(bluetoothAdapter.getBondedDevices());

            PrinterManager.this.dataManager = dataManager;

            findBtDevice();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    private void findBtDevice() {
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                ArrayList<ItemBluetoothDevice> itemBluetoothDevices = dataManager.getBluetoothDevices("printer");

                if (null != itemBluetoothDevices) {
                    if (LUtils.toConnectDevicePrinter(device.getName(), device.getAddress(), itemBluetoothDevices)) {
                        this.device = device;
                        break;
                    }
                } else if (device.getName().startsWith("MTP-II")
                        || device.getName().startsWith("MPT-II")) {
                    this.device = device;
                    break;
                }
            }
        }
    }

    private void openBT() {
        while (null != device && null != pairedDevices && !pairedDevices.isEmpty()) {
            try {
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
                socket = device.createRfcommSocketToServiceRecord(uuid);
                socket.connect();

                outputStream = socket.getOutputStream();
                inputStream  = socket.getInputStream();

                beginListenForData();
                break;

            } catch (Exception e) {
                pairedDevices.remove(device);
                device = null;
                findBtDevice();
                LLog.printStackTrace(e);
            }
        }
    }

    private void beginListenForData() {
        try {
            final byte delimiter = 10;

            stopWorker         = false;
            readBufferPosition = 0;
            readBuffer         = new byte[1024];

            workerThread = new Thread(new Runnable() {

                public void run() {
                    while (!Thread.currentThread().isInterrupted() && !stopWorker) {
                        try {
                            int bytesAvailable = inputStream.available();

                            if (bytesAvailable > 0) {

                                byte[] packetBytes = new byte[bytesAvailable];
                                inputStream.read(packetBytes);

                                for (int i = 0; i < bytesAvailable; i++) {

                                    byte b = packetBytes[i];
                                    if (b == delimiter) {

                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(
                                                readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length
                                        );

                                        final String data = new String(encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;

                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }

                        } catch (IOException ex) {
                            stopWorker = true;
                        }
                    }
                }
            });
            workerThread.start();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }


    private void printId(DataManager dataManager, String regId, String name) throws Exception {
        byte[] arrayOfByte1 = {27, 33, 0};
        byte[] format       = {27, 33, 0};

        byte[] left   = new byte[]{0x1b, 0x61, 0x00};
        byte[] center = new byte[]{0x1b, 0x61, 0x01};
        byte[] right  = new byte[]{0x1b, 0x61, 0x02};

        //------------------------------- FORMAT 1 --------------------------------//

        byte[] printformat = {0x1B, 0 * 21, FONT_TYPE};
        outputStream.write(printformat);
        outputStream.flush();

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df            = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String           formattedDate = df.format(c.getTime());

        String date_time = "Date & Time " + formattedDate + "\n";

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(date_time.getBytes(), 0, date_time.getBytes().length);

        //------------------------------- FORMAT 2 --------------------------------//

        byte[] format2 = {27, 33, 0};
        format2[2] = (byte) (((byte) (0x80 | arrayOfByte1[2])) + ((byte) (0x8 | arrayOfByte1[2])) + ((byte) (0x20 | arrayOfByte1[2])));

        byte[] format3 = {27, 33, 0};
        format3[2] = (byte) (((byte) (0x77 | arrayOfByte1[2])) + ((byte) (0x8 | arrayOfByte1[2])) + ((byte) (0x22 | arrayOfByte1[2])));

        TableCampInfo campInfo = dataManager.getCampInfo();

        String heading = "";

        try {
            String campName = campInfo.getCampName();

            if (!campName.isEmpty()) {
                heading = "Camp name: ";

                outputStream.write(left);
                outputStream.write(format);
                outputStream.write(heading.getBytes(), 0, heading.getBytes().length);
                outputStream.flush();

                heading = trim(campName, 14);
                heading += "\n";

            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        heading += "\n";

        outputStream.write(left);
        outputStream.write(format3);
        outputStream.write(heading.getBytes(), 0, heading.getBytes().length);

        try {
            String text = regId;

            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();

            BitMatrix      bitMatrix      = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap         bitmap         = barcodeEncoder.createBitmap(bitMatrix);

            if (bitmap != null) {
                byte[] command = PrinterUtils.decodeBitmap(bitmap);
                outputStream.write(new byte[]{0x1b, 'a', 0x01});
                outputStream.write(command);
                Thread.sleep(800);
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        String data = "Name: " + trim(name, 25);
        printUnicodeText(data);
        Thread.sleep(800);
        outputStream.flush();
        data = "";

        try {
            int age = LUtils.calculateAge(dataManager.getPatient(regId).getYob());

            if (age > 0) {
                data += "Age : " + age + "\n";
            }
        } catch (Exception e) {
        }

        data += "Reg.Id:\n";
        data += regId;
        data += "\n\n\n\n";

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(data.getBytes(), 0, data.getBytes().length);
        outputStream.flush();

//        outputStream.write(left);
//        outputStream.write(format3);
//        outputStream.write(data.getBytes(), 0, data.getBytes().length);
//        outputStream.flush();
    }


    private boolean printUnicodeText(String paramString) {
        TextPaint localTextPaint = new TextPaint();
        localTextPaint.setColor(-16777216);
        int i1 = 22;
        localTextPaint.setTextSize(i1);
        return printUnicodeText(paramString, Layout.Alignment.ALIGN_NORMAL, localTextPaint);
    }

    private int BtpLineWidth = 384;

    private boolean printUnicodeText(String paramString, Layout.Alignment paramAlignment, TextPaint paramTextPaint) {
        Bitmap localBitmap = a(LApplication.getContext(), paramString, paramAlignment, paramTextPaint);
        if (localBitmap == null) {
            return false;
        }
        int    i1          = localBitmap.getWidth();
        int    i2          = localBitmap.getHeight();
        byte[] arrayOfByte = a(localBitmap);
        localBitmap.recycle();
        return c(i1, i2, arrayOfByte);
    }

    private boolean c(int paramInt1, int paramInt2, byte[] paramArrayOfByte) {

        byte[] arrayOfByte = c(paramArrayOfByte, paramInt1, paramInt2, 0);

        try {
            outputStream.write(arrayOfByte, 0, arrayOfByte.length);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    private byte[] c(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3) {
        int    i1          = paramInt1 / 8;
        byte[] arrayOfByte = new byte[paramInt2 * (8 + i1)];
        int    i2          = 0;
        int    i3          = 0;
        for (int i4 = 0; i4 < paramInt2; i4++) {
            i2                    = i4 * (8 + i1);
            arrayOfByte[i2]       = 29;
            arrayOfByte[(i2 + 1)] = 118;
            arrayOfByte[(i2 + 2)] = 48;
            arrayOfByte[(i2 + 3)] = ((byte) (paramInt3 & 0x1));
            arrayOfByte[(i2 + 4)] = ((byte) (i1 % 256));
            arrayOfByte[(i2 + 5)] = ((byte) (i1 / 256));
            arrayOfByte[(i2 + 6)] = 1;
            arrayOfByte[(i2 + 7)] = 0;
            for (int i5 = 0; i5 < i1; i5++) {
                arrayOfByte[(i2 + 8 + i5)] = paramArrayOfByte[i3];
                i3++;
            }
        }
        return arrayOfByte;
    }

    protected Bitmap a(Context paramContext, String paramString, Layout.Alignment paramAlignment, TextPaint paramTextPaint) {
        TextPaint localTextPaint = new TextPaint();
        if (paramTextPaint != null) {
            localTextPaint.set(paramTextPaint);
        } else {
            localTextPaint.setColor(-16777216);
            localTextPaint.setTextSize(16.0F);
        }
        StaticLayout  localStaticLayout = new StaticLayout(paramString, localTextPaint, BtpLineWidth, paramAlignment, 1.0F, 1.0F, true);
        int           i                 = localStaticLayout.getHeight();
        Bitmap.Config localConfig       = Bitmap.Config.ARGB_8888;
        Bitmap        localBitmap       = Bitmap.createBitmap(BtpLineWidth, i, localConfig);
        Canvas        localCanvas       = new Canvas(localBitmap);
        localCanvas.drawColor(-1);
        localStaticLayout.draw(localCanvas);
        return localBitmap;
    }

    protected byte[] a(Bitmap paramBitmap) {
        int    m           = paramBitmap.getWidth();
        int    n           = paramBitmap.getHeight();
        int    i1          = m * n / 8;
        byte[] arrayOfByte = new byte[i1];
        byte   b           = 0;
        int    i2          = 7;
        int    i3          = 0;
        int[]  arrayOfInt  = new int[m * n];
        paramBitmap.getPixels(arrayOfInt, 0, m, 0, 0, m, n);
        for (int i4 = 0; i4 < arrayOfInt.length; i4++) {
            int k = Color.red(arrayOfInt[i4]);
            int j = Color.green(arrayOfInt[i4]);
            int i = Color.blue(arrayOfInt[i4]);
            k = (int) (0.21D * k + 0.71D * j + 0.07D * i);
            if (k == 0) {
                b = a(b, i2);
            }
            if (i2 == 0) {
                i2                  = 7;
                arrayOfByte[(i3++)] = b;
                b                   = 0;
            } else {
                i2--;
            }
        }
        return arrayOfByte;
    }

    private byte a(byte paramByte, int paramInt) {
        return (byte) (paramByte | 1 << paramInt);
    }

    private String trim(String name, int length) {
        String nameTrimmed = name;

        try {
            nameTrimmed = name.substring(0, length);
        } catch (Exception e) {

        }

        return nameTrimmed;
    }

    private HashMap<String, Integer> priority = new HashMap<>();

    private void sendData(DataManager dataManager, String registrationId, List<TableVitals> vitalsList) throws Exception {

        priority.put("Height", 1);
        priority.put("Weight", 2);
        priority.put("Blood Pressure", 3);
        priority.put("Pulse", 4);
        priority.put("Blood Sugar", 5);
        priority.put("Hemoglobin", 6);
        priority.put("Albumin", 7);
        priority.put("Blood Group", 8);
        priority.put("Prescription", 9);
        priority.put("Malaria", 10);
        priority.put("Dengue", 11);
        priority.put("Syphilis", 12);
        priority.put("HIV-AIDS", 13);
        priority.put("Pregnancy", 14);
        priority.put("Heart Attack", 15);

        byte[] arrayOfByte1 = {27, 33, 0};
        byte[] format       = {27, 33, 0};

        byte[] left   = new byte[]{0x1b, 0x61, 0x00};
        byte[] center = new byte[]{0x1b, 0x61, 0x01};

        //------------------------------- FORMAT 1 --------------------------------//

        Collections.sort(vitalsList, new Comparator<TableVitals>() {
            @Override
            public int compare(TableVitals left, TableVitals right) {
                int val = 0;

                try {
                    val = priority.get(left.getVitalType()) - priority.get(right.getVitalType());
                } catch (Exception e) {
                    LLog.printStackTrace(e);
                }
                return val;
            }
        });

        byte[] printformat = {0x1B, 0 * 21, FONT_TYPE};
        outputStream.write(printformat);
        outputStream.flush();

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df            = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String           formattedDate = df.format(c.getTime());

        String date_time = "Date & Time " + formattedDate + "\n";

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(date_time.getBytes(), 0, date_time.getBytes().length);

        //------------------------------- FORMAT 2 --------------------------------//

        byte[] format2 = {27, 33, 0};
        format2[2] = (byte) (((byte) (0x80 | arrayOfByte1[2])) + ((byte) (0x8 | arrayOfByte1[2])) + ((byte) (0x20 | arrayOfByte1[2])));

        byte[] format3 = {27, 33, 0};
        format3[2] = (byte) (((byte) (0x77 | arrayOfByte1[2])) + ((byte) (0x8 | arrayOfByte1[2])) + ((byte) (0x22 | arrayOfByte1[2])));

        TableCampInfo campInfo = dataManager.getCampInfo();

        String heading = "";

        try {
            String campName = campInfo.getCampName();

            if (!campName.isEmpty()) {
                heading = "Camp name: ";

                outputStream.write(left);
                outputStream.write(format);
                outputStream.write(heading.getBytes(), 0, heading.getBytes().length);
                outputStream.flush();

                heading = trim(campName, 14);
                heading += "\n";

            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        heading += "\n";

        outputStream.write(left);
        outputStream.write(format3);
        outputStream.write(heading.getBytes(), 0, heading.getBytes().length);

        outputStream.write(printformat);

//        try {
//            String text = registrationId;
//
//            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
//
//            BitMatrix      bitMatrix      = multiFormatWriter.encode(text, BarcodeFormat.QR_CODE, 200, 200);
//            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
//            Bitmap         bitmap         = barcodeEncoder.createBitmap(bitMatrix);
//
//            if (bitmap != null) {
//                byte[] command = PrinterUtils.decodeBitmap(bitmap);
//                outputStream.write(new byte[]{0x1b, 'a', 0x01});
//                outputStream.write(command);
//            }
//
//        } catch (Exception e) {
//            LLog.printStackTrace(e);
//        }

        String data = "";
        try {
            String name = dataManager.getPatient(registrationId).getFullName();
            data += "Name: " + trim(name, 25);

            printUnicodeText(data);
            Thread.sleep(800);
            outputStream.flush();

            data = "";

            int age = LUtils.calculateAge(dataManager.getPatient(registrationId).getYob());

            if (age > 0) {
                data += "Age : " + age + "\n";
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        data += "Reg.Id:\n";
        data += registrationId;
        data += "\n\n";

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(data.getBytes(), 0, data.getBytes().length);
        outputStream.flush();

//        outputStream.write(left);
//        outputStream.write(format3);
//        outputStream.write(data.getBytes(), 0, data.getBytes().length);

        data = "";

        for (TableVitals vitals : vitalsList) {
            if (vitals.isInactive()) {
                continue;
            }

            switch (vitals.getVitalType()) {
                case "Height": {
                    data += "Height : " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Weight": {
                    data += "Weight : " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";

                    String latestHeight = dataManager.getLatestHeight(StaticData.getRegistrationId(), vitals.getVitalDateTimeMillis());

                    if (null != latestHeight) {
                        data += "BMI    : ";

                        String[] height = latestHeight.split(";");

                        data += LUtils.findBmi(Float.parseFloat(height[0]),
                                height[1],
                                Float.parseFloat(vitals.getVitalValues()))
                                + " kg/m2 ";

                        data += "\n";
                    }

                    break;
                }
                case "Blood Pressure": {

                    String[] values = vitals.getVitalValues().split(";");
                    String[] units  = vitals.getVitalUnits().split(";");

                    data += "BP     : " + values[0] + " / " + values[1] + " " + units[0] + "\n";

                    String pulse = values[2];

                    if (!pulse.equals("0")) {
                        data += "Pulse  : " + values[2] + " " + units[2] + "\n";
                    }

                    break;
                }

                case "Pulse": {
                    String[] values = vitals.getVitalValues().split(";");
                    String[] units  = vitals.getVitalUnits().split(";");

                    data += "SpO2   : " + values[0] + " " + units[0] + " ,    ";

                    if (!values[2].equals("0")) {
                        data += "PI : " + values[2] + " " + units[2] + "\n";
                    }

                    data += "PR     : " + values[1] + " " + units[1] + "\n";

                    break;
                }

                case "Blood Sugar": {

                    switch (vitals.getVitalSubTypes()) {
                        case "RBS": {
                            data += "RBS    : ";
                            break;
                        }

                        case "FBS": {
                            data += "FBS    : ";
                            break;
                        }

                        case "PPBS": {
                            data += "PPBS   : ";
                            break;
                        }
                    }

                    data += vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }
                case "Hemoglobin": {
                    data += "Hemoglobin: " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Albumin": {
                    data += "Albumin: " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Blood Group": {
                    data += "Blood Group: " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Prescription": {
                    ItemPrescriptionDetails prescriptionDetails = LUtils.getPrescriptionDetails(vitals.getVitalValues());

                    String prescription = prescriptionDetails.getPrescription();
                    String timing       = prescriptionDetails.getTimings();
                    String food         = prescriptionDetails.getFood();

                    timing = timing.replaceAll(",", ", ");
                    String foodShort = food.equals("Before food") ? "BF" : "AF";

                    String timingBinary = "";
                    if (timing.contains("Morning")) {
                        timingBinary += "1-";
                    } else {
                        timingBinary += "0-";
                    }

                    if (timing.contains("Afternoon")) {
                        timingBinary += "1-";
                    } else {
                        timingBinary += "0-";
                    }

                    if (timing.contains("Night")) {
                        timingBinary += "1";
                    } else {
                        timingBinary += "0";
                    }

                    data += "Prescription: \n" + prescription;

                    String prescriptionExtras = " (" + LUtils.getDaysText(prescriptionDetails.getDays()) + "," + timingBinary + "," + foodShort + ")\n";

                    if (((prescription.length() % 32)) > 0 && (32 - (prescription.length() % 32)) > prescriptionExtras.length()) {
                        data += prescriptionExtras;
                    } else {
                        data += "\n" + fillSpaces(prescriptionExtras);
                    }
                    break;
                }

                case "Malaria":
                case "Dengue":
                case "Syphilis":
                case "HIV-AIDS":
                case "Pregnancy":
                case "Heart Attack": {
                    data += vitals.getVitalType() + ": " + vitals.getVitalValues() + "\n";
                    break;
                }
            }

            data += "         " + getFormattedDateTimeAm(vitals.getVitalDateTimeMillis()) + "\n\n";
        }

        data += "";

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(data.getBytes(), 0, data.getBytes().length);
        outputStream.flush();
    }

    private void printCustomText(DataManager dataManager) throws Exception {
        byte[] format = {27, 33, 0};
        byte[] left   = new byte[]{0x1b, 0x61, 0x00};

        String customText = dataManager.getStringPref(KEY_CUSTOM_TEXT_TO_PRINT, "");

        if (hasValue(customText)) {
            customText += "\n\n\n\n";
        } else {
            customText += "\n\n";
        }

        outputStream.write(left);
        outputStream.write(format);
        outputStream.write(customText.getBytes(), 0, customText.getBytes().length);
        outputStream.flush();
    }

    private String fillSpaces(String prescriptionExtras) {
        while (28 > prescriptionExtras.length()) {
            prescriptionExtras = " " + prescriptionExtras;
        }

        return prescriptionExtras;
    }

    private void closeBT() throws IOException {
        try {
            stopWorker = true;
            outputStream.close();
            inputStream.close();
            socket.close();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    @Override
    public void onState(int i) {
        if (i == 3) {
            switch (printAction) {
                case PRINT_REG_ID: {
                    printIdHoin(true);
                    break;
                }
                case PRINT_SUMMARY: {
                    printSummaryHoin();
                    break;
                }
                case PRINT_VITAL_DETAILS: {
                    printVitalDetailsHoin();
                    break;
                }
                case PRINT_ALL_VITALS: {
                    printAllVitalsHoin();
                    break;
                }
            }
        }
    }

    @Override
    public void onError(int i) {
        LToast.errorLong("Printer not connected. Please connect the printer and try again");
    }

    @Override
    public void onEvent(PrinterEvent printerEvent) {

    }

    private void printIdHoin(boolean addNewLine) {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df            = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String           formattedDate = df.format(c.getTime());

        String date_time = "Date & Time " + formattedDate + "";
        hoinPrinter.printText(date_time, false, false, false, false);

        TableCampInfo campInfo = dataManager.getCampInfo();

        String heading = "";

        try {
            String campName = campInfo.getCampName();

            if (!campName.isEmpty()) {
                heading = "Camp name: " + trim(campName, 14);
                hoinPrinter.printText(heading, false, false, false, false);
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        String path = printQrCode(regId);
        hoinPrinter.printImage(path, true);

        name = dataManager.getPatient(regId).getFullName();

        String data = "Name: " + trim(name, 25);
        if (!isEnglish(name)) {
            path = printUnicodeTextHoin(data);
            hoinPrinter.printImage(path, false);
            data = "";
        }

        try {
            if (hasValue(data)) {
                data += "\n";
            }
            int age = LUtils.calculateAge(dataManager.getPatient(regId).getYob());

            if (age > 0) {
                data += "Age : " + age + "\n";
            }
        } catch (Exception e) {
        }

        data += "Reg.Id:\n";
        data += regId;

        if (addNewLine) {
            data += "\n\n";
        }
        hoinPrinter.printText(data, false, false, false, false);
    }

    private void printSummaryHoin() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df            = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String           formattedDate = df.format(c.getTime());

        String date_time = "Date & Time " + formattedDate + "";
        hoinPrinter.printText(date_time, false, false, false, false);

        TableCampInfo campInfo = dataManager.getCampInfo();

        String heading = "";

        try {
            String campName = campInfo.getCampName();

            if (!campName.isEmpty()) {
                heading = "Camp name: " + trim(campName, 14);
                hoinPrinter.printText(heading, false, false, false, false);
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        heading = "Summary";
        hoinPrinter.printText(heading, false, false, false, true);

        String data = "";

        for (ItemSummaryPrint itemSummaryPrint : summaryList) {

            if (!data.isEmpty()) {
                data += "--------------------------------\n";
            }

            if (!itemSummaryPrint.getVital().isEmpty()) {
                data += itemSummaryPrint.getVital() + "\n";
            }

            for (ItemRangeCount rangeCount : itemSummaryPrint.getRanges()) {
                data += rangeCount.getRange() + " : " + rangeCount.getCount() + "\n";
            }
        }

        data += "\n\n";
        hoinPrinter.printText(data, false, false, false, false);
    }

    private void printVitalDetailsHoin() {

        priority.put("Height", 1);
        priority.put("Weight", 2);
        priority.put("Blood Pressure", 3);
        priority.put("Pulse", 4);
        priority.put("Blood Sugar", 5);
        priority.put("Hemoglobin", 6);
        priority.put("Albumin", 7);
        priority.put("Blood Group", 8);
        priority.put("Prescription", 9);
        priority.put("Malaria", 10);
        priority.put("Dengue", 11);
        priority.put("Syphilis", 12);
        priority.put("HIV-AIDS", 13);
        priority.put("Pregnancy", 14);
        priority.put("Heart Attack", 15);

        byte[] arrayOfByte1 = {27, 33, 0};
        byte[] format       = {27, 33, 0};

        byte[] left   = new byte[]{0x1b, 0x61, 0x00};
        byte[] center = new byte[]{0x1b, 0x61, 0x01};

        //------------------------------- FORMAT 1 --------------------------------//

        Collections.sort(vitalsList, new Comparator<TableVitals>() {
            @Override
            public int compare(TableVitals left, TableVitals right) {
                int val = 0;

                try {
                    val = priority.get(left.getVitalType()) - priority.get(right.getVitalType());
                } catch (Exception e) {
                    LLog.printStackTrace(e);
                }
                return val;
            }
        });

        printIdHoin(false);

        String data = "";

        for (TableVitals vitals : vitalsList) {
            if (vitals.isInactive()) {
                continue;
            }

            switch (vitals.getVitalType()) {
                case "Height": {
                    data += "Height : " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Weight": {
                    data += "Weight : " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";

                    String latestHeight = dataManager.getLatestHeight(StaticData.getRegistrationId(), vitals.getVitalDateTimeMillis());

                    if (null != latestHeight) {
                        data += "BMI    : ";

                        String[] height = latestHeight.split(";");

                        data += LUtils.findBmi(Float.parseFloat(height[0]),
                                height[1],
                                Float.parseFloat(vitals.getVitalValues()))
                                + " kg/m2 ";

                        data += "\n";
                    }

                    break;
                }
                case "Blood Pressure": {

                    String[] values = vitals.getVitalValues().split(";");
                    String[] units  = vitals.getVitalUnits().split(";");

                    data += "BP     : " + values[0] + " / " + values[1] + " " + units[0] + "\n";

                    String pulse = values[2];

                    if (!pulse.equals("0")) {
                        data += "Pulse  : " + values[2] + " " + units[2] + "\n";
                    }

                    break;
                }

                case "Pulse": {
                    String[] values = vitals.getVitalValues().split(";");
                    String[] units  = vitals.getVitalUnits().split(";");

                    data += "SpO2   : " + values[0] + " " + units[0] + " ,    ";

                    if (!values[2].equals("0")) {
                        data += "PI : " + values[2] + " " + units[2] + "\n";
                    }

                    data += "PR     : " + values[1] + " " + units[1] + "\n";

                    break;
                }

                case "Blood Sugar": {

                    switch (vitals.getVitalSubTypes()) {
                        case "RBS": {
                            data += "RBS    : ";
                            break;
                        }

                        case "FBS": {
                            data += "FBS    : ";
                            break;
                        }

                        case "PPBS": {
                            data += "PPBS   : ";
                            break;
                        }
                    }

                    data += vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }
                case "Hemoglobin": {
                    data += "Hemoglobin: " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Albumin": {
                    data += "Albumin: " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Blood Group": {
                    data += "Blood Group: " + vitals.getVitalValues() + " " + vitals.getVitalUnits() + "\n";
                    break;
                }

                case "Prescription": {
                    ItemPrescriptionDetails prescriptionDetails = LUtils.getPrescriptionDetails(vitals.getVitalValues());

                    String prescription = prescriptionDetails.getPrescription();
                    String timing       = prescriptionDetails.getTimings();
                    String food         = prescriptionDetails.getFood();

                    timing = timing.replaceAll(",", ", ");
                    String foodShort = food.equals("Before food") ? "BF" : "AF";

                    String timingBinary = "";
                    if (timing.contains("Morning")) {
                        timingBinary += "1-";
                    } else {
                        timingBinary += "0-";
                    }

                    if (timing.contains("Afternoon")) {
                        timingBinary += "1-";
                    } else {
                        timingBinary += "0-";
                    }

                    if (timing.contains("Night")) {
                        timingBinary += "1";
                    } else {
                        timingBinary += "0";
                    }

                    data += "Prescription: \n" + prescription;

                    String prescriptionExtras = " (" + LUtils.getDaysText(prescriptionDetails.getDays()) + "," + timingBinary + "," + foodShort + ")\n";

                    if (((prescription.length() % 32)) > 0 && (32 - (prescription.length() % 32)) > prescriptionExtras.length()) {
                        data += prescriptionExtras;
                    } else {
                        data += "\n" + fillSpaces(prescriptionExtras);
                    }
                    break;
                }

                case "Malaria":
                case "Dengue":
                case "Syphilis":
                case "HIV-AIDS":
                case "Pregnancy":
                case "Heart Attack": {
                    data += vitals.getVitalType() + ": " + vitals.getVitalValues() + "\n";
                    break;
                }
            }

            data += "         " + getFormattedDateTimeAm(vitals.getVitalDateTimeMillis()) + "\n\n";
        }

        data += "\n\n";

        hoinPrinter.printText(data, false, false, false, false);
    }

    private void printAllVitalsHoin() {
        boolean dataAvailable = false;

        List<TableDemographics> tableDemographicsList = dataManager.getAllPatients();

        for (TableDemographics tableDemographics : tableDemographicsList) {
            if (tableDemographics.isInactive()) {
                continue;
            }

            List<ItemVitalHeader> vitalHeaders = dataManager.getAllVitalHeaders(tableDemographics.getRegistrationId());

            List<TableVitals> vitalsList = new ArrayList<>();

            for (ItemVitalHeader vitalHeader : vitalHeaders) {

                if (LUtils.getLongTime(vitalHeader.getDate()) >= startDateLong
                        && LUtils.getLongTime(vitalHeader.getDate()) <= endDateLong) {
                    vitalsList.addAll(vitalHeader.getVitals());
                }
            }

            if (0 < vitalsList.size()) {
                dataAvailable   = true;
                this.regId      = tableDemographics.getRegistrationId();
                this.vitalsList = vitalsList;
                printVitalDetailsHoin();
            }
        }

        if (!dataAvailable) {
            LToast.warningLong("No data to print");
        }
    }

    public String printUnicodeTextHoin(String paramString) {
        TextPaint localTextPaint = new TextPaint();
        localTextPaint.setColor(-16777216);
        int i1 = 22;
        localTextPaint.setTextSize(i1);
        return printUnicodeTextHoin(paramString, Layout.Alignment.ALIGN_NORMAL, localTextPaint);
    }

    public String printUnicodeTextHoin(String paramString, Layout.Alignment paramAlignment, TextPaint paramTextPaint) {
        Bitmap localBitmap = a(LApplication.getContext(), paramString, paramAlignment, paramTextPaint);
        return saveImage(localBitmap);
    }

    public String printQrCode(String paramString) {
        try {
            TextPaint localTextPaint = new TextPaint();
            localTextPaint.setColor(-16777216);
            int i1 = 22;
            localTextPaint.setTextSize(i1);

            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            BitMatrix         bitMatrix         = multiFormatWriter.encode(paramString, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder    barcodeEncoder    = new BarcodeEncoder();
            Bitmap            bitmap            = barcodeEncoder.createBitmap(bitMatrix);

            bitmap = pad(bitmap, 90, 0);
            if (bitmap != null) {
                return saveImage(bitmap);
            }

        } catch (Exception e) {
        }

        return null;
    }

    public Bitmap pad(Bitmap Src, int padding_x, int padding_y) {
        Bitmap outputimage = Bitmap.createBitmap(Src.getWidth() + padding_x, Src.getHeight() + padding_y, Bitmap.Config.ARGB_8888);
        Canvas can         = new Canvas(outputimage);
        can.drawARGB(0xFF, 0xFF, 0xFF, 0xFF); //This represents White color
        can.drawBitmap(Src, padding_x, padding_y, null);
        return outputimage;
    }

    private static String saveImage(Bitmap finalBitmap) {
        File myDir = new File(Environment.getExternalStorageDirectory(), "");

        String fname = "tempimg.jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    private boolean isEnglish(String str) {
        for (char c : str.toCharArray()) {
            if (!(c >= 'a' && c <= 'z') && !(c >= 'A' && c <= 'Z')) {
                return false;
            }
        }
        return true;
    }
}