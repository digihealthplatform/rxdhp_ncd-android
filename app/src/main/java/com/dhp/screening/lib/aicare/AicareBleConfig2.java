//package com.dhp.screening.lib.aicare;
//
//import android.os.ParcelUuid;
//import android.util.SparseArray;
//
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.util.Comparator;
//import java.util.List;
//
//
//public class AicareBleConfig2 {
//    private static final byte          ADC                        = -3;
//    public static final  int           ADC_STR                    = 6;
//    private static final byte          AICARE_FLAG                = -84;
//    private static final int           AICARE_TYPE_UNKNOWN        = 65452;
//    private static final int           AICARE_TYPE_WEI            = 684;
//    private static final int           AICARE_TYPE_WEI_BROAD      = 172;
//    private static final int           AICARE_TYPE_WEI_BROAD_OLD  = 2086;
//    private static final int           AICARE_TYPE_WEI_TEMP       = 940;
//    private static final int           AICARE_TYPE_WEI_TEMP_BROAD = 428;
//    public static final  int           ALGORITHM_INFO             = 11;
//    public static final  byte          SET_DID                    = 29;
//    private static final byte[]        ALI_AICARE_FAT             = {1, 1, SET_DID};
//    private static final byte[]        ALI_FAT                    = {1, 1, 26};
//    private static final int           ALI_FLAG                   = 424;
//    private static final String        ALI_UUID                   = "0000feb3-0000-1000-8000-00805f9b34fb";
//    private static final byte[]        ALI_WEI                    = {1, 1, 27};
//    public static final  int           BLE_VERSION                = 2;
//    public static final  byte          BM02V6_WEI_DATA_CHANGE     = 6;
//    private static final byte          BM02V6_WEI_DATA_FAT        = 8;
//    public static final  byte          BM02V6_WEI_DATA_STABLE     = 7;
//    private static final String        BM09_UUID                  = "0000ffa0-0000-1000-8000-00805f9b34fb";
//    public static final  int           BM15KG1                    = 1;
//    public static final  int           BM15KG2                    = 4;
//    public static final  int           BM15LB1                    = 2;
//    public static final  int           BM15LB2                    = 5;
//    public static final  int           BM15ST1                    = 3;
//    public static final  int           BM15ST2                    = 6;
//    public static final  byte          BM_09                      = 9;
//    public static final  byte          BM_15                      = 15;
//    private static final byte          BM_15_FLAG                 = -68;
//    public static final  int           BODY_FAT_DATA              = 8;
//    public static final  int           CMD_INDEX                  = 3;
//    private static final byte          DATA                       = -53;
//    private static final byte          DATA_SEND_OVER             = -4;
//    public static final  int           DECIMAL_INFO               = 10;
//    public static final  int           DID_STR                    = 9;
//    private static final byte          FAT_DATA                   = -2;
//    public static final  byte          GET_BLE_VERSION            = -9;
//    public static final  byte          GET_DECIMAL_INFO           = 4;
//    public static final  int           HISTORY_DATA               = 7;
//    private static final String        JD_UUID                    = "0000d618-0000-1000-8000-00805f9b34fb";
//    private static final byte          MCU_DATE                   = -5;
//    public static final  int           MCU_DATE_STR               = 4;
//    private static final byte          MCU_TIME                   = -6;
//    public static final  int           MCU_TIME_STR               = 5;
//    private static final byte          NEW_HISTORY_DATA           = 5;
//    private static final byte          OPERATE_OR_STATE           = -2;
//    public static final  byte          QUERY_DID                  = 30;
//    private static final byte          SETTINGS                   = -52;
//    public static final  int           SETTINGS_STATUS            = 1;
//    private static final byte          START_FLAG                 = -82;
//    private static final int           SUM_END                    = 7;
//    private static final int           SUM_START                  = 2;
//    public static final  byte          SYNC_DATE                  = -3;
//    public static final  byte          SYNC_HISTORY               = -1;
//    private static final byte          SYNC_HISTORY_OR_LIST       = -49;
//    private static final byte          SYNC_HISTORY_STATUS        = -2;
//    public static final  byte          SYNC_LIST_OVER             = 2;
//    public static final  byte          SYNC_TIME                  = -4;
//    public static final  byte          SYNC_UNIT                  = 6;
//    public static final  byte          SYNC_USER_ID               = -6;
//    public static final  byte          SYNC_USER_INFO             = -5;
//    private static final byte          SYNC_USER_STATUS           = -4;
//    private static final String        TAG                        = "AicareBleConfig2";
//    private static final byte          TYPE_WEI                   = 2;
//    public static final  byte          TYPE_WEI_BROAD             = 0;
//    private static final byte          TYPE_WEI_TEMP              = 3;
//    public static final  byte          TYPE_WEI_TEMP_BROAD        = 1;
//    public static final  byte          UNIT_JIN                   = 3;
//    public static final  byte          UNIT_KG                    = 0;
//    public static final  byte          UNIT_LB                    = 1;
//    public static final  byte          UNIT_ST                    = 2;
//    private static final byte          UPDATE_USER                = 1;
//    public static final  byte          UPDATE_USER_OR_LIST        = -3;
//    private static final byte          USER_ID                    = -4;
//    public static final  int           USER_ID_STR                = 3;
//    public static final  int           WEIGHT_DATA                = 0;
//    private static final byte          WEI_CHANGE                 = -50;
//    public static final  byte          WEI_DATA_CHANGE            = 1;
//    public static final  byte          WEI_DATA_STABLE            = 2;
//    private static final byte          WEI_STABLE                 = -54;
//    private static       AlgorithmInfo algorithmInfo;
//    private static       byte          deviceType                 = 2;
//    private static       FatData       fatData                    = null;
//    private static       boolean       isHistory                  = false;
//    private static       byte[]        mByte                      = null;
//    private static       byte[]        preByte                    = null;
//
//    @Retention(RetentionPolicy.SOURCE)
//    public @interface SettingStatus {
//        public static final int ADC_ERROR                = 21;
//        public static final int ADC_MEASURED_ING         = 20;
//        public static final int DATA_SEND_END            = 25;
//        public static final int ERROR                    = 3;
//        public static final int HISTORY_SEND_OVER        = 18;
//        public static final int HISTORY_START_SEND       = 17;
//        public static final int LOW_POWER                = 1;
//        public static final int LOW_VOLTAGE              = 2;
//        public static final int NORMAL                   = 0;
//        public static final int NO_HISTORY               = 16;
//        public static final int NO_MATCH_USER            = 19;
//        public static final int REQUEST_DISCONNECT       = 22;
//        public static final int SET_DID_FAILED           = 24;
//        public static final int SET_DID_SUCCESS          = 23;
//        public static final int SET_TIME_FAILED          = 9;
//        public static final int SET_TIME_SUCCESS         = 8;
//        public static final int SET_UNIT_FAILED          = 7;
//        public static final int SET_UNIT_SUCCESS         = 6;
//        public static final int SET_USER_FAILED          = 11;
//        public static final int SET_USER_SUCCESS         = 10;
//        public static final int TIME_OUT                 = 4;
//        public static final int UNKNOWN                  = -1;
//        public static final int UNSTABLE                 = 5;
//        public static final int UPDATE_USER_FAILED       = 15;
//        public static final int UPDATE_USER_LIST_FAILED  = 13;
//        public static final int UPDATE_USER_LIST_SUCCESS = 12;
//        public static final int UPDATE_USER_SUCCESS      = 14;
//        public static final int USER_LIST_SEND_OVER      = 26;
//    }
//
//    private static class UserComparator implements Comparator<User> {
//        private UserComparator() {
//        }
//
//        public int compare(User user, User user2) {
//            return user.getId() - user2.getId();
//        }
//    }
//
//    public static byte[] initNewCmd(byte b) {
//        byte[] bArr = new byte[6];
//        bArr[0] = START_FLAG;
//        bArr[1] = 3;
//        bArr[2] = deviceType;
//        bArr[3] = b;
//        bArr[4] = 1;
//        bArr[5] = getByteSum(bArr, 2, bArr.length - 1);
//        return bArr;
//    }
//
//
//    private static byte getByteSum(byte[] bArr, int i, int i2) {
//        int i3 = 0;
//        while (i < i2) {
//            i3 += bArr[i];
//            i++;
//        }
//        return (byte) (i3 & 255);
//    }
//
//    private static boolean checkData(byte[] bArr) {
//        boolean z = false;
//        if (bArr == null || bArr.length == 0) {
//            return false;
//        }
//        if ((bArr.length != 8 && bArr.length != 20) || bArr[0] != -84 || (bArr[1] != 2 && bArr[1] != 3 && bArr[1] != 1 && bArr[1] != 0)) {
//            return false;
//        }
//        byte          byteSum = getByteSum(bArr, 2, 7);
//        String        str     = TAG;
//        StringBuilder sb      = new StringBuilder();
//        sb.append("result = ");
//        sb.append(byteSum);
//        String        str2 = TAG;
//        StringBuilder sb2  = new StringBuilder();
//        sb2.append("b[SUM_END] = ");
//        sb2.append(bArr[7]);
//        if (byteSum == bArr[7]) {
//            z = true;
//        }
//        return z;
//    }
//
//    public static boolean isValid(byte[] bArr) {
//        boolean z = false;
//        if (bArr[0] != -82 || ((bArr[2] != 2 && bArr[2] != 3) || bArr[1] + 2 >= bArr.length)) {
//            return false;
//        }
//        if (getByteSum(bArr, 2, bArr[1] + 2) == bArr[bArr.length - 1]) {
//            z = true;
//        }
//        return z;
//    }
//
//    private static void setDeviceType(int i) {
//        if (i == AICARE_TYPE_WEI_BROAD) {
//            deviceType = 0;
//        } else if (i == AICARE_TYPE_WEI_TEMP_BROAD) {
//            deviceType = 1;
//        } else if (i == AICARE_TYPE_WEI) {
//            deviceType = 2;
//        } else if (i == AICARE_TYPE_WEI_TEMP) {
//            deviceType = 3;
//        }
//    }
//
//    private static void setDeviceType(byte[] bArr) {
//        if (isArrStartWith(bArr, ALI_WEI)) {
//            deviceType = 2;
//        } else if (isArrStartWith(bArr, ALI_AICARE_FAT) || isArrStartWith(bArr, ALI_FAT)) {
//            deviceType = 2;
//        } else {
//            deviceType = 2;
//        }
//    }
//
//    private static int getFlag(byte[] bArr) {
//        byte b = 0;
//        if (bArr.length < 7) {
//            return 0;
//        }
//        if (bArr[6] <= 1 && bArr[6] >= 0) {
//            b = bArr[6];
//        }
//        return b;
//    }
//
//    private static boolean isArrStartWith(byte[] bArr, byte[] bArr2) {
//        if (bArr.length == 0 || bArr2.length == 0) {
//            return false;
//        }
//        for (int i = 0; i < bArr2.length; i++) {
//            if (bArr2[i] != bArr[i]) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    private static boolean isBM09(List<ParcelUuid> list) {
//        return !isListEmpty(list) && list.contains(ParcelUuid.fromString(BM09_UUID));
//    }
//
//    private static boolean isArrEmpty(byte[] bArr) {
//        return bArr == null || bArr.length == 0;
//    }
//
//    private static <T> boolean isListEmpty(List<T> list) {
//        return list == null || list.size() == 0;
//    }
//
////    public static WeiData getDatas(byte[] bArr) {
////        if (checkData(bArr)) {
////            switch (bArr[6]) {
////                case -54:
////                case -50:
////                    return getWeightData(bArr);
////
////                default: {
////                    Log.d(TAG, "getDatas: default " + bArr[6]);
////                }
////            }
////        } else {
////            Log.d(TAG, "getDatas:isBm02V6 ");
////        }
////        return null;
////    }
//
////    private static SparseArray<Object> getData(byte[] bArr) {
////        SparseArray<Object> sparseArray = new SparseArray<>();
////        switch (bArr[2]) {
////            case -6:
////                String dateOrTime = getDateOrTime(-6, bArr, 3);
////                if (fatData == null) {
////                    fatData = new FatData();
////                }
////                fatData.setTime(dateOrTime);
////                sparseArray.put(5, dateOrTime);
////                return sparseArray;
////            case -5:
////                String dateOrTime2 = getDateOrTime(-5, bArr, 3);
////                if (fatData == null) {
////                    fatData = new FatData();
////                }
////                fatData.setDate(dateOrTime2);
////                sparseArray.put(4, dateOrTime2);
////                return sparseArray;
////            case -4:
////                return getUserId(bArr);
////            case -3:
////                return getADC(bArr);
////            case -2:
////                return getBodyFatData(bArr);
////            default:
////                return sparseArray;
////        }
////    }
//
//
//    public static SparseArray<Object> getDatas(byte[] bArr) {
//        SparseArray<Object> sparseArray = new SparseArray<>();
//        if (checkData(bArr)) {
//            switch (bArr[6]) {
//                case -54:
//                case -50:
//                    return getWeiData(bArr);
//                case -53:
//                    return getData(bArr);
//                case -52:
//                    return getDeviceStatus(bArr);
//                case -49:
//                    if (bArr[2] == -2) {
//                        return getHistoryStatus(bArr);
//                    }
//                    if (bArr[2] == -4) {
//                        return getSyncUserStatus(bArr);
//                    }
//                    return sparseArray;
//                default:
//                    return sparseArray;
//            }
//        } else if (isBm02V6(bArr)) {
//            return getBm02V6Data(bArr);
//        } else {
//            if (isValid(bArr)) {
//                return getBleData(bArr);
//            }
//            if (isHistoryData(bArr)) {
//                return getHistoryData(getConcatBytes(bArr));
//            }
//            byte[] unpack = unpack(bArr);
//            if (unpack == null) {
//                return sparseArray;
//            }
//            if (unpack[0] != -84) {
//                return unpack[0] == -82 ? getDecimalInfo(unpack) : sparseArray;
//            }
//            if (unpack[2] == -9) {
//                sparseArray.put(2, getVersion(unpack, 3, 2015));
//                return sparseArray;
//            } else if (unpack[2] == -2 && unpack[6] == -49) {
//                return getHistoryStatus(unpack);
//            } else {
//                return sparseArray;
//            }
//        }
//    }
//
//    private static boolean isBm02V6(byte[] bArr) {
//        boolean z = false;
//        if ((bArr[0] & SYNC_HISTORY) != 174 || ((bArr[2] != 2 && bArr[2] != 3) || (bArr[3] != 6 && bArr[3] != 7 && bArr[3] != 8))) {
//            return false;
//        }
//        int i = bArr[1] + 2;
//        if (getByteSum(bArr, 2, i) == bArr[i]) {
//            z = true;
//        }
//        return z;
//    }
//
//
//    private static SparseArray<Object> getWeiData(byte[] bArr) {
//        SparseArray<Object> sparseArray = new SparseArray<>();
//        sparseArray.put(0, getWeightData(bArr));
//        return sparseArray;
//    }
//
//    private static SparseArray<Object> getBm02V6Data(byte[] bArr) {
//        SparseArray<Object> sparseArray = new SparseArray<>();
//        WeiData             weiData     = new WeiData();
//        float               f           = (float) (((bArr[4] & SYNC_HISTORY) << 8) + (bArr[5] & SYNC_HISTORY));
//        weiData.setWeight(f);
//        if (bArr[2] == 3) {
//            int i = (bArr[6] & BM_15) << ((bArr[7] & SYNC_HISTORY) + 8);
//            if ((bArr[6] & 240) == 240) {
//                i = -i;
//            }
//            weiData.setTemp(((float) i) / 10.0f);
//        } else {
//            weiData.setTemp(Float.MAX_VALUE);
//        }
//        DecInfo decInfo = new DecInfo();
//        decInfo.setSourceDecimal(bArr[8] >> 4);
//        decInfo.setKgDecimal(bArr[8] & BM_15);
//        decInfo.setLbDecimal(bArr[9] >> 4);
//        decInfo.setStDecimal(bArr[9] & BM_15);
//        decInfo.setKgGraduation(bArr[10] >> 4);
//        decInfo.setLbGraduation(bArr[10] & BM_15);
//        weiData.setDecInfo(decInfo);
//        switch (bArr[3]) {
//            case 6:
//                weiData.setCmdType(1);
//                break;
//            case 7:
//                weiData.setCmdType(2);
//                if (fatData == null) {
//                    fatData = new FatData();
//                }
//                fatData.setWeight(f);
//                break;
//        }
//        sparseArray.put(0, weiData);
//        return sparseArray;
//    }
//
//    private static SparseArray<Object> getHistoryStatus(byte[] bArr) {
//        SparseArray<Object> sparseArray = new SparseArray<>();
//        switch (bArr[3]) {
//            case 0:
//                sparseArray.put(1, Integer.valueOf(16));
//                break;
//            case 1:
//                isHistory = true;
//                sparseArray.put(1, Integer.valueOf(17));
//                break;
//            case 2:
//                isHistory = false;
//                sparseArray.put(1, Integer.valueOf(18));
//                break;
//        }
//        return sparseArray;
//    }
//
//    private static SparseArray<Object> getSyncUserStatus(byte[] bArr) {
//        SparseArray<Object> sparseArray = new SparseArray<>();
//        switch (bArr[3]) {
//            case 0:
//                sparseArray.put(1, Integer.valueOf(12));
//                break;
//            case 1:
//                sparseArray.put(1, Integer.valueOf(15));
//                break;
//            case 2:
//                sparseArray.put(1, Integer.valueOf(14));
//                break;
//            case 3:
//                sparseArray.put(1, Integer.valueOf(15));
//                break;
//        }
//        return sparseArray;
//    }
//
//    private static boolean isHistoryStart(byte[] bArr) {
//        if (!(bArr[0] == -84 && ((bArr[1] == 2 || bArr[1] == 3) && bArr[2] == -1))) {
//            if (bArr[0] != -82) {
//                return false;
//            }
//            if (!((bArr[2] == 2 || bArr[2] == 3) && bArr[3] == 5)) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    public static byte[] getRandomBytes() {
//        return AicareUtils.initRandomByteArr();
//    }
//
//    public static byte[] encrypt(byte[] bArr, boolean z) {
//        return AicareUtils.encrypt(bArr, z);
//    }
//
//    public static byte[] decrypt(byte[] bArr, boolean z) {
//        return AicareUtils.decrypt(bArr, z);
//    }
//
//    public static boolean compareBytes(byte[] bArr, byte[] bArr2) {
//        return AicareUtils.compareBytes(bArr, bArr2);
//    }
//
//    public static boolean compareVersion(String str, String str2) {
//        return AicareUtils.compareVersion(str, str2);
//    }
//
//    public static boolean compareAddress(String str) {
//        return AicareUtils.compareAddress(str);
//    }
//
//    public static WeiData getWeightData(byte[] bArr) {
//        if (!checkData(bArr) || (bArr[6] != -50 && bArr[6] != -54)) {
//            return null;
//        }
//        int   i       = 2;
//        float dataInt = (float) getDataInt(2, 3, bArr);
//        float f       = Float.MAX_VALUE;
//        if (bArr[1] == 3 || bArr[1] == 1) {
//            f = getTemp(4, 5, bArr);
//        }
//        byte b = bArr[6];
//        if (bArr[6] == -54) {
//            if (fatData == null) {
//                fatData = new FatData();
//            }
//            fatData.setWeight(dataInt);
//        } else {
//            i = 1;
//        }
//        return new WeiData(i, dataInt, f, null);
//    }
//
//    private static float getTemp(int i, int i2, byte[] bArr) {
//        byte  byteValue = Integer.valueOf(bArr[i] >> 4).byteValue();
//        float dataInt   = ((float) getDataInt(0, 1, new byte[]{Integer.valueOf(bArr[i] & BM_15).byteValue(), bArr[i2]})) / 10.0f;
//        return byteValue == 15 ? -dataInt : dataInt;
//    }
//
//    public static int getDataInt(int i, int i2, byte[] bArr) {
//        int           i3 = ((bArr[i] & AicareBleConfig2.SYNC_HISTORY) << 8) + (bArr[i2] & AicareBleConfig2.SYNC_HISTORY);
//        StringBuilder sb = new StringBuilder();
//        sb.append("getDataInt = ");
//        sb.append(i3);
//        return i3;
//    }
//}
