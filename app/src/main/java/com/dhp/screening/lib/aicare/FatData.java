package com.dhp.screening.lib.aicare;

import android.os.Parcel;
import android.os.Parcelable;

public class FatData implements Parcelable {
    public static final Creator<FatData> CREATOR = new Creator<FatData>() {
        public FatData createFromParcel(Parcel parcel) {
            return new FatData(parcel);
        }

        public FatData[] newArray(int i) {
            return new FatData[i];
        }
    };
    private             int              adc;
    private             int              age;
    private             float            bfr;
    private             float            bm;
    private             float            bmi;
    private             float            bmr;
    private             int              bodyAge;
    private             String           date;
    private             DecInfo          decInfo;
    private             int              height;
    private             int              number;
    private             float            pp;
    private             float            rom;
    private             int              sex;
    private             float            sfr;
    private             String           syncFlag;
    private             String           time;
    private             int              uvi;
    private             float            vwc;
    private             float            weight;

    public int describeContents() {
        return 0;
    }

    public FatData() {
    }

    public FatData(String str, String str2, float f, float f2, float f3, float f4, int i, float f5, float f6, float f7, float f8, int i2, float f9, int i3, int i4, int i5, int i6, int i7, DecInfo decInfo2) {
        this.date = str;
        this.time = str2;
        this.weight = f;
        this.bmi = f2;
        this.bfr = f3;
        this.sfr = f4;
        this.uvi = i;
        this.rom = f5;
        this.bmr = f6;
        this.bm = f7;
        this.vwc = f8;
        this.bodyAge = i2;
        this.pp = f9;
        this.number = i3;
        this.sex = i4;
        this.age = i5;
        this.height = i6;
        this.adc = i7;
        this.decInfo = decInfo2;
    }

    public FatData(String str, String str2, float f, float f2, float f3, float f4, int i, float f5, float f6, float f7, float f8, int i2, float f9, int i3, int i4, int i5, int i6, int i7) {
        this.date = str;
        this.time = str2;
        this.weight = f;
        this.bmi = f2;
        this.bfr = f3;
        this.sfr = f4;
        this.uvi = i;
        this.rom = f5;
        this.bmr = f6;
        this.bm = f7;
        this.vwc = f8;
        this.bodyAge = i2;
        this.pp = f9;
        this.number = i3;
        this.sex = i4;
        this.age = i5;
        this.height = i6;
        this.adc = i7;
    }

    protected FatData(Parcel parcel) {
        this.date = parcel.readString();
        this.time = parcel.readString();
        this.weight = parcel.readFloat();
        this.bmi = parcel.readFloat();
        this.bfr = parcel.readFloat();
        this.sfr = parcel.readFloat();
        this.uvi = parcel.readInt();
        this.rom = parcel.readFloat();
        this.bmr = parcel.readFloat();
        this.bm = parcel.readFloat();
        this.vwc = parcel.readFloat();
        this.bodyAge = parcel.readInt();
        this.pp = parcel.readFloat();
        this.number = parcel.readInt();
        this.sex = parcel.readInt();
        this.age = parcel.readInt();
        this.height = parcel.readInt();
        this.adc = parcel.readInt();
        this.decInfo = (DecInfo) parcel.readParcelable(DecInfo.class.getClassLoader());
        this.syncFlag = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.date);
        parcel.writeString(this.time);
        parcel.writeFloat(this.weight);
        parcel.writeFloat(this.bmi);
        parcel.writeFloat(this.bfr);
        parcel.writeFloat(this.sfr);
        parcel.writeInt(this.uvi);
        parcel.writeFloat(this.rom);
        parcel.writeFloat(this.bmr);
        parcel.writeFloat(this.bm);
        parcel.writeFloat(this.vwc);
        parcel.writeInt(this.bodyAge);
        parcel.writeFloat(this.pp);
        parcel.writeInt(this.number);
        parcel.writeInt(this.sex);
        parcel.writeInt(this.age);
        parcel.writeInt(this.height);
        parcel.writeInt(this.adc);
        parcel.writeParcelable(this.decInfo, i);
        parcel.writeString(this.syncFlag);
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String str) {
        this.date = str;
    }

    public String getTime() {
        return this.time;
    }

    public void setTime(String str) {
        this.time = str;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(float f) {
        this.weight = f;
    }

    public float getBmi() {
        return this.bmi;
    }

    public void setBmi(float f) {
        this.bmi = f;
    }

    public float getBfr() {
        return this.bfr;
    }

    public void setBfr(float f) {
        this.bfr = f;
    }

    public float getSfr() {
        return this.sfr;
    }

    public void setSfr(float f) {
        this.sfr = f;
    }

    public int getUvi() {
        return this.uvi;
    }

    public void setUvi(int i) {
        this.uvi = i;
    }

    public float getRom() {
        return this.rom;
    }

    public void setRom(float f) {
        this.rom = f;
    }

    public float getBmr() {
        return this.bmr;
    }

    public void setBmr(float f) {
        this.bmr = f;
    }

    public float getBm() {
        return this.bm;
    }

    public void setBm(float f) {
        this.bm = f;
    }

    public float getVwc() {
        return this.vwc;
    }

    public void setVwc(float f) {
        this.vwc = f;
    }

    public int getBodyAge() {
        return this.bodyAge;
    }

    public void setBodyAge(int i) {
        this.bodyAge = i;
    }

    public float getPp() {
        float f = this.pp;
        return f >= 100.0f ? f / 10.0f : f;
    }

    public void setPp(float f) {
        this.pp = f;
    }

    public int getNumber() {
        return this.number;
    }

    public void setNumber(int i) {
        this.number = i;
    }

    public int getAdc() {
        return this.adc;
    }

    public void setAdc(int i) {
        this.adc = i;
    }

    public int getSex() {
        return this.sex;
    }

    public void setSex(int i) {
        this.sex = i;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int i) {
        this.age = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public DecInfo getDecInfo() {
        return this.decInfo;
    }

    public void setDecInfo(DecInfo decInfo2) {
        this.decInfo = decInfo2;
    }

    public String getSyncFlag() {
        return this.syncFlag;
    }

    public void setSyncFlag(String str) {
        this.syncFlag = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BodyFatData{date='");
        sb.append(this.date);
        sb.append('\'');
        sb.append(", time='");
        sb.append(this.time);
        sb.append('\'');
        sb.append(", weight=");
        sb.append(this.weight);
        sb.append(", bmi=");
        sb.append(this.bmi);
        sb.append(", bfr(体脂率)=");
        sb.append(this.bfr);
        sb.append(", sfr(皮下脂肪)=");
        sb.append(this.sfr);
        sb.append(", uvi(内脏脂肪)=");
        sb.append(this.uvi);
        sb.append(", rom(肌肉率)=");
        sb.append(this.rom);
        sb.append(", bmr(基础代谢率)=");
        sb.append(this.bmr);
        sb.append(", bm(骨量)=");
        sb.append(this.bm);
        sb.append(", vwc(水份)=");
        sb.append(this.vwc);
        sb.append(", bodyAge=");
        sb.append(this.bodyAge);
        sb.append(", pp(蛋白率)=");
        sb.append(this.pp);
        sb.append(", number=");
        sb.append(this.number);
        sb.append(", sex=");
        sb.append(this.sex);
        sb.append(", age=");
        sb.append(this.age);
        sb.append(", height=");
        sb.append(this.height);
        sb.append(", adc=");
        sb.append(this.adc);
        sb.append(", decimalInfo=");
        sb.append(this.decInfo);
        sb.append('}');
        return sb.toString();
    }
}
