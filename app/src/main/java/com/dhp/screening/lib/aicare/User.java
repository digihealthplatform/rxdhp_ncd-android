package com.dhp.screening.lib.aicare;

public class User {
    private int adc;
    private int age;
    private int height;
    private int id;
    private int sex;
    private int weight;

    public User() {
    }

    public User(int i, int i2, int i3, int i4, int i5, int i6) {
        this.id = i;
        this.sex = i2;
        this.age = i3;
        this.height = i4;
        this.weight = i5;
        this.adc = i6;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getSex() {
        return this.sex;
    }

    public void setSex(int i) {
        this.sex = i;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int i) {
        this.age = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public int getWeight() {
        return this.weight;
    }

    public void setWeight(int i) {
        this.weight = i;
    }

    public int getAdc() {
        return this.adc;
    }

    public void setAdc(int i) {
        this.adc = i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("User{id=");
        sb.append(this.id);
        sb.append(", sex=");
        sb.append(this.sex);
        sb.append(", age=");
        sb.append(this.age);
        sb.append(", height=");
        sb.append(this.height);
        sb.append(", weight=");
        sb.append(this.weight);
        sb.append(", adc=");
        sb.append(this.adc);
        sb.append('}');
        return sb.toString();
    }
}
