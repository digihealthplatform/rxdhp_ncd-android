package com.dhp.screening.lib.bluetooth;

import com.dhp.screening.util.LLog;

import java.util.concurrent.LinkedBlockingQueue;


public class ParseSteamRunnable
        implements Runnable {

    private static final String TAG = ParseSteamRunnable.class.getSimpleName();

    private boolean              isStop = false;
    private OnDataChangeListener mOnDataChangeListener;

    private OxiParams                    mOxiParams = new OxiParams();
    private LinkedBlockingQueue<Integer> oxiData    = new LinkedBlockingQueue(256);
    private int[]                        parseBuf   = new int[5];

    public interface OnDataChangeListener {

        void onPulseWaveDetected();

        void onSpO2ParamsChanged();

        void onSpO2WaveChanged(int paramInt);
    }

    ParseSteamRunnable(OnDataChangeListener paramOnDataChangeListener) {
        this.mOnDataChangeListener = paramOnDataChangeListener;
    }

    private int getData() {
        try {
            return this.oxiData.take();
        } catch (InterruptedException localInterruptedException) {
            localInterruptedException.printStackTrace();
        }
        return 0;
    }

    public static float getFloatPi(int paramInt) {
        switch (paramInt) {
            case 0:
                return 0.1F;
            case 1:
                return 0.2F;
            case 2:
                return 0.4F;
            case 3:
                return 0.7F;
            case 4:
                return 1.4F;
            case 5:
                return 2.7F;
            case 6:
                return 5.3F;
            case 7:
                return 10.3F;
            case 8:
                return 20.0F;
            default:
                return 0.0F;
        }
    }

    private int toUnsignedInt(byte paramByte) {
        return paramByte & 0xFF;
    }

    public void add(byte[] paramArrayOfByte) {

        int j = paramArrayOfByte.length;
        int i = 0;

        while (i < j) {
            byte b = paramArrayOfByte[i];
            try {
                this.oxiData.put(toUnsignedInt(b));
                i += 1;
            } catch (InterruptedException localInterruptedException) {
                localInterruptedException.printStackTrace();
            }
        }

        LLog.d(TAG, "add: ");
    }

    public OxiParams getOxiParams() {
        return this.mOxiParams;
    }

    public void run() {
        while (!this.isStop) {
            LLog.d(TAG, "run: ");
            int i = getData();
            if ((i & 0x80) > 0) {
                this.parseBuf[0] = i;
                i = 1;
                int PACKAGE_LEN = 5;
                while (i < PACKAGE_LEN) {
                    int j = getData();
                    if ((j & 0x80) == 0) {
                        this.parseBuf[i] = j;
                    }
                    i += 1;
                }
                i = this.parseBuf[4];
                int j = this.parseBuf[3] | (this.parseBuf[2] & 0x40) << 1;
                int k = this.parseBuf[0] & 0xF;
                if ((i != this.mOxiParams.spo2) || (j != this.mOxiParams.pulseRate) || (k != this.mOxiParams.pi)) {
                    this.mOxiParams.update(i, j, k);
                    this.mOnDataChangeListener.onSpO2ParamsChanged();
                }
                this.mOnDataChangeListener.onSpO2WaveChanged(this.parseBuf[1]);
                if ((this.parseBuf[0] & 0x40) != 0) {
                    this.mOnDataChangeListener.onPulseWaveDetected();
                }
            }
        }
    }

    public void stop() {
        this.isStop = false;
    }

    public class OxiParams {
        int PI_INVALID_VALUE   = 15;
        int PR_INVALID_VALUE   = 255;
        int SPO2_INVALID_VALUE = 127;

        private int pi;
        private int pulseRate;
        private int spo2;

        OxiParams() {
        }

        private void update(int paramInt1, int paramInt2, int paramInt3) {
            this.spo2 = paramInt1;
            this.pulseRate = paramInt2;
            this.pi = paramInt3;
        }

        public int getPi() {
            return this.pi;
        }

        public int getPulseRate() {
            return this.pulseRate;
        }

        public int getSpo2() {
            return this.spo2;
        }

        public boolean isParamsValid() {
            return (this.spo2 != this.SPO2_INVALID_VALUE) && (this.pulseRate != this.PR_INVALID_VALUE) && (this.pi != this.PI_INVALID_VALUE) && (this.spo2 != 0) && (this.pulseRate != 0) && (this.pi != 0);
        }
    }
}
