package com.dhp.screening.lib.barcodereader.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;

import com.dhp.screening.data.asset.AssetReader;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.dhp.screening.R;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.ui.BaseActivity;
import com.dhp.screening.util.LLog;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.google.zxing.BarcodeFormat.QR_CODE;

public class ActivityQr extends BaseActivity implements ZXingScannerView.ResultHandler {

    private static final String TAG = ActivityQr.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_frame)
    ViewGroup contentFrame;

    private ZXingScannerView scannerView;

    private boolean isSearchRegistrationId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);
        initToolbar(toolbar, AssetReader.getLangKeyword("scan_qr"), true);
        ButterKnife.bind(this);

        isSearchRegistrationId = getIntent().getBooleanExtra("is_search_registration_id", false);

        scannerView = new ZXingScannerView(this);

        ArrayList<BarcodeFormat> formats = new ArrayList<>(1);
        formats.add(QR_CODE);

        scannerView.setFormats(formats);
        contentFrame.addView(scannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void handleResult(Result result) {
        LLog.d(TAG, "handleResult: " + result.getText());

        if (isSearchRegistrationId) {
            StaticData.setQrSearchResult(result.getText());
        } else {
            StaticData.setQrResult(result.getText());
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 500);
    }
}