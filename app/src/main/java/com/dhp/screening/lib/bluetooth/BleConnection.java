package com.dhp.screening.lib.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import com.dhp.screening.lib.aicare.AicareBleConfig;
import com.dhp.screening.lib.aicare.AlgorithmInfo;
import com.dhp.screening.lib.aicare.BleInfo;
import com.dhp.screening.lib.aicare.DecInfo;
import com.dhp.screening.lib.aicare.FatData;
import com.dhp.screening.lib.aicare.User;
import com.dhp.screening.lib.aicare.WeiData;
import com.dhp.screening.util.LLog;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_FINISHED;
import static android.bluetooth.BluetoothAdapter.ACTION_DISCOVERY_STARTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_START_TEST_BP;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_START_TEST_GLUCOSE;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_START_TEST_SPO2;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.ASHA_UUID;
import static com.dhp.screening.lib.bluetooth.BleConstants.BATTERY_LEVEL;
import static com.dhp.screening.lib.bluetooth.BleConstants.BERRY_MED_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.BERRY_MED_DEVICE_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_FINAL_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_START_TEST_NOW;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_TEST_ERROR;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_TEST_IN_PROGRESS;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_TEST_STARTED;
import static com.dhp.screening.lib.bluetooth.BleConstants.BP_WAIT;
import static com.dhp.screening.lib.bluetooth.BleConstants.CHIPSEA_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.CHIPSEA_START_TEST_CHARACTERISTIC;
import static com.dhp.screening.lib.bluetooth.BleConstants.CHIPSEA_START_TEST_SERVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.CLIENT_CHARACTERISTIC_CONFIG;
import static com.dhp.screening.lib.bluetooth.BleConstants.SFBP;
import static com.dhp.screening.lib.bluetooth.BleConstants.SWAN_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SWAN_START_TEST_SERVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SWAN_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNCPLUS_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_BATTERY_LEVEL_SERVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_LEVEL_CHARACTERISTIC;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_START_TEST_CHARACTERISTIC;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_START_TEST_SERVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.SYNC_TEST_RESULT;
import static com.dhp.screening.lib.bluetooth.BleConstants.TECHNAXX_BP_BATTERY_LEVEL_CHARACTERISTIC;
import static com.dhp.screening.lib.bluetooth.BleConstants.TECHNAXX_BP_BATTERY_LEVEL_SERVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.TECHNAXX_BP_DEVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.TECHNAXX_BP_START_TEST_CHARACTERISTIC;
import static com.dhp.screening.lib.bluetooth.BleConstants.TECHNAXX_BP_START_TEST_SERVICE;
import static com.dhp.screening.lib.bluetooth.BleConstants.UUID_CHARACTER_RECEIVE;
import static com.dhp.screening.lib.bluetooth.BleConstants.UUID_SERVICE_DATA;
import static com.dhp.screening.lib.bluetooth.BleUtils.bytesToInt;
import static com.dhp.screening.lib.bluetooth.BleUtils.hexStringToByteArray;
import static com.dhp.screening.lib.bluetooth.BleUtils.x1;

public class BleConnection extends BroadcastReceiver implements BleConnectionInterface, ParseSteamRunnable.OnDataChangeListener {
    private static final String TAG = "BleConnection";

    private Context     context;
    private BleCallback callback;

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGatt    bluetoothDevice;

    public ParseSteamRunnable parseSteamRunnable;

    private List<BluetoothGattService> serviceList;

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi,
                             byte[] scanRecord) {
            callback.onBleScan(device);
        }
    };

    private boolean isWritten;

    private String        f;
    private String        b;
    private StringBuilder e = new StringBuilder();

    private ArrayList d = new ArrayList();

    private ModelBloodPressureData modelBloodPressureData;
    private ModelBloodPressureData modelBloodPressureDataOld;

    private boolean isBpTestStarted;
    private String  btName;

    private BluetoothSocket  ashaSocket;
    private AshaAcceptThread ashaAcceptThread;

    public List<byte[]> usersByte = new ArrayList();

    User user = new User(1, 1, 10, 20, 50, 6);

    public  byte[] userIdByte   = AicareBleConfig.initCmd((byte) -6, user, (byte) 0);
    private byte[] userInfoByte = AicareBleConfig.initCmd((byte) -5, user, (byte) 0);

    public int index = 0;


    public TimeOutHandler handler = new TimeOutHandler();

    private final BluetoothGattCallback gattConnectionCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                callback.onConnected();

                if (null == parseSteamRunnable && bluetoothDevice.getDevice().getName().equals(BERRY_MED_DEVICE)) {
                    parseSteamRunnable = new ParseSteamRunnable(BleConnection.this);
                    new Thread(parseSteamRunnable).start();
                }
                bluetoothDevice.discoverServices();

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                callback.onDisconnected();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {

            if (status == BluetoothGatt.GATT_SUCCESS) {
                serviceList = bluetoothDevice.getServices();
                callback.onServicesDiscovered();
            }
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,
                                     int status) {
            String serviceUUID = descriptor.getCharacteristic().getService().getUuid().toString();
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic, int status) {
            String batteryLevel = "" + BleUtils.fromByteArray(characteristic.getValue());
            LLog.d(TAG, "Battery level " + batteryLevel);
            callback.onValueRead(BATTERY_LEVEL, batteryLevel);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt paramBluetoothGatt, BluetoothGattDescriptor paramBluetoothGattDescriptor, int paramInt) {
            LLog.d(TAG, "onDescriptorWrite: ");

            if (bluetoothDevice.getDevice().getName().equals(SWAN_DEVICE)) {
                if (0 == paramInt) {
//                    sendCmd((byte) -9, (byte) 0);
                }
            } else {
                if (paramInt == 0) {
                    if (((paramBluetoothGatt.getDevice() != null) && (paramBluetoothGatt.getDevice().getName().startsWith("HL158HC"))) || (paramBluetoothGatt.getDevice().getName().contains("SFBPBLE"))) {

                        Calendar calendar = Calendar.getInstance();

                        sendData(paramBluetoothGatt,
                                "0000fff5-0000-1000-8000-00805f9b34fb",
                                new byte[]{-95, (byte) (calendar.get(Calendar.YEAR) - 2000), (byte) (calendar.get(Calendar.MONTH) + 1), (byte) calendar.get(Calendar.DAY_OF_MONTH), (byte) calendar.get(Calendar.HOUR_OF_DAY), (byte) calendar.get(Calendar.MINUTE), (byte) calendar.get(Calendar.SECOND)},
                                "TIME_SYNC");

                        sendData(3000L, paramBluetoothGatt);
                    }
                }
            }
        }

        private void sendData(long ll, final BluetoothGatt bluetoothgatt) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    sendData(bluetoothgatt, "0000fff5-0000-1000-8000-00805f9b34fb", "BT:9");
                }
            }, ll);
        }

        private void sendData(BluetoothGatt bluetoothgatt, String s, String s1) {
            for (Iterator iterator = bluetoothgatt.getServices().iterator(); iterator.hasNext(); ) {
                BluetoothGattService bluetoothgattservice = (BluetoothGattService) iterator.next();
                Iterator             iterator1            = bluetoothgattservice.getCharacteristics().iterator();
                while (iterator1.hasNext()) {
                    BluetoothGattCharacteristic bluetoothgattcharacteristic = (BluetoothGattCharacteristic) iterator1.next();
                    if (bluetoothgattcharacteristic.getUuid().toString().equals(s)) {
                        bluetoothgattcharacteristic.setValue(s1);
                        f = s1;
                        bluetoothgatt.writeCharacteristic(bluetoothgattcharacteristic);
                    }
                }
            }
        }

        private void sendData(BluetoothGatt bluetoothgatt, String s, byte abyte0[], String s1) {
            for (Iterator iterator = bluetoothgatt.getServices().iterator(); iterator.hasNext(); ) {
                Iterator iterator1 = ((BluetoothGattService) iterator.next()).getCharacteristics().iterator();
                while (iterator1.hasNext()) {
                    BluetoothGattCharacteristic bluetoothgattcharacteristic = (BluetoothGattCharacteristic) iterator1.next();
                    if (bluetoothgattcharacteristic.getUuid().toString().equals(s)) {
                        bluetoothgattcharacteristic.setValue(abyte0);
                        bluetoothgatt.writeCharacteristic(bluetoothgattcharacteristic);
                        f = s1;
                    }
                }
            }
        }

        public void onCharacteristicWrite(BluetoothGatt bluetoothGatt, BluetoothGattCharacteristic bluetoothGattCharacteristic, int i) {

            if (true) {
                return;
            }

            if (i != 0) {

            } else if (bluetoothGattCharacteristic.getUuid().equals(AICARE_WRITE_CHARACTERISTIC_UUID)) {
                byte[] value = bluetoothGattCharacteristic.getValue();

                List<User> userList = new ArrayList<>();
                userList.add(user);

                usersByte = AicareBleConfig.initUserListCmds(userList);

                if (usersByte.size() != 0) {

                    if (index < usersByte.size() - 1) {
                        if (Arrays.equals(value, (byte[]) usersByte.get(index))) {
                            writeValue((byte[]) usersByte.get(access$1204()));
                        }
                    } else if (Arrays.equals(value, (byte[]) usersByte.get(index))) {
                        sendCmd((byte) 2, (byte) 0);
                        index = 0;
                    }
                }
                if (Arrays.equals(value, AicareBleConfig.initCmd((byte) 2, null, (byte) 0))) {
                    // TODO: 29-03-2020
                    Log.d(TAG, "onCharacteristicWrite: ");
//                    WBYManager.this.handler.postDelayed(new Runnable() {
//                        public void run() {
//                            WBYManager.this.mCallbacks.getSettingStatus(26);
//                        }
//                    }, 600);
                }
                if (Arrays.equals(value, userIdByte)) {
                    writeValue(userInfoByte);
                }
                if (Arrays.equals(value, dateByte)) {
                    sendCmd((byte) -4, (byte) 0);
                }
            }
        }

        /* synthetic */ int access$1204() {
            int i = index + 1;
            index = i;
            return i;
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {

            switch (bluetoothDevice.getDevice().getName()) {

                case SYNCPLUS_DEVICE:
                case SYNC_DEVICE: {
                    String value = "" + BleUtils.fromByteArray(characteristic.getValue());
                    callback.onValueRead(SYNC_TEST_RESULT, value);
                    break;
                }

                case TECHNAXX_BP_DEVICE: {
                    LLog.d(TAG, "length: " + characteristic.getValue().length);

                    try {
                        if (8 == characteristic.getValue().length) {
                            String batteryLevel = "" + characteristic.getIntValue(17, 6);

                            callback.onValueRead(BATTERY_LEVEL, batteryLevel);
                            LLog.d(TAG, "Battery level " + batteryLevel);

                        } else if (7 == characteristic.getValue().length) {
                            int value1 = characteristic.getIntValue(17, 5);
                            int value2 = characteristic.getIntValue(17, 6);

                            callback.onValueRead(BP_TEST_IN_PROGRESS, "" + value1);
                            LLog.d(TAG, "Test in progress: " + value1 + "," + value2);

                        } else if (10 == characteristic.getValue().length) {
                            callback.onValueRead(BP_TEST_STARTED, "");

                        } else if (17 == characteristic.getValue().length) {

                            if (255 == characteristic.getIntValue(17, 6)) {
                                LLog.d(TAG, "Error. Try again");
                                callback.onValueRead(BP_TEST_ERROR, "");

                            } else {
                                int systolic  = characteristic.getIntValue(17, 6);
                                int diastolic = characteristic.getIntValue(17, 8);
                                int heartRate = characteristic.getIntValue(17, 12);

                                callback.onValueRead(BP_FINAL_RESULT, (systolic + ";" + diastolic + ";" + heartRate));
                                LLog.d(TAG, "Test final result: " + systolic + ", " + diastolic + ", " + heartRate);
                            }
                        }
                    } catch (Exception e) {
//                        LLog.printStackTrace(e);
                    }
                    break;
                }

                case SWAN_DEVICE: {
                    if (characteristic.getUuid().equals(AICARE_NOTIFY_CHARACTERISTIC_UUID)) {
                        handleAiCareData(characteristic.getValue());
                    }

                    break;
                }

                case CHIPSEA_DEVICE: {
                    byte[] values = characteristic.getValue();

                    String value = "" + BleUtils.fromByteArray(values);

                    LLog.d(TAG, "onCharacteristicChanged: " + value);

                    byte[] valuesBytes = new byte[2];
                    valuesBytes[0] = values[5];
                    valuesBytes[1] = values[6];

                    callback.onValueRead(SYNC_TEST_RESULT, "" + bytesToInt(valuesBytes));
                    break;
                }

                case BERRY_MED_DEVICE: {
                    parseSteamRunnable.add(characteristic.getValue());
                    break;
                }

                case SFBP: {
                    String s3 = x1(characteristic.getValue());

                    if (s3.equals("52")) {
                        isBpTestStarted = true;
                        callback.onValueRead(BP_TEST_STARTED, "");
                    }

                    if (s3.length() == "200F0B1A05020009053704AE03008650".length()) {
                        if (s3.startsWith("000")) {
                            btName = "BT:0";
                        } else if (s3.startsWith("100")) {
                            btName = "BT:1";
                        } else if (s3.startsWith("200")) {
                            btName = "BT:2";
                        }
                    }

                    if (f.equals("TIME_SYNC")) {
//                        sendData(bluetoothDevice, "0000fff5-0000-1000-8000-00805f9b34fb", "BT:9");
                        return;
                    }

                    if (!s3.equals("FA5AF1F2FA5AF3F4") && !s3.equals("F5A5F5F6F5A5F7F8") && !s3.equals("FFFFFFFFFFFFFFFF")) {
                        b = s3;
                        d.clear();
                        if (e.toString().length() > 0) {
                            e.delete(0, e.length());
                        }
                    }

                    if (s3.equals("F5A5F5F6F5A5F7F8") && !isWritten) {
                        isWritten = true;
                        sendData(bluetoothDevice, "0000fff5-0000-1000-8000-00805f9b34fb", btName);
                        return;
                    }

                    if (!s3.equals("FA5AF1F2FA5AF3F4") && !s3.equals("F5A5F5F6F5A5F7F8") && !s3.equals("FFFFFFFFFFFFFFFF")) {
                        ModelBloodPressureData data = new ModelBloodPressureData(characteristic.getValue(), b);

                        if (!data.getSystolic().equals("0") && !data.getDiastolic().equals("0")) {
                            modelBloodPressureData = data;
                        }
                    }

                    if (s3.equals("FFFFFFFFFFFFFFFF")) {

                        if (isBpTestStarted) {
                            if (null != modelBloodPressureData) {
                                if (null != modelBloodPressureDataOld && modelBloodPressureData.getDateTime() != modelBloodPressureDataOld.getDateTime()) {
                                    callback.onValueRead(BP_FINAL_RESULT, (modelBloodPressureData.getSystolic() + ";" + modelBloodPressureData.getDiastolic() + ";" + modelBloodPressureData.getPulse()));
                                    modelBloodPressureData = null;
                                    isBpTestStarted        = false;
                                }
                            }
                        } else {
                            modelBloodPressureDataOld = modelBloodPressureData;
                            callback.onValueRead(BP_START_TEST_NOW, "");
                        }
                    }
                }

                break;
            }
        }
    };

    private BluetoothGattCharacteristic mAicareWCharacteristic;
    private BluetoothGattCharacteristic mAicareNCharacteristic;

    private LinkedList<byte[]> mLinkedList;
    private byte[]             authBytes;

    private String bleVersion;
    private int    times = 0;
//    private BleInfo bleInfo;

    @Override
    public void init(Context context, BleCallback callback) {
        this.context  = context;
        this.callback = callback;

        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);

        if (null != bluetoothManager) {
            bluetoothAdapter = bluetoothManager.getAdapter();
        } else {
            callback.onFailure();
        }
    }


    public  byte[]  dateByte;
    private DecInfo decInfo;
    private DecInfo defaultDecInfo;
    private byte[]  encryptBytes;

//    public void handleAiCareData(byte[] bArr) {
//        SparseArray datas = AicareBleConfig.getDatas(bArr);
//        if (datas != null && datas.size() != 0) {
//            if (datas.indexOfKey(0) >= 0) {
//                WeiData weiData = (WeiData) datas.get(0);
//                if (weiData.getDecInfo() != null) {
//                    this.decInfo = weiData.getDecInfo();
////                    this.mCallbacks.getWeightData(weiData);
//                    return;
//                }
//
//                String kgValue = getKgWeight(weiData.getWeight(), weiData.getDecInfo());
//                callback.onValueRead(SWAN_TEST_RESULT, kgValue + "-" + bArr[7]);
//
//                // TODO: 23-03-2020
////                    this.mCallbacks.getWeightData(weiData);
//            } else if (datas.indexOfKey(1) >= 0) {
//                if (((Integer) datas.get(1)).intValue() == 22) {
////                    this.bleRequestDisconnect = true;
//                    disconnect();
//                }
////                this.mCallbacks.getSettingStatus(((Integer) datas.get(1)).intValue());
//            } else if (datas.indexOfKey(2) >= 0) {
////                cancelGetVersionTimer();
////                this.bleVersion = String.valueOf(datas.get(2));
////                this.bleInfo.setVersion(this.bleVersion);
////                if (!AicareBleConfig2.compareAddress(this.device.getAddress()) || !AicareBleConfig2.compareVersion(this.bleVersion.split("_")[0], this.bleVersion.split("_")[1])) {
////                    auth();
////                } else {
////                    onAuthStatus(true);
////                    this.decInfo = this.defaultDecInfo;
////                    returnDecimal();
////                }
//                Log.d(TAG, "handleAiCareData: ");
//            } else if (datas.indexOfKey(3) >= 0) {
//                Log.d(TAG, "handleAiCareData: ");
//            } else if (datas.indexOfKey(4) >= 0) {
//                Log.d(TAG, "handleAiCareData: ");
//            } else if (datas.indexOfKey(5) >= 0) {
//                Log.d(TAG, "handleAiCareData: ");
//            } else if (datas.indexOfKey(6) >= 0) {
//                Log.d(TAG, "handleAiCareData: ");
//            } else if (datas.indexOfKey(7) >= 0) {
//                if (this.decInfo != null) {
//                    FatData fatData = (FatData) datas.get(7);
//                    fatData.setDecInfo(this.decInfo);
//
//                }
//            } else if (datas.indexOfKey(8) >= 0) {
//                if (this.decInfo != null) {
//                    FatData fatData2 = (FatData) datas.get(8);
//                    fatData2.setDecInfo(this.decInfo);
//                }
//            } else if (datas.indexOfKey(9) >= 0) {
//                Log.d(TAG, "handleAiCareData: ");
//
//            } else if (datas.indexOfKey(10) >= 0) {
//
//                Log.d(TAG, "handleAiCareData: ");
////                cancelGetDecimalInfoTimer();
////                this.decInfo = (DecInfo) datas.get(10);
////                if (TextUtils.equals(this.bleVersion, "20181229_1.0")) {
////                    this.decInfo.setLbGraduation(1);
////                }
////                returnDecimal();
//            } else if (datas.indexOfKey(11) >= 0 && this.decInfo != null) {
//                AlgorithmInfo algorithmInfo = (AlgorithmInfo) datas.get(11);
//                algorithmInfo.setDecInfo(this.decInfo);
//            }
//        }
//
////        Log.d(TAG, "handleAiCareDatabytes " + bArr[0] + "  " + bArr[1] + "  " + bArr[2] + "  " + bArr[3] + "  " + bArr[4] + "  " + bArr[5] + "  " + bArr[6] + "  " + bArr[7]);
////
////        WeiData weiData = AicareBleConfig2.getDatas(bArr);
////
////        try {
////            String kgValue = getKgWeight(weiData.getWeight(), weiData.getDecInfo());
////            callback.onValueRead(SWAN_TEST_RESULT, kgValue + "-" + bArr[7]);
////            Log.d(TAG, "handleAiCareData: kg " + kgValue);
////        } catch (Exception e) {
////            Log.d(TAG, "handleAiCareData: error ");
////        }
//    }

    public void handleAiCareData(byte[] bArr) {
        if (bArr[0] == -83 && bArr[1] == 1) {
            boolean compareBytes = AicareBleConfig.compareBytes(bArr, this.encryptBytes);
            cancelAuthTimer();
//            if (TextUtils.isEmpty(this.bleVersion) || VERSION_MIN_DATE.compareTo(this.bleVersion.split("_")[0]) > 0) {
//                this.decInfo = this.defaultDecInfo;
//                returnDecimal();
//            } else {
            getDecimalInfo();
//            }
//            onAuthStatus(compareBytes);
//            this.mCallbacks.getAuthData(this.authBytes, bArr, this.encryptBytes, compareBytes);
            this.authBytes    = null;
            this.encryptBytes = null;
        }
        SparseArray datas = AicareBleConfig.getDatas(bArr);

//        if (times < 10) {
//            getDecimalInfo();
//            ++times;
//        }

//        if (times < 2) {
//            writeValue(this.userIdByte);
//            writeValue(this.userInfoByte);
//        }

        if (datas != null && datas.size() != 0) {
            if (datas.indexOfKey(0) >= 0) {
                WeiData weiData = (WeiData) datas.get(0);
//                if (weiData.getDecInfo() != null) {
//                    this.decInfo = weiData.getDecInfo();
//                    this.mCallbacks.getWeightData(weiData);
//                    return;
//                }
//                DecInfo decInfo2 = this.decInfo;
//                if (decInfo2 != null) {
//                    weiData.setDecInfo(decInfo2);
//                    this.mCallbacks.getWeightData(weiData);
//                }

                String kgValue = getKgWeight(weiData.getWeight(), weiData.getDecInfo());
                callback.onValueRead(SWAN_TEST_RESULT, kgValue);

            } else if (datas.indexOfKey(1) >= 0) {
                int a = datas.indexOfKey(1);
//                if (((Integer) datas.get(1)).intValue() == 22) {
//                    this.bleRequestDisconnect = true;
//                    disconnect();
//                }

                int val = ((Integer) datas.get(1)).intValue();

//                this.mCallbacks.getSettingStatus(((Integer) datas.get(1)).intValue());
                Log.d(TAG, "handleAiCareData:  22222");
            } else if (datas.indexOfKey(2) >= 0) {
                Log.d(TAG, "handleAiCareData: 111111");
                cancelGetVersionTimer();
                bleVersion = String.valueOf(datas.get(2));
//                bleInfo.setVersion(this.bleVersion);
//                if (!AicareBleConfig.compareAddress(this.bluetoothDevice.getDevice().getAddress()) || !AicareBleConfig.compareVersion(this.bleVersion.split("_")[0], this.bleVersion.split("_")[1])) {
//                    auth();
//                } else {
//                    onAuthStatus(true);
//                    this.decInfo = this.defaultDecInfo;
//                }
//                this.mCallbacks.getResult(0, this.bleVersion);
            } else if (datas.indexOfKey(3) >= 0) {
                Log.d(TAG, "handleAiCareData: ");
//                this.mCallbacks.getResult(3, String.valueOf(datas.get(3)));
            } else if (datas.indexOfKey(4) >= 0) {
                Log.d(TAG, "handleAiCareData: ");
//                this.mCallbacks.getResult(1, String.valueOf(datas.get(4)));
            } else if (datas.indexOfKey(5) >= 0) {
                Log.d(TAG, "handleAiCareData: ");
//                this.mCallbacks.getResult(2, String.valueOf(datas.get(5)));
            } else if (datas.indexOfKey(6) >= 0) {
                Log.d(TAG, "handleAiCareData: ");
//                this.mCallbacks.getResult(4, String.valueOf(datas.get(6)));
            } else if (datas.indexOfKey(7) >= 0) {
                Log.d(TAG, "handleAiCareData: ");
//                if (this.decInfo != null) {
//                    FatData fatData = (FatData) datas.get(7);
//                    fatData.setDecInfo(this.decInfo);
//                    this.mCallbacks.getFatData(true, fatData);
//                }
            } else if (datas.indexOfKey(8) >= 0) {
//                if (this.decInfo != null) {
//                    FatData fatData2 = (FatData) datas.get(8);
//                    fatData2.setDecInfo(this.decInfo);
//                    this.mCallbacks.getFatData(false, fatData2);
//                }
                Log.d(TAG, "handleAiCareData: ");
            } else if (datas.indexOfKey(9) >= 0) {
                Log.d(TAG, "handleAiCareData: ");
//                this.mCallbacks.getDID(((Integer) datas.get(9)).intValue());
            } else if (datas.indexOfKey(10) >= 0) {
                Log.d(TAG, "handleAiCareData: ");
                cancelGetDecimalInfoTimer();
                this.decInfo = (DecInfo) datas.get(10);

                Log.d(TAG, "getDecimalInfo: "
                        + decInfo.getSourceDecimal() + ", "
                        + decInfo.getKgDecimal() + ", "
                        + decInfo.getLbDecimal() + ", "
                        + decInfo.getStDecimal() + ", "
                        + decInfo.getKgGraduation() + ", "
                        + decInfo.getLbGraduation() + ", "
                );

//                if (TextUtils.equals(this.bleVersion, "20181229_1.0")) {
//                    this.decInfo.setLbGraduation(1);
//                }
//                returnDecimal();
            } else if (datas.indexOfKey(11) >= 0 && this.decInfo != null) {
                AlgorithmInfo algorithmInfo = (AlgorithmInfo) datas.get(11);
                algorithmInfo.setDecInfo(this.decInfo);
                Log.d(TAG, "handleAiCareData: ");
//                this.mCallbacks.getAlgorithmInfo(algorithmInfo);
            }
        }
    }

    private void startGetDecimalInfoTimer() {
        cancelGetDecimalInfoTimer();
        this.handler.sendEmptyMessageDelayed(1, 2000);
    }

    private void cancelGetDecimalInfoTimer() {
        TimeOutHandler timeOutHandler = this.handler;
        if (timeOutHandler != null) {
            timeOutHandler.removeMessages(1);
        }
    }

    private void startAuthTimer() {
        cancelAuthTimer();
        this.handler.sendEmptyMessageDelayed(2, 1000);
    }

    private void cancelAuthTimer() {
//        TimeOutHandler timeOutHandler = this.handler;
//        if (timeOutHandler != null) {
//            timeOutHandler.removeMessages(2);
//        }
    }

    private void cancelGetVersionTimer() {
        TimeOutHandler timeOutHandler = this.handler;
        if (timeOutHandler != null) {
            timeOutHandler.removeMessages(0);
        }
    }

    public void auth() {
        this.authBytes = AicareBleConfig.getRandomBytes();
        byte[] bArr = this.authBytes;
        this.encryptBytes = AicareBleConfig.encrypt(Arrays.copyOfRange(bArr, 2, bArr.length), false);
        writeValue(this.authBytes);
        startAuthTimer();
    }

    private class TimeOutHandler extends Handler {
        private static final int MAX_GET_DECIMAL_TIMES = 3;
        private              int getDecimalCount;

        private TimeOutHandler() {
            this.getDecimalCount = 0;
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);

            switch (message.what) {
//                case 0:
//                    getVersionFail();
//                    onAuthStatus(false);
//                    getDecimalInfoFail();
//                    returnDecimal();
//                    return;
                case 1:

                    if (this.getDecimalCount < 3) {
                        getDecimalInfo();
                        this.getDecimalCount++;
                        return;
                    }
                    this.getDecimalCount = 0;
//                    getDecimalInfoFail();
//                    returnDecimal();
                    return;
//                case 2:
//                    onAuthStatus(false);
//                    getDecimalInfoFail();
//                    returnDecimal();
//                    return;
//                default:
//                    return;
            }
        }
    }

    public void getDecimalInfo() {
        writeValue(AicareBleConfig.initNewCmd((byte) 4));
        startGetDecimalInfoTimer();
    }

//    public void getVersionFail() {
//        if (wBYManagerCallbacks != null) {
//            wBYManagerCallbacks.getResult(0, "");
//        }
//    }
//
//    public void onAuthStatus(boolean z) {
//        String str = TAG;
//        StringBuilder sb = new StringBuilder();
//        sb.append("isAuth = ");
//        sb.append(z);
//        L.e(str, sb.toString());
//        BleInfo bleInfo2 = this.bleInfo;
//        if (bleInfo2 != null) {
//            bleInfo2.setIsCheck(z ? 1 : 0);
//            if (BleUtils.isConnected(this.mContext)) {
//                new PostInfoTask(BleUtils.getBleJson(this.mContext, this.bleInfo)).execute(new Void[0]);
//            }
//        }
//    }

    public static String getKgWeight(double d, DecInfo decInfo) {

        decInfo = new DecInfo(2, 2, 1, 1, 5, 2);
//        decInfo.setKgDecimal(0);
//        decInfo.setSourceDecimal(2);
//        decInfo.setKgGraduation(0);
//
//        kgDecimal = 2
//        kgGraduation = 5
//        lbDecimal = 1
//        lbGraduation = 2
//        sourceDecimal = 2
//        stDecimal = 1

        long   pow  = (long) Math.pow(10.0d, (double) decInfo.getKgDecimal());
        double pow2 = (double) ((long) Math.pow(10.0d, (double) decInfo.getSourceDecimal()));

        double d2 = d / pow2;
        double d3 = (double) pow;

        long j = (long) ((d2 * d3) + 0.5d);

        if (decInfo.getKgGraduation() != 0) {
            j = (j / ((long) decInfo.getKgGraduation())) * ((long) decInfo.getKgGraduation());
        }

        float value = (((float) j) * 1.0f) / ((float) pow);

        String val = String.valueOf(value);

        Log.d(TAG, "getKgWeight: " + d + "   -    " + val);

        return val;
    }

    @Override
    public void startScan() {
        if (!bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
        } else {
            bluetoothAdapter.startLeScan(leScanCallback);
        }

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(ACTION_DISCOVERY_STARTED);
        intentFilter.addAction(ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(this, intentFilter);

        bluetoothAdapter.cancelDiscovery();
        bluetoothAdapter.startDiscovery();
    }

    @Override
    public void stopScan() {
        bluetoothAdapter.stopLeScan(leScanCallback);
    }

    @Override
    public void connectDevice(BluetoothDevice device) {
        if (null != bluetoothDevice) {
            disconnect();
        }

        if (device.getName().equals(CHIPSEA_DEVICE) || device.getName().equals(SWAN_DEVICE)) {
            bluetoothDevice = device.connectGatt(context, false, gattConnectionCallback);
        } else {
            bluetoothDevice = device.connectGatt(context, true, gattConnectionCallback);
        }
    }

    @Override
    public void disconnect() {
        if (null != bluetoothDevice) {
            bluetoothAdapter.stopLeScan(leScanCallback);
            bluetoothDevice.disconnect();
        }

        try {
            bluetoothAdapter.cancelDiscovery();
        } catch (Exception e) {
        }

        try {
            ashaSocket.close();
            ashaAcceptThread.cancel();
        } catch (Exception e) {
        }

        try {
            LinkedList<byte[]> linkedList = this.mLinkedList;
            if (linkedList != null) {
                linkedList.clear();
            }
        } catch (Exception e) {
        }
    }

    public void turnOffBle() {
        if (bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
        }
    }

    @Override
    public void getBatteryLevel() {

        switch (bluetoothDevice.getDevice().getName()) {

            case SYNCPLUS_DEVICE:
            case SYNC_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(SYNC_BATTERY_LEVEL_SERVICE, SYNC_LEVEL_CHARACTERISTIC);
                if (null != gattCharacteristic) {
                    bluetoothDevice.readCharacteristic(gattCharacteristic);
                }
            }
            break;

            case TECHNAXX_BP_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(TECHNAXX_BP_BATTERY_LEVEL_SERVICE, TECHNAXX_BP_BATTERY_LEVEL_CHARACTERISTIC);

                if (null != gattCharacteristic) {

                    int i = gattCharacteristic.getProperties();

                    if ((i | 0x2) > 0) {
                        this.bluetoothDevice.readCharacteristic(gattCharacteristic);
                    }
                    if ((i | 0x10) > 0) {
                        bluetoothDevice.setCharacteristicNotification(gattCharacteristic, true);
                    }
                }
            }
            break;
        }
    }

    @Override
    public void startTest() {
        switch (bluetoothDevice.getDevice().getName()) {

            case SYNCPLUS_DEVICE:
            case SYNC_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(SYNC_START_TEST_SERVICE, SYNC_START_TEST_CHARACTERISTIC);
                if (null != gattCharacteristic) {
                    sendNotification(gattCharacteristic, true);
                }
            }
            break;

            case TECHNAXX_BP_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(TECHNAXX_BP_START_TEST_SERVICE, TECHNAXX_BP_START_TEST_CHARACTERISTIC);
                if (null != gattCharacteristic) {
                    gattCharacteristic.setValue(hexStringToByteArray("0x0240dc01a13c"));
                    bluetoothDevice.writeCharacteristic(gattCharacteristic);
                }
            }
            break;

            case CHIPSEA_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(CHIPSEA_START_TEST_SERVICE, CHIPSEA_START_TEST_CHARACTERISTIC);
                if (null != gattCharacteristic) {
                    sendNotification(gattCharacteristic, true);
                }
            }
            break;

            case SWAN_DEVICE: {
                BluetoothGattService       service  = bluetoothDevice.getService(UUID.fromString(SWAN_START_TEST_SERVICE));
                List<BluetoothGattService> services = bluetoothDevice.getServices();

                if (services.contains(bluetoothDevice.getService(UUID.fromString(SWAN_START_TEST_SERVICE)))) {
                    mAicareWCharacteristic = service.getCharacteristic(AICARE_WRITE_CHARACTERISTIC_UUID);
                    mAicareNCharacteristic = service.getCharacteristic(AICARE_NOTIFY_CHARACTERISTIC_UUID);
                    this.bluetoothDevice.setCharacteristicNotification(this.mAicareNCharacteristic, true);
                    BluetoothGattDescriptor descriptor = this.mAicareNCharacteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    this.bluetoothDevice.writeDescriptor(descriptor);
                }

//                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(SWAN_START_TEST_SERVICE, SWAN_START_TEST_CHARACTERISTIC);
//                if (null != gattCharacteristic) {
//                    sendNotification(gattCharacteristic, true);
//                }
            }
            break;

            case BERRY_MED_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(UUID_SERVICE_DATA, UUID_CHARACTER_RECEIVE);
                if (null != gattCharacteristic) {
                    sendNotification(gattCharacteristic, true);
                }
            }
            break;

            case SFBP: {
                isWritten              = false;
                modelBloodPressureData = null;
                getchar(bluetoothDevice);
                callback.onValueRead(BP_WAIT, "");
            }
            break;
        }
    }

    public static final UUID AICARE_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ffb2-0000-1000-8000-00805f9b34fb");
    public static final UUID AICARE_WRITE_CHARACTERISTIC_UUID  = UUID.fromString("0000ffb1-0000-1000-8000-00805f9b34fb");

    private void getchar(BluetoothGatt bluetoothgatt) {
        Iterator iterator = bluetoothgatt.getServices().iterator();
        label0:
        do {
            if (!iterator.hasNext()) {
                break;
            }
            Iterator iterator1 = ((BluetoothGattService) iterator.next()).getCharacteristics().iterator();
            Object   obj;
            do {
                do {
                    if (!iterator1.hasNext()) {
                        continue label0;
                    }
                    obj = (BluetoothGattCharacteristic) iterator1.next();
                }
                while (!((BluetoothGattCharacteristic) (obj)).getUuid().toString().equals("0000fff4-0000-1000-8000-00805f9b34fb"));
                bluetoothgatt.setCharacteristicNotification(((BluetoothGattCharacteristic) (obj)), true);
                obj = ((BluetoothGattCharacteristic) (obj)).getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
            } while (obj == null);
            ((BluetoothGattDescriptor) (obj)).setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            boolean flag = bluetoothgatt.writeDescriptor(((BluetoothGattDescriptor) (obj)));
//            LLog.e(a, (new StringBuilder()).append("listServices: ").append(((BluetoothGattDescriptor) (obj)).getUuid()).append(" = is write:  ").append(flag).toString());
        } while (true);
    }

    @Override
    public void stopTest() {

        switch (bluetoothDevice.getDevice().getName()) {

            case SYNCPLUS_DEVICE:
            case SYNC_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(SYNC_START_TEST_SERVICE, SYNC_START_TEST_CHARACTERISTIC);

                if (null != gattCharacteristic) {
                    sendNotification(gattCharacteristic, false);
                }
            }
            break;

            case TECHNAXX_BP_DEVICE: {
                BluetoothGattCharacteristic gattCharacteristic = getGattCharacteristic(TECHNAXX_BP_START_TEST_SERVICE, TECHNAXX_BP_START_TEST_CHARACTERISTIC);

                if (null != gattCharacteristic) {
                    sendNotification(gattCharacteristic, false);
                }
            }
            break;
            // TODO: 17-Sep-18 stop berry oxi
        }
    }

    private BluetoothGattCharacteristic getGattCharacteristic(String serviceString, String characteristicString) {

        if (null == serviceList) {
            return null;
        }

        for (BluetoothGattService service : serviceList) {

            if (service.getUuid().toString().equals(serviceString)) {

                List<BluetoothGattCharacteristic> gattCharacteristics = service
                        .getCharacteristics();

                for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {

                    String uuidChar = gattCharacteristic.getUuid().toString();

                    if (uuidChar.equalsIgnoreCase(characteristicString)) {
                        return gattCharacteristic;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void deInit() {
        try {
            context.unregisterReceiver(this);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    private void sendNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {

        try {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID
                    .fromString(CLIENT_CHARACTERISTIC_CONFIG));

            if (enabled) {
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                bluetoothDevice.writeDescriptor(descriptor);

            } else {
                descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                bluetoothDevice.writeDescriptor(descriptor);
            }

            bluetoothDevice.setCharacteristicNotification(characteristic, enabled);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
            int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);

            switch (state) {
                case BluetoothAdapter.STATE_ON:
                    bluetoothAdapter.startLeScan(leScanCallback);
                    break;
            }

        } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            String deName = device.getName() + " - " + device.getAddress();

            callback.onBleScan(device);
        }
    }

    @Override
    public void onPulseWaveDetected() {
        LLog.d(TAG, "onPulseWaveDetected: ");
    }

    @Override
    public void onSpO2ParamsChanged() {
        LLog.d(TAG, "onSpO2ParamsChanged: ");

        int spo2      = this.parseSteamRunnable.getOxiParams().getSpo2();
        int pulseRate = this.parseSteamRunnable.getOxiParams().getPulseRate();
        int pi        = this.parseSteamRunnable.getOxiParams().getPi();

        float piFloat = ParseSteamRunnable.getFloatPi(pi);

        callback.onValueRead(BERRY_MED_DEVICE_TEST_RESULT, spo2 + "," + pulseRate + "," + piFloat);
    }

    @Override
    public void onSpO2WaveChanged(int paramInt) {
        LLog.d(TAG, "onSpO2WaveChanged: ");
    }

    public void connectToAsha(BluetoothDevice device) {
        try {
            ashaSocket = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
            ashaSocket.connect();

            ashaAcceptThread = new AshaAcceptThread();
            ashaAcceptThread.start();

            callback.onConnected();

        } catch (Exception e) {
            callback.onDisconnected();
        }
    }

    public void startAshaTest(int testType) {
        try {
            switch (testType) {
                case ASHA_START_TEST_BP: {
                    ashaSocket.getOutputStream().write("b".getBytes("UTF-8"));
                    ashaSocket.getOutputStream().flush();
                    break;
                }
                case ASHA_START_TEST_SPO2: {
                    ashaSocket.getOutputStream().write("p".getBytes("UTF-8"));
                    ashaSocket.getOutputStream().flush();
                    break;
                }
                case ASHA_START_TEST_GLUCOSE: {
                    ashaSocket.getOutputStream().write("g".getBytes("UTF-8"));
                    ashaSocket.getOutputStream().flush();
                    break;
                }
            }
        } catch (Exception e) {
        }
    }

    public void stopAshaTest() {
        try {
            ashaSocket.getOutputStream().write("x".getBytes("UTF-8"));
            ashaSocket.getOutputStream().flush();
        } catch (Exception e) {
        }
    }

    private class AshaAcceptThread extends Thread {

        private BluetoothServerSocket serverSocket;

        public AshaAcceptThread() {
            try {
                this.serverSocket = bluetoothAdapter.listenUsingRfcommWithServiceRecord("BluetoothListen", ASHA_UUID);
            } catch (IOException this$1) {
                callback.onDisconnected();
            }
        }

        public void cancel() {
            try {
                this.serverSocket.close();
                return;
            } catch (IOException localIOException) {
                callback.onDisconnected();
            }
        }

        public void run() {
            while (true) {
                try {
                    final byte[] bytes = new byte[4096];

                    InputStream inputStream = ashaSocket.getInputStream();

                    int value = inputStream.read(bytes);

                    callback.onValueRead(ASHA_TEST_RESULT, new String(bytes));

                    Thread.sleep(1000);

                } catch (Exception e) {
                    callback.onDisconnected();
                    break;
                }
            }
        }
    }

    public Handler mHandler = new Handler(Looper.myLooper()) {
        public void handleMessage(Message message) {
            if (message.what == 1 && mLinkedList.size() > 0) {
                byte[] bArr = (byte[]) mLinkedList.pollLast();
                if (!(bArr == null || mAicareWCharacteristic == null)) {
                    mAicareWCharacteristic.setValue(bArr);
                    mAicareWCharacteristic.setWriteType(1);
                    if (bluetoothDevice.writeCharacteristic(mAicareWCharacteristic)) {
                        Log.d(TAG, "handleMessage: ");
                    }
                }
                mHandler.sendEmptyMessageDelayed(1, 200);
            }
        }
    };

    public void writeValue(byte[] bArr) {
        if (hasAicareUUID()) {
            if (this.mLinkedList == null) {
                this.mLinkedList = new LinkedList<>();
            }
            this.mLinkedList.addFirst(bArr);
            if (this.mLinkedList.size() <= 1) {
                mHandler.removeMessages(1);
                mHandler.sendEmptyMessageDelayed(1, 100);
            }
        }
    }

    public boolean hasAicareUUID() {
        return this.mAicareWCharacteristic != null;
    }

    public void sendCmd(byte b, byte b2) {
        writeValue(initCmd(b, null, b2));
    }

    public static byte[] initCmd(byte b, User user, byte b2) {
        byte[] bArr = new byte[8];
        bArr[0] = -84;
        bArr[1] = 2;

        if (b == -9) {
            bArr[2] = -9;
            bArr[6] = -52;
        }

        bArr[7] = getByteSum(bArr, 2, 7);

        return bArr;
    }

    private static byte getByteSum(byte[] bArr, int i, int i2) {
        int i3 = 0;
        while (i < i2) {
            i3 += bArr[i];
            i++;
        }
        return (byte) (i3 & 255);
    }
}