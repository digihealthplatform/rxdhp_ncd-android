package com.dhp.screening.lib.aicare;

public class BM15Data {
    private int    adc;
    private String address;
    private int    agreementType;
    private int    algorithmType;
    private int    bleType;
    private int    did;
    private float  temp;
    private int    unitType;
    private String version;
    private float  weight;

    public BM15Data() {
    }

    public BM15Data(String str, int i, int i2, float f, float f2, int i3, int i4, int i5, int i6, String str2) {
        this.version = str;
        this.agreementType = i;
        this.unitType = i2;
        this.weight = f;
        this.temp = f2;
        this.adc = i3;
        this.algorithmType = i4;
        this.did = i5;
        this.bleType = i6;
        this.address = str2;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String str) {
        this.version = str;
    }

    public int getAgreementType() {
        return this.agreementType;
    }

    public void setAgreementType(int i) {
        this.agreementType = i;
    }

    public int getUnitType() {
        return this.unitType;
    }

    public void setUnitType(int i) {
        this.unitType = i;
    }

    public float getWeight() {
        return this.weight;
    }

    public void setWeight(float f) {
        this.weight = f;
    }

    public float getTemp() {
        return this.temp;
    }

    public void setTemp(float f) {
        this.temp = f;
    }

    public int getAdc() {
        return this.adc;
    }

    public void setAdc(int i) {
        this.adc = i;
    }

    public int getAlgorithmType() {
        return this.algorithmType;
    }

    public void setAlgorithmType(int i) {
        this.algorithmType = i;
    }

    public int getDid() {
        return this.did;
    }

    public void setDid(int i) {
        this.did = i;
    }

    public int getBleType() {
        return this.bleType;
    }

    public void setBleType(int i) {
        this.bleType = i;
    }

    public String getAddress() {
        return this.address;
    }

    public void setAddress(String str) {
        this.address = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BM15Data{version(版本)='");
        sb.append(this.version);
        sb.append('\'');
        sb.append(", agreementType(命令)=");
        sb.append(this.agreementType);
        sb.append(", unitType(单位)=");
        sb.append(this.unitType);
        sb.append(", weight(重量)=");
        sb.append(this.weight);
        sb.append(", temp(温度)=");
        sb.append(this.temp);
        sb.append(", adc(阻抗)=");
        sb.append(this.adc);
        sb.append(", algorithmType(算法)=");
        sb.append(this.algorithmType);
        sb.append(", did=");
        sb.append(this.did);
        sb.append(", bleType=");
        sb.append(this.bleType);
        sb.append(", address='");
        sb.append(this.address);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }
}
