package com.dhp.screening.lib.aicare;

import android.os.ParcelUuid;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class ScanRecord {
    private static final int                     DATA_TYPE_FLAGS                          = 1;
    private static final int                     DATA_TYPE_LOCAL_NAME_COMPLETE            = 9;
    private static final int                     DATA_TYPE_LOCAL_NAME_SHORT               = 8;
    private static final int                     DATA_TYPE_MANUFACTURER_SPECIFIC_DATA     = 255;
    private static final int                     DATA_TYPE_SERVICE_DATA                   = 22;
    private static final int                     DATA_TYPE_SERVICE_UUIDS_128_BIT_COMPLETE = 7;
    private static final int                     DATA_TYPE_SERVICE_UUIDS_128_BIT_PARTIAL  = 6;
    private static final int                     DATA_TYPE_SERVICE_UUIDS_16_BIT_COMPLETE  = 3;
    private static final int                     DATA_TYPE_SERVICE_UUIDS_16_BIT_PARTIAL   = 2;
    private static final int                     DATA_TYPE_SERVICE_UUIDS_32_BIT_COMPLETE  = 5;
    private static final int                     DATA_TYPE_SERVICE_UUIDS_32_BIT_PARTIAL   = 4;
    private static final int                     DATA_TYPE_TX_POWER_LEVEL                 = 10;
    private static final String                  TAG                                      = "ScanRecord";
    private final        int                     mAdvertiseFlags;
    private final        byte[]                  mBytes;
    private final        String                  mDeviceName;
    private final        SparseArray<byte[]>     mManufacturerSpecificData;
    private final        Map<ParcelUuid, byte[]> mServiceData;
    @Nullable
    private final        List<ParcelUuid>        mServiceUuids;
    private final        int                     mTxPowerLevel;

    public int getAdvertiseFlags() {
        return this.mAdvertiseFlags;
    }

    public List<ParcelUuid> getServiceUuids() {
        return this.mServiceUuids;
    }

    public SparseArray<byte[]> getManufacturerSpecificData() {
        return this.mManufacturerSpecificData;
    }

    @Nullable
    public byte[] getManufacturerSpecificData(int i) {
        return (byte[]) this.mManufacturerSpecificData.get(i);
    }

    public Map<ParcelUuid, byte[]> getServiceData() {
        return this.mServiceData;
    }

    @Nullable
    public byte[] getServiceData(ParcelUuid parcelUuid) {
        if (parcelUuid == null) {
            return null;
        }
        return (byte[]) this.mServiceData.get(parcelUuid);
    }

    public int getTxPowerLevel() {
        return this.mTxPowerLevel;
    }

    @Nullable
    public String getDeviceName() {
        return this.mDeviceName;
    }

    public byte[] getBytes() {
        return this.mBytes;
    }

    private ScanRecord(List<ParcelUuid> list, SparseArray<byte[]> sparseArray, Map<ParcelUuid, byte[]> map, int i, int i2, String str, byte[] bArr) {
        this.mServiceUuids = list;
        this.mManufacturerSpecificData = sparseArray;
        this.mServiceData = map;
        this.mDeviceName = str;
        this.mAdvertiseFlags = i;
        this.mTxPowerLevel = i2;
        this.mBytes = bArr;
    }

    public static ScanRecord parseFromBytes(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        int         i           = 0;
        ArrayList   arrayList   = new ArrayList();
        SparseArray sparseArray = new SparseArray();
        HashMap     hashMap     = new HashMap();
        String      str         = null;
        byte        b           = -1;
        byte        b2          = (byte) -2147483648;
        while (true) {
            try {
                if (i < bArr.length) {
                    int i2 = i + 1;
                    byte b3 = (byte) (bArr[i] & AicareBleConfig.SYNC_HISTORY);
                    if (b3 != 0) {
                        int i3 = b3 - 1;
                        int i4 = i2 + 1;
                        byte b4 = (byte) (bArr[i2] & AicareBleConfig.SYNC_HISTORY);
                        if (b4 == 22) {
                            hashMap.put(BluetoothUuid.parseUuidFrom(extractBytes(bArr, i4, 2)), extractBytes(bArr, i4 + 2, i3 - 2));
                        } else if (b4 != 255) {
                            switch (b4) {
                                case 1:
                                    b = (byte) (bArr[i4] & AicareBleConfig.SYNC_HISTORY);
                                    break;
                                case 2:
                                case 3:
                                    parseServiceUuid(bArr, i4, i3, 2, arrayList);
                                    break;
                                case 4:
                                case 5:
                                    parseServiceUuid(bArr, i4, i3, 4, arrayList);
                                    break;
                                case 6:
                                case 7:
                                    parseServiceUuid(bArr, i4, i3, 16, arrayList);
                                    break;
                                case 8:
                                case 9:
                                    str = new String(extractBytes(bArr, i4, i3));
                                    break;
                                case 10:
                                    b2 = bArr[i4];
                                    break;
                            }
                        } else {
                            sparseArray.put(((bArr[i4 + 1] & AicareBleConfig.SYNC_HISTORY) << 8) + (255 & bArr[i4]), extractBytes(bArr, i4 + 2, i3 - 2));
                        }
                        i = i3 + i4;
                    }
                }
            } catch (Exception unused) {
                String        str2 = TAG;
                StringBuilder sb   = new StringBuilder();
                sb.append("unable to parse scan record: ");
                sb.append(Arrays.toString(bArr));
                Log.e(str2, sb.toString());
                ScanRecord scanRecord = new ScanRecord(null, null, null, -1, Integer.MIN_VALUE, null, bArr);
                return scanRecord;
            }
        }
//        ScanRecord scanRecord2 = new ScanRecord(arrayList.isEmpty() ? null : arrayList, sparseArray, hashMap, b, b2, str, bArr);
//        return scanRecord2;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ScanRecord [mAdvertiseFlags=");
        sb.append(this.mAdvertiseFlags);
        sb.append(", mServiceUuids=");
        sb.append(this.mServiceUuids);
        sb.append(", mManufacturerSpecificData=");
        sb.append(BluetoothLeUtils.toString(this.mManufacturerSpecificData));
        sb.append(", mServiceData=");
        sb.append(BluetoothLeUtils.toString(this.mServiceData));
        sb.append(", mTxPowerLevel=");
        sb.append(this.mTxPowerLevel);
        sb.append(", mDeviceName=");
        sb.append(this.mDeviceName);
        sb.append("]");
        return sb.toString();
    }

    private static int parseServiceUuid(byte[] bArr, int i, int i2, int i3, List<ParcelUuid> list) {
        while (i2 > 0) {
            list.add(BluetoothUuid.parseUuidFrom(extractBytes(bArr, i, i3)));
            i2 -= i3;
            i += i3;
        }
        return i;
    }

    private static byte[] extractBytes(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }
}
