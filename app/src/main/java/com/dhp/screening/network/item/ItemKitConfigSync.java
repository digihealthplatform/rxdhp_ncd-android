package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemKitConfigSync {

    @SerializedName("kitConfigAutoNumber")
    private int tableKey;

    @SerializedName("kitConfigType")
    private String type = "";

    @SerializedName("kitConfigMake")
    private String make = "";

    @SerializedName("kitConfigModel")
    private String model = "";

    @SerializedName("kitConfigProductName")
    private String productName = "";

    @SerializedName("kitConfigProductId")
    private String productId = "";

    @SerializedName("kitConfigMacAddress")
    private String macAddress = "";

    @SerializedName("kitConfigValidStartDate")
    private String validStartDate = "";

    @SerializedName("kitConfigValidEndDate")
    private String validEndDate = "";

    @SerializedName("kitConfigNotes")
    private String notes;

    @SerializedName("kitConfigPartnerId")
    private String partnerId;

    @SerializedName("kitConfigDeviceNumber")
    private String deviceNumber;

    @SerializedName("kitConfigDeviceId")
    private String deviceId;

    @SerializedName("kitConfigEntryDateTime")
    private String entryDateTime = "";

    @SerializedName("kitConfigEntryDateTimeMilliSeconds")
    private long entryDateTimeMillis;

    @SerializedName("kitConfigInactive")
    private boolean inactive;

    @SerializedName("kitConfigInactiveReason")
    private String inactiveReason = "";

    @SerializedName("kitConfigModified")
    private boolean modified;

    @SerializedName("kitConfigModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("kitConfigSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public String getValidStartDate() {
        return validStartDate;
    }

    public void setValidStartDate(String validStartDate) {
        this.validStartDate = validStartDate;
    }

    public String getValidEndDate() {
        return validEndDate;
    }

    public void setValidEndDate(String validEndDate) {
        this.validEndDate = validEndDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(String entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public long getEntryDateTimeMillis() {
        return entryDateTimeMillis;
    }

    public void setEntryDateTimeMillis(long entryDateTimeMillis) {
        this.entryDateTimeMillis = entryDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}