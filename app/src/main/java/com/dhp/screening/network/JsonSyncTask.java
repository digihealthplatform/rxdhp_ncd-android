package com.dhp.screening.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.dhp.screening.BuildConfig;
import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.data.table.TableKitConfig;
import com.dhp.screening.data.table.TableProblems;
import com.dhp.screening.data.table.TableSociodemographics;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.network.item.ItemCampInfoSync;
import com.dhp.screening.network.item.ItemDeathInfoSync;
import com.dhp.screening.network.item.ItemDemographicsSync;
import com.dhp.screening.network.item.ItemHouseSync;
import com.dhp.screening.network.item.ItemImageSync;
import com.dhp.screening.network.item.ItemKitConfigSync;
import com.dhp.screening.network.item.ItemProblemSync;
import com.dhp.screening.network.item.ItemSociodemographicSync;
import com.dhp.screening.network.item.ItemStockSync;
import com.dhp.screening.network.item.ItemUserSync;
import com.dhp.screening.network.item.ItemVitalSync;
import com.dhp.screening.util.LConstants;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.dhp.screening.util.LUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import es.dmoral.toasty.Toasty;

import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_LAST_SYNC_DATE_TIME;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_LAST_SYNC_MILLIS;
import static com.dhp.screening.util.LConstants.JSON_SYNC_URL;
import static com.dhp.screening.util.LConstants.SCREEN_TYPE;
import static com.dhp.screening.util.LUtils.getLocalFilesPath;
import static com.dhp.screening.util.LUtils.hasValue;
import static com.dhp.screening.util.LUtils.isConnectedToInternet;

public class JsonSyncTask extends AsyncTask<String, Integer, String> {

    private final DataManager dataManager;

    private final boolean isBackground;

    private boolean serverError;
    private boolean connectionError;
    private boolean syncHappened;

    private ProgressDialog progressDialog;

    private Context context;

    private Gson gson;

    private File tempFolder;

    private CallBack callback;

    public interface CallBack {

        void onSyncStarted();

        void onSyncComplete();

        void onSyncFailed(String message);
    }

    public JsonSyncTask(Context context, DataManager dataManager, boolean isBackground) {
        this.context = context;
        this.dataManager = dataManager;
        this.isBackground = isBackground;
        gson = new GsonBuilder().disableHtmlEscaping().create();
    }

    public void setCallback(CallBack callback) {
        this.callback = callback;
    }

    @Override
    protected void onPreExecute() {
        if (!isBackground) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(AssetReader.getLangKeyword("sync_in_progress"));
            progressDialog.show();
        }

        callback.onSyncStarted();
    }

    @Override
    protected void onPostExecute(String result) {
        if (!isBackground) {
            progressDialog.dismiss();

            if (serverError) {
                callback.onSyncFailed(AssetReader.getLangKeyword("server_error"));

            } else if (connectionError) {
                if (isConnectedToInternet(context)) {
                    callback.onSyncFailed(AssetReader.getLangKeyword("server_error"));
                } else {
                    callback.onSyncFailed(AssetReader.getLangKeyword("no_internet"));
                }

            } else {
                if (syncHappened) {
                    dataManager.setPref(KEY_LAST_SYNC_DATE_TIME, LUtils.getCurrentDateTime());
                    dataManager.setPref(KEY_LAST_SYNC_MILLIS, LUtils.getCurrentDateTimeMillis());
                    Toasty.success(context, AssetReader.getLangKeyword("sync_complete"), Toast.LENGTH_LONG).show();
                    callback.onSyncComplete();

                } else {
                    LToast.toast(AssetReader.getLangKeyword("no_data_to_sync"));
                    callback.onSyncComplete();
                }
            }
        } else {
            if (syncHappened) {
                dataManager.setPref(KEY_LAST_SYNC_DATE_TIME, LUtils.getCurrentDateTime());
                dataManager.setPref(KEY_LAST_SYNC_MILLIS, LUtils.getCurrentDateTimeMillis());
                callback.onSyncComplete();

            } else {
//                if (serverError) {
//                    callback.onSyncFailed(AssetReader.getLangKeyword("server_error"));
//
//                } else if (connectionError) {
//                    if (isConnectedToInternet(context)) {
//                        callback.onSyncFailed(AssetReader.getLangKeyword("server_error"));
//                    } else {
//                        callback.onSyncFailed(null);
//                    }
//                }

                callback.onSyncFailed(null);
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {

        tempFolder = new File(Environment.getExternalStorageDirectory(), context.getResources().getString(R.string.temp_folder));

        if (!tempFolder.exists()) {
            tempFolder.mkdirs();
        }

        uploadCampInfo();
        uploadUsers();
        uploadDemographics();
        uploadSociodemographics();
        uploadProblems();
        uploadHouseInfo();
        uploadDeathInfo();
        uploadVitals();
        uploadStocks();
        uploadKitInfo();
        uploadImages();

        try {
            tempFolder.delete();
        } catch (Exception e) {

        }
        return null;
    }

    private void uploadCampInfo() {
        List<TableCampInfo> tableCampInfoList = dataManager.getCampInfo(false);

        for (TableCampInfo tableCampInfo : tableCampInfoList) {
            try {
                ItemCampInfoSync itemCampInfoSync = new ItemCampInfoSync();
                itemCampInfoSync.setTableKey(tableCampInfo.getTableKey());
                itemCampInfoSync.setCampName(tableCampInfo.getCampName());
                itemCampInfoSync.setPartnerId(tableCampInfo.getPartnerId());
                itemCampInfoSync.setStartDate(tableCampInfo.getStartDate());
                itemCampInfoSync.setEndDate(tableCampInfo.getEndDate());
                itemCampInfoSync.setDoorToDoor(tableCampInfo.isDoorToDoor());
                itemCampInfoSync.setDeviceId(tableCampInfo.getDeviceId());
                itemCampInfoSync.setEntryDateTime(tableCampInfo.getEntryDateTime());
                itemCampInfoSync.setEntryDateTimeMillis(tableCampInfo.getEntryDateTimeMillis());

                itemCampInfoSync.setModified(tableCampInfo.isModified());
                itemCampInfoSync.setModifiedDateTime(tableCampInfo.getModifiedDateTime());
                itemCampInfoSync.setSynced(tableCampInfo.isSynced());
                itemCampInfoSync.setVersion(AssetReader.getVersion());
                itemCampInfoSync.setAppVersion(BuildConfig.VERSION_NAME);

                String vitalJson = "{ \"campInfo\": " + gson.toJson(itemCampInfoSync) + "}";
                if (createAndUploadFile(getJsonFilePrefix() + "cmp_" + tableCampInfo.getEntryDateTimeMillis() + ".json", vitalJson, false)) {
                    tableCampInfo.setSynced(true);
                    tableCampInfo.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadUsers() {
        List<TableUser> tableUsers = dataManager.getAllUsers(false);

        for (TableUser tableUser : tableUsers) {
            try {
                ItemUserSync itemUserSync = new ItemUserSync();
                itemUserSync.setTableKey(tableUser.getTableKey());
                itemUserSync.setUserFullName(tableUser.getFullName());
                itemUserSync.setUserId(tableUser.getUserId());
                itemUserSync.setPassword(tableUser.getPassword());
                itemUserSync.setUserType(tableUser.getUserType());

                itemUserSync.setGender(tableUser.getGender());
                itemUserSync.setDob(tableUser.getDob());
                itemUserSync.setYob(tableUser.getYob());
                itemUserSync.setIdentityType(tableUser.getIdentityType());
                itemUserSync.setIdentityNumber(tableUser.getIdentityNumber());
                itemUserSync.setPhoneNumber(tableUser.getPhoneNumber());
                itemUserSync.setEmailId(tableUser.getEmailId());
                itemUserSync.setHouseNo(tableUser.getHouseNo());
                itemUserSync.setStreet(tableUser.getStreet());
                itemUserSync.setPost(tableUser.getPost());
                itemUserSync.setLocality(tableUser.getLocality());
                itemUserSync.setDistrict(tableUser.getDistrict());
                itemUserSync.setState(tableUser.getState());
                itemUserSync.setCountry(tableUser.getCountry());
                itemUserSync.setPinCode(tableUser.getPinCode());
                itemUserSync.setAllowPrescription(tableUser.isAllowPrescription());
                itemUserSync.setUserIsDemo(tableUser.isDemoUser());
                itemUserSync.setQrScanned(tableUser.isQrScanned());
                itemUserSync.setPartnerId(tableUser.getPartnerId());
                itemUserSync.setCampName(tableUser.getCampName());
                itemUserSync.setDeviceNumber(tableUser.getDeviceNumber());
                itemUserSync.setDeviceId(tableUser.getDeviceId());
                itemUserSync.setUserEntryDateTime(tableUser.getEntryDateTime());
                itemUserSync.setUserEntryDateTimeMillis(tableUser.getEntryDateTimeMillis());
                itemUserSync.setInactive(tableUser.isInactive());
                itemUserSync.setInactiveReason(tableUser.getInactiveReason());
                itemUserSync.setModified(tableUser.isModified());
                itemUserSync.setModifiedDateTime(tableUser.getModifiedDateTime());
                itemUserSync.setSynced(tableUser.isSynced());
                itemUserSync.setVersion(AssetReader.getVersion());
                itemUserSync.setAppVersion(BuildConfig.VERSION_NAME);

                String vitalJson = "{ \"userInfo\": " + gson.toJson(itemUserSync) + "}";
                if (createAndUploadFile(getJsonFilePrefix() + "usr_" + tableUser.getEntryDateTimeMillis() + ".json", vitalJson, false)) {
                    tableUser.setSynced(true);
                    tableUser.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadDemographics() {
        List<TableDemographics> tableDemographicList = dataManager.getAllPatients(false);

        for (TableDemographics tableDemographics : tableDemographicList) {
            try {
                ItemDemographicsSync itemDemographics = new ItemDemographicsSync();
                itemDemographics.setTableKey(tableDemographics.getTableKey());
                itemDemographics.setRegistrationId(tableDemographics.getRegistrationId());
                itemDemographics.setEhrNumber(tableDemographics.getEhrNumber());
                itemDemographics.setFullName(tableDemographics.getFullName());
                itemDemographics.setGender(tableDemographics.getGender());
                itemDemographics.setYob(tableDemographics.getYob());
                itemDemographics.setDob(tableDemographics.getDob());
                itemDemographics.setPhoneNumber(tableDemographics.getPhoneNumber());
                itemDemographics.setEmailId(tableDemographics.getEmailId());
                itemDemographics.setIdentityType(tableDemographics.getIdentityType());
                itemDemographics.setIdentityNumber(tableDemographics.getIdentityNumber());
                itemDemographics.setHouseId(tableDemographics.getHouseId());
                itemDemographics.setCareOf(tableDemographics.getCareOf());
                itemDemographics.setImageName(tableDemographics.getImageName());
                itemDemographics.setImagePath(tableDemographics.getImagePath());
                itemDemographics.setNotes(tableDemographics.getNotes());
                itemDemographics.setDoorToDoor(tableDemographics.isDoorToDoor());
                itemDemographics.setGpsLocation(tableDemographics.getGpsLocation());
                itemDemographics.setQrScanned(tableDemographics.isQrScanned());
                itemDemographics.setSurveyorUserId(tableDemographics.getSurveyorUserId());
                itemDemographics.setPartnerId(tableDemographics.getPartnerId());
                itemDemographics.setCampName(tableDemographics.getCampName());
                itemDemographics.setDeviceNumber(tableDemographics.getDeviceNumber());
                itemDemographics.setDeviceId(tableDemographics.getDeviceId());
                itemDemographics.setSurveyDateTime(tableDemographics.getSurveyDateTime());
                itemDemographics.setSurveyDateTimeMillis(tableDemographics.getSurveyDateTimeMillis());
                itemDemographics.setInactive(tableDemographics.isInactive());
                itemDemographics.setInactiveReason(tableDemographics.getInactiveReason());
                itemDemographics.setModified(tableDemographics.isModified());
                itemDemographics.setModifiedDateTime(tableDemographics.getModifiedDateTime());
                itemDemographics.setSynced(tableDemographics.isSynced());
                itemDemographics.setMrNumber(tableDemographics.getMrNumber());

                itemDemographics.setHouseFileName(getHouseFileName(tableDemographics.getHouseId()));

                itemDemographics.setVersion(AssetReader.getVersion());
                itemDemographics.setAppVersion(BuildConfig.VERSION_NAME);

                String vitalJson = "{ \"demographics\": " + gson.toJson(itemDemographics) + "}";

                boolean isDemo = false;
                if (tableDemographics.getRegistrationId().startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                if (createAndUploadFile(getJsonFilePrefix() + "dem_"
                                + tableDemographics.getRegistrationId() + "_" + tableDemographics.getSurveyDateTimeMillis() + ".json",
                        vitalJson,
                        isDemo)) {
                    tableDemographics.setSynced(true);
                    tableDemographics.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadSociodemographics() {
        List<TableSociodemographics> tableSociodemographicsList = dataManager.getAllSociodemographics(false);

        for (TableSociodemographics tableSociodemographics : tableSociodemographicsList) {
            try {
                ItemSociodemographicSync itemSociodemographicSync = new ItemSociodemographicSync();
                itemSociodemographicSync.setTableKey(tableSociodemographics.getTableKey());
                itemSociodemographicSync.setRegistrationId(tableSociodemographics.getRegistrationId());
                itemSociodemographicSync.setEhrNumber(tableSociodemographics.getEhrNumber());
                itemSociodemographicSync.setRelationToHoh(tableSociodemographics.getRelationToHoh());
                itemSociodemographicSync.setMaritalStatus(tableSociodemographics.getMaritalStatus());
                itemSociodemographicSync.setEducation(tableSociodemographics.getEducation());
                itemSociodemographicSync.setLiteracy(tableSociodemographics.getLiteracy());
                itemSociodemographicSync.setCurrentOccupation(tableSociodemographics.getCurrentOccupation());
                itemSociodemographicSync.setMonthlyIncome(tableSociodemographics.getMonthlyIncome());
                itemSociodemographicSync.setHouseType(tableSociodemographics.getHouseType());
                itemSociodemographicSync.setLanguagesSpoken(tableSociodemographics.getLanguagesSpoken());
                itemSociodemographicSync.setHabits(tableSociodemographics.getHabits());
                itemSociodemographicSync.setExercise(tableSociodemographics.getExercise());
                itemSociodemographicSync.setDiet(tableSociodemographics.getDiet());
                itemSociodemographicSync.setDisease(tableSociodemographics.getDisease());
                itemSociodemographicSync.setDiseaseTreatment(tableSociodemographics.getDiseaseTreatment());
                itemSociodemographicSync.setDisability(tableSociodemographics.getDisability());
                itemSociodemographicSync.setSick(tableSociodemographics.getSick());
                itemSociodemographicSync.setAilment(tableSociodemographics.getAilment());
                itemSociodemographicSync.setSocioHealthCard(tableSociodemographics.getHealthCard());
                itemSociodemographicSync.setNotes(tableSociodemographics.getNotes());

                itemSociodemographicSync.setSurveyorUserId(tableSociodemographics.getSurveyorUserId());
                itemSociodemographicSync.setPartnerId(tableSociodemographics.getPartnerId());
                itemSociodemographicSync.setCampName(tableSociodemographics.getCampName());
                itemSociodemographicSync.setDeviceNumber(tableSociodemographics.getDeviceNumber());
                itemSociodemographicSync.setDeviceId(tableSociodemographics.getDeviceId());
                itemSociodemographicSync.setSurveyDateTime(tableSociodemographics.getSurveyDateTime());
                itemSociodemographicSync.setSurveyDateTimeMillis(tableSociodemographics.getSurveyDateTimeMillis());
                itemSociodemographicSync.setInactive(tableSociodemographics.isInactive());
                itemSociodemographicSync.setInactiveReason(tableSociodemographics.getInactiveReason());
                itemSociodemographicSync.setModified(tableSociodemographics.isModified());
                itemSociodemographicSync.setModifiedDateTime(tableSociodemographics.getModifiedDateTime());
                itemSociodemographicSync.setSynced(tableSociodemographics.isSynced());

                itemSociodemographicSync.setVersion(AssetReader.getVersion());
                itemSociodemographicSync.setAppVersion(BuildConfig.VERSION_NAME);

                itemSociodemographicSync.setMedicinesTakenDaily(tableSociodemographics.getMedicinesTakenDaily());
                itemSociodemographicSync.setDiseaseTestLastDone(tableSociodemographics.getDiseaseTestLastDone());
                itemSociodemographicSync.setInformedAboutDisease(tableSociodemographics.getInformedAboutDisease());
                itemSociodemographicSync.setMedicationForDisease(tableSociodemographics.getMedicationForDisease());
                itemSociodemographicSync.setChangesToControlDisease(tableSociodemographics.getChangesToControlDisease());
                itemSociodemographicSync.setWomenSpecialPrecautionsOnPeriod(tableSociodemographics.getWomenSpecialPrecautionsOnPeriod());
                itemSociodemographicSync.setMobileUsed(tableSociodemographics.getMobileUsed());
                itemSociodemographicSync.setMobileComfort(tableSociodemographics.getMobileComfort());
                itemSociodemographicSync.setHealthRecordsStorage(tableSociodemographics.getHealthRecordsStorage());

                String vitalJson = "{ \"socioDemographics\": " + gson.toJson(itemSociodemographicSync) + "}";

                boolean isDemo = false;
                if (tableSociodemographics.getRegistrationId().startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                if (createAndUploadFile(getJsonFilePrefix()
                        + "soc_" + itemSociodemographicSync.getRegistrationId() + "_" + tableSociodemographics.getSurveyDateTimeMillis() + ".json", vitalJson, isDemo)) {
                    tableSociodemographics.setSynced(true);
                    tableSociodemographics.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadProblems() {
        List<TableProblems> tableProblems = dataManager.getAllQuestions(false);

        for (TableProblems tableProblem : tableProblems) {
            try {
                ItemProblemSync itemProblemSync = new ItemProblemSync();

                itemProblemSync.setTableKey(tableProblem.getTableKey());
                itemProblemSync.setRegistrationId(tableProblem.getRegistrationId());
                itemProblemSync.setEhrNumber(tableProblem.getEhrNumber());
                itemProblemSync.setQuestionGroup(tableProblem.getQuestionGroup());
                itemProblemSync.setQuestion(tableProblem.getQuestion());
                itemProblemSync.setQuestionOrder(tableProblem.getQuestionOrder());
                itemProblemSync.setAnswer(tableProblem.getAnswer());

                itemProblemSync.setSurveyorUserId(tableProblem.getSurveyorUserId());
                itemProblemSync.setPartnerId(tableProblem.getPartnerId());
                itemProblemSync.setCampName(tableProblem.getCampName());
                itemProblemSync.setDeviceNumber(tableProblem.getDeviceNumber());
                itemProblemSync.setDeviceId(tableProblem.getDeviceId());
                itemProblemSync.setSurveyDateTime(tableProblem.getSurveyDateTime());
                itemProblemSync.setSurveyDateTimeMillis(tableProblem.getSurveyDateTimeMillis());
                itemProblemSync.setInactive(tableProblem.isInactive());
                itemProblemSync.setInactiveReason(tableProblem.getInactiveReason());
                itemProblemSync.setModified(tableProblem.isModified());
                itemProblemSync.setModifiedDateTime(tableProblem.getModifiedDateTime());
                itemProblemSync.setSynced(tableProblem.isSynced());

                itemProblemSync.setVersion(AssetReader.getVersion());
                itemProblemSync.setAppVersion(BuildConfig.VERSION_NAME);

                String vitalJson = "{ \"problem" + itemProblemSync.getQuestionGroup() + "\": " + gson.toJson(itemProblemSync) + "}";

                boolean isDemo = false;
                if (tableProblem.getRegistrationId().startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                String postfix = tableProblem.getQuestionGroup().toLowerCase() + "_" + tableProblem.getQuestionOrder() + "_";

                if (createAndUploadFile(getJsonFilePrefix() + "prb_" + postfix + tableProblem.getRegistrationId()
                        + "_" + tableProblem.getSurveyDateTimeMillis() + ".json", vitalJson, isDemo)) {
                    tableProblem.setSynced(true);
                    tableProblem.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadHouseInfo() {
        List<TableHouse> tableHouses = dataManager.getAllHousesSync(false);

        for (TableHouse tableHouse : tableHouses) {

            try {
                ItemHouseSync itemHouseSync = new ItemHouseSync();
                itemHouseSync.setTableKey(tableHouse.getTableKey());
                itemHouseSync.setHouseId(tableHouse.getHouseId());
                itemHouseSync.setHouseNo(tableHouse.getHouseNo());
                itemHouseSync.setStreet(tableHouse.getStreet());
                itemHouseSync.setPost(tableHouse.getPost());
                itemHouseSync.setLocality(tableHouse.getLocality());
                itemHouseSync.setDistrict(tableHouse.getDistrict());
                itemHouseSync.setState(tableHouse.getState());
                itemHouseSync.setCountry(tableHouse.getCountry());
                itemHouseSync.setPinCode(tableHouse.getPinCode());
                itemHouseSync.setHeadOfHouse(tableHouse.getHeadOfHouse());
                itemHouseSync.setRespondent(tableHouse.getRespondent());
                itemHouseSync.setTotalMembers(tableHouse.getTotalMembers());
                itemHouseSync.setHouseOwnership(tableHouse.getHouseOwnership());
                itemHouseSync.setRationCard(tableHouse.getRationCard());
                itemHouseSync.setHealthInsurance(tableHouse.getHealthInsurance());
                itemHouseSync.setMealTimes(tableHouse.getMealTimes());
                itemHouseSync.setCookingFuel(tableHouse.getCookingFuel());
                itemHouseSync.setTreatmentPlace(tableHouse.getTreatmentPlace());
                itemHouseSync.setRecentDeath(tableHouse.getRecentDeath());

                itemHouseSync.setSpouseOfHeadOfHouse(tableHouse.getSpouseOfHeadOfHouse());
                itemHouseSync.setHouseStatus(tableHouse.getHouseStatus());
                itemHouseSync.setTotalRooms(tableHouse.getTotalRooms());
                itemHouseSync.setToiletAvailability(tableHouse.getToiletAvailability());
                itemHouseSync.setDrinkingWaterSource(tableHouse.getDrinkingWaterSource());
                itemHouseSync.setElectricityAvailability(tableHouse.getElectricityAvailability());
                itemHouseSync.setMotorisedVehicle(tableHouse.getMotorisedVehicle());
                itemHouseSync.setVillage(tableHouse.getVillage());
                itemHouseSync.setGramPanchayat(tableHouse.getGramPanchayat());

                itemHouseSync.setNotes(tableHouse.getNotes());
                itemHouseSync.setGpsLocation(tableHouse.getGpsLocation());
                itemHouseSync.setSurveyorUserId(tableHouse.getSurveyorUserId());
                itemHouseSync.setPartnerId(tableHouse.getPartnerId());
                itemHouseSync.setCampName(tableHouse.getCampName());
                itemHouseSync.setDeviceNumber(tableHouse.getDeviceNumber());
                itemHouseSync.setDeviceId(tableHouse.getDeviceId());
                itemHouseSync.setSurveyDateTime(tableHouse.getSurveyDateTime());
                itemHouseSync.setSurveyDateTimeMillis(tableHouse.getSurveyDateTimeMillis());
                itemHouseSync.setInactive(tableHouse.isInactive());
                itemHouseSync.setInactiveReason(tableHouse.getInactiveReason());
                itemHouseSync.setModified(tableHouse.isModified());
                itemHouseSync.setModifiedDateTime(tableHouse.getModifiedDateTime());
                itemHouseSync.setSynced(tableHouse.isSynced());

                itemHouseSync.setVersion(AssetReader.getVersion());
                itemHouseSync.setAppVersion(BuildConfig.VERSION_NAME);

                itemHouseSync.setEarningMembers(tableHouse.getEarningMembers());
                itemHouseSync.setTotalIncome(tableHouse.getTotalIncome());
                itemHouseSync.setTreatmentCost(tableHouse.getTreatmentCost());
                itemHouseSync.setDrinkWeekly(tableHouse.getDrinkWeekly());
                itemHouseSync.setDrugsUse(tableHouse.getDrugsUse());
                itemHouseSync.setHandicappedPresent(tableHouse.getHandicappedPresent());
                itemHouseSync.setHandicappedSuffering(tableHouse.getHandicappedSuffering());
                itemHouseSync.setPets(tableHouse.getPets());
                itemHouseSync.setPetsBitten(tableHouse.getPetsBitten());
                itemHouseSync.setPetsBiteAction(tableHouse.getPetsBiteAction());
                itemHouseSync.setDrinkingWaterCleaningMethod(tableHouse.getDrinkingWaterCleaningMethod());
                itemHouseSync.setBathingToiletFacility(tableHouse.getBathingToiletFacility());
                itemHouseSync.setChildHealthCheckupLastDone(tableHouse.getChildHealthCheckupLastDone());
                itemHouseSync.setChildrenVaccinated(tableHouse.getChildrenVaccinated());
                itemHouseSync.setVaccinationRecordsStorage(tableHouse.getVaccinationRecordsStorage());
                itemHouseSync.setChildGenderResponsibility(tableHouse.getChildGenderResponsibility());
                itemHouseSync.setPreferredChildGender(tableHouse.getPreferredChildGender());

                boolean isDemo = false;
                if (tableHouse.getHouseId().startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                String vitalJson = "{ \"houseData\": " + gson.toJson(itemHouseSync) + "}";
                if (createAndUploadFile(getJsonFilePrefix() + "hus_" + tableHouse.getHouseId()
                        + "_" + tableHouse.getSurveyDateTimeMillis() + ".json", vitalJson, isDemo)) {
                    tableHouse.setSynced(true);
                    tableHouse.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadDeathInfo() {
        List<TableDeathInfo> tableDeathInfoList = dataManager.getAllDeathInfoList(false);

        for (TableDeathInfo tableDeathInfo : tableDeathInfoList) {
            try {
                ItemDeathInfoSync itemDeathInfoSync = new ItemDeathInfoSync();
                itemDeathInfoSync.setTableKey(tableDeathInfo.getTableKey());
                itemDeathInfoSync.setHouseId(tableDeathInfo.getHouseId());
                itemDeathInfoSync.setPersonName(tableDeathInfo.getPersonName());
                itemDeathInfoSync.setAge(tableDeathInfo.getAge());
                itemDeathInfoSync.setGender(tableDeathInfo.getGender());
                itemDeathInfoSync.setAgeUnit(tableDeathInfo.getAgeUnit());
                itemDeathInfoSync.setRelationToHoh(tableDeathInfo.getRelationToHoh());
                itemDeathInfoSync.setDeathCause(tableDeathInfo.getDeathCause());
                itemDeathInfoSync.setNotes(tableDeathInfo.getNotes());
                itemDeathInfoSync.setSurveyorUserId(tableDeathInfo.getSurveyorUserId());
                itemDeathInfoSync.setPartnerId(tableDeathInfo.getPartnerId());
                itemDeathInfoSync.setCampName(tableDeathInfo.getCampName());
                itemDeathInfoSync.setDeviceNumber(tableDeathInfo.getDeviceNumber());
                itemDeathInfoSync.setDeviceId(tableDeathInfo.getDeviceId());
                itemDeathInfoSync.setSurveyDateTime(tableDeathInfo.getSurveyDateTime());
                itemDeathInfoSync.setSurveyDateTimeMillis(tableDeathInfo.getSurveyDateTimeMillis());
                itemDeathInfoSync.setInactive(tableDeathInfo.isInactive());
                itemDeathInfoSync.setInactiveReason(tableDeathInfo.getInactiveReason());
                itemDeathInfoSync.setModified(tableDeathInfo.isModified());
                itemDeathInfoSync.setModifiedDateTime(tableDeathInfo.getModifiedDateTime());
                itemDeathInfoSync.setSynced(tableDeathInfo.isSynced());

                itemDeathInfoSync.setVersion(AssetReader.getVersion());
                itemDeathInfoSync.setAppVersion(BuildConfig.VERSION_NAME);

                boolean isDemo = false;
                if (tableDeathInfo.getHouseId().startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                String vitalJson = "{ \"deathInfo\": " + gson.toJson(itemDeathInfoSync) + "}";
                if (createAndUploadFile(getJsonFilePrefix() + "dth_" + tableDeathInfo.getHouseId()
                        + "_" + tableDeathInfo.getSurveyDateTimeMillis() + ".json", vitalJson, isDemo)) {
                    tableDeathInfo.setSynced(true);
                    tableDeathInfo.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadVitals() {
        List<TableVitals> tableVitals = dataManager.getAllVitals(false);

        for (TableVitals tableVital : tableVitals) {
            try {
                ItemVitalSync itemVital = new ItemVitalSync();
                itemVital.setTableKey(tableVital.getTableKey());
                itemVital.setRegistrationId(tableVital.getRegistrationId());
                itemVital.setEhrNumber(tableVital.getEhrNumber());
                itemVital.setVitalType(tableVital.getVitalType());
                itemVital.setVitalSubTypes(tableVital.getVitalSubTypes());
                itemVital.setVitalValues(tableVital.getVitalValues());
                itemVital.setVitalUnits(tableVital.getVitalUnits());
                itemVital.setVitalDateTime(tableVital.getVitalDateTime());
                itemVital.setVitalDateTimeMillis(tableVital.getVitalDateTimeMillis());
                itemVital.setVitalImage(tableVital.getVitalImage());
                itemVital.setNotes(tableVital.getNotes());
                itemVital.setDeviceName(tableVital.getDeviceName());
                itemVital.setGpsLocation(tableVital.getGpsLocation());
                itemVital.setQrScanned(tableVital.isQrScanned());
                itemVital.setSurveyorUserId(tableVital.getSurveyorUserId());
                itemVital.setPartnerId(tableVital.getPartnerId());
                itemVital.setCampName(tableVital.getCampName());
                itemVital.setDeviceNumber(tableVital.getDeviceNumber());
                itemVital.setDeviceId(tableVital.getDeviceId());
                itemVital.setSurveyDateTime(tableVital.getSurveyDateTime());
                itemVital.setSurveyDateTimeMillis(tableVital.getSurveyDateTimeMillis());
                itemVital.setInactive(tableVital.isInactive());
                itemVital.setInactiveReason(tableVital.getInactiveReason());
                itemVital.setModified(tableVital.isModified());
                itemVital.setModifiedDateTime(tableVital.getModifiedDateTime());
                itemVital.setSynced(tableVital.isSynced());

                itemVital.setDemographicsFileName(getDemographicsFileName(tableVital.getRegistrationId()));

                itemVital.setVersion(AssetReader.getVersion());
                itemVital.setAppVersion(BuildConfig.VERSION_NAME);

                String vitalPrefix      = "";
                String vitalTitleSuffix = "";

                switch (tableVital.getVitalType()) {
                    case "Height":
                        vitalPrefix = "vht";
                        vitalTitleSuffix = "Height";
                        break;

                    case "Weight":
                        vitalPrefix = "vwt";
                        vitalTitleSuffix = "Weight";
                        break;

                    case "Blood Pressure":
                        vitalPrefix = "vbp";
                        vitalTitleSuffix = "BloodPressure";
                        break;

                    case "Blood Sugar":
                        vitalPrefix = "vbs";
                        vitalTitleSuffix = "BloodSugar";

                        break;

                    case "Hemoglobin":
                        vitalPrefix = "vhb";
                        vitalTitleSuffix = "Hemoglobin";

                        break;

                    case "Prescription":
                        vitalPrefix = "vpr";
                        vitalTitleSuffix = "Prescription";

                        break;

                    case "Pulse":
                        vitalPrefix = "vpl";
                        vitalTitleSuffix = "Pulse";

                        break;
                }

                // TODO: 21-May-19, Billing

                String vitalJson = "{ \"vitals" + vitalTitleSuffix + "\": " + gson.toJson(itemVital) + "}";

                boolean isDemo = false;
                if (tableVital.getRegistrationId().startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                if (createAndUploadFile(getJsonFilePrefix() + vitalPrefix + "_" + tableVital.getRegistrationId() + "_" +
                        tableVital.getSurveyDateTimeMillis() + ".json", vitalJson, isDemo)) {
                    tableVital.setSynced(true);
                    tableVital.update();

                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadStocks() {
        List<TableStock> tableStockList = dataManager.getStockItems(false);

        for (TableStock tableStock : tableStockList) {
            try {
                ItemStockSync itemStockSync = new ItemStockSync();
                itemStockSync.setTableKey(tableStock.getTableKey());
                itemStockSync.setPurchasedDate(tableStock.getPurchasedDate());
                itemStockSync.setItemName(tableStock.getItemName());
                itemStockSync.setQuantity(tableStock.getQuantity());
                itemStockSync.setPrice(tableStock.getPrice());
                itemStockSync.setNotes(tableStock.getNotes());
                itemStockSync.setSurveyorUserId(tableStock.getSurveyorUserId());
                itemStockSync.setPartnerId(tableStock.getPartnerId());
                itemStockSync.setCampName(tableStock.getCampName());
                itemStockSync.setDeviceNumber(tableStock.getDeviceNumber());
                itemStockSync.setDeviceId(tableStock.getDeviceId());
                itemStockSync.setEntryDateTime(tableStock.getEntryDateTime());
                itemStockSync.setEntryDateTimeMillis(tableStock.getEntryDateTimeMillis());
                itemStockSync.setInactive(tableStock.isInactive());
                itemStockSync.setInactiveReason(tableStock.getInactiveReason());
                itemStockSync.setModified(tableStock.isModified());
                itemStockSync.setModifiedDateTime(tableStock.getModifiedDateTime());
                itemStockSync.setSynced(tableStock.isSynced());

                itemStockSync.setVersion(AssetReader.getVersion());
                itemStockSync.setAppVersion(BuildConfig.VERSION_NAME);

                String stockJson = "{ \"stockInfo\": " + gson.toJson(itemStockSync) + "}";
                if (createAndUploadFile(getJsonFilePrefix() + "stk_" + tableStock.getEntryDateTimeMillis() + ".json", stockJson, false)) {
                    tableStock.setSynced(true);
                    tableStock.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadKitInfo() {
        List<TableKitConfig> tableKitConfigs = dataManager.getAllKitConfig(false);

        for (TableKitConfig tableKitConfig : tableKitConfigs) {
            try {
                ItemKitConfigSync itemKitConfigSync = new ItemKitConfigSync();

                itemKitConfigSync.setTableKey(tableKitConfig.getTableKey());
                itemKitConfigSync.setType(tableKitConfig.getConfigType());
                itemKitConfigSync.setMake(tableKitConfig.getMake());
                itemKitConfigSync.setModel(tableKitConfig.getModel());
                itemKitConfigSync.setProductName(tableKitConfig.getProductName());

                itemKitConfigSync.setProductId(tableKitConfig.getProductId());
                itemKitConfigSync.setMacAddress(tableKitConfig.getMacAddress());
                itemKitConfigSync.setValidStartDate(tableKitConfig.getValidStartDate());
                itemKitConfigSync.setValidEndDate(tableKitConfig.getValidEndDate());
                itemKitConfigSync.setNotes(tableKitConfig.getNotes());
                itemKitConfigSync.setPartnerId(tableKitConfig.getPartnerId());
                itemKitConfigSync.setDeviceNumber(tableKitConfig.getDeviceNumber());
                itemKitConfigSync.setDeviceId(tableKitConfig.getDeviceId());
                itemKitConfigSync.setEntryDateTime(tableKitConfig.getEntryDateTime());
                itemKitConfigSync.setEntryDateTimeMillis(tableKitConfig.getEntryDateTimeMillis());
                itemKitConfigSync.setInactive(tableKitConfig.isInactive());
                itemKitConfigSync.setInactiveReason(tableKitConfig.getInactiveReason());
                itemKitConfigSync.setModified(tableKitConfig.isModified());
                itemKitConfigSync.setModifiedDateTime(tableKitConfig.getModifiedDateTime());
                itemKitConfigSync.setSynced(tableKitConfig.isSynced());

                itemKitConfigSync.setVersion(AssetReader.getVersion());
                itemKitConfigSync.setAppVersion(BuildConfig.VERSION_NAME);

                String postfixFileName = "";
                String postfixHeader   = "";

                switch (tableKitConfig.getConfigType()) {

                    case "printer": {
                        postfixFileName = "kpr";
                        postfixHeader = "Printer";
                        break;
                    }

                    case "weight": {
                        postfixFileName = "kwt";
                        postfixHeader = "Weight";
                        break;
                    }

                    case "sugar": {
                        postfixFileName = "kbs";
                        postfixHeader = "Height";
                        break;
                    }

                    case "bp": {
                        postfixFileName = "kbp";
                        postfixHeader = "BloodPressure";
                        break;
                    }

                    case "pulse": {
                        postfixFileName = "kpo";
                        postfixHeader = "PulseOximeter";
                        break;
                    }
                }

                boolean isDemo = false;
                if (tableKitConfig.getPartnerId().equals(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                String vitalJson = "{ \"kitConfig" + postfixHeader + "\": " + gson.toJson(itemKitConfigSync) + "}";
                if (createAndUploadFile(getJsonFilePrefix() + "kit_" + postfixFileName + "_" + tableKitConfig.getEntryDateTimeMillis() + ".json", vitalJson, isDemo)) {
                    tableKitConfig.setSynced(true);
                    tableKitConfig.update();
                } else {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private void uploadImages() {
        List<TableImage> tableImagesList = dataManager.getImages(false);

        for (TableImage tableImage : tableImagesList) {
            try {
                ItemImageSync itemImageSync = new ItemImageSync();
                itemImageSync.setTableKey(tableImage.getTableKey());
                itemImageSync.setRegistrationId(tableImage.getRegistrationId());
                itemImageSync.setEhrNumber(tableImage.getEhrNumber());
                itemImageSync.setImageType(tableImage.getImageType());
                itemImageSync.setImageName(tableImage.getImageName());
                itemImageSync.setImageDateTime(tableImage.getImageDateTime());
                itemImageSync.setImageDateTimeMillis(tableImage.getImageDateTimeMillis());
                itemImageSync.setGpsLocation(tableImage.getGpsLocation());

                itemImageSync.setNotes(tableImage.getNotes());
                itemImageSync.setSurveyorUserId(tableImage.getSurveyorUserId());
                itemImageSync.setPartnerId(tableImage.getPartnerId());
                itemImageSync.setCampName(tableImage.getCampName());
                itemImageSync.setDeviceNumber(tableImage.getDeviceNumber());
                itemImageSync.setDeviceId(tableImage.getDeviceId());
                itemImageSync.setSurveyDateTime(tableImage.getSurveyDateTime());
                itemImageSync.setSurveyDateTimeMillis(tableImage.getSurveyDateTimeMillis());
                itemImageSync.setInactive(tableImage.isInactive());
                itemImageSync.setInactiveReason(tableImage.getInactiveReason());
                itemImageSync.setModified(tableImage.isModified());
                itemImageSync.setModifiedDateTime(tableImage.getModifiedDateTime());
                itemImageSync.setSynced(tableImage.isSynced());

                itemImageSync.setVersion(AssetReader.getVersion());
                itemImageSync.setAppVersion(BuildConfig.VERSION_NAME);

                String imgJson = "{ \"image\": " + gson.toJson(itemImageSync) + "}";

                boolean isDemo = false;
                if (tableImage.getRegistrationId().startsWith(LConstants.DEFAULT_PARTNER_ID)) {
                    isDemo = true;
                }

                if (createAndUploadFile(getJsonFilePrefix() + "img_" + tableImage.getImageDateTimeMillis() + ".json", imgJson, isDemo)) {
                    tableImage.setSynced(true);
                    tableImage.update();
                } else {
                    return;
                }

                // Upload the file
                if (!uploadFile(new File(getLocalFilesPath(), tableImage.getImageName()), isDemo)) {
                    return;
                }
            } catch (Exception e) {
                LLog.printStackTrace(e);
            }
        }
    }

    private boolean createAndUploadFile(String fileName, String content, boolean isDemo) throws Exception {
        final File outputFile = new File(tempFolder, fileName);

        FileOutputStream fos = new FileOutputStream(outputFile);
        fos.write(content.getBytes());
        fos.close();

        boolean success = uploadFile(outputFile, isDemo);

        outputFile.delete();
        return success;
    }

    private synchronized boolean uploadFile(File file, boolean isDemo) {
        int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream  dataOutputStream;
        String            lineEnd    = "\r\n";
        String            twoHyphens = "--";
        String            boundary   = "*****";

        int    bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int    maxBufferSize = 1024 * 1024;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);

            String partnerId = dataManager.getPartnerId().toLowerCase();

            if (isDemo) {
                partnerId = LConstants.DEFAULT_PARTNER_ID.toLowerCase();
            }

            String syncUrl = AssetReader.getStringKey("sync_url");

            if (!hasValue(syncUrl)) {
                syncUrl = JSON_SYNC_URL;
            }

            URL url = new URL(syncUrl + "?partnerId=" + partnerId + "&screenType=" + SCREEN_TYPE + "&version=" + AssetReader.getVersion());

            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("ENCTYPE", "multipart/form-data");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            connection.setRequestProperty("uploaded_file", file.getName());

            dataOutputStream = new DataOutputStream(connection.getOutputStream());

            dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
            dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                    + file.getName() + "\"" + lineEnd);

            dataOutputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dataOutputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            dataOutputStream.writeBytes(lineEnd);
            dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();

            fileInputStream.close();
            dataOutputStream.flush();
            dataOutputStream.close();

            if (serverResponseCode == 200) {
                syncHappened = true;
                return true;
            }

            serverError = true;

            return false;

        } catch (FileNotFoundException e) {
            connectionError = true;
            return false;

        } catch (MalformedURLException e) {
            connectionError = true;
            return false;

        } catch (ConnectException e) {
            connectionError = true;
            return false;

        } catch (IOException e) {
            serverError = true;
            return false;
        }
    }

    private String getJsonFilePrefix() {
        return dataManager.getPartnerId() + "_" + dataManager.getDeviceNumber() + "_";
    }

    private String getDemographicsFileName(String registrationId) {
        TableDemographics tableDemographics = dataManager.getPatient(registrationId);

        try {
            return getJsonFilePrefix()
                    + "dem_"
                    + tableDemographics.getRegistrationId()
                    + "_"
                    + tableDemographics.getSurveyDateTimeMillis()
                    + ".json";
        } catch (Exception e) {
            return "";
        }
    }

    private String getHouseFileName(String houseId) {
        TableHouse tableHouse = dataManager.getHouse(houseId);
        try {
            return getJsonFilePrefix()
                    + "hus_"
                    + tableHouse.getHouseId()
                    + "_"
                    + tableHouse.getSurveyDateTimeMillis()
                    + ".json";

        } catch (Exception e) {
            return "";
        }
    }
}