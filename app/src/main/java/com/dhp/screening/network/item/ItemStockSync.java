package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemStockSync {

    @SerializedName("stockAutoNumber")
    private int tableKey;

    @SerializedName("stockPurchasedDate")
    private String purchasedDate = "";

    @SerializedName("stockItemName")
    private String itemName = "";

    @SerializedName("stockQuantity")
    private int quantity;

    @SerializedName("stockPrice")
    private int price;

    @SerializedName("stockNotes")
    private String notes = "";

    @SerializedName("stockSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("stockPartnerId")
    private String partnerId = "";

    @SerializedName("stockCampName")
    private String campName = "";

    @SerializedName("stockDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("stockDeviceId")
    private String deviceId;

    @SerializedName("stockEntryDateTime")
    private String entryDateTime = "";

    @SerializedName("stockEntryDateTimeMilliSeconds")
    private long entryDateTimeMillis;

    @SerializedName("stockInactive")
    private boolean inactive;

    @SerializedName("stockInactiveReason")
    private String inactiveReason = "";

    @SerializedName("stockModified")
    private boolean modified;

    @SerializedName("stockModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("stockSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getPurchasedDate() {
        return purchasedDate;
    }

    public void setPurchasedDate(String purchasedDate) {
        this.purchasedDate = purchasedDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(String entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public long getEntryDateTimeMillis() {
        return entryDateTimeMillis;
    }

    public void setEntryDateTimeMillis(long entryDateTimeMillis) {
        this.entryDateTimeMillis = entryDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}