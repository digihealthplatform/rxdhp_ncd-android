package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemCampInfoSync {

    @SerializedName("campAutoNumber")
    private int tableKey;

    @SerializedName("campName")
    private String campName = "";

    @SerializedName("campPartnerId")
    private String partnerId = "";

    @SerializedName("campStartDate")
    private String startDate = "";

    @SerializedName("campEndDate")
    private String endDate = "";

    @SerializedName("campDoorToDoor")
    private boolean doorToDoor;

    @SerializedName("campDeviceId")
    private String deviceId;

    @SerializedName("campEntryDateTime")
    private String entryDateTime = "";

    @SerializedName("campEntryDateTimeMilliSeconds")
    private long entryDateTimeMillis;

    @SerializedName("campModified")
    private boolean modified;

    @SerializedName("campModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("campSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isDoorToDoor() {
        return doorToDoor;
    }

    public void setDoorToDoor(boolean doorToDoor) {
        this.doorToDoor = doorToDoor;
    }

    public String getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(String entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public long getEntryDateTimeMillis() {
        return entryDateTimeMillis;
    }

    public void setEntryDateTimeMillis(long entryDateTimeMillis) {
        this.entryDateTimeMillis = entryDateTimeMillis;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}