package com.dhp.screening.network;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.util.LConstants;
import com.dhp.screening.util.LLog;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import static com.dhp.screening.util.LConstants.HB_FOLDER_PATH;
import static com.dhp.screening.util.LConstants.HB_UPLOADED_FOLDER_PATH;
import static com.dhp.screening.util.LConstants.JSON_SYNC_URL;
import static com.dhp.screening.util.LConstants.SCREEN_TYPE;
import static com.dhp.screening.util.LUtils.hasValue;

public class ImageUploadTask extends AsyncTask<String, Integer, String> {

    private final DataManager dataManager;

    public ImageUploadTask(Context context, DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    protected void onPreExecute() {
        StaticData.imageUploadInProgress = true;
    }

    @Override
    protected void onPostExecute(String result) {
        StaticData.imageUploadInProgress = false;
    }

    @Override
    protected String doInBackground(String... params) {
        uploadHbCaptures();
        return null;
    }

    private void uploadHbCaptures() {

        File hbImagesFolder = new File(Environment.getExternalStorageDirectory() + "/" + HB_FOLDER_PATH);

        if (!hbImagesFolder.exists()) {
            return;
        }

        int failureCount = 0;

        File[] files = hbImagesFolder.listFiles();

        if (null == files || 0 == files.length) {
            return;
        }

        for (File file : files) {

            if (file.getName().startsWith("_")) {
                continue;
            }

            boolean success = uploadFile(file);

            if (success) {
                try {
                    moveFile(file);
                } catch (Exception e) {
                    LLog.printStackTrace(e);
                }

            } else {
                ++failureCount;

                if (failureCount > 5) {
                    return;
                }
            }
        }
    }

    private void moveFile(File source) throws Exception {
        File hbUploadedFolder = new File(Environment.getExternalStorageDirectory() + "/" + HB_UPLOADED_FOLDER_PATH);

        if (!hbUploadedFolder.exists()) {
            hbUploadedFolder.mkdir();
        }

        File destinationFile = new File(hbUploadedFolder, source.getName());

        FileInputStream  sourceStream      = new FileInputStream(source);
        FileOutputStream destinationStream = new FileOutputStream(destinationFile);

        byte[] buffer = new byte[1024];

        int read;
        while ((read = sourceStream.read(buffer)) > 0) {
            destinationStream.write(buffer, 0, read);
        }

        sourceStream.close();
        destinationStream.close();

        source.delete();
    }

    private synchronized boolean uploadFile(File file) {
        int serverResponseCode = 0;

        HttpURLConnection connection;
        DataOutputStream  dataOutputStream;
        String            lineEnd    = "\r\n";
        String            twoHyphens = "--";
        String            boundary   = "*****";

        int    bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int    maxBufferSize = 1024 * 1024;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);

            String partnerId = dataManager.getPartnerId().toLowerCase();

            if (file.getName().toLowerCase().contains("demo")) {
                partnerId = LConstants.DEFAULT_PARTNER_ID.toLowerCase();
            }

            String syncUrl = AssetReader.getStringKey("sync_url");

            if (!hasValue(syncUrl)) {
                syncUrl = JSON_SYNC_URL;
            }

            URL url = new URL(syncUrl + "?partnerId=" + partnerId + "&screenType=" + SCREEN_TYPE);

            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("ENCTYPE", "multipart/form-data");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            connection.setRequestProperty("uploaded_file", file.getName());

            dataOutputStream = new DataOutputStream(connection.getOutputStream());

            dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
            dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
                    + file.getName() + "\"" + lineEnd);

            dataOutputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dataOutputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            dataOutputStream.writeBytes(lineEnd);
            dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();

            fileInputStream.close();
            dataOutputStream.flush();
            dataOutputStream.close();

            return serverResponseCode == 200;

        } catch (FileNotFoundException e) {
            return false;

        } catch (MalformedURLException e) {
            return false;

        } catch (IOException e) {
            return false;
        }
    }
}