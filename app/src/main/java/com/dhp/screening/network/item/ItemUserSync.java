package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemUserSync {

    @SerializedName("userAutoNumber")
    private int tableKey;

    @SerializedName("userId")
    private String userId = "";

    @SerializedName("userFullName")
    private String userFullName = "";

    @SerializedName("userPassword")
    private String password = "";

    @SerializedName("userType")
    private String userType = "";

    @SerializedName("userGender")
    private String gender = "";

    @SerializedName("userDateOfBirth")
    private String dob = "";

    @SerializedName("userYearOfBirth")
    private int yob = 0;

    @SerializedName("userIdentityType")
    private String identityType = "";

    @SerializedName("userIdentityNumber")
    private String identityNumber = "";

    @SerializedName("userPhone")
    private String phoneNumber = "";

    @SerializedName("userEmailId")
    private String emailId = "";

    @SerializedName("userHouseNumber")
    private String houseNo = "";

    @SerializedName("userStreet")
    private String street = "";

    @SerializedName("userPost")
    private String post = "";

    @SerializedName("userLocality")
    private String locality = "";

    @SerializedName("userDistrict")
    private String district = "";

    @SerializedName("userState")
    private String state = "";

    @SerializedName("userCountry")
    private String country = "";

    @SerializedName("userPincode")
    private int pinCode = 0;

    @SerializedName("userAllowPrescription")
    private boolean allowPrescription;

    @SerializedName("userIsDemo")
    private boolean userIsDemo;

    @SerializedName("userQrScanned")
    private boolean qrScanned;

    @SerializedName("userPartnerId")
    private String partnerId = "";

    @SerializedName("userCampName")
    private String campName = "";

    @SerializedName("userDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("userDeviceId")
    private String deviceId;

    @SerializedName("userEntryDateTime")
    private String userEntryDateTime = "";

    @SerializedName("userEntryDateTimeMilliSeconds")
    private long userEntryDateTimeMillis;

    @SerializedName("userInactive")
    private boolean inactive;

    @SerializedName("userInactiveReason")
    private String inactiveReason = "";

    @SerializedName("userModified")
    private boolean modified;

    @SerializedName("userModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("userSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getYob() {
        return yob;
    }

    public void setYob(int yob) {
        this.yob = yob;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public boolean isAllowPrescription() {
        return allowPrescription;
    }

    public void setAllowPrescription(boolean allowPrescription) {
        this.allowPrescription = allowPrescription;
    }

    public boolean isQrScanned() {
        return qrScanned;
    }

    public void setQrScanned(boolean qrScanned) {
        this.qrScanned = qrScanned;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserEntryDateTime() {
        return userEntryDateTime;
    }

    public void setUserEntryDateTime(String userEntryDateTime) {
        this.userEntryDateTime = userEntryDateTime;
    }

    public long getUserEntryDateTimeMillis() {
        return userEntryDateTimeMillis;
    }

    public void setUserEntryDateTimeMillis(long userEntryDateTimeMillis) {
        this.userEntryDateTimeMillis = userEntryDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isUserIsDemo() {
        return userIsDemo;
    }

    public void setUserIsDemo(boolean userIsDemo) {
        this.userIsDemo = userIsDemo;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}