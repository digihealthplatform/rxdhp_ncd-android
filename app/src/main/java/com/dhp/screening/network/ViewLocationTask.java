package com.dhp.screening.network;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;

import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;

import java.util.List;
import java.util.Locale;


public class ViewLocationTask extends AsyncTask<String, Integer, String> {

    private final String location;

    List<Address> addresses;

    private ProgressDialog progressDialog;

    private Context context;

    public ViewLocationTask(Context context, String location) {
        this.context = context;
        this.location = location;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(AssetReader.getLangKeyword("fetching_location"));
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();

        if (null != addresses && 0 < addresses.size()) {
            AlertDialog.Builder builder;
            builder = new AlertDialog.Builder(context);

            try {
                String address = addresses.get(0).getAddressLine(0);

                builder.setMessage(address);
                builder.setPositiveButton(AssetReader.getLangKeyword("close"), null);
                builder.setNeutralButton(AssetReader.getLangKeyword("view_on_map"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String[] loc = location.split(",");

                        String geoUri = "http://maps.google.com/maps?q=loc:" + loc[0] + "," + loc[1] + " (" + "" + ")";

                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                        context.startActivity(intent);
                    }
                });

                builder.show();

            } catch (Exception e) {
                LToast.toast(AssetReader.getLangKeyword("failed_to_fetch_location"));
            }

        } else {
            LToast.toast(AssetReader.getLangKeyword("failed_to_fetch_location"));
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Geocoder geocoder;
        geocoder = new Geocoder(context, Locale.getDefault());

        String[] loc = location.split(",");

        try {
            addresses = geocoder.getFromLocation(Double.valueOf(loc[0]), Double.valueOf(loc[1]), 1);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }
}