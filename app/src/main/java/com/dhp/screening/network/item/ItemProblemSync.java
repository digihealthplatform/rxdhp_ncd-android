package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemProblemSync {

    @SerializedName("problemAutoNumber")
    private int tableKey;

    @SerializedName("problemRegistrationId")
    private String registrationId = "";

    @SerializedName("ehrNumber")
    private String ehrNumber = "";

    @SerializedName("problemGroup")
    private String questionGroup = "";

    @SerializedName("problemQuestion")
    private String question = "";

    @SerializedName("problemQuestionOrder")
    private int questionOrder;

    @SerializedName("problemAnswer")
    private String answer = "";

    @SerializedName("problemSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("problemPartnerId")
    private String partnerId = "";

    @SerializedName("problemCampName")
    private String campName = "";

    @SerializedName("problemDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("problemDeviceId")
    private String deviceId;

    @SerializedName("problemSurveyDateTime")
    private String surveyDateTime = "";

    @SerializedName("problemSurveyDateTimeMilliSeconds")
    private long surveyDateTimeMillis;

    @SerializedName("problemInactive")
    private boolean inactive;

    @SerializedName("problemInactiveReason")
    private String inactiveReason = "";

    @SerializedName("problemModified")
    private boolean modified;

    @SerializedName("problemModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("problemSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getQuestionGroup() {
        return questionGroup;
    }

    public void setQuestionGroup(String questionGroup) {
        this.questionGroup = questionGroup;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(int questionOrder) {
        this.questionOrder = questionOrder;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}