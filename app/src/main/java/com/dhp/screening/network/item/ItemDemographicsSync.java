package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemDemographicsSync {

    @SerializedName("demographicsAutoNumber")
    private int tableKey;

    @SerializedName("demographicsRegistrationId")
    private String registrationId = "";

    @SerializedName("ehrNumber")
    private String ehrNumber = "";

    @SerializedName("demographicsRegistrationName")
    private String fullName = "";

    @SerializedName("demographicsGender")
    private String gender = "";

    @SerializedName("demographicsYearOfBirth")
    private int yob = 0;

    @SerializedName("demographicsDateOfBirth")
    private String dob = "";

    @SerializedName("demographicsMobileNumber")
    private String phoneNumber = "";

    @SerializedName("demographicsEmailId")
    private String emailId = "";

    @SerializedName("demographicsIdentityType")
    private String identityType = "";

    @SerializedName("demographicsIdentityNumber")
    private String identityNumber = "";

    @SerializedName("demographicsHouseId")
    private String houseId = "";

    @SerializedName("demographicsCareOf")
    private String careOf = "";

    @SerializedName("demographicsImageName")
    private String imageName = "";

    @SerializedName("demographicsImagePath")
    private String imagePath = "";

    @SerializedName("demographicsMrNumber")
    private String mrNumber = "";

    @SerializedName("demographicsNotes")
    private String notes = "";

    @SerializedName("demographicsIsDoorToDoor")
    private boolean doorToDoor;

    @SerializedName("demographicsGpsLocation")
    private String gpsLocation = "";

    @SerializedName("demographicsQrScanned")
    private boolean qrScanned;

    @SerializedName("demographicsSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("demographicsPartnerId")
    private String partnerId = "";

    @SerializedName("demographicsCampName")
    private String campName = "";

    @SerializedName("demographicsDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("demographicsDeviceId")
    private String deviceId;

    @SerializedName("demographicsSurveyDateTime")
    private String surveyDateTime = "";

    @SerializedName("demographicsSurveyDateTimeMilliSeconds")
    private long surveyDateTimeMillis;

    @SerializedName("demographicsInactive")
    private boolean inactive;

    @SerializedName("demographicsInactiveReason")
    private String inactiveReason = "";

    @SerializedName("demographicsModified")
    private boolean modified;

    @SerializedName("demographicsModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("demographicsSynced")
    private boolean synced = false;

    @SerializedName("demographicsHouseFileName")
    private String houseFileName = "";

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getYob() {
        return yob;
    }

    public void setYob(int yob) {
        this.yob = yob;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getCareOf() {
        return careOf;
    }

    public void setCareOf(String careOf) {
        this.careOf = careOf;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isDoorToDoor() {
        return doorToDoor;
    }

    public void setDoorToDoor(boolean doorToDoor) {
        this.doorToDoor = doorToDoor;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public boolean isQrScanned() {
        return qrScanned;
    }

    public void setQrScanned(boolean qrScanned) {
        this.qrScanned = qrScanned;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getHouseFileName() {
        return houseFileName;
    }

    public void setHouseFileName(String houseFileName) {
        this.houseFileName = houseFileName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getMrNumber() {
        return mrNumber;
    }

    public void setMrNumber(String mrNumber) {
        this.mrNumber = mrNumber;
    }
}