package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;


public class ItemImageSync {

    @SerializedName("imageAutoNumber")
    private int tableKey;

    @SerializedName("imageRegistrationId")
    private String registrationId = "";

    @SerializedName("ehrNumber")
    private String ehrNumber = "";

    @SerializedName("imageType")
    private String imageType = "";

    @SerializedName("imageName")
    private String imageName = "";

    @SerializedName("imageGpsLocation")
    private String gpsLocation = "";

    @SerializedName("imageDateTime")
    private String imageDateTime = "";

    @SerializedName("imageDateTimeMillis")
    private long imageDateTimeMillis;

    @SerializedName("imageCamera")
    private boolean imageCamera;

    @SerializedName("imageNotes")
    private String notes = "";

    @SerializedName("imageSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("imagePartnerId")
    private String partnerId = "";

    @SerializedName("imageCampName")
    private String campName = "";

    @SerializedName("imageDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("imageDeviceId")
    private String deviceId;

    @SerializedName("imageSurveyDateTime")
    private String surveyDateTime = "";

    @SerializedName("imageSurveyDateTimeMilliSeconds")
    private long surveyDateTimeMillis;

    @SerializedName("imageInactive")
    private boolean inactive;

    @SerializedName("imageInactiveReason")
    private String inactiveReason = "";

    @SerializedName("imageModified")
    private boolean modified;

    @SerializedName("imageModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("imageSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getImageDateTime() {
        return imageDateTime;
    }

    public void setImageDateTime(String imageDateTime) {
        this.imageDateTime = imageDateTime;
    }

    public long getImageDateTimeMillis() {
        return imageDateTimeMillis;
    }

    public void setImageDateTimeMillis(long imageDateTimeMillis) {
        this.imageDateTimeMillis = imageDateTimeMillis;
    }

    public boolean isImageCamera() {
        return imageCamera;
    }

    public void setImageCamera(boolean imageCamera) {
        this.imageCamera = imageCamera;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}