package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;


public class ItemHouseSync {

    @SerializedName("houseAutoNumber")
    private int tableKey;

    @SerializedName("houseId")
    private String houseId = "";

    @SerializedName("houseNumber")
    private String houseNo = "";

    @SerializedName("houseStreet")
    private String street = "";

    @SerializedName("housePost")
    private String post = "";

    @SerializedName("houseLocality")
    private String locality = "";

    @SerializedName("houseDistrict")
    private String district = "";

    @SerializedName("houseState")
    private String state = "";

    @SerializedName("houseCountry")
    private String country = "";

    @SerializedName("housePincode")
    private int pinCode = 0;

    @SerializedName("houseHead")
    private String headOfHouse = "";

    @SerializedName("houseRespondent")
    private String respondent = "";

    @SerializedName("houseTotalMembers")
    private int totalMembers = 0;

    @SerializedName("houseOwnership")
    private String houseOwnership = "";

    @SerializedName("houseRationCard")
    private String rationCard = "";

    @SerializedName("houseHealthInsurance")
    private String healthInsurance = "";

    @SerializedName("houseMealTimes")
    private String mealTimes = "";

    @SerializedName("houseCookingFuel")
    private String cookingFuel = "";

    @SerializedName("houseTreatmentPlace")
    private String treatmentPlace = "";

    @SerializedName("houseRecentDeath")
    private String recentDeath = "";

    @SerializedName("houseSpouseOfHouseHead")
    private String spouseOfHeadOfHouse = "";

    @SerializedName("houseStatus")
    private String houseStatus = "";

    @SerializedName("houseTotalRooms")
    private int totalRooms = 0;

    @SerializedName("houseToiletAvailability")
    private String toiletAvailability = "";

    @SerializedName("houseDrinkingWaterSource")
    private String drinkingWaterSource = "";

    @SerializedName("houseElectricityAvailability")
    private String electricityAvailability = "";

    @SerializedName("houseMotorisedVehicle")
    private String motorisedVehicle = "";

    @SerializedName("houseVillage")
    private String village = "";

    @SerializedName("houseGramPanchayat")
    private String gramPanchayat = "";

    @SerializedName("houseNotes")
    private String notes = "";

    @SerializedName("houseGpsLocation")
    private String gpsLocation = "";

    @SerializedName("houseSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("housePartnerId")
    private String partnerId = "";

    @SerializedName("houseCampName")
    private String campName = "";

    @SerializedName("houseDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("houseDeviceId")
    private String deviceId;

    @SerializedName("houseSurveyDateTime")
    private String surveyDateTime = "";

    @SerializedName("houseSurveyDateTimeMilliSeconds")
    private long surveyDateTimeMillis;

    @SerializedName("houseInactive")
    private boolean inactive;

    @SerializedName("houseInactiveReason")
    private String inactiveReason = "";

    @SerializedName("houseModified")
    private boolean modified;

    @SerializedName("houseModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("houseSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    @SerializedName("houseEarningMembers")
    private int earningMembers;

    @SerializedName("houseTotalIncome")
    private int totalIncome;

    @SerializedName("houseTreatmentCost")
    private int treatmentCost;

    @SerializedName("houseDrinkWeekly")
    private String drinkWeekly = "";

    @SerializedName("houseDrugsUse")
    private String drugsUse = "";

    @SerializedName("houseHandicappedPresent")
    private String handicappedPresent = "";

    @SerializedName("houseHandicappedSuffering")
    private String handicappedSuffering = "";

    @SerializedName("housePets")
    private String pets = "";

    @SerializedName("housePetsBitten")
    private String petsBitten = "";

    @SerializedName("housePetsBiteAction")
    private String petsBiteAction = "";

    @SerializedName("houseDrinkingWaterCleaningMethod")
    private String drinkingWaterCleaningMethod = "";

    @SerializedName("houseBathingToiletFacility")
    private String bathingToiletFacility = "";

    @SerializedName("houseChildHealthCheckupLastDone")
    private String childHealthCheckupLastDone = "";

    @SerializedName("houseChildrenVaccinated")
    private String childrenVaccinated = "";

    @SerializedName("houseVaccinationRecordsStorage")
    private String vaccinationRecordsStorage = "";

    @SerializedName("houseChildGenderResponsibility")
    private String childGenderResponsibility = "";

    @SerializedName("housePreferredChildGender")
    private String preferredChildGender = "";

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public String getHeadOfHouse() {
        return headOfHouse;
    }

    public void setHeadOfHouse(String headOfHouse) {
        this.headOfHouse = headOfHouse;
    }

    public String getRespondent() {
        return respondent;
    }

    public void setRespondent(String respondent) {
        this.respondent = respondent;
    }

    public int getTotalMembers() {
        return totalMembers;
    }

    public void setTotalMembers(int totalMembers) {
        this.totalMembers = totalMembers;
    }

    public String getHouseOwnership() {
        return houseOwnership;
    }

    public void setHouseOwnership(String houseOwnership) {
        this.houseOwnership = houseOwnership;
    }

    public String getRationCard() {
        return rationCard;
    }

    public void setRationCard(String rationCard) {
        this.rationCard = rationCard;
    }

    public String getHealthInsurance() {
        return healthInsurance;
    }

    public void setHealthInsurance(String healthInsurance) {
        this.healthInsurance = healthInsurance;
    }

    public String getMealTimes() {
        return mealTimes;
    }

    public void setMealTimes(String mealTimes) {
        this.mealTimes = mealTimes;
    }

    public String getCookingFuel() {
        return cookingFuel;
    }

    public void setCookingFuel(String cookingFuel) {
        this.cookingFuel = cookingFuel;
    }

    public String getTreatmentPlace() {
        return treatmentPlace;
    }

    public void setTreatmentPlace(String treatmentPlace) {
        this.treatmentPlace = treatmentPlace;
    }

    public String getRecentDeath() {
        return recentDeath;
    }

    public void setRecentDeath(String recentDeath) {
        this.recentDeath = recentDeath;
    }

    public String getSpouseOfHeadOfHouse() {
        return spouseOfHeadOfHouse;
    }

    public void setSpouseOfHeadOfHouse(String spouseOfHeadOfHouse) {
        this.spouseOfHeadOfHouse = spouseOfHeadOfHouse;
    }

    public String getHouseStatus() {
        return houseStatus;
    }

    public void setHouseStatus(String houseStatus) {
        this.houseStatus = houseStatus;
    }

    public int getTotalRooms() {
        return totalRooms;
    }

    public void setTotalRooms(int totalRooms) {
        this.totalRooms = totalRooms;
    }

    public String getToiletAvailability() {
        return toiletAvailability;
    }

    public void setToiletAvailability(String toiletAvailability) {
        this.toiletAvailability = toiletAvailability;
    }

    public String getDrinkingWaterSource() {
        return drinkingWaterSource;
    }

    public void setDrinkingWaterSource(String drinkingWaterSource) {
        this.drinkingWaterSource = drinkingWaterSource;
    }

    public String getElectricityAvailability() {
        return electricityAvailability;
    }

    public void setElectricityAvailability(String electricityAvailability) {
        this.electricityAvailability = electricityAvailability;
    }

    public String getMotorisedVehicle() {
        return motorisedVehicle;
    }

    public void setMotorisedVehicle(String motorisedVehicle) {
        this.motorisedVehicle = motorisedVehicle;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getGramPanchayat() {
        return gramPanchayat;
    }

    public void setGramPanchayat(String gramPanchayat) {
        this.gramPanchayat = gramPanchayat;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public int getEarningMembers() {
        return earningMembers;
    }

    public void setEarningMembers(int earningMembers) {
        this.earningMembers = earningMembers;
    }

    public int getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(int totalIncome) {
        this.totalIncome = totalIncome;
    }

    public int getTreatmentCost() {
        return treatmentCost;
    }

    public void setTreatmentCost(int treatmentCost) {
        this.treatmentCost = treatmentCost;
    }

    public String getDrinkWeekly() {
        return drinkWeekly;
    }

    public void setDrinkWeekly(String drinkWeekly) {
        this.drinkWeekly = drinkWeekly;
    }

    public String getDrugsUse() {
        return drugsUse;
    }

    public void setDrugsUse(String drugsUse) {
        this.drugsUse = drugsUse;
    }

    public String getHandicappedPresent() {
        return handicappedPresent;
    }

    public void setHandicappedPresent(String handicappedPresent) {
        this.handicappedPresent = handicappedPresent;
    }

    public String getHandicappedSuffering() {
        return handicappedSuffering;
    }

    public void setHandicappedSuffering(String handicappedSuffering) {
        this.handicappedSuffering = handicappedSuffering;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getPetsBitten() {
        return petsBitten;
    }

    public void setPetsBitten(String petsBitten) {
        this.petsBitten = petsBitten;
    }

    public String getPetsBiteAction() {
        return petsBiteAction;
    }

    public void setPetsBiteAction(String petsBiteAction) {
        this.petsBiteAction = petsBiteAction;
    }

    public String getDrinkingWaterCleaningMethod() {
        return drinkingWaterCleaningMethod;
    }

    public void setDrinkingWaterCleaningMethod(String drinkingWaterCleaningMethod) {
        this.drinkingWaterCleaningMethod = drinkingWaterCleaningMethod;
    }

    public String getBathingToiletFacility() {
        return bathingToiletFacility;
    }

    public void setBathingToiletFacility(String bathingToiletFacility) {
        this.bathingToiletFacility = bathingToiletFacility;
    }

    public String getChildHealthCheckupLastDone() {
        return childHealthCheckupLastDone;
    }

    public void setChildHealthCheckupLastDone(String childHealthCheckupLastDone) {
        this.childHealthCheckupLastDone = childHealthCheckupLastDone;
    }

    public String getChildrenVaccinated() {
        return childrenVaccinated;
    }

    public void setChildrenVaccinated(String childrenVaccinated) {
        this.childrenVaccinated = childrenVaccinated;
    }

    public String getVaccinationRecordsStorage() {
        return vaccinationRecordsStorage;
    }

    public void setVaccinationRecordsStorage(String vaccinationRecordsStorage) {
        this.vaccinationRecordsStorage = vaccinationRecordsStorage;
    }

    public String getChildGenderResponsibility() {
        return childGenderResponsibility;
    }

    public void setChildGenderResponsibility(String childGenderResponsibility) {
        this.childGenderResponsibility = childGenderResponsibility;
    }

    public String getPreferredChildGender() {
        return preferredChildGender;
    }

    public void setPreferredChildGender(String preferredChildGender) {
        this.preferredChildGender = preferredChildGender;
    }
}