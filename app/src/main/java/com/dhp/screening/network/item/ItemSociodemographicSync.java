package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;


public class ItemSociodemographicSync {

    @SerializedName("socioAutoNumber")
    private int tableKey;

    @SerializedName("socioRegistrationId")
    private String registrationId = "";

    @SerializedName("ehrNumber")
    private String ehrNumber = "";

    @SerializedName("socioRelationToHouseHead")
    private String relationToHoh = "";

    @SerializedName("socioMaritalStatus")
    private String maritalStatus = "";

    @SerializedName("socioEducationLevel")
    private String education = "";

    @SerializedName("socioLiteracy")
    private String literacy = "";

    @SerializedName("socioCurrentOccupation")
    private String currentOccupation = "";

    @SerializedName("socioMonthlyIncome")
    private int monthlyIncome;

    @SerializedName("socioHouseType")
    private String houseType = "";

    @SerializedName("socioLanguagesSpoken")
    private String languagesSpoken = "";

    @SerializedName("socioHabits")
    private String habits = "";

    @SerializedName("socioExercise")
    private String exercise = "";

    @SerializedName("socioDiet")
    private String diet = "";

    @SerializedName("socioDisease")
    private String disease = "";

    @SerializedName("socioDiseaseTreatment")
    private String diseaseTreatment = "";

    @SerializedName("socioDisability")
    private String disability = "";

    @SerializedName("socioSickDuringSurvey")
    private String sick = "";

    @SerializedName("socioAilment")
    private String ailment = "";

    @SerializedName("socioHealthCard")
    private String socioHealthCard = "";

    @SerializedName("socioNotes")
    private String notes = "";

    @SerializedName("socioSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("socioPartnerId")
    private String partnerId = "";

    @SerializedName("socioCampName")
    private String campName = "";

    @SerializedName("socioDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("socioDeviceId")
    private String deviceId;

    @SerializedName("socioSurveyDateTime")
    private String surveyDateTime = "";

    @SerializedName("socioSurveyDateTimeMilliSeconds")
    private long surveyDateTimeMillis;

    @SerializedName("socioInactive")
    private boolean inactive;

    @SerializedName("socioInactiveReason")
    private String inactiveReason = "";

    @SerializedName("socioModified")
    private boolean modified;

    @SerializedName("socioModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("socioSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    @SerializedName("socioMedicinesTakenDaily")
    private String medicinesTakenDaily = "";

    @SerializedName("socioDiseaseTestLastDone")
    private String diseaseTestLastDone = "";

    @SerializedName("socioInformedAboutDisease")
    private String informedAboutDisease = "";

    @SerializedName("socioMedicationForDisease")
    private String medicationForDisease = "";

    @SerializedName("socioChangesToControlDisease")
    private String changesToControlDisease = "";

    @SerializedName("socioWomenSpecialPrecautionsOnPeriod")
    private String womenSpecialPrecautionsOnPeriod = "";

    @SerializedName("socioMobileUsed")
    private String mobileUsed = "";

    @SerializedName("socioMobileComfort")
    private String mobileComfort = "";

    @SerializedName("socioHealthRecordsStorage")
    private String healthRecordsStorage = "";

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getRelationToHoh() {
        return relationToHoh;
    }

    public void setRelationToHoh(String relationToHoh) {
        this.relationToHoh = relationToHoh;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getLiteracy() {
        return literacy;
    }

    public void setLiteracy(String literacy) {
        this.literacy = literacy;
    }

    public String getCurrentOccupation() {
        return currentOccupation;
    }

    public void setCurrentOccupation(String currentOccupation) {
        this.currentOccupation = currentOccupation;
    }

    public int getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(int monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public String getLanguagesSpoken() {
        return languagesSpoken;
    }

    public void setLanguagesSpoken(String languagesSpoken) {
        this.languagesSpoken = languagesSpoken;
    }

    public String getHabits() {
        return habits;
    }

    public void setHabits(String habits) {
        this.habits = habits;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getDiseaseTreatment() {
        return diseaseTreatment;
    }

    public void setDiseaseTreatment(String diseaseTreatment) {
        this.diseaseTreatment = diseaseTreatment;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getSick() {
        return sick;
    }

    public void setSick(String sick) {
        this.sick = sick;
    }

    public String getAilment() {
        return ailment;
    }

    public void setAilment(String ailment) {
        this.ailment = ailment;
    }

    public String getSocioHealthCard() {
        return socioHealthCard;
    }

    public void setSocioHealthCard(String socioHealthCard) {
        this.socioHealthCard = socioHealthCard;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getMedicinesTakenDaily() {
        return medicinesTakenDaily;
    }

    public void setMedicinesTakenDaily(String medicinesTakenDaily) {
        this.medicinesTakenDaily = medicinesTakenDaily;
    }

    public String getDiseaseTestLastDone() {
        return diseaseTestLastDone;
    }

    public void setDiseaseTestLastDone(String diseaseTestLastDone) {
        this.diseaseTestLastDone = diseaseTestLastDone;
    }

    public String getInformedAboutDisease() {
        return informedAboutDisease;
    }

    public void setInformedAboutDisease(String informedAboutDisease) {
        this.informedAboutDisease = informedAboutDisease;
    }

    public String getMedicationForDisease() {
        return medicationForDisease;
    }

    public void setMedicationForDisease(String medicationForDisease) {
        this.medicationForDisease = medicationForDisease;
    }

    public String getChangesToControlDisease() {
        return changesToControlDisease;
    }

    public void setChangesToControlDisease(String changesToControlDisease) {
        this.changesToControlDisease = changesToControlDisease;
    }

    public String getWomenSpecialPrecautionsOnPeriod() {
        return womenSpecialPrecautionsOnPeriod;
    }

    public void setWomenSpecialPrecautionsOnPeriod(String womenSpecialPrecautionsOnPeriod) {
        this.womenSpecialPrecautionsOnPeriod = womenSpecialPrecautionsOnPeriod;
    }

    public String getMobileUsed() {
        return mobileUsed;
    }

    public void setMobileUsed(String mobileUsed) {
        this.mobileUsed = mobileUsed;
    }

    public String getMobileComfort() {
        return mobileComfort;
    }

    public void setMobileComfort(String mobileComfort) {
        this.mobileComfort = mobileComfort;
    }

    public String getHealthRecordsStorage() {
        return healthRecordsStorage;
    }

    public void setHealthRecordsStorage(String healthRecordsStorage) {
        this.healthRecordsStorage = healthRecordsStorage;
    }
}