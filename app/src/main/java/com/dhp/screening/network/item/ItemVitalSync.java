package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemVitalSync {

    @SerializedName("vitalAutoNumber")
    private int tableKey;

    @SerializedName("vitalRegistrationId")
    private String registrationId = "";

    @SerializedName("ehrNumber")
    private String ehrNumber = "";

    @SerializedName("vitalType")
    private String vitalType = "";

    @SerializedName("vitalSubTypes")
    private String vitalSubTypes = "";

    @SerializedName("vitalValues")
    private String vitalValues = "";

    @SerializedName("vitalUnits")
    private String vitalUnits = "";

    @SerializedName("vitalDateTime")
    private String vitalDateTime = "";

    @SerializedName("vitalDateTimeMilliSeconds")
    private long vitalDateTimeMillis = 0;

    @SerializedName("vitalImage")
    private String vitalImage = "";

    @SerializedName("vitalNotes")
    private String notes = "";

    @SerializedName("vitalDeviceName")
    private String deviceName = "";

    @SerializedName("vitalGpsLocation")
    private String gpsLocation = "";

    @SerializedName("vitalQrScanned")
    private boolean qrScanned;

    @SerializedName("vitalSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("vitalPartnerId")
    private String partnerId = "";

    @SerializedName("vitalCampName")
    private String campName = "";

    @SerializedName("vitalDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("vitalDeviceId")
    private String deviceId;

    @SerializedName("vitalSurveyDateTime")
    private String surveyDateTime = "";

    @SerializedName("vitalSurveyDateTimeMilliSeconds")
    private long surveyDateTimeMillis;

    @SerializedName("vitalInactive")
    private boolean inactive;

    @SerializedName("vitalInactiveReason")
    private String inactiveReason = "";

    @SerializedName("vitalModified")
    private boolean modified;

    @SerializedName("vitalModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("vitalSynced")
    private boolean synced = false;

    @SerializedName("vitalDemographicsFileName")
    private String DemographicsFileName = "";

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getVitalType() {
        return vitalType;
    }

    public void setVitalType(String vitalType) {
        this.vitalType = vitalType;
    }

    public String getVitalSubTypes() {
        return vitalSubTypes;
    }

    public void setVitalSubTypes(String vitalSubTypes) {
        this.vitalSubTypes = vitalSubTypes;
    }

    public String getVitalValues() {
        return vitalValues;
    }

    public void setVitalValues(String vitalValues) {
        this.vitalValues = vitalValues;
    }

    public String getVitalUnits() {
        return vitalUnits;
    }

    public void setVitalUnits(String vitalUnits) {
        this.vitalUnits = vitalUnits;
    }

    public String getVitalDateTime() {
        return vitalDateTime;
    }

    public void setVitalDateTime(String vitalDateTime) {
        this.vitalDateTime = vitalDateTime;
    }

    public long getVitalDateTimeMillis() {
        return vitalDateTimeMillis;
    }

    public void setVitalDateTimeMillis(long vitalDateTimeMillis) {
        this.vitalDateTimeMillis = vitalDateTimeMillis;
    }

    public String getVitalImage() {
        return vitalImage;
    }

    public void setVitalImage(String vitalImage) {
        this.vitalImage = vitalImage;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public boolean isQrScanned() {
        return qrScanned;
    }

    public void setQrScanned(boolean qrScanned) {
        this.qrScanned = qrScanned;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getDemographicsFileName() {
        return DemographicsFileName;
    }

    public void setDemographicsFileName(String demographicsFileName) {
        DemographicsFileName = demographicsFileName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}