package com.dhp.screening.network.item;

import com.google.gson.annotations.SerializedName;

public class ItemDeathInfoSync {

    @SerializedName("deathAutoNumber")
    private int tableKey;

    @SerializedName("deathHouseId")
    private String houseId;

    @SerializedName("deathPersonName")
    private String personName = "";

    @SerializedName("deathGender")
    private String gender;

    @SerializedName("deathAge")
    private int age;

    @SerializedName("deathAgeUnit")
    private String ageUnit = "";

    @SerializedName("deathRelationToHouseHead")
    private String relationToHoh = "";

    @SerializedName("deathCause")
    private String deathCause = "";

    @SerializedName("deathNotes")
    private String notes = "";

    @SerializedName("deathSurveyorUserId")
    private String surveyorUserId = "";

    @SerializedName("deathPartnerId")
    private String partnerId = "";

    @SerializedName("deathCampName")
    private String campName = "";

    @SerializedName("deathDeviceNumber")
    private String deviceNumber = "";

    @SerializedName("deathDeviceId")
    private String deviceId;

    @SerializedName("deathSurveyDateTime")
    private String surveyDateTime = "";

    @SerializedName("deathEntryDateTimeMilliSeconds")
    private long surveyDateTimeMillis;

    @SerializedName("deathInactive")
    private boolean inactive;

    @SerializedName("deathInactiveReason")
    private String inactiveReason = "";

    @SerializedName("deathModified")
    private boolean modified;

    @SerializedName("deathModifiedDateTime")
    private String modifiedDateTime = "";

    @SerializedName("deathSynced")
    private boolean synced = false;

    @SerializedName("version")
    private String version = "";

    @SerializedName("appVersion")
    private String appVersion = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAgeUnit() {
        return ageUnit;
    }

    public void setAgeUnit(String ageUnit) {
        this.ageUnit = ageUnit;
    }

    public String getRelationToHoh() {
        return relationToHoh;
    }

    public void setRelationToHoh(String relationToHoh) {
        this.relationToHoh = relationToHoh;
    }

    public String getDeathCause() {
        return deathCause;
    }

    public void setDeathCause(String deathCause) {
        this.deathCause = deathCause;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}