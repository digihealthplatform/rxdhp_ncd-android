package com.dhp.screening.data.range;

import com.google.gson.annotations.SerializedName;

public class ItemBpRange {

    @SerializedName("range")
    private String range = "";

    @SerializedName("min")
    private ItemBp min;

    @SerializedName("max")
    private ItemBp max;

    @SerializedName("color")
    private String color;

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public ItemBp getMin() {
        return min;
    }

    public void setMin(ItemBp min) {
        this.min = min;
    }

    public ItemBp getMax() {
        return max;
    }

    public void setMax(ItemBp max) {
        this.max = max;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}