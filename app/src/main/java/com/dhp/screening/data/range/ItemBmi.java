package com.dhp.screening.data.range;

import com.google.gson.annotations.SerializedName;

public class ItemBmi {

    @SerializedName("age_min")
    private String ageMin = "";

    @SerializedName("age_max")
    private String ageMax = "";

    @SerializedName("ranges")
    private ItemBmiRange[] itemBmiRange;

    public String getAgeMin() {
        return ageMin;
    }

    public void setAgeMin(String ageMin) {
        this.ageMin = ageMin;
    }

    public String getAgeMax() {
        return ageMax;
    }

    public void setAgeMax(String ageMax) {
        this.ageMax = ageMax;
    }

    public ItemBmiRange[] getItemBmiRange() {
        return itemBmiRange;
    }

    public void setItemBmiRange(ItemBmiRange[] itemBmiRange) {
        this.itemBmiRange = itemBmiRange;
    }
}