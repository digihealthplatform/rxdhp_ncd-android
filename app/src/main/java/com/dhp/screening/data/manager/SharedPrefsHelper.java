package com.dhp.screening.data.manager;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SharedPrefsHelper {

    public static final String SHARED_PREFS_NAME = "screening_app_shared_prefs";

    public static final String KEY_LOGGED_IN             = "logged_in";
    public static final String KEY_REMEMBER_LOG_IN       = "remember_login";
    public static final String LOGGED_IN_USER_ID         = "logged_in_user_id";
    public static final String ADMIN_TYPE                = "admin_type";
    public static final String KEY_AUTO_SYNC             = "auto_sync";
    public static final String KEY_ALLOW_SYNC            = "allow_sync";
    public static final String KEY_PRINT_ID              = "print_id";
    public static final String KEY_DEVICE_NUMBER         = "device_number";
    public static final String KEY_LAST_SYNC_DATE_TIME   = "last_sync_date_time";
    public static final String KEY_LAST_SYNC_MILLIS      = "last_sync_millis";
    public static final String KEY_ASSETS_COPIED_VERSION = "assets_copied_version";

    public static final String KEY_DEFAULT_VILLAGE        = "default_village";
    public static final String KEY_DEFAULT_GRAM_PANCHAYAT = "default_gram_panchayat";

    public static final String KEY_DEFAULT_DISTRICT = "default_district";
    public static final String KEY_DEFAULT_STATE    = "default_state";

    public static final String KEY_CUSTOM_TEXT_TO_PRINT = "custom_text_to_print";

    public static final String KEY_HB_WAIT_DURATION = "key_hb_wait_duration";

    public static final String KEY_L_VALUES = "key_l_values";
    public static final String KEY_A_VALUES = "key_a_values";
    public static final String KEY_B_VALUES = "key_b_values";

    public static final String KEY_BMI_FROM        = "bmi_from";
    public static final String KEY_BMI_TO          = "bmi_to";
    public static final String KEY_BP_FROM         = "bp_from";
    public static final String KEY_BP_TO           = "bp_to";
    public static final String KEY_RBS_FROM        = "rbs_from";
    public static final String KEY_RBS_TO          = "rbs_to";
    public static final String KEY_HEMOGLOBIN_FROM = "hemoglobin_from";
    public static final String KEY_HEMOGLOBIN_TO   = "hemoglobin_to";
    public static final String KEY_ALBUMIN_FROM    = "albumin_from";
    public static final String KEY_ALBUMIN_TO      = "albumin_to";

    public static final String VITAL_TYPE = "vital_type";
    public static final String VITAL_ID   = "vital_id";

    public static int SUPER_ADMIN_1 = 1;
    public static int SUPER_ADMIN_2 = 2;
    public static int SUPER_ADMIN_3 = 3;

    public static final int NOT_LOGGED_IN      = 1;
    public static final int USER_LOGGED_IN     = 2;
    public static final int ADMIN_LOGGED_IN    = 3;
    public static final int ADMIN_HB_LOGGED_IN = 4;
    public static final int ADMIN4_LOGGED_IN   = 5;

    public static final int ADMIN_PARTNER_ID_LOGGED_IN    = 6;
    public static final int ADMIN_DEVICE_NUMBER_LOGGED_IN = 7;
    public static final int ADMIN_EXPORT_LOGGED_IN        = 8;
    public static final int ADMIN_IMPORT_LOGGED_IN        = 9;

    public static final boolean AUTO_SYNC_ENABLED = false;
    public static final boolean PRINT_ID          = false;

    public static final String KEY_DEFAULT_USER_CREATED = "key_default_user_created";

    private SharedPreferences sharedPreferences;

    @Inject
    public SharedPrefsHelper(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void setPref(String key, int value) {
        sharedPreferences.edit().putInt(key, value).apply();
    }

    public void setPref(String key, long value) {
        sharedPreferences.edit().putLong(key, value).apply();
    }

    public void setPref(String key, boolean value) {
        sharedPreferences.edit().putBoolean(key, value).apply();
    }

    public void setPref(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public int getIntPref(String key, int defaultValue) {
        return sharedPreferences.getInt(key, defaultValue);
    }

    public long getLongPref(String key, long defaultValue) {
        return sharedPreferences.getLong(key, defaultValue);
    }

    public boolean getBooleanPref(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

    public String getStringPref(String key, String defaultValue) {
        return sharedPreferences.getString(key, defaultValue);
    }
}