package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.ui.images.ActivityImageDetails;

import java.util.List;

import static com.dhp.screening.util.LUtils.getFormattedDateTimeAm;


public class AdapterImages extends RecyclerView.Adapter<AdapterImages.ViewHolder> {

    private final Context context;

    private List<TableImage> items;

    private final LayoutInflater layoutInflater;

    public AdapterImages(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(List<TableImage> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public AdapterImages.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_image, parent, false);
        return new AdapterImages.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterImages.ViewHolder holder, int position) {
        holder.tvDateTime.setText(getFormattedDateTimeAm(items.get(position).getImageDateTimeMillis()));
        // TODO: 24-Aug-19 , lang specific
        holder.tvImageType.setText(items.get(position).getImageType());
    }

    @Override
    public int getItemCount() {
        if (null != items) {
            return items.size();
        }

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final TextView tvDateTime;
        private final TextView tvImageType;

        ViewHolder(View itemView) {
            super(itemView);
            tvDateTime = (TextView) itemView.findViewById(R.id.tv_date_time);
            tvImageType = (TextView) itemView.findViewById(R.id.tv_image_type);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityImageDetails.class);
            intent.putExtra("table_key", items.get(getAdapterPosition()).getTableKey());
            context.startActivity(intent);
        }
    }
}