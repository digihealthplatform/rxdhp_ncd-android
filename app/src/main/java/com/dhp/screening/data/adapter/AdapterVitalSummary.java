package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.dhp.screening.R;
import com.dhp.screening.data.item.ItemVitalValue;

public class AdapterVitalSummary extends RecyclerView.Adapter<AdapterVitalSummary.ViewHolder> {

    private ArrayList<ItemVitalValue> vitalValues;

    private final LayoutInflater layoutInflater;

    public AdapterVitalSummary(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(ArrayList<ItemVitalValue> vitalValues) {
        this.vitalValues = vitalValues;
        notifyDataSetChanged();
    }

    @Override
    public AdapterVitalSummary.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_vital_summary, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final AdapterVitalSummary.ViewHolder holder, final int position) {
        holder.tvVital.setText(vitalValues.get(position).getVital() + ":");
        holder.tvValue.setText(vitalValues.get(position).getValue() + " " + vitalValues.get(position).getUnit());
    }

    @Override
    public int getItemCount() {
        if (null != vitalValues) {
            return vitalValues.size();
        }

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvVital;
        private TextView tvValue;

        ViewHolder(View itemView) {
            super(itemView);
            tvVital = (TextView) itemView.findViewById(R.id.tv_vital);
            tvValue = (TextView) itemView.findViewById(R.id.tv_value);
        }
    }
}