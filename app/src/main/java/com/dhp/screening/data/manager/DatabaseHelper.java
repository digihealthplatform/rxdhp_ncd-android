package com.dhp.screening.data.manager;

import android.content.Context;
import android.provider.Settings;

import com.dhp.screening.LApplication;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.di.ApplicationContext;
import com.dhp.screening.data.di.DatabaseInfo;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemSummary;
import com.dhp.screening.data.item.ItemSurveyDetails;
import com.dhp.screening.data.item.ItemUserSurveySummary;
import com.dhp.screening.data.item.ItemVitalHeader;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.data.table.TableCampInfo_Table;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.data.table.TableDeathInfo_Table;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableDemographics_Table;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableHouse_Table;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.data.table.TableImage_Table;
import com.dhp.screening.data.table.TableKitConfig;
import com.dhp.screening.data.table.TableKitConfig_Table;
import com.dhp.screening.data.table.TableProblems;
import com.dhp.screening.data.table.TableProblems_Table;
import com.dhp.screening.data.table.TableSociodemographics;
import com.dhp.screening.data.table.TableSociodemographics_Table;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.data.table.TableStock_Table;
import com.dhp.screening.data.table.TableSubscription;
import com.dhp.screening.data.table.TableTarget;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.data.table.TableUser_Table;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.data.table.TableVitals_Table;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LUtils;
import com.raizlabs.android.dbflow.sql.language.SQLOperator;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.dhp.screening.data.DataManager.PATIENT_INFO;
import static com.dhp.screening.data.DataManager.SOCIODEMOGRAPHICS;
import static com.dhp.screening.data.asset.AssetReader.KEY_ACCEPT_AADHAAR_NUMBER;
import static com.dhp.screening.util.LConstants.DEFAULT_PARTNER_ID;
import static com.dhp.screening.util.LUtils.hasValue;

@Singleton
public class DatabaseHelper {

    private Context context;

    @Inject
    DatabaseHelper(@ApplicationContext Context context,
                   @DatabaseInfo String dbName,
                   @DatabaseInfo Integer version) {
        this.context = context;
    }

    public void createOrUpdateCampInfo(TableCampInfo campInfo) {
        try {
            if (null != getCampInfo()) {
                TableCampInfo campInfoOld = getCampInfo();
                campInfoOld.setModified(true);
                campInfoOld.setModifiedDateTime(LUtils.getCurrentDateTime());
                campInfoOld.setSynced(false);
                campInfoOld.update();

                campInfo.setEntryDateTime((LUtils.getCurrentDateTime()));
                campInfo.setEntryDateTimeMillis((LUtils.getCurrentDateTimeMillis()));
                campInfo.setTableKey(0);
                campInfo.setDeviceId(getDeviceId());
                campInfo.setSynced(false);
                campInfo.save();

            } else {
                campInfo.setDeviceId(getDeviceId());
                campInfo.setEntryDateTime((LUtils.getCurrentDateTime()));
                campInfo.setEntryDateTimeMillis((LUtils.getCurrentDateTimeMillis()));
                campInfo.save();
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public TableCampInfo getCampInfo() {
        try {
            return SQLite.select()
                    .from(TableCampInfo.class)
                    .where(TableCampInfo_Table.modified.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public List<TableCampInfo> getCampInfo(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableCampInfo.class)
                    .where(TableCampInfo_Table.synced.eq(synced))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public void createUser(TableUser user, ItemSurveyDetails surveyDetails) {
        try {
//            user.setIdentityType("");
//            user.setIdentityNumber("");

            user.setPartnerId(surveyDetails.getPartnerId());
            user.setCampName(surveyDetails.getCampName());
            user.setDeviceNumber(surveyDetails.getDeviceNumber());
            user.setDeviceId(getDeviceId());
            user.setEntryDateTime(LUtils.getCurrentDateTime());
            user.setEntryDateTimeMillis(LUtils.getCurrentDateTimeMillis());
            user.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public TableUser getUser(String userId) {
        try {
            return SQLite.select()
                    .from(TableUser.class)
                    .where(TableUser_Table.user_id.eq(userId))
                    .and(TableUser_Table.modified.eq(false))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TableUser();
    }

    private TableUser getUser(int tableKey) {
        try {
            return SQLite.select()
                    .from(TableUser.class)
                    .where(TableUser_Table.table_key.eq(tableKey))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TableUser();
    }

    public List<TableUser> getAllUsers() {
        try {
            return SQLite.select()
                    .from(TableUser.class)
                    .where(TableUser_Table.modified.notEq(true))
                    .orderBy(TableHouse_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public List<TableUser> getAllActiveUsers() {
        try {
            return SQLite.select()
                    .from(TableUser.class)
                    .where(TableUser_Table.modified.notEq(true))
                    .and(TableUser_Table.inactive.notEq(true))
                    .orderBy(TableHouse_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public List<TableUser> getAllUsers(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableUser.class)
                    .where(TableUser_Table.synced.eq(synced))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return new ArrayList<>();
    }

    public void updateUser(TableUser user, ItemSurveyDetails surveyDetails) {
        try {
            TableUser oldUser = getUser(user.getTableKey());
            oldUser.setModified(true);
            oldUser.setModifiedDateTime(LUtils.getCurrentDateTime());
            oldUser.setSynced(false);
            oldUser.update();

            user.setTableKey(0);
            user.setPartnerId(surveyDetails.getPartnerId());
            user.setCampName(surveyDetails.getCampName());
            user.setDeviceNumber(surveyDetails.getDeviceNumber());
            user.setDeviceId(getDeviceId());
            user.setEntryDateTime(LUtils.getCurrentDateTime());
            user.setEntryDateTimeMillis(LUtils.getCurrentDateTimeMillis());
            user.setSynced(false);
            user.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public void removeUser(TableUser user) {
        try {
            user.delete();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public void createHouse(TableHouse tableHouse, List<TableDeathInfo> tableDeathInfoList, ItemSurveyDetails surveyDetails) {
        try {
            tableHouse.setCampName(surveyDetails.getCampName());
            tableHouse.setDeviceNumber(surveyDetails.getDeviceNumber());
            tableHouse.setPartnerId(surveyDetails.getPartnerId());

            tableHouse.setDeviceId(getDeviceId());
            tableHouse.setSurveyorUserId(surveyDetails.getSurveyorUserId());
            tableHouse.setSurveyDateTime(LUtils.getCurrentDateTime());
            tableHouse.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            String houseId = surveyDetails.getPartnerId().toUpperCase();

            if (surveyDetails.isDemoUser()) {
                houseId = DEFAULT_PARTNER_ID;
            }

            tableHouse.setHouseId(houseId
                    + surveyDetails.getDeviceNumber()
                    + "H"
                    + getDeviceId()
                    + LUtils.prependZeros("" + getNextHouseNumber(surveyDetails), 6));

            tableHouse.save();

            if (null != tableDeathInfoList) {
                for (TableDeathInfo tableDeathInfo : tableDeathInfoList) {
                    tableDeathInfo.setHouseId(tableHouse.getHouseId());
                    tableDeathInfo.setSurveyorUserId(surveyDetails.getSurveyorUserId());
                    tableDeathInfo.setSurveyDateTime(LUtils.getCurrentDateTime());
                    tableDeathInfo.setSurveyDateTimeMillis((LUtils.getCurrentDateTimeMillis()));

                    tableDeathInfo.setDeviceId(getDeviceId());
                    tableDeathInfo.setCampName(surveyDetails.getCampName());
                    tableDeathInfo.setDeviceNumber(surveyDetails.getDeviceNumber());
                    tableDeathInfo.setPartnerId(surveyDetails.getPartnerId());

                    tableDeathInfo.save();
                }
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public TableHouse getHouse(String houseId) {
        try {
            return SQLite.select()
                    .from(TableHouse.class)
                    .where(TableHouse_Table.house_id.eq(houseId))
                    .and(TableHouse_Table.modified.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public List<TableHouse> getAllHouses(boolean isActiveNeeded, ItemSurveyDetails surveyDetails) {
        try {

            SQLOperator condition;

            if (surveyDetails.isDemoUser()) {
                condition = TableDemographics_Table.house_id.like(DEFAULT_PARTNER_ID + "%");
            } else {
                condition = TableDemographics_Table.house_id.notLike(DEFAULT_PARTNER_ID + "%");
            }

            if (isActiveNeeded) {
                return SQLite.select()
                        .from(TableHouse.class)
                        .where(TableHouse_Table.modified.notEq(true))
                        .and(TableHouse_Table.inactive.notEq(true))
                        .and(condition)
                        .orderBy(TableHouse_Table.table_key, false)
                        .queryList();

            } else {
                return SQLite.select()
                        .from(TableHouse.class)
                        .where(TableHouse_Table.modified.notEq(true))
                        .and(condition)
                        .orderBy(TableHouse_Table.table_key, false)
                        .queryList();
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableHouse> getAllHousesSync(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableHouse.class)
                    .where(TableHouse_Table.synced.eq(synced))
                    .orderBy(TableHouse_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableDeathInfo> getDeathInfoList(String houseId) {
        try {
            return SQLite.select()
                    .from(TableDeathInfo.class)
                    .where(TableDeathInfo_Table.house_id.eq(houseId))
                    .and(TableDeathInfo_Table.modified.notEq(true))
                    .orderBy(TableDeathInfo_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableDeathInfo> getAllDeathInfoList(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableDeathInfo.class)
                    .where(TableDeathInfo_Table.synced.eq(synced))
                    .orderBy(TableDeathInfo_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public void updateHouse(TableHouse tableHouse, List<TableDeathInfo> tableDeathInfoList, ItemSurveyDetails surveyDetails) {
        try {
            TableHouse oldEntry = getHouse(tableHouse.getHouseId());
            oldEntry.setModified(true);
            oldEntry.setModifiedDateTime(LUtils.getCurrentDateTime());
            oldEntry.setSynced(false);
            oldEntry.update();

            tableHouse.setTableKey(0);

            tableHouse.setCampName(surveyDetails.getCampName());
            tableHouse.setDeviceNumber(surveyDetails.getDeviceNumber());
            tableHouse.setPartnerId(surveyDetails.getPartnerId());

            tableHouse.setDeviceId(getDeviceId());
            tableHouse.setSurveyDateTime(LUtils.getCurrentDateTime());
            tableHouse.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());
            tableHouse.setSynced(false);
            tableHouse.save();

            List<TableDeathInfo> tableDeathInfoListOld = getDeathInfoList(tableHouse.getHouseId());
            for (TableDeathInfo tableDeathInfo : tableDeathInfoListOld) {
                tableDeathInfo.setModified(true);
                tableDeathInfo.setModifiedDateTime(LUtils.getCurrentDateTime());
                tableDeathInfo.setSynced(false);
                tableDeathInfo.update();
            }

            if (null != tableDeathInfoList) {
                for (TableDeathInfo tableDeathInfo : tableDeathInfoList) {
                    tableDeathInfo.setTableKey(0);
                    tableDeathInfo.setHouseId(tableHouse.getHouseId());
                    tableDeathInfo.setSurveyorUserId(surveyDetails.getSurveyorUserId());

                    tableDeathInfo.setDeviceId(getDeviceId());
                    tableDeathInfo.setCampName(surveyDetails.getCampName());
                    tableDeathInfo.setDeviceNumber(surveyDetails.getDeviceNumber());
                    tableDeathInfo.setPartnerId(surveyDetails.getPartnerId());

                    tableDeathInfo.setSurveyDateTime(LUtils.getCurrentDateTime());
                    tableDeathInfo.setSurveyDateTimeMillis((LUtils.getCurrentDateTimeMillis()));
                    tableDeathInfo.setSynced(false);

                    if (tableHouse.isInactive()) {
                        tableDeathInfo.setInactive(true);
                        tableDeathInfo.setInactiveReason(tableHouse.getInactiveReason());
                    } else {
                        tableDeathInfo.setInactive(false);
                        tableDeathInfo.setInactiveReason("");
                    }
                    tableDeathInfo.save();
                }
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public String createDemographics(TableDemographics patient, ItemSurveyDetails surveyDetails) {
        try {
            patient.setSurveyorUserId(surveyDetails.getSurveyorUserId());
            patient.setSurveyDateTime(LUtils.getCurrentDateTime());
            patient.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            patient.setDoorToDoor(surveyDetails.isDoorToDoor());
            patient.setDeviceId(getDeviceId());
            patient.setCampName(surveyDetails.getCampName());
            patient.setDeviceNumber(surveyDetails.getDeviceNumber());
            patient.setPartnerId(surveyDetails.getPartnerId());

            String registrationId = surveyDetails.getPartnerId().toUpperCase();

            if (surveyDetails.isDemoUser()) {
                registrationId = DEFAULT_PARTNER_ID;
            }

            patient.setRegistrationId(registrationId
                    + surveyDetails.getDeviceNumber()
                    + getDeviceId()
                    + LUtils.prependZeros("" + getNextPatientNumber(surveyDetails),
                    6));

            patient.save();

            return patient.getRegistrationId();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    private int getNextPatientNumber(ItemSurveyDetails surveyDetails) {
        int maxId = 0;

        List<TableDemographics> tableDemographicsList = getAllPatients(surveyDetails);

        try {
            for (int i = 0; i < tableDemographicsList.size(); ++i) {

                String registrationId = tableDemographicsList.get(i).getRegistrationId();

                String regIdPrefixDemo = DEFAULT_PARTNER_ID + surveyDetails.getDeviceNumber() + getDeviceId();
                String regIdPrefix     = surveyDetails.getPartnerId() + surveyDetails.getDeviceNumber() + getDeviceId();

                if (surveyDetails.isDemoUser()) {
                    if (registrationId.startsWith(regIdPrefixDemo)) {
                        int number = getRegistrationNumber(registrationId);

                        if (number > maxId) {
                            maxId = number;
                        }
                    }
                } else {
                    if (registrationId.startsWith(regIdPrefix)) {
                        int number = getRegistrationNumber(registrationId);

                        if (number > maxId) {
                            maxId = number;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return maxId + 1;
    }

    private int getNextHouseNumber(ItemSurveyDetails surveyDetails) {
        int maxId = 0;

        List<TableHouse> tableHouseList = getAllHouses(false, surveyDetails);

        try {
            for (int i = 0; i < tableHouseList.size(); ++i) {

                String houseId = tableHouseList.get(0).getHouseId();

                String regIdPrefixDemo = DEFAULT_PARTNER_ID + surveyDetails.getDeviceNumber() + "H" + getDeviceId();
                String regIdPrefix     = surveyDetails.getPartnerId() + surveyDetails.getDeviceNumber() + "H" + getDeviceId();

                if (surveyDetails.isDemoUser()) {
                    if (houseId.startsWith(regIdPrefixDemo)) {
                        int number = getRegistrationNumber(houseId);

                        if (number > maxId) {
                            maxId = number;
                        }
                    }

                } else {
                    if (houseId.startsWith(regIdPrefix)) {
                        int number = getRegistrationNumber(houseId);

                        if (number > maxId) {
                            maxId = number;
                        }
                    }
                }
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return maxId + 1;
    }

    private int getRegistrationNumber(String regId) {
        String text = regId.substring(regId.length() - 6);
        return Integer.parseInt(text);
    }

    public TableDemographics getPatient(String registrationId) {
        try {
            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.registration_id.eq(registrationId))
                    .and(TableDemographics_Table.modified.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public TableDemographics getPatientByIdentity(String identityNumber) {
        try {
            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.identity_number.eq(identityNumber))
                    .and(TableDemographics_Table.modified.notEq(true))
                    .and(TableDemographics_Table.inactive.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public TableDemographics searchPatient(String text) {
        try {
            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.registration_id.eq(text))
                    .or(TableDemographics_Table.identity_number.eq(text))
                    .or(TableDemographics_Table.phone_number.eq(text))
                    .and(TableDemographics_Table.modified.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public List<TableDemographics> getAllPatients(ItemSurveyDetails surveyDetails) {
        try {
            SQLOperator condition;

            if (surveyDetails.isDemoUser()) {
                condition = TableDemographics_Table.registration_id.like(DEFAULT_PARTNER_ID + "%");
            } else {
                condition = TableDemographics_Table.registration_id.notLike(DEFAULT_PARTNER_ID + "%");
            }

            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.modified.notEq(true))
                    .and(condition)
                    .orderBy(TableDemographics_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableDemographics> getAllPatients(ItemSurveyDetails surveyDetails, String surveyorId) {
        try {

            SQLOperator condition;

            if (surveyDetails.isDemoUser()) {
                condition = TableDemographics_Table.registration_id.like(DEFAULT_PARTNER_ID + "%");
            } else {
                condition = TableDemographics_Table.registration_id.notLike(DEFAULT_PARTNER_ID + "%");
            }

            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.modified.notEq(true))
                    .and(TableDemographics_Table.surveyor_user_id.eq(surveyorId))
                    .and(condition)
                    .orderBy(TableDemographics_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableDemographics> getAllActivePatientsForUser(String surveyorId) {
        try {
            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.modified.notEq(true))
                    .and(TableDemographics_Table.inactive.notEq(true))
                    .and(TableDemographics_Table.surveyor_user_id.eq(surveyorId))
                    .orderBy(TableDemographics_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableDemographics> getAllPatients(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.synced.eq(synced))
                    .orderBy(TableDemographics_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    private List<TableDemographics> getAllActivePatients(String gender) {
        try {
            if (gender.equals("")) {
                return SQLite.select()
                        .from(TableDemographics.class)
                        .where(TableDemographics_Table.modified.notEq(true))
                        .and(TableDemographics_Table.inactive.notEq(true))
                        .orderBy(TableDemographics_Table.table_key, false)
                        .queryList();
            } else {
                return SQLite.select()
                        .from(TableDemographics.class)
                        .where(TableDemographics_Table.gender.eq(gender))
                        .and(TableDemographics_Table.modified.notEq(true))
                        .and(TableDemographics_Table.inactive.notEq(true))
                        .orderBy(TableDemographics_Table.table_key, false)
                        .queryList();
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    private List<TableDemographics> getAllActivePatients() {
        return SQLite.select()
                .from(TableDemographics.class)
                .where(TableDemographics_Table.modified.notEq(true))
                .and(TableDemographics_Table.inactive.notEq(true))
                .orderBy(TableDemographics_Table.table_key, false)
                .queryList();
    }

    public void updatePatient(TableDemographics patient, ItemSurveyDetails surveyDetails) {
        try {
            TableDemographics oldEntry = getPatient(patient.getRegistrationId());
            oldEntry.setModified(true);
            oldEntry.setModifiedDateTime(LUtils.getCurrentDateTime());
            oldEntry.setSynced(false);
            oldEntry.update();

            patient.setTableKey(0);

            patient.setSurveyorUserId(surveyDetails.getSurveyorUserId());

            patient.setCampName(surveyDetails.getCampName());
            patient.setDeviceNumber(surveyDetails.getDeviceNumber());
            patient.setDeviceId(getDeviceId());
            patient.setPartnerId(surveyDetails.getPartnerId());

            patient.setSurveyDateTime(LUtils.getCurrentDateTime());
            patient.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());
            patient.setSynced(false);
            patient.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public List<TableDemographics> searchPatients(String queryString) {
        try {
            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.full_name.like(queryString + "%"))
                    .and(TableDemographics_Table.modified.notEq(true))
                    .or(TableDemographics_Table.phone_number.like(queryString + "%"))
                    .and(TableDemographics_Table.modified.notEq(true))
                    .or(TableDemographics_Table.registration_id.like(queryString + "%"))
                    .and(TableDemographics_Table.modified.notEq(true))
                    .orderBy(TableDemographics_Table.full_name, true)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public void createSociodemographics(TableSociodemographics sociodemographics, String registrationId, ItemSurveyDetails surveyDetails) {
        try {
            sociodemographics.setRegistrationId(registrationId);
            sociodemographics.setSurveyorUserId(surveyDetails.getSurveyorUserId());

            sociodemographics.setCampName(surveyDetails.getCampName());
            sociodemographics.setDeviceNumber(surveyDetails.getDeviceNumber());
            sociodemographics.setDeviceId(getDeviceId());
            sociodemographics.setPartnerId(surveyDetails.getPartnerId());

            sociodemographics.setSurveyDateTime(LUtils.getCurrentDateTime());
            sociodemographics.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            sociodemographics.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public TableSociodemographics getSociodemographics(String registrationId) {
        try {
            return SQLite.select()
                    .from(TableSociodemographics.class)
                    .where(TableSociodemographics_Table.registration_id.eq(registrationId))
                    .and(TableSociodemographics_Table.modified.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public List<TableSociodemographics> getAllSociodemographics(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableSociodemographics.class)
                    .where(TableSociodemographics_Table.synced.eq(synced))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public void updateSociodemographics(TableSociodemographics sociodemographics, ItemSurveyDetails surveyDetails) {
        try {
            TableSociodemographics oldEntry = getSociodemographics(sociodemographics.getRegistrationId());
            oldEntry.setModified(true);
            oldEntry.setModifiedDateTime(LUtils.getCurrentDateTime());
            oldEntry.setSynced(false);
            oldEntry.update();

            sociodemographics.setTableKey(0);
            sociodemographics.setSurveyorUserId(surveyDetails.getSurveyorUserId());

            sociodemographics.setCampName(surveyDetails.getCampName());
            sociodemographics.setDeviceNumber(surveyDetails.getDeviceNumber());
            sociodemographics.setDeviceId(getDeviceId());
            sociodemographics.setPartnerId(surveyDetails.getPartnerId());

            sociodemographics.setSurveyDateTime(LUtils.getCurrentDateTime());
            sociodemographics.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            sociodemographics.setSynced(false);
            sociodemographics.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public void addOrUpdateQuestion(String registrationId, ItemSurveyDetails surveyDetails, TableProblems question) {

        try {
            TableProblems oldQuestion = getQuestion(registrationId, question.getQuestionGroup(), question.getQuestion());

            if (null != oldQuestion) {
                if (oldQuestion.getAnswer().equals(question.getAnswer())) {
                    return;
                }

                oldQuestion.setModified(true);
                oldQuestion.setModifiedDateTime(LUtils.getCurrentDateTime());

                oldQuestion.setSynced(false);
                oldQuestion.update();
            }

            addQuestion(registrationId, surveyDetails, question);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    private void addQuestion(String registrationId, ItemSurveyDetails surveyDetails, TableProblems question) {

        try {
            question.setRegistrationId(registrationId);
            question.setSurveyorUserId(surveyDetails.getSurveyorUserId());

            question.setCampName(surveyDetails.getCampName());
            question.setDeviceId(getDeviceId());
            question.setDeviceNumber(surveyDetails.getDeviceNumber());
            question.setPartnerId(surveyDetails.getPartnerId());

            question.setSurveyDateTime(LUtils.getCurrentDateTime());
            question.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            question.setSynced(false);
            question.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public TableProblems getQuestion(String registrationId, String questionGroup, String question) {
        try {
            return SQLite.select()
                    .from(TableProblems.class)
                    .where(TableProblems_Table.registration_id.eq(registrationId))
                    .and(TableProblems_Table.question_group.eq(questionGroup))
                    .and(TableProblems_Table.question.eq(question))
                    .and(TableProblems_Table.modified.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public List<TableProblems> getAllQuestions(String registrationId) {
        try {
            return SQLite.select()
                    .from(TableProblems.class)
                    .where(TableProblems_Table.registration_id.eq(registrationId))
                    .and(TableProblems_Table.modified.notEq(true))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableProblems> getAllQuestions(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableProblems.class)
                    .where(TableProblems_Table.synced.eq(synced))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public void addVital(TableVitals vitals, ItemSurveyDetails surveyDetails, String deviceId) {
        try {
            vitals.setSurveyorUserId(surveyDetails.getSurveyorUserId());

            vitals.setCampName(surveyDetails.getCampName());
            vitals.setDeviceId(deviceId);
            vitals.setDeviceNumber(surveyDetails.getDeviceNumber());
            vitals.setPartnerId(surveyDetails.getPartnerId());

            vitals.setSurveyDateTime(LUtils.getCurrentDateTime());
            vitals.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            vitals.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public TableVitals getVital(int tableKey) {
        try {
            return SQLite.select()
                    .from(TableVitals.class)
                    .where(TableVitals_Table.table_key.eq(tableKey))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TableVitals();
    }

    public List<TableVitals> getAllVitals(String registrationId, boolean showInactive) {
        try {
            if (registrationId.equals("")) {
                if (showInactive) {
                    return SQLite.select().
                            from(TableVitals.class)
                            .where(TableVitals_Table.modified.notEq(true))
                            .orderBy(TableVitals_Table.vital_date_time_millis, false)
                            .queryList();
                } else {
                    return SQLite.select().
                            from(TableVitals.class)
                            .where(TableVitals_Table.modified.notEq(true))
                            .and(TableVitals_Table.inactive.notEq(true))
                            .orderBy(TableVitals_Table.vital_date_time_millis, false)
                            .queryList();
                }
            } else {

//                String aadhaar = "";
//                try {
//                    aadhaar = getPatient(registrationId).getIdentityNumber();
//                } catch (Exception e) {
//                    LLog.printStackTrace(e);
//                }

                if (showInactive) {
                    return SQLite.select()
                            .from(TableVitals.class)
                            .where(TableVitals_Table.registration_id.eq(registrationId))
                            .and(TableVitals_Table.modified.notEq(true))
                            .or(TableVitals_Table.registration_id.eq(registrationId))
                            .and(TableVitals_Table.modified.notEq(true))
                            .orderBy(TableVitals_Table.vital_date_time_millis, false)
                            .queryList();
                } else {
                    return SQLite.select()
                            .from(TableVitals.class)
                            .where(TableVitals_Table.registration_id.eq(registrationId))
                            .and(TableVitals_Table.modified.notEq(true))
                            .and(TableVitals_Table.inactive.notEq(true))
                            .or(TableVitals_Table.registration_id.eq(registrationId))
                            .and(TableVitals_Table.modified.notEq(true))
                            .and(TableVitals_Table.inactive.notEq(true))
                            .orderBy(TableVitals_Table.vital_date_time_millis, false)
                            .queryList();
                }
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableVitals> getAllVitals(ItemSurveyDetails surveyDetails) {
        try {

            SQLOperator condition;

            if (surveyDetails.isDemoUser()) {
                condition = TableVitals_Table.registration_id.like(DEFAULT_PARTNER_ID + "%");
            } else {
                condition = TableVitals_Table.registration_id.notLike(DEFAULT_PARTNER_ID + "%");
            }

            return SQLite.select().
                    from(TableVitals.class)
                    .where(TableVitals_Table.modified.notEq(true))
                    .and(TableVitals_Table.inactive.notEq(true))
                    .and(condition)
                    .orderBy(TableVitals_Table.vital_date_time_millis, false)
                    .queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    private List<TableVitals> getAllVitalsForSurveyor(String surveyorId) {
        try {
            return SQLite.select().
                    from(TableVitals.class)
                    .where(TableVitals_Table.modified.notEq(true))
                    .and(TableVitals_Table.inactive.notEq(true))
                    .and(TableVitals_Table.surveyor_user_id.eq(surveyorId))
                    .orderBy(TableVitals_Table.vital_date_time_millis, false)
                    .queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableVitals> getAllVitals(boolean synced) {
        try {
            return SQLite.select().
                    from(TableVitals.class)
                    .where(TableVitals_Table.synced.eq(synced))
                    .orderBy(TableVitals_Table.vital_date_time_millis, false)
                    .queryList();
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    private List<TableVitals> getVitalStatistics(ItemSurveyDetails surveyDetails, String vitalType) {
        try {

            SQLOperator condition;

            if (surveyDetails.isDemoUser()) {
                condition = TableVitals_Table.registration_id.like(DEFAULT_PARTNER_ID + "%");
            } else {
                condition = TableVitals_Table.registration_id.notLike(DEFAULT_PARTNER_ID + "%");
            }

            return SQLite.select().
                    from(TableVitals.class)
                    .where(TableVitals_Table.vital_type.eq(vitalType))
                    .and(TableVitals_Table.modified.notEq(true))
                    .and(TableVitals_Table.inactive.notEq(true))
                    .and(condition)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<ItemVitalHeader> getAllVitalHeaders(String registrationId) {
        try {
            List<TableVitals> vitalsList;

            if (registrationId.equals("")) {
                vitalsList = SQLite.select().
                        from(TableVitals.class)
                        .where(TableVitals_Table.modified.notEq(true))
                        .orderBy(TableVitals_Table.vital_date_time_millis, false)
                        .queryList();
            } else {
//                String identityNumber = "";
//                try {
//                    identityNumber = getPatient(registrationId).getIdentityNumber();
//                } catch (Exception e) {
//                    LLog.printStackTrace(e);
//                }

                vitalsList = SQLite.select().
                        from(TableVitals.class)
                        .where(TableVitals_Table.registration_id.eq(registrationId))
                        .and(TableVitals_Table.modified.notEq(true))
                        .or(TableVitals_Table.registration_id.eq(registrationId))
                        .and(TableVitals_Table.modified.notEq(true))
                        .orderBy(TableVitals_Table.vital_date_time_millis, false)
                        .queryList();
            }

            List<ItemVitalHeader> vitalWithDates = new ArrayList<>();

            boolean firstTime = true;

            while (!vitalsList.isEmpty()) {
                TableVitals vital = vitalsList.get(0);

                ItemVitalHeader itemVitalHeader = new ItemVitalHeader();

                String currentDate = vital.getVitalDateTime().split(" ")[0];

                itemVitalHeader.setDate(currentDate);

                if (firstTime) {
                    itemVitalHeader.setVitalsShown(true);
                    firstTime = false;
                }

                List<TableVitals> tableVitalsList = new ArrayList<>();
                tableVitalsList.add(vitalsList.get(0));

                List<Integer> removeIndexes = new ArrayList<>();

                for (int i = 1; i < vitalsList.size(); i++) {
                    String vitalDate = vitalsList.get(i).getVitalDateTime().split(" ")[0];

                    if (currentDate.equals(vitalDate)) {
                        tableVitalsList.add(vitalsList.get(i));
                        removeIndexes.add(i);
                    }
                }

                int totalRemoved = 0;

                for (Integer removeIndex : removeIndexes) {
                    vitalsList.remove((removeIndex) - totalRemoved);
                    ++totalRemoved;
                }

                vitalsList.remove(0);

                itemVitalHeader.setVitals(tableVitalsList);

                vitalWithDates.add(itemVitalHeader);
            }

            return vitalWithDates;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public void updateVital(TableVitals vitals, ItemSurveyDetails surveyDetails, String deviceId) {
        try {
            TableVitals oldVital = getVital(vitals.getTableKey());
            oldVital.setModified(true);
            oldVital.setModifiedDateTime(LUtils.getCurrentDateTime());

            oldVital.setSynced(false);
            oldVital.update();

            vitals.setTableKey(0);
            vitals.setSurveyorUserId(surveyDetails.getSurveyorUserId());
            vitals.setSurveyDateTime(LUtils.getCurrentDateTime());
            vitals.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            vitals.setDeviceId(deviceId);
            vitals.setCampName(surveyDetails.getCampName());
            vitals.setDeviceNumber(surveyDetails.getDeviceNumber());
            vitals.setPartnerId(surveyDetails.getPartnerId());

            vitals.setSynced(false);
            vitals.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public boolean isInactive(String registrationId, int type) {
        switch (type) {
            case PATIENT_INFO: {
                TableDemographics patient = getPatient(registrationId);

                if (patient.isInactive()) {
                    return true;
                }
            }
            break;

            case SOCIODEMOGRAPHICS: {
                TableSociodemographics sociodemographics = getSociodemographics(registrationId);

                if (sociodemographics.isInactive()) {
                    return true;
                }
            }
            break;
        }

        return false;
    }

    public String getLatestHeight(String registrationId, long timeMillis) {
        try {
            List<TableVitals> tableVitals = SQLite.select()
                    .from(TableVitals.class)
                    .where(TableVitals_Table.registration_id.eq(registrationId))
                    .and(TableVitals_Table.vital_type.eq("Height"))
                    .and(TableVitals_Table.inactive.notEq(true))
                    .and(TableVitals_Table.modified.notEq(true))
                    .orderBy(TableVitals_Table.table_key, false)
                    .queryList();

            if (0 == tableVitals.size()) {

                try {
//                    String aadhaar = getPatient(registrationId).getIdentityNumber();

                    tableVitals = SQLite.select()
                            .from(TableVitals.class)
                            .where(TableVitals_Table.registration_id.eq(registrationId))
                            .and(TableVitals_Table.vital_type.eq("Height"))
                            .and(TableVitals_Table.inactive.notEq(true))
                            .and(TableVitals_Table.modified.notEq(true))
                            .orderBy(TableVitals_Table.table_key, false)
                            .queryList();
                } catch (Exception e) {

                }
            }

            if (0 == tableVitals.size()) {

                try {
                    registrationId = getPatientByIdentity(registrationId).getRegistrationId();

                    tableVitals = SQLite.select()
                            .from(TableVitals.class)
                            .where(TableVitals_Table.registration_id.eq(registrationId))
                            .and(TableVitals_Table.vital_type.eq("Height"))
                            .and(TableVitals_Table.inactive.notEq(true))
                            .and(TableVitals_Table.modified.notEq(true))
                            .orderBy(TableVitals_Table.table_key, false)
                            .queryList();
                } catch (Exception e) {

                }
            }

            int  closestId              = 0;
            long smallestTimeDifference = Math.abs(tableVitals.get(0).getVitalDateTimeMillis() - timeMillis);

            for (int i = 1; i < tableVitals.size(); ++i) {
                if (Math.abs(tableVitals.get(i).getVitalDateTimeMillis() - timeMillis) < smallestTimeDifference) {
                    closestId = i;
                }
            }

            return tableVitals.get(closestId).getVitalValues() + ";" + tableVitals.get(closestId).getVitalUnits();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public String getHeightOnDate(String registrationId, String date) {
        try {
            List<TableVitals> tableVitals = SQLite.select()
                    .from(TableVitals.class)
                    .where(TableVitals_Table.registration_id.eq(registrationId))
                    .and(TableVitals_Table.vital_type.eq("Height"))
                    .and(TableVitals_Table.inactive.notEq(true))
                    .and(TableVitals_Table.modified.notEq(true))
                    .orderBy(TableVitals_Table.table_key, false)
                    .queryList();

            for (TableVitals tableVital : tableVitals) {
                if (tableVital.getVitalDateTime().startsWith(date)) {
                    return tableVital.getVitalValues() + ";" + tableVital.getVitalUnits();
                }
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public boolean isIdentityExists(TableDemographics tableDemographics) {

        if (null == tableDemographics.getIdentityType()
                || tableDemographics.getIdentityType().equals("")
                || tableDemographics.getIdentityType().equals("None")) {
            return false;
        }

        try {
            TableDemographics patient = SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.identity_type.eq(tableDemographics.getIdentityType()))
                    .and(TableDemographics_Table.identity_number.eq(tableDemographics.getIdentityNumber()))
                    .querySingle();

            if (null != patient) {
                if (tableDemographics.getIdentityType().equals("Aadhaar")) {
                    if (AssetReader.toShow(KEY_ACCEPT_AADHAAR_NUMBER)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return true;
                }
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public boolean isSimilarDemographicExists(TableDemographics tableDemographics, ItemSurveyDetails surveyDetails) {
        try {

            if (surveyDetails.isDemoUser()) {
                TableDemographics patient = SQLite.select()
                        .from(TableDemographics.class)
                        .where(TableDemographics_Table.full_name.eq(tableDemographics.getFullName()))
                        .and(TableDemographics_Table.gender.eq(tableDemographics.getGender()))
                        .and(TableDemographics_Table.yob.eq(tableDemographics.getYob()))
                        .querySingle();

                if (null != patient) {
                    return true;
                }
            } else {
                TableDemographics patient = SQLite.select()
                        .from(TableDemographics.class)
                        .where(TableDemographics_Table.full_name.eq(tableDemographics.getFullName()))
                        .and(TableDemographics_Table.registration_id.notLike(DEFAULT_PARTNER_ID + "%"))
                        .and(TableDemographics_Table.gender.eq(tableDemographics.getGender()))
                        .and(TableDemographics_Table.yob.eq(tableDemographics.getYob()))
                        .querySingle();

                if (null != patient) {
                    return true;
                }
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public ItemSummary getSurveySummary(ItemSurveyDetails surveyDetails) {
        ItemSummary itemSummary = new ItemSummary();

        List<TableDemographics> patientsList = getAllActivePatients();

        int totalCount  = 0;
        int maleCount   = 0;
        int femaleCount = 0;
        int otherCount  = 0;

        for (TableDemographics patient : patientsList) {
            boolean isDemoPatient = false;

            if (patient.getRegistrationId().startsWith(DEFAULT_PARTNER_ID)) {
                isDemoPatient = true;
            }

            if ((surveyDetails.isDemoUser() && isDemoPatient)
                    || (!surveyDetails.isDemoUser() && !isDemoPatient)) {
                ++totalCount;

                if (patient.getGender().equals("Male")) {
                    ++maleCount;

                } else if (patient.getGender().equals("Female")) {
                    ++femaleCount;

                } else {
                    ++otherCount;
                }
            }
        }

//        itemSummary.setTotalDemographics(getAllActivePatients("").size());
//        itemSummary.setMale(getAllActivePatients("Male").size());
//        itemSummary.setFemale(getAllActivePatients("Female").size());
//        itemSummary.setOthers(getAllActivePatients("Other").size());

        itemSummary.setTotalDemographics(totalCount);
        itemSummary.setMale(maleCount);
        itemSummary.setFemale(femaleCount);
        itemSummary.setOthers(otherCount);

        itemSummary.setTotalVitals(getAllVitals(surveyDetails).size());
        itemSummary.setHeight(getVitalStatistics(surveyDetails, "Height").size());
        itemSummary.setWeight(getVitalStatistics(surveyDetails, "Weight").size());
        itemSummary.setBp(getVitalStatistics(surveyDetails, "Blood Pressure").size());
        itemSummary.setRbs(getVitalStatistics(surveyDetails, "Blood Sugar").size());
        itemSummary.setHemoglobin(getVitalStatistics(surveyDetails, "Hemoglobin").size());
        itemSummary.setAlbumin(getVitalStatistics(surveyDetails, "Albumin").size());
        itemSummary.setBloodGroup(getVitalStatistics(surveyDetails, "Blood Group").size());
        itemSummary.setPrescription(getVitalStatistics(surveyDetails, "Prescription").size());

        return itemSummary;
    }

    public boolean checkVitalEnteredAlready(String registrationId, String vitalSubtype) {
        try {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();

            TableVitals tableVitals = SQLite.select()
                    .from(TableVitals.class)
                    .where(TableVitals_Table.registration_id.eq(registrationId))
                    .and(TableVitals_Table.vital_subtypes.eq(vitalSubtype))
                    .and(TableVitals_Table.inactive.notEq(true))
                    .and(TableVitals_Table.modified.notEq(true))
                    .orderBy(TableHouse_Table.table_key, false)
                    .querySingle();

            if (tableVitals != null) {
                cal2.setTimeInMillis(tableVitals.getVitalDateTimeMillis());

                return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                        cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public String getPartnerId() {

        TableCampInfo campInfo = getCampInfo();

        if (null != campInfo && !campInfo.getPartnerId().isEmpty()) {
            return getCampInfo().getPartnerId();
        }

        return DEFAULT_PARTNER_ID;
    }

    public ArrayList<String> getMissingDemographics(ItemSurveyDetails surveyDetails) {

        List<TableVitals> vitals = getAllVitals(surveyDetails);

        ArrayList<String> missingDemographics = new ArrayList<>();

        for (TableVitals vital : vitals) {

            TableDemographics demographics = null;

            try {
                demographics = SQLite.select().
                        from(TableDemographics.class)
                        .where(TableDemographics_Table.registration_id.eq(vital.getRegistrationId()))
                        .and(TableDemographics_Table.modified.notEq(true))
                        .or(TableDemographics_Table.identity_number.eq(vital.getRegistrationId()))
                        .and(TableDemographics_Table.modified.notEq(true))
                        .querySingle();

            } catch (Exception e) {
                LLog.printStackTrace(e);
            }

            if (null == demographics && !missingDemographics.contains(vital.getRegistrationId())) {
                missingDemographics.add(vital.getRegistrationId());
            }
        }

        return missingDemographics;
    }

    public void merge(TableUser tableUser) {
        try {
            TableUser user = SQLite.select()
                    .from(TableUser.class)
                    .where(TableUser_Table.user_id.eq(tableUser.getUserId()))
                    .querySingle();

            if (null == user) {
                tableUser.save();

            } else if (!tableUser.getModifiedDateTime().equals(user.getModifiedDateTime())) {
                user.setModifiedDateTime(tableUser.getModifiedDateTime());
                user.update();
            }

        } catch (Exception e) {
            tableUser.save();
        }
    }

    public void merge(TableKitConfig tableKitConfig) {
        try {
            TableKitConfig table = SQLite.select()
                    .from(TableKitConfig.class)
                    .where(TableKitConfig_Table.mac_address.eq(tableKitConfig.getMacAddress()))
                    .and(TableKitConfig_Table.entry_date_time_millis.eq(tableKitConfig.getEntryDateTimeMillis()))
                    .querySingle();

            if (null == table) {
                tableKitConfig.save();

            } else if (!tableKitConfig.getModifiedDateTime().equals(table.getModifiedDateTime())) {
                table.setModifiedDateTime(tableKitConfig.getModifiedDateTime());
                table.update();
            }

        } catch (Exception e) {
            tableKitConfig.save();
        }
    }

    public void merge(TableDemographics tableDemographics) {
        try {
            TableDemographics demographics = SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.registration_id.eq(tableDemographics.getRegistrationId()))
                    .and(TableDemographics_Table.survey_date_time_millis.eq(tableDemographics.getSurveyDateTimeMillis()))
                    .querySingle();

            if (null == demographics) {
                tableDemographics.save();

            } else if (!tableDemographics.getModifiedDateTime().equals(demographics.getModifiedDateTime())) {
                demographics.setModifiedDateTime(tableDemographics.getModifiedDateTime());
                demographics.update();
            }

        } catch (Exception e) {
            tableDemographics.save();
        }
    }

    public void merge(TableSociodemographics tableSociodemographics) {
        try {
            TableSociodemographics sociodemographics = SQLite.select()
                    .from(TableSociodemographics.class)
                    .where(TableSociodemographics_Table.registration_id.eq(tableSociodemographics.getRegistrationId()))
                    .and(TableSociodemographics_Table.survey_date_time_millis.eq(tableSociodemographics.getSurveyDateTimeMillis()))
                    .querySingle();

            if (null == sociodemographics) {
                tableSociodemographics.save();

            } else if (!tableSociodemographics.getModifiedDateTime().equals(sociodemographics.getModifiedDateTime())) {
                sociodemographics.setModifiedDateTime(tableSociodemographics.getModifiedDateTime());
                sociodemographics.update();
            }

        } catch (Exception e) {
            tableSociodemographics.save();
        }
    }

    public void merge(TableProblems tableProblems) {
        try {
            TableProblems problems = SQLite.select()
                    .from(TableProblems.class)
                    .where(TableProblems_Table.registration_id.eq(tableProblems.getRegistrationId()))
                    .and(TableProblems_Table.question.eq(tableProblems.getQuestion()))
                    .and(TableProblems_Table.question_group.eq(tableProblems.getQuestionGroup()))
                    .and(TableProblems_Table.survey_date_time_millis.eq(tableProblems.getSurveyDateTimeMillis()))
                    .querySingle();

            if (null == problems) {
                tableProblems.save();

            } else if (!tableProblems.getModifiedDateTime().equals(problems.getModifiedDateTime())) {
                problems.setModifiedDateTime(tableProblems.getModifiedDateTime());
                problems.update();
            }

        } catch (Exception e) {
            tableProblems.save();
        }
    }

    public void merge(TableImage tableImage) {
        try {
            TableProblems problems = SQLite.select()
                    .from(TableProblems.class)
                    .where(TableProblems_Table.registration_id.eq(tableImage.getRegistrationId()))
                    .and(TableProblems_Table.survey_date_time_millis.eq(tableImage.getSurveyDateTimeMillis()))
                    .querySingle();

            if (null == problems) {
                tableImage.save();

            } else if (!tableImage.getModifiedDateTime().equals(problems.getModifiedDateTime())) {
                problems.setModifiedDateTime(tableImage.getModifiedDateTime());
                problems.update();
            }

        } catch (Exception e) {
            tableImage.save();
        }
    }

    public void merge(TableHouse tableHouse) {
        try {
            TableHouse house = SQLite.select()
                    .from(TableHouse.class)
                    .where(TableHouse_Table.house_id.eq(tableHouse.getHouseId()))
                    .and(TableHouse_Table.survey_date_time_millis.eq(tableHouse.getSurveyDateTimeMillis()))
                    .querySingle();

            if (null == house) {
                tableHouse.save();

            } else if (!tableHouse.getModifiedDateTime().equals(house.getModifiedDateTime())) {
                house.setModifiedDateTime(tableHouse.getModifiedDateTime());
                house.update();
            }

        } catch (Exception e) {
            tableHouse.save();
        }
    }

    public void merge(TableDeathInfo tableDeathInfo) {
        try {
            TableDeathInfo deathInfo = SQLite.select()
                    .from(TableDeathInfo.class)
                    .where(TableDeathInfo_Table.house_id.eq(tableDeathInfo.getHouseId()))
                    .and(TableDeathInfo_Table.survey_date_time_millis.eq(tableDeathInfo.getSurveyDateTimeMillis()))
                    .querySingle();

            if (null == deathInfo) {
                tableDeathInfo.save();

            } else if (!tableDeathInfo.getModifiedDateTime().equals(deathInfo.getModifiedDateTime())) {
                deathInfo.setModifiedDateTime(tableDeathInfo.getModifiedDateTime());
                deathInfo.update();
            }

        } catch (Exception e) {
            tableDeathInfo.save();
        }
    }

    public void merge(TableVitals tableVitals) {
        try {
            TableVitals vitals = SQLite.select()
                    .from(TableVitals.class)
                    .where(TableVitals_Table.registration_id.eq(tableVitals.getRegistrationId()))
                    .and(TableVitals_Table.vital_type.eq(tableVitals.getVitalType()))
                    .and(TableVitals_Table.survey_date_time_millis.eq(tableVitals.getSurveyDateTimeMillis()))
                    .querySingle();

            if (null == vitals) {
                tableVitals.save();

            } else if (!tableVitals.getModifiedDateTime().equals(vitals.getModifiedDateTime())) {
                vitals.setModifiedDateTime(tableVitals.getModifiedDateTime());
                vitals.update();
            }

        } catch (Exception e) {
            tableVitals.save();
        }
    }

    private String getDeviceId() {
        return Settings.Secure.getString(LApplication.getContext().getContentResolver(),
                Settings.Secure.ANDROID_ID).toUpperCase();
    }

    public boolean toShowPrescription() {
//        try {
//            List<TableUser> users = SQLite.select()
//                    .from(TableUser.class)
//                    .where(TableUser_Table.modified.notEq(true))
//                    .and(TableUser_Table.allow_prescription.eq(true))
//                    .queryList();
//
//            if (0 < users.size()) {
//                return true;
//            }
//
//        } catch (Exception e) {
//            LLog.printStackTrace(e);
//        }

        // Allow viewing for all
        return true;
    }

    public ArrayList<ItemUserSurveySummary> getUsersSummary(ItemSurveyDetails surveyDetails, long startDateMillis, long endDateMillis) {
        ArrayList<ItemUserSurveySummary> summaries = new ArrayList<>();

        List<TableUser> users = getAllUsers();

        for (TableUser user : users) {
            ItemUserSurveySummary summary = new ItemUserSurveySummary();
            summary.setUserName(user.getFullName());

            int demographicsCount = 0;

            List<TableDemographics> demographicsList = getAllPatients(surveyDetails, user.getUserId());

            for (TableDemographics demographics : demographicsList) {
                if (!(LUtils.getLongTime(demographics.getSurveyDateTime()) >= startDateMillis
                        && LUtils.getLongTime(demographics.getSurveyDateTime()) <= endDateMillis)) {
                    continue;
                }
                ++demographicsCount;
            }
            summary.setDemographics(demographicsCount);

            List<TableVitals> vitals = getAllVitalsForSurveyor(user.getUserId());

            for (TableVitals vital : vitals) {
                if (!(LUtils.getLongTime(vital.getVitalDateTime()) >= startDateMillis
                        && LUtils.getLongTime(vital.getVitalDateTime()) <= endDateMillis)) {
                    continue;
                }

                switch (vital.getVitalType()) {
                    case "Height":
                        summary.setHeight(summary.getHeight() + 1);
                        break;

                    case "Weight":
                        summary.setWeight(summary.getWeight() + 1);
                        break;

                    case "Blood Pressure":
                        summary.setBp(summary.getBp() + 1);
                        break;

                    case "Blood Sugar":
                        summary.setSugar(summary.getSugar() + 1);
                        break;

                    case "Hemoglobin":
                        summary.setHb(summary.getHb() + 1);
                        break;

                    case "Prescription":
                        summary.setPrescription(summary.getPrescription() + 1);
                        break;
                }
            }

            summaries.add(summary);
        }

        return summaries;
    }

    public ItemUserSurveySummary getUserSummary(long startDateMillis, long endDateMillis, String userId) {
        ItemUserSurveySummary summary = new ItemUserSurveySummary();

        int demographicsCount = 0;

        List<TableDemographics> demographicsList = getAllActivePatientsForUser(userId);

        for (TableDemographics demographics : demographicsList) {
            if (!(LUtils.getLongTime(demographics.getSurveyDateTime()) >= startDateMillis
                    && LUtils.getLongTime(demographics.getSurveyDateTime()) <= endDateMillis)) {
                continue;
            }
            ++demographicsCount;
        }
        summary.setDemographics(demographicsCount);

        List<TableVitals> vitals = getAllVitalsForSurveyor(userId);

        for (TableVitals vital : vitals) {
            if (!(LUtils.getLongTime(vital.getVitalDateTime()) >= startDateMillis
                    && LUtils.getLongTime(vital.getVitalDateTime()) <= endDateMillis)) {
                continue;
            }

            switch (vital.getVitalType()) {
                case "Height":
                    summary.setHeight(summary.getHeight() + 1);
                    break;

                case "Weight":
                    summary.setWeight(summary.getWeight() + 1);
                    break;

                case "Blood Pressure":
                    summary.setBp(summary.getBp() + 1);
                    break;

                case "Blood Sugar":
                    summary.setSugar(summary.getSugar() + 1);
                    break;

                case "Hemoglobin":
                    summary.setHb(summary.getHb() + 1);
                    break;

                case "Prescription":
                    summary.setPrescription(summary.getPrescription() + 1);
                    break;
            }
        }

        return summary;
    }

    public void addStock(TableStock tableStock, ItemSurveyDetails surveyDetails) {
        try {
            tableStock.setSurveyorUserId(surveyDetails.getSurveyorUserId());

            tableStock.setCampName(surveyDetails.getCampName());
            tableStock.setDeviceId(getDeviceId());
            tableStock.setDeviceNumber(surveyDetails.getDeviceNumber());
            tableStock.setPartnerId(surveyDetails.getPartnerId());

            tableStock.setEntryDateTime(LUtils.getCurrentDateTime());
            tableStock.setEntryDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            tableStock.setSynced(false);
            tableStock.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public void updateStock(TableStock tableStock, ItemSurveyDetails surveyDetails) {
        try {
            TableStock oldStock = getStock(tableStock.getTableKey());
            oldStock.setModified(true);
            oldStock.setModifiedDateTime(LUtils.getCurrentDateTime());
            oldStock.setSynced(false);
            oldStock.update();

            tableStock.setTableKey(0);

            tableStock.setCampName(surveyDetails.getCampName());
            tableStock.setDeviceNumber(surveyDetails.getDeviceNumber());
            tableStock.setDeviceId(getDeviceId());
            tableStock.setPartnerId(surveyDetails.getPartnerId());

            tableStock.setDeviceId(getDeviceId());

            tableStock.setEntryDateTime(LUtils.getCurrentDateTime());
            tableStock.setEntryDateTimeMillis(LUtils.getCurrentDateTimeMillis());
            tableStock.setSynced(false);
            tableStock.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public List<TableStock> getStockItems() {
        try {
            return SQLite.select()
                    .from(TableStock.class)
                    .where(TableStock_Table.modified.eq(false))
                    .orderBy(TableStock_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableStock> getStockItemsReport() {
        try {
            return SQLite.select()
                    .from(TableStock.class)
                    .where(TableStock_Table.modified.eq(false))
                    .and(TableStock_Table.inactive.eq(false))
                    .orderBy(TableStock_Table.table_key, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableStock> getStockItems(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableStock.class)
                    .where(TableStock_Table.synced.eq(synced))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableImage> getImages(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableImage.class)
                    .where(TableImage_Table.synced.eq(synced))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public List<TableImage> getImages(String registrationId) {
        try {
            return SQLite.select()
                    .from(TableImage.class)
                    .where(TableImage_Table.registration_id.eq(registrationId))
                    .and(TableImage_Table.modified.notEq(true))
                    .orderBy(TableImage_Table.image_date_time_millis, false)
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public TableStock getStock(int tableKey) {
        try {
            return SQLite.select()
                    .from(TableStock.class)
                    .where(TableStock_Table.table_key.eq(tableKey))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new TableStock();
    }

    public String getHeadOfHouse(String houseId) {
        try {
            return SQLite.select()
                    .from(TableHouse.class)
                    .where(TableHouse_Table.house_id.eq(houseId))
                    .and(TableHouse_Table.modified.notEq(true))
                    .querySingle().getHeadOfHouse();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public String getAddress(String houseId) {
        String address = "";

        try {
            TableHouse tableHouse = SQLite.select()
                    .from(TableHouse.class)
                    .where(TableHouse_Table.house_id.eq(houseId))
                    .and(TableHouse_Table.modified.notEq(true))
                    .querySingle();

            if (hasValue(tableHouse.getHouseNo())) {
                address += tableHouse.getHouseNo() + ", ";
            }

            if (hasValue(tableHouse.getStreet())) {
                address += tableHouse.getStreet() + ", ";
            }

            if (hasValue(tableHouse.getPost())) {
                address += tableHouse.getPost() + ", ";
            }

            if (hasValue(tableHouse.getLocality())) {
                address += tableHouse.getLocality() + ", ";
            }

            if (hasValue(tableHouse.getVillage())) {
                address += tableHouse.getVillage() + ", ";
            }

            if (hasValue(tableHouse.getDistrict())) {
                address += tableHouse.getDistrict() + ", ";
            }

            if (hasValue(tableHouse.getState())) {
                address += tableHouse.getState() + ", ";
            }

            if (hasValue("" + tableHouse.getPinCode())) {
                address += tableHouse.getPinCode() + " ";
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return address;
    }

    public int getHouseMembersCount(String houseId) {
        try {
            return SQLite.select()
                    .from(TableDemographics.class)
                    .where(TableDemographics_Table.house_id.eq(houseId))
                    .and(TableDemographics_Table.modified.notEq(true))
                    .and(TableDemographics_Table.inactive.notEq(true))
                    .queryList().size();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return 0;
    }

    public int getHouseMembersCount(TableHouse tableHouse) {

        try {
            List<TableHouse> housesList = SQLite.select()
                    .from(TableHouse.class)
                    .where(TableHouse_Table.house_number.eq(tableHouse.getHouseNo()))
                    .and(TableHouse_Table.street.eq(tableHouse.getStreet()))
                    .and(TableHouse_Table.post.eq(tableHouse.getPost()))
                    .and(TableHouse_Table.locality.eq(tableHouse.getLocality()))
                    .and(TableHouse_Table.pincode.eq(tableHouse.getPinCode()))
                    .and(TableHouse_Table.modified.notEq(true))
                    .and(TableHouse_Table.inactive.notEq(true))
                    .queryList();

            int demographicsCount = 0;

            for (TableHouse house : housesList) {
                demographicsCount += getHouseMembersCount(house.getHouseId());
            }

            return demographicsCount;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return 0;
    }

    public List<TableDemographics> getHouseMembers(String houseId, boolean showInactive) {
        try {
            if (showInactive) {
                return SQLite.select()
                        .from(TableDemographics.class)
                        .where(TableDemographics_Table.house_id.eq(houseId))
                        .and(TableDemographics_Table.modified.notEq(true))
                        .queryList();
            } else {
                return SQLite.select()
                        .from(TableDemographics.class)
                        .where(TableDemographics_Table.house_id.eq(houseId))
                        .and(TableDemographics_Table.modified.notEq(true))
                        .and(TableDemographics_Table.inactive.notEq(true))
                        .queryList();
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public List<TableDemographics> getHouseMembers(TableHouse tableHouse, boolean showInactive) {

        try {
            List<TableHouse> housesList = SQLite.select()
                    .from(TableHouse.class)
                    .where(TableHouse_Table.house_number.eq(tableHouse.getHouseNo()))
                    .and(TableHouse_Table.street.eq(tableHouse.getStreet()))
                    .and(TableHouse_Table.post.eq(tableHouse.getPost()))
                    .and(TableHouse_Table.locality.eq(tableHouse.getLocality()))
                    .and(TableHouse_Table.pincode.eq(tableHouse.getPinCode()))
                    .and(TableHouse_Table.modified.notEq(true))
                    .queryList();

            List<TableDemographics> demographicsList = new ArrayList<>();

            for (TableHouse house : housesList) {
                demographicsList.addAll(getHouseMembers(house.getHouseId(), showInactive));
            }

            return demographicsList;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public void updateBluetoothDevices(ItemBluetoothDevice[] itemBluetoothDevices, ItemSurveyDetails surveyDetails) {
        for (ItemBluetoothDevice itemBluetoothDevice : itemBluetoothDevices) {
            TableKitConfig tableKitConfig = getBluetoothDevice(itemBluetoothDevice.getDeviceType(), itemBluetoothDevice.getDeviceName());

            if (null == tableKitConfig) {
                // Create
                tableKitConfig = new TableKitConfig();

                tableKitConfig.setConfigType(itemBluetoothDevice.getDeviceType());
                tableKitConfig.setProductName(itemBluetoothDevice.getDeviceName());
                tableKitConfig.setMacAddress(itemBluetoothDevice.getMacAddress());

                tableKitConfig.setDeviceNumber(surveyDetails.getDeviceNumber());
                tableKitConfig.setPartnerId(surveyDetails.getPartnerId());

                tableKitConfig.setDeviceId(getDeviceId());
                tableKitConfig.setEntryDateTime(LUtils.getCurrentDateTime());
                tableKitConfig.setEntryDateTimeMillis(LUtils.getCurrentDateTimeMillis());
                tableKitConfig.setSynced(false);
                tableKitConfig.save();

            } else {
                // Update if mac addresses are different
                if (!tableKitConfig.getMacAddress().equals(itemBluetoothDevice.getMacAddress())) {

                    tableKitConfig.setModified(true);
                    tableKitConfig.setModifiedDateTime(LUtils.getCurrentDateTime());
                    tableKitConfig.setInactive(true);
                    tableKitConfig.setSynced(false);
                    tableKitConfig.update();

                    tableKitConfig.setTableKey(0);

                    tableKitConfig.setMacAddress(itemBluetoothDevice.getMacAddress());
                    tableKitConfig.setDeviceNumber(surveyDetails.getDeviceNumber());
                    tableKitConfig.setPartnerId(surveyDetails.getPartnerId());

                    tableKitConfig.setDeviceId(getDeviceId());
                    tableKitConfig.setEntryDateTime(LUtils.getCurrentDateTime());
                    tableKitConfig.setEntryDateTimeMillis(LUtils.getCurrentDateTimeMillis());
                    tableKitConfig.setModified(false);
                    tableKitConfig.setModifiedDateTime("");
                    tableKitConfig.setInactive(false);
                    tableKitConfig.setSynced(false);
                    tableKitConfig.save();
                }
            }
        }
    }

    private TableKitConfig getBluetoothDevice(String type, String name) {
        try {
            return SQLite.select()
                    .from(TableKitConfig.class)
                    .where(TableKitConfig_Table.config_type.eq(type))
                    .and(TableKitConfig_Table.product_name.eq(name))
                    .and(TableKitConfig_Table.modified.notEq(true))
                    .and(TableKitConfig_Table.inactive.notEq(true))
                    .querySingle();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public ArrayList<ItemBluetoothDevice> getBluetoothDevice(String type) {

        try {
            List<TableKitConfig> tableKitConfigList = SQLite.select()
                    .from(TableKitConfig.class)
                    .where(TableKitConfig_Table.config_type.eq(type))
                    .and(TableKitConfig_Table.modified.notEq(true))
                    .and(TableKitConfig_Table.inactive.notEq(true))
                    .queryList();

            ArrayList<ItemBluetoothDevice> bluetoothDevices = null;

            for (TableKitConfig tableKitConfig : tableKitConfigList) {
                ItemBluetoothDevice itemBluetoothDevice = new ItemBluetoothDevice();
                itemBluetoothDevice.setDeviceType(type);
                itemBluetoothDevice.setDeviceName(tableKitConfig.getProductName());
                itemBluetoothDevice.setMacAddress(tableKitConfig.getMacAddress());

                if (null == bluetoothDevices) {
                    bluetoothDevices = new ArrayList<>();
                }

                bluetoothDevices.add(itemBluetoothDevice);
            }

            return bluetoothDevices;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public void updateTableKitWithPartnerDetails(ItemSurveyDetails surveyDetails) {
        List<TableKitConfig> tableKitConfigList = SQLite.select()
                .from(TableKitConfig.class)
                .where(TableKitConfig_Table.modified.notEq(true))
                .and(TableKitConfig_Table.inactive.notEq(true))
                .queryList();

        for (TableKitConfig tableKitConfig : tableKitConfigList) {
            tableKitConfig.setModified(true);
            tableKitConfig.setModifiedDateTime(LUtils.getCurrentDateTime());
            tableKitConfig.setInactive(true);
            tableKitConfig.setSynced(false);
            tableKitConfig.update();

            tableKitConfig.setTableKey(0);
            tableKitConfig.setDeviceNumber(surveyDetails.getDeviceNumber());
            tableKitConfig.setPartnerId(surveyDetails.getPartnerId());

            tableKitConfig.setDeviceId(getDeviceId());
            tableKitConfig.setEntryDateTime(LUtils.getCurrentDateTime());
            tableKitConfig.setEntryDateTimeMillis(LUtils.getCurrentDateTimeMillis());
            tableKitConfig.setModified(false);
            tableKitConfig.setModifiedDateTime("");
            tableKitConfig.setInactive(false);
            tableKitConfig.setSynced(false);
            tableKitConfig.save();
        }
    }

    public List<TableKitConfig> getAllKitConfig(boolean synced) {
        try {
            return SQLite.select()
                    .from(TableKitConfig.class)
                    .where(TableKitConfig_Table.synced.eq(synced))
                    .queryList();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ArrayList<>();
    }

    public void createImage(TableImage tableImage, ItemSurveyDetails surveyDetails) {
        try {

            tableImage.setSurveyorUserId(surveyDetails.getSurveyorUserId());
            tableImage.setSurveyDateTime(LUtils.getCurrentDateTime());
            tableImage.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            tableImage.setPartnerId(surveyDetails.getPartnerId());
            tableImage.setCampName(surveyDetails.getCampName());
            tableImage.setDeviceNumber(surveyDetails.getDeviceNumber());
            tableImage.setDeviceId(getDeviceId());

            tableImage.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public String getProfilePic(String registrationId) {
        String imgType = AssetReader.getFaceImageType();

        try {
            return SQLite.select()
                    .from(TableImage.class)
                    .where(TableImage_Table.registration_id.eq(registrationId))
                    .and(TableImage_Table.image_type.eq(imgType))
                    .orderBy(TableImage_Table.survey_date_time_millis, false)
                    .querySingle()
                    .getImageName();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public TableImage getImageTable(int tableKey) {
        return SQLite.select()
                .from(TableImage.class)
                .where(TableImage_Table.table_key.eq(tableKey))
                .querySingle();
    }

    public void updateImage(TableImage tableImage, ItemSurveyDetails surveyDetails, String deviceId) {
        try {
            TableImage oldImage = getImageTable(tableImage.getTableKey());
            oldImage.setModified(true);
            oldImage.setModifiedDateTime(LUtils.getCurrentDateTime());

            oldImage.setSynced(false);
            oldImage.update();

            tableImage.setTableKey(0);
            tableImage.setSurveyorUserId(surveyDetails.getSurveyorUserId());
            tableImage.setSurveyDateTime(LUtils.getCurrentDateTime());
            tableImage.setSurveyDateTimeMillis(LUtils.getCurrentDateTimeMillis());

            tableImage.setDeviceId(deviceId);
            tableImage.setCampName(surveyDetails.getCampName());
            tableImage.setDeviceNumber(surveyDetails.getDeviceNumber());
            tableImage.setPartnerId(surveyDetails.getPartnerId());

            tableImage.setSynced(false);
            tableImage.save();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
    }

    public void clearDatabase() {
        // Delete all table rows
        SQLite.delete().from(TableCampInfo.class).execute();
        SQLite.delete().from(TableDeathInfo.class).execute();
        SQLite.delete().from(TableDemographics.class).execute();
        SQLite.delete().from(TableHouse.class).execute();
        SQLite.delete().from(TableImage.class).execute();
        SQLite.delete().from(TableKitConfig.class).execute();
        SQLite.delete().from(TableProblems.class).execute();
        SQLite.delete().from(TableSociodemographics.class).execute();
        SQLite.delete().from(TableStock.class).execute();
        SQLite.delete().from(TableSubscription.class).execute();
        SQLite.delete().from(TableTarget.class).execute();
        SQLite.delete().from(TableUser.class).execute();
        SQLite.delete().from(TableVitals.class).execute();
    }
}