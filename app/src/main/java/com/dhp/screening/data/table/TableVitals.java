package com.dhp.screening.data.table;

import com.dhp.screening.data.manager.LDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

@Table(database = LDatabase.class)
public class TableVitals extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "registration_id")
    private String registrationId = "";

    @Column(name = "ehr_number")
    private String ehrNumber = "";

    @Column(name = "vital_type")
    private String vitalType = "";

    @Column(name = "vital_subtypes")
    private String vitalSubTypes = "";

    @Column(name = "vital_values")
    private String vitalValues = "";

    @Column(name = "vital_units")
    private String vitalUnits = "";

    @Column(name = "vital_date_time")
    private String vitalDateTime = "";

    @Column(name = "vital_date_time_millis")
    private long vitalDateTimeMillis = 0;

    @Column(name = "vital_image")
    private String vitalImage = "";

    @Column(name = "notes")
    private String notes = "";

    @Column(name = "device_name")
    private String deviceName = "";

    @Column(name = "gps_location")
    private String gpsLocation = "";

    @Column(name = "qr_scanned")
    private boolean qrScanned;

    @Column(name = "surveyor_user_id")
    private String surveyorUserId = "";

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "device_number")
    private String deviceNumber = "";

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "survey_date_time")
    private String surveyDateTime = "";

    @Column(name = "survey_date_time_millis")
    private long surveyDateTimeMillis;

    @Column(name = "inactive")
    private boolean inactive;

    @Column(name = "inactive_reason")
    private String inactiveReason = "";

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getVitalType() {
        return vitalType;
    }

    public void setVitalType(String vitalType) {
        this.vitalType = vitalType;
    }

    public String getVitalSubTypes() {
        return vitalSubTypes;
    }

    public void setVitalSubTypes(String vitalSubTypes) {
        this.vitalSubTypes = vitalSubTypes;
    }

    public String getVitalValues() {
        return vitalValues;
    }

    public void setVitalValues(String vitalValues) {
        this.vitalValues = vitalValues;
    }

    public String getVitalUnits() {
        return vitalUnits;
    }

    public void setVitalUnits(String vitalUnits) {
        this.vitalUnits = vitalUnits;
    }

    public String getVitalDateTime() {
        return vitalDateTime;
    }

    public void setVitalDateTime(String vitalDateTime) {
        this.vitalDateTime = vitalDateTime;
    }

    public long getVitalDateTimeMillis() {
        return vitalDateTimeMillis;
    }

    public void setVitalDateTimeMillis(long vitalDateTimeMillis) {
        this.vitalDateTimeMillis = vitalDateTimeMillis;
    }

    public String getVitalImage() {
        return vitalImage;
    }

    public void setVitalImage(String vitalImage) {
        this.vitalImage = vitalImage;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public boolean isQrScanned() {
        return qrScanned;
    }

    public void setQrScanned(boolean qrScanned) {
        this.qrScanned = qrScanned;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}