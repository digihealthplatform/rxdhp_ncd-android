package com.dhp.screening.data.table;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

import com.dhp.screening.data.manager.LDatabase;


@Table(database = LDatabase.class)
public class TableUser extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "user_id")
    private String userId = "";

    @Column(name = "full_name")
    private String fullName = "";

    @Column(name = "password")
    private String password = "";

    @Column(name = "user_type")
    private String userType = "";

    @Column(name = "gender")
    private String gender = "";

    @Column(name = "dob")
    private String dob = "";

    @Column(name = "yob")
    private int yob = 0;

    @Column(name = "identity_type")
    private String identityType = "";

    @Column(name = "identity_number")
    private String identityNumber = "";

    @Column(name = "phone_number")
    private String phoneNumber = "";

    @Column(name = "email_id")
    private String emailId = "";

    @Column(name = "house_number")
    private String houseNo = "";

    @Column(name = "street")
    private String street = "";

    @Column(name = "post")
    private String post = "";

    @Column(name = "locality")
    private String locality = "";

    @Column(name = "district")
    private String district = "";

    @Column(name = "state")
    private String state = "";

    @Column(name = "country")
    private String country = "";

    @Column(name = "pincode")
    private int pinCode = 0;

    @Column(name = "allow_prescription")
    private boolean allowPrescription;

    @Column(name = "is_demo_user")
    private boolean isDemoUser;

    @Column(name = "qr_scanned")
    private boolean qrScanned;

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "device_number")
    private String deviceNumber = "";

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "entry_date_time")
    private String entryDateTime = "";

    @Column(name = "entry_date_time_millis")
    private long entryDateTimeMillis;

    @Column(name = "inactive")
    private boolean inactive;

    @Column(name = "inactive_reason")
    private String inactiveReason = "";

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public int getYob() {
        return yob;
    }

    public void setYob(int yob) {
        this.yob = yob;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public void setIdentityNumber(String identityNumber) {
        this.identityNumber = identityNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public boolean isAllowPrescription() {
        return allowPrescription;
    }

    public void setAllowPrescription(boolean allowPrescription) {
        this.allowPrescription = allowPrescription;
    }

    public boolean isDemoUser() {
        return isDemoUser;
    }

    public void setDemoUser(boolean demoUser) {
        isDemoUser = demoUser;
    }

    public boolean isQrScanned() {
        return qrScanned;
    }

    public void setQrScanned(boolean qrScanned) {
        this.qrScanned = qrScanned;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(String entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public long getEntryDateTimeMillis() {
        return entryDateTimeMillis;
    }

    public void setEntryDateTimeMillis(long entryDateTimeMillis) {
        this.entryDateTimeMillis = entryDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}