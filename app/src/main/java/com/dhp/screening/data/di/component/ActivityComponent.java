package com.dhp.screening.data.di.component;

import dagger.Component;

import com.dhp.screening.data.di.PerActivity;
import com.dhp.screening.data.di.module.ActivityModule;
import com.dhp.screening.ui.admin.ActivityAddUser;
import com.dhp.screening.ui.admin.ActivityAdmin4Settings;
import com.dhp.screening.ui.admin.ActivityAdminHemoglobin;
import com.dhp.screening.ui.admin.ActivityAdminHome;
import com.dhp.screening.ui.admin.ActivityAdminSettings;
import com.dhp.screening.ui.admin.ActivityCampSettings;
import com.dhp.screening.ui.admin.ActivityDeviceNumberSettings;
import com.dhp.screening.ui.admin.ActivityExportSettings;
import com.dhp.screening.ui.admin.ActivityImportSettings;
import com.dhp.screening.ui.admin.ActivityPartnerIdSettings;
import com.dhp.screening.ui.admin.ActivityViewUser;
import com.dhp.screening.ui.house.ActivityEditHouse;
import com.dhp.screening.ui.house.ActivityHouseDetails;
import com.dhp.screening.ui.house.ActivityHouseList;
import com.dhp.screening.ui.images.ActivityAddImage;
import com.dhp.screening.ui.images.ActivityImageDetails;
import com.dhp.screening.ui.images.ActivityImagesList;
import com.dhp.screening.ui.login.ActivityCopyAssets;
import com.dhp.screening.ui.login.ActivityLogin;
import com.dhp.screening.ui.login.ActivityLoginCheck;
import com.dhp.screening.ui.patient.ActivityEditDemographics;
import com.dhp.screening.ui.patient.ActivityEditQuestions;
import com.dhp.screening.ui.patient.ActivityEditSociodemographics;
import com.dhp.screening.ui.patient.ActivityHouseMembers;
import com.dhp.screening.ui.patient.ActivityPatientDetails;
import com.dhp.screening.ui.patient.ActivityPatientSearch;
import com.dhp.screening.ui.patient.ActivityPatientsList;
import com.dhp.screening.ui.statistics.ActivityGraph;
import com.dhp.screening.ui.statistics.ActivityMissingDemographics;
import com.dhp.screening.ui.statistics.ActivityStatistics;
import com.dhp.screening.ui.statistics.ActivitySummary;
import com.dhp.screening.ui.stock.ActivityAddStock;
import com.dhp.screening.ui.stock.ActivityStock;
import com.dhp.screening.ui.stock.ActivityViewStock;
import com.dhp.screening.ui.user.ActivityMyAccount;
import com.dhp.screening.ui.user.ActivityUserHome;
import com.dhp.screening.ui.vital.ActivityEnterAlbumin;
import com.dhp.screening.ui.vital.ActivityEnterBloodGroup;
import com.dhp.screening.ui.vital.ActivityEnterBp;
import com.dhp.screening.ui.vital.ActivityEnterHeight;
import com.dhp.screening.ui.vital.ActivityEnterPulse;
import com.dhp.screening.ui.vital.ActivitySelectVital;
import com.dhp.screening.ui.vital.hemoglobin.ActivityStart;
import com.dhp.screening.ui.vital.test.ActivityTestResult;
import com.dhp.screening.ui.vital.test.ActivityTestSelection;
import com.dhp.screening.ui.vital.ActivityVitalSummary;
import com.dhp.screening.ui.vital.hemoglobin.ActivityCameraView;
import com.dhp.screening.ui.vital.hemoglobin.ActivityEnterHemoglobin;
import com.dhp.screening.ui.vital.ActivityEnterPrescription;
import com.dhp.screening.ui.vital.ActivityEnterRbs;
import com.dhp.screening.ui.vital.ActivityEnterWeight;
import com.dhp.screening.ui.vital.ActivityVitalDetails;
import com.dhp.screening.ui.vital.ActivityVitalsList;
import com.dhp.screening.ui.vital.hemoglobin.ActivitySettings;
import com.dhp.screening.ui.vital.hemoglobin.ActivityTestStep2;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(ActivityLoginCheck activityLoginCheck);

    void inject(ActivityLogin activityLogin);

    void inject(ActivityAdminHome activityAdminHome);

    void inject(ActivityUserHome activityUserHome);

    void inject(ActivityPatientDetails activityPatientDetails);

    void inject(ActivityPatientsList activityPatientsList);

    void inject(ActivityPatientSearch activityPatientSearch);

    void inject(ActivityEnterHeight activityEnterHeight);

    void inject(ActivityEnterWeight activityEnterWeight);

    void inject(ActivityEnterBp activityEnterBp);

    void inject(ActivityEnterRbs activityEnterRbs);

    void inject(ActivityEnterHemoglobin activityEnterHemoglobin);

    void inject(ActivityEnterAlbumin activityEnterAlbumin);

    void inject(ActivityHouseDetails activityHouseDetails);

    void inject(ActivityHouseList activityHouseList);

    void inject(ActivityEditHouse activityEditHouse);

    void inject(ActivityEditQuestions activityEditQuestions);

    void inject(ActivityVitalDetails activityVitalDetails);

    void inject(ActivityEditDemographics activityEditDemographics);

    void inject(ActivityEditSociodemographics activityEditSociodemographics);

    void inject(ActivityMyAccount activityMyAccount);

    void inject(ActivityEnterBloodGroup activityEnterBloodGroup);

    void inject(ActivityEnterPrescription activityEnterPrescription);

    void inject(ActivitySummary activitySummary);

    void inject(ActivityVitalsList activityVitalsList);

    void inject(ActivityStatistics activityStatistics);

    void inject(ActivityCameraView activityCameraView);

    void inject(ActivitySettings activitySettings);

    void inject(ActivityTestStep2 activityTestStep2);

    void inject(ActivitySelectVital activitySelectVital);

    void inject(ActivityGraph activityGraph);

    void inject(ActivityVitalSummary activityVitalSummary);

    void inject(ActivityAddUser activityAddUser);

    void inject(ActivityViewUser activityViewUser);

    void inject(ActivityMissingDemographics activityMissingDemographics);

    void inject(ActivityViewStock activityViewStock);

    void inject(ActivityStock activityStock);

    void inject(ActivityAddStock activityAddStock);

    void inject(ActivityCopyAssets activityCopyAssets);

    void inject(ActivityAdminSettings activityAdminSettings);

    void inject(ActivityCampSettings activityCampSettings);

    void inject(ActivityHouseMembers activityHouseMembers);

    void inject(ActivityEnterPulse activityEnterPulse);

    void inject(ActivityTestResult activityTestResult);

    void inject(ActivityAdminHemoglobin activityAdminHemoglobin);

    void inject(ActivityStart activityStart);

    void inject(ActivityImagesList activityImagesList);

    void inject(ActivityAddImage activityAddImage);

    void inject(ActivityImageDetails activityImageDetails);

    void inject(ActivityAdmin4Settings activityAdmin4Settings);

    void inject(ActivityExportSettings activityExportSettings);

    void inject(ActivityDeviceNumberSettings activityDeviceNumberSettings);

    void inject(ActivityImportSettings activityImportSettings);

    void inject(ActivityPartnerIdSettings activityPartnerIdSettings);
}