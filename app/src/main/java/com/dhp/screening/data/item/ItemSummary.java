package com.dhp.screening.data.item;

public class ItemSummary {

    private int totalDemographics;
    private int male;
    private int female;
    private int others;

    private int totalVitals;
    private int height;
    private int weight;
    private int bp;
    private int rbs;
    private int hemoglobin;
    private int albumin;
    private int bloodGroup;
    private int prescription;

    public int getTotalDemographics() {
        return totalDemographics;
    }

    public void setTotalDemographics(int totalDemographics) {
        this.totalDemographics = totalDemographics;
    }

    public int getMale() {
        return male;
    }

    public void setMale(int male) {
        this.male = male;
    }

    public int getFemale() {
        return female;
    }

    public void setFemale(int female) {
        this.female = female;
    }

    public int getOthers() {
        return others;
    }

    public void setOthers(int others) {
        this.others = others;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getBp() {
        return bp;
    }

    public void setBp(int bp) {
        this.bp = bp;
    }

    public int getRbs() {
        return rbs;
    }

    public void setRbs(int rbs) {
        this.rbs = rbs;
    }

    public int getHemoglobin() {
        return hemoglobin;
    }

    public void setHemoglobin(int hemoglobin) {
        this.hemoglobin = hemoglobin;
    }

    public int getAlbumin() {
        return albumin;
    }

    public void setAlbumin(int albumin) {
        this.albumin = albumin;
    }

    public int getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(int bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public int getPrescription() {
        return prescription;
    }

    public void setPrescription(int prescription) {
        this.prescription = prescription;
    }

    public int getTotalVitals() {
        return totalVitals;
    }

    public void setTotalVitals(int totalVitals) {
        this.totalVitals = totalVitals;
    }
}
