package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.ui.vital.ActivityEnterAlbumin;
import com.dhp.screening.ui.vital.ActivityEnterBloodGroup;
import com.dhp.screening.ui.vital.ActivityEnterBp;
import com.dhp.screening.ui.vital.ActivityEnterHeight;
import com.dhp.screening.ui.vital.ActivityEnterPrescription;
import com.dhp.screening.ui.vital.ActivityEnterPulse;
import com.dhp.screening.ui.vital.ActivityEnterRbs;
import com.dhp.screening.ui.vital.ActivityEnterWeight;
import com.dhp.screening.ui.vital.hemoglobin.ActivityEnterHemoglobin;
import com.dhp.screening.ui.vital.test.ActivityTestSelection;
import com.dhp.screening.util.LLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class AdapterSelectVitals extends RecyclerView.Adapter<AdapterSelectVitals.ViewHolder> {

    private final Context context;

    private ArrayList<String> vitals;
    private JSONObject        icons;

    private final LayoutInflater layoutInflater;

    public AdapterSelectVitals(Context context, ArrayList<String> vitals, JSONObject icons) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.vitals = vitals;
        this.icons = icons;
    }

    @Override
    public AdapterSelectVitals.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_select_vital, parent, false);

        if (vitals.size() > 8) {
            view.getLayoutParams().height = (parent.getMeasuredHeight() / 5);

        } else if (vitals.size() > 6) {
            view.getLayoutParams().height = (parent.getMeasuredHeight() / 4);

        } else {
            view.getLayoutParams().height = (parent.getMeasuredHeight() / 3);
        }

        return new AdapterSelectVitals.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSelectVitals.ViewHolder holder, int position) {
        try {
            File imageFile = new File(Environment.getExternalStorageDirectory(), context.getString(R.string.icons_folder) + icons.getString(vitals.get(position)));

            if (imageFile.exists()) {
                holder.ivIcon.setImageBitmap(BitmapFactory.decodeFile(imageFile.getAbsolutePath()));
            }
        } catch (JSONException e) {
            LLog.printStackTrace(e);
        }

        holder.tLabel.setText(vitals.get(position));
    }

    @Override
    public int getItemCount() {
        if (null != vitals) {
            return vitals.size();
        }

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView ivIcon;
        private TextView  tLabel;

        ViewHolder(View itemView) {
            super(itemView);
            ivIcon = (ImageView) itemView.findViewById(R.id.iv_icon);
            tLabel = (TextView) itemView.findViewById(R.id.tv_label);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            Intent intent = null;

            switch (vitals.get(getAdapterPosition())) {
                case "Height": {
                    intent = new Intent(context, ActivityEnterHeight.class);
                    break;
                }
                case "Weight": {
                    intent = new Intent(context, ActivityEnterWeight.class);
                    break;
                }
                case "Blood Pressure": {
                    intent = new Intent(context, ActivityEnterBp.class);
                    break;
                }
                case "Blood Sugar": {
                    intent = new Intent(context, ActivityEnterRbs.class);
                    break;
                }
                case "Hemoglobin": {
                    intent = new Intent(context, ActivityEnterHemoglobin.class);
                    break;
                }
                case "Albumin": {
                    intent = new Intent(context, ActivityEnterAlbumin.class);
                    break;
                }
                case "Blood Group": {
                    intent = new Intent(context, ActivityEnterBloodGroup.class);
                    break;
                }
                case "Pulse Oximeter": {
                    intent = new Intent(context, ActivityEnterPulse.class);
                    break;
                }
                case "Prescription": {
                    intent = new Intent(context, ActivityEnterPrescription.class);
                    break;
                }
                case "Malaria":
                case "Dengue":
                case "Syphilis":
                case "HIV-AIDS":
                case "Pregnancy":
                case "Heart Attack": {
                    intent = new Intent(context, ActivityTestSelection.class);
                    intent.putExtra("test_type", vitals.get(getAdapterPosition()));
                    break;
                }
            }

            if (null != intent) {
                context.startActivity(intent);
            }
        }
    }
}