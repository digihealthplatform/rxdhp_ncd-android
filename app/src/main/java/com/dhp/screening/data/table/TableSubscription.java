package com.dhp.screening.data.table;

import com.dhp.screening.data.manager.LDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

@Table(database = LDatabase.class)
public class TableSubscription extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "subscription_type")
    private String subscriptionType = "";

    @Column(name = "subscription_start_date")
    private String subscriptionStartDate = "";

    @Column(name = "subscription_end_date")
    private String subscriptionEndDate = "";

    @Column(name = "subscription_start_number")
    private String subscriptionStartNumber = "";

    @Column(name = "subscription_end_number")
    private String subscriptionEndNumber = "";

    @Column(name = "reorder")
    private int reorder;

    @Column(name = "notes")
    private String notes = "";

    @Column(name = "surveyor_user_id")
    private String surveyorUserId = "";

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "device_number")
    private String deviceNumber = "";

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "entry_date_time")
    private String entryDateTime = "";

    @Column(name = "entry_date_time_millis")
    private long entryDateTimeMillis;

    @Column(name = "inactive")
    private boolean inactive;

    @Column(name = "inactive_reason")
    private String inactiveReason = "";

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(String subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public String getSubscriptionStartDate() {
        return subscriptionStartDate;
    }

    public void setSubscriptionStartDate(String subscriptionStartDate) {
        this.subscriptionStartDate = subscriptionStartDate;
    }

    public String getSubscriptionEndDate() {
        return subscriptionEndDate;
    }

    public void setSubscriptionEndDate(String subscriptionEndDate) {
        this.subscriptionEndDate = subscriptionEndDate;
    }

    public String getSubscriptionStartNumber() {
        return subscriptionStartNumber;
    }

    public void setSubscriptionStartNumber(String subscriptionStartNumber) {
        this.subscriptionStartNumber = subscriptionStartNumber;
    }

    public String getSubscriptionEndNumber() {
        return subscriptionEndNumber;
    }

    public void setSubscriptionEndNumber(String subscriptionEndNumber) {
        this.subscriptionEndNumber = subscriptionEndNumber;
    }

    public int getReorder() {
        return reorder;
    }

    public void setReorder(int reorder) {
        this.reorder = reorder;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(String entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public long getEntryDateTimeMillis() {
        return entryDateTimeMillis;
    }

    public void setEntryDateTimeMillis(long entryDateTimeMillis) {
        this.entryDateTimeMillis = entryDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}