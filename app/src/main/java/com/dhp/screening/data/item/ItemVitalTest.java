package com.dhp.screening.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemVitalTest {

    @SerializedName("test_name")
    private String testName;

    @SerializedName("test_types")
    private ItemVitalTestType[] testTypes;

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public ItemVitalTestType[] getTestTypes() {
        return testTypes;
    }

    public void setTestTypes(ItemVitalTestType[] testTypes) {
        this.testTypes = testTypes;
    }
}
