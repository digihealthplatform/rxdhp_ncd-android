package com.dhp.screening.data.item;

import java.util.List;

import com.dhp.screening.data.table.TableVitals;

public class ItemVitalHeader {

    private boolean vitalsShown;

    private String date;

    private List<TableVitals> vitals;

    public boolean isVitalsShown() {
        return vitalsShown;
    }

    public void setVitalsShown(boolean vitalsShown) {
        this.vitalsShown = vitalsShown;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<TableVitals> getVitals() {
        return vitals;
    }

    public void setVitals(List<TableVitals> vitals) {
        this.vitals = vitals;
    }
}