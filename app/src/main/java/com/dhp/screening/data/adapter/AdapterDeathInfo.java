package com.dhp.screening.data.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.List;

import com.dhp.screening.R;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.ui.dialog.DialogDeathInfo;
import com.dhp.screening.ui.dialog.DialogDeathInfoCallbacks;

public class AdapterDeathInfo extends RecyclerView.Adapter<AdapterDeathInfo.ViewHolder> {

    private boolean contentChanged;

    private final Context              context;
    private       List<TableDeathInfo> items;
    private final LayoutInflater       layoutInflater;

    public AdapterDeathInfo(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(List<TableDeathInfo> items) {
        this.items = items;

        if (0 < items.size()) {
            contentChanged = true;
        }

        notifyDataSetChanged();
    }

    public List<TableDeathInfo> getItems() {
        return items;
    }

    @Override
    public AdapterDeathInfo.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_death_info, parent, false);
        return new AdapterDeathInfo.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterDeathInfo.ViewHolder holder, int position) {
        holder.etDeathInfo.setText(items.get(position).getPersonName());

        if (0 == position) {
            holder.ivClearDeathInfo.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (null != items) {
            return items.size();
        }

        return 0;
    }

    public void addNewItem(boolean firstPosition) {
        if (firstPosition) {
            items.add(new TableDeathInfo());
            notifyDataSetChanged();
            return;
        }

        new DialogDeathInfo((Activity) context, new DialogDeathInfoCallbacks() {
            @Override
            public void onDeathInfoChanged(TableDeathInfo item, int position) {

                if (!contentChanged) {
                    updateItem(item, 0);
                } else {
                    items.add(item);
                }

                contentChanged = true;
                notifyDataSetChanged();
            }
        }, new TableDeathInfo()).showDialog(items.size());
    }

    private void updateItem(TableDeathInfo item, int position) {
        // Update old item
        items.remove(position);
        items.add(position, item);
        notifyDataSetChanged();
    }

    private void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final EditText  etDeathInfo;
        private final ImageView ivClearDeathInfo;

        ViewHolder(View itemView) {
            super(itemView);
            etDeathInfo = (EditText) itemView.findViewById(R.id.et_death_info);
            ivClearDeathInfo = (ImageView) itemView.findViewById(R.id.iv_clear_death_info);

            etDeathInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new DialogDeathInfo((Activity) context, new DialogDeathInfoCallbacks() {
                        @Override
                        public void onDeathInfoChanged(TableDeathInfo item, int position) {
                            contentChanged = true;
                            updateItem(item, position);
                        }
                    }, items.get(getAdapterPosition())).showDialog(getAdapterPosition());
                }
            });

            ivClearDeathInfo.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            AlertDialog.Builder builder;

            builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

            builder.setMessage(AssetReader.getLangKeyword("confirm_remove"));
            builder.setPositiveButton(AssetReader.getLangKeyword("yes"), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    removeItem(getAdapterPosition());
                }
            });

            builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
            builder.show();
        }
    }
}