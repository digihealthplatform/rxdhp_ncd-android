package com.dhp.screening.data.range;

import com.google.gson.annotations.SerializedName;

public class ItemHb {

    @SerializedName("gender")
    private String gender;

    @SerializedName("age_ranges")
    private ItemHbAge[] itemHbAge;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ItemHbAge[] getItemHbAge() {
        return itemHbAge;
    }

    public void setItemHbAge(ItemHbAge[] itemHbAge) {
        this.itemHbAge = itemHbAge;
    }
}