package com.dhp.screening.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemDefaultUser {

    @SerializedName("user_id")
    private String userId = "";

    @SerializedName("full_name")
    private String fullName = "";

    @SerializedName("password")
    private String password = "";

    @SerializedName("is_demo_user")
    private String isDemoUser = "";

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsDemoUser() {
        return isDemoUser;
    }

    public void setIsDemoUser(String isDemoUser) {
        this.isDemoUser = isDemoUser;
    }
}