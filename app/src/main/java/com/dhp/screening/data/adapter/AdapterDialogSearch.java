package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.dhp.screening.R;
import com.dhp.screening.ui.dialog.DialogSearchCallbacks;

public class AdapterDialogSearch extends RecyclerView.Adapter<AdapterDialogSearch.ViewHolder> {
    private final DialogSearchCallbacks callback;
    private       ArrayList<String>     items;
    private       ArrayList<String>     houseHeads;

    private final LayoutInflater mLayoutInflater;

    private TextView tvEmpty;

    public AdapterDialogSearch(DialogSearchCallbacks callbacks,
                               Context context,
                               ArrayList<String> items,
                               ArrayList<String> houseHeads,
                               TextView tvEmpty) {
        callback = callbacks;
        mLayoutInflater = LayoutInflater.from(context);
        this.items = items;
        this.houseHeads = houseHeads;
        this.tvEmpty = tvEmpty;

        showHideEmpty();
    }

    private void showHideEmpty() {
        if (null == items || 0 == items.size()) {
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            tvEmpty.setVisibility(View.GONE);
        }
    }

    @Override
    public AdapterDialogSearch.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_text, parent, false);
        return new AdapterDialogSearch.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterDialogSearch.ViewHolder holder, int position) {
        holder.tvText.setText(items.get(position));

        if (null != houseHeads) {
            holder.tvHead.setVisibility(View.VISIBLE);
            holder.tvHead.setText("Head of House: " + houseHeads.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (null != items) {
            return items.size();
        }

        return 0;
    }

    public void setItems(ArrayList<String> items, ArrayList<String> houseHeads) {
        this.items = items;
        this.houseHeads = houseHeads;
        showHideEmpty();
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvText;
        private TextView tvHead;

        ViewHolder(View itemView) {
            super(itemView);
            tvText = (TextView) itemView.findViewById(R.id.tv_text);
            tvHead = (TextView) itemView.findViewById(R.id.tv_house_head);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            callback.onSelected(items.get(getAdapterPosition()), getAdapterPosition());
        }
    }
}