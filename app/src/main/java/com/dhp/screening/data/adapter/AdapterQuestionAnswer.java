package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.item.ItemQuestionAnswer;

import java.util.ArrayList;


public class AdapterQuestionAnswer extends RecyclerView.Adapter<AdapterQuestionAnswer.ViewHolder> {

    private ArrayList<ItemQuestionAnswer> items;

    private final LayoutInflater layoutInflater;

    public AdapterQuestionAnswer(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(ArrayList<ItemQuestionAnswer> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public AdapterQuestionAnswer.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_question_answer, parent, false);
        return new AdapterQuestionAnswer.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterQuestionAnswer.ViewHolder holder, int position) {
        holder.tvQuestion.setText(items.get(position).getQuestion());
        holder.tvAnswer.setText(items.get(position).getAnswer());
    }

    @Override
    public int getItemCount() {
        if (null != items) {
            return items.size();
        }

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestion;
        private TextView tvAnswer;

        ViewHolder(View itemView) {
            super(itemView);
            tvQuestion = (TextView) itemView.findViewById(R.id.tv_question);
            tvAnswer = (TextView) itemView.findViewById(R.id.tv_answer);
        }

    }
}