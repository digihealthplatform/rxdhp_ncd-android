package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dhp.screening.R;

public class AdapterScreeningQuestions extends RecyclerView.Adapter<AdapterScreeningQuestions.ViewHolder> {

    private String[] questions;
    private String[] answers;

    private final LayoutInflater layoutInflater;

    public AdapterScreeningQuestions(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(String[] questions, String[] answers) {
        this.questions = questions;
        this.answers = answers;
        notifyDataSetChanged();
    }

    @Override
    public AdapterScreeningQuestions.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_screening_question, parent, false);
        return new AdapterScreeningQuestions.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterScreeningQuestions.ViewHolder holder, final int position) {
        holder.tvQuestion.setText(questions[position]);
        holder.etAnswer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                answers[position] = charSequence.toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        holder.etAnswer.setText(answers[position]);
    }

    @Override
    public int getItemCount() {
        if (null != questions) {
            return questions.length;
        }

        return 0;
    }

    public String[] getAnswers() {
        return answers;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestion;
        private EditText etAnswer;

        ViewHolder(View itemView) {
            super(itemView);
            tvQuestion = (TextView) itemView.findViewById(R.id.tv_question);
            etAnswer = (EditText) itemView.findViewById(R.id.et_answer);
        }
    }
}