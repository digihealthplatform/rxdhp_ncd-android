package com.dhp.screening.data.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import com.dhp.screening.LApplication;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.di.ApplicationContext;
import com.dhp.screening.data.di.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(LApplication lApplication);

    @ApplicationContext
    Context getContext();

    DataManager getDataManager();
}