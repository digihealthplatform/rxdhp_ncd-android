package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.item.ItemLab;

import java.util.ArrayList;

import static com.dhp.screening.util.LUtils.hasValue;


public class AdapterLab extends RecyclerView.Adapter<AdapterLab.ViewHolder> {

    private ArrayList<ItemLab> items;

    private final LayoutInflater layoutInflater;

    public AdapterLab(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(ArrayList<ItemLab> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public AdapterLab.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_lab, parent, false);
        return new AdapterLab.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterLab.ViewHolder holder, int position) {
        final ItemLab itemLab = items.get(position);
        holder.tvHb.setText(itemLab.getHb());
        holder.etLValue.setText(itemLab.getlValue());
        holder.etAValue.setText(itemLab.getaValue());
        holder.etBValue.setText(itemLab.getbValue());

        holder.etLValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                double value = 0.0d;

                try {
                    value = Double.valueOf(holder.etLValue.getText().toString().trim());
                } catch (Exception e) {
                }

                itemLab.setlValue("" + value);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        holder.etAValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                double value = 0.0d;

                try {
                    value = Double.valueOf(holder.etAValue.getText().toString().trim());
                } catch (Exception e) {
                }

                itemLab.setaValue("" + value);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        holder.etBValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                double value = 0.0d;

                try {
                    value = Double.valueOf(holder.etBValue.getText().toString().trim());
                } catch (Exception e) {
                }

                itemLab.setbValue("" + value);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    public int getItemCount() {
        if (null != items) {
            return items.size();
        }

        return 0;
    }

    public boolean isAllValuesEntered() {
        for (int i = 0; i < items.size(); i++) {
            ItemLab itemLab = items.get(i);

            if (!hasValue(itemLab.getlValue())
                    || !hasValue(itemLab.getaValue())
                    || !hasValue(itemLab.getbValue())) {
                return false;
            }
        }

        return true;
    }

    public ArrayList<ItemLab> getItems() {
        return items;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvHb;
        private EditText etLValue;
        private EditText etAValue;
        private EditText etBValue;

        ViewHolder(View itemView) {
            super(itemView);
            tvHb = (TextView) itemView.findViewById(R.id.tv_hb);
            etLValue = (EditText) itemView.findViewById(R.id.et_lValue);
            etAValue = (EditText) itemView.findViewById(R.id.et_aValue);
            etBValue = (EditText) itemView.findViewById(R.id.et_bValue);
        }
    }
}