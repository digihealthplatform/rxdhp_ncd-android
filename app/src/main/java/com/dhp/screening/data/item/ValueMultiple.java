package com.dhp.screening.data.item;

public class ValueMultiple {

    private double valueMin1;
    private double valueMin2;
    private double valueMax1;
    private double valueMax2;

    public double getValueMin1() {
        return valueMin1;
    }

    public void setValueMin1(double valueMin1) {
        this.valueMin1 = valueMin1;
    }

    public double getValueMin2() {
        return valueMin2;
    }

    public void setValueMin2(double valueMin2) {
        this.valueMin2 = valueMin2;
    }

    public double getValueMax1() {
        return valueMax1;
    }

    public void setValueMax1(double valueMax1) {
        this.valueMax1 = valueMax1;
    }

    public double getValueMax2() {
        return valueMax2;
    }

    public void setValueMax2(double valueMax2) {
        this.valueMax2 = valueMax2;
    }
}