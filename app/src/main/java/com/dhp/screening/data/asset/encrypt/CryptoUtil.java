package com.dhp.screening.data.asset.encrypt;

import android.util.Base64;

import com.dhp.screening.util.LLog;

import java.io.File;
import java.io.FileInputStream;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import static com.dhp.screening.util.LConstants.INPUT_JSON_DECRYPT_KEY;


public class CryptoUtil {

    private static byte[] readFromFile(String fileName) {

        try {
            FileInputStream stream = new FileInputStream(new File(fileName));
            byte[]          buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();
            return buffer;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public String encrypt(String secretKey, byte[] plainText) {

        String strData = "";

        try {
            SecretKeySpec skeyspec = new SecretKeySpec(secretKey.getBytes(), "Blowfish");
            Cipher        cipher   = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);

            byte[] encrypted = cipher.doFinal(plainText);
            strData = Base64.encodeToString(encrypted, Base64.DEFAULT);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return strData;
    }

    private String decrypt(String secretKey, byte[] encryptedText) throws Exception {
        String strData = "";

        try {
            SecretKeySpec skeyspec = new SecretKeySpec(secretKey.getBytes(), "Blowfish");
            Cipher        cipher   = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);

            byte[] decrypted = cipher.doFinal(Base64.decode(encryptedText, Base64.DEFAULT));
            strData = new String(decrypted, "UTF-8");

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }

        return strData;
    }

    public static String decryptString(String filePath) throws Exception {
        CryptoUtil cryptoUtil = new CryptoUtil();
        byte[]     content    = readFromFile(filePath);
        return cryptoUtil.decrypt(INPUT_JSON_DECRYPT_KEY, content);

//        FileInputStream stream  = new FileInputStream(filePath);
//        byte[]          content = new byte[stream.available()];
//        stream.read(content);
//        stream.close();
//        return new String(content);
    }
}