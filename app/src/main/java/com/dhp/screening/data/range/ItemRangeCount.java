package com.dhp.screening.data.range;

public class ItemRangeCount {

    private int count;
    private String range = "";
    private String color = "";

    public ItemRangeCount() {
    }

    public ItemRangeCount(String range) {
        this.range = range;
    }

    public ItemRangeCount(String range, int count) {
        this.range = range;
        this.count = count;
    }

    public ItemRangeCount(String range, String color) {
        this.range = range;
        this.color = color;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}