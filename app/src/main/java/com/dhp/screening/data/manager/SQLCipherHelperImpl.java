package com.dhp.screening.data.manager;

import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.sqlcipher.SQLCipherOpenHelper;
import com.raizlabs.android.dbflow.structure.database.DatabaseHelperListener;

import static com.dhp.screening.util.LConstants.DATABASE_ENCRYPTION_KEY;


public class SQLCipherHelperImpl extends SQLCipherOpenHelper {

    public SQLCipherHelperImpl(DatabaseDefinition databaseDefinition, DatabaseHelperListener listener) {
        super(databaseDefinition, listener);
    }

    @Override
    protected String getCipherSecret() {
        return DATABASE_ENCRYPTION_KEY;
    }

//    @Override
//    public void onConfigure(SQLiteDatabase db) {
//        db.rawExecSQL("PRAGMA journal_mode=DELETE");
//        super.onConfigure(db);
//    }
}