package com.dhp.screening.data.item;

import java.util.ArrayList;

public class ItemQuestionHeader {

    private int headerIndex;

    private boolean childrenShown;

    private String groupName;

    private ArrayList<ItemQuestionChildren> questionChildren;

    public int getHeaderIndex() {
        return headerIndex;
    }

    public void setHeaderIndex(int headerIndex) {
        this.headerIndex = headerIndex;
    }

    public boolean isChildrenShown() {
        return childrenShown;
    }

    public void setChildrenShown(boolean childrenShown) {
        this.childrenShown = childrenShown;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ArrayList<ItemQuestionChildren> getQuestionChildren() {
        return questionChildren;
    }

    public void setQuestionChildren(ArrayList<ItemQuestionChildren> questionChildren) {
        this.questionChildren = questionChildren;
    }
}