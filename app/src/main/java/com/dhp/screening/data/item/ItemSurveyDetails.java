package com.dhp.screening.data.item;

import com.dhp.screening.util.LConstants;

public class ItemSurveyDetails {

    private String deviceNumber;
    private String partnerId = LConstants.DEFAULT_PARTNER_ID;
    private String campName  = "";
    private String surveyorUserId;

    private boolean doorToDoor;
    private boolean isDemoUser;

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public boolean isDoorToDoor() {
        return doorToDoor;
    }

    public void setDoorToDoor(boolean doorToDoor) {
        this.doorToDoor = doorToDoor;
    }

    public boolean isDemoUser() {
        return isDemoUser;
    }

    public void setDemoUser(boolean demoUser) {
        isDemoUser = demoUser;
    }
}