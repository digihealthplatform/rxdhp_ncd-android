package com.dhp.screening.data.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dhp.screening.R;


public class AdapterPagination extends RecyclerView.Adapter<AdapterPagination.ViewHolder> {

    private int currentSelection;
    private int totalPages;

    private final Context        context;
    private final LayoutInflater layoutInflater;

    public AdapterPagination(Context context, int totalPages) {
        this.context = context;
        this.totalPages = totalPages;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public AdapterPagination.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_page, parent, false);
        return new AdapterPagination.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterPagination.ViewHolder holder, int position) {
        if (position == currentSelection) {
            holder.tvPage.setBackground(context.getResources().getDrawable(R.drawable.rounded_textview_selected));
            holder.tvPage.setTextColor(Color.WHITE);
        } else {
            holder.tvPage.setBackground(context.getResources().getDrawable(R.drawable.rounded_textview));
            holder.tvPage.setTextColor(Color.BLACK);
        }

        holder.tvPage.setText("" + (position + 1));
    }

    @Override
    public int getItemCount() {
        return totalPages;
    }

    public void setSelection(int position) {
        currentSelection = position;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPage;

        ViewHolder(View itemView) {
            super(itemView);
            tvPage = (TextView) itemView.findViewById(R.id.tv_page_number);
        }
    }
}