package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.item.ItemDiseaseControl;
import com.dhp.screening.util.LUtils;
import com.dhp.screening.util.LView;

import java.util.ArrayList;

import static android.view.View.VISIBLE;
import static com.dhp.screening.data.asset.AssetReader.KEY_AGE_UNITS;
import static com.dhp.screening.data.asset.AssetReader.KEY_CHANGES_TO_CONTROL_DISEASE;
import static com.dhp.screening.util.LUtils.hasValue;


public class AdapterDiseaseControl extends RecyclerView.Adapter<AdapterDiseaseControl.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final Context        context;

    private final String[] timeUnitsEnglish;

    private ArrayList<ItemDiseaseControl> items;

    public AdapterDiseaseControl(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        items = new ArrayList<>();

        timeUnitsEnglish = AssetReader.getSpinnerArrayEnglish(KEY_AGE_UNITS);
    }

    @Override
    public AdapterDiseaseControl.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_disease_control, parent, false);
        return new AdapterDiseaseControl.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterDiseaseControl.ViewHolder holder, int position) {
        final ItemDiseaseControl item = items.get(position);

        holder.tvTestLastDone.setText(AssetReader.getLangKeyword("last_time_test_done") + " " + item.getDiseaseLang() + "?");

        holder.etTestLastDone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                item.setTestDoneTime(editable.toString());
            }
        });

        holder.etTestLastDone.setText(item.getTestDoneTime());

        holder.spTestLastDone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }
                item.setTestDoneTimeUnit(timeUnitsEnglish[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        int index = LUtils.getItemWithOthersIndex(item.getTestDoneTimeUnit(), timeUnitsEnglish);
        if (0 <= index) {
            holder.spTestLastDone.setSelection(index);
        } else {
            holder.spTestLastDone.setSelection(2);
        }

        holder.tvInformedAboutDisease.setText(AssetReader.getLangKeyword("doctor_told_about_disease") + " " + item.getDiseaseLang() + "?");
        holder.rgInformedAboutDisease.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = holder.rgInformedAboutDisease.indexOfChild(holder.rgInformedAboutDisease.findViewById(indexId));

                if (0 == selectedIndex) {
                    holder.tvMedication.setVisibility(VISIBLE);
                    holder.rgMedication.setVisibility(VISIBLE);
                    holder.tvChanges.setVisibility(VISIBLE);
                    holder.llChangesWrapper.setVisibility(VISIBLE);
                    item.setInformedAboutDisease("Yes");

                } else if (1 == selectedIndex) {
                    holder.tvMedication.setVisibility(View.GONE);
                    holder.rgMedication.setVisibility(View.GONE);
                    holder.tvChanges.setVisibility(View.GONE);
                    holder.llChangesWrapper.setVisibility(View.GONE);
                    item.setInformedAboutDisease("No");

                } else {
                    holder.tvMedication.setVisibility(View.GONE);
                    holder.rgMedication.setVisibility(View.GONE);
                    holder.tvChanges.setVisibility(View.GONE);
                    holder.llChangesWrapper.setVisibility(View.GONE);
                    item.setInformedAboutDisease("Don't know");
                }
            }
        });

        if (hasValue(item.getInformedAboutDisease())) {

            if (item.getInformedAboutDisease().equals("Don't know")) {
                holder.tvMedication.setVisibility(View.GONE);
                holder.rgMedication.setVisibility(View.GONE);
                holder.tvChanges.setVisibility(View.GONE);
                holder.llChangesWrapper.setVisibility(View.GONE);
                ((RadioButton) holder.rgInformedAboutDisease.getChildAt(2)).setChecked(true);

            } else if (item.getInformedAboutDisease().equals("No")) {
                holder.tvMedication.setVisibility(View.GONE);
                holder.rgMedication.setVisibility(View.GONE);
                holder.tvChanges.setVisibility(View.GONE);
                holder.llChangesWrapper.setVisibility(View.GONE);
                ((RadioButton) holder.rgInformedAboutDisease.getChildAt(1)).setChecked(true);

            } else {
                holder.tvMedication.setVisibility(VISIBLE);
                holder.rgMedication.setVisibility(VISIBLE);
                holder.tvChanges.setVisibility(VISIBLE);
                holder.llChangesWrapper.setVisibility(VISIBLE);
                ((RadioButton) holder.rgInformedAboutDisease.getChildAt(0)).setChecked(true);
            }
        }

        holder.rgMedication.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int indexId) {
                if (-1 == indexId) {
                    return;
                }

                int selectedIndex = holder.rgMedication.indexOfChild(holder.rgMedication.findViewById(indexId));

                if (0 == selectedIndex) {
                    item.setMedications("Yes");
                } else {
                    item.setMedications("No");
                }
            }
        });

        if (hasValue(item.getMedications())) {
            if (item.getMedications().equals("Yes")) {
                ((RadioButton) holder.rgMedication.getChildAt(0)).setChecked(true);
            } else {
                ((RadioButton) holder.rgMedication.getChildAt(1)).setChecked(true);
            }
        }

        for (int i = 0; i < holder.cbChanges.size(); i++) {
            final int index1   = i;
            CheckBox  checkBox = holder.cbChanges.get(i);

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    item.getChangesChecked()[index1] = b;

                    if (compoundButton.getText().equals(AssetReader.getLangKeyword("other"))) {
                        if (compoundButton.isChecked()) {
                            holder.etChanges.setVisibility(VISIBLE);
                        } else {
                            holder.etChanges.setVisibility(View.GONE);
                        }
                    }
                }
            });
        }

        for (int i = 0; i < item.getChangesChecked().length; i++) {
            if (item.getChangesChecked()[i]) {
                holder.cbChanges.get(i).setChecked(true);
            }
        }

        holder.etChanges.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i1, int i2, int i3) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                item.setChangesOther(editable.toString());
            }
        });

        holder.etChanges.setText(item.getChangesOther());

        if (hasValue(item.getChangesOther())) {
            holder.etChanges.setVisibility(VISIBLE);
        } else {
            holder.etChanges.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (null != items) {
            return items.size();
        }

        return 0;
    }

    public void addItem(String diseaseName, String diseaseLang) {
        ItemDiseaseControl itemDiseaseControl = new ItemDiseaseControl();
        itemDiseaseControl.setDiseaseName(diseaseName);
        itemDiseaseControl.setDiseaseLang(diseaseLang);
        itemDiseaseControl.setChangesChecked(new boolean[AssetReader.getSpinnerArray(KEY_CHANGES_TO_CONTROL_DISEASE).length]);
        items.add(itemDiseaseControl);
        notifyDataSetChanged();
    }

    public void removeItem(String diseaseName) {
        int index = -1;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getDiseaseName().equals(diseaseName)) {
                index = i;
                break;
            }
        }

        if (0 <= index) {
            items.remove(index);
        }

        notifyDataSetChanged();
    }

    public ArrayList<ItemDiseaseControl> getItems() {
        return items;
    }

    public void setItems(ArrayList<ItemDiseaseControl> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView tvTestLastDone;
        private final EditText etTestLastDone;
        private final Spinner  spTestLastDone;

        private final TextView   tvInformedAboutDisease;
        private final RadioGroup rgInformedAboutDisease;

        private final TextView   tvMedication;
        private final RadioGroup rgMedication;

        private final TextView     tvChanges;
        private final LinearLayout llChangesWrapper;
        private final LinearLayout llChanges;
        private final EditText     etChanges;

        private ArrayList<CheckBox> cbChanges = new ArrayList<>(16);

        ViewHolder(View itemView) {
            super(itemView);

            tvTestLastDone = (TextView) itemView.findViewById(R.id.tv_disease_test_last_done);
            etTestLastDone = (EditText) itemView.findViewById(R.id.et_disease_test_last_done);
            spTestLastDone = (Spinner) itemView.findViewById(R.id.sp_disease_test_last_done);
            tvInformedAboutDisease = (TextView) itemView.findViewById(R.id.tv_informed_about_disease);
            rgInformedAboutDisease = (RadioGroup) itemView.findViewById(R.id.rg_informed_about_disease);
            tvMedication = (TextView) itemView.findViewById(R.id.tv_medication_for_disease);
            rgMedication = (RadioGroup) itemView.findViewById(R.id.rg_medication_for_disease);
            tvChanges = (TextView) itemView.findViewById(R.id.tv_changes_to_control_disease);
            llChangesWrapper = (LinearLayout) itemView.findViewById(R.id.ll_changes_to_control_disease_wrapper);
            llChanges = (LinearLayout) itemView.findViewById(R.id.ll_changes_to_control_disease);
            etChanges = (EditText) itemView.findViewById(R.id.et_changes_to_control_disease_other);

            tvMedication.setText(AssetReader.getLangKeyword("do_you_take_medications_for_it"));
            tvChanges.setText(AssetReader.getLangKeyword("what_changes_have_you_made_to_control"));

            ((RadioButton) rgInformedAboutDisease.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
            ((RadioButton) rgInformedAboutDisease.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));
            ((RadioButton) rgInformedAboutDisease.getChildAt(2)).setText(AssetReader.getLangKeyword("dont_know"));

            ((RadioButton) rgMedication.getChildAt(0)).setText(AssetReader.getLangKeyword("yes"));
            ((RadioButton) rgMedication.getChildAt(1)).setText(AssetReader.getLangKeyword("no"));

            etTestLastDone.setHint(AssetReader.getLangKeyword("enter"));
            etChanges.setHint(AssetReader.getLangKeyword("enter"));

            String[] timeUnits = AssetReader.getSpinnerArray(KEY_AGE_UNITS);
            String[] changes   = AssetReader.getCheckBoxArray(KEY_CHANGES_TO_CONTROL_DISEASE);

            LView.setSpinnerAdapter(context, spTestLastDone, timeUnits);

            initCheckboxes(changes);
        }

        private void initCheckboxes(String[] array) {

            for (int i = 0; i < array.length; i++) {
                CheckBox cb = new CheckBox(context);
                cb.setText(array[i]);
                int states[][] = {{android.R.attr.state_checked}, {}};
                int colors[]   = {R.color.teal_dark, R.color.white};
                CompoundButtonCompat.setButtonTintList(cb, new ColorStateList(states, colors));
                cb.setTextColor(ContextCompat.getColor(context, R.color.text_color_dark));
                cb.setTextSize(18.0f);
                llChanges.addView(cb);
                cbChanges.add(cb);
            }
        }
    }
}