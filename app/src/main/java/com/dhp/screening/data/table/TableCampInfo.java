package com.dhp.screening.data.table;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

import com.dhp.screening.data.manager.LDatabase;

@Table(database = LDatabase.class)
public class TableCampInfo extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "start_date")
    private String startDate = "";

    @Column(name = "end_date")
    private String endDate = "";

    @Column(name = "door_to_door")
    private boolean doorToDoor;

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "entry_date_time")
    private String entryDateTime = "";

    @Column(name = "entry_date_time_millis")
    private long entryDateTimeMillis;

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isDoorToDoor() {
        return doorToDoor;
    }

    public void setDoorToDoor(boolean doorToDoor) {
        this.doorToDoor = doorToDoor;
    }

    public String getEntryDateTime() {
        return entryDateTime;
    }

    public void setEntryDateTime(String entryDateTime) {
        this.entryDateTime = entryDateTime;
    }

    public long getEntryDateTimeMillis() {
        return entryDateTimeMillis;
    }

    public void setEntryDateTimeMillis(long entryDateTimeMillis) {
        this.entryDateTimeMillis = entryDateTimeMillis;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}