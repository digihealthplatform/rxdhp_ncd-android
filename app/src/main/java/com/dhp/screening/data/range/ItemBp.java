package com.dhp.screening.data.range;

import com.google.gson.annotations.SerializedName;

public class ItemBp {

    @SerializedName("systolic")
    private String systolic;

    @SerializedName("diastolic")
    private String diastolic;

    public String getSystolic() {
        return systolic;
    }

    public void setSystolic(String systolic) {
        this.systolic = systolic;
    }

    public String getDiastolic() {
        return diastolic;
    }

    public void setDiastolic(String diastolic) {
        this.diastolic = diastolic;
    }
}