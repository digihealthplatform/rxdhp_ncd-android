package com.dhp.screening.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemVitalTests {

    @SerializedName("vital_tests")
    private ItemVitalTest[] vitalTests;

    public ItemVitalTest[] getVitalTests() {
        return vitalTests;
    }

    public void setVitalTests(ItemVitalTest[] vitalTests) {
        this.vitalTests = vitalTests;
    }
}