package com.dhp.screening.data.table;

import com.dhp.screening.data.manager.LDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

@Table(database = LDatabase.class)
public class TableSociodemographics extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "registration_id")
    private String registrationId = "";

    @Column(name = "ehr_number")
    private String ehrNumber = "";

    @Column(name = "relation_to_hoh")
    private String relationToHoh = "";

    @Column(name = "marital_status")
    private String maritalStatus = "";

    @Column(name = "education")
    private String education = "";

    @Column(name = "literacy")
    private String literacy = "";

    @Column(name = "current_occupation")
    private String currentOccupation = "";

    @Column(name = "monthly_income")
    private int monthlyIncome;

    @Column(name = "house_type")
    private String houseType = "";

    @Column(name = "languages_spoken")
    private String languagesSpoken = "";

    @Column(name = "habits")
    private String habits = "";

    @Column(name = "exercise")
    private String exercise = "";

    @Column(name = "diet")
    private String diet = "";

    @Column(name = "disease")
    private String disease = "";

    @Column(name = "disease_treatment")
    private String diseaseTreatment = "";

    @Column(name = "disability")
    private String disability = "";

    @Column(name = "sickDuringSurvey")
    private String sick = "";

    @Column(name = "ailment")
    private String ailment = "";

    @Column(name = "notes")
    private String notes = "";

    @Column(name = "surveyor_user_id")
    private String surveyorUserId = "";

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "device_number")
    private String deviceNumber = "";

    @Column(name = "device_id")
    private String deviceId = "";

    @Column(name = "survey_date_time")
    private String surveyDateTime = "";

    @Column(name = "survey_date_time_millis")
    private long surveyDateTimeMillis;

    @Column(name = "inactive")
    private boolean inactive;

    @Column(name = "inactive_reason")
    private String inactiveReason = "";

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    @Column(name = "health_card")
    private String healthCard = "";

    @Column(name = "medicines_taken_daily")
    private String medicinesTakenDaily = "";

    @Column(name = "disease_test_last_done")
    private String diseaseTestLastDone = "";

    @Column(name = "informed_about_disease")
    private String informedAboutDisease = "";

    @Column(name = "medication_for_disease")
    private String medicationForDisease = "";

    @Column(name = "changes_to_control_disease")
    private String changesToControlDisease = "";

    @Column(name = "women_special_precautions_on_period")
    private String womenSpecialPrecautionsOnPeriod = "";

    @Column(name = "mobile_used")
    private String mobileUsed = "";

    @Column(name = "mobile_comfort")
    private String mobileComfort = "";

    @Column(name = "health_records_storage")
    private String healthRecordsStorage = "";

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getRelationToHoh() {
        return relationToHoh;
    }

    public void setRelationToHoh(String relationToHoh) {
        this.relationToHoh = relationToHoh;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getLiteracy() {
        return literacy;
    }

    public void setLiteracy(String literacy) {
        this.literacy = literacy;
    }

    public String getCurrentOccupation() {
        return currentOccupation;
    }

    public void setCurrentOccupation(String currentOccupation) {
        this.currentOccupation = currentOccupation;
    }

    public int getMonthlyIncome() {
        return monthlyIncome;
    }

    public void setMonthlyIncome(int monthlyIncome) {
        this.monthlyIncome = monthlyIncome;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public String getLanguagesSpoken() {
        return languagesSpoken;
    }

    public void setLanguagesSpoken(String languagesSpoken) {
        this.languagesSpoken = languagesSpoken;
    }

    public String getHabits() {
        return habits;
    }

    public void setHabits(String habits) {
        this.habits = habits;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getDiseaseTreatment() {
        return diseaseTreatment;
    }

    public void setDiseaseTreatment(String diseaseTreatment) {
        this.diseaseTreatment = diseaseTreatment;
    }

    public String getDisability() {
        return disability;
    }

    public void setDisability(String disability) {
        this.disability = disability;
    }

    public String getSick() {
        return sick;
    }

    public void setSick(String sick) {
        this.sick = sick;
    }

    public String getAilment() {
        return ailment;
    }

    public void setAilment(String ailment) {
        this.ailment = ailment;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getHealthCard() {
        return healthCard;
    }

    public void setHealthCard(String healthCard) {
        this.healthCard = healthCard;
    }

    public String getMedicinesTakenDaily() {
        return medicinesTakenDaily;
    }

    public void setMedicinesTakenDaily(String medicinesTakenDaily) {
        this.medicinesTakenDaily = medicinesTakenDaily;
    }

    public String getDiseaseTestLastDone() {
        return diseaseTestLastDone;
    }

    public void setDiseaseTestLastDone(String diseaseTestLastDone) {
        this.diseaseTestLastDone = diseaseTestLastDone;
    }

    public String getInformedAboutDisease() {
        return informedAboutDisease;
    }

    public void setInformedAboutDisease(String informedAboutDisease) {
        this.informedAboutDisease = informedAboutDisease;
    }

    public String getMedicationForDisease() {
        return medicationForDisease;
    }

    public void setMedicationForDisease(String medicationForDisease) {
        this.medicationForDisease = medicationForDisease;
    }

    public String getChangesToControlDisease() {
        return changesToControlDisease;
    }

    public void setChangesToControlDisease(String changesToControlDisease) {
        this.changesToControlDisease = changesToControlDisease;
    }

    public String getWomenSpecialPrecautionsOnPeriod() {
        return womenSpecialPrecautionsOnPeriod;
    }

    public void setWomenSpecialPrecautionsOnPeriod(String womenSpecialPrecautionsOnPeriod) {
        this.womenSpecialPrecautionsOnPeriod = womenSpecialPrecautionsOnPeriod;
    }

    public String getMobileUsed() {
        return mobileUsed;
    }

    public void setMobileUsed(String mobileUsed) {
        this.mobileUsed = mobileUsed;
    }

    public String getMobileComfort() {
        return mobileComfort;
    }

    public void setMobileComfort(String mobileComfort) {
        this.mobileComfort = mobileComfort;
    }

    public String getHealthRecordsStorage() {
        return healthRecordsStorage;
    }

    public void setHealthRecordsStorage(String healthRecordsStorage) {
        this.healthRecordsStorage = healthRecordsStorage;
    }
}