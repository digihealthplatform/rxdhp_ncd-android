package com.dhp.screening.data.range;

public class ItemRange {

    private String range = "";
    private String color = "";

    public ItemRange() {
    }

    public ItemRange(String range, String color) {
        this.range = range;
        this.color = color;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}