package com.dhp.screening.data;

import com.dhp.screening.data.manager.SQLCipherHelperImpl;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.ui.vital.hemoglobin.Value;

import org.w3c.dom.NamedNodeMap;

import java.util.ArrayList;

public class StaticData {
    private static String registrationId;
    private static String qrResult;
    private static String qrSearchResult;

    private static NamedNodeMap aadhaarAttributes;

    private static TableVitals currentVital;
    private static TableHouse  currentHouse;

    private static String           hbResult  = null;
    private static ArrayList<Value> labValues = null;

    private static boolean scannedHouseSaved;

    public static boolean imageUploadInProgress;

    private static String campDeviceInfo;

    public static String capturedImageNames[] = new String[3];

    public static TableHouse currentSelectedHouse;
    public static String     currentSelectedHouseId;

    private static String              databasePath;

    private static SQLCipherHelperImpl sqlCipherHelper;

    public static String getRegistrationId() {
        return registrationId;
    }

    public static void setRegistrationId(String registrationId) {
        StaticData.registrationId = registrationId;
    }

    public static void setAadhaarAttributes(NamedNodeMap aadhaarAttributes) {
        StaticData.aadhaarAttributes = aadhaarAttributes;
    }

    public static NamedNodeMap getAadhaarAttributes() {
        return StaticData.aadhaarAttributes;
    }

    public static void setCurrentVital(TableVitals vital) {
        currentVital = vital;
    }

    public static TableVitals getCurrentVital() {
        return currentVital;
    }

    public static void setCurrentHouse(TableHouse tablehouse) {
        StaticData.currentHouse = tablehouse;
    }

    public static TableHouse getCurrentHouse() {
        return StaticData.currentHouse;
    }

    public static String getQrResult() {
        return qrResult;
    }

    public static void setQrResult(String qrResult) {
        StaticData.qrResult = qrResult;
    }

    public static void saveResult(String result) {
        StaticData.hbResult = result;
    }

    public static String getHbResult() {
        return hbResult;
    }

    public static boolean isScannedHouseSaved() {
        return scannedHouseSaved;
    }

    public static void setScannedHouseSaved(boolean scannedHouseSaved) {
        StaticData.scannedHouseSaved = scannedHouseSaved;
    }

    public static void setCampDeviceInfo(String info) {
        campDeviceInfo = info;
    }

    public static String getCampDeviceInfo() {
        return campDeviceInfo;
    }

    public static String getQrSearchResult() {
        return qrSearchResult;
    }

    public static void setQrSearchResult(String qrSearchResult) {
        StaticData.qrSearchResult = qrSearchResult;
    }

    public static ArrayList<Value> getLabValues() {
        return labValues;
    }

    public static void setLabValues(ArrayList<Value> labValues) {
        StaticData.labValues = labValues;
    }

    public static String getDatabasePath() {
        return databasePath;
    }

    public static void setDatabasePath(String databasePath) {
        StaticData.databasePath = databasePath;
    }

    public static SQLCipherHelperImpl getSqlCipherHelper() {
        return sqlCipherHelper;
    }

    public static void setSqlCipherHelper(SQLCipherHelperImpl sqlCipherHelper) {
        StaticData.sqlCipherHelper = sqlCipherHelper;
    }
}