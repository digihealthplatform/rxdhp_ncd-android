package com.dhp.screening.data.item;

public class ItemVitalRange {

    public static final int HEADER         = 1;
    public static final int HEADER_EMPTY   = 2;
    public static final int RANGE          = 3;
    public static final int VALUE          = 4;
    public static final int VALUE_MULTIPLE = 5;
    public static final int DIVIDER        = 6;

    private int type;

    private String header;
    private String header2;
    private String range;

    private double min;
    private double max;

    private int color;

    private ValueMultiple valueMultiple;

    public ItemVitalRange(int type) {
        this.type = type;
    }

    public ItemVitalRange(int type, String header) {
        this.type = type;
        this.header = header;
    }

    public ItemVitalRange(int type, String header, String range) {
        this.type = type;
        this.header = header;
        this.range = range;
    }

    public ItemVitalRange(int type, String header, String header2, String range) {
        this.type = type;
        this.header = header;
        this.header2 = header2;
        this.range = range;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public double getMax() {
        return max;
    }

    public void setMax(double max) {
        this.max = max;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public ValueMultiple getValueMultiple() {
        return valueMultiple;
    }

    public void setValueMultiple(ValueMultiple valueMultiple) {
        this.valueMultiple = valueMultiple;
    }

    public String getHeader2() {
        return header2;
    }

    public void setHeader2(String header2) {
        this.header2 = header2;
    }
}