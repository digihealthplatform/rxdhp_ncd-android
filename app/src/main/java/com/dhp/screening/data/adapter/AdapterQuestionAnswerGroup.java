package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.dhp.screening.R;
import com.dhp.screening.data.item.ItemQuestionChildren;
import com.dhp.screening.data.item.ItemQuestionHeader;
import com.dhp.screening.data.item.ItemScreeningQuestion;

import static com.dhp.screening.data.item.ItemScreeningQuestion.TYPE_CHILD;
import static com.dhp.screening.data.item.ItemScreeningQuestion.TYPE_HEADER;

public class AdapterQuestionAnswerGroup extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;

    ArrayList<ItemScreeningQuestion> headersList;
    ArrayList<ItemScreeningQuestion> childrenList;

    private final LayoutInflater layoutInflater;

    public AdapterQuestionAnswerGroup(Context context) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    public void setItems(ArrayList<ItemScreeningQuestion> headersList, ArrayList<ItemScreeningQuestion> childrenList) {
        this.headersList = headersList;
        this.childrenList = childrenList;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case TYPE_HEADER: {
                view = layoutInflater.inflate(R.layout.item_question_header, parent, false);
                return new AdapterQuestionAnswerGroup.HeaderHolder(view);
            }

            case TYPE_CHILD: {
                view = layoutInflater.inflate(R.layout.item_screening_question, parent, false);
                return new AdapterQuestionAnswerGroup.ChildrenHolder(view);
            }
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {
            case TYPE_HEADER: {
                String headerText = childrenList.get(position).getQuestionHeader().getGroupName();
                ((HeaderHolder) holder).tvQuestionGroup.setText(headerText);

                if (childrenList.get(position).getQuestionHeader().isChildrenShown()) {
                    ((HeaderHolder) holder).ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_up));
                }

                break;
            }

            case TYPE_CHILD: {
                final ItemQuestionChildren children = childrenList.get(position).getQuestionChildren();
                ((ChildrenHolder) holder).tvQuestion.setText(children.getQuestion());

                ((ChildrenHolder) holder).etAnswer.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        headersList.get(children.getHeaderIndex()).
                                getQuestionHeader().
                                getQuestionChildren().
                                get(children.getChildIndex()).
                                setAnswer(charSequence.toString());
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });

                ((ChildrenHolder) holder).etAnswer.setText(headersList.get(children.getHeaderIndex()).
                        getQuestionHeader().
                        getQuestionChildren().
                        get(children.getChildIndex()).
                        getAnswer());

                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (null != childrenList) {
            return childrenList.size();
        }

        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return childrenList.get(position).getItemType();
    }

    public ArrayList<ItemScreeningQuestion> getHeaderList() {
        return headersList;
    }

    class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView  tvQuestionGroup;
        private ImageView ivArrow;

        HeaderHolder(View itemView) {
            super(itemView);
            tvQuestionGroup = (TextView) itemView.findViewById(R.id.tv_title);
            ivArrow = (ImageView) itemView.findViewById(R.id.iv_arrow);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            final ItemQuestionHeader itemHeader = childrenList.get(getAdapterPosition()).getQuestionHeader();

            final ItemQuestionHeader header = headersList.get(itemHeader.getHeaderIndex()).getQuestionHeader();

            if (header.isChildrenShown()) {
                // Collapse
                int itemsToRemove = header.getQuestionChildren().size();

                int index = 0;

                while (itemsToRemove > 0) {

                    if (TYPE_CHILD == childrenList.get(index).getItemType()
                            && itemHeader.getHeaderIndex() == childrenList.get(index).getQuestionChildren().getHeaderIndex()) {

                        childrenList.remove(index);
                        notifyItemRemoved(index);
                        --itemsToRemove;

                    } else {
                        ++index;
                    }
                }

                ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_down));
                header.setChildrenShown(false);

            } else {
                // Expand
                ArrayList<ItemQuestionChildren> questionChildren = header.getQuestionChildren();

                int position = getAdapterPosition();

                for (ItemQuestionChildren children : questionChildren) {
                    childrenList.add(position + 1, new ItemScreeningQuestion(TYPE_CHILD, null, children));
                    notifyItemInserted(position + 1);
                    ++position;
                }

                ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_up));
                header.setChildrenShown(true);
            }
        }
    }

    class ChildrenHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestion;
        private EditText etAnswer;

        ChildrenHolder(View itemView) {
            super(itemView);
            tvQuestion = (TextView) itemView.findViewById(R.id.tv_question);
            etAnswer = (EditText) itemView.findViewById(R.id.et_answer);
        }
    }
}