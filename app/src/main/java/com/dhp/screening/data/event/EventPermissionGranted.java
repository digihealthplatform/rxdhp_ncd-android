package com.dhp.screening.data.event;


public class EventPermissionGranted {

    private boolean isPermissionGranted;
    private int     action;

    public EventPermissionGranted(int action, boolean isPermissionGranted) {
        this.action = action;
        this.isPermissionGranted = isPermissionGranted;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public boolean isPermissionGranted() {
        return isPermissionGranted;
    }

    public void setPermissionGranted(boolean permissionGranted) {
        isPermissionGranted = permissionGranted;
    }
}