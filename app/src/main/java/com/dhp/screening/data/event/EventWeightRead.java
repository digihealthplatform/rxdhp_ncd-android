package com.dhp.screening.data.event;


public class EventWeightRead {

    private String weight;

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}