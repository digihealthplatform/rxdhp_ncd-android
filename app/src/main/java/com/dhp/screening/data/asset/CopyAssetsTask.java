package com.dhp.screening.data.asset;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Environment;

import com.dhp.screening.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class CopyAssetsTask extends AsyncTask<String, Integer, String> {

    private ProgressDialog progressDialog;

    private Context context;

    private boolean copySuccess;

    private CallBack callBack;

    public interface CallBack {
        void onAssetCopied(boolean copySuccess);
    }

    public CopyAssetsTask(Context context, CallBack callBack) {
        this.context = context;
        this.callBack = callBack;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(context.getString(R.string.message_updating_app));
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            copyAssets();
            copySuccess = true;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }


    private void copyAssets() throws IOException {

        AssetManager assetManager = context.getAssets();

        String[] files = assetManager.list("");

        for (String fileName : files) {

            if (fileName.endsWith(".txt") || fileName.endsWith(".pdf") || fileName.endsWith(".ktpo") || fileName.endsWith(".json")) {
                copyFile(fileName, context.getString(R.string.input_json_folder));

            } else if (fileName.endsWith(".png") || fileName.endsWith(".jpg") || fileName.endsWith(".jpeg")) {
                copyFile(fileName, context.getString(R.string.icons_folder));

            } else if (fileName.endsWith(".mp3")) {
                copyFile(fileName, context.getString(R.string.audio_folder));
            }
        }
    }

    private void copyFile(String fileName, String filePath) throws IOException {

        AssetManager assetManager = context.getAssets();

        File file = new File(Environment.getExternalStorageDirectory(), filePath + fileName);

        if (file.exists()) {
            file.delete();
        }

        file = new File(Environment.getExternalStorageDirectory(), filePath + fileName);
        OutputStream out = new FileOutputStream(file);

        InputStream in = assetManager.open(fileName);

        byte[] buffer = new byte[1024];

        int read;

        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }

        in.close();
        out.flush();
        out.close();
    }

    @Override
    protected void onPostExecute(String result) {
        progressDialog.dismiss();
        callBack.onAssetCopied(copySuccess);
    }
}