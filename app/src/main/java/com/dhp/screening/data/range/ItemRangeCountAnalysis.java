package com.dhp.screening.data.range;

public class ItemRangeCountAnalysis {

    private String range = "";
    private ItemAnalysisCount maleAnalysis;
    private ItemAnalysisCount femaleAnalysis;

    public ItemRangeCountAnalysis(String range) {
        this.range = range;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public ItemAnalysisCount getMaleAnalysis() {
        return maleAnalysis;
    }

    public void setMaleAnalysis(ItemAnalysisCount maleAnalysis) {
        this.maleAnalysis = maleAnalysis;
    }

    public ItemAnalysisCount getFemaleAnalysis() {
        return femaleAnalysis;
    }

    public void setFemaleAnalysis(ItemAnalysisCount femaleAnalysis) {
        this.femaleAnalysis = femaleAnalysis;
    }
}