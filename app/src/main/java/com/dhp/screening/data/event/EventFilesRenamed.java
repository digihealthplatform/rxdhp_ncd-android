package com.dhp.screening.data.event;

import com.dhp.screening.data.item.ItemFileAttachment;

import java.util.ArrayList;


public class EventFilesRenamed {

    private ArrayList<ItemFileAttachment> outputFilePaths;

    public ArrayList<ItemFileAttachment> getOutputFilePaths() {
        return outputFilePaths;
    }

    public void setOutputFilePaths(ArrayList<ItemFileAttachment> outputFilePaths) {
        this.outputFilePaths = outputFilePaths;
    }
}