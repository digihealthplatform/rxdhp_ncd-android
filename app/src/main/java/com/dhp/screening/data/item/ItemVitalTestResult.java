package com.dhp.screening.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemVitalTestResult {

    @SerializedName("title")
    private String title;

    @SerializedName("value")
    private String value;

    @SerializedName("image")
    private String image;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}