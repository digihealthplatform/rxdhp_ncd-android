package com.dhp.screening.data.merger;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.dhp.screening.LApplication;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableProblems;
import com.dhp.screening.data.table.TableSociodemographics;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LToast;

import es.dmoral.toasty.Toasty;

import static com.dhp.screening.util.LUtils.toBoolean;


public class ImportData {

    public static void mergeDb(Context context, DataManager dataManager, String fromPath, int mergeReplace) {
        boolean success = ImportDataEncrypted.mergeDb(context, dataManager, fromPath, mergeReplace);

        if (!success) {
            try {
                SQLiteDatabase oldDb = SQLiteDatabase.openDatabase(fromPath, null, SQLiteDatabase.OPEN_READONLY);

                if (1 == mergeReplace) {
                    dataManager.clearDatabase();
                }

                int version = oldDb.getVersion();

                if (3 == version) {
                    ImportDataV4.mergeDb(dataManager, oldDb, mergeReplace);
                    return;
                }

                mergeCampInfo(dataManager, oldDb);
                mergeUsers(dataManager, oldDb);
                mergeDemographics(dataManager, oldDb);
                mergeSociodemographics(dataManager, oldDb);
                mergeProblems(dataManager, oldDb);
                mergeHouse(dataManager, oldDb);
                mergeDeathInfo(dataManager, oldDb);
                mergeVitals(dataManager, oldDb);
                LToast.success("Imported successfully");

                oldDb.close();

            } catch (Exception e) {
                Toasty.error(LApplication.getContext(), "Something went wrong. Please try again.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private static void mergeUsers(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableUser", null);

        if (cursor.moveToFirst()) {
            do {
                TableUser tableUser = new TableUser();

                tableUser.setUserId(cursor.getString(1));
                tableUser.setFullName(cursor.getString(2));
                tableUser.setPassword(cursor.getString(3));
                tableUser.setUserType(cursor.getString(4));
                tableUser.setGender(cursor.getString(5));
                tableUser.setDob(cursor.getString(6));
                tableUser.setYob(cursor.getInt(7));
                tableUser.setIdentityType(cursor.getString(8));
                tableUser.setIdentityNumber(cursor.getString(9));
                tableUser.setPhoneNumber(cursor.getString(10));
                tableUser.setEmailId(cursor.getString(11));
                tableUser.setHouseNo(cursor.getString(12));
                tableUser.setStreet(cursor.getString(13));
                tableUser.setPost(cursor.getString(14));
                tableUser.setLocality(cursor.getString(15));
                tableUser.setDistrict(cursor.getString(16));
                tableUser.setState(cursor.getString(17));
                tableUser.setPinCode(cursor.getInt(18));
                tableUser.setAllowPrescription(toBoolean(cursor.getInt(19)));
                tableUser.setDemoUser(toBoolean(cursor.getInt(20)));
                tableUser.setQrScanned(toBoolean(cursor.getInt(21)));

                tableUser.setPartnerId(cursor.getString(22));
                tableUser.setCampName(cursor.getString(23));
                tableUser.setDeviceNumber(cursor.getString(24));

                tableUser.setDeviceId(cursor.getString(25));
                tableUser.setEntryDateTime(cursor.getString(26));
                tableUser.setEntryDateTimeMillis(cursor.getLong(27));
                tableUser.setInactive(toBoolean(cursor.getInt(28)));
                tableUser.setInactiveReason(cursor.getString(29));
                tableUser.setModified(toBoolean(cursor.getInt(30)));
                tableUser.setModifiedDateTime(cursor.getString(31));
                tableUser.setSynced(toBoolean(cursor.getInt(32)));

                dataManager.merge(tableUser);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeDemographics(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableDemographics", null);

        if (cursor.moveToFirst()) {
            do {
                TableDemographics tableDemographics = new TableDemographics();
                tableDemographics.setRegistrationId(cursor.getString(1));
                tableDemographics.setFullName(cursor.getString(2));
                tableDemographics.setGender(cursor.getString(3));
                tableDemographics.setYob(cursor.getInt(4));
                tableDemographics.setDob(cursor.getString(5));
                tableDemographics.setPhoneNumber(cursor.getString(6));
                tableDemographics.setEmailId(cursor.getString(7));
                tableDemographics.setIdentityType(cursor.getString(8));
                tableDemographics.setIdentityNumber(cursor.getString(9));
                tableDemographics.setHouseId(cursor.getString(10));
                tableDemographics.setCareOf(cursor.getString(11));
                tableDemographics.setNotes(cursor.getString(12));
                tableDemographics.setDoorToDoor(toBoolean(cursor.getInt(13)));
                tableDemographics.setGpsLocation(cursor.getString(14));
                tableDemographics.setQrScanned(toBoolean(cursor.getInt(15)));
                tableDemographics.setSurveyorUserId(cursor.getString(16));
                tableDemographics.setPartnerId(cursor.getString(17));
                tableDemographics.setCampName(cursor.getString(18));
                tableDemographics.setDeviceNumber(cursor.getString(19));
                tableDemographics.setDeviceId(cursor.getString(20));
                tableDemographics.setSurveyDateTime(cursor.getString(21));
                tableDemographics.setSurveyDateTimeMillis(cursor.getLong(22));
                tableDemographics.setInactive(toBoolean(cursor.getInt(23)));
                tableDemographics.setInactiveReason(cursor.getString(24));
                tableDemographics.setModified(toBoolean(cursor.getInt(25)));
                tableDemographics.setModifiedDateTime(cursor.getString(26));
                tableDemographics.setSynced(toBoolean(cursor.getInt(27)));

                dataManager.merge(tableDemographics);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeSociodemographics(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableSociodemographics", null);

        if (cursor.moveToFirst()) {
            do {
                TableSociodemographics tableSociodemographics = new TableSociodemographics();

                tableSociodemographics.setRegistrationId(cursor.getString(1));
                tableSociodemographics.setRelationToHoh(cursor.getString(2));
                tableSociodemographics.setMaritalStatus(cursor.getString(3));
                tableSociodemographics.setEducation(cursor.getString(4));
                tableSociodemographics.setLiteracy(cursor.getString(5));
                tableSociodemographics.setCurrentOccupation(cursor.getString(6));
                tableSociodemographics.setLanguagesSpoken(cursor.getString(7));
                tableSociodemographics.setHabits(cursor.getString(8));

                tableSociodemographics.setExercise(cursor.getString(9));
                tableSociodemographics.setDisease(cursor.getString(10));
                tableSociodemographics.setDiseaseTreatment(cursor.getString(11));
                tableSociodemographics.setDisability(cursor.getString(12));
                tableSociodemographics.setSick(cursor.getString(13));
                tableSociodemographics.setAilment(cursor.getString(14));

                tableSociodemographics.setNotes(cursor.getString(15));
                tableSociodemographics.setSurveyorUserId(cursor.getString(16));
                tableSociodemographics.setPartnerId(cursor.getString(17));
                tableSociodemographics.setCampName(cursor.getString(18));
                tableSociodemographics.setDeviceNumber(cursor.getString(19));
                tableSociodemographics.setDeviceId(cursor.getString(20));
                tableSociodemographics.setSurveyDateTime(cursor.getString(21));
                tableSociodemographics.setSurveyDateTimeMillis(cursor.getLong(22));
                tableSociodemographics.setInactive(toBoolean(cursor.getInt(23)));
                tableSociodemographics.setInactiveReason(cursor.getString(24));

                tableSociodemographics.setModified(toBoolean(cursor.getInt(25)));
                tableSociodemographics.setModifiedDateTime(cursor.getString(26));
                tableSociodemographics.setSynced(toBoolean(cursor.getInt(27)));

                dataManager.merge(tableSociodemographics);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeProblems(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableProblems", null);

        if (cursor.moveToFirst()) {
            do {
                TableProblems tableProblems = new TableProblems();
                tableProblems.setRegistrationId(cursor.getString(1));
                tableProblems.setQuestionGroup(cursor.getString(2));
                tableProblems.setQuestion(cursor.getString(3));
                tableProblems.setAnswer(cursor.getString(4));
                tableProblems.setSurveyorUserId(cursor.getString(5));
                tableProblems.setPartnerId(cursor.getString(6));
                tableProblems.setCampName(cursor.getString(7));
                tableProblems.setDeviceNumber(cursor.getString(8));
                tableProblems.setDeviceId(cursor.getString(9));
                tableProblems.setSurveyDateTime(cursor.getString(10));
                tableProblems.setSurveyDateTimeMillis(cursor.getLong(11));
                tableProblems.setInactive(toBoolean(cursor.getInt(12)));
                tableProblems.setInactiveReason(cursor.getString(13));
                tableProblems.setModified(toBoolean(cursor.getInt(14)));
                tableProblems.setModifiedDateTime(cursor.getString(15));
                tableProblems.setSynced(toBoolean(cursor.getInt(16)));

                dataManager.merge(tableProblems);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeHouse(DataManager dataManager, SQLiteDatabase oldDb) {

        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableHouse", null);

        if (cursor.moveToFirst()) {
            do {
                TableHouse tableHouse = new TableHouse();

                tableHouse.setHouseId(cursor.getString(1));
                tableHouse.setHouseNo(cursor.getString(2));
                tableHouse.setStreet(cursor.getString(3));
                tableHouse.setPost(cursor.getString(4));
                tableHouse.setLocality(cursor.getString(5));
                tableHouse.setDistrict(cursor.getString(6));
                tableHouse.setState(cursor.getString(7));
                tableHouse.setPinCode(cursor.getInt(8));
                tableHouse.setHeadOfHouse(cursor.getString(9));
                tableHouse.setRespondent(cursor.getString(10));
                tableHouse.setTotalMembers(cursor.getInt(11));
                tableHouse.setHouseOwnership(cursor.getString(12));
                tableHouse.setRationCard(cursor.getString(13));
                tableHouse.setHealthInsurance(cursor.getString(14));
                tableHouse.setMealTimes(cursor.getString(15));
                tableHouse.setCookingFuel(cursor.getString(16));
                tableHouse.setTreatmentPlace(cursor.getString(17));
                tableHouse.setRecentDeath(cursor.getString(18));
                tableHouse.setNotes(cursor.getString(19));
                tableHouse.setGpsLocation(cursor.getString(20));
                tableHouse.setSurveyorUserId(cursor.getString(21));
                tableHouse.setPartnerId(cursor.getString(22));
                tableHouse.setCampName(cursor.getString(23));
                tableHouse.setDeviceNumber(cursor.getString(24));
                tableHouse.setDeviceId(cursor.getString(25));
                tableHouse.setSurveyDateTime(cursor.getString(26));
                tableHouse.setSurveyDateTimeMillis(cursor.getLong(27));
                tableHouse.setInactive(toBoolean(cursor.getInt(28)));
                tableHouse.setInactiveReason(cursor.getString(29));
                tableHouse.setModified(toBoolean(cursor.getInt(30)));
                tableHouse.setModifiedDateTime(cursor.getString(31));
                tableHouse.setSynced(toBoolean(cursor.getInt(32)));

                dataManager.merge(tableHouse);

            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private static void mergeDeathInfo(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableDeathInfo", null);

        if (cursor.moveToFirst()) {
            do {
                TableDeathInfo tableDeathInfo = new TableDeathInfo();
                tableDeathInfo.setHouseId(cursor.getString(1));
                tableDeathInfo.setPersonName(cursor.getString(2));
                tableDeathInfo.setGender(cursor.getString(3));
                tableDeathInfo.setAge(cursor.getInt(4));
                tableDeathInfo.setAgeUnit(cursor.getString(5));
                tableDeathInfo.setRelationToHoh(cursor.getString(6));
                tableDeathInfo.setDeathCause(cursor.getString(7));
                tableDeathInfo.setNotes(cursor.getString(8));
                tableDeathInfo.setSurveyorUserId(cursor.getString(9));
                tableDeathInfo.setPartnerId(cursor.getString(10));
                tableDeathInfo.setCampName(cursor.getString(11));
                tableDeathInfo.setDeviceNumber(cursor.getString(12));
                tableDeathInfo.setDeviceId(cursor.getString(13));
                tableDeathInfo.setSurveyDateTime(cursor.getString(14));
                tableDeathInfo.setSurveyDateTimeMillis(cursor.getLong(15));
                tableDeathInfo.setInactive(toBoolean(cursor.getInt(16)));
                tableDeathInfo.setInactiveReason(cursor.getString(17));
                tableDeathInfo.setModified(toBoolean(cursor.getInt(18)));
                tableDeathInfo.setModifiedDateTime(cursor.getString(19));
                tableDeathInfo.setSynced(toBoolean(cursor.getInt(20)));

                dataManager.merge(tableDeathInfo);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeVitals(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableVitals", null);

        if (cursor.moveToFirst()) {
            do {
                TableVitals tableVitals = new TableVitals();

                tableVitals.setRegistrationId(cursor.getString(1));
                tableVitals.setVitalType(cursor.getString(2));
                tableVitals.setVitalSubTypes(cursor.getString(3));
                tableVitals.setVitalValues(cursor.getString(4));
                tableVitals.setVitalUnits(cursor.getString(5));
                tableVitals.setVitalDateTime(cursor.getString(6));
                tableVitals.setVitalDateTimeMillis(cursor.getLong(7));
                tableVitals.setVitalImage(cursor.getString(8));
                tableVitals.setNotes(cursor.getString(9));
                tableVitals.setDeviceName(cursor.getString(10));
                tableVitals.setGpsLocation(cursor.getString(11));
                tableVitals.setQrScanned(toBoolean(cursor.getInt(12)));
                tableVitals.setSurveyorUserId(cursor.getString(13));
                tableVitals.setPartnerId(cursor.getString(14));
                tableVitals.setCampName(cursor.getString(15));
                tableVitals.setDeviceNumber(cursor.getString(16));
                tableVitals.setDeviceId(cursor.getString(17));
                tableVitals.setSurveyDateTime(cursor.getString(18));
                tableVitals.setSurveyDateTimeMillis(cursor.getLong(19));
                tableVitals.setInactive(toBoolean(cursor.getInt(20)));
                tableVitals.setInactiveReason(cursor.getString(21));
                tableVitals.setModified(toBoolean(cursor.getInt(22)));
                tableVitals.setModifiedDateTime(cursor.getString(23));
                tableVitals.setSynced(toBoolean(cursor.getInt(24)));

                dataManager.merge(tableVitals);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeCampInfo(DataManager dataManager, android.database.sqlite.SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableCampInfo", null);

        if (cursor.moveToFirst()) {
            do {
                TableCampInfo tableCampInfo = new TableCampInfo();

                tableCampInfo.setCampName(cursor.getString(1));
                tableCampInfo.setPartnerId(cursor.getString(2));
                tableCampInfo.setStartDate(cursor.getString(3));
                tableCampInfo.setEndDate(cursor.getString(4));
                tableCampInfo.setDoorToDoor(toBoolean(cursor.getInt(5)));
                tableCampInfo.setDeviceId(cursor.getString(6));
                tableCampInfo.setEntryDateTime(cursor.getString(7));
                tableCampInfo.setEntryDateTimeMillis(cursor.getLong(8));
                tableCampInfo.setModified(toBoolean(cursor.getInt(9)));
                tableCampInfo.setModifiedDateTime(cursor.getString(10));
                tableCampInfo.setSynced(toBoolean(cursor.getInt(11)));

                tableCampInfo.save();

            } while (cursor.moveToNext());
        }

        cursor.close();
    }
}