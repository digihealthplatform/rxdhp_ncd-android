package com.dhp.screening.data.table;

import com.dhp.screening.data.manager.LDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

@Table(database = LDatabase.class)
public class TableImage extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "registration_id")
    private String registrationId = "";

    @Column(name = "ehr_number")
    private String ehrNumber = "";

    @Column(name = "image_type")
    private String imageType = "";

    @Column(name = "image_name")
    private String imageName = "";

    @Column(name = "gps_location")
    private String gpsLocation = "";

    @Column(name = "notes")
    private String notes = "";

    @Column(name = "surveyor_user_id")
    private String surveyorUserId = "";

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "device_number")
    private String deviceNumber = "";

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "survey_date_time")
    private String surveyDateTime = "";

    @Column(name = "survey_date_time_millis")
    private long surveyDateTimeMillis;

    @Column(name = "inactive")
    private boolean inactive;

    @Column(name = "inactive_reason")
    private String inactiveReason = "";

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    @Column(name = "image_date_time")
    private String imageDateTime = "";

    @Column(name = "image_date_time_millis")
    private long imageDateTimeMillis;

    @Column(name = "image_camera")
    private boolean imageCamera;

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getEhrNumber() {
        return ehrNumber;
    }

    public void setEhrNumber(String ehrNumber) {
        this.ehrNumber = ehrNumber;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getImageDateTime() {
        return imageDateTime;
    }

    public void setImageDateTime(String imageDateTime) {
        this.imageDateTime = imageDateTime;
    }

    public long getImageDateTimeMillis() {
        return imageDateTimeMillis;
    }

    public void setImageDateTimeMillis(long imageDateTimeMillis) {
        this.imageDateTimeMillis = imageDateTimeMillis;
    }

    public boolean isImageCamera() {
        return imageCamera;
    }

    public void setImageCamera(boolean imageCamera) {
        this.imageCamera = imageCamera;
    }
}