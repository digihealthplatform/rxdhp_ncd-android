package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.dhp.screening.R;
import com.dhp.screening.data.item.ItemVitalRange;

import static com.dhp.screening.data.item.ItemVitalRange.DIVIDER;
import static com.dhp.screening.data.item.ItemVitalRange.HEADER;
import static com.dhp.screening.data.item.ItemVitalRange.HEADER_EMPTY;
import static com.dhp.screening.data.item.ItemVitalRange.RANGE;
import static com.dhp.screening.data.item.ItemVitalRange.VALUE;
import static com.dhp.screening.data.item.ItemVitalRange.VALUE_MULTIPLE;
import static com.dhp.screening.util.LUtils.RANGE_COLORS;

public class AdapterVitalsRange extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final AdapterRangeColor adapterColors;

    private final DecimalFormat decimalFormat;

    private ArrayList<ItemVitalRange> vitalRanges;

    private final LayoutInflater layoutInflater;

    private static final String TAG = "AdapterVitalsRange";

    public AdapterVitalsRange(Context context) {
        layoutInflater = LayoutInflater.from(context);

        adapterColors = new AdapterRangeColor(context, RANGE_COLORS);
//        adapterColors.setSelectedPosition(currentFontColorIndex);

        decimalFormat = new DecimalFormat();
    }

    public void setItems(ArrayList<ItemVitalRange> vitalRanges) {
        this.vitalRanges = vitalRanges;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        switch (viewType) {
            case HEADER: {
                view = layoutInflater.inflate(R.layout.item_vital_range_header, parent, false);
                return new ViewHolderHeader(view);
            }

            case HEADER_EMPTY: {
                view = layoutInflater.inflate(R.layout.item_vital_range_header_empty, parent, false);
                return new ViewHolderHeader(view);
            }

            case RANGE: {
                view = layoutInflater.inflate(R.layout.item_vital_range, parent, false);
                return new ViewHolderRange(view);
            }

            case VALUE: {
                view = layoutInflater.inflate(R.layout.item_vital_range_value, parent, false);
                return new ViewHolderValue(view);
            }

            case VALUE_MULTIPLE: {
                view = layoutInflater.inflate(R.layout.item_vital_range_value_multiple, parent, false);
                return new ViewHolderValueMultiple(view);
            }

            case DIVIDER: {
                view = layoutInflater.inflate(R.layout.item_vital_range_divider, parent, false);
                return new ViewHolderDivider(view);
            }
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case HEADER: {
                ((ViewHolderHeader) holder).tvHeader.setText(vitalRanges.get(position).getHeader() + ":");
                break;
            }

            case HEADER_EMPTY: {
                ((ViewHolderHeader) holder).tvHeader.setText(vitalRanges.get(position).getHeader() + ":");
                break;
            }

            case RANGE: {
                ((ViewHolderRange) holder).tvHeader.setText(vitalRanges.get(position).getHeader());
                break;
            }

            case VALUE: {
                ((ViewHolderValue) holder).tvRange.setText(vitalRanges.get(position).getRange());

                if (vitalRanges.get(position).getMin() > 0.0) {
                    ((ViewHolderValue) holder).etMin.setText(decimalFormat.format(vitalRanges.get(position).getMin()));
                }

                if (vitalRanges.get(position).getMax() > 0.0) {
                    ((ViewHolderValue) holder).etMax.setText(decimalFormat.format(vitalRanges.get(position).getMax()));
                }

                ((ViewHolderValue) holder).etMin.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        try {
                            vitalRanges.get(position).setMin(Double.valueOf(charSequence.toString()));

                        } catch (Exception e) {
                            vitalRanges.get(position).setMin(0);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                ((ViewHolderValue) holder).etMax.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        try {
                            vitalRanges.get(position).setMax(Double.valueOf(charSequence.toString()));

                        } catch (Exception e) {
                            vitalRanges.get(position).setMax(0);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                ((ViewHolderValue) holder).spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int spinnerPosition, long id) {
                        if (spinnerPosition > 0) {
                            vitalRanges.get(holder.getAdapterPosition()).setColor(spinnerPosition);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                ((ViewHolderValue) holder).spColor.setSelection(vitalRanges.get(position).getColor());

                break;
            }

            case VALUE_MULTIPLE: {

                double min1 = vitalRanges.get(position).getValueMultiple().getValueMin1();
                double max1 = vitalRanges.get(position).getValueMultiple().getValueMax1();
                double min2 = vitalRanges.get(position).getValueMultiple().getValueMin2();
                double max2 = vitalRanges.get(position).getValueMultiple().getValueMax2();

                if (min1 > 0.0) {
                    ((ViewHolderValueMultiple) holder).etMin1.setText(decimalFormat.format(min1));
                }

                if (max1 > 0.0) {
                    ((ViewHolderValueMultiple) holder).etMax1.setText(decimalFormat.format(max1));
                }

                if (min2 > 0.0) {
                    ((ViewHolderValueMultiple) holder).etMin2.setText(decimalFormat.format(min2));
                }

                if (max2 > 0.0) {
                    ((ViewHolderValueMultiple) holder).etMax2.setText(decimalFormat.format(max2));
                }

                ((ViewHolderValueMultiple) holder).etMin1.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        try {
                            vitalRanges.get(position).getValueMultiple().setValueMin1(Double.valueOf(charSequence.toString()));

                        } catch (Exception e) {
                            vitalRanges.get(position).getValueMultiple().setValueMin1(0);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                ((ViewHolderValueMultiple) holder).etMax1.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        try {
                            vitalRanges.get(position).getValueMultiple().setValueMax1(Double.valueOf(charSequence.toString()));

                        } catch (Exception e) {
                            vitalRanges.get(position).getValueMultiple().setValueMax1(0);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                ((ViewHolderValueMultiple) holder).etMin2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        try {
                            vitalRanges.get(position).getValueMultiple().setValueMin2(Double.valueOf(charSequence.toString()));

                        } catch (Exception e) {
                            vitalRanges.get(position).getValueMultiple().setValueMin2(0);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });


                ((ViewHolderValueMultiple) holder).etMax2.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        try {
                            vitalRanges.get(position).getValueMultiple().setValueMax2(Double.valueOf(charSequence.toString()));

                        } catch (Exception e) {
                            vitalRanges.get(position).getValueMultiple().setValueMax2(0);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                    }
                });

                ((ViewHolderValueMultiple) holder).spColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int spinnerPosition, long id) {
                        if (spinnerPosition > 0) {
                            vitalRanges.get(holder.getAdapterPosition()).setColor(spinnerPosition);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                ((ViewHolderValueMultiple) holder).spColor.setSelection(vitalRanges.get(position).getColor());

                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (null != vitalRanges) {
            return vitalRanges.size();
        }

        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return vitalRanges.get(position).getType();
    }

    public ArrayList<ItemVitalRange> getRangeList() {
        return vitalRanges;
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        private TextView tvHeader;

        ViewHolderHeader(View itemView) {
            super(itemView);
            tvHeader = (TextView) itemView.findViewById(R.id.tv_range_header);
        }
    }

    class ViewHolderRange extends RecyclerView.ViewHolder {
        private TextView tvHeader;

        ViewHolderRange(View itemView) {
            super(itemView);
            tvHeader = (TextView) itemView.findViewById(R.id.tv_range_header);
        }
    }

    class ViewHolderValue extends RecyclerView.ViewHolder {
        private final TextView tvRange;
        private final EditText etMin;
        private final EditText etMax;
        private final Spinner  spColor;

        ViewHolderValue(View itemView) {
            super(itemView);
            tvRange = (TextView) itemView.findViewById(R.id.tv_range);
            etMin = (EditText) itemView.findViewById(R.id.et_min);
            etMax = (EditText) itemView.findViewById(R.id.et_max);
            spColor = (Spinner) itemView.findViewById(R.id.sp_color);
            spColor.setAdapter(adapterColors);
        }
    }

    class ViewHolderValueMultiple extends RecyclerView.ViewHolder {
        private final TextView tvRange;
        private final EditText etMin1;
        private final EditText etMax1;
        private final EditText etMin2;
        private final EditText etMax2;
        private final Spinner  spColor;

        ViewHolderValueMultiple(View itemView) {
            super(itemView);
            tvRange = (TextView) itemView.findViewById(R.id.tv_range);
            etMin1 = (EditText) itemView.findViewById(R.id.et_min_1);
            etMax1 = (EditText) itemView.findViewById(R.id.et_max_1);
            etMin2 = (EditText) itemView.findViewById(R.id.et_min_2);
            etMax2 = (EditText) itemView.findViewById(R.id.et_max_2);
            spColor = (Spinner) itemView.findViewById(R.id.sp_color);
            spColor.setAdapter(adapterColors);
        }
    }

    class ViewHolderDivider extends RecyclerView.ViewHolder {

        ViewHolderDivider(View itemView) {
            super(itemView);
        }
    }
}