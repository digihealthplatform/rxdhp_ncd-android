package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.dhp.screening.R;

import static com.dhp.screening.util.LUtils.getColorValue;

public class AdapterRangeColor extends ArrayAdapter<Integer> {

    private final int SIX_DP;
    private final int THREE_DP;
    private final int TWO_DP;

    private int selectedPosition = -1;

    private final LayoutInflater inflater;

    private Integer[] imageArray;

    public AdapterRangeColor(Context context, Integer[] imageArray) {
        super(context, R.layout.spinner_colors, imageArray);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.imageArray = imageArray;
        float scale = getContext().getResources().getDisplayMetrics().density;

        SIX_DP = (int) (6 * scale + 0.5f);
        THREE_DP = (int) (3 * scale + 0.5f);
        TWO_DP = (int) (2 * scale + 0.5f);
    }

    @Override
    public boolean isEnabled(int position) {
        return position != 0;
    }

    public void setSelectedPosition(int position) {
        this.selectedPosition = position;
    }

    @Override
    public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;

        if (null == convertView) {
            row = inflater.inflate(R.layout.spinner_colors, parent, false);
        }

        View view = (View) row.findViewById(R.id.view);
        view.setBackgroundColor(getContext().getResources().getColor(imageArray[position]));

        ImageView selected    = (ImageView) row.findViewById(R.id.iv_selected);
        ImageView colorPicker = (ImageView) row.findViewById(R.id.iv_color);

        if (position == selectedPosition) {
            selected.setVisibility(View.VISIBLE);
        } else {
            selected.setVisibility(View.GONE);
        }

        selected.setColorFilter(getColorValue(R.color.white));

        if (0 == position) {
            row.setPadding(SIX_DP, SIX_DP, SIX_DP, THREE_DP);
            selected.setPadding(TWO_DP, THREE_DP, TWO_DP, 0);

        } else if (imageArray.length - 1 == position) {
            row.setPadding(SIX_DP, THREE_DP, SIX_DP, SIX_DP);
            selected.setPadding(TWO_DP, 0, TWO_DP, THREE_DP);

        } else {
            row.setPadding(SIX_DP, THREE_DP, SIX_DP, THREE_DP);
            selected.setPadding(TWO_DP, 0, TWO_DP, 0);
        }

        if (position == 0) {
            colorPicker.setVisibility(View.VISIBLE);
        } else {
            colorPicker.setVisibility(View.GONE);
        }

        return row;
    }
}