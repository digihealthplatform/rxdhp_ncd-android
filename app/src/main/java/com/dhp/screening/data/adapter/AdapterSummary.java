package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import com.dhp.screening.R;
import com.dhp.screening.data.item.ItemSummaryView;
import com.dhp.screening.data.range.ItemRangeCount;

import static com.dhp.screening.data.item.ItemSummaryView.TYPE_DIVIDER;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_HEADER;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_BMI;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_BP;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_HB;
import static com.dhp.screening.data.item.ItemSummaryView.TYPE_SUMMARY_VITAL_SUGAR;

public class AdapterSummary extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ItemSummaryView> summaryViews;

    private final LayoutInflater layoutInflater;

    private static final String TAG = "AdapterSummary";

    public AdapterSummary(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(ArrayList<ItemSummaryView> summaryViews) {
        this.summaryViews = summaryViews;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        switch (viewType) {

            case TYPE_HEADER: {
                view = layoutInflater.inflate(R.layout.item_summary_header, parent, false);
                return new ViewHolderHeader(view);
            }

            case TYPE_SUMMARY: {
                view = layoutInflater.inflate(R.layout.item_summary, parent, false);
                return new ViewHolderSummary(view);
            }

            case TYPE_SUMMARY_VITAL: {
                view = layoutInflater.inflate(R.layout.item_summary_vital, parent, false);
                return new ViewHolderSummary(view);
            }

            case TYPE_SUMMARY_VITAL_BMI: {
                view = layoutInflater.inflate(R.layout.item_summary_vital_two_lines, parent, false);
                return new ViewHolderTwoLines(view);
            }

            case TYPE_SUMMARY_VITAL_BP: {
                view = layoutInflater.inflate(R.layout.item_summary_vital_bp, parent, false);
                return new ViewHolderBp(view);
            }

            case TYPE_SUMMARY_VITAL_SUGAR: {
                view = layoutInflater.inflate(R.layout.item_summary_vital_single_line, parent, false);
                return new ViewHolderSingleLine(view);
            }

            case TYPE_SUMMARY_VITAL_HB: {
                view = layoutInflater.inflate(R.layout.item_summary_vital_two_lines, parent, false);
                return new ViewHolderTwoLines(view);
            }

            case TYPE_DIVIDER: {
                view = layoutInflater.inflate(R.layout.item_vital_range_divider, parent, false);
                return new ViewHolderDivider(view);
            }
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case TYPE_HEADER: {
                ((ViewHolderHeader) holder).tvHeader.setText(summaryViews.get(position).getKey());
                break;
            }

            case TYPE_SUMMARY_VITAL:
            case TYPE_SUMMARY: {
                ((ViewHolderSummary) holder).tvKey.setText(summaryViews.get(position).getKey() + ":");
                ((ViewHolderSummary) holder).tvValue.setText(" " + summaryViews.get(position).getValue());
                break;
            }

            case TYPE_SUMMARY_VITAL_BMI: {
                ArrayList<ItemRangeCount> itemRangeCounts = summaryViews.get(position).getItemRangeCounts();

                ((ViewHolderTwoLines) holder).tvText1.setText(itemRangeCounts.get(0).getRange() + ": " + itemRangeCounts.get(0).getCount());
                ((ViewHolderTwoLines) holder).tvText2.setText(itemRangeCounts.get(1).getRange() + ": " + itemRangeCounts.get(1).getCount());
                ((ViewHolderTwoLines) holder).tvText3.setText(itemRangeCounts.get(2).getRange() + ": " + itemRangeCounts.get(2).getCount());
                ((ViewHolderTwoLines) holder).tvText4.setText(itemRangeCounts.get(3).getRange() + ": " + itemRangeCounts.get(3).getCount());
                break;
            }

            case TYPE_SUMMARY_VITAL_BP: {
                ArrayList<ItemRangeCount> itemRangeCounts = summaryViews.get(position).getItemRangeCounts();

                ((ViewHolderBp) holder).tvText1.setText(itemRangeCounts.get(0).getRange() + ": " + itemRangeCounts.get(0).getCount());
                ((ViewHolderBp) holder).tvText2.setText(itemRangeCounts.get(1).getRange() + ": " + itemRangeCounts.get(1).getCount());
                ((ViewHolderBp) holder).tvText3.setText(itemRangeCounts.get(2).getRange() + ": " + itemRangeCounts.get(2).getCount());
                ((ViewHolderBp) holder).tvText4.setText(itemRangeCounts.get(3).getRange() + ": " + itemRangeCounts.get(3).getCount());
                ((ViewHolderBp) holder).tvText5.setText(itemRangeCounts.get(4).getRange() + ": " + itemRangeCounts.get(4).getCount());
                break;
            }

            case TYPE_SUMMARY_VITAL_SUGAR: {
                StringBuilder text = new StringBuilder();

                for (ItemRangeCount itemRangeCount : summaryViews.get(position).getItemRangeCounts()) {

                    if (!text.toString().isEmpty()) {
                        text.append(",  ");
                    }
                    text.append("").append(itemRangeCount.getRange()).append(": ").append(itemRangeCount.getCount());
                }

                ((ViewHolderSingleLine) holder).tvText.setText(text.toString());
                break;
            }

            case TYPE_SUMMARY_VITAL_HB: {
                ArrayList<ItemRangeCount> itemRangeCounts = summaryViews.get(position).getItemRangeCounts();

                ((ViewHolderTwoLines) holder).tvText1.setText(itemRangeCounts.get(0).getRange() + ": " + itemRangeCounts.get(0).getCount());
                ((ViewHolderTwoLines) holder).tvText2.setText(itemRangeCounts.get(1).getRange() + ": " + itemRangeCounts.get(1).getCount());
                ((ViewHolderTwoLines) holder).tvText3.setText(itemRangeCounts.get(2).getRange() + ": " + itemRangeCounts.get(2).getCount());
                ((ViewHolderTwoLines) holder).tvText4.setText(itemRangeCounts.get(3).getRange() + ": " + itemRangeCounts.get(3).getCount());
                break;
            }

            case TYPE_DIVIDER: {
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (null != summaryViews) {
            return summaryViews.size();
        }

        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return summaryViews.get(position).getType();
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        private TextView tvHeader;

        ViewHolderHeader(View itemView) {
            super(itemView);
            tvHeader = (TextView) itemView.findViewById(R.id.tv_header);
        }
    }

    class ViewHolderSummary extends RecyclerView.ViewHolder {
        private TextView tvKey;
        private TextView tvValue;

        ViewHolderSummary(View itemView) {
            super(itemView);
            tvKey = (TextView) itemView.findViewById(R.id.tv_key);
            tvValue = (TextView) itemView.findViewById(R.id.tv_value);
        }
    }

    class ViewHolderSingleLine extends RecyclerView.ViewHolder {
        private TextView tvText;

        ViewHolderSingleLine(View itemView) {
            super(itemView);
            tvText = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }

    class ViewHolderTwoLines extends RecyclerView.ViewHolder {
        private TextView tvText1;
        private TextView tvText2;
        private TextView tvText3;
        private TextView tvText4;

        ViewHolderTwoLines(View itemView) {
            super(itemView);
            tvText1 = (TextView) itemView.findViewById(R.id.tv_text1);
            tvText2 = (TextView) itemView.findViewById(R.id.tv_text2);
            tvText3 = (TextView) itemView.findViewById(R.id.tv_text3);
            tvText4 = (TextView) itemView.findViewById(R.id.tv_text4);
        }
    }

    class ViewHolderBp extends RecyclerView.ViewHolder {
        private TextView tvText1;
        private TextView tvText2;
        private TextView tvText3;
        private TextView tvText4;
        private TextView tvText5;

        ViewHolderBp(View itemView) {
            super(itemView);
            tvText1 = (TextView) itemView.findViewById(R.id.tv_text1);
            tvText2 = (TextView) itemView.findViewById(R.id.tv_text2);
            tvText3 = (TextView) itemView.findViewById(R.id.tv_text3);
            tvText4 = (TextView) itemView.findViewById(R.id.tv_text4);
            tvText5 = (TextView) itemView.findViewById(R.id.tv_text5);
        }
    }

    class ViewHolderDivider extends RecyclerView.ViewHolder {

        ViewHolderDivider(View itemView) {
            super(itemView);
        }
    }
}