package com.dhp.screening.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemVitalTestStep {

    @SerializedName("image")
    private String image;

    @SerializedName("timer_duration")
    private int duration;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}