package com.dhp.screening.data.item;

import java.util.ArrayList;

import com.dhp.screening.data.range.ItemRangeCount;

public class ItemSummaryPrint {

    private String vital;

    private ArrayList<ItemRangeCount> ranges;

    public ItemSummaryPrint(String vital, ArrayList<ItemRangeCount> ranges) {
        this.vital = vital;
        this.ranges = ranges;
    }

    public String getVital() {
        return vital;
    }

    public void setVital(String vital) {
        this.vital = vital;
    }

    public ArrayList<ItemRangeCount> getRanges() {
        return ranges;
    }

    public void setRanges(ArrayList<ItemRangeCount> ranges) {
        this.ranges = ranges;
    }
}