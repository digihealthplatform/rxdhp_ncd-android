package com.dhp.screening.data.range;

import com.google.gson.annotations.SerializedName;

public class ItemHbAge {

    @SerializedName("age_min")
    private String ageMin;

    @SerializedName("age_max")
    private String ageMax;

    @SerializedName("ranges")
    private ItemHbRange[] itemHbRange;

    public String getAgeMin() {
        return ageMin;
    }

    public void setAgeMin(String ageMin) {
        this.ageMin = ageMin;
    }

    public String getAgeMax() {
        return ageMax;
    }

    public void setAgeMax(String ageMax) {
        this.ageMax = ageMax;
    }

    public ItemHbRange[] getItemHbRange() {
        return itemHbRange;
    }

    public void setItemHbRange(ItemHbRange[] itemHbRange) {
        this.itemHbRange = itemHbRange;
    }
}