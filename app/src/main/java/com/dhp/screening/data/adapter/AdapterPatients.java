package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.ui.patient.ActivityPatientDetails;
import com.dhp.screening.ui.patient.ActivityPatientSearch;
import com.dhp.screening.util.LUtils;

import java.util.List;

import static com.dhp.screening.util.LUtils.hasValue;

public class


AdapterPatients extends RecyclerView.Adapter<AdapterPatients.ViewHolder> {

    private       List<TableDemographics> patients;
    private final DataManager             dataManager;

    private final Context        context;
    private final LayoutInflater layoutInflater;

    public AdapterPatients(Context context, DataManager dataManager) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.dataManager = dataManager;

        refreshData();
    }

    public void refreshData() {
        this.patients = dataManager.getAllPatients();
        notifyDataSetChanged();
    }

    public void setData(List<TableDemographics> demographicsList) {
        this.patients = demographicsList;
        notifyDataSetChanged();
    }

    @Override
    public AdapterPatients.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_patient, parent, false);
        return new AdapterPatients.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterPatients.ViewHolder holder, int position) {
        TableDemographics patient = patients.get(position);
        holder.tvPatientName.setText(patient.getFullName());
        holder.tvPatientId.setText(AssetReader.getLangKeyword("reg_id") + "  " + patient.getRegistrationId());

        int age = LUtils.calculateAge(patient.getYob());

        if (0 < age) {
            holder.tvAge.setText(AssetReader.getLangKeyword("age") + ": " + age);
            holder.tvAge.setVisibility(View.VISIBLE);
        } else {
            holder.tvAge.setVisibility(View.GONE);
        }

        if (hasValue(patient.getPhoneNumber())) {
            holder.tvPhone.setText(AssetReader.getLangKeyword("phone_number") + " " + patient.getPhoneNumber());
            holder.tvPhone.setVisibility(View.VISIBLE);
        } else {
            holder.tvPhone.setVisibility(View.GONE);
        }

        if (patient.isInactive()) {
            holder.rlMain.setBackground(ContextCompat
                    .getDrawable(context, R.drawable.item_inactive));
        } else {
            holder.rlMain.setBackground(ContextCompat
                    .getDrawable(context, R.drawable.ripple_effect));
        }
    }

    @Override
    public int getItemCount() {
        if (null != patients) {
            return patients.size();
        }
        return 0;
    }

    public void searchPatient(String query) {
        patients = dataManager.searchPatients(query);
        notifyDataSetChanged();

        if (null == patients || 0 == patients.size()) {
            ((ActivityPatientSearch) context).onEmpty();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvPatientName;
        private TextView tvPatientId;
        private TextView tvAge;
        private TextView tvPhone;

        private RelativeLayout rlMain;

        ViewHolder(View itemView) {
            super(itemView);
            tvPatientName = (TextView) itemView.findViewById(R.id.tv_patient_name);
            tvPatientId = (TextView) itemView.findViewById(R.id.tv_patient_id);
            tvAge = (TextView) itemView.findViewById(R.id.tv_age);
            tvPhone = (TextView) itemView.findViewById(R.id.tv_phone);
            rlMain = (RelativeLayout) itemView.findViewById(R.id.rl_main);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            StaticData.setRegistrationId(patients.get(getAdapterPosition()).getRegistrationId());
            context.startActivity(new Intent(context, ActivityPatientDetails.class));
        }
    }
}