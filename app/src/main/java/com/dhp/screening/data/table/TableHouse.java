package com.dhp.screening.data.table;

import com.dhp.screening.data.manager.LDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

@Table(database = LDatabase.class)
public class TableHouse extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "house_id")
    private String houseId = "";

    @Column(name = "house_number")
    private String houseNo = "";

    @Column(name = "street")
    private String street = "";

    @Column(name = "post")
    private String post = "";

    @Column(name = "locality")
    private String locality = "";

    @Column(name = "district")
    private String district = "";

    @Column(name = "state")
    private String state = "";

    @Column(name = "country")
    private String country = "";

    @Column(name = "pincode")
    private int pinCode = 0;

    @Column(name = "head_of_house")
    private String headOfHouse = "";

    @Column(name = "respondent")
    private String respondent = "";

    @Column(name = "total_members")
    private int totalMembers = 0;

    @Column(name = "house_ownership")
    private String houseOwnership = "";

    @Column(name = "ration_card")
    private String rationCard = "";

    @Column(name = "health_insurance")
    private String healthInsurance = "";

    @Column(name = "meal_times")
    private String mealTimes = "";

    @Column(name = "cooking_fuel")
    private String cookingFuel = "";

    @Column(name = "treatment_place")
    private String treatmentPlace = "";

    @Column(name = "recent_death")
    private String recentDeath = "";

    @Column(name = "notes")
    private String notes = "";

    @Column(name = "gps_location")
    private String gpsLocation = "";

    @Column(name = "surveyor_user_id")
    private String surveyorUserId = "";

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "device_number")
    private String deviceNumber = "";

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "survey_date_time")
    private String surveyDateTime = "";

    @Column(name = "survey_date_time_millis")
    private long surveyDateTimeMillis;

    @Column(name = "inactive")
    private boolean inactive;

    @Column(name = "inactive_reason")
    private String inactiveReason = "";

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    @Column(name = "spouse_of_head_of_house")
    private String spouseOfHeadOfHouse = "";

    @Column(name = "house_status")
    private String houseStatus = "";

    @Column(name = "total_rooms")
    private int totalRooms = 0;

    @Column(name = "toilet_availability")
    private String toiletAvailability = "";

    @Column(name = "drinking_water_source")
    private String drinkingWaterSource = "";

    @Column(name = "electricity_availability")
    private String electricityAvailability = "";

    @Column(name = "motorised_vehicle")
    private String motorisedVehicle = "";

    @Column(name = "village")
    private String village = "";

    @Column(name = "gram_panchayat")
    private String gramPanchayat = "";

    @Column(name = "earning_members")
    private int earningMembers;

    @Column(name = "total_income")
    private int totalIncome;

    @Column(name = "treatment_cost")
    private int treatmentCost;

    @Column(name = "drink_weekly")
    private String drinkWeekly = "";

    @Column(name = "drugs_use")
    private String drugsUse = "";

    @Column(name = "handicapped_present")
    private String handicappedPresent = "";

    @Column(name = "handicapped_suffering")
    private String handicappedSuffering = "";

    @Column(name = "pets")
    private String pets = "";

    @Column(name = "pets_bitten")
    private String petsBitten = "";

    @Column(name = "pets_bite_action")
    private String petsBiteAction = "";

    @Column(name = "drinking_water_cleaning_method")
    private String drinkingWaterCleaningMethod = "";

    @Column(name = "bathing_toilet_facility")
    private String bathingToiletFacility = "";

    @Column(name = "child_health_checkup_last_done")
    private String childHealthCheckupLastDone = "";

    @Column(name = "children_vaccinated")
    private String childrenVaccinated = "";

    @Column(name = "vaccination_records_storage")
    private String vaccinationRecordsStorage = "";

    @Column(name = "child_gender_responsibility")
    private String childGenderResponsibility = "";

    @Column(name = "preferred_child_gender")
    private String preferredChildGender = "";

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPinCode() {
        return pinCode;
    }

    public void setPinCode(int pinCode) {
        this.pinCode = pinCode;
    }

    public String getHeadOfHouse() {
        return headOfHouse;
    }

    public void setHeadOfHouse(String headOfHouse) {
        this.headOfHouse = headOfHouse;
    }

    public String getRespondent() {
        return respondent;
    }

    public void setRespondent(String respondent) {
        this.respondent = respondent;
    }

    public int getTotalMembers() {
        return totalMembers;
    }

    public void setTotalMembers(int totalMembers) {
        this.totalMembers = totalMembers;
    }

    public String getHouseOwnership() {
        return houseOwnership;
    }

    public void setHouseOwnership(String houseOwnership) {
        this.houseOwnership = houseOwnership;
    }

    public String getRationCard() {
        return rationCard;
    }

    public void setRationCard(String rationCard) {
        this.rationCard = rationCard;
    }

    public String getHealthInsurance() {
        return healthInsurance;
    }

    public void setHealthInsurance(String healthInsurance) {
        this.healthInsurance = healthInsurance;
    }

    public String getMealTimes() {
        return mealTimes;
    }

    public void setMealTimes(String mealTimes) {
        this.mealTimes = mealTimes;
    }

    public String getCookingFuel() {
        return cookingFuel;
    }

    public void setCookingFuel(String cookingFuel) {
        this.cookingFuel = cookingFuel;
    }

    public String getTreatmentPlace() {
        return treatmentPlace;
    }

    public void setTreatmentPlace(String treatmentPlace) {
        this.treatmentPlace = treatmentPlace;
    }

    public String getRecentDeath() {
        return recentDeath;
    }

    public void setRecentDeath(String recentDeath) {
        this.recentDeath = recentDeath;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getSpouseOfHeadOfHouse() {
        return spouseOfHeadOfHouse;
    }

    public void setSpouseOfHeadOfHouse(String spouseOfHeadOfHouse) {
        this.spouseOfHeadOfHouse = spouseOfHeadOfHouse;
    }

    public String getHouseStatus() {
        return houseStatus;
    }

    public void setHouseStatus(String houseStatus) {
        this.houseStatus = houseStatus;
    }

    public int getTotalRooms() {
        return totalRooms;
    }

    public void setTotalRooms(int totalRooms) {
        this.totalRooms = totalRooms;
    }

    public String getToiletAvailability() {
        return toiletAvailability;
    }

    public void setToiletAvailability(String toiletAvailability) {
        this.toiletAvailability = toiletAvailability;
    }

    public String getDrinkingWaterSource() {
        return drinkingWaterSource;
    }

    public void setDrinkingWaterSource(String drinkingWaterSource) {
        this.drinkingWaterSource = drinkingWaterSource;
    }

    public String getElectricityAvailability() {
        return electricityAvailability;
    }

    public void setElectricityAvailability(String electricityAvailability) {
        this.electricityAvailability = electricityAvailability;
    }

    public String getMotorisedVehicle() {
        return motorisedVehicle;
    }

    public void setMotorisedVehicle(String motorisedVehicle) {
        this.motorisedVehicle = motorisedVehicle;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getGramPanchayat() {
        return gramPanchayat;
    }

    public void setGramPanchayat(String gramPanchayat) {
        this.gramPanchayat = gramPanchayat;
    }

    public int getEarningMembers() {
        return earningMembers;
    }

    public void setEarningMembers(int earningMembers) {
        this.earningMembers = earningMembers;
    }

    public int getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(int totalIncome) {
        this.totalIncome = totalIncome;
    }

    public int getTreatmentCost() {
        return treatmentCost;
    }

    public void setTreatmentCost(int treatmentCost) {
        this.treatmentCost = treatmentCost;
    }

    public String getDrinkWeekly() {
        return drinkWeekly;
    }

    public void setDrinkWeekly(String drinkWeekly) {
        this.drinkWeekly = drinkWeekly;
    }

    public String getDrugsUse() {
        return drugsUse;
    }

    public void setDrugsUse(String drugsUse) {
        this.drugsUse = drugsUse;
    }

    public String getHandicappedPresent() {
        return handicappedPresent;
    }

    public void setHandicappedPresent(String handicappedPresent) {
        this.handicappedPresent = handicappedPresent;
    }

    public String getHandicappedSuffering() {
        return handicappedSuffering;
    }

    public void setHandicappedSuffering(String handicappedSuffering) {
        this.handicappedSuffering = handicappedSuffering;
    }

    public String getPets() {
        return pets;
    }

    public void setPets(String pets) {
        this.pets = pets;
    }

    public String getPetsBitten() {
        return petsBitten;
    }

    public void setPetsBitten(String petsBitten) {
        this.petsBitten = petsBitten;
    }

    public String getPetsBiteAction() {
        return petsBiteAction;
    }

    public void setPetsBiteAction(String petsBiteAction) {
        this.petsBiteAction = petsBiteAction;
    }

    public String getDrinkingWaterCleaningMethod() {
        return drinkingWaterCleaningMethod;
    }

    public void setDrinkingWaterCleaningMethod(String drinkingWaterCleaningMethod) {
        this.drinkingWaterCleaningMethod = drinkingWaterCleaningMethod;
    }

    public String getBathingToiletFacility() {
        return bathingToiletFacility;
    }

    public void setBathingToiletFacility(String bathingToiletFacility) {
        this.bathingToiletFacility = bathingToiletFacility;
    }

    public String getChildHealthCheckupLastDone() {
        return childHealthCheckupLastDone;
    }

    public void setChildHealthCheckupLastDone(String childHealthCheckupLastDone) {
        this.childHealthCheckupLastDone = childHealthCheckupLastDone;
    }

    public String getChildrenVaccinated() {
        return childrenVaccinated;
    }

    public void setChildrenVaccinated(String childrenVaccinated) {
        this.childrenVaccinated = childrenVaccinated;
    }

    public String getVaccinationRecordsStorage() {
        return vaccinationRecordsStorage;
    }

    public void setVaccinationRecordsStorage(String vaccinationRecordsStorage) {
        this.vaccinationRecordsStorage = vaccinationRecordsStorage;
    }

    public String getChildGenderResponsibility() {
        return childGenderResponsibility;
    }

    public void setChildGenderResponsibility(String childGenderResponsibility) {
        this.childGenderResponsibility = childGenderResponsibility;
    }

    public String getPreferredChildGender() {
        return preferredChildGender;
    }

    public void setPreferredChildGender(String preferredChildGender) {
        this.preferredChildGender = preferredChildGender;
    }
}