package com.dhp.screening.data.event;


public class EventConnectionStateChanged {

    private int state;

    public EventConnectionStateChanged(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}