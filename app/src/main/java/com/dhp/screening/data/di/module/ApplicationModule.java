package com.dhp.screening.data.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import dagger.Module;
import dagger.Provides;
import com.dhp.screening.data.manager.SharedPrefsHelper;
import com.dhp.screening.data.manager.LDatabase;
import com.dhp.screening.data.di.ApplicationContext;
import com.dhp.screening.data.di.DatabaseInfo;

@Module
public class ApplicationModule {
    private final Application application;

    public ApplicationModule(Application app) {
        application = app;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    @DatabaseInfo
    String provideDatabaseName() {
        return LDatabase.DATABASE_NAME;
    }

    @Provides
    @DatabaseInfo
    Integer provideDatabaseVersion() {
        return 1;
    }

    @Provides
    SharedPreferences provideSharedPrefs() {
        return application.getSharedPreferences(SharedPrefsHelper.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }
}