package com.dhp.screening.data.asset;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.dhp.screening.LApplication;
import com.dhp.screening.R;
import com.dhp.screening.data.asset.encrypt.CryptoUtil;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemDefaultUser;
import com.dhp.screening.data.item.ItemLanguage;
import com.dhp.screening.data.item.ItemMinMax;
import com.dhp.screening.data.item.ItemPartnerUser;
import com.dhp.screening.data.item.ItemQuestionChildren;
import com.dhp.screening.data.item.ItemQuestionHeader;
import com.dhp.screening.data.item.ItemScreeningQuestion;
import com.dhp.screening.data.item.ItemVitalTest;
import com.dhp.screening.data.item.ItemVitalTests;
import com.dhp.screening.data.range.ItemBmi;
import com.dhp.screening.data.range.ItemBmiRange;
import com.dhp.screening.data.range.ItemBp;
import com.dhp.screening.data.range.ItemBpRange;
import com.dhp.screening.data.range.ItemHb;
import com.dhp.screening.data.range.ItemHbAge;
import com.dhp.screening.data.range.ItemHbRange;
import com.dhp.screening.data.range.ItemRange;
import com.dhp.screening.data.range.ItemRangeCount;
import com.dhp.screening.data.range.ItemRbsRange;
import com.dhp.screening.data.range.ItemVital;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LToast;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import static com.dhp.screening.data.item.ItemScreeningQuestion.TYPE_HEADER;
import static com.dhp.screening.util.LConstants.DEFAULT_AUTO_SYNC_INTERVAL;
import static com.dhp.screening.util.LConstants.DEFAULT_DURATION;
import static com.dhp.screening.util.LUtils.toDouble;


public class AssetReader {

    public static final String KEY_USER_TYPES                      = "user_types";
    public static final String KEY_GENDER_TYPES                    = "gender_types";
    public static final String KEY_STATE                           = "states";
    public static final String KEY_IDENTITY_TYPES                  = "identity_types";
    public static final String KEY_HOUSE_OWNERSHIP_TYPES           = "house_ownership_types";
    public static final String KEY_RATION                          = "ration_types";
    public static final String KEY_INSURANCE                       = "insurance_types";
    public static final String KEY_TREATMENT_PLACE                 = "treatment_places";
    public static final String KEY_COOKING_FUELS                   = "cooking_fuels";
    public static final String KEY_TOILET_AVAILABILITY             = "toilet_availability";
    public static final String KEY_ELECTRICITY_AVAILABILITY        = "electricity_availability";
    public static final String KEY_WATER_SOURCE                    = "drinking_water_source";
    public static final String KEY_VEHICLES                        = "motorised_vehicle";
    public static final String KEY_MEAL_TIMINGS                    = "meal_timings";
    public static final String KEY_HOUSE_STATUS                    = "house_status";
    public static final String KEY_DEATH_CAUSES                    = "death_causes";
    public static final String KEY_AGE_UNITS                       = "age_units";
    public static final String KEY_STOCK_ITEMS                     = "stock_items";
    public static final String KEY_HOUSE_TYPES                     = "house_types";
    public static final String KEY_HB_VALUES                       = "hb_values";
    public static final String KEY_ALBUMIN_VALUES                  = "albumin_values";
    public static final String KEY_DATE_RANGES                     = "date_ranges";
    public static final String KEY_HEALTH_CARD_TYPES               = "health_cards";
    public static final String KEY_PETS_BITE_ACTION                = "pets_bite_action";
    public static final String KEY_DRINKING_WATER_CLEANING_METHODS = "drinking_water_cleaning_methods";
    public static final String KEY_BATHING_TOILET_FACILITY         = "bathing_toilet_facility";
    public static final String KEY_VACCINATION_RECORDS_STORAGE     = "vaccination_records_storage";
    public static final String KEY_HEALTH_RECORDS_STORAGE          = "health_records_storage";

    public static final String KEY_RELATIONS                  = "relations";
    public static final String KEY_MARITAL_STATUS             = "marital_status";
    public static final String KEY_EDUCATION                  = "education";
    public static final String KEY_INACTIVE_REASON            = "inactive_reason";
    public static final String KEY_LITERACY                   = "literacy";
    public static final String KEY_OCCUPATIONS                = "occupations";
    public static final String KEY_LANGUAGES                  = "languages_spoken";
    public static final String KEY_HABITS                     = "habits";
    public static final String KEY_DISEASES                   = "diseases";
    public static final String KEY_DISABILITIES               = "disabilities";
    public static final String KEY_MEDICINES_DAILY            = "medicines";
    public static final String KEY_MOBILE_PHONES              = "mobile_phones";
    public static final String KEY_MOBILE_COMFORT             = "mobile_comfort";
    public static final String KEY_CHANGES_TO_CONTROL_DISEASE = "changes_to_control_disease";

    public static final String KEY_SCREENING_QUESTIONS       = "screening_questions";
    public static final String KEY_SCREENING_QUESTION_GROUPS = "screening_question_groups";

    public static final String KEY_SHOW_SOCIODEMOGRAPHICS           = "show_sociodemographics";
    public static final String KEY_SHOW_PROBLEMS                    = "show_problems";
    public static final String KEY_SHOW_STOCK_OPTION                = "show_stock_option";
    public static final String KEY_SHOW_GRAPH_OPTION                = "show_graph_option";
    public static final String KEY_SHOW_CSV                         = "show_csv_format_in_summary";
    public static final String KEY_SHOW_HB_CAPTURE                  = "hb_show_capture";
    public static final String KEY_SHOW_ALBUMIN_CAPTURE             = "albumin_show_capture";
    public static final String KEY_ACCEPT_AADHAAR_NUMBER            = "accept_aadhaar_number";
    public static final String KEY_DISPLAY_AADHAAR_NUMBER           = "display_aadhaar_number";
    public static final String KEY_ALLOW_DUPLICATE_DEMOGRAPHICS     = "allow_duplicate_demographics";
    public static final String KEY_ALLOW_DUPLICATE_IDENTITY_DETAILS = "allow_duplicate_identity_details";
    public static final String KEY_SHOW_ADD_VITAL_IN_HOME           = "show_add_vital_in_home";
    public static final String KEY_SHOW_CAPTURE_FACE                = "show_capture_face";

    public static final String KEY_EXPIRED_DEMOGRAPHICS_COUNT       = "show_expired_message_demographics_count";
    public static final String KEY_EXPIRED_VITALS_COUNT             = "show_expired_message_vitals_count";
    public static final String KEY_EXPIRED_POPUP_DEMOGRAPHICS_COUNT = "show_expired_popup_demographics_count";
    public static final String KEY_EXPIRED_POPUP_VITALS_COUNT       = "show_expired_popup_vitals_count";
    public static final String KEY_SHOW_HOUSE_EXTRA_QUESTIONS       = "show_house_extra_questions";
    public static final String KEY_SHOW_SOCIO_EXTRA_QUESTIONS       = "show_socio_extra_questions";

    public static final String AUDIO_INSERT_STRIP  = "insert_strip";
    public static final String AUDIO_INSERT_BLOOD  = "insert_blood";
    public static final String AUDIO_USE_NEW_STRIP = "use_new_strip";
    public static final String AUDIO_ERROR         = "error";
    public static final String AUDIO_WAIT          = "wait";
    public static final String AUDIO_TEST_COMPLETE = "test_complete";

    public static final String HB_ALLOW_FREE_TEXT      = "hb_allow_free_text";
    public static final String ALBUMIN_ALLOW_FREE_TEXT = "albumin_allow_free_text";
    public static final String SHOW_HB_LAB_VALUES      = "show_hb_lab_values";

    private static JSONObject jsonObject;
    private static JSONObject jsonObjectEnglish;
    private static JSONObject jsonObjectLanguage;

    private static Gson gson;

    private static ItemBmi[]      itemBmiList;
    private static ItemBpRange[]  itemBpList;
    private static ItemRbsRange[] itemRbsRangeList;
    private static ItemRbsRange[] itemFbsRangeList;
    private static ItemRbsRange[] itemPpbsRangeList;
    private static ItemHb[]       itemHbList;

    public static boolean init() {
        try {
            // Init input json

            String buffer = getFileContent(LApplication.getContext().getString(R.string.input_json_file));
//            String buffer = getFileContent("input_json.txt");

            jsonObject = new JSONObject(buffer);
            gson       = new Gson();

            // Init English
            String fileName = getCurrentLanguageDataFile("English");
            buffer            = getFileContent(fileName);
            jsonObjectEnglish = new JSONObject(buffer);

            return true;

        } catch (Exception e) {
            LToast.error("APK is installed from invalid sources");
            LLog.printStackTrace(e);
            return false;
        }
    }

    public static boolean initSelectedLanguage(String selectedLanguage) {
        try {
            String fileName = getCurrentLanguageDataFile(selectedLanguage);
            String buffer   = getFileContent(fileName);
            jsonObjectLanguage = new JSONObject(buffer);
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    private static String getFileContent(String fileName) {
        String buffer = null;

        try {
            buffer = CryptoUtil.decryptString(Environment.getExternalStorageDirectory() + "/" + LApplication.getContext().getString(R.string.input_json_folder) + fileName);

        } catch (Exception e) {
            LLog.d(TAG, "getFileContent: ");
        }

        return buffer;
    }

    public static String getLangKeyword(String keyword) {
        try {
            return jsonObjectLanguage.getJSONObject("keywords").getString(keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getEngKeyword(String keyword) {
        try {
            return jsonObjectEnglish.getJSONObject("keywords").getString(keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getSelectedLanguageVersion(String value, String arrayName) {
        try {
            int index = indexOf(getEnglishArray(arrayName), value);
            return getLangArray(arrayName)[index];
        } catch (Exception e) {
            return value;
        }
    }

    public static String getEnglishVersion(String value, String arrayName) {
        try {
            int index = indexOf(getLangArray(arrayName), value);
            return getEnglishArray(arrayName)[index];
        } catch (Exception e) {
            return value;
        }
    }

    private static int indexOf(String[] array, String item) {

        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(item)) {
                return i;
            }
        }
        return -1;
    }


    public static String[] getLangSpinnerArray(String keyword) {
        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add(AssetReader.getLangKeyword("select"));

        try {
            JSONArray jsonArray = jsonObjectLanguage.getJSONObject("arrays").getJSONArray(keyword);

            for (int i = 0; i < jsonArray.length(); i++) {
                arrayList.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
        }
        return arrayList.toArray(new String[0]);
    }

    public static String[] getSpinnerArrayEnglish(String keyword) {
        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Select");

        try {
            JSONArray jsonArray = jsonObjectEnglish.getJSONObject("arrays").getJSONArray(keyword);

            for (int i = 0; i < jsonArray.length(); i++) {
                arrayList.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
        }
        return arrayList.toArray(new String[0]);
    }

    public static String[] getLangArray(String keyword) {
        ArrayList<String> arrayList = new ArrayList<>();

        try {
            JSONArray jsonArray = jsonObjectLanguage.getJSONObject("arrays").getJSONArray(keyword);

            for (int i = 0; i < jsonArray.length(); i++) {
                arrayList.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
            LToast.error("APK is installed from invalid sources");
        }
        return arrayList.toArray(new String[0]);
    }

    public static String[] getEnglishArray(String keyword) {
        ArrayList<String> arrayList = new ArrayList<>();

        try {
            JSONArray jsonArray = jsonObjectEnglish.getJSONObject("arrays").getJSONArray(keyword);

            for (int i = 0; i < jsonArray.length(); i++) {
                arrayList.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
        }
        return arrayList.toArray(new String[0]);
    }

    public static ItemDefaultUser getDefaultUser() {
        try {
            return gson.fromJson(jsonObject.getJSONObject("default_user_details").toString(), ItemDefaultUser.class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public static ItemPartnerUser getPartnerUser() {
        try {
            return gson.fromJson(jsonObject.getJSONObject("partner_user_details").toString(), ItemPartnerUser.class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public static String getEnglishIndex(String keyword, int index) {
        String[] engArray = getEnglishArray(keyword);
        return engArray[index];
    }

    public static String getLangAudio(String keyword) {
        try {
            return jsonObjectLanguage.getJSONObject("audio_files").getString(keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static String getNestedText(String parent, String keyword) {
        try {
            return jsonObjectLanguage.getJSONObject(parent).getString(keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

    public static ArrayList<ItemLanguage> getLanguages() {
        ArrayList<ItemLanguage> itemLanguages = new ArrayList<>();

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("languages");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                ItemLanguage itemLanguage = new ItemLanguage();

                itemLanguage.setLanguage(jsonObject.getString("language"));
                itemLanguage.setLanguageTitle(jsonObject.getString("language_title"));
                itemLanguage.setDataFile(jsonObject.getString("data_file"));

                itemLanguages.add(itemLanguage);
            }
        } catch (JSONException e) {
        }

        return itemLanguages;
    }

    private static String getCurrentLanguageDataFile(String selectedLanguage) {
        ArrayList<ItemLanguage> languages = getLanguages();

        for (ItemLanguage itemLanguage : languages) {
            if (itemLanguage.getLanguage().toLowerCase().trim().equals(selectedLanguage.toLowerCase().trim())) {
                return itemLanguage.getDataFile();
            }
        }
        return "";
    }

    public static String[] getSpinnerArray(String key) {
        return getLangSpinnerArray(key);
    }

    public static String[] getJsonSpinnerArray(String key) {
        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Select");

        try {
            JSONArray jsonArray = jsonObject.getJSONArray(key);

            for (int i = 0; i < jsonArray.length(); i++) {
                arrayList.add(jsonArray.getString(i));
            }
        } catch (JSONException e) {
        }
        return arrayList.toArray(new String[0]);
    }

    public static String[] getRelationsDeathInfo() {
        ArrayList<String> arrayList = new ArrayList<>();

        try {
            String[] relations = getLangArray(KEY_RELATIONS);

            for (int i = 0; i < relations.length; i++) {
                if (relations[i].equals(AssetReader.getLangKeyword("self"))) {
                    continue;
                }
                arrayList.add(relations[i]);
            }

        } catch (Exception e) {
            LToast.error("APK is installed from invalid sources");
            LLog.printStackTrace(e);
        }

        return arrayList.toArray(new String[arrayList.size()]);
    }

    public static String[] getRelationsDeathInfoEnglish() {
        ArrayList<String> arrayList = new ArrayList<>();

        try {
            String[] relations = getEnglishArray(KEY_RELATIONS);

            for (int i = 0; i < relations.length; i++) {
                if (relations[i].equals("Self")) {
                    continue;
                }
                arrayList.add(relations[i]);
            }

        } catch (Exception e) {
            LToast.error("APK is installed from invalid sources");
            LLog.printStackTrace(e);
        }

        return arrayList.toArray(new String[arrayList.size()]);
    }

    public static String[] getRadioArray(String key) {
        return getLangArray(key);
    }

    public static String[] getCheckBoxArray(String key) {
        return getLangArray(key);
    }

    public static String[] getCheckBoxArrayEnglish(String key) {
        return getEnglishArray(key);
    }

    public static String[] getStringArray(int arrayId) {
        return LApplication.getContext().getResources().getStringArray(arrayId);
    }

    public static ArrayList<ItemScreeningQuestion> getScreeningQuestions(boolean customLanguage) {
        ArrayList<ItemScreeningQuestion> screeningQuestions = new ArrayList<>();

        String[] questionGroupsLang    = getLangArray(KEY_SCREENING_QUESTION_GROUPS);
        String[] questionGroupsEnglish = getEnglishArray(KEY_SCREENING_QUESTION_GROUPS);
        String[] questionsLang         = getLangArray(KEY_SCREENING_QUESTIONS);
        String[] questionsEnglish      = getEnglishArray(KEY_SCREENING_QUESTIONS);

        boolean firstTime = true;

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("screening_questions");

            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject questionItem = jsonArray.getJSONObject(i);
                JSONArray  questions    = questionItem.getJSONArray("questions");

                String questionGroup;

                if (customLanguage) {
                    questionGroup = questionGroupsLang[i];
                } else {
                    questionGroup = questionGroupsEnglish[i];
                }

                ItemQuestionHeader header = new ItemQuestionHeader();

                ArrayList<ItemQuestionChildren> questionChildren = new ArrayList<>();

                int childIndex = 0;

                for (int j = 0; j < questions.length(); ++j) {
                    JSONObject questionObject = questions.getJSONObject(j);

                    try {
                        if (questionObject.getBoolean("inactive")) {
                            continue;
                        }
                    } catch (Exception e) {
                        LLog.printStackTrace(e);
                    }

                    ItemQuestionChildren itemQuestionChildren;

                    if (customLanguage) {
                        itemQuestionChildren = new ItemQuestionChildren(i, childIndex, questionsLang[j], "", questionGroup);
                    } else {
                        itemQuestionChildren = new ItemQuestionChildren(i, childIndex, questionsEnglish[j], "", questionGroup);
                    }

                    itemQuestionChildren.setQuestionOrder(questionObject.getInt("question_order"));

                    questionChildren.add(itemQuestionChildren);

                    ++childIndex;
                }

                Collections.sort(questionChildren, new Comparator<ItemQuestionChildren>() {
                    @Override
                    public int compare(ItemQuestionChildren left, ItemQuestionChildren right) {
                        int val = 0;

                        try {
                            val = left.getQuestionOrder() - right.getQuestionOrder();
                        } catch (Exception e) {
                            LLog.printStackTrace(e);
                        }
                        return val;
                    }
                });

                header.setHeaderIndex(i);
                header.setChildrenShown(firstTime);
                header.setGroupName(questionGroup);
                header.setQuestionChildren(questionChildren);

                screeningQuestions.add(new ItemScreeningQuestion(TYPE_HEADER, header, null));

                firstTime = false;
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return screeningQuestions;
    }

//    public static String[] getScreeningQuestions() {
//
//        String[] array = new String[4];
//
//        try {
//            JSONArray jsonArray = jsonObject.getJSONArray("screening_questions");
//
//            array = new String[jsonArray.length()];
//
//            for (int i = 0; i < jsonArray.length(); i++) {
//                array[i] = jsonArray.getString(i);
//            }
//
//        } catch (Exception e) {
//            LLog.printStackTrace(e);
//        }
//
//        return array;
//    }

    public static String getAudioFileName(String key) {
        return getLangAudio(key);
    }

    public static ArrayList<String> getVitalsList() {
        return getVitalsList(true);
    }

    public static ArrayList<String> getVitalsList(boolean isAllowPrescription) {
        ArrayList<String> vitalsList = new ArrayList<>();

        try {
            JSONObject vitals = jsonObject.getJSONObject("vitals");

            Iterator<String> iterator = vitals.keys();

            while (iterator.hasNext()) {
                String key = iterator.next();

                if (vitals.getString(key).toLowerCase().equals("yes")) {
                    if (key.equals("Prescription")) {
                        if (isAllowPrescription) {
                            vitalsList.add(key);
                        }

                    } else {
                        vitalsList.add(key);
                    }
                }
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return vitalsList;
    }

    public static ItemVital[] getVitalTabs() {
        try {
            return gson.fromJson(jsonObject.getJSONArray("vital_tabs").toString(), ItemVital[].class);
        } catch (JSONException e) {
            return null;
        }
    }

    public static JSONObject getVitalIcons() {
        try {
            return jsonObject.getJSONObject("vital_icons");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    public static ItemMinMax getVitalRange(String vitalType) {
        try {
            String[] range = jsonObject.getJSONObject("vital_ranges").getString(vitalType).split(", ");
            return new ItemMinMax(Float.parseFloat(range[0].trim()), Float.parseFloat(range[1].trim()));
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return new ItemMinMax();
    }

    public static String getIconFileName(String key) {
        try {
            return jsonObject.getJSONObject("vital_icons").getString(key);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static String getNestedValueFromAppConfig(String parent, String key) {
        try {
            return jsonObject.getJSONObject(parent).getString(key);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static boolean showSyncOptions() {
        try {
            String value = jsonObject.getJSONObject("admin_options").getString("show_sync_option");

            if (value.toLowerCase().equals("yes")) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static void openHelp(Context context) {

        String filename = AssetReader.getLangKeyword("help_file");

        if (null == filename) {
            LToast.warningLong("Help file not found");
            return;
        }

        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + LApplication.getContext().getString(R.string.input_json_folder) + filename);

        if (!file.exists()) {
            LToast.warningLong("Help file not found");
            return;
        }

        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

        Intent intent = Intent.createChooser(target, "Open File");

        try {
            context.startActivity(intent);

        } catch (ActivityNotFoundException e) {
            // Instruct the user to install a PDF reader here, or something
        }
    }

    public static int getTestDuration() {
        try {
            return jsonObject.getInt("hemoglobin_test_wait_duration");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return DEFAULT_DURATION;
    }

    public static String getValue(String key) {
        String defaultState = "";

        try {
            defaultState = jsonObject.getString(key);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return defaultState;
    }

    public static ItemRange getBmiRange(double bmi, int age) {
        return getBmiRange(bmi, age, false);
    }

    public static ItemRange getBmiRange(double bmi, int age, boolean isCustomLanguage) {
        ItemRange itemRange = new ItemRange();

        if (null == itemBmiList) {
            try {
                itemBmiList = gson.fromJson(jsonObject.getJSONArray("bmi_range").toString(), ItemBmi[].class);
            } catch (JSONException e) {
            }
        }

        if (null != itemBmiList) {
            for (ItemBmi itemBmi : itemBmiList) {
                if (toDouble(itemBmi.getAgeMin()) <= age && toDouble(itemBmi.getAgeMax()) >= age) {
                    for (ItemBmiRange itemBmiRange : itemBmi.getItemBmiRange()) {
                        if (toDouble(itemBmiRange.getMin()) <= bmi && toDouble(itemBmiRange.getMax()) >= bmi) {
                            itemRange.setRange(itemBmiRange.getRange());
                            itemRange.setColor(itemBmiRange.getColor());
                        }
                    }
                }
            }
        }

        return itemRange;
    }

    public static String[] getAbnormalBmiRanges() {
        ArrayList<String> ranges = new ArrayList<>();
        ranges.add("Select");

        if (null == itemBmiList) {
            try {
                itemBmiList = gson.fromJson(jsonObject.getJSONArray("bmi_range").toString(), ItemBmi[].class);
            } catch (JSONException e) {
            }
        }

        ItemBmi itemBmi = itemBmiList[0];

        for (ItemBmiRange item : itemBmi.getItemBmiRange()) {
            if (!item.getRange().equals("Normal")) {
                ranges.add(item.getRange());
            }
        }

        return ranges.toArray(new String[0]);
    }

    public static String[] getAbnormalBpRanges() {
        ArrayList<String> ranges = new ArrayList<>();
        ranges.add("Select");

        if (null == itemBpList) {
            try {
                itemBpList = gson.fromJson(jsonObject.getJSONArray("bp_range").toString(), ItemBpRange[].class);
            } catch (JSONException e) {
            }
        }

        for (ItemBpRange item : itemBpList) {
            if (!item.getRange().equals("Normal")) {
                ranges.add(item.getRange());
            }
        }

        return ranges.toArray(new String[0]);
    }

    public static String[] getAbnormalHbRanges() {
        ArrayList<String> ranges = new ArrayList<>();
        ranges.add("Select");

        if (null == itemHbList) {
            try {
                itemHbList = gson.fromJson(jsonObject.getJSONArray("hb_range").toString(), ItemHb[].class);
            } catch (JSONException e) {
            }
        }

        ItemHbRange[] itemHbRanges = itemHbList[0].getItemHbAge()[0].getItemHbRange();

        for (ItemHbRange item : itemHbRanges) {
            if (!item.getRange().equals("Normal")) {
                ranges.add(item.getRange());
            }
        }

        return ranges.toArray(new String[0]);
    }

    public static String[] getAbnormalRbsRanges() {
        ArrayList<String> ranges = new ArrayList<>();
        ranges.add("Select");

        if (null == itemRbsRangeList) {
            try {
                itemRbsRangeList = gson.fromJson(jsonObject.getJSONArray("rbs_range").toString(), ItemRbsRange[].class);
            } catch (JSONException e) {
            }
        }

        for (ItemRbsRange item : itemRbsRangeList) {
            if (!item.getRange().equals("Normal")) {
                ranges.add(item.getRange());
            }
        }

        return ranges.toArray(new String[0]);
    }

    public static ItemRange getBpRange(double systolic, double diastolic) {
        return getBpRange(systolic, diastolic, false);
    }

    public static ItemRange getBpRange(double systolic, double diastolic, boolean isCustomLanguage) {
        ItemRange itemRange = new ItemRange();

        if (null == itemBpList) {
            try {
                itemBpList = gson.fromJson(jsonObject.getJSONArray("bp_range").toString(), ItemBpRange[].class);
            } catch (JSONException e) {
            }
        }

        if (null != itemBpList) {
            for (ItemBpRange itemBpRange : itemBpList) {

                if ((toDouble(itemBpRange.getMin().getSystolic()) <= systolic && toDouble(itemBpRange.getMax().getSystolic()) >= systolic)
                        && (toDouble(itemBpRange.getMin().getDiastolic()) <= diastolic && toDouble(itemBpRange.getMax().getDiastolic()) >= diastolic)) {

                    itemRange.setRange(itemBpRange.getRange());
                    itemRange.setColor(itemBpRange.getColor());

                    break;

                } else if ((toDouble(itemBpRange.getMin().getSystolic()) <= systolic && toDouble(itemBpRange.getMax().getSystolic()) >= systolic)
                        || (toDouble(itemBpRange.getMin().getDiastolic()) <= diastolic && toDouble(itemBpRange.getMax().getDiastolic()) >= diastolic)) {

                    if (itemBpRange.getRange().equals("Normal")) {
                        continue;
                    }

                    itemRange.setRange(itemBpRange.getRange());
                    itemRange.setColor(itemBpRange.getColor());
                }
            }
        }

        String range = itemRange.getRange();

        if (range.isEmpty()) {
            LLog.d(TAG, "getBpRange: ");
        }

        return itemRange;
    }

    private static final String TAG = "AssetReader";

    public static ItemRange getRbsRange(double value, String type) {
        ItemRange itemRange = new ItemRange();

        ItemRbsRange[] currentRangeList;

        if (type.equals("RBS")) {
            if (null == itemRbsRangeList) {
                try {
                    itemRbsRangeList = gson.fromJson(jsonObject.getJSONArray("rbs_range").toString(), ItemRbsRange[].class);
                } catch (JSONException e) {
                }
            }

            currentRangeList = itemRbsRangeList;

        } else if (type.equals("FBS")) {
            if (null == itemFbsRangeList) {
                try {
                    itemFbsRangeList = gson.fromJson(jsonObject.getJSONArray("fbs_range").toString(), ItemRbsRange[].class);
                } catch (JSONException e) {
                }
            }

            currentRangeList = itemFbsRangeList;

        } else {
            if (null == itemPpbsRangeList) {
                try {
                    itemPpbsRangeList = gson.fromJson(jsonObject.getJSONArray("ppbs_range").toString(), ItemRbsRange[].class);
                } catch (JSONException e) {
                }
            }

            currentRangeList = itemPpbsRangeList;
        }

        int rangeIndex = 0;

        if (null != currentRangeList) {

            rangeIndex = currentRangeList.length - 1;

            int i = 0;

            for (ItemRbsRange itemRbsRange : currentRangeList) {

                if (toDouble(itemRbsRange.getMin()) <= value && toDouble(itemRbsRange.getMax()) >= value) {
                    rangeIndex = i;
                    break;
                }

                ++i;
            }

            itemRange.setRange(currentRangeList[rangeIndex].getRange());
            itemRange.setColor(currentRangeList[rangeIndex].getColor());
        }

        return itemRange;
    }

    public static ItemRange getHbRange(double value, String gender, double age) {
        ItemRange itemRange = new ItemRange();

        if (null == itemHbList) {
            try {
                itemHbList = gson.fromJson(jsonObject.getJSONArray("hb_range").toString(), ItemHb[].class);
            } catch (JSONException e) {
            }
        }

        if (null != itemHbList) {
            for (ItemHb itemHb : itemHbList) {
                if (itemHb.getGender().equals(gender)) {
                    for (ItemHbAge itemHbAge : itemHb.getItemHbAge()) {
                        if (toDouble(itemHbAge.getAgeMin()) <= age && toDouble(itemHbAge.getAgeMax()) >= age) {
                            for (ItemHbRange itemHbRange : itemHbAge.getItemHbRange()) {
                                if (toDouble(itemHbRange.getMin()) <= value && toDouble(itemHbRange.getMax()) >= value) {
                                    itemRange.setRange(itemHbRange.getRange());
                                    itemRange.setColor(itemHbRange.getColor());
                                }
                            }
                        }
                    }
                }
            }
        }

        return itemRange;
    }

    public static ArrayList<ItemRangeCount> getBmiRanges() {
        return getBmiRanges(false);
    }

    public static ArrayList<ItemRangeCount> getBmiRanges(boolean isCustomLanguage) {
        ArrayList<ItemRangeCount> ranges = new ArrayList<>(4);

        try {
            ItemBmi[] list    = gson.fromJson(jsonObject.getJSONArray("bmi_range").toString(), ItemBmi[].class);
            ItemBmi   itemBmi = list[0];
            for (ItemBmiRange itemBmiRange : itemBmi.getItemBmiRange()) {
                ranges.add(new ItemRangeCount(itemBmiRange.getRange(), itemBmiRange.getColor()));
            }
        } catch (JSONException e) {
        }

        return ranges;
    }

    public static ArrayList<ItemRangeCount> getBpRanges() {
        ArrayList<ItemRangeCount> ranges = new ArrayList<>(4);

        try {
            ItemBpRange[] list = gson.fromJson(jsonObject.getJSONArray("bp_range").toString(), ItemBpRange[].class);
            for (ItemBpRange range : list) {
                ranges.add(new ItemRangeCount(range.getRange(), range.getColor()));
            }
        } catch (JSONException e) {
        }

        return ranges;
    }

    public static ArrayList<ItemRangeCount> getRbsRanges() {
        ArrayList<ItemRangeCount> ranges = new ArrayList<>(4);

        try {
            ItemRbsRange[] list = gson.fromJson(jsonObject.getJSONArray("rbs_range").toString(), ItemRbsRange[].class);
            for (ItemRbsRange range : list) {
                ranges.add(new ItemRangeCount(range.getRange(), range.getColor()));
            }
        } catch (JSONException e) {
        }

        return ranges;
    }

    public static ArrayList<ItemRangeCount> getFbsRanges() {
        ArrayList<ItemRangeCount> ranges = new ArrayList<>(4);

        try {
            ItemRbsRange[] list = gson.fromJson(jsonObject.getJSONArray("fbs_range").toString(), ItemRbsRange[].class);
            for (ItemRbsRange range : list) {
                ranges.add(new ItemRangeCount(range.getRange(), range.getColor()));
            }
        } catch (JSONException e) {
        }

        return ranges;
    }

    public static ArrayList<ItemRangeCount> getPpbsRanges() {
        ArrayList<ItemRangeCount> ranges = new ArrayList<>(4);

        try {
            ItemRbsRange[] list = gson.fromJson(jsonObject.getJSONArray("ppbs_range").toString(), ItemRbsRange[].class);
            for (ItemRbsRange range : list) {
                ranges.add(new ItemRangeCount(range.getRange(), range.getColor()));
            }
        } catch (JSONException e) {
        }

        return ranges;
    }

    public static ArrayList<ItemRangeCount> getHemoglobinRanges() {
        ArrayList<ItemRangeCount> ranges = new ArrayList<>(4);

        try {
            ItemHb[] list = gson.fromJson(jsonObject.getJSONArray("hb_range").toString(), ItemHb[].class);

            ItemHb    itemHb    = list[0];
            ItemHbAge itemHbAge = itemHb.getItemHbAge()[0];

            for (ItemHbRange itemHbRange : itemHbAge.getItemHbRange()) {
                ranges.add(new ItemRangeCount(itemHbRange.getRange(), itemHbRange.getColor()));
            }

        } catch (JSONException e) {
        }

        return ranges;
    }

    public static long getAutoSyncInterval() {
        try {
            return jsonObject.getInt("auto_sync_interval") * 1000;

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return DEFAULT_AUTO_SYNC_INTERVAL * 1000;
    }

    public static boolean toShow(String key) {
        try {
            return jsonObject.get(key).toString().toLowerCase().equals("yes");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return true;
    }

    public static int getMinPhoneLength() {
        try {
            return jsonObject.getInt("min_phone_length");
        } catch (Exception e) {
            return 8;
        }
    }

    public static int getMaxPhoneLength() {
        try {
            return jsonObject.getInt("max_phone_length");
        } catch (Exception e) {
            return 8;
        }
    }

    public static int getIntKey(String key) {
        try {
            return jsonObject.getInt(key);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return 100;
    }

    public static String getStringKey(String key) {
        try {
            return jsonObject.getString(key);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static String[] getUserTypes() {
        String[] array = new String[1];

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("user_types");

            array    = new String[jsonArray.length() + 1];
            array[0] = "Select";

            for (int i = 0; i < jsonArray.length(); i++) {
                array[i + 1] = jsonArray.getJSONObject(i).getString("user_type");
            }

        } catch (Exception e) {
            LToast.error("APK is installed from invalid sources");
            LLog.printStackTrace(e);
        }

        return array;
    }

    public static boolean isPrescriptionAllowed(String userType) {

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("user_types");

            for (int i = 0; i < jsonArray.length(); i++) {
                String userTypeJson = jsonArray.getJSONObject(i).getString("user_type");

                if (userTypeJson.equals(userType)) {

                    if (jsonArray.getJSONObject(i)
                            .getString("allow_prescription")
                            .toLowerCase().equals("yes")) {
                        return true;
                    }
                    break;
                }
            }

        } catch (Exception e) {
            LToast.error("APK is installed from invalid sources");
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static boolean isEnabled(String key) {
        ArrayList<String> vitalsList = getVitalsList();
        return null != vitalsList && 0 < vitalsList.size() && vitalsList.contains(key);
    }

    public static String getDefaultPrescriptionType() {
        try {
            return jsonObject.get("default_prescription_type").toString().toLowerCase();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static String getCurrentPartnerId() {
        try {
            return jsonObject.get("current_partner_id").toString().toUpperCase();

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static String getCurrentDeviceNumber() {
        try {
            return jsonObject.getString("current_device_number");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static boolean isDoorToDoor() {
        try {
            return jsonObject.getString("is_door_to_door").toLowerCase().equals("yes");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static boolean toShowAudio() {
        try {
            return jsonObject.getString("show_audio_link").toLowerCase().equals("yes");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static boolean allowDeviceToSync() {
        try {
            return jsonObject.getString("allow_device_to_sync").toLowerCase().equals("yes");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static boolean isAutoSyncEnabled() {
        try {
            return jsonObject.getString("auto_sync_enabled").toLowerCase().equals("yes");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return false;
    }

    public static ItemBluetoothDevice getBluetoothDevice(String deviceType) {
        ItemBluetoothDevice itemBluetoothDevices[] = null;

        try {
            itemBluetoothDevices = gson.fromJson(jsonObject.getJSONArray("mac_addresses").toString(), ItemBluetoothDevice[].class);
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (null == itemBluetoothDevices) {
            return null;
        }

        return getBluetoothDeviceFromType(itemBluetoothDevices, deviceType);
    }

    public static ItemBluetoothDevice[] getBluetoothDevices() {
        try {
            return gson.fromJson(jsonObject.getJSONArray("bluetooth_devices").toString(), ItemBluetoothDevice[].class);

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return null;
    }

    private static ItemBluetoothDevice getBluetoothDeviceFromType(ItemBluetoothDevice itemBluetoothDevices[],
                                                                  String deviceType) {

        for (ItemBluetoothDevice itemBluetoothDevice : itemBluetoothDevices) {

            if (itemBluetoothDevice.getDeviceType().equals(deviceType)) {
                ItemBluetoothDevice device = new ItemBluetoothDevice();
                device.setDeviceType(itemBluetoothDevice.getDeviceType());
                device.setDeviceName(itemBluetoothDevice.getDeviceName());
                device.setMacAddress(itemBluetoothDevice.getMacAddress());

                return device;
            }
        }

        return null;
    }

    public static String getVersion() {
        try {
            return jsonObject.getString("version");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    public static boolean isAllowSave(String vital) {
        try {
            JSONObject vitals = jsonObject.getJSONObject("allow_manual_entry");

            if (vitals.getString(vital).toLowerCase().equals("yes")) {
                return true;
            }
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }
        return false;
    }

    public static String getDefaultCountry() {
        try {
            return jsonObject.getString("default_country");

        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        return "";
    }

    private static ItemVitalTests getVitalTests() {
        try {
            FileInputStream stream = new FileInputStream(new File(Environment.getExternalStorageDirectory(), LApplication.getContext().getString(R.string.input_json_folder) + "vital_tests.json"));
            byte[]          buffer = new byte[stream.available()];
            stream.read(buffer);
            stream.close();

            gson = new Gson();
            return gson.fromJson(new String(buffer), ItemVitalTests.class);

        } catch (Exception e) {
        }

        return null;
    }

    public static ItemVitalTest getVitalTest(String testName) {
        ItemVitalTests itemVitalTests = getVitalTests();

        for (ItemVitalTest vitalTest : itemVitalTests.getVitalTests()) {
            if (vitalTest.getTestName().toLowerCase().equals(testName.toLowerCase())) {
                return vitalTest;
            }
        }

        return null;
    }

    private static String getBmiRangeText(String level, boolean isCustomLanguage) {
        try {
            if (isCustomLanguage) {
                return jsonObjectLanguage.getJSONObject("vital_levels").getJSONObject("bmi").getString(level);
            } else {
                return jsonObjectEnglish.getJSONObject("vital_levels").getJSONObject("bmi").getString(level);
            }
        } catch (Exception e) {
            return "";
        }
    }

    public static String getFaceImageType() {
        // TODO: 24-Aug-19
        return "Photo (Face)";
    }
}