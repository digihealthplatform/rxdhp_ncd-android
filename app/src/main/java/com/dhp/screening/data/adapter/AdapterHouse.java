package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.ui.house.ActivityHouseDetails;

public class AdapterHouse extends RecyclerView.Adapter<AdapterHouse.ViewHolder> {
    private final Context        context;
    private final LayoutInflater layoutInflater;

    private String[]  householdList;
    private String[]  houseIds;
    private boolean[] houseInactive;

    public AdapterHouse(Context context, String[] householdList, String[] houseIds, boolean[] houseInactive) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.householdList = householdList;
        this.houseIds = houseIds;
        this.houseInactive = houseInactive;
    }

    @Override
    public AdapterHouse.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_house, parent, false);
        return new AdapterHouse.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterHouse.ViewHolder holder, int position) {
        holder.tvHouseInfo.setText(householdList[position]);

        if (houseInactive[position]) {
            holder.llHouseItem.setBackground(ContextCompat
                    .getDrawable(context, R.drawable.item_inactive));
        }
    }

    @Override
    public int getItemCount() {
        if (null != householdList) {
            return householdList.length;
        }
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView     tvHouseInfo;
        private LinearLayout llHouseItem;

        ViewHolder(View itemView) {
            super(itemView);
            tvHouseInfo = (TextView) itemView.findViewById(R.id.tv_house_info);

            llHouseItem = (LinearLayout) itemView.findViewById(R.id.ll_house_item);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityHouseDetails.class);
            intent.putExtra("house_id", houseIds[getAdapterPosition()]);
            context.startActivity(intent);
        }
    }
}