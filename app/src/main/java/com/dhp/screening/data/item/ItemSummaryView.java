package com.dhp.screening.data.item;

import java.util.ArrayList;

import com.dhp.screening.data.range.ItemRangeCount;

public class ItemSummaryView {
    public static final int TYPE_DIVIDER             = 1;
    public static final int TYPE_SUMMARY             = 2;
    public static final int TYPE_SUMMARY_VITAL       = 3;
    public static final int TYPE_SUMMARY_VITAL_BMI   = 4;
    public static final int TYPE_SUMMARY_VITAL_BP    = 5;
    public static final int TYPE_SUMMARY_VITAL_SUGAR = 6;
    public static final int TYPE_SUMMARY_VITAL_HB    = 7;
    public static final int TYPE_HEADER              = 8;

    private int type;
    private int value;

    private String key;

    ArrayList<ItemRangeCount> itemRangeCounts;

    public ItemSummaryView(int type, String key, int value) {
        this.type = type;
        this.key = key;
        this.value = value;
    }

    public ItemSummaryView(int type, ArrayList<ItemRangeCount> itemRangeCounts) {
        this.type = type;
        this.itemRangeCounts = itemRangeCounts;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public ArrayList<ItemRangeCount> getItemRangeCounts() {
        return itemRangeCounts;
    }

    public void setItemRangeCounts(ArrayList<ItemRangeCount> itemRangeCounts) {
        this.itemRangeCounts = itemRangeCounts;
    }
}