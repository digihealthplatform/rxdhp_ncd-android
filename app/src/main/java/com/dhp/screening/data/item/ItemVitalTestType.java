package com.dhp.screening.data.item;

import com.google.gson.annotations.SerializedName;


public class ItemVitalTestType {

    @SerializedName("column_size")
    private int columnSize = 1;

    @SerializedName("test_type")
    private String testType;

    @SerializedName("steps")
    private ItemVitalTestStep[] testSteps;

    @SerializedName("results")
    private ItemVitalTestResult[] testResults;

    public int getColumnSize() {
        return columnSize;
    }

    public void setColumnSize(int columnSize) {
        this.columnSize = columnSize;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public ItemVitalTestStep[] getTestSteps() {
        return testSteps;
    }

    public void setTestSteps(ItemVitalTestStep[] testSteps) {
        this.testSteps = testSteps;
    }

    public ItemVitalTestResult[] getTestResults() {
        return testResults;
    }

    public void setTestResults(ItemVitalTestResult[] testResults) {
        this.testResults = testResults;
    }
}
