package com.dhp.screening.data.table;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

import com.dhp.screening.data.manager.LDatabase;

@Table(database = LDatabase.class)
public class TableDeathInfo extends BaseModel implements Serializable {

    @Column(name = "table_key")
    @PrimaryKey(autoincrement = true)
    private int tableKey;

    @Column(name = "house_id")
    private String houseId;

    @Column(name = "person_name")
    private String personName = "";

    @Column(name = "gender")
    private String gender = "";

    @Column(name = "age")
    private int age;

    @Column(name = "age_unit")
    private String ageUnit = "";

    @Column(name = "relation_to_hoh")
    private String relationToHoh = "";

    @Column(name = "death_cause")
    private String deathCause = "";

    @Column(name = "notes")
    private String notes = "";

    @Column(name = "surveyor_user_id")
    private String surveyorUserId = "";

    @Column(name = "partner_id")
    private String partnerId = "";

    @Column(name = "camp_name")
    private String campName = "";

    @Column(name = "device_number")
    private String deviceNumber = "";

    @Column(name = "device_id")
    private String deviceId;

    @Column(name = "survey_date_time")
    private String surveyDateTime = "";

    @Column(name = "survey_date_time_millis")
    private long surveyDateTimeMillis;

    @Column(name = "inactive")
    private boolean inactive;

    @Column(name = "inactive_reason")
    private String inactiveReason = "";

    @Column(name = "modified")
    private boolean modified;

    @Column(name = "modified_date_time")
    private String modifiedDateTime = "";

    @Column(name = "synced")
    private boolean synced = false;

    public int getTableKey() {
        return tableKey;
    }

    public void setTableKey(int tableKey) {
        this.tableKey = tableKey;
    }

    public String getHouseId() {
        return houseId;
    }

    public void setHouseId(String houseId) {
        this.houseId = houseId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAgeUnit() {
        return ageUnit;
    }

    public void setAgeUnit(String ageUnit) {
        this.ageUnit = ageUnit;
    }

    public String getRelationToHoh() {
        return relationToHoh;
    }

    public void setRelationToHoh(String relationToHoh) {
        this.relationToHoh = relationToHoh;
    }

    public String getDeathCause() {
        return deathCause;
    }

    public void setDeathCause(String deathCause) {
        this.deathCause = deathCause;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSurveyorUserId() {
        return surveyorUserId;
    }

    public void setSurveyorUserId(String surveyorUserId) {
        this.surveyorUserId = surveyorUserId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getCampName() {
        return campName;
    }

    public void setCampName(String campName) {
        this.campName = campName;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSurveyDateTime() {
        return surveyDateTime;
    }

    public void setSurveyDateTime(String surveyDateTime) {
        this.surveyDateTime = surveyDateTime;
    }

    public long getSurveyDateTimeMillis() {
        return surveyDateTimeMillis;
    }

    public void setSurveyDateTimeMillis(long surveyDateTimeMillis) {
        this.surveyDateTimeMillis = surveyDateTimeMillis;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getInactiveReason() {
        return inactiveReason;
    }

    public void setInactiveReason(String inactiveReason) {
        this.inactiveReason = inactiveReason;
    }

    public boolean isModified() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified = modified;
    }

    public String getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(String modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }
}