package com.dhp.screening.data.manager;

import android.support.annotation.NonNull;

import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.data.table.TableSociodemographics;
import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;
import com.raizlabs.android.dbflow.sql.migration.BaseMigration;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;


@Database(name = LDatabase.DATABASE_NAME, version = LDatabase.VERSION)
public class LDatabase {

    private static final String TAG = "LDatabase";

    public static final String DATABASE_NAME = "ncd_screening_encrypted";

    public static final int VERSION = 7;

    @Migration(version = 2, database = LDatabase.class)
    public static class MigrationHouse2 extends AlterTableMigration<TableHouse> {

        public MigrationHouse2(Class<TableHouse> table) {
            super(table);
        }

        @Override
        public void onPreMigrate() {
            addColumn(SQLiteType.TEXT, "spouse_of_head_of_house");
            addColumn(SQLiteType.TEXT, "house_status");
            addColumn(SQLiteType.INTEGER, "total_rooms");
            addColumn(SQLiteType.TEXT, "toilet_availability");
            addColumn(SQLiteType.TEXT, "drinking_water_source");
            addColumn(SQLiteType.TEXT, "electricity_availability");
            addColumn(SQLiteType.TEXT, "motorised_vehicle");
            addColumn(SQLiteType.TEXT, "village");
            addColumn(SQLiteType.TEXT, "gram_panchayat");
        }
    }

    @Migration(version = 2, database = LDatabase.class)
    public static class MigrationSociodemographics2 extends AlterTableMigration<TableSociodemographics> {

        public MigrationSociodemographics2(Class<TableSociodemographics> table) {
            super(table);
        }

        @Override
        public void onPreMigrate() {
            addColumn(SQLiteType.TEXT, "health_card");
        }
    }

    @Migration(version = 2, database = LDatabase.class)
    public static class MigrationDemographics2 extends AlterTableMigration<TableDemographics> {

        public MigrationDemographics2(Class<TableDemographics> table) {
            super(table);
        }

        @Override
        public void onPreMigrate() {
            addColumn(SQLiteType.TEXT, "mr_number");
        }
    }

    @Migration(version = 4, database = LDatabase.class)
    public static class MigrationImage2 extends AlterTableMigration<TableImage> {

        public MigrationImage2(Class<TableImage> table) {
            super(table);
        }

        @Override
        public void onPreMigrate() {
            addColumn(SQLiteType.TEXT, "image_date_time");
            addColumn(SQLiteType.INTEGER, "image_date_time_millis");
            addColumn(SQLiteType.INTEGER, "image_camera");
        }
    }

    @Migration(version = 5, database = LDatabase.class)
    public static class MigrationSocioDemographics5 extends BaseMigration {

        @Override
        public void migrate(@NonNull DatabaseWrapper database) {
            addStringColumn(database, "medicines_taken_daily");
            addStringColumn(database, "disease_test_last_done");
            addStringColumn(database, "informed_about_disease");
            addStringColumn(database, "medication_for_disease");
            addStringColumn(database, "changes_to_control_disease");
            addStringColumn(database, "women_special_precautions_on_period");
            addStringColumn(database, "mobile_used");
            addStringColumn(database, "mobile_comfort");
            addStringColumn(database, "health_records_storage");
        }

        private void addStringColumn(DatabaseWrapper database, String columnName) {
            database.execSQL("ALTER TABLE TableSociodemographics ADD COLUMN " + columnName + " TEXT DEFAULT ''");
        }
    }

    @Migration(version = 5, database = LDatabase.class)
    public static class MigrationHouse5 extends BaseMigration {
        @Override
        public void migrate(@NonNull DatabaseWrapper database) {
            addIntegerColumn(database, "earning_members");
            addIntegerColumn(database, "total_income");
            addIntegerColumn(database, "treatment_cost");

            addStringColumn(database, "drink_weekly");
            addStringColumn(database, "drugs_use");
            addStringColumn(database, "handicapped_present");
            addStringColumn(database, "handicapped_suffering");
            addStringColumn(database, "pets");
            addStringColumn(database, "pets_bitten");
            addStringColumn(database, "pets_bite_action");
            addStringColumn(database, "drinking_water_cleaning_method");
            addStringColumn(database, "bathing_toilet_facility");
            addStringColumn(database, "child_health_checkup_last_done");
            addStringColumn(database, "children_vaccinated");
            addStringColumn(database, "vaccination_records_storage");
            addStringColumn(database, "child_gender_responsibility");
            addStringColumn(database, "preferred_child_gender");
        }

        private void addStringColumn(DatabaseWrapper database, String columnName) {
            database.execSQL("ALTER TABLE TableHouse ADD COLUMN " + columnName + " TEXT DEFAULT ''");
        }

        private void addIntegerColumn(DatabaseWrapper database, String columnName) {
            database.execSQL("ALTER TABLE TableHouse ADD COLUMN " + columnName + " INTEGER DEFAULT 0");
        }
    }
}