package com.dhp.screening.data.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.item.ItemVitalTestResult;
import com.dhp.screening.util.LView;


public class AdapterTestResult extends RecyclerView.Adapter<AdapterTestResult.ViewHolder> {

    private static final String TAG = "AdapterTestResult";

    private final Context context;

    private final LayoutInflater layoutInflater;

    private final ItemVitalTestResult[] testResults;

    private final int columnSize;

    private int selectedIndex = -1;

    private int viewHeight;

    public AdapterTestResult(Context context, ItemVitalTestResult[] testResults, int columnSize) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.testResults = testResults;
        this.columnSize = columnSize;
    }

    @Override
    public AdapterTestResult.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_test_result, parent, false);

        int measuredHeight = viewHeight;
        int height;

        if (1 == columnSize) {
            height = (int) measuredHeight / testResults.length;
        } else {
            int rows = (int) testResults.length / 2;

            if (5 > testResults.length) {
                ++rows;
            }

            height = (int) measuredHeight / rows;
        }

        view.getLayoutParams().height = height;

        return new AdapterTestResult.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterTestResult.ViewHolder holder, int position) {
        ItemVitalTestResult itemTestResult = testResults[position];

        String title = itemTestResult.getTitle();

        if (columnSize > 1) {
            title = title.replace("(", "\n(");
            holder.tvTitle.setMinLines(3);
            holder.tvTitle.setMaxLines(3);
        }

        holder.tvTitle.setText(title);

        LView.setImageFromFile(holder.ivPic, itemTestResult.getImage());

        if (position == selectedIndex) {
            holder.rlMain.setBackground(ContextCompat.getDrawable(context, R.drawable.bg_selected_answer));
        } else {
            holder.rlMain.setBackground(null);
        }
    }

    @Override
    public int getItemCount() {
        if (null != testResults) {
            return testResults.length;
        }

        return 0;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public String getResult() {
        try {
            return testResults[selectedIndex].getValue();
        } catch (Exception e) {
            return "";
        }
    }

    public int getViewHeight() {
        return viewHeight;
    }

    public void setViewHeight(int viewHeight) {
        this.viewHeight = viewHeight;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final RelativeLayout rlMain;

        private final ImageView ivPic;
        private final TextView  tvTitle;

        ViewHolder(View itemView) {
            super(itemView);

            rlMain = (RelativeLayout) itemView.findViewById(R.id.rl_main);
            ivPic = (ImageView) itemView.findViewById(R.id.iv_pic);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int previousSelectedIndex = selectedIndex;
            selectedIndex = getAdapterPosition();
            notifyItemChanged(selectedIndex);
            notifyItemChanged(previousSelectedIndex);
        }
    }
}