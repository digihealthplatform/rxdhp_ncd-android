package com.dhp.screening.data;

import android.content.Context;

import com.dhp.screening.BuildConfig;
import com.dhp.screening.data.di.ApplicationContext;
import com.dhp.screening.data.item.ItemBluetoothDevice;
import com.dhp.screening.data.item.ItemSummary;
import com.dhp.screening.data.item.ItemSurveyDetails;
import com.dhp.screening.data.item.ItemUserSurveySummary;
import com.dhp.screening.data.item.ItemVitalHeader;
import com.dhp.screening.data.manager.DatabaseHelper;
import com.dhp.screening.data.manager.SharedPrefsHelper;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.data.table.TableKitConfig;
import com.dhp.screening.data.table.TableProblems;
import com.dhp.screening.data.table.TableSociodemographics;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN4_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_DEVICE_NUMBER_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_EXPORT_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_HB_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_IMPORT_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_PARTNER_ID_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.ADMIN_TYPE;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_ASSETS_COPIED_VERSION;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_DEVICE_NUMBER;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.KEY_REMEMBER_LOG_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.LOGGED_IN_USER_ID;
import static com.dhp.screening.data.manager.SharedPrefsHelper.NOT_LOGGED_IN;
import static com.dhp.screening.data.manager.SharedPrefsHelper.USER_LOGGED_IN;

@Singleton
public class DataManager {
    private Context           context;
    private DatabaseHelper    databaseHelper;
    private SharedPrefsHelper sharedPrefsHelper;

    public static final int PATIENT_INFO        = 100;
    public static final int SOCIODEMOGRAPHICS   = 101;
    public static final int SCREENING_QUESTIONS = 102;
    public static final int VITALS              = 103;

    @Inject
    public DataManager(@ApplicationContext Context context,
                       DatabaseHelper databaseHelper,
                       SharedPrefsHelper sharedPrefsHelper) {
        this.context           = context;
        this.databaseHelper    = databaseHelper;
        this.sharedPrefsHelper = sharedPrefsHelper;
    }

    public void createOrUpdateCampInfo(TableCampInfo campInfo) {
        databaseHelper.createOrUpdateCampInfo(campInfo);

        databaseHelper.updateTableKitWithPartnerDetails(getSurveyDetails());
    }

    public TableCampInfo getCampInfo() {
        return databaseHelper.getCampInfo();
    }

    public List<TableCampInfo> getCampInfo(boolean synced) {
        return databaseHelper.getCampInfo(synced);
    }

    public void createUser(TableUser user) {
        databaseHelper.createUser(user, getSurveyDetails());
    }

    public TableUser getUser(String username) {
        return databaseHelper.getUser(username);
    }

    public List<TableUser> getAllUsers() {
        return databaseHelper.getAllUsers();
    }

    public List<TableUser> getAllActiveUsers() {
        return databaseHelper.getAllActiveUsers();
    }

    public List<TableUser> getAllUsers(boolean synced) {
        return databaseHelper.getAllUsers(synced);
    }

    public void updateUser(TableUser user) {
        databaseHelper.updateUser(user, getSurveyDetails());
    }

    public void removeUser(TableUser user) {
        databaseHelper.removeUser(user);
    }

    public void createHouse(TableHouse tableHouse, List<TableDeathInfo> tableDeathInfoList) {
        databaseHelper.createHouse(tableHouse, tableDeathInfoList, getSurveyDetails());
    }

    private ItemSurveyDetails getSurveyDetails() {

        TableCampInfo tableCampInfo = getCampInfo();

        ItemSurveyDetails itemSurveyDetails = new ItemSurveyDetails();

        itemSurveyDetails.setSurveyorUserId(getSurveyorUserId());
        itemSurveyDetails.setDeviceNumber(getDeviceNumber());

        itemSurveyDetails.setPartnerId(getPartnerId());

        try {
            itemSurveyDetails.setDemoUser(getLoggedInUser().isDemoUser());
        } catch (Exception e) {
            LLog.printStackTrace(e);
        }

        if (null != tableCampInfo) {
            itemSurveyDetails.setCampName(tableCampInfo.getCampName());
            itemSurveyDetails.setDoorToDoor(tableCampInfo.isDoorToDoor());
        }

        return itemSurveyDetails;
    }

    public TableHouse getHouse(String houseId) {
        return databaseHelper.getHouse(houseId);
    }

    public List<TableHouse> getAllHouses() {
        return databaseHelper.getAllHouses(false, getSurveyDetails());
    }

    public List<TableHouse> getAllHousesSync(boolean synced) {
        return databaseHelper.getAllHousesSync(synced);
    }

    public List<TableHouse> getAllActiveHouses() {
        return databaseHelper.getAllHouses(true, getSurveyDetails());
    }

    public List<TableDeathInfo> getDeathInfoList(String houseId) {
        return databaseHelper.getDeathInfoList(houseId);
    }

    public List<TableDeathInfo> getAllDeathInfoList(boolean synced) {
        return databaseHelper.getAllDeathInfoList(synced);
    }

    public void updateHouse(TableHouse tableHouse, List<TableDeathInfo> tableDeathInfoList) {
        databaseHelper.updateHouse(tableHouse, tableDeathInfoList, getSurveyDetails());
    }

    public String createDemographics(TableDemographics patient) {
        return databaseHelper.createDemographics(patient, getSurveyDetails());
    }

    public String getDeviceNumber() {
        String deviceNumber = sharedPrefsHelper.getStringPref(KEY_DEVICE_NUMBER, "");
        return LUtils.prependZeros(deviceNumber, 3);
    }

    public String getPartnerId() {
        return databaseHelper.getPartnerId();
    }

    public TableDemographics getPatient(String registrationId) {
        return databaseHelper.getPatient(registrationId);
    }

    public TableDemographics getPatientByIdentity(String identityNumber) {
        return databaseHelper.getPatientByIdentity(identityNumber);
    }

    public TableDemographics searchPatient(String text) {
        return databaseHelper.searchPatient(text);
    }

    public List<TableDemographics> getAllPatients() {
        return databaseHelper.getAllPatients(getSurveyDetails());
    }

    public List<TableDemographics> getAllPatients(boolean synced) {
        return databaseHelper.getAllPatients(synced);
    }

    public List<TableDemographics> searchPatients(String queryString) {
        return databaseHelper.searchPatients(queryString);
    }

    public void updatePatient(TableDemographics patient) {
        databaseHelper.updatePatient(patient, getSurveyDetails());
    }

    public void createSociodemographics(TableSociodemographics sociodemographics, String registrationId) {
        databaseHelper.createSociodemographics(sociodemographics, registrationId, getSurveyDetails());
    }

    public TableSociodemographics getSociodemographics(String registrationId) {
        return databaseHelper.getSociodemographics(registrationId);
    }

    public List<TableSociodemographics> getAllSociodemographics(boolean synced) {
        return databaseHelper.getAllSociodemographics(synced);
    }

    public void updateSociodemographics(TableSociodemographics sociodemographics) {
        databaseHelper.updateSociodemographics(sociodemographics, getSurveyDetails());
    }

    public void addOrUpdateQuestion(String registrationId, TableProblems question) {
        databaseHelper.addOrUpdateQuestion(registrationId, getSurveyDetails(), question);
    }

    public List<TableProblems> getAllQuestions(String registrationId) {
        return databaseHelper.getAllQuestions(registrationId);
    }

    public List<TableProblems> getAllQuestions(boolean synced) {
        return databaseHelper.getAllQuestions(synced);
    }

    public void addVital(TableVitals vitals, String deviceId) {
        databaseHelper.addVital(vitals, getSurveyDetails(), deviceId.toUpperCase());
    }

    public TableVitals getVital(int tableKey) {
        return databaseHelper.getVital(tableKey);
    }

    public void updateVital(TableVitals vitals, String deviceId) {
        databaseHelper.updateVital(vitals, getSurveyDetails(), deviceId.toUpperCase());
    }

    public List<TableVitals> getAllVitals(String registrationId, boolean showInactive) {
        return databaseHelper.getAllVitals(registrationId, showInactive);
    }

    public List<TableVitals> getAllVitals() {
        return databaseHelper.getAllVitals(getSurveyDetails());
    }

    public List<TableVitals> getAllVitals(boolean synced) {
        return databaseHelper.getAllVitals(synced);
    }

    public List<ItemVitalHeader> getAllVitalHeaders(String registrationId) {
        return databaseHelper.getAllVitalHeaders(registrationId);
    }

    public String getLatestHeight(String registrationId, long timeMillis) {
        return databaseHelper.getLatestHeight(registrationId, timeMillis);
    }

    public String getHeightOnDate(String registrationId, String date) {
        return databaseHelper.getHeightOnDate(registrationId, date);
    }

    public void setPref(String key, int value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public void setPref(String key, long value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public void setPref(String key, boolean value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public void setPref(String key, String value) {
        sharedPrefsHelper.setPref(key, value);
    }

    public int getIntPref(String key, int defaultValue) {
        return sharedPrefsHelper.getIntPref(key, defaultValue);
    }

    public long getLongPref(String key, long defaultValue) {
        return sharedPrefsHelper.getLongPref(key, defaultValue);
    }

    public boolean getBooleanPref(String key, boolean defaultValue) {
        return sharedPrefsHelper.getBooleanPref(key, defaultValue);
    }

    public String getStringPref(String key, String defaultValue) {
        return sharedPrefsHelper.getStringPref(key, defaultValue);
    }

    public void adminLoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void adminHbLoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN_HB_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void admin4LoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN4_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void adminImportLoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN_IMPORT_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void adminExportLoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN_EXPORT_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void adminPartnerIdLoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN_PARTNER_ID_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void adminDeviceNumberLoggedIn(boolean rememberLogin) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, ADMIN_DEVICE_NUMBER_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
    }

    public void userLoggedIn(boolean rememberLogin, TableUser tableUser) {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, USER_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, rememberLogin);
        sharedPrefsHelper.setPref(LOGGED_IN_USER_ID, tableUser.getUserId());
    }

    public void logout() {
        sharedPrefsHelper.setPref(KEY_LOGGED_IN, NOT_LOGGED_IN);
        sharedPrefsHelper.setPref(KEY_REMEMBER_LOG_IN, false);
        sharedPrefsHelper.setPref(LOGGED_IN_USER_ID, "");
        sharedPrefsHelper.setPref(ADMIN_TYPE, 0);
    }

    public TableUser validateUserLogin(String username, String password) {
        TableUser tableUser = getUser(username);

        if (null != tableUser && password.equals(tableUser.getPassword())) {
            return tableUser;
        }

        return null;
    }

    public boolean isAllowPrescription() {
        return getLoggedInUser().isAllowPrescription();
    }

    public TableUser getLoggedInUser() {
        return databaseHelper.getUser(getSurveyorUserId());
    }

    private String getSurveyorUserId() {
        return sharedPrefsHelper.getStringPref(LOGGED_IN_USER_ID, "");
    }

    public boolean isInactive(String registrationId, int type) {
        return databaseHelper.isInactive(registrationId, type);
    }

    public boolean isIdentityExists(TableDemographics tableDemographics) {
        return databaseHelper.isIdentityExists(tableDemographics);
    }

    public boolean isSimilarDemographicExists(TableDemographics tableDemographics) {
        return databaseHelper.isSimilarDemographicExists(tableDemographics, getSurveyDetails());
    }

    public ItemSummary getSurveySummary() {
        return databaseHelper.getSurveySummary(getSurveyDetails());
    }

    public boolean checkVitalEnteredAlready(String registrationId, String vitalSubtype) {
        return databaseHelper.checkVitalEnteredAlready(registrationId, vitalSubtype);
    }

    public ArrayList<String> getMissingDemographics() {
        return databaseHelper.getMissingDemographics(getSurveyDetails());
    }

    public void merge(TableUser tableUser) {
        databaseHelper.merge(tableUser);
    }

    public void merge(TableKitConfig tableKitConfig) {
        databaseHelper.merge(tableKitConfig);
    }

    public void merge(TableDemographics tableDemographics) {
        databaseHelper.merge(tableDemographics);
    }

    public void merge(TableSociodemographics tableSociodemographics) {
        databaseHelper.merge(tableSociodemographics);
    }

    public void merge(TableProblems tableProblems) {
        databaseHelper.merge(tableProblems);
    }

    public void merge(TableImage tableImage) {
        databaseHelper.merge(tableImage);
    }

    public void merge(TableHouse tableHouse) {
        databaseHelper.merge(tableHouse);
    }

    public void merge(TableDeathInfo tableDeathInfo) {
        databaseHelper.merge(tableDeathInfo);
    }

    public void merge(TableVitals tableVitals) {
        databaseHelper.merge(tableVitals);
    }

    public ArrayList<ItemUserSurveySummary> getUsersSummary(long startDateMillis, long endDateMillis) {
        return databaseHelper.getUsersSummary(getSurveyDetails(), startDateMillis, endDateMillis);
    }

    public ItemUserSurveySummary getUserSummary(long startDateMillis, long endDateMillis, String userId) {
        return databaseHelper.getUserSummary(startDateMillis, endDateMillis, userId);
    }

    public void addStock(TableStock tableStock) {
        databaseHelper.addStock(tableStock, getSurveyDetails());
    }

    public void updateStock(TableStock tableStock) {
        databaseHelper.updateStock(tableStock, getSurveyDetails());
    }

    public TableStock getStock(int tableKey) {
        return databaseHelper.getStock(tableKey);
    }

    public List<TableStock> getStockItems() {
        return databaseHelper.getStockItems();
    }

    public List<TableStock> getStockItemsReport() {
        return databaseHelper.getStockItemsReport();
    }

    public List<TableStock> getStockItems(boolean synced) {
        return databaseHelper.getStockItems(synced);
    }

    public List<TableImage> getImages(boolean synced) {
        return databaseHelper.getImages(synced);
    }

    public List<TableImage> getImages(String registrationId) {
        return databaseHelper.getImages(registrationId);
    }

    public void onAssetsCopied() {
        sharedPrefsHelper.setPref(KEY_ASSETS_COPIED_VERSION, BuildConfig.VERSION_CODE);
    }

    public boolean isCopyAssetsNeeded() {
        return BuildConfig.VERSION_CODE > sharedPrefsHelper.getIntPref(KEY_ASSETS_COPIED_VERSION, 0);
    }

    public boolean toShowPrescription() {
        return databaseHelper.toShowPrescription();
    }

    public String getHeadOfHouse(String houseId) {
        return databaseHelper.getHeadOfHouse(houseId);
    }

    public String getAddress(String houseId) {
        return databaseHelper.getAddress(houseId);
    }

    public int getAdminType() {
        return sharedPrefsHelper.getIntPref(ADMIN_TYPE, 0);
    }

    public void setAdminType(int adminType) {
        sharedPrefsHelper.setPref(ADMIN_TYPE, adminType);
    }

    public String getPdfPassword() {
        return ("dhp" + getPartnerId()).toLowerCase();
    }

    public int getHouseMembersCount(String houseId) {
        return databaseHelper.getHouseMembersCount(houseId);
    }

    public int getHouseMembersCount(TableHouse tableHouse) {
        return databaseHelper.getHouseMembersCount(tableHouse);
    }

    public List<TableDemographics> getHouseMembers(String houseId) {
        return databaseHelper.getHouseMembers(houseId, sharedPrefsHelper.getBooleanPref("show_inactive_house_members", false));
    }

    public List<TableDemographics> getHouseMembers(TableHouse tableHouse) {
        return databaseHelper.getHouseMembers(tableHouse, sharedPrefsHelper.getBooleanPref("show_inactive_house_members", false));
    }

    public void updateBluetoothDevices(ItemBluetoothDevice[] itemBluetoothDevices) {
        databaseHelper.updateBluetoothDevices(itemBluetoothDevices, getSurveyDetails());
    }

    public ArrayList<ItemBluetoothDevice> getBluetoothDevices(String type) {
        return databaseHelper.getBluetoothDevice(type);
    }

    public List<TableKitConfig> getAllKitConfig(boolean synced) {
        return databaseHelper.getAllKitConfig(synced);
    }

    public void createImage(TableImage tableImage) {
        databaseHelper.createImage(tableImage, getSurveyDetails());
    }

    public String getProfilePic(String registrationId) {
        return databaseHelper.getProfilePic(registrationId);
    }

    public TableImage getImageTable(int tableKey) {
        return databaseHelper.getImageTable(tableKey);
    }

    public void updateImage(TableImage tableImage, String deviceId) {
        databaseHelper.updateImage(tableImage, getSurveyDetails(), deviceId.toUpperCase());
    }

    public void clearDatabase() {
        databaseHelper.clearDatabase();
    }
}