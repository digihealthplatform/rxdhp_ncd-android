package com.dhp.screening.data.item;


public class ItemFile {

    private String name;
    private String localPath;
    private String fileUrl;
    private String fileStoredName;

    private boolean isCamera;

    private long size;

    public ItemFile() {
    }

    public ItemFile(String name, String localPath, String fileStoredName, String fileUrl) {
        this.name = name;
        this.localPath = localPath;
        this.fileStoredName = fileStoredName;
        this.fileUrl = fileUrl;
    }

    public ItemFile(String name, String localPath) {
        this.name = name;
        this.localPath = localPath;
    }

    public ItemFile(String name, String localPath, String fileUrl) {
        this.name = name;
        this.localPath = localPath;
        this.fileUrl = fileUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileStoredName() {
        return fileStoredName;
    }

    public void setFileStoredName(String fileStoredName) {
        this.fileStoredName = fileStoredName;
    }

    public boolean isCamera() {
        return isCamera;
    }

    public void setCamera(boolean camera) {
        isCamera = camera;
    }
}
