package com.dhp.screening.data.item;

public class ItemQuestionChildren {

    private int    headerIndex;
    private int    childIndex;
    private String question;
    private String answer;
    private String groupName;

    private int questionOrder;

    public ItemQuestionChildren(int headerIndex, int childIndex, String question, String answer, String groupName) {
        this.headerIndex = headerIndex;
        this.childIndex = childIndex;
        this.question = question;
        this.answer = answer;
        this.groupName = groupName;
    }

    public int getHeaderIndex() {
        return headerIndex;
    }

    public void setHeaderIndex(int headerIndex) {
        this.headerIndex = headerIndex;
    }

    public int getChildIndex() {
        return childIndex;
    }

    public void setChildIndex(int childIndex) {
        this.childIndex = childIndex;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getQuestionOrder() {
        return questionOrder;
    }

    public void setQuestionOrder(int questionOrder) {
        this.questionOrder = questionOrder;
    }
}