package com.dhp.screening.data.adapter;

import android.content.Context;
import android.util.AttributeSet;

public class SpinnerColors extends android.support.v7.widget.AppCompatSpinner {

    public SpinnerColors(Context context) {
        super(context);
    }

    public SpinnerColors(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpinnerColors(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setSelection(int position, boolean animate) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position, animate);
        if (sameSelected) {
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }

    @Override
    public void setSelection(int position) {
        boolean sameSelected = position == getSelectedItemPosition();
        super.setSelection(position);
        if (sameSelected) {
            getOnItemSelectedListener().onItemSelected(this, getSelectedView(), position, getSelectedItemId());
        }
    }
}