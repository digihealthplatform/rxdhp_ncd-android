package com.dhp.screening.data.item;

public class ItemVitalValue {
    private String vital;
    private String value;
    private String unit;

    public ItemVitalValue(String vital, String value, String unit) {
        this.vital = vital;
        this.value = value;
        this.unit = unit;
    }

    public String getVital() {
        return vital;
    }

    public void setVital(String vital) {
        this.vital = vital;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}