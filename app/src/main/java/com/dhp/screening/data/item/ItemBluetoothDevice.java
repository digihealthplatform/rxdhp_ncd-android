package com.dhp.screening.data.item;


import com.google.gson.annotations.SerializedName;

public class ItemBluetoothDevice {

    @SerializedName("device_type")
    private String deviceType;

    @SerializedName("device_name")
    private String deviceName;

    @SerializedName("mac_address")
    private String macAddress;

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }
}