package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dhp.screening.R;
import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.ui.admin.ActivityAddUser;
import com.dhp.screening.ui.admin.ActivityViewUser;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class AdapterUser extends RecyclerView.Adapter<AdapterUser.ViewHolder> {

    private List<TableUser> tableUsers;

    private final Context        context;
    private final LayoutInflater layoutInflater;

    private TextView     tvEmpty;
    private RecyclerView rvUserList;

    private DataManager dataManager;

    public AdapterUser(Context context, DataManager dataManager, RecyclerView rvUserList, TextView tvEmpty) {
        this.context = context;
        this.dataManager = dataManager;

        layoutInflater = LayoutInflater.from(context);

        this.rvUserList = rvUserList;
        this.tvEmpty = tvEmpty;

        showHideEmpty();

        tableUsers = dataManager.getAllUsers();
    }

    @Override
    public AdapterUser.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvUserName.setText(tableUsers.get(position).getFullName());

        if (tableUsers.get(position).isInactive()) {
            holder.rlBackground.setBackground(ContextCompat
                    .getDrawable(context, R.drawable.item_inactive));
        } else {
            holder.rlBackground.setBackground(ContextCompat
                    .getDrawable(context, R.drawable.ripple_effect));
        }
    }

    @Override
    public int getItemCount() {
        tableUsers = dataManager.getAllUsers();

        if (null != tableUsers) {
            return tableUsers.size();
        }

        return 0;
    }

    private void showHideEmpty() {
        if (0 == dataManager.getAllUsers().size()) {
            tvEmpty.setVisibility(View.VISIBLE);
            rvUserList.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            rvUserList.setVisibility(View.VISIBLE);
        }
    }

    private void removeItem(int position) {
        dataManager.removeUser(tableUsers.get(position));
        tableUsers.remove(position);
        notifyItemRemoved(position);
        Toasty.success(context, AssetReader.getLangKeyword("user_removed"), Toast.LENGTH_SHORT).show();

        showHideEmpty();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener {

        private TextView       tvUserName;
        private RelativeLayout rlBackground;

        ViewHolder(View itemView) {
            super(itemView);

            tvUserName = (TextView) itemView.findViewById(R.id.tv_user_name);
            rlBackground = (RelativeLayout) itemView.findViewById(R.id.rl_background);

            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public boolean onLongClick(View view) {

            if (tableUsers.get(getAdapterPosition()).isInactive()) {
                return true;
            }

            String options_array[] = {AssetReader.getLangKeyword("edit"), AssetReader.getLangKeyword("remove")};

            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            updateUser(tableUsers.get(getAdapterPosition()));
                        }
                        break;

                        case 1: {
                            AlertDialog.Builder builder;

                            builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

                            builder.setMessage(AssetReader.getLangKeyword("confirm_remove_user"));
                            builder.setPositiveButton(AssetReader.getLangKeyword("remove"), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    removeItem(getAdapterPosition());
                                }
                            });
                            builder.setNegativeButton(AssetReader.getLangKeyword("cancel"), null);
                            builder.show();
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityViewUser.class);
            intent.putExtra("username", tableUsers.get(getAdapterPosition()).getUserId());
            context.startActivity(intent);
        }
    }

    private void updateUser(final TableUser tableUser) {
        Intent intent = new Intent(context, ActivityAddUser.class);
        intent.putExtra("is_edit", true);
        intent.putExtra("username", tableUser.getUserId());
        context.startActivity(intent);
    }
}