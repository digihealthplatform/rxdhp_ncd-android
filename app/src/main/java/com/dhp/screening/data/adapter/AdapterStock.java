package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhp.screening.R;
import com.dhp.screening.data.asset.AssetReader;
import com.dhp.screening.data.table.TableStock;
import com.dhp.screening.ui.stock.ActivityAddStock;
import com.dhp.screening.ui.stock.ActivityViewStock;

import java.util.List;

public class AdapterStock extends RecyclerView.Adapter<AdapterStock.ViewHolder> {
    private final Context          context;
    private       List<TableStock> items;
    private final LayoutInflater   layoutInflater;

    public AdapterStock(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
    }

    public void setItems(List<TableStock> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public AdapterStock.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_stock, parent, false);
        return new AdapterStock.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterStock.ViewHolder holder, int position) {
        holder.tvItem.setText(items.get(position).getItemName());
        holder.tvItemQuantity.setText("Quantity: " + items.get(position).getQuantity());

        if (items.get(position).isInactive()) {
            holder.llBackground.setBackground(ContextCompat
                    .getDrawable(context, R.drawable.item_inactive));
        } else {
            holder.llBackground.setBackground(ContextCompat
                    .getDrawable(context, R.drawable.ripple_effect));
        }
    }

    @Override
    public int getItemCount() {
        if (null != items) {
            return items.size();
        }

        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private final TextView     tvItem;
        private final TextView     tvItemQuantity;
        private final LinearLayout llBackground;

        ViewHolder(View itemView) {
            super(itemView);
            tvItem = (TextView) itemView.findViewById(R.id.tv_item_name);
            tvItemQuantity = (TextView) itemView.findViewById(R.id.tv_item_quantity);
            llBackground = (LinearLayout) itemView.findViewById(R.id.ll_background);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ActivityViewStock.class);
            intent.putExtra("table_key", items.get(getAdapterPosition()).getTableKey());
            context.startActivity(intent);
        }

        @Override
        public boolean onLongClick(View view) {

            if (items.get(getAdapterPosition()).isInactive()) {
                return true;
            }

            String options_array[] = {AssetReader.getLangKeyword("edit")};

            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogLight);

            builder.setItems(options_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    switch (which) {
                        case 0: {
                            Intent intent = new Intent(context, ActivityAddStock.class);
                            intent.putExtra("is_edit", true);
                            intent.putExtra("table_key", items.get(getAdapterPosition()).getTableKey());
                            context.startActivity(intent);
                        }
                        break;
                    }
                }
            });

            builder.show();

            return true;
        }
    }
}