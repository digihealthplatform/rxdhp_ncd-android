package com.dhp.screening.data.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.dhp.screening.R;
import com.dhp.screening.data.StaticData;
import com.dhp.screening.data.item.ItemVital;
import com.dhp.screening.data.item.ItemVitalHeader;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.ui.vital.ActivityVitalDetails;
import com.dhp.screening.util.LLog;
import com.dhp.screening.util.LUtils;

import static com.dhp.screening.data.item.ItemVital.VITAL_CHILD;
import static com.dhp.screening.data.item.ItemVital.VITAL_HEADER;
import static com.dhp.screening.util.LUtils.getFormattedDateTimeAm;

public class AdapterVitals extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "AdapterVitals";

    private List<ItemVitalHeader> vitalHeaders;

    private List<ItemVital> vitals;

    private final Context        context;
    private final LayoutInflater layoutInflater;

    public AdapterVitals(Context context, List<ItemVitalHeader> vitalHeaders, List<ItemVital> vitals) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.vitalHeaders = vitalHeaders;
        this.vitals = vitals;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = layoutInflater.inflate(R.layout.item_vital, parent, false);
//        return new AdapterVitals.HeaderHolder(view);
        View view;

        switch (viewType) {
            case VITAL_HEADER: {
                view = layoutInflater.inflate(R.layout.item_vital_header, parent, false);
                return new HeaderHolder(view);
            }

            case VITAL_CHILD: {
                view = layoutInflater.inflate(R.layout.item_vital, parent, false);
                return new ViewHolder(view);
            }
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case VITAL_HEADER: {

                String headerText  = vitals.get(position).getHeader().getDate();
                int    totalVitals = vitals.get(position).getHeader().getVitals().size();

                if (1 == totalVitals) {
                    headerText += " - 1 Vital";
                } else {
                    headerText += " - " + totalVitals + " Vitals";
                }

                ((HeaderHolder) holder).tvVitalTitle.setText(headerText);

                if (vitals.get(position).getHeader().isVitalsShown()) {
                    ((HeaderHolder) holder).ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_up));
                }
            }
            break;

            case VITAL_CHILD: {
                TableVitals vital = vitals.get(position).getVital();

                ((ViewHolder) holder).tvVitalType.setText(vital.getVitalType());

                ((ViewHolder) holder).tvVitalDateTime
                        .setText(getFormattedDateTimeAm(vital.getVitalDateTimeMillis()).split(",")[0]);

                if (vital.isInactive()) {
                    ((ViewHolder) holder).tvInactive.setText("Inactive");
                    ((ViewHolder) holder).rlVitalItem.setBackground(ContextCompat
                            .getDrawable(context, R.drawable.item_inactive));
                } else {
                    ((ViewHolder) holder).tvInactive.setText("");
                    ((ViewHolder) holder).rlVitalItem.setBackground(ContextCompat
                            .getDrawable(context, R.drawable.ripple_effect));
                }
            }
            break;
        }
    }

    @Override
    public int getItemCount() {
        if (null != vitals) {
            return vitals.size();
        }

        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return vitals.get(position).getVitalType();
    }


    public List<TableVitals> getVitalsList(String startDate, String endDate) {
        final long startDateLong = LUtils.getLongTime(startDate);
        final long endDateLong   = LUtils.getLongTime(endDate);

        List<TableVitals> vitals = new ArrayList<>();

        for (ItemVitalHeader header : vitalHeaders) {

            LLog.d(TAG, "getVitalsList: ");

            if (LUtils.getLongTime(header.getDate()) >= startDateLong
                    && LUtils.getLongTime(header.getDate()) <= endDateLong) {
                vitals.addAll(header.getVitals());
//                break;
            }
        }

        return vitals;
    }

    public List<TableVitals> getVitalsList(String date) {
        List<TableVitals> vitals = new ArrayList<>();

        for (ItemVitalHeader header : vitalHeaders) {
            if (header.getDate().equals(date)) {
                vitals.addAll(header.getVitals());
                break;
            }
        }

        return vitals;
    }

    class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView  tvVitalTitle;
        private ImageView ivArrow;

        HeaderHolder(View itemView) {
            super(itemView);
            tvVitalTitle = (TextView) itemView.findViewById(R.id.tv_title);
            ivArrow = (ImageView) itemView.findViewById(R.id.iv_arrow);

            itemView.setOnClickListener(this);
//            ivArrow.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String date = vitals.get(getAdapterPosition()).getHeader().getDate();

            int headerIndex     = getHeaderIndex(date);
            int headerIndexList = getHeaderIndexList(date);

            ItemVitalHeader vitalHeader = vitalHeaders.get(headerIndex);

            if (vitalHeader.isVitalsShown()) {
                for (int i = 0; i < vitalHeader.getVitals().size(); ++i) {
                    vitals.remove(headerIndexList + 1);
                    notifyItemRemoved(headerIndexList + 1);
                }

                ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_down));
                vitalHeaders.get(headerIndex).setVitalsShown(false);

            } else {
                for (TableVitals vital : vitalHeader.getVitals()) {
                    vitals.add(headerIndexList + 1, new ItemVital(VITAL_CHILD, null, vital));
                    notifyItemInserted(headerIndexList + 1);
                    ++headerIndexList;
                }

                ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_up));
                vitalHeaders.get(headerIndex).setVitalsShown(true);
            }
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView       tvVitalType;
        private TextView       tvVitalDateTime;
        private TextView       tvInactive;
        private RelativeLayout rlVitalItem;

        ViewHolder(View itemView) {
            super(itemView);
            tvVitalType = (TextView) itemView.findViewById(R.id.tv_vital_type);
            tvVitalDateTime = (TextView) itemView.findViewById(R.id.tv_date_time);
            tvInactive = (TextView) itemView.findViewById(R.id.tv_inactive);
            rlVitalItem = (RelativeLayout) itemView.findViewById(R.id.rl_vital_item);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            StaticData.setCurrentVital(vitals.get(getAdapterPosition()).getVital());
            context.startActivity(new Intent(context, ActivityVitalDetails.class));
        }
    }

    private int getHeaderIndex(String date) {
        int i = 0;

        for (ItemVitalHeader vitalHeader : vitalHeaders) {
            if (vitalHeader.getDate().equals(date)) {
                break;
            }
            ++i;
        }

        return i;
    }

    private int getHeaderIndexList(String date) {
        int i = 0;

        for (ItemVital vital : vitals) {
            if (VITAL_HEADER == vital.getVitalType()) {
                if (vital.getHeader().getDate().equals(date)) {
                    break;
                }

            } else {
                if (vital.getVital().getVitalDateTime().split(" ")[0].equals(date)) {
                    break;
                }
            }

            ++i;
        }

        return i;
    }
}