package com.dhp.screening.data.range;

public class ItemAnalysisCount {

    private int firstRange;
    private int secondRange;
    private int thirdRange;

    public int getFirstRange() {
        return firstRange;
    }

    public void setFirstRange(int firstRange) {
        this.firstRange = firstRange;
    }

    public int getSecondRange() {
        return secondRange;
    }

    public void setSecondRange(int secondRange) {
        this.secondRange = secondRange;
    }

    public int getThirdRange() {
        return thirdRange;
    }

    public void setThirdRange(int thirdRange) {
        this.thirdRange = thirdRange;
    }
}