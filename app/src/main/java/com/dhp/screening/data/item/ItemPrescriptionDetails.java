package com.dhp.screening.data.item;

public class ItemPrescriptionDetails {

    private String prescription;
    private int    days;
    private String timings;
    private String food;
    private String new_or_existing = "";

    public String getPrescription() {
        return prescription;
    }

    public void setPrescription(String prescription) {
        this.prescription = prescription;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getTimings() {
        return timings;
    }

    public void setTimings(String timings) {
        this.timings = timings;
    }

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public String getNew_or_existing() {
        return new_or_existing;
    }

    public void setNew_or_existing(String new_or_existing) {
        this.new_or_existing = new_or_existing;
    }
}
