package com.dhp.screening.data.merger;

import android.content.Context;
import android.database.Cursor;

import com.dhp.screening.data.DataManager;
import com.dhp.screening.data.table.TableCampInfo;
import com.dhp.screening.data.table.TableDeathInfo;
import com.dhp.screening.data.table.TableDemographics;
import com.dhp.screening.data.table.TableHouse;
import com.dhp.screening.data.table.TableImage;
import com.dhp.screening.data.table.TableKitConfig;
import com.dhp.screening.data.table.TableProblems;
import com.dhp.screening.data.table.TableSociodemographics;
import com.dhp.screening.data.table.TableUser;
import com.dhp.screening.data.table.TableVitals;
import com.dhp.screening.util.LConstants;
import com.dhp.screening.util.LToast;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.File;

import static com.dhp.screening.util.LUtils.toBoolean;


public class ImportDataEncrypted {

    public static boolean mergeDb(Context context, DataManager dataManager, String fromPath, int mergeReplace) {

        try {
            SQLiteDatabase.loadLibs(context);

            File databaseFile = new File(fromPath);

            SQLiteDatabase oldDb = SQLiteDatabase.openOrCreateDatabase(databaseFile, LConstants.DATABASE_ENCRYPTION_KEY, null);

            int version = oldDb.getVersion();

            if (1 == mergeReplace) {
                dataManager.clearDatabase();
            }

            mergeUsers(dataManager, oldDb);
            mergeCampInfo(dataManager, oldDb);
            mergeDemographics(dataManager, oldDb);
            mergeSociodemographics(dataManager, oldDb);
            mergeProblems(dataManager, oldDb);
            mergeHouse(dataManager, oldDb);
            mergeDeathInfo(dataManager, oldDb);
            mergeVitals(dataManager, oldDb);
            mergeImages(dataManager, oldDb);

            if (3 <= oldDb.getVersion()) {
                mergeKitConifg(dataManager, oldDb);
            }

            // TODO: 07-Nov-18, mergeStocks,

            LToast.success("Imported successfully");

            oldDb.close();

            return true;

        } catch (Exception e) {
            return false;
        }
    }

    private static void mergeUsers(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableUser", null);

        if (cursor.moveToFirst()) {
            do {
                TableUser tableUser = new TableUser();

                tableUser.setUserId(cursor.getString(1));
                tableUser.setFullName(cursor.getString(2));
                tableUser.setPassword(cursor.getString(3));
                tableUser.setUserType(cursor.getString(4));
                tableUser.setGender(cursor.getString(5));
                tableUser.setDob(cursor.getString(6));
                tableUser.setYob(cursor.getInt(7));
                tableUser.setIdentityType(cursor.getString(8));
                tableUser.setIdentityNumber(cursor.getString(9));
                tableUser.setPhoneNumber(cursor.getString(10));
                tableUser.setEmailId(cursor.getString(11));
                tableUser.setHouseNo(cursor.getString(12));
                tableUser.setStreet(cursor.getString(13));
                tableUser.setPost(cursor.getString(14));
                tableUser.setLocality(cursor.getString(15));
                tableUser.setDistrict(cursor.getString(16));
                tableUser.setState(cursor.getString(17));
                tableUser.setCountry(cursor.getString(18));
                tableUser.setPinCode(cursor.getInt(19));
                tableUser.setAllowPrescription(toBoolean(cursor.getInt(20)));
                tableUser.setDemoUser(toBoolean(cursor.getInt(21)));
                tableUser.setQrScanned(toBoolean(cursor.getInt(22)));

                tableUser.setPartnerId(cursor.getString(23));
                tableUser.setCampName(cursor.getString(24));
                tableUser.setDeviceNumber(cursor.getString(25));

                tableUser.setDeviceId(cursor.getString(26));
                tableUser.setEntryDateTime(cursor.getString(27));
                tableUser.setEntryDateTimeMillis(cursor.getLong(28));
                tableUser.setInactive(toBoolean(cursor.getInt(29)));
                tableUser.setInactiveReason(cursor.getString(30));
                tableUser.setModified(toBoolean(cursor.getInt(31)));
                tableUser.setModifiedDateTime(cursor.getString(32));
                tableUser.setSynced(toBoolean(cursor.getInt(33)));

                dataManager.merge(tableUser);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeDemographics(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableDemographics", null);

        if (cursor.moveToFirst()) {
            do {
                TableDemographics tableDemographics = new TableDemographics();
                tableDemographics.setRegistrationId(cursor.getString(1));
                tableDemographics.setEhrNumber(cursor.getString(2));
                tableDemographics.setFullName(cursor.getString(3));
                tableDemographics.setGender(cursor.getString(4));
                tableDemographics.setYob(cursor.getInt(5));
                tableDemographics.setDob(cursor.getString(6));
                tableDemographics.setPhoneNumber(cursor.getString(7));
                tableDemographics.setEmailId(cursor.getString(8));
                tableDemographics.setIdentityType(cursor.getString(9));
                tableDemographics.setIdentityNumber(cursor.getString(10));
                tableDemographics.setHouseId(cursor.getString(11));
                tableDemographics.setCareOf(cursor.getString(12));
                tableDemographics.setImageName(cursor.getString(13));
                tableDemographics.setImagePath(cursor.getString(14));
                tableDemographics.setNotes(cursor.getString(15));
                tableDemographics.setDoorToDoor(toBoolean(cursor.getInt(16)));
                tableDemographics.setGpsLocation(cursor.getString(17));
                tableDemographics.setQrScanned(toBoolean(cursor.getInt(18)));
                tableDemographics.setSurveyorUserId(cursor.getString(19));
                tableDemographics.setPartnerId(cursor.getString(20));
                tableDemographics.setCampName(cursor.getString(21));
                tableDemographics.setDeviceNumber(cursor.getString(22));
                tableDemographics.setDeviceId(cursor.getString(23));
                tableDemographics.setSurveyDateTime(cursor.getString(24));
                tableDemographics.setSurveyDateTimeMillis(cursor.getLong(25));
                tableDemographics.setInactive(toBoolean(cursor.getInt(26)));
                tableDemographics.setInactiveReason(cursor.getString(27));
                tableDemographics.setModified(toBoolean(cursor.getInt(28)));
                tableDemographics.setModifiedDateTime(cursor.getString(29));
                tableDemographics.setSynced(toBoolean(cursor.getInt(30)));

                if (2 <= oldDb.getVersion()) {
                    tableDemographics.setMrNumber(cursor.getString(31));
                }

                dataManager.merge(tableDemographics);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeSociodemographics(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableSociodemographics", null);

        if (cursor.moveToFirst()) {
            do {
                TableSociodemographics tableSociodemographics = new TableSociodemographics();

                tableSociodemographics.setRegistrationId(cursor.getString(1));
                tableSociodemographics.setEhrNumber(cursor.getString(2));
                tableSociodemographics.setRelationToHoh(cursor.getString(3));
                tableSociodemographics.setMaritalStatus(cursor.getString(4));
                tableSociodemographics.setEducation(cursor.getString(5));
                tableSociodemographics.setLiteracy(cursor.getString(6));
                tableSociodemographics.setCurrentOccupation(cursor.getString(7));
                tableSociodemographics.setMonthlyIncome(cursor.getInt(8));
                tableSociodemographics.setHouseType(cursor.getString(9));
                tableSociodemographics.setLanguagesSpoken(cursor.getString(10));
                tableSociodemographics.setHabits(cursor.getString(11));

                tableSociodemographics.setExercise(cursor.getString(12));
                tableSociodemographics.setDiet(cursor.getString(13));
                tableSociodemographics.setDisease(cursor.getString(14));
                tableSociodemographics.setDiseaseTreatment(cursor.getString(15));
                tableSociodemographics.setDisability(cursor.getString(16));
                tableSociodemographics.setSick(cursor.getString(17));
                tableSociodemographics.setAilment(cursor.getString(18));

                tableSociodemographics.setNotes(cursor.getString(19));
                tableSociodemographics.setSurveyorUserId(cursor.getString(20));
                tableSociodemographics.setPartnerId(cursor.getString(21));
                tableSociodemographics.setCampName(cursor.getString(22));
                tableSociodemographics.setDeviceNumber(cursor.getString(23));
                tableSociodemographics.setDeviceId(cursor.getString(24));
                tableSociodemographics.setSurveyDateTime(cursor.getString(25));
                tableSociodemographics.setSurveyDateTimeMillis(cursor.getLong(26));
                tableSociodemographics.setInactive(toBoolean(cursor.getInt(27)));
                tableSociodemographics.setInactiveReason(cursor.getString(28));

                tableSociodemographics.setModified(toBoolean(cursor.getInt(29)));
                tableSociodemographics.setModifiedDateTime(cursor.getString(30));
                tableSociodemographics.setSynced(toBoolean(cursor.getInt(31)));

                if (2 <= oldDb.getVersion()) {
                    tableSociodemographics.setHealthCard(cursor.getString(32));
                }

                if (5 <= oldDb.getVersion()) {
                    tableSociodemographics.setMedicinesTakenDaily(cursor.getString(33));
                    tableSociodemographics.setDiseaseTestLastDone(cursor.getString(34));
                    tableSociodemographics.setInformedAboutDisease(cursor.getString(35));
                    tableSociodemographics.setMedicationForDisease(cursor.getString(36));
                    tableSociodemographics.setChangesToControlDisease(cursor.getString(37));
                    tableSociodemographics.setWomenSpecialPrecautionsOnPeriod(cursor.getString(38));
                    tableSociodemographics.setMobileUsed(cursor.getString(39));
                    tableSociodemographics.setMobileComfort(cursor.getString(40));
                    tableSociodemographics.setHealthRecordsStorage(cursor.getString(41));
                }

                dataManager.merge(tableSociodemographics);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeProblems(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableProblems", null);

        if (cursor.moveToFirst()) {
            do {
                TableProblems tableProblems = new TableProblems();
                tableProblems.setRegistrationId(cursor.getString(1));
                tableProblems.setEhrNumber(cursor.getString(2));
                tableProblems.setQuestionGroup(cursor.getString(3));
                tableProblems.setQuestion(cursor.getString(4));
                tableProblems.setQuestionOrder(cursor.getInt(5));
                tableProblems.setAnswer(cursor.getString(6));
                tableProblems.setSurveyorUserId(cursor.getString(7));
                tableProblems.setPartnerId(cursor.getString(8));
                tableProblems.setCampName(cursor.getString(9));
                tableProblems.setDeviceNumber(cursor.getString(10));
                tableProblems.setDeviceId(cursor.getString(11));
                tableProblems.setSurveyDateTime(cursor.getString(12));
                tableProblems.setSurveyDateTimeMillis(cursor.getLong(13));
                tableProblems.setInactive(toBoolean(cursor.getInt(14)));
                tableProblems.setInactiveReason(cursor.getString(15));
                tableProblems.setModified(toBoolean(cursor.getInt(16)));
                tableProblems.setModifiedDateTime(cursor.getString(17));
                tableProblems.setSynced(toBoolean(cursor.getInt(18)));

                dataManager.merge(tableProblems);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeImages(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableImage", null);

        if (cursor.moveToFirst()) {
            do {
                TableImage tableImage = new TableImage();
                tableImage.setRegistrationId(cursor.getString(1));
                tableImage.setEhrNumber(cursor.getString(2));

                tableImage.setImageType(cursor.getString(3));
                tableImage.setImageName(cursor.getString(4));
                tableImage.setGpsLocation(cursor.getString(5));
                tableImage.setNotes(cursor.getString(6));

                tableImage.setSurveyorUserId(cursor.getString(7));
                tableImage.setPartnerId(cursor.getString(8));
                tableImage.setCampName(cursor.getString(9));
                tableImage.setDeviceNumber(cursor.getString(10));
                tableImage.setDeviceId(cursor.getString(11));
                tableImage.setSurveyDateTime(cursor.getString(12));
                tableImage.setSurveyDateTimeMillis(cursor.getLong(13));
                tableImage.setInactive(toBoolean(cursor.getInt(14)));
                tableImage.setInactiveReason(cursor.getString(15));
                tableImage.setModified(toBoolean(cursor.getInt(16)));
                tableImage.setModifiedDateTime(cursor.getString(17));
                tableImage.setSynced(toBoolean(cursor.getInt(18)));

                if (4 <= oldDb.getVersion()) {
                    tableImage.setImageDateTime(cursor.getString(19));
                    tableImage.setImageDateTimeMillis(cursor.getLong(20));
                    tableImage.setImageCamera(toBoolean(cursor.getInt(21)));
                }

                dataManager.merge(tableImage);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeHouse(DataManager dataManager, SQLiteDatabase oldDb) {

        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableHouse", null);

        if (cursor.moveToFirst()) {
            do {
                TableHouse tableHouse = new TableHouse();

                tableHouse.setHouseId(cursor.getString(1));
                tableHouse.setHouseNo(cursor.getString(2));
                tableHouse.setStreet(cursor.getString(3));
                tableHouse.setPost(cursor.getString(4));
                tableHouse.setLocality(cursor.getString(5));
                tableHouse.setDistrict(cursor.getString(6));
                tableHouse.setState(cursor.getString(7));
                tableHouse.setCountry(cursor.getString(8));
                tableHouse.setPinCode(cursor.getInt(9));
                tableHouse.setHeadOfHouse(cursor.getString(10));
                tableHouse.setRespondent(cursor.getString(11));
                tableHouse.setTotalMembers(cursor.getInt(12));
                tableHouse.setHouseOwnership(cursor.getString(13));
                tableHouse.setRationCard(cursor.getString(14));
                tableHouse.setHealthInsurance(cursor.getString(15));
                tableHouse.setMealTimes(cursor.getString(16));
                tableHouse.setCookingFuel(cursor.getString(17));
                tableHouse.setTreatmentPlace(cursor.getString(18));
                tableHouse.setRecentDeath(cursor.getString(19));
                tableHouse.setNotes(cursor.getString(20));
                tableHouse.setGpsLocation(cursor.getString(21));
                tableHouse.setSurveyorUserId(cursor.getString(22));
                tableHouse.setPartnerId(cursor.getString(23));
                tableHouse.setCampName(cursor.getString(24));
                tableHouse.setDeviceNumber(cursor.getString(25));
                tableHouse.setDeviceId(cursor.getString(26));
                tableHouse.setSurveyDateTime(cursor.getString(27));
                tableHouse.setSurveyDateTimeMillis(cursor.getLong(28));
                tableHouse.setInactive(toBoolean(cursor.getInt(29)));
                tableHouse.setInactiveReason(cursor.getString(30));
                tableHouse.setModified(toBoolean(cursor.getInt(31)));
                tableHouse.setModifiedDateTime(cursor.getString(32));
                tableHouse.setSynced(toBoolean(cursor.getInt(33)));

                if (2 <= oldDb.getVersion()) {
                    tableHouse.setSpouseOfHeadOfHouse(cursor.getString(34));
                    tableHouse.setHouseStatus(cursor.getString(35));
                    tableHouse.setTotalRooms(cursor.getInt(36));
                    tableHouse.setToiletAvailability(cursor.getString(37));
                    tableHouse.setDrinkingWaterSource(cursor.getString(38));
                    tableHouse.setElectricityAvailability(cursor.getString(39));
                    tableHouse.setMotorisedVehicle(cursor.getString(40));
                    tableHouse.setVillage(cursor.getString(41));
                    tableHouse.setGramPanchayat(cursor.getString(42));
                }

                if (5 <= oldDb.getVersion()) {
                    tableHouse.setEarningMembers(cursor.getInt(43));
                    tableHouse.setTotalIncome(cursor.getInt(44));
                    tableHouse.setTreatmentCost(cursor.getInt(45));
                    tableHouse.setDrinkWeekly(cursor.getString(46));
                    tableHouse.setDrugsUse(cursor.getString(47));
                    tableHouse.setHandicappedPresent(cursor.getString(48));
                    tableHouse.setHandicappedSuffering(cursor.getString(49));
                    tableHouse.setPets(cursor.getString(50));
                    tableHouse.setPetsBitten(cursor.getString(51));
                    tableHouse.setPetsBiteAction(cursor.getString(52));
                    tableHouse.setDrinkingWaterCleaningMethod(cursor.getString(53));
                    tableHouse.setBathingToiletFacility(cursor.getString(54));
                    tableHouse.setChildHealthCheckupLastDone(cursor.getString(55));
                    tableHouse.setChildrenVaccinated(cursor.getString(56));
                    tableHouse.setVaccinationRecordsStorage(cursor.getString(57));
                    tableHouse.setChildGenderResponsibility(cursor.getString(58));
                    tableHouse.setPreferredChildGender(cursor.getString(59));
                }

                dataManager.merge(tableHouse);

            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private static void mergeDeathInfo(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableDeathInfo", null);

        if (cursor.moveToFirst()) {
            do {
                TableDeathInfo tableDeathInfo = new TableDeathInfo();
                tableDeathInfo.setHouseId(cursor.getString(1));
                tableDeathInfo.setPersonName(cursor.getString(2));
                tableDeathInfo.setGender(cursor.getString(3));
                tableDeathInfo.setAge(cursor.getInt(4));
                tableDeathInfo.setAgeUnit(cursor.getString(5));
                tableDeathInfo.setRelationToHoh(cursor.getString(6));
                tableDeathInfo.setDeathCause(cursor.getString(7));
                tableDeathInfo.setNotes(cursor.getString(8));
                tableDeathInfo.setSurveyorUserId(cursor.getString(9));
                tableDeathInfo.setPartnerId(cursor.getString(10));
                tableDeathInfo.setCampName(cursor.getString(11));
                tableDeathInfo.setDeviceNumber(cursor.getString(12));
                tableDeathInfo.setDeviceId(cursor.getString(13));
                tableDeathInfo.setSurveyDateTime(cursor.getString(14));
                tableDeathInfo.setSurveyDateTimeMillis(cursor.getLong(15));
                tableDeathInfo.setInactive(toBoolean(cursor.getInt(16)));
                tableDeathInfo.setInactiveReason(cursor.getString(17));
                tableDeathInfo.setModified(toBoolean(cursor.getInt(18)));
                tableDeathInfo.setModifiedDateTime(cursor.getString(19));
                tableDeathInfo.setSynced(toBoolean(cursor.getInt(20)));

                dataManager.merge(tableDeathInfo);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeVitals(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableVitals", null);

        if (cursor.moveToFirst()) {
            do {
                TableVitals tableVitals = new TableVitals();

                tableVitals.setRegistrationId(cursor.getString(1));
                tableVitals.setEhrNumber(cursor.getString(2));
                tableVitals.setVitalType(cursor.getString(3));
                tableVitals.setVitalSubTypes(cursor.getString(4));
                tableVitals.setVitalValues(cursor.getString(5));
                tableVitals.setVitalUnits(cursor.getString(6));
                tableVitals.setVitalDateTime(cursor.getString(7));
                tableVitals.setVitalDateTimeMillis(cursor.getLong(8));
                tableVitals.setVitalImage(cursor.getString(9));
                tableVitals.setNotes(cursor.getString(10));
                tableVitals.setDeviceName(cursor.getString(11));
                tableVitals.setGpsLocation(cursor.getString(12));
                tableVitals.setQrScanned(toBoolean(cursor.getInt(13)));
                tableVitals.setSurveyorUserId(cursor.getString(14));
                tableVitals.setPartnerId(cursor.getString(15));
                tableVitals.setCampName(cursor.getString(16));
                tableVitals.setDeviceNumber(cursor.getString(17));
                tableVitals.setDeviceId(cursor.getString(18));
                tableVitals.setSurveyDateTime(cursor.getString(19));
                tableVitals.setSurveyDateTimeMillis(cursor.getLong(20));
                tableVitals.setInactive(toBoolean(cursor.getInt(21)));
                tableVitals.setInactiveReason(cursor.getString(22));
                tableVitals.setModified(toBoolean(cursor.getInt(23)));
                tableVitals.setModifiedDateTime(cursor.getString(24));
                tableVitals.setSynced(toBoolean(cursor.getInt(25)));

                dataManager.merge(tableVitals);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeCampInfo(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableCampInfo", null);

        if (cursor.moveToFirst()) {
            do {
                TableCampInfo tableCampInfo = new TableCampInfo();

                tableCampInfo.setCampName(cursor.getString(1));
                tableCampInfo.setPartnerId(cursor.getString(2));
                tableCampInfo.setStartDate(cursor.getString(3));
                tableCampInfo.setEndDate(cursor.getString(4));
                tableCampInfo.setDoorToDoor(toBoolean(cursor.getInt(5)));
                tableCampInfo.setDeviceId(cursor.getString(6));
                tableCampInfo.setEntryDateTime(cursor.getString(7));
                tableCampInfo.setEntryDateTimeMillis(cursor.getLong(8));
                tableCampInfo.setModified(toBoolean(cursor.getInt(9)));
                tableCampInfo.setModifiedDateTime(cursor.getString(10));
                tableCampInfo.setSynced(toBoolean(cursor.getInt(11)));

                tableCampInfo.save();

            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private static void mergeKitConifg(DataManager dataManager, SQLiteDatabase oldDb) {
        Cursor cursor = oldDb.rawQuery("SELECT * FROM TableKitConfig", null);

        if (cursor.moveToFirst()) {
            do {
                TableKitConfig tableKitConfig = new TableKitConfig();

                tableKitConfig.setConfigType(cursor.getString(1));
                tableKitConfig.setMake(cursor.getString(2));
                tableKitConfig.setModel(cursor.getString(3));
                tableKitConfig.setProductName(cursor.getString(4));
                tableKitConfig.setProductId(cursor.getString(5));
                tableKitConfig.setMacAddress(cursor.getString(6));
                tableKitConfig.setValidStartDate(cursor.getString(7));
                tableKitConfig.setValidEndDate(cursor.getString(8));
                tableKitConfig.setNotes(cursor.getString(9));
                tableKitConfig.setPartnerId(cursor.getString(10));
                tableKitConfig.setDeviceNumber(cursor.getString(11));
                tableKitConfig.setDeviceId(cursor.getString(12));
                tableKitConfig.setEntryDateTime(cursor.getString(13));

                tableKitConfig.setEntryDateTimeMillis(cursor.getLong(14));
                tableKitConfig.setInactive(toBoolean(cursor.getInt(15)));
                tableKitConfig.setInactiveReason(cursor.getString(16));
                tableKitConfig.setModified(toBoolean(cursor.getInt(17)));
                tableKitConfig.setModifiedDateTime(cursor.getString(18));
                tableKitConfig.setSynced(true);

                dataManager.merge(tableKitConfig);

            } while (cursor.moveToNext());
        }

        cursor.close();
    }
}