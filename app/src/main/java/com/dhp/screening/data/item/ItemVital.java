package com.dhp.screening.data.item;

import com.dhp.screening.data.table.TableVitals;

public class ItemVital {

    public static final int VITAL_HEADER = 11;
    public static final int VITAL_CHILD  = 12;

    private int vitalType;

    private ItemVitalHeader header;

    private TableVitals vital;

    public ItemVital(int vitalType, ItemVitalHeader header, TableVitals vital) {
        this.vitalType = vitalType;
        this.header = header;
        this.vital = vital;
    }

    public int getVitalType() {
        return vitalType;
    }

    public void setVitalType(int vitalType) {
        this.vitalType = vitalType;
    }

    public ItemVitalHeader getHeader() {
        return header;
    }

    public void setHeader(ItemVitalHeader header) {
        this.header = header;
    }

    public TableVitals getVital() {
        return vital;
    }

    public void setVital(TableVitals vital) {
        this.vital = vital;
    }
}