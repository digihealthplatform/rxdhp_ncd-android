package com.dhp.screening.data.event;


public class EventOpenPicker {

    private boolean isCamera;

    public boolean isCamera() {
        return isCamera;
    }

    public void setCamera(boolean camera) {
        isCamera = camera;
    }
}