package com.dhp.screening.data.item;


public class ItemDiseaseControl {

    private String diseaseName = "";
    private String diseaseLang = "";

    private String testDoneTime     = "";
    private String testDoneTimeUnit = "";

    private String informedAboutDisease = "";
    private String medications          = "";

    private boolean[] changesChecked;
    private String    changesOther = "";

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDiseaseLang() {
        return diseaseLang;
    }

    public void setDiseaseLang(String diseaseLang) {
        this.diseaseLang = diseaseLang;
    }

    public String getTestDoneTime() {
        return testDoneTime;
    }

    public void setTestDoneTime(String testDoneTime) {
        this.testDoneTime = testDoneTime;
    }

    public String getTestDoneTimeUnit() {
        return testDoneTimeUnit;
    }

    public void setTestDoneTimeUnit(String testDoneTimeUnit) {
        this.testDoneTimeUnit = testDoneTimeUnit;
    }

    public String getInformedAboutDisease() {
        return informedAboutDisease;
    }

    public void setInformedAboutDisease(String informedAboutDisease) {
        this.informedAboutDisease = informedAboutDisease;
    }

    public String getMedications() {
        return medications;
    }

    public void setMedications(String medications) {
        this.medications = medications;
    }

    public boolean[] getChangesChecked() {
        return changesChecked;
    }

    public void setChangesChecked(boolean[] changesChecked) {
        this.changesChecked = changesChecked;
    }

    public String getChangesOther() {
        return changesOther;
    }

    public void setChangesOther(String changesOther) {
        this.changesOther = changesOther;
    }
}