package com.dhp.screening.data.item;


public class ItemScreeningQuestion {

    public static final int TYPE_HEADER = 11;
    public static final int TYPE_CHILD  = 12;

    private int itemType;

    private ItemQuestionHeader   questionHeader;
    private ItemQuestionChildren questionChildren;

    public ItemScreeningQuestion(int itemType, ItemQuestionHeader questionHeader, ItemQuestionChildren questionChildren) {
        this.itemType = itemType;
        this.questionHeader = questionHeader;
        this.questionChildren = questionChildren;
    }

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public ItemQuestionHeader getQuestionHeader() {
        return questionHeader;
    }

    public void setQuestionHeader(ItemQuestionHeader questionHeader) {
        this.questionHeader = questionHeader;
    }

    public ItemQuestionChildren getQuestionChildren() {
        return questionChildren;
    }

    public void setQuestionChildren(ItemQuestionChildren questionChildren) {
        this.questionChildren = questionChildren;
    }
}