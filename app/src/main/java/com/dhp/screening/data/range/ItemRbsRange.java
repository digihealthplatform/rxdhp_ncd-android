package com.dhp.screening.data.range;

import com.google.gson.annotations.SerializedName;

public class ItemRbsRange {

    @SerializedName("range")
    private String range = "";

    @SerializedName("min")
    private String min = "";

    @SerializedName("max")
    private String max = "";

    @SerializedName("color")
    private String color = "";

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getMin() {
        return min;
    }

    public void setMin(String min) {
        this.min = min;
    }

    public String getMax() {
        return max;
    }

    public void setMax(String max) {
        this.max = max;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}