package org.opencv.android;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Build;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ViewGroup.LayoutParams;
import android.widget.Toast;

import com.google.api.client.util.NullValue;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import static android.content.Context.MODE_PRIVATE;

/**
 * This class is an implementation of the Bridge View between OpenCV and Java Camera.
 * This class relays on the functionality available in base class and only implements
 * required functions:
 * connectCamera - opens Java camera and sets the PreviewCallback to be delivered.
 * disconnectCamera - closes the camera and stops preview.
 * When frame is delivered via callback from Camera - it processed via OpenCV to be
 * converted to RGBA32 and then passed to the external callback for modifications if required.
 */
public class JavaCameraView extends CameraBridgeViewBase implements PreviewCallback {

    private static final int    MAGIC_TEXTURE_ID = 10;
    private static final String TAG              = "JavaCameraView";

    private byte    mBuffer[];
    private Mat[]   mFrameChain;
    private int     mChainIdx = 0;
    private Thread  mThread;
    private boolean mStopThread;

    protected Camera            mCamera;
    protected JavaCameraFrame[] mCameraFrame;
    private   SurfaceTexture    mSurfaceTexture;

    public static class JavaCameraSizeAccessor implements ListItemAccessor {

        @Override
        public int getWidth(Object obj) {
            Camera.Size size = (Camera.Size) obj;
            return size.width;
        }

        @Override
        public int getHeight(Object obj) {
            Camera.Size size = (Camera.Size) obj;
            return size.height;
        }
    }

    public JavaCameraView(Context context, int cameraId) {
        super(context, cameraId);
    }

    public JavaCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected boolean initializeCamera(int width, int height) {
        Log.d(TAG, "Initialize java camera");
        boolean result = true;
        synchronized (this) {
            mCamera = null;

            if (mCameraIndex == CAMERA_ID_ANY) {
                Log.d(TAG, "Trying to open camera with old open()");
                try {
                    mCamera = Camera.open();
                } catch (Exception e) {
                    Log.e(TAG, "Camera is not available (in use or does not exist): " + e.getLocalizedMessage());
                }

                if (mCamera == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    boolean connected = false;
                    for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); ++camIdx) {
                        Log.d(TAG, "Trying to open camera with new open(" + Integer.valueOf(camIdx) + ")");
                        try {
                            mCamera = Camera.open(camIdx);
                            connected = true;
                        } catch (RuntimeException e) {
                            Log.e(TAG, "Camera #" + camIdx + "failed to open: " + e.getLocalizedMessage());
                        }
                        if (connected) break;
                    }
                }
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
                    int localCameraIndex = mCameraIndex;
                    if (mCameraIndex == CAMERA_ID_BACK) {
                        Log.i(TAG, "Trying to open back camera");
                        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                        for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); ++camIdx) {
                            Camera.getCameraInfo(camIdx, cameraInfo);
                            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                                localCameraIndex = camIdx;
                                break;
                            }
                        }
                    } else if (mCameraIndex == CAMERA_ID_FRONT) {
                        Log.i(TAG, "Trying to open front camera");
                        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                        for (int camIdx = 0; camIdx < Camera.getNumberOfCameras(); ++camIdx) {
                            Camera.getCameraInfo(camIdx, cameraInfo);
                            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                                localCameraIndex = camIdx;
                                break;
                            }
                        }
                    }
                    if (localCameraIndex == CAMERA_ID_BACK) {
                        Log.e(TAG, "Back camera not found!");
                    } else if (localCameraIndex == CAMERA_ID_FRONT) {
                        Log.e(TAG, "Front camera not found!");
                    } else {
                        Log.d(TAG, "Trying to open camera with new open(" + Integer.valueOf(localCameraIndex) + ")");
                        try {
                            mCamera = Camera.open(localCameraIndex);
                        } catch (RuntimeException e) {
                            Log.e(TAG, "Camera #" + localCameraIndex + "failed to open: " + e.getLocalizedMessage());
                        }
                    }
                }
            }

            if (mCamera == null)
                return false;

            /* Now set camera parameters */
            try {
                Camera.Parameters params = mCamera.getParameters();
                Log.d(TAG, "getSupportedPictureSizes()");
                List<android.hardware.Camera.Size> sizes = params.getSupportedPictureSizes();

                for (int i = 0; i < sizes.size(); i++) {

                    System.out.println("manav Picture sizes: width " + sizes.get(i).width + " height: " + sizes.get(i).height);

                }

                params.setPictureSize(sizes.get(0).width, sizes.get(0).height);
                mCamera.setParameters(params);

/*
                // Check what resolutions are supported by your camera
                List<android.hardware.Camera.Size> sizesPicture = params.getSupportedPictureSizes();

                // Iterate through all available resolutions and choose one.
                // The chosen resolution will be stored in mSize.
                android.hardware.Camera.Size mSize;
                for (android.hardware.Camera.Size size : sizesPicture) {
                    Log.i(TAG, "Available resolution: "+size.width+" "+size.height);
                    mSize = size;
                }

                Log.i(TAG, "Chosen resolution: "+mSize.width+" "+mSize.height);
                params.setPictureSize(mSize.width, mSize.height);
                mCamera.setParameters(params);
*/

                Log.d(TAG, "getSupportedPreviewSizes()");
                List<android.hardware.Camera.Size> Sizes = params.getSupportedPreviewSizes();


                if (Sizes != null) {
                    /* Select the size that fits surface considering maximum size allowed */
                    Size frameSize = calculateCameraFrameSize(Sizes, new JavaCameraSizeAccessor(), width, height);

                    params.setPreviewFormat(ImageFormat.NV21);
                    Log.d(TAG, "Set preview size to " + Integer.valueOf((int) frameSize.width) + "x" + Integer.valueOf((int) frameSize.height));
                    params.setPreviewSize((int) frameSize.width, (int) frameSize.height);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH && !android.os.Build.MODEL.equals("GT-I9100"))
                        params.setRecordingHint(true);

                    List<String> FocusModes = params.getSupportedFocusModes();

                    for (int i = 0; i < FocusModes.size(); i++) {

                        System.out.println("manav focus modes" + FocusModes.get(i));

                    }

                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
/*

                    if (FocusModes != null && FocusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
                    {
                        System.out.println("manav in focus mode");
                        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    }
*/


                    SharedPreferences pref = getContext().getSharedPreferences("hb_app_preferences", MODE_PRIVATE);

                    boolean torch = pref.getBoolean("torch", false);

                    if (torch) {
                        params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    } else params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

                    // params.setExposureCompensation(2);
                    params.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_CLOUDY_DAYLIGHT);

                    String saturationMin = params.get("saturation-min");
                    String saturationMax = params.get("saturation-max");

                    System.out.println(params.getPreviewSize());

                    //    params.setPreviewSize(1000,700);

                    for (Camera.Size size : params.getSupportedPreviewSizes()) {


                        System.out.println(size.width + " x " + size.height);


                        if (size.width == 1280) {
                            System.out.println("manav");
                            params.setPreviewSize(1280, size.height);
                            break;
                        }

                        if (size.width == 1024) {
                            System.out.println("manav 2");
                            params.setPreviewSize(1024, size.height);
                            break;
                        }


                    }

                    mCamera.setParameters(params);
                    params = mCamera.getParameters();

                    mFrameWidth = params.getPreviewSize().width;
                    mFrameHeight = params.getPreviewSize().height;

                    System.out.println("frame width :" + mFrameWidth);
                    System.out.println("frame Height : " + mFrameHeight);

                    if ((getLayoutParams().width == LayoutParams.MATCH_PARENT) && (getLayoutParams().height == LayoutParams.MATCH_PARENT))
                        mScale = Math.min(((float) height) / mFrameHeight, ((float) width) / mFrameWidth);
                    else
                        mScale = 0;

                    if (mFpsMeter != null) {
                        // TODO: 12/04/2019 remove hardcoding
                        mFpsMeter.setResolution(mFrameWidth, mFrameHeight);
//                        mFpsMeter.setResolution(mFrameWidth, mFrameHeight);
                    }

                    int size = mFrameWidth * mFrameHeight;
                    size = size * ImageFormat.getBitsPerPixel(params.getPreviewFormat()) / 8;
                    mBuffer = new byte[size];

                    mCamera.addCallbackBuffer(mBuffer);
                    mCamera.setPreviewCallbackWithBuffer(this);

                    mFrameChain = new Mat[2];
                    mFrameChain[0] = new Mat(mFrameHeight + (mFrameHeight / 2), mFrameWidth, CvType.CV_8UC1);
                    mFrameChain[1] = new Mat(mFrameHeight + (mFrameHeight / 2), mFrameWidth, CvType.CV_8UC1);

                    AllocateCache();

                    mCameraFrame = new JavaCameraFrame[2];
                    mCameraFrame[0] = new JavaCameraFrame(mFrameChain[0], mFrameWidth, mFrameHeight);
                    mCameraFrame[1] = new JavaCameraFrame(mFrameChain[1], mFrameWidth, mFrameHeight);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        mSurfaceTexture = new SurfaceTexture(MAGIC_TEXTURE_ID);
                        mCamera.setPreviewTexture(mSurfaceTexture);
                    } else
                        mCamera.setPreviewDisplay(null);

                    /* Finally we are ready to start the preview */
                    Log.d(TAG, "startPreview");
                    mCamera.startPreview();
                } else
                    result = false;
            } catch (Exception e) {
                result = false;
                e.printStackTrace();
            }
        }

        return result;
    }

    protected void releaseCamera() {
        synchronized (this) {
            if (mCamera != null) {
                mCamera.stopPreview();
                mCamera.setPreviewCallback(null);

                mCamera.release();
            }
            mCamera = null;
            if (mFrameChain != null) {
                mFrameChain[0].release();
                mFrameChain[1].release();
            }
            if (mCameraFrame != null) {
                mCameraFrame[0].release();
                mCameraFrame[1].release();
            }
        }
    }

    private boolean mCameraFrameReady = false;

    @Override
    protected boolean connectCamera(int width, int height) {

        /* 1. We need to instantiate camera
         * 2. We need to start thread which will be getting frames
         */
        /* First step - initialize camera connection */
        Log.d(TAG, "Connecting to camera");
        if (!initializeCamera(width, height))
            return false;

        mCameraFrameReady = false;

        /* now we can start update thread */
        Log.d(TAG, "Starting processing thread");
        mStopThread = false;
        mThread = new Thread(new CameraWorker());
        mThread.start();

        return true;
    }

    @Override
    protected void disconnectCamera() {
        /* 1. We need to stop thread which updating the frames
         * 2. Stop camera and release it
         */
        Log.d(TAG, "Disconnecting from camera");
        try {
            mStopThread = true;
            Log.d(TAG, "Notify thread");
            synchronized (this) {
                this.notify();
            }
            Log.d(TAG, "Wating for thread");
            if (mThread != null)
                mThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            mThread = null;
        }

        /* Now release camera */
        releaseCamera();

        mCameraFrameReady = false;
    }

    @Override
    public void onPreviewFrame(byte[] frame, Camera arg1) {
        //Log.d(TAG, "Preview Frame received. Frame size: " + frame.length);
        synchronized (this) {
            mFrameChain[mChainIdx].put(0, 0, frame);
            mCameraFrameReady = true;
            this.notify();
        }
        if (mCamera != null)
            mCamera.addCallbackBuffer(mBuffer);
    }


    private class JavaCameraFrame implements CvCameraViewFrame {
        @Override
        public Mat gray() {
            return mYuvFrameData.submat(0, mHeight, 0, mWidth);
        }

        @Override
        public Mat rgba() {
            Imgproc.cvtColor(mYuvFrameData, mRgba, Imgproc.COLOR_YUV2RGBA_NV21, 4);
            return mRgba;
        }

        public JavaCameraFrame(Mat Yuv420sp, int width, int height) {
            super();
            mWidth = width;
            mHeight = height;
            mYuvFrameData = Yuv420sp;
            mRgba = new Mat();
        }

        public void release() {
            mRgba.release();
        }

        private Mat mYuvFrameData;
        private Mat mRgba;
        private int mWidth;
        private int mHeight;
    }

    ;

    private class CameraWorker implements Runnable {

        @Override
        public void run() {
            do {
                boolean hasFrame = false;
                synchronized (JavaCameraView.this) {
                    try {
                        while (!mCameraFrameReady && !mStopThread) {
                            JavaCameraView.this.wait();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (mCameraFrameReady) {
                        mChainIdx = 1 - mChainIdx;
                        mCameraFrameReady = false;
                        hasFrame = true;
                    }
                }

                if (!mStopThread && hasFrame) {
                    if (!mFrameChain[1 - mChainIdx].empty())
                        deliverAndDrawFrame(mCameraFrame[1 - mChainIdx]);
                }
            } while (!mStopThread);
            Log.d(TAG, "Finish processing thread");
        }
    }

    public int getContrast() {
        Camera.Parameters params = mCamera.getParameters();

        try {
            return Integer.parseInt(params.get("contrast"));
        } catch (Exception e) {
            return 0;
        }
    }

    public int getSaturation() {
        Camera.Parameters params = mCamera.getParameters();

        try {
            return Integer.parseInt(params.get("saturation"));
        } catch (Exception e) {
            return 0;
        }
    }

    public void setContrast(int value) {
        Camera.Parameters params = mCamera.getParameters();
        params.set("contrast", value);
        List<Camera.Size> resList = mCamera.getParameters().getSupportedPictureSizes();

        int listNum = 1;// 0 is the maximum resolution
        int width   = resList.get(listNum).width;
        int height  = resList.get(listNum).height;
        params.setPictureSize(width, height);

        mCamera.setParameters(params);
    }

    public void setSaturation(int value) {
        Camera.Parameters params = mCamera.getParameters();
//        String c = params.get("contrast");
//        String s = params.get("saturation");
//        params.set("saturation", value);

        String supportedValues = params.get("iso-values");

        if (supportedValues == null) {
            // ISO is not supported
        } else {
            params.set("iso", "ISO100");
        }

        mCamera.setParameters(params);
    }

    public void setIso(String iso) {
        Camera.Parameters params = mCamera.getParameters();
        params.set("iso", iso);
        mCamera.setParameters(params);
    }

    public String getIsoValues() {
        Camera.Parameters params = mCamera.getParameters();
//        String c = params.get("contrast");
//        String s = params.get("saturation");
//        params.set("saturation", value);

        return params.get("iso-values");
    }

    public void onCaptureRaw() {
        Camera.Parameters params = mCamera.getParameters();
        params.set("rawsave-mode", "1");

        File file = new File(Environment.getExternalStorageDirectory() + "/dirr/aa.raw");

        Log.d(TAG, "onCaptureRaw: " + file.getAbsolutePath());

        params.set("rawfname", file.getAbsolutePath());
        mCamera.setParameters(params);
        mCamera.takePicture(new Camera.ShutterCallback() {
            @Override
            public void onShutter() {

            }
        }, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                if (data != null) {
                    Toast.makeText(getContext(), "Data is not empty", Toast.LENGTH_SHORT).show();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);

                    if (bitmap != null) {

                        File dir = new File(Environment.getExternalStorageDirectory() + "/HBRaw");

                        if (!dir.exists()) {
                            dir.mkdirs();
                        }

                        File file = new File(Environment.getExternalStorageDirectory() + "/HBRaw", System.currentTimeMillis() + ".jpg");

                        try {

                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                            bos.write(data);
                            bos.flush();
                            bos.close();
//
//                            FileOutputStream fileOutputStream = new FileOutputStream(file);
//                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
//
//                            fileOutputStream.flush();
//                            fileOutputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (Exception exception) {
                            exception.printStackTrace();
                        }

                    }
                } else {
                    Toast.makeText(getContext(), "Data is empty", Toast.LENGTH_SHORT).show();
                }
            }
        }, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                int a = 0;
            }
        });
    }
}
