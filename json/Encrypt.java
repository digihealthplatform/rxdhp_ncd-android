import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class Encrypt {

    public static final String INPUT_JSON_ENCRYPT_KEY = "dhp123enc@key";

    private static String encrypt(byte[] plainText) throws Exception {
        String strData = "";

        try {
            SecretKeySpec skeyspec = new SecretKeySpec(INPUT_JSON_ENCRYPT_KEY.getBytes(), "Blowfish");
            Cipher        cipher   = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, skeyspec);

            byte[] encrypted = cipher.doFinal(plainText);

            strData = Base64.getEncoder().encodeToString(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }

        return strData;
    }

    private static String decrypt(byte[] encryptedText) throws Exception {

        String strData = "";

        try {
            SecretKeySpec skeyspec = new SecretKeySpec(INPUT_JSON_ENCRYPT_KEY.getBytes(), "Blowfish");
            Cipher        cipher   = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, skeyspec);

            byte[] decrypted = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
            strData = new String(decrypted, "UTF-8");

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e);
        }

        return strData;
    }

    private static String readFile(String fileName) {

        StringBuilder content = new StringBuilder();
        try {
            File fileDir = new File(fileName);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(fileDir), StandardCharsets.UTF_8));
            String str;

            while ((str = in.readLine()) != null) {
                content.append(str);
            }

            in.close();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return content.toString();
    }

    private static void encryptAndSave(String inputFileName, String outputFilePath) {
        String fileContent = readFile(inputFileName);

        try {
            String encryptedText = encrypt(fileContent.getBytes(StandardCharsets.UTF_8));

            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outputFilePath), StandardCharsets.UTF_8));

            out.write(encryptedText);
            out.close();

        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        encryptAndSave("app_config.json", "../app/src/releaseFull/assets/joqvuktpo.ktpo");
        encryptAndSave("app_config_a.json", "../app/src/releaseA/assets/joqvuktpo.ktpo");
        encryptAndSave("app_config_b.json", "../app/src/releaseB/assets/joqvuktpo.ktpo");

        encryptAndSave("data_english.json", "../app/src/main/assets/ebub_fohmjti.ktpo");
        encryptAndSave("data_kannada.json", "../app/src/main/assets/ebub_lboobeb.ktpo");
        encryptAndSave("data_hindi.json", "../app/src/main/assets/ebub_ijoej.ktpo");

        System.out.println("Encrypted successfully");
    }
}